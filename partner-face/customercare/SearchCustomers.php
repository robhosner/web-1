<?php

    

include_once('db/htt_customers_overlay_ultra.php');

// Provides basic search interface for customers.
function customercare__SearchCustomers($partner_tag, $msisdn, $iccid, $name, $email, $actcode)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success   = FALSE;
  $customers = '';

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__SearchCustomers",
                                    func_get_args(),
                                    array("success"  => $success,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__SearchCustomers", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__SearchCustomers",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customers_list = array();

  // try actcode

  if ( $actcode != '' )
  {
    if ( ( strlen($actcode) == 11 ) && is_numeric($actcode) )
    {
      $customer = get_customer_from_actcode($actcode);

      if ( $customer )
        $customers_list[] = $customer;
    }
    else
    {
      $errors[] = "ERR_API_INVALID_ARGUMENTS: actcode is not valid";
    }
  }

  // try msisdn

  if ( strlen($msisdn) == 11 )
  {
    $msisdn = substr($msisdn,1);
  }

  if ( ( $msisdn != '' ) && is_numeric($msisdn) )
  {
    $customer = get_customer_from_msisdn($msisdn);

    if ( $customer )
      $customers_list[] = $customer;
  }

  // try iccid

  if ( ( $iccid != '' ) && is_numeric($iccid) && ( ! count($customers_list) ) )
  {
    $find_ultra_customer_query_from_iccid = make_find_ultra_customer_query_from_iccid($iccid);

    dlog('',$find_ultra_customer_query_from_iccid);

    $customer = find_customer($find_ultra_customer_query_from_iccid);

    if ( $customer )
      $customers_list[] = $customer;
  }

  // try name

  if ( ( $name != '' ) && ( ! count($customers_list) ) )
  {
    $customers_list = get_customers_from_name($name);
  }

  // try email

  if ( ( $email != '' ) && ( ! count($customers_list) ) )
  {
    $customers_list = get_customers_from_email($email);
  }

  if ( count($customers_list) )
  {
    // prepare output

    foreach($customers_list as $customer)
    {
      $customers[] = array(
        'customer_id'           => $customer->CUSTOMER_ID,
        'plan_state'            => $customer->plan_state,
        'plan_started'          => $customer->plan_started_epoch,
        'current_mobile_number' => $customer->current_mobile_number,
        'current_iccid_full'    => $customer->CURRENT_ICCID_FULL,
        'actcode'               => get_act_code_from_iccid( $customer->ACTIVATION_ICCID ),
        'MVNE'                  => $customer->MVNE
      );
    }

    $success = TRUE;
  }
  else
  {
    $errors[] = "ERR_API_INTERNAL: no customers found";
  }

  return flexi_encode(fill_return($p,
                                  "customercare__SearchCustomers",
                                  func_get_args(),
                                  array("success"   => $success,
                                        "customers" => $customers,
                                        "errors"    => $errors)));
}

?>

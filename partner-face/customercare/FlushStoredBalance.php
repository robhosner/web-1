<?php

include_once('db/htt_customers_overlay_ultra.php');

// Set stored balance to zero.
function customercare__FlushStoredBalance($partner_tag, $reason, $agent_name, $customercare_event_id, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $warnings = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__FlushStoredBalance",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__FlushStoredBalance", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__FlushStoredBalance",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  $customer = get_customer_from_customer_id($customer_id);

  // MVNO-2528: prevent flushing of zero balance
  if ( ! $stored_value = $customer->stored_value)
    return flexi_encode(fill_return($p, __FUNCTION__, func_get_args(), array("success" => $success, "errors"  => array('ERR_API_INVALID_ARGUMENTS: stored balance is empty'))));

  $return = func_flush_stored_value($customer_id);

  $success = $return['success'];
  $errors  = $return['errors'];

  if ( $success )
  {
    $source = 'CSCOURTESY';

    $reference_source = get_reference_source($source);

    $history_params = array(
      "customer_id"            => $customer_id,
      "date"                   => 'now',
      "cos_id"                 => $customer->COS_ID,
      "entry_type"             => 'FLUSH',
      "stored_value_change"    => ( - $stored_value ),
      "balance_change"         => 0,
      "package_balance_change" => 0,
      "charge_amount"          => 0,
      "reference"              => create_guid('PHPAPI'),
      "reference_source"       => 'CS',
      "detail"                 => 'FlushStoredBalance',
      "description"            => $reason,
      "result"                 => 'COMPLETE',
      "source"                 => $source,
      "is_commissionable"      => 0
    );

    $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

    if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
    {
      $warnings[] = "DB error";
      dlog('',"DB Error");
    }
  }

  $fraud_data = array(
    'source' => $customercare_event_id, 'dest' => $customer_id, 'channel' => $agent_name, 'channel_type' => 'customercare', 'request_id' => $customercare_event_id
  );

  $fraud_status = ( $errors ) ? 'error' : 'success' ;

  fraud_event($customer, 'customercare', 'FlushStoredBalance', $fraud_status, $fraud_data);

  return flexi_encode(fill_return($p,
                                  "customercare__FlushStoredBalance",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "warnings" => $warnings,
                                        "errors"   => $errors)));
}

?>

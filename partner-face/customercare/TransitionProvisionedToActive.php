<?php


include_once('db/htt_transition_log.php');
include_once('lib/state_machine/functions.php');


// Attempt a transition from Provisioned to Active.
function customercare__TransitionProvisionedToActive($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__TransitionProvisionedToActive",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__TransitionProvisionedToActive", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__TransitionProvisionedToActive",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  // retrieve customer data

  $customer = get_customer_from_customer_id( $customer_id );

  if ( $customer )
  {
    // get current state
    $state = internal_func_get_state_from_customer_id($customer_id);

    if ( $state )
    {

      if ( $state['state'] == 'Provisioned' )
      {
        // verify that there are no transitions in progress

        $htt_transition_log_select_query = htt_transition_log_select_query(
          array(
            'customer_id' => $customer_id,
            'NOT#CLOSED'  => TRUE,
          )
        );

        $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_select_query));

        if ( ( $query_result ) && is_array($query_result) )
        {

          if ( count($query_result) == 0 )
          {

            # parameters for change_state #

            $resolve_now    = FALSE;
            $max_path_depth = 1;
            $dry_run        = TRUE;
            $targetPlan     = get_plan_from_cos_id( $customer->COS_ID );
            $state_name     = 'Active';
            $context        = array( 'customer_id' => $customer->CUSTOMER_ID );

            // dry run to Active

            $result_status = change_state($context, $resolve_now, $targetPlan, $state_name, $dry_run, $max_path_depth);

            if ( $result_status['success'] == 1 )
            {

              // go to Active

              $dry_run = FALSE;

              $result_status = change_state($context, $resolve_now, $targetPlan, $state_name, $dry_run, $max_path_depth);

              if ( $result_status['success'] == 1 )
              {
                $success = TRUE;
              }
              else # change_state failed (live run)
              { $errors[] = "ERR_API_INTERNAL: could not transition from Provisioned to Active (2)"; }

            }
            else # change_state failed (dry run)
            { $errors[] = "ERR_API_INTERNAL: could not transition from Provisioned to Active (1)"; }

          }
          else # there are transitions in progress
          { $errors[] = "ERR_API_INTERNAL: There are pending transitions for this customer"; }

        }
        else # could not query HTT_TRANSITION_LOG
        { $errors[] = "ERR_API_INTERNAL: DB error"; }

      }
      else # customer not in 'Provisioned' state
      { $errors[] = "ERR_API_INTERNAL: customer not in 'Provisioned' state"; }

    }
    else # cannot determine customer state
    { $errors[] = "ERR_API_INTERNAL: customer state could not be determined"; }

  }
  else # customer not found in DB
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "customercare__TransitionProvisionedToActive",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>

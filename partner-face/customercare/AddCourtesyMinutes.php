<?php

// Gives the customer increased ILD minutes for calling that will expire at the end of the service period; will not be commissioned.
function customercare__AddCourtesyMinutes($partner_tag, $reason, $agent_name, $customercare_event_id, $customer_id, $amount)
{
  // $amount is in cents

  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__AddCourtesyMinutes",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__AddCourtesyMinutes", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__AddCourtesyMinutes",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {

    $return = func_courtesy_add_ild_minutes(
      array(
        'reason'      => $reason,
        'terminal_id' => $agent_name,
        'reference'   => $customercare_event_id,
        'amount'      => ( $amount / 100 ),
        'customer'    => $customer,
        'detail'      => __FUNCTION__
      )
    );

    if ( count($return['errors']) == 0 )
    {
      $success = TRUE;
    }
    else
    {
      $errors = $return['errors'];
    }

  }
  else
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";
  }

  return flexi_encode(fill_return($p,
                                  "customercare__AddCourtesyMinutes",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}

?>

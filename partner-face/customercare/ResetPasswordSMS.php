<?php


include_once('db/customers.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('lib/internal/functions.php');
include_once('lib/messaging/functions.php');
include_once('classes/postageapp.inc.php');


// Sends new login password via SMS, resets PASSWORD.  With TTL, the new password will expire in that many seconds.
function customercare__ResetPasswordSMS($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__ResetPasswordSMS",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__ResetPasswordSMS", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__ResetPasswordSMS",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query(
    array(
      'select_fields'    => array("CUSTOMER_ID", 'current_mobile_number'),
      'customer_id'      => $customer_id
    )
  );

  $ultra_customer_data = mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));

  if ( $ultra_customer_data && is_array($ultra_customer_data) && count($ultra_customer_data) && trim($ultra_customer_data[0]->current_mobile_number) )
  {
    # generate a temporary password ( token = 'reset_password' )

    $password = func_create_temp_password( $customer_id , 'reset_password', 60*60*24);

    $result = funcSendExemptCustomerSMSTempPassword(
      array(
        'customer'      => $ultra_customer_data[0],
        'temp_password' => $password
      )
    );

    if ( count($result['errors']) == 0 )
    {
      # reset user password
      $check = reset_customer_password( $customer_id );

      if ( $check )
      {
        $success = TRUE;
      }
      else
      { $errors[] = "ERR_API_INTERNAL: DB error"; }

    }
    else
    { $errors[] = "ERR_API_INTERNAL: internal error"; }

  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer or mobile number not found"; }

  return flexi_encode(fill_return($p,
                                  "customercare__ResetPasswordSMS",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>

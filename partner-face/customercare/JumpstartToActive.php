<?php


include_once('lib/state_machine/aspider.php');
include_once('lib/state_machine/functions.php');


// Activate the given customer, if possible.
function customercare__JumpstartToActive($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  $success = FALSE;

  $warnings = array();

  $message = '';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__JumpstartToActive",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "customercare__JumpstartToActive", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__JumpstartToActive",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);
  
  // clean MSISDN
  $customer->current_mobile_number = trim($customer->current_mobile_number);

  $customers_from_iccid  = get_ultra_customers_from_iccid($customer->CURRENT_ICCID_FULL,array('customer_id','plan_state'));

  foreach ($customers_from_iccid as $from_iccid)
  {
    $plan_state = $from_iccid->plan_state;
    if ($from_iccid->customer_id != $customer_id && ($plan_state == STATE_ACTIVE || $plan_state == STATE_PROVISIONED))
    {
      $errors[] =  "ERROR: $plan_state customer with ICCID $customer->CURRENT_ICCID_FULL already exists.";
      break;
    }
  }

  if (!count($errors) && $customer->current_mobile_number && is_numeric($customer->current_mobile_number))
  {
    $customers_from_msisdn = get_ultra_customers_from_msisdn($customer->current_mobile_number,array('customer_id','plan_state'));
    
    foreach ($customers_from_msisdn as $from_msisdn)
    {
      $plan_state = $from_msisdn->plan_state;
      if ($from_msisdn->customer_id != $customer_id && ($plan_state == STATE_ACTIVE || $plan_state == STATE_PROVISIONED))
      {
        $errors[] =  "ERROR: $plan_state customer with MSISDN $customer->current_mobile_number already exists.";
        break;
      }
    }
  }

  if (count($errors))
  {
    return flexi_encode(fill_return($p,
                                  "customercare__JumpstartToActive",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  if ( $customer )
  {
    /* *** get current state *** */
    $state = internal_func_get_state_from_customer_id($customer_id);

    $context = array(
      'customer_id' => $customer_id
    );

    if ( $state )
    {
      if ( $state['state'] == 'Port-In Requested' )
      {
        // Completed ports but have not transitioned from Port-In Requested

        $message .= "The customer was in Port-In Requested, we checked if it could be transitioned to Provisioned or Active.";

        try
        {
          $portInQueue = new \PortInQueue();

          $result = $portInQueue->loadByMsisdn( $customer->current_mobile_number );

          if ( $result->is_success() )
          {
            $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

            $result = $mwControl->mwQueryStatus(
              array(
                'actionUUID'         => $request_id,
                'msisdn'             => $customer->current_mobile_number
              )
            );

            teldata_change_db();

            if ( $result->is_success() )
            {
              $message .= " We attempted a QueryStatus.";

              dlog('',"QueryStatus result data = %s",$result->data_array);

              if ( isset($result->data_array['body']) )
              {
                dlog('',"QueryStatus result data body = %s",$result->data_array['body']);

                if ( is_object($result->data_array['body'])
                  && property_exists( $result->data_array['body'] , 'ResultCode'        )
                  && property_exists( $result->data_array['body'] , 'ResultMsg'         )
                  && property_exists( $result->data_array['body'] , 'QueryStatusResult' )
                  && property_exists( $result->data_array['body'] , 'PortStatus'        )
                  && ( $result->data_array['body']->ResultCode        == '100'      )
                  && ( $result->data_array['body']->ResultMsg         == 'Success'  )
                  && ( $result->data_array['body']->QueryStatusResult == '1'        )
                  && ( $result->data_array['body']->PortStatus        == 'COMPLETE' )
                )
                {
                  $message .= " According to QueryStatus the Port Attempt is Complete.";

                  // Check to make sure that no other customer is active with this MSISDN
                  $overlay_data = get_ultra_customers_from_msisdn(
                    $customer->current_mobile_number,
                    array('customer_id','plan_state')
                  );

                  dlog('',"overlay_data from msisdn = %s",$overlay_data);

                  if ( $overlay_data && is_array($overlay_data) && count($overlay_data) )
                  {
                    if ( count($overlay_data) > 1 )
                      throw new Exception(
                        "ERR_API_INTERNAL: MSISDN ".$customer->current_mobile_number." is currently assigned to at least 2 customers: ".
                        $overlay_data[0]->customer_id." and ".$overlay_data[1]->customer_id
                      );
                    else
                      // count($overlay_data) == 1
                      if ( $overlay_data[0]->customer_id != $customer_id )
                        throw new Exception(
                          "ERR_API_INTERNAL: MSISDN ".$customer->current_mobile_number." is currently assigned to a different customer: ".$overlay_data[0]->customer_id
                        );
                  }

                  // Check to make sure that no other customer is active on this ICCID
                  $overlay_data = get_ultra_customers_from_iccid( $customer->CURRENT_ICCID_FULL , array('customer_id') );

                  dlog('',"overlay_data from iccid = %s",$overlay_data);

                  if ( $overlay_data && is_array($overlay_data) && count($overlay_data) )
                  {
                    if ( count($overlay_data) > 1 )
                      throw new Exception(
                        "ERR_API_INTERNAL: ICCID ".$customer->CURRENT_ICCID_FULL." is currently assigned to at least 2 customers: ".
                        $overlay_data[0]->customer_id." and ".$overlay_data[1]->customer_id
                      );
                    else
                      // count($overlay_data) == 1
                      if ( $overlay_data[0]->customer_id != $customer_id )
                        throw new Exception(
                          "ERR_API_INTERNAL: ICCID ".$customer->CURRENT_ICCID_FULL." is currently assigned to a different customer: ".$overlay_data[0]->customer_id
                        );
                  }

                  // check ICCID with mwGetNetworkDetails
                  $result = $mwControl->mwGetNetworkDetails(
                    array(
                      'iccid'      => $customer->CURRENT_ICCID_FULL,
                      'actionUUID' => $request_id
                    )
                  );

                  teldata_change_db();

                  if ( $result->is_failure() )
                    throw new Exception("ERR_API_INTERNAL: [ Port-In Requested ] GetNetworkDetails failed");

                  $message .= " We attempted a GetNetworkDetails.";

                  dlog('',"GetNetworkDetails result data = %s",$result->data_array);

                  $msisdn = NULL;

                  if ( isset($result->data_array['body']) )
                  {
                    dlog('',"GetNetworkDetails result data body = %s",$result->data_array['body']);

                    if ( is_object($result->data_array['body'])
                      && property_exists( $result->data_array['body'] , 'MSISDN' )
                      && $result->data_array['body']->MSISDN
                      && ( $result->data_array['body']->MSISDN == $customer->current_mobile_number )
                    )
                      $msisdn = $customer->current_mobile_number;
                  }

                  if ( !$msisdn )
                    throw new Exception(
                      "ERR_API_INTERNAL: [ Port-In Requested ] ICCID ".$customer->CURRENT_ICCID_FULL." is not associated with MSISDN ".$customer->current_mobile_number
                    );

                  $message .= " We tried to set PORT_STATUS to COMPLETE";

                  $redis = new \Ultra\Lib\Util\Redis();

                  $redis->set( 'ultra/port/status/'            . $msisdn , 'COMPLETE' , 60*60*24*7 );
                  $redis->set( 'ultra/port/update_timestamp/'  . $msisdn , time()     , 60*60*24*7 );
                  $redis->set( 'ultra/port/port_query_status/' . $msisdn , 'COMPLETE' , 60*60*24*7 );

                  // connect to ULTRA_ACC DB
                  \Ultra\Lib\DB\ultra_acc_connect();

                  $result = $portInQueue->endAsSuccess();

                  teldata_change_db();

                  dlog('',"PortInQueue::endAsSuccess result = %s",$result);

                  // create a transition from Port In Requested to Provisioned or Active because we assume the port was successful

                  dlog('', "change_state 1: trying transition 'Port Activated'");

                  // trying to transition to Active or Provisioned
                  $result_status = change_state($context, TRUE, 'Port Activated', 'take transition', TRUE, 1);

                  $try_provision = ! $result_status['success'];

                  if ( ! $try_provision )
                  {
                    $message .= " We tried to transition to Active";

                    dlog('', "change_state 2: trying to transition to Active");
                    $change = change_state($context, TRUE, 'Port Activated', 'take transition', FALSE, 1); # [ Port-In Requested ] => [ Active ]

                    $success = $change['success'];

                    if ( ! $success )
                    {
                      dlog('', "change_state 2: transitioning to Active failed, trying to go into Provisioned");
                      $try_provision = TRUE;
                    }
                  }

                  if ( $try_provision )
                  {
                    $message .= " We tried to transition to Provisioned";

                    dlog('', "change_state 3: trying to transition to Provisioned");
                    $change = change_state($context, TRUE, 'Port Provisioned', 'take transition', FALSE, 1); # [ Port-In Requested ] => [ Provisioned ]

                    $success = $change['success'];

                    if ( ! $success )
                    {
                      dlog('', "change_state 3: transitioning to Provisioned failed");
                      throw new Exception("ERR_API_INTERNAL: Cannot transition from Port In Requested to Provisioned or Active");
                    }
                  }
                }
              }
            }
            else
              throw new Exception("ERR_API_INTERNAL: [ Port-In Requested ] QueryStatus failed");
          }
          else
            throw new Exception("ERR_API_INTERNAL: [ Port-In Requested ] could not find Portin queue data in DB");
        }
        catch(Exception $e)
        {
          dlog('', $e->getMessage());
          $errors[] = $e->getMessage();
        }
      }
      elseif ( $state['state'] == 'Promo Unused' )
      {
        $message .= "The customer was in Promo Unused state, we tried to activate him.";

        $result_status = activate_promo_account($state,$customer,$context);

        if ( count($result_status['errors']) )
          $errors = $result_status['errors'];
        else
          $success = TRUE;
      }
      elseif ( $state['state'] == 'Suspended' )
      {
        $message .= "The customer was in Suspended state, we tried to reactivate him.";

        $result_status = reactivate_suspended_account($state,$customer,$context);

        if ( count($result_status['errors']) )
          $errors = $result_status['errors'];
        else
        {
          $success = $result_status['reactivated'];

          if ( ! $success )
            $errors[] = "ERR_API_INTERNAL: the suspended account could not be reactivated";
        }
      }
      elseif ( $state['state'] == 'Provisioned' )
      {
        // PROD-665: update missing MSISDN
        if (empty($customer->current_mobile_number))
          $customer->current_mobile_number = queryAndUpdateSubscriberMsisdn($request_id, $customer->CURRENT_ICCID_FULL, $customer_id);

        $message .= "The customer was in Provisioned state, we tried to activate him.";

        $result_status = activate_provisioned_account($state,$customer,$context);

        if ( count($result_status['errors']) )
          $errors = $result_status['errors'];
        else
        {
          $success = $result_status['activated'];

          if ( ! $success )
            $errors[] = "ERR_API_INTERNAL: the provisioned account could not be activated";
        }
      }
      elseif ( $state['state'] == 'Active' )
      {
        // PROD-665: update missing MSISDN
        if (empty($customer->current_mobile_number))
          $customer->current_mobile_number = queryAndUpdateSubscriberMsisdn($request_id, $customer->CURRENT_ICCID_FULL, $customer_id);

        $success    = FALSE;
        $errors[] = "ERR_API_INTERNAL: there was no action to take - customer state is already Active";
      }
      elseif ( $state['state'] == 'Neutral' )
      {
        try
        {
          $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

          // attempt a QuerySubscriber MW call

          $querySubscriberParams = array(
            'actionUUID' => $request_id,
            'iccid'      => $customer->CURRENT_ICCID_FULL,
          );

          if ( $customer->current_mobile_number && is_numeric($customer->current_mobile_number) )
            $querySubscriberParams['msisdn'] = $customer->current_mobile_number;

          $result = $mwControl->mwQuerySubscriber( $querySubscriberParams );

          if ( ! $result->is_success() )
          {
            dlog('',"result errors = %s",$result->get_errors());

            throw new Exception("ERR_API_INTERNAL: [ Neutral ] QuerySubscriber failed ( msisdn = ".$customer->current_mobile_number." ; iccid = ".$customer->CURRENT_ICCID_FULL.")");
          }

          dlog('',"result data = %s",$result->data_array);

          if ( ( ! empty($result->data_array['ResultCode']) ) && ( $result->data_array['ResultCode'] != '100' ) )
            throw new Exception("ERR_API_INTERNAL: [ Neutral ] QuerySubscriber error : ".$result->data_array['ResultCode'].' - '.$result->data_array['ResultMsg']);

          if ( ! isset($result->data_array['body']) )
            throw new Exception("ERR_API_INTERNAL: [ Neutral ] QuerySubscriber output error");

          if ( ( property_exists( $result->data_array['body'] , 'CurrentAsyncService' ) ) && $result->data_array['body']->CurrentAsyncService )
            throw new Exception("ERR_API_INTERNAL: [ Neutral ] QuerySubscriber detected CurrentAsyncService = ".$result->data_array['body']->CurrentAsyncService);

          $message .= "Customer SubscriberStatus = ".$result->data_array['body']->SubscriberStatus." . ";

          if ( $result->data_array['body']->SubscriberStatus != 'Active' )
            throw new Exception("ERR_API_INTERNAL: [ Neutral ] QuerySubscriber detected SubscriberStatus = ".$result->data_array['body']->SubscriberStatus);

          /*
            TODO: update current_mobile_number

            if ( $customer->current_mobile_number && is_numeric($customer->current_mobile_number) )
          */
          // SubscriberStatus is Active ; check ABORTED state transition

          $query = htt_transition_log_select_query(
            array(
              'customer_id'        => $customer_id,
              'status'             => 'ABORTED',
              'action_status'      => 'ABORTED',
              'from_plan_state'    => 'Neutral',
              'real_customer'      => TRUE,
              'order_by'           => 'ORDER BY t.CREATED ASC'
            )
          );

          $aborted_transitions = mssql_fetch_all_objects(logged_mssql_query($query));

          if (!( ( is_array($aborted_transitions) ) && count($aborted_transitions) ))
            throw new Exception("ERR_API_INTERNAL: [ Neutral ] - No aborted transition found");

          dlog('',"aborted_transitions = %s",$aborted_transitions);

          if ( $aborted_transitions[0]->TO_PLAN_STATE == 'Port-In Requested' )
          {
            // Customer failed to transition to 'Port-In Requested'

            if ( $aborted_transitions[0]->ACTION_NAME != 'mvneRequestPortIn' )
              throw new Exception("ERR_API_INTERNAL: [ Neutral ] to [ ".$aborted_transitions[0]->TO_PLAN_STATE." ] - logic not implemented for ACTION_NAME = ".$aborted_transition->ACTION_NAME);

            $message .= "Updated Customer plan_state . ";

            $q = "update htt_customers_overlay_ultra set plan_state = 'Port-In Requested' where customer_id = ".$customer_id;

            if ( ! run_sql_and_check($q) )
              throw new Exception("ERR_API_INTERNAL: [ Neutral ] to [ ".$aborted_transitions[0]->TO_PLAN_STATE." ] - DB error (1)");

            $message .= "Updated Customer cos_id . ";

            $q = "update accounts set cos_id = ".$aborted_transitions[0]->TO_COS_ID." where customer_id = ".$customer_id;

            if ( ! run_sql_and_check($q) )
              throw new Exception("ERR_API_INTERNAL: [ Neutral ] to [ ".$aborted_transitions[0]->TO_PLAN_STATE." ] - DB error (2)");

            $message .= "Initiated state transition to Provisioned . ";

            $change = change_state($context, TRUE, 'Port Provisioned', 'take transition', FALSE, 1);

            if ( ! $change['success'] )
              throw new Exception("ERR_API_INTERNAL: [ Neutral ] to [ ".$aborted_transitions[0]->TO_PLAN_STATE." ] - state machine error - ".$result['errors'][0]);

            $success = TRUE;
          }
          elseif ( $aborted_transitions[0]->TO_PLAN_STATE == 'Provisioned' )
          {
            // Customer failed to transition to Provisioned

            if ( $aborted_transitions[0]->ACTION_NAME != 'mvneProvisionSIM' )
              throw new Exception("ERR_API_INTERNAL: [ Neutral ] to [ ".$aborted_transitions[0]->TO_PLAN_STATE." ] - logic not implemented for ACTION_NAME = ".$aborted_transition->ACTION_NAME);

            $success = update_htt_action_log_status( 'CLOSED' , $aborted_transitions[0]->ACTION_UUID );

            if ( ! $success )
              throw new Exception("ERR_API_INTERNAL: [ Neutral ] to [ ".$aborted_transitions[0]->TO_PLAN_STATE." ] - DB error (1)");

            $success = redo_transition( $aborted_transitions[0]->TRANSITION_UUID );

            $message .= "Transition recovered . ";

            if ( ! $success )
              throw new Exception("ERR_API_INTERNAL: [ Neutral ] to [ ".$aborted_transitions[0]->TO_PLAN_STATE." ] - DB error (2)");

            $success = TRUE;
          }
          else
            throw new Exception("ERR_API_INTERNAL: [ Neutral ] to [ ".$aborted_transitions[0]->TO_PLAN_STATE." ] - logic not implemented yet");
        }
        catch(Exception $e)
        {
          dlog('', $e->getMessage());
          $errors[] = $e->getMessage();
        }
      }
      else
        $errors[] = "ERR_API_INTERNAL: there was no action to take - customer state is not one of the following: Suspended , Provisioned , Port-In Requested , Promo Unused , Neutral";
    }
    else # cannot determine customer state
      $errors[] = "ERR_API_INTERNAL: customer state could not be determined";

  }
  else
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";

  return flexi_encode(fill_return($p,
                                  "customercare__JumpstartToActive",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "warnings" => $warnings,
                                        "message"  => $message,
                                        "errors"   => $errors)));
}

/**
 * queryAndUpdateSubscriberMsisdn
 * query missing subscriber MSISDN by ICCID and update HTT_CUSTOMERS_OVERLAY_ULTRA and HTT_ULTRA_MSISDN and return it
 * @see PROD-665
 */
function queryAndUpdateSubscriberMsisdn($request_id, $iccid, $customer_id)
{
  dlog('', 'parameters: %s', func_get_args());

  try
  {
    // query middleware by ICCID
    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
    $querySubscriberParams = array(
      'actionUUID' => $request_id,
      'iccid'      => $iccid);
    $result = $mwControl->mwQuerySubscriber($querySubscriberParams);

    // update if we received MSISDN
    if ($result->is_success() && ! empty($result->data_array['body']->MSISDN))
    {
      // update HTT_CUSTOMERS_OVERLAY_ULTRA
      $msisdn = $result->data_array['body']->MSISDN;
      $query = Ultra\Lib\DB\makeUpdateQuery('HTT_CUSTOMERS_OVERLAY_ULTRA',
        array('CURRENT_MOBILE_NUMBER' => $msisdn),
        array('CUSTOMER_ID' => $customer_id));
      if ( ! run_sql_and_check($query))
        throw new Exception('failed to update HTT_CUSTOMERS_OVERLAY_ULTRA');

      // update HTT_ULTRA_MSISDN
      if ( ! add_to_htt_ultra_msisdn($customer_id, $msisdn, 1, $request_id))
        throw new Exception('failed to update HTT_ULTRA_MSISDN');
      
      return $msisdn;
    }
    else
      throw new Exception('failed to query MSISDN');
  }
  catch (\Exception $e)
  {
    dlog('', 'EXCEPTION: ' . $e->getMessage());
  }

  return NULL;
}
?>

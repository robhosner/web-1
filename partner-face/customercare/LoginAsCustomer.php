<?php

// Customer Login for Customer Care, no authorization required. Using Customer_Id and account_login as a double verification.
function customercare__LoginAsCustomer($partner_tag, $customer_id, $account_login, $agent_name, $customercare_event_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  dlog('',"agent_name = $agent_name ; customercare_event_id = $customercare_event_id");

  $success   = FALSE;
  $login_url = '';
  $customer = NULL;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__LoginAsCustomer",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "customercare__LoginAsCustomer", func_get_args(), $mock);

  teldata_change_db(); // connect to the DB

  if ( ! ( $errors && count($errors) > 0 ) )
  {
    /* *** get customer data *** */
    $customer = get_ultra_customer_from_customer_id($customer_id, array('plan_state'));

    if ( $customer )
    {
      if ( ! $customer->plan_state )
        $errors[] = "ERR_API_INTERNAL: cannot determine customer status";
    }
    else
      $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";
  }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__LoginAsCustomer",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  if ( $customer->plan_state != STATE_CANCELLED )
  {

    /* *** check customer login *** */

#    if ( $customer->LOGIN_NAME == $account_login )
#    {

      /* *** verify the session *** */

      $customer = NULL;

      if (is_numeric($customer_id))
      {
        $customer = find_customer(make_find_customer_query_anycosid($customer_id));
      }
      else
      {
        $customer = find_customer(make_find_customer_query_anycosid(-1, $customer_id));
      }

      if ( $customer )
      {
        # set up a temporary password

        $password = func_create_temp_password($customer_id);

        $login_url = sprintf('https://%s/login_api_restricted.php?customer_id=%s&password=%s',
                               find_credential("ultra/login/site"),
                               urlencode($customer_id),
                               urlencode($password));

        dlog('',"login_url = <$login_url>");

        $success = TRUE;
      }
      else
      { $errors[] = "ERR_API_INTERNAL: database error"; }
#    }
#    else
#    { $errors[] = "ERR_API_INVALID_ARGUMENTS: invalid login"; }
  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer's account has been cancelled."; }

  return flexi_encode(fill_return($p,
                                  "customercare__LoginAsCustomer",
                                  func_get_args(),
                                  array("success"   => $success,
                                        "login_url" => $login_url,
                                        "errors"    => $errors)));
}


<?php

// Replaces SIM card for another.
function customercare__ReplaceSIMCardByMSISDN($partner_tag, $msisdn, $new_ICCID, $store_id, $user_id)
{
  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  $success  = FALSE;
  $customer = NULL;
  $mvne     = '1';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__ReplaceSIMCardByMSISDN",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  teldata_change_db(); // connect to the DB

  $errors = validate_params($p, "customercare__ReplaceSIMCardByMSISDN", func_get_args(), $mock);

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** verify that new_ICCID is valid *** */

    $newSim = get_htt_inventory_sim_from_iccid($new_ICCID);
    if ( ! $newSim || ! validate_ICCID($newSim,1) )
    { $errors[] = 'ERR_API_INVALID_ARGUMENTS: the given new_ICCID is invalid or already used'; }

    // block replacement ORANGE sim
    if (strtoupper($newSim->PRODUCT_TYPE) == 'ORANGE')
    { $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot replace with Orange SIM'; }
  
  }

  $new_mvne = '1';
  $old_mvne = '1';

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** get customer data *** */

    $customer = get_customer_from_msisdn($msisdn);

    if ( $customer )
      $old_mvne = $customer->MVNE;
    else
      $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";
  }

  if ( $customer )
  {
    /* *** do not allow SIM replacement during MISO tasks *** */
    if ( \Ultra\Lib\MVNE\exists_open_task( $customer->CUSTOMER_ID ) )
      $errors[] = "ERR_API_INTERNAL: The subscriber cannot currently change their details due to system alignment. Please try again in 10 minutes.";
    
    // PROD-511: prohibit modifications to in-Active subscribers
    if ($customer->plan_state !== 'Active')
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: Subscriber must be active to change SIM or phone number.';

    if ( ! isSameBrandByICCID($customer->current_iccid, $new_ICCID))
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: the given new_ICCID is of an INVALID BRAND type';
  }

  if ( ! ($errors && count($errors) > 0) )
  {
    // MVNE associated with $new_ICCID
    $new_mvne = \Ultra\Lib\DB\Getter\getScalar('ICCID', $new_ICCID, 'MVNE');

    if ( ( $new_mvne == '1' ) && ( $old_mvne = '2' ) )
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: SIM/MVNE mismatch';
  }

  if ( ! ($errors && count($errors) > 0) )
    {
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $result = $mwControl->mwCanActivate(
        array(
          'actionUUID' => getNewActionUUID('customercare ' . time()),
          'iccid'      => $new_ICCID
        )
      );
      if ($result->is_failure() || ! isset($result->data_array['available']) || $result->data_array['available'] != 'true')
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot activate this ICCID (2)';
    }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__ReplaceSIMCardByMSISDN",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  $warnings = array();
  $result   = NULL;

    $result = $mwControl->mwChangeSIM(
      array(
        'actionUUID'         => $request_id,
        'msisdn'             => normalize_msisdn($customer->current_mobile_number, TRUE),
        'old_iccid'          => $customer->current_iccid,
        'new_iccid'          => $new_ICCID,
        'customer_id'        => $customer->CUSTOMER_ID
      )
    );

    // the MW call failed
    if ( ! $result->is_success() )
    {
      $errors = $result->get_errors();

      dlog('',"mwChangeSIM result errors = %s",$errors);
    }
    // the MW ChangeSIM command failed
    elseif ( ! $result->data_array['success'] )
    {
      $errors = $result->data_array['errors'];

      dlog('',"mwChangeSIM result data errors = %s",$errors);
    }

    $warnings = $result->get_warnings();

    teldata_change_db(); // done with MVNE2 calls

  if ( ! ($errors && count($errors) > 0) )
  {
    // side effects: HTT_CUSTOMERS_OVERLAY_ULTRA and HTT_INVENTORY_SIM
    $errors = callbackChangeSIM( $new_ICCID , $customer->CUSTOMER_ID , $store_id, $user_id );

    if ( ! count($errors) )
    {
      /* *** store $reason, $agent_name, $customercare_event_id in HTT_BILLING_HISTORY *** */

      $description = 'Replace SIM';
      $entry_type  = 'SIM_REPLACEMENT';

      if ( ( $old_mvne == '1' ) && ( $new_mvne == '2' ) )
      {
        $description = 'MVNEs SIM Swap';
        $entry_type  = 'MVNES_SIM_SWAP';
      }

      $history_params = array(
        "customer_id"            => $customer->CUSTOMER_ID,
        "date"                   => 'now',
        "cos_id"                 => $customer->COS_ID,
        "entry_type"             => $entry_type,
        "stored_value_change"    => 0,
        "balance_change"         => 0,
        "package_balance_change" => 0,
        "charge_amount"          => 0,
        "reference"              => 'customercare__ReplaceSIMCardByMSISDN',
        "reference_source"       => 'CELLUPHONE',
        "detail"                 => 'customercare__ReplaceSIMCardByMSISDN',
        "description"            => $description,
        "result"                 => 'COMPLETE',
        "source"                 => $entry_type,
        "is_commissionable"      => 0,
        "store_id"               => $store_id,
        "clerk_id"               => $user_id
      );

      $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

      if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
      {
        $success = TRUE;
      }
      else
      # htt_billing_history insert error
        $errors[] = "ERR_API_INTERNAL: Database write error (2)";
    }
  }

  return flexi_encode(fill_return($p,
                                  "customercare__ReplaceSIMCardByMSISDN",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>

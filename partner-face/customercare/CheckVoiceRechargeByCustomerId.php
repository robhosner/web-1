<?php

// Return to the caller info about customer and his/her Voice Recharges.
function customercare__CheckVoiceRechargeByCustomerId($customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__CheckVoiceRechargeByCustomerId",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__CheckVoiceRechargeByCustomerId", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__CheckVoiceRechargeByCustomerId",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  $days_plan_expires = '';
  $voice_recharge    = '';

  teldata_change_db();

  // get customer info
  $customer = get_ultra_customer_from_customer_id($customer_id, array('days_before_expire'));
  $account  = get_account_from_customer_id($customer_id, array('COS_ID'));

  if ( $customer && $account )
  {
    if ( ! $account->COS_ID )
    {
      $errors[] = "ERR_API_INVALID_ARGUMENTS: customer is invalid";
    }
    else
    {
      $success = TRUE;

      $days_plan_expires = $customer->days_before_expire;
      $voice_recharge    = json_encode( get_voice_recharge_by_plan( get_plan_from_cos_id($account->COS_ID) ) );
    }
  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "customercare__CheckVoiceRechargeByCustomerId",
                                  func_get_args(),
                                  array("success"           => $success,
                                        "days_plan_expires" => $days_plan_expires,
                                        "voice_recharge"    => $voice_recharge,
                                        "errors"            => $errors)));
}

?>

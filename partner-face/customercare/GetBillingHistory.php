<?php

require_once 'Ultra/Billing/Repositories/Mssql/BillingHistoryRepository.php';

// Returns billing history for a given customer.
use Ultra\Billing\Repositories\Mssql\BillingHistoryRepository;

function customercare__GetBillingHistory($customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;
  // Local Function Variables
  $success        = FALSE;   // Default status is a failure
  $billingHistory = array(); // return NULL Billing History if no data
  $warnings       = array(); // Used to let the user know, in this case, no data was returned.

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
        "customercare__GetBillingHistory",
        func_get_args(),
        array("success"  => TRUE,
            "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params( $p, 
                         "customercare__GetBillingHistory", 
                         func_get_args(), 
                         $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
        "customercare__GetBillingHistory",
        func_get_args(),
        array("success" => FALSE,
            "errors"  => $errors)));
  }
  
  
  // Main Logic -- Extract billing history and return to the caller
  //
  
  try {
    $history = (new BillingHistoryRepository())->getBillingTransactionHistoryByCustomerId($customer_id);
    $count = count($history);
    $billingHistory = [];

    if (!$count)
    {
      $warnings[] = "WARN_API_INTERNAL: No Data Found";
    }

    for ($i = 0, $details = [], $j = $count; $i < $j; $i++)
    {
      // save surcharge detail
      $row = $history[$i];

      if ($row->SURCHARGE_HISTORY_ID)
      {
        $details[] = sprintf('$%.2f %s', $row->AMOUNT, strtolower($row->SURCHARGE_TYPE));
      }

      // check if we are at the end of result set or current transaction
      if ($i == ($j - 1) || $row->order_id != $history[$i + 1]->order_id)
      {
        // append surcharge details to description
        if (count($details))
        {
          $row->DESCRIPTION .= ' (includes ' . implode(', ', $details) . ')';
          unset($details);
          $details = [];
        }
      }

      $billingHistory[] = implode("|", [
        get_date_from_full_date($row->TRANSACTION_DATE),
        $row->ENTRY_TYPE,
        $row->STORED_VALUE_CHANGE,
        $row->BALANCE_CHANGE,
        $row->DESCRIPTION,
        $row->SOURCE,
      ]);
    }
  } catch (Exception $e) {
    // Append the error message to the error array.
        $errors[] = $e->getMessage();
  }
    
  $success = !count($errors);
  
  return flexi_encode(fill_return($p,
      "customercare__GetBillingHistory",
      func_get_args(),
      array( "billing_history" => $billingHistory,
           "success"         => $success,
          "warnings"       => $warnings,
           "errors"          => $errors)));
}

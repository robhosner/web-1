<?php

// Takes away cash form a customer – only to be used by CS for manual adjustments of balance. Use VoidCourtesy Cash to undo courtesy cash.
function customercare__RemoveCash($partner_tag, $reason, $agent_name, $customercare_event_id, $customer_id, $amount)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__RemoveCash",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__RemoveCash", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                    "customercare__RemoveCash",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {

    // verify that the customer has sufficient balance to remove the amount
    if ( ( $customer->BALANCE * 100 ) >= $amount )
    {

      $return = func_courtesy_add_balance(
        array(
          'reason'      => $reason,
          'terminal_id' => $agent_name,
          'reference'   => $customercare_event_id,
          'amount'      => - ( $amount / 100 ),
          'customer'    => $customer,
          'detail'      => __FUNCTION__
        )
      );

      if ( count($return['errors']) == 0 )
      {
        $success = TRUE;

        // TODO: should we trigger a customer transition to 'Suspended' ?
      }
      else
      { $errors = $return['errors']; }

    }
    else
    { $errors[] = "ERR_API_INTERNAL: amount exceeds customer balance."; }

  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "customercare__RemoveCash",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>

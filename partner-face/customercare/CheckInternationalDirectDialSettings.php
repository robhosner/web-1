<?php

// Checks if the customer is correctly setup with ACCOUNT_ALIASES and DESTINATION_ORIGIN_MAP, and if not, correctly sets up customer.
function customercare__CheckInternationalDirectDialSettings($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;
  $repaired = FALSE;
  $messages = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "customercare__CheckInternationalDirectDialSettings",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "repaired" => $repaired,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "customercare__CheckInternationalDirectDialSettings", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "customercare__CheckInternationalDirectDialSettings",
                                    func_get_args(),
                                    array("success"  => $success,
                                          "repaired" => $repaired,
                                          "errors"   => $errors)));

  teldata_change_db();

  $customer = find_customer(
    make_find_ultra_customer_query_from_customer_id(
      $customer_id,
      array( 'u.CUSTOMER_ID' , 'u.current_mobile_number' , 'a.ACCOUNT_ID' , 'a.ACCOUNT_STATUS_TYPE' )
    )
  );

  if ( $customer )
  {
    // check ACCOUNT_ALIASES
    $success = verify_account_aliases($customer->CUSTOMER_ID,$customer->current_mobile_number);

    if ( ! $success ) // ACCOUNT_ALIASES is not correctly set
    {
      $messages[] = "ACCOUNT_ALIASES was not correctly set";

      # insert into ACCOUNT_ALIASES
      $account_aliases_params = array(
        'msisdn'     => $customer->current_mobile_number,
        'account_id' => $customer->ACCOUNT_ID
      );

      if ( \Ultra\Lib\DB\AccountAliases\insertAccountAliases($account_aliases_params) == 0 )
      {
        if ( verify_account_aliases($customer->CUSTOMER_ID,$customer->current_mobile_number) )
        {
          $messages[] = "ACCOUNT_ALIASES has been repaired";
          $repaired = TRUE;
          $success  = TRUE;
        }
        else
        {
          $messages[] = "ACCOUNT_ALIASES could not be repaired";
          $success  = FALSE;
        }
      }
      else
        $errors[] = "ERR_API_INTERNAL: ACCOUNT_ALIASES DB error";
    }

    if ( $success )
    {
      // check DESTINATION_ORIGIN_MAP
      $destination_origin_map_data = verify_destination_origin_map($customer->CUSTOMER_ID,$customer->current_mobile_number);

      foreach( $destination_origin_map_data as $number => $verified )
      {
        if ( ( ! $verified ) && $success )
        {
          # insert into DESTINATION_ORIGIN_MAP

          if ( ! in_array("DESTINATION_ORIGIN_MAP was not correctly set",$messages ) )
            $messages[] = "DESTINATION_ORIGIN_MAP was not correctly set";

          $destination_origin_map_insert_query = destination_origin_map_insert_query(
            array( 'destination' => $number )
          );

          dlog('',$destination_origin_map_insert_query);

          if ( run_sql_and_check($destination_origin_map_insert_query) )
          {
            $repaired = TRUE;
            $success  = TRUE;
          }
          else
          {
            $messages[] = "DESTINATION_ORIGIN_MAP could not be repaired";
            $errors[] = "ERR_API_INTERNAL: DESTINATION_ORIGIN_MAP DB error";
            $repaired = FALSE;
            $success  = FALSE;
          }
        }
      }

      if ( in_array("DESTINATION_ORIGIN_MAP was not correctly set",$messages ) && $repaired )
        $messages[] = "DESTINATION_ORIGIN_MAP has been repaired";
    }

    // check ACCOUNTS.ENABLED
    if ( $customer->ACCOUNT_STATUS_TYPE == ACCOUNT_STATUS_TYPE_DEACTIVATED )
    {
      $success = FALSE;
      $messages[] = "Account is not ENABLED";
    }
  }
  else
    $errors[] = "ERR_API_INTERNAL: customer not found";

  return flexi_encode(fill_return($p,
                                  "customercare__CheckInternationalDirectDialSettings",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "repaired" => $repaired,
                                        "message"  => implode( '; ' , $messages ),
                                        "errors"   => $errors)));
}


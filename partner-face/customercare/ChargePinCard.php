<?php

use Ultra\Lib\Util\Redis as RedisUtils;

// Tests and charges a pin cards and puts the balance into the account; then tries to reactivate a suspended account if necessary.
// accepts both Ultra and Incomm PINs
function customercare__ChargePinCard($partner_tag, $customer_id, $msisdn, $store_id, $user_id, $destination="WALLET", $pin, $pin2, $pin3, $pin4, $pin5, $pin6)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success     = FALSE;
  $reactivated = FALSE;
  $state       = '';
  $customer    = NULL;

  teldata_change_db(); // connect to the DB

  $redis = new \Ultra\Lib\Util\Redis;

  // block if there are more than 2500 calls in an hour
  RedisUtils\redis_increment_command_count_by_hour( $redis , "customercare__ChargePinCard" );

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__ChargePinCard",
                                    func_get_args(),
                                    array("success"     => TRUE,
                                          "reactivated" => $reactivated,
                                          "warnings"    => array("ERR_API_INTERNAL: always_succeed"))));
  }

  // block if there are more than 2500 calls in an hour
  if ( RedisUtils\redis_get_command_count_by_hour( $redis , "customercare__ChargePinCard" ) > 2500 )
  {
    $errors = array( "ERR_API_INTERNAL: this command has been currently disabled. Please try again later." );
  }
  else
  {
    $errors = validate_params($p, "customercare__ChargePinCard", func_get_args(), $mock);
  }

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__ChargePinCard",
                                    func_get_args(),
                                    array("success"     => $success,
                                          "reactivated" => $reactivated,
                                          "errors"      => $errors)));
  }

  try
  {
    if ( $customer_id )
    {
      $customer = get_customer_from_customer_id($customer_id);
    }
    elseif ( $msisdn )
    {
      $customer = get_customer_from_msisdn($msisdn);
    }
    else
    { throw new Exception("ERR_API_INVALID_ARGUMENTS: customer_id or msisdn must be provided"); }

    if ( ! $customer )
    { throw new Exception("ERR_API_INVALID_ARGUMENTS: customer not found"); }

    $customer_id = $customer->CUSTOMER_ID;

    // get customer state
    $state = internal_func_get_state_from_customer_id($customer_id);

    if ( ! $state )
    { throw new Exception("ERR_API_INTERNAL: Could not load current state for customer"); }

    // state must be one of the following: Active, Provisioned, Suspended, Port-In Requested.
    if ( ( $state['state'] != 'Suspended' )
      && ( $state['state'] != 'Active' )
      && ( $state['state'] != 'Provisioned' )
      && ( $state['state'] != 'Port-In Requested' ) )
    { throw new Exception("ERR_API_INTERNAL: Invalid customer state for this command"); }

    // All pin numbers must be valid, correct and un-used for the command to succeed, otherwise return an error and do not use any pins.

    $pin_list = array($pin);

    if ( $pin2 )
    { $pin_list[] = $pin2; }

    if ( $pin3 )
    { $pin_list[] = $pin3; }

    if ( $pin4 )
    { $pin_list[] = $pin4; }

    if ( $pin5 )
    { $pin_list[] = $pin5; }

    if ( $pin6 )
    { $pin_list[] = $pin6; }

    dlog('',"PIN list = %s",$pin_list);

    $validate_pin_result = func_validate_pin_cards(
      array(
        'pin_list' => $pin_list
      )
    );

    dlog('',"validate_pin_result = %s",$validate_pin_result);

    if ( $validate_pin_result["at_least_one_customer_used"] )
    { throw new Exception("ERR_API_INTERNAL: One or more PINs are already used"); }

    if ( $validate_pin_result["at_least_one_not_found"] )
    { throw new Exception("ERR_API_INTERNAL: Could not find one or more PINs"); }

    // PIN status should be AT_MASTER
    if ( $validate_pin_result["at_least_one_at_foundry"] )
    { throw new Exception("ERR_API_INTERNAL: One or more PINs are cannot be used"); }

    // stored_value+Balance+new_load must not be greater than $200
    $total_value = $customer->BALANCE + $customer->stored_value;

    foreach($validate_pin_result["values"] as $value)
    {
      $total_value += $value;
    }

    dlog('',"total_value = $total_value");

    if ( $total_value > 200 )
    { throw new Exception("ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet. Please use up some of your current balance before adding more."); }

    $return = func_apply_pin_cards(
      array(
        "destination" => $destination,
        "pin_list"    => $pin_list,
        "customer"    => $customer,
        "source"      => 'PHONEPIN',
        "reference"   => create_guid('customercare'),
        "reason"      => __FUNCTION__,
        "entry_type"  => 'LOAD',
        'validated'   => $validate_pin_result
      )
    );

    if ( count($return['errors']) )
    { throw new Exception("ERR_API_INTERNAL: could not apply PIN cards"); }

    // try to reactivate a suspended account if necessary
    if ( $state['state'] == 'Suspended' )
    {
       $context = array(
         'customer_id' => $customer->CUSTOMER_ID
       );

      $result_status = reactivate_suspended_account($state,$customer,$context);

      if ( $result_status['success'] )
      {
        $reactivated = $result_status['reactivated'];
      }
      else
      {
        // non-fatal errors
        $warnings = $result_status['errors'];
      }

    }

    $success = TRUE;
  }
  catch(Exception $e)
  {
    $success = FALSE;
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }

  return flexi_encode(fill_return($p,
                                  "customercare__ChargePinCard",
                                  func_get_args(),
                                  array("success"     => $success,
                                        "reactivated" => $reactivated,
                                        "errors"      => $errors)));
}

?>

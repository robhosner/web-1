<?php


// Returns a list of all the shipwire order_ids associated with a customer_id
function customercare__GetCustomerShipwireOrders($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $order_ids   = '';
  $order_dates = '';

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__GetCustomerShipwireOrders",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__GetCustomerShipwireOrders", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__GetCustomerShipwireOrders",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $output_params = get_shipwire_log_data_by_customer_id( $customer_id );

  return flexi_encode(fill_return($p,
                                  "customercare__GetCustomerShipwireOrders",
                                  func_get_args(),
                                  $output_params));
}


?>

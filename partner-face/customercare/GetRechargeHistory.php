<?php

// Return to the caller customer data recharge history.
function customercare__GetRechargeHistory($customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  $recharge_history = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__GetRechargeHistory",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__GetRechargeHistory", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__GetRechargeHistory",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db();

  $data_event_log = get_data_event_log(
    array(
      'customer_id' => $customer_id,
      'days_ago'    => 60
    )
  );

  if ( $data_event_log && is_array($data_event_log) && count($data_event_log) )
  {
    foreach($data_event_log as $data_event_row)
    {
      $recharge_history[] = implode("|",
                                    array(
                                      $data_event_row->ACTION,
                                      get_date_from_full_date($data_event_row->EVENT_DATE),
                                      $data_event_row->SOC
                                    )
                            );
    }
  }

  $bucket_event_log = get_bucket_event_log(
    array(
      'customer_id' => $customer_id,
      'days_ago'    => 60
    )
  );

  if ( $bucket_event_log && is_array($bucket_event_log) && count($bucket_event_log) )
  {
    foreach($bucket_event_log as $bucket_event_row)
    {
      $recharge_history[] = implode("|",
                                    array(
                                      $bucket_event_row->ACTION,
                                      get_date_from_full_date($bucket_event_row->EVENT_DATE),
                                      $bucket_event_row->SOC
                                    )
                            );
    }
  }

  $success = TRUE;

  return flexi_encode(fill_return($p,
                                  "customercare__GetRechargeHistory",
                                  func_get_args(),
                                  array("success"          => $success,
                                        "recharge_history" => $recharge_history,
                                        "errors"           => $errors)));
}

?>

<?php

// Website or customer service triggers a shipment message to Shipwire to send off another SIM.
function customercare__ReshipSIM($partner_tag, $reason, $agent_name, $customercare_event_id, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__ReshipSIM",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__ReshipSIM", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__ReshipSIM",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  /* *** get customer data *** */

  $customer = get_account_from_customer_id( $customer_id , array('customer_id','COS_ID') );

  if ( $customer )
  {

    /* *** send off another SIM *** */
    $result = inventory_individual_ship_sim($customer_id, __FUNCTION__);
    if ( $result['success'] )
    {

      /* *** store $reason, $agent_name, $customercare_event_id in HTT_BILLING_HISTORY *** */

      $history_params = array(
        "customer_id"             => $customer_id,
        "date"                    => 'now',
        "cos_id"                  => $customer->COS_ID,
        "entry_type"              => 'SHIP',
        "stored_value_change"     => 0,
        "balance_change"          => 0,
        "package_balance_change"  => 0,
        "charge_amount"           => 0,
        "reference"               => $customercare_event_id,
        "reference_source"        => 'CS',
        "detail"                  => 'customercare__ReshipSIM',
        "description"             => 'Re-ship SIM',
        "result"                  => 'COMPLETE',
        "source"                  => 'SHIP',
        "is_commissionable"       => 0,
        "terminal_id"             => $agent_name
      );

      $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

      if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
      {
        $success = TRUE;
      }
      else
      { $errors[] = "ERR_API_INTERNAL: Database write error"; }
    }
    else
    { $errors = "ERR_API_INTERNAL: {$result['error']}"; }
  }
  else
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found"; }

  return flexi_encode(fill_return($p,
                                  "customercare__ReshipSIM",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}

?>

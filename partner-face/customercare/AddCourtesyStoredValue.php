<?php

// Gives the customer increased $amount (in cents) that can be used for monthly recharge only. This should go into stored_value rather than balance.
function customercare__AddCourtesyStoredValue($partner_tag, $reason, $agent_name, $customercare_event_id, $customer_id, $amount)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "customercare__AddCourtesyStoredValue",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "customercare__AddCourtesyStoredValue", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "customercare__AddCourtesyStoredValue",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {
    $result = func_courtesy_add_stored_value(
      array(
        'terminal_id'      => $agent_name,
        'amount'           => ($amount/100),
        'reference'        => $customercare_event_id,
        'reason'           => $reason,
        'customer'         => $customer,
        'detail'           => __FUNCTION__
      )
    );

    if ( count($result['errors']) > 0 )
    {
      $errors = $result['errors'];
    }
    else
    {
      $success = TRUE;
    }
  }
  else
  {
    $errors[] = "ERR_API_INVALID_ARGUMENTS: customer not found";
  }

  return flexi_encode(fill_return($p,
                                  "customercare__AddCourtesyStoredValue",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

?>

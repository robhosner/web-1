<?php

require_once 'classes/VersionOneResult.php';
require_once 'classes/Flex.php';

require_once 'Ultra/Activations/Repositories/Mssql/ActivationHistoryRepository.php';
require_once 'Ultra/Customers/Repositories/Mssql/CustomerRepository.php';
require_once 'Ultra/Sims/Repositories/Mssql/SimRepository.php';

use Ultra\Activations\Repositories\Mssql\ActivationHistoryRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Sims\Repositories\Mssql\SimRepository;

/**
 * externalpayments__SubmitRealtimeReload
 * Allows external payment processors to apply a balance directly to a live account.
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/ePay+Integration
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/How+it+works+-+Payments+and+Billing+Runner
 */
function externalpayments__SubmitRealtimeReload(
  $partner_tag,
  $phone_number,
  $request_epoch,
  $store_zipcode,
  $store_zipcode_extra4,
  $store_id,
  $clerk_id,
  $terminal_id,
  $product_id,
  $subproduct_id,
  $sku,
  $load_amount,
  $provider_trans_id,
  $provider_name)
{
  global $p;
  global $mock;
  global $always_succeed;

  // setup
  $apiResult = new VersionOneResult($p, __FUNCTION__);
  $apiResult->setArgs(func_get_args(), $mock);

  $request_id = create_guid('EPAY_API');

  $customer               = NULL;
  $success                = TRUE;
  $customer_balance       = '';
  $ultra_payment_trans_id = '';

  $fraud_data = [
    'load_amount'     => $load_amount,
    'provider'        => $provider_name,
    'subproduct_id'   => $subproduct_id,
    'store_id'        => $store_id,
    'clerk_id'        => $clerk_id,
    'terminal_id'     => $terminal_id
  ];

  try
  {
    if ($always_succeed)
    {
      $apiResult->succeed();
      $apiResult->addWarning('ERR_API_INTERNAL: always_succeed');
      $apiResult->clearErrors();

      return $apiResult->getJSON();
    }

    // input validation errors
    if ($apiResult->hasErrors())
      return $apiResult->getJSON();

    teldata_change_db();

    // verify provider_name
    // $provider = 'EPAY'; // VYT @ 2015-01-06: this API is used by EPAY only
    if ( ! in_array($provider_name, array('EPAY', 'IAS')))
      throw new Exception("ERR_API_INVALID_ARGUMENTS: Cannot handle payment provider $provider_name");

    // if product_id is ULTRA_PLAN_RECHARGE, validate subproduct_id for the allowed values
    $subproduct = Ultra\UltraConfig\getPaymentProviderSubproductInfo($subproduct_id);
    if ($product_id == 'ULTRA_PLAN_RECHARGE' && ! $subproduct)
      throw new Exception("ERR_EPY_INVALID_SUBPRODUCT_ID: $subproduct_id is not a valid subproduct_id");

    // get customer from phone number, verify
    $customerRepo = new CustomerRepository();
    if ( ! $customer = $customerRepo->getCustomerFromMsisdn($phone_number))
      throw new Exception("ERR_API_INVALID_ARGUMENTS: customer not found from MSISDN $phone_number");

    if (\Ultra\Lib\Flex::isFlexPlan($customer->cos_id))
      throw new Exception('This command does not support Ultra Flex subscribers. Please use the Ultra Flex products in your terminal.');

    // verify SKU/UPC and amount
    {
      $errors = array();

      // verify UPC
      if ( ! verifyProviderSku($provider_name, $sku, $customer))
        $errors[] = "ERR_API_INVALID_ARGUMENTS: Invalid UPC or SKU $sku for provider $provider_name";

      // verify UPC amount
      if ( ! $destination = verifyProviderSkuAmount($provider_name, $sku, $load_amount))
        $errors[] = "ERR_API_INVALID_ARGUMENTS: Invalid amount $load_amount for SKU $sku";

      if (count($errors))
      {
        $apiResult->addErrors($errors);
        throw new Exception(NULL);
      }
    }

    $subproduct_id = checkProductV1($customer, $product_id, $subproduct_id, $load_amount);

    // PROD-1855: validate that commission can be factored if factoring is specified
    if ($product_id == 'ULTRA_PLAN_RECHARGE' && ! $subproduct['commissionable'])
    {
      $errors = array();

      $simRepo = new SimRepository();
      $sim = $simRepo->getSimFromIccid($customer->current_iccid);

      if ( ! $sim)
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot find SIM for subscriber';
      else
      {
        if ($error = \validateFactoredCommission($sim->INVENTORY_MASTERAGENT))
          $errors[] = 'ERR_API_INVALID_ARGUMENTS: Customer SIM is not eligible for instant spiff';
      }

      if (count($errors))
      {
        $apiResult->addErrors($errors);
        throw new Exception(NULL);
      }
    }

    // make sure that $load_amount is equal to or greater than Monthly plan cost for ULTRA_PLAN_RECHARGE
    list($errors, $plan_cost, $total_cost) = validate_customer_plan($customer, $product_id, $subproduct_id, $load_amount);
    if (count($errors))
    {
      $apiResult->addErrors($errors);
      throw new Exception(NULL);
    }

    // BOLT-30: if amount equals the exact plan cost then apply it to STORED_VALUE
    if (
      $product_id == 'ULTRA_ADD_BALANCE'
      && $load_amount == $total_cost
      && $load_amount != $plan_cost)
    {
      $product_id = 'ULTRA_PLAN_RECHARGE';
    }

    $customer_balance = $customer->BALANCE + $customer->stored_value; # in $

    if ( $customer_balance + ( $load_amount / 100 ) > 200 ) # 200 $
    {
      throw new Exception(
        "ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet."
        . " Please use up some of your current balance before adding more."
      );
    }

    // insert a new row into HTT_BILLING_ACTIONS
    $check = \Ultra\Lib\Billing\addBillingActionData(
      $customer,
      $request_id,
      $provider_trans_id,
      $product_id,
      $subproduct_id,
      $load_amount,
      $sku,
      $provider_name
    );
    if ( ! $check )
    {
      $apiResult->addData([
        "phone_number"           => $phone_number,
        "customer_balance"       => '',
        "provider_trans_id"      => $provider_trans_id,
        "ultra_payment_trans_id" => $ultra_payment_trans_id,
        "load_amount"            => ''
      ]);
      throw new Exception("ERR_API_INTERNAL: Database write error");
    }

    $customer_balance       = ( $customer_balance * 100 ) + $load_amount; # in cents
    $ultra_payment_trans_id = $request_id;

    # store tracking parameters in Redis
    $redis = new \Ultra\Lib\Util\Redis;
    $redis_epay_key_prepend = "externalpayments/".$provider_trans_id.'/';
    $redis->set($redis_epay_key_prepend.'store_zipcode',       $store_zipcode,        60*30);
    $redis->set($redis_epay_key_prepend.'store_zipcode_extra4',$store_zipcode_extra4, 60*30);
    $redis->set($redis_epay_key_prepend.'store_id',            $store_id,             60*30);
    $redis->set($redis_epay_key_prepend.'clerk_id',            $clerk_id,             60*30);
    $redis->set($redis_epay_key_prepend.'terminal_id',         $terminal_id,          60*30);
    $redis->set($redis_epay_key_prepend.'source',              $provider_name,        60*30);
    $redis->set($redis_epay_key_prepend.'detail',              __FUNCTION__,          60*30);

    // should we update ULTRA.HTT_ACTIVATION_HISTORY ?
    if ( ! in_array( $customer->plan_state , array( STATE_ACTIVE , STATE_SUSPENDED , STATE_CANCELLED ) ) )
    {
      $activationHistoryRepo = new ActivationHistoryRepository();
      $activation_history = $activationHistoryRepo->getHistory($customer->CUSTOMER_ID);

      if ( $activation_history
        && ( $activation_history->FINAL_STATE != STATE_CANCELLED )
        && ( $activation_history->FINAL_STATE != FINAL_STATE_COMPLETE )
        && ( ! $activation_history->FUNDING_SOURCE )
      )
      {
        $activationHistoryRepo->logFunding($customer->CUSTOMER_ID, $provider_name, $load_amount / 100);
      }
    }

    $apiResult->addDataArray([
      "phone_number"           => $phone_number,
      "customer_balance"       => floor($customer_balance),
      "provider_trans_id"      => $provider_trans_id,
      "ultra_payment_trans_id" => $ultra_payment_trans_id,
      "load_amount"            => $load_amount
    ]);

    $apiResult->succeed();

    // successful fraud event
    fraud_event($customer, 'externalpayments', 'SubmitRealtimeReload', 'success', $fraud_data);
  }
  catch (Exception $e)
  {
    if ($error = $e->getMessage())
      $apiResult->addError($error);

    // error fraud event
    fraud_event($customer, 'externalpayments', 'SubmitRealtimeReload', 'error', $fraud_data);
  }

  return $apiResult->getJSON();
}

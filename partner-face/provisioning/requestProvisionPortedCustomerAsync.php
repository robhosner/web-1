<?php

require_once 'classes/Provisioning.php';

// This API call is used to create a customer and 'activate' their SIM. This call is ONLY for ported in customers.
function provisioning__requestProvisionPortedCustomerAsync(
  $partner_tag, $ICCID, $loadPayment, $targetPlan, 
  $numberToPort, $portAccountNumber, $portAccountPassword, $portAccountZipcode, 
  $pinsToApply,
  $creditCard, $creditExpiry, $creditCVV, $creditAutoRecharge, $creditAmount, 
  $customerZip, $customerAddressOne, $customerAddressTwo, $customerCity, $customerState,
  $customerFirstName, $customerLastName, $customerEMail, 
  $activation_masteragent, $activation_distributor, $activation_agent, $activation_store, $activation_userid, 
  $preferred_language, $dealer_code, $bolt_ons)
{
  # ( $loadPayment == "NONE" ) ~~>> [ Neutral ] => [ Port-In Requested ]
  # $ICCID is 19 digits

  global $p;
  global $mock;
  global $always_succeed;

  // version 1 result object
  $apiResult = new \VersionOneResult($p, 'provisioning__requestProvisionPortedCustomerAsync');
  $apiResult->setArgs(func_get_args(), $mock);

  $success    = FALSE;
  $request_id = '';
  $customer   = FALSE;
  $cc_errors  = array();
  $context = array(); // port transition context

  // zip code adjustments
  if ( ( ! isset($portAccountZipcode) ) || ( ! $portAccountZipcode ) )
    $portAccountZipcode = $customerZip;

  # English is the default preferred language
  if ( is_null($preferred_language) || ! $preferred_language )
    $preferred_language = 'EN';

  $bolt_ons = $bolt_ons ? (is_array($bolt_ons) ? $bolt_ons : array($bolt_ons)) : array();

  # trim spaces
  $portAccountPassword = trim($portAccountPassword);
  $portAccountNumber   = trim($portAccountNumber);

  $customer = null;

  try
  {
      // confirm activations
    if (! activations_enabled())
      throw new Exception('ERR_API_INTERNAL: account activations are temporarily disabled, please try later');

    teldata_change_db();

    if ($always_succeed)
    {
      $apiResult->succeed();
      $apiResult->addWarning('ERR_API_INTERNAL: always_succeed');
      $apiResult->clearErrors();

      return $apiResult->getJSON();
    }

    // input validation errors
    if ($apiResult->hasErrors())
      return $apiResult->getJSON();

    // PROD-975: dealer code overrides all other parameters
    if ($dealer_code)
    {
      if ($dealer = \Ultra\Lib\DB\Celluphone\getDealerInfo($dealer_code))
      {
        $activation_masteragent = $dealer->masterId;
        $activation_distributor = $dealer->distributorId;
        $activation_store       = $dealer->dealerId;
      }
      else
        throw new Exception('ERR_API_INVALID_ARGUMENTS: unknown dealer code');
    }

      // MVNO-2695: default dealer is 23
    if (empty($activation_store))
      $activation_store = DEFAULT_PROVISIONING_DEALER_ID;

    $customer_source = get_customer_source($activation_masteragent, $activation_store, $activation_userid);

    // configure Recurring Bolt Ons
    $configureBoltonsResult = \Provisioning::configureBoltOns($bolt_ons);
    if ($error = $configureBoltonsResult->get_first_error())
      throw new Exception($error);

    // translate dealer code into dealer ID and over-write activation_store
    if ( ! empty($dealer_code) && ! $activation_store = \Ultra\Lib\DB\Getter\getScalar('DealerCD', $dealer_code, 'DealerSiteID'))
      throw new Exception('ERR_API_INVALID_ARGUMENTS: unknown dealer code');

    // validate ICCID
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID', $ICCID, 'VALID');
    if ( ! empty( $error ) )
      throw new Exception($error);

    // verify that $zipcode is allowed (in coverage)
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode',$customerZip,'in_coverage');
    if ( ! empty( $error ) )
      throw new Exception($error);

    // validate credit card details if loadPayment="CCARD"
    if ($loadPayment == "CCARD")
    {
      if ( ! isset( $creditCard )         || $creditCard == '' )
        $apiResult->addError('ERR_API_INVALID_ARGUMENTS: Missing creditCard, required for Credit Card Payments');

      if ( ! isset( $creditExpiry )       || $creditExpiry == '' )
        $apiResult->addError('ERR_API_INVALID_ARGUMENTS: Missing creditExpiry, required for Credit Card Payments');

      if ( ! isset( $creditCVV )          || $creditCVV == '' )
        $apiResult->addError('ERR_API_INVALID_ARGUMENTS: Missing creditCVV, required for Credit Card Payments');

      if ( ! isset( $creditAutoRecharge ) || $creditAutoRecharge === '' )
        $apiResult->addError('ERR_API_INVALID_ARGUMENTS: Missing creditAutoRecharge, required for Credit Card Payments');

      if ( ! isset( $customerAddressOne ) || $customerAddressOne == '' )
        $apiResult->addError('ERR_API_INVALID_ARGUMENTS: Missing customerAddressOne, required for Credit Card Payments');

      if ( ! isset( $customerCity )       || $customerCity == '' )
        $apiResult->addError('ERR_API_INVALID_ARGUMENTS: Missing customerCity, required for Credit Card Payments');

      if ( ! isset( $customerState )      || $customerState == '' )
        $apiResult->addError('ERR_API_INVALID_ARGUMENTS: Missing customerState, required for Credit Card Payments');

      if ( ! isset( $creditAmount )       || $creditAmount == '' )
        $apiResult->addError('ERR_API_INVALID_ARGUMENTS: Missing creditAmount, required for Credit Card Payments');
      else
      {
        if ( $creditAmountErrors = validate_plan_amount($creditAmount, $targetPlan) )
          $apiResult->addError($creditAmountErrors);
      }

      // output all collected errors
      if ($apiResult->hasErrors())
        throw new Exception('ERR_API_INVALID_ARGUMENTS: Credit Card validation error');
    }

    // get SIM data
    $sim = get_htt_inventory_sim_from_iccid($ICCID);
    if ( ! $sim)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: invalid ICCID');

    // check if brand id for ICCID is allowed
    if ( ! \Ultra\UltraConfig\isBrandIdAllowed($sim->BRAND_ID))
      throw new Exception('ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid');

    // validate ICCID
    if ( ! $valid = validate_ICCID($sim, 1) )
      throw new Exception('ERR_API_INVALID_ARGUMENTS: the given ICCID is invalid or already used');

    $redis = new \Ultra\Lib\Util\Redis;

    // validate ICCID good to activate
    $result = \Provisioning::verifyICCIDGoodToActivate($sim->ICCID_FULL, $redis);
    if ($error = $result->get_first_error())
      throw new Exception($error);

    // validate pinsToApply
    $validatePinsResult = null;
    if ($loadPayment == 'PINS')
    {
      $validatePinsResult = \Provisioning::validatePinsToApply($pinsToApply);
      if ($error = $validatePinsResult->get_first_error())
        throw new Exception($error);
    }

    // transition name is different for intra and normal ports
    $transition_name = '';

    // check for intra-brand port
    $customer = get_customer_from_msisdn($numberToPort, 'u.CUSTOMER_ID, u.BRAND_ID, plan_state, u.CURRENT_ICCID');

    // intra-brand port
    if ($customer && $customer->BRAND_ID != get_brand_id_from_iccid($ICCID))
    {
      // deny port if plan_state not in
      if ( ! in_array($customer->plan_state, array(STATE_ACTIVE, STATE_SUSPENDED)))
        throw new Exception('ERR_API_INTERNAL: port-In denied due to customer state');

      $context['old_iccid'] = luhnenize($customer->CURRENT_ICCID);
      $context['old_brand_id'] = $customer->BRAND_ID;

      // soft cancel and clone customer
      $result = \Provisioning::intraBrandPortOperations($customer->CUSTOMER_ID, $sim);
      if ($error = $result->get_first_error())
        throw new Exception($error);

      // new customer has been cloned
      $customer = $result->get_data_key('customer');

      // transition name for intra ports
      $transition_name = 'Provision Intra Port ' . get_plan_name_from_short_name($targetPlan);
    }
    // is normal port
    else
    {
      // verify over port eligibility over middleware
      $result = \Provisioning::verifyPortInEligibility($numberToPort);
      if ($error = $result->get_first_error())
        throw new Exception($error);

      // verifiy numberToPort not in system
      $result = \Provisioning::verifyNumberNotInSystem($numberToPort);
      if ($error = $result->get_first_error())
        throw new Exception($error);

      $ICCID_18 = substr($ICCID, 0, -1);

      if ( ! $customerFirstName ) $customerFirstName = 'FirstName';
      if ( ! $customerLastName  ) $customerLastName  = 'LastName';
      if ( ! $customerEMail     ) $customerEMail     = 'portal-activated@null';

      // create ULTRA user in DB

      $plan_cost       = get_plan_cost_by_cos_id(get_cos_id_from_plan($targetPlan));
      $promised_amount = $plan_cost + $result->get_data_key('bolt_ons_cost');

      $params = array(
        'masteragent'           => $sim->INVENTORY_MASTERAGENT, // MVNO-2279: overwrite MASTER_AGENT, DISTRIBUTOR
        'distributor'           => $sim->INVENTORY_DISTRIBUTOR,
        'userid'                => $activation_userid,
        'preferred_language'    => $preferred_language,
        'cos_id'                => get_cos_id_from_plan('STANDBY'),
        'first_name'            => $customerFirstName,
        'last_name'             => $customerLastName,
        'postal_code'           => $customerZip,
        'country'               => 'USA',
        'e_mail'                => $customerEMail,
        'plan_state'            => 'Neutral',
        'plan_started'          => 'NULL',
        'plan_expires'          => 'NULL',
        'customer_source'       => $customer_source,
        'current_iccid'         => $ICCID_18, # Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
        'current_iccid_full'    => $ICCID,    # Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
        'current_mobile_number' => $numberToPort,
        'promised_amount'       => $promised_amount
      );

      if ( isset( $activation_store ) && $activation_store )
        $params['dealer'] = $activation_store;

      \logit("Creating user with current_mobile_number = $numberToPort");

      $result   = create_ultra_customer_db_transaction($params);
      $customer = $result['customer'];

      if ( ! $customer)
        throw new Exception('ERR_API_INTERNAL: error creating customer');
      // end create new customer

      // transition name for normal port
      $transition_name = 'Request Port ' . get_plan_name_from_short_name($targetPlan);
    }
    // end normal port

    // if CC is chosen, stores the CC
    if ( $loadPayment == "CCARD" )
    {
      $result = func_update_stored_credit_card(
        array(
          'customer'                => $customer,
          'account_country'         => 'US',
          'account_cc_number'       => $creditCard,
          'account_cc_exp'          => $creditExpiry,
          'account_cc_cvv'          => $creditCVV,
          'account_postal_code'     => $customerZip,
          'account_address1'        => $customerAddressOne,
          'account_address2'        => $customerAddressTwo,
          'account_city'            => $customerCity,
          'account_state_or_region' => $customerState,
          'account_first_name'      => $customerFirstName,
          'account_last_name'       => $customputtrLastName,
          // 'account_number_email'    => $customerEMail // validation always fails ( I use customers_update_query instead )
        )
      );

      if (count($result['errors']))
      {
        $apiResult->addErrors($result['errors']);
        throw new Exception('ERR_API_INTERNAL: error updating stored credit card');
      }

      // sets easypay_activated = 1
      $update_params = array(
        'easypay_activated'  => 1,
        'customer_id'        => $customer->CUSTOMER_ID,
        'monthly_cc_renewal' => ( isset($creditAutoRecharge) && $creditAutoRecharge ) ? 1 : 0
      );

      $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query( $update_params );

      if ( ! is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_update_query)) )
        throw new Exception('ERR_API_INTERNAL: DB error (2)');

      // verify cc number propagation
      $result_status = array('errors' => array(), 'success' => 0);
      list($result_status, $customer) = verify_cc_number_propagation($result_status, $customer);
      if ( ! $result_status['success'])
        throw new Exception('ERR_API_INTERNAL: failure verifying cc number propagation');
    }

    // update customer email address if set
    if ( $customerEMail )
    {
      $customers_update_query = customers_update_query(
        array(
          'customer_id' => $customer->CUSTOMER_ID,
          'e_mail'      => $customerEMail
        )
      );

      if ( ! is_mssql_successful(logged_mssql_query($customers_update_query)) )
        throw new Exception('ERR_API_INTERNAL: error updating customer email');
    }

    // pinsToApply was validated earlier
    if ($validatePinsResult)
    {
      $result = func_apply_pin_cards(
        array(
          'customer'      => $customer,
          'reason'        => __FUNCTION__,
          'reference'     => create_guid('provisioning'),
          'source'        => ( $loadPayment == "CCARD" ) ? 'WEBCC' : 'WEBPIN' ,
          'pin_list'      => explode(",", $pinsToApply),
          'target_cos_id' => get_cos_id_from_plan($targetPlan),
          'validated'     => $validation
        )
      );

      if (count($result['errors']))
      {
        $apiResult->addErrors($result['errors']);
        throw new Exception('ERR_API_INTERNAL: failure applying pin cards');
      }

      log_funding_in_activation_history( $customer->CUSTOMER_ID , 'PIN' , $result['total_amount'] );
    }

    // if CC is chosen, charges the plan balance onto the CC. (the transaction would have to be committed here.)
    if ( $loadPayment == "CCARD" )
    {
      $add_balance_params = array(
        'customer'             => $customer,
        'amount'               => ( substr($targetPlan, -2) ), # $ L[12345]9 => [12345]9.00 $
        'reason'               => 'provisioning__requestProvisionPortedCustomerAsync',
        'session'              => create_guid('PHPAPI'),
        'upgrade_seconds_gain' => 0,
        'target_cos_id'        => get_cos_id_from_plan($targetPlan),
        'detail'               => __FUNCTION__
      );

      $result = func_add_balance_by_credit_card( $add_balance_params );

      if (count($result['errors']))
      {
        $apiResult->addData('cc_errors', $result['rejection_errors']);
        $apiResult->addErrors($result['errors']);

        throw new Exception('ERR_API_INTERNAL: failure adding balance by credit card');
      }
      else
        log_funding_in_activation_history( $customer->CUSTOMER_ID , 'CCARD' , substr($targetPlan, - 2) );
    }

    // logs the port request (if carrier details fails, just places null)
    $result_status = log_port_request_from_number(
      $numberToPort,
      $customer,
      $activation_masteragent,
      $activation_distributor,
      $activation_agent,
      $activation_store,
      $activation_userid
    );

    // parameters for change_state - Transition [ Neutral ] => [ Port-In Requested ]

    $context['customer_id']              = $customer->CUSTOMER_ID;
    $context['port_in_msisdn']           = $numberToPort;
    $context['port_in_iccid']            = $ICCID;
    $context['port_in_account_number']   = $portAccountNumber;
    $context['port_in_account_password'] = $portAccountPassword;
    $context['port_in_zipcode']          = $portAccountZipcode;

    // modify configuration in ULTRA.CUSTOMER_OPTIONS and log into ULTRA.BOLTON_TRACKER
    $source = ($dealer_code == 'UMIVR') ? 'IVR' : 'CRM';

    // we are ignoring error from result_bolt_ons
    $result_bolt_ons = \update_bolt_ons_values_to_customer_options(
      $customer->CUSTOMER_ID,
      array(),
      $configureBoltonsResult->get_data_key('bolt_on_configuration'),
      $source
    );

    $resolve_now      = FALSE;
    $max_path_depth   = 1;

    $dry_run     = true;
    $resolve_now = false;

    $changeStateResult = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, 1);

    if ( ! $changeStateResult['success'])
      throw new Exception('ERR_API_INTERNAL: state transition error (1)');

    // save activation attribution info in redis before attempting state transition
    set_redis_provisioning_values(
      $customer->CUSTOMER_ID,
      $activation_masteragent,
      $activation_agent,
      $activation_distributor,
      $activation_store,
      $activation_userid,
      $redis
    );

    $dry_run     = false;
    $resolve_now = true;

    $changeStateResult = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, 1);
      
    if ( ! $changeStateResult['success'])
    {
      clear_redis_provisioning_values($customer->CUSTOMER_ID);
      throw new Exception('ERR_API_INTERNAL: state transition error (2)');
    }

    reserve_iccid($ICCID);

    $array_values = array_values($changeStateResult['transitions']);
    $request_id   = end($array_values);
    $apiResult->addData('request_id', $request_id);

    // memorize $request_id in Redis by customer_id for portal__SearchActivationHistoryDealer
    $redis_port = new \Ultra\Lib\Util\Redis\Port();
    $redis_port->setRequestId($customer->CUSTOMER_ID, $request_id);

    $apiResult->succeed();
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    if ( count($pipeError = explode('|', $error)) )
      $error = $pipeError[0];

    $apiResult->addError($error);
  }

  if ( ! $apiResult->isSuccess())
    $apiResult->addData('request_id', '');

  $fraud_data = array(
    'amount'                 => 0,
    'activation_masteragent' => $activation_masteragent,
    'activation_distributor' => $activation_distributor,
    'activation_agent'       => $activation_agent,
    'activation_store'       => $activation_store,
    'activation_userid'      => $activation_userid,
    'cos_id'                 => get_cos_id_from_plan($targetPlan),
    'transition_uuid'        => create_guid('PROVISIONING_API')
  );

  # audit logs the request
  $fraud_status = ($apiResult->isSuccess()) ? 'success' : 'error' ;
  fraud_event($customer, 'provisioning', 'requestProvisionPortedCustomerAsync', $fraud_status, $fraud_data);

  return $apiResult->getJSON();
}


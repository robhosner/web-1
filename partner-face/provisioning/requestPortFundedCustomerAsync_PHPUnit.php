<?php

require_once 'db.php';
require_once 'classes/PHPUnitBase.php';
require_once 'test/api/test_api_include.php';

class requestPortFundedCustomerAsyncTest extends PHPUnitBase
{
  public function setUp()
  {
    $partner = 'provisioning';
    $api = 'requestPortFundedCustomerAsync';

    // API setup
    $this->setOptions(array(
      'api'       => "{$partner}__{$api}",
      'bath'      => 'rest',
      'version'   => 1,
      'partner'   => 'rainmaker',
    ));

    parent::setUp();
  }
  public function test__main()
  {
    $params = array();
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    // required
    $params['ICCID']        = '8901260842107316904';
    $params['numberToPort'] = '6462887041';
    $params['portAccountNumber']   = '12345';
    $params['portAccountPassword'] = '54321';
    $params['portAccountZipcode']  = 34743;
    $params['zession'] = 'xxx';

    // iccid is currently being used
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    //
    $params['zsession'] = '7545A177-A5C9-75F7-3ECB-DB0BB3734D49';

    // iccid is currently being used
    $result = $this->callApi($params);
    $this->assertFalse($result->success);

    // OK at time of test
    $params['ICCID'] = '1010101010101010606';

    // not eligible for port in
    $params['numberToPort'] = '5555555555';
    $result = $this->callApi($params);
    $this->assertFalse($result->success);
  }
}

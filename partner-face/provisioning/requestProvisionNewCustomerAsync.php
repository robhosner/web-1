<?php

include_once 'lib/util-common.php';

# Important: for Async APIS, request_id is HTT_TRANSITION_LOG.TRANSITION_UUID

// This API call is used to create a customer and 'activate' their SIM.
function provisioning__requestProvisionNewCustomerAsync($partner_tag, $ICCID, $loadPayment, $targetPlan, $pinsToApply, $creditCard, $creditExpiry, $creditCVV, $creditAutoRecharge, $creditAmount, $customerZip, $customerAddressOne, $customerAddressTwo, $customerCity, $customerState, $customerFirstName, $customerLastName, $customerEMail, $activation_masteragent, $activation_distributor, $activation_agent, $activation_store, $activation_userid, $preferred_language, $dealer_code, $bolt_ons)
{
  global $p;
  global $mock;
  global $always_succeed;

  teldata_change_db(); // connect to the DB

  # ( $loadPayment == "NONE" ) ~~>> [ Neutral ] => [ Provisioned ]
  # ( $loadPayment != "NONE" ) ~~>> [ Neutral ] => [ Active ]

  // PROD-975: dealer code overrides all other parameters
  if ($dealer_code)
  {
    if ($dealer = \Ultra\Lib\DB\Celluphone\getDealerInfo($dealer_code))
    {
      $activation_masteragent = $dealer->masterId;
      $activation_distributor = $dealer->distributorId;
      $activation_store = $dealer->dealerId;
    }
    else
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: unknown dealer code';
  }

  // MVNO-2695: default dealer is 23
  if (empty($activation_store))
    $activation_store = DEFAULT_PROVISIONING_DEALER_ID;

  $fraud_data = array(
    'amount'                 => 0,
    'activation_masteragent' => $activation_masteragent,
    'activation_distributor' => $activation_distributor,
    'activation_agent'       => $activation_agent,
    'activation_store'       => $activation_store,
    'activation_userid'      => $activation_userid,
    'cos_id'                 => get_cos_id_from_plan($targetPlan),
    'transition_uuid'        => create_guid('PROVISIONING_API')
  );

  $success         = FALSE;
  $transition_uuid = '';
  $customer        = FALSE;
  $cc_errors       = array();
  $customer_source = get_customer_source($activation_masteragent, $activation_store, $activation_userid);

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "provisioning__requestProvisionNewCustomerAsync",
                                    func_get_args(),
                                    array("success"    => TRUE,
                                          "request_id" => $transition_uuid,
                                          "warnings"   => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "provisioning__requestProvisionNewCustomerAsync", func_get_args(), $mock);

  // BOLT-31: get Bolt Ons info
  $bolt_ons = $bolt_ons ? (is_array($bolt_ons) ? $bolt_ons : array($bolt_ons)) : array();

  $new_bolt_on_configuration = array();
  $bolt_ons_cost = 0;
  foreach( $bolt_ons as $bolt_on_id )
    if ($bolt_on_id = trim($bolt_on_id))
    {
      if ( ! $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo( $bolt_on_id ))
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid';
      $new_bolt_on_configuration[ $boltOnInfo['option_attribute'] ] = $boltOnInfo['cost'];
      $bolt_ons_cost += $boltOnInfo['cost'];
    }
  dlog('', 'new_bolt_on_configuration = %s', $new_bolt_on_configuration);

  if ( empty($errors) )
  {
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('customerZip',$customerZip,'in_coverage');

    if ( $error )
      $errors[] = $error;
  }

  if ( empty($errors) )
  {
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID',$ICCID,'VALID');

    if ( $error )
      $errors[] = $error;
  }

  // translate dealer code into dealer ID and over-write activation_store
  if ( ! empty($dealer_code) && ! $activation_store = \Ultra\Lib\DB\Getter\getScalar('DealerCD', $dealer_code, 'DealerSiteID'))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: unknown dealer code';

  if (
    ( ! ($errors && count($errors) > 0) )
    &&
    ( $loadPayment == "CCARD" )
  )
  {

    ##==-- validate credit card details if loadPayment="CCARD" --==##

    if ( ! isset( $creditCard )         || $creditCard == '' )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: Missing creditCard, required for Credit Card Payments"; }

    if ( ! isset( $creditExpiry )       || $creditExpiry == '' )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: Missing creditExpiry, required for Credit Card Payments"; }

    if ( ! isset( $creditCVV )          || $creditCVV == '' )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: Missing creditCVV, required for Credit Card Payments"; }

    if ( ! isset( $creditAutoRecharge ) || $creditAutoRecharge === '' )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: Missing creditAutoRecharge, required for Credit Card Payments"; }

    if ( ! isset( $customerAddressOne ) || $customerAddressOne == '' )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: Missing customerAddressOne, required for Credit Card Payments"; }

    if ( ! isset( $customerCity )       || $customerCity == '' )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: Missing customerCity, required for Credit Card Payments"; }

    if ( ! isset( $customerState )      || $customerState == '' )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: Missing customerState, required for Credit Card Payments"; }

    if ( ! isset( $creditAmount )       || $creditAmount == '' )
    { $errors[] = "ERR_API_INVALID_ARGUMENTS: Missing creditAmount, required for Credit Card Payments"; }
    else
    {
      $creditAmountErrors = validate_plan_amount($creditAmount,$targetPlan);

      if ( $creditAmountErrors )
      { $errors[] = $creditAmountErrors; }
    }
  }

  // get SIM data
  if ( ! ($errors && count($errors) > 0) )
  {
    $sim = get_htt_inventory_sim_from_iccid($ICCID);
    if (! $sim)
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: invalid ICCID';
    else
    {
      // determine MVNE 1 or 2
      $mvne = $sim->MVNE;
      if ( $mvne != '2' )
        $errors[] = "ERR_API_INVALID_ARGUMENTS: the given ICCID cannot be activated";

      if ( ! \Ultra\UltraConfig\isBrandIdAllowed($sim->BRAND_ID))
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid';
    }
  }

  // validate ICCID
  if ( ! ($errors && count($errors) > 0) )
  {
    $valid = validate_ICCID($sim, 1);

    if ( ! $valid ) { $errors[] = "ERR_API_INVALID_ARGUMENTS: the given ICCID is invalid or already used"; }
  }

  if ( ! ($errors && count($errors) > 0) )
  {
    ##==-- validate ICCID against htt_customers_overlay_ultra --==##

    $overlay_data = get_ultra_customers_from_iccid( $ICCID , array('customer_id') );

    if ( $overlay_data && is_array($overlay_data) && count($overlay_data) )
      $errors[] = "ERR_API_INVALID_ARGUMENTS: the given ICCID is already used";
  }

  ##==-- validate PINS / pinsToApply --==##

  if (
    ( ! ($errors && count($errors) > 0) )
    &&
    ( $loadPayment == "PINS" )
    &&
    (
      ( ! ( isset($pinsToApply) ) )
      ||
      ( $pinsToApply == '' )
    )
  )
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: Missing pinsToApply, required for PINS Payments"; }

  // confirm activations
  if (! activations_enabled())
    $errors[] = 'ERR_API_INTERNAL: account activations are temporarily disabled, please try later';

  if ( ! ($errors && count($errors) > 0) )
  {
    ##==-- validate PinCards if present --==##

    if ( isset($pinsToApply) && $pinsToApply != '' )
    {
      list($errors, $validation) = validate_pins_to_apply($pinsToApply);
    }
  }

  if ( ! ($errors && count($errors) > 0) )
  {

    ##==-- check ICCID with Activate/Can --==##

    $redis = new \Ultra\Lib\Util\Redis;

    if ( $redis->get( 'iccid/good_to_activate/' . luhnenize( $ICCID ) ) )
    {
      $redis->del( 'iccid/good_to_activate/' . luhnenize( $ICCID ) );
    }
    else
    {
        // Activate/Can

        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

        $result = $mwControl->mwCanActivate(
          array(
            'iccid'      => $ICCID,
            'actionUUID' => getNewActionUUID('provisioning ' . time())
          )
        );

        if ( $result->is_failure() )
          $errors[] = "ERR_API_INVALID_ARGUMENTS: the given ICCID cannot be activated";
    }
  }

  if ($errors && count($errors) > 0)
  {
    fraud_event('NULL', 'provisioning', 'requestProvisionNewCustomerAsync', 'error', $fraud_data);

    return flexi_encode(fill_return($p,
                                  "provisioning__requestProvisionNewCustomerAsync",
                                    func_get_args(),
                                    array("success"    => $success,
                                          "request_id" => $transition_uuid,
                                          "errors"     => $errors)));
  }

  ##==-- create ULTRA user in DB --==##

  if ( ( ! $customerFirstName ) && ( ! $customerLastName) && ( $activation_masteragent == 54 ) && ( $activation_store == 28 ) && ( $activation_userid == 67 ) )
  {
    # default name for IVR

    $customerFirstName = 'IVR';
    $customerLastName  = 'Activated';
  }
  elseif ( ( ! $customerFirstName ) && ( ! $customerLastName) )
  {
    $customerFirstName = 'FirstName';
    $customerLastName  = 'LastName';
  }

  if ( ! $customerEMail )
  {
    $customerEMail = 'portal-activated@null';
  }

  $ICCID_18 = substr($ICCID,0,-1);

  # English is the default preferred language
  if ( is_null($preferred_language) || ! $preferred_language )
    $preferred_language = 'EN';

  $params = array(
    'masteragent'           => $sim->INVENTORY_MASTERAGENT, // MVNO-2279: overwrite MASTER_AGENT, DISTRIBUTOR
    'distributor'           => $sim->INVENTORY_DISTRIBUTOR,
    "userid"                => $activation_userid,
    "preferred_language"    => $preferred_language,
    'cos_id'                => get_cos_id_from_plan('STANDBY'),
    'first_name'            => $customerFirstName,
    'last_name'             => $customerLastName,
    'postal_code'           => $customerZip,
    'country'               => 'USA',
    'e_mail'                => $customerEMail,
    "plan_state"            => 'Neutral',
    "plan_started"          => 'NULL',
    "plan_expires"          => 'NULL',
    "customer_source"       => $customer_source,
    "current_iccid"         => $ICCID_18, # Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
    "current_iccid_full"    => $ICCID,    # Important: this is not (yet) in ACTIVATION_ICCID (MVNO-517),
    'promised_amount'       => get_plan_cost_by_cos_id(get_cos_id_from_plan($targetPlan)) + $bolt_ons_cost);

  if ( isset( $activation_store ) && $activation_store )
    $params['dealer'] = $activation_store;

  $result = create_ultra_customer_db_transaction($params);

  $customer = $result['customer'];

  if ( $customer )
  {

    $context    = array('customer_id' => $customer->CUSTOMER_ID );
    $action_transaction = ''; # to be ignored for now
    $result_status = array( 'success' => 1 , 'errors' => array() );

    if ( $loadPayment == "CCARD" )
    {

      ##==-- if CC is chosen, stores the CC --==##

      $result = func_update_stored_credit_card(
        array(
          'customer'                 => $customer,
          'account_country'          => 'US',
          'account_cc_number'        => $creditCard,
          'account_cc_exp'           => $creditExpiry,
          'account_cc_cvv'           => $creditCVV,
          'account_postal_code'      => $customerZip,
          'account_address1'         => $customerAddressOne,
          'account_address2'         => $customerAddressTwo,
          'account_city'             => $customerCity,
          'account_state_or_region'  => $customerState,
          'account_first_name'       => $customerFirstName,
          'account_last_name'        => $customerLastName,
          #'account_number_email'     => $customerEMail # validation always fails ( I use customers_update_query instead )
        )
      );

      if ( count($result['errors']) > 0 )
      {
        $result_status['errors']  = $result['errors'];
        $result_status['success'] = 0;
      }
      else
      {
        if ( isset($customerEMail) && $customerEMail )
        {
          $customers_update_query = customers_update_query(
            array(
              'customer_id' => $customer->CUSTOMER_ID,
              'e_mail'      => $customerEMail
            )
          );

          if ( ! is_mssql_successful(logged_mssql_query($customers_update_query)) )
          {
            $result_status['errors'][] = "ERR_API_INTERNAL: DB error";
            $result_status['success']  = 0;
          }
        }

        list($result_status,$customer) = verify_cc_number_propagation($result_status,$customer);
      }
    }

    if ( $result_status['success'] == 1 )
    {

      if ( isset($pinsToApply) && $pinsToApply != '' )
      {

        ##==-- if PinCards are chosen apply them. --==##

        $result = func_apply_pin_cards(
          array(
            'customer'  => $customer,
            'reason'    => __FUNCTION__,
            'reference' => create_guid('provisioning'),
            'source'    => ( $loadPayment == "CCARD" ) ? 'WEBCC' : 'WEBPIN' ,
            "pin_list"  => explode(",",$pinsToApply),
            'target_cos_id' => get_cos_id_from_plan($targetPlan),
            'validated' => $validation
          )
        );

        if ( count($result['errors']) > 0 )
        {
          $result_status['success'] = 0;
          $result_status['errors']  = $result['errors'];
        }
        else
        {
          log_funding_in_activation_history( $customer->CUSTOMER_ID , 'PIN' , $result['total_amount'] );
        }
      }
    }

    if ( $result_status['success'] == 1 )
    {
      if ( $loadPayment == "CCARD" )
      {

        ##==-- if CC is chosen, sets easypay_activated=1 --==##

        $update_params = array(
          'easypay_activated'  => 1,
          'customer_id'        => $customer->CUSTOMER_ID,
          'monthly_cc_renewal' => ( isset($creditAutoRecharge) && $creditAutoRecharge ) ? 1 : 0
        );

        $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query( $update_params );

        if ( ! is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_update_query)) )
        {
          $result_status['success']  = 0;
          $result_status['errors'][] = "ERR_API_INTERNAL: DB error";
        }
      }
    }

    if ( $result_status['success'] == 1 )
    {
      if ( $loadPayment == "CCARD" )
      {

        ##==-- if CC is chosen, charges the plan balance onto the CC. (the transaction would have to be committed here.) --==##

        $add_balance_params = array(
          'customer'             => $customer,
          'amount'               => ( $creditAmount / 100 ), # in $
          'reason'               => 'provisioning__requestProvisionNewCustomerAsync',
          'session'              => create_guid('PHPAPI'),
          'upgrade_seconds_gain' => 0,
          'target_cos_id'        => get_cos_id_from_plan($targetPlan),
          'detail'               => __FUNCTION__
        );

        $result = func_add_balance_by_credit_card( $add_balance_params );

        if ( count($result['errors']) > 0 )
        {
          $result_status['success'] = 0;
          $result_status['errors']  = $result['errors'];

          $cc_errors = $result['rejection_errors'];
        }
        else
        {
          log_funding_in_activation_history( $customer->CUSTOMER_ID , 'CCARD' , ( $creditAmount / 100 ) );
        }
      }
    }

    // modify configuration in ULTRA.CUSTOMER_OPTIONS and log into ULTRA.BOLTON_TRACKER
    $source = $dealer_code == 'UMIVR' ? 'IVR' : 'CRM';
    $result_bolt_ons = \update_bolt_ons_values_to_customer_options($customer->CUSTOMER_ID, array(), $new_bolt_on_configuration, $source);

    # parameters for change_state #

    $resolve_now      = FALSE;
    $max_path_depth   = 1;
    $dry_run          = TRUE;

    $plan = get_plan_name_from_short_name($targetPlan);
    $transition_name  = ( $loadPayment == "NONE" ) ? 'Provision '.$plan : 'Activate '.$plan ;

    if ( $result_status['success'] == 1 )
    {

      ##==-- Tests State Transition (the correct plan). --==##

      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

      if ( $result_status['success'] != 1 )
      {
        $result_status['errors'][] = "ERR_API_INTERNAL: state transition error (1)";
      }
    }

    if ( $result_status['success'] == 1 )
    {
      ##==-- save activation attribution info in redis before attempting state transition --==##

      set_redis_provisioning_values($customer->CUSTOMER_ID, $activation_masteragent, $activation_agent, $activation_distributor, $activation_store, $activation_userid, $redis);

      ##==-- If State Transition is allowed (pre-requisites are met), executes --==##

      $dry_run        = FALSE;

      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

      if ( $result_status['success'] == 1 )
      {
        reserve_iccid($ICCID);

        # add transition Id to context

        $values = array_values($result_status['transitions']);
        $context['transition_id'] = end($values);
      }
      else
      {
        $result_status['errors'] = "ERR_API_INTERNAL: state transition error (2)";
      }
    }

    if ( $result_status['success'] == 1 )
    {
      $transition_uuid = $result_status['transitions'][0];
      $fraud_data['transition_uuid'] = $transition_uuid;
      $success = TRUE;
    }
    else
    {
      // clear activation attribution info from redis
      clear_redis_provisioning_values($customer->CUSTOMER_ID);

      $errors = $result_status['errors'];
    }

  }
  else
  { $errors[] = "ERR_API_INTERNAL: DB error (1)"; }

  # audit logs the request
  $fraud_status = ($success) ? 'success' : 'error' ;
  fraud_event($customer, 'provisioning', 'requestProvisionNewCustomerAsync', $fraud_status, $fraud_data);

  return flexi_encode(fill_return($p,
                                  "provisioning__requestProvisionNewCustomerAsync",
                                  func_get_args(),
                                  array("success"    => $success,
                                        "cc_errors"  => $cc_errors,
                                        "request_id" => $transition_uuid,
                                        "errors"     => $errors)));
}


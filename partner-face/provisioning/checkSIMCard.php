<?php

/**
 * Check if the ICCID is unused for activation.
 */
function provisioning__checkSIMCard($partner_tag, $ICCID, $mode)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  $iccid_user_status = '';
  $iccid_user_status_extended = '';
  $sim_ready_activate = '';
  $expires_date = '';
  $master_agent = '';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "provisioning__checkSIMCard",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "valid"    => $iccid_user_status,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "provisioning__checkSIMCard", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "provisioning__checkSIMCard",
                                    func_get_args(),
                                    array("success" => $success,
                                          "valid"   => $iccid_user_status,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  // check for 19 digits ICCID
  $htt_inventory_sim_select_query = htt_inventory_sim_select_query( array( "iccid_full" => $ICCID ) );

  $htt_inventory_sim_data = mssql_fetch_all_objects(logged_mssql_query($htt_inventory_sim_select_query));

  if ( $htt_inventory_sim_data && is_array($htt_inventory_sim_data) && count($htt_inventory_sim_data) )
  {
    $brand_allowed = \Ultra\UltraConfig\isBrandIdAllowed($htt_inventory_sim_data[0]->BRAND_ID);

    $simbatch = htt_inventory_simbatches_get_by_batch_id($htt_inventory_sim_data[0]->ICCID_BATCH_ID, array('EXPIRES_DATE'));

    $master_agent = $htt_inventory_sim_data[0]->INVENTORY_MASTERAGENT;

    $iccid_user_status          = get_ICCID_user_status($ICCID);
    $iccid_user_status_extended = get_ICCID_user_status($ICCID, 'allow reserved');

    if ( $brand_allowed && $iccid_user_status === 'VALID' && $simbatch )
    {
      $success = TRUE;

      $expires_date = $simbatch->EXPIRES_DATE;

      if ( isset($mode) && ( $mode == 'PREACTIVATION' ) )
      {
        $sim_ready_activate = $success;

        // if the SIM comes back as ready to activate; a flag should be set in Redis with ttl = 5m noting that sim is 'good to activate.'
        if ( $success )
        {
          $redis = new \Ultra\Lib\Util\Redis;

          $redis->set( 'iccid/good_to_activate/' . $ICCID , 1 , 60 * 20 );
        }
      }
      elseif ( isset($mode) && ( $mode == 'BG_PREACTIVATION' ) )
      {
        $sim_ready_activate = $success;

          $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

          $result = $mwControl->mwCanActivate(
            array(
              'actionUUID' => getNewActionUUID('provisioning ' . time()),
              'iccid'      => $ICCID
            )
          );

          if ( $result->has_errors() )
          {
            $success = FALSE;
            $errors  = $result->get_errors();
          }
          elseif ( ! isset($result->data_array['available']) || $result->data_array['available'] != 'true')
          {
            $success = FALSE;
            $errors  = array('ERR_API_INVALID_ARGUMENTS: ICCID not available.');
          }
      }
    }
    else
      $errors[] = "ERR_API_INVALID_ARGUMENTS: ICCID not valid or already used.";
  }
  else
    $errors[] = "ERR_API_INVALID_ARGUMENTS: ICCID not found.";

  return flexi_encode(fill_return($p,
                                  "provisioning__checkSIMCard",
                                  func_get_args(),
                                  array("success"   => $success,
                                        "errors"    => $errors,
                                        "sim_ready_activate" => $sim_ready_activate,
                                        "valid_ext"          => $iccid_user_status_extended,
                                        "valid"              => $iccid_user_status,
                                        "expires_date"       => $expires_date,
                                        "master_agent"       => $master_agent
                                  )));
}

?>

<?php


// This is the API to be invoked after requestProvisionNewCustomerAsync in order to get the complete response
function provisioning__verifyProvisionNewCustomerAsync($partner_tag, $request_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success      = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "provisioning__verifyProvisionNewCustomerAsync",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "provisioning__verifyProvisionNewCustomerAsync", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "provisioning__verifyProvisionNewCustomerAsync",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  ##==-- check transition --==##

  $transition_result = provision_check_transition($request_id);

  $transition_result['errors'] = append_transition_failure_reason($transition_result['errors'],$request_id);

  return flexi_encode(fill_return($p,
                                  "provisioning__verifyProvisionNewCustomerAsync",
                                  func_get_args(),
                                  array("success"      => $transition_result['success'],
                                        "phone_number" => $transition_result['phone_number'],
                                        "status"       => $transition_result['status'],
                                        "pending"      => $transition_result['pending'],
                                        "warnings"     => $transition_result['warnings'],
                                        "errors"       => $transition_result['errors'])));
}


?>

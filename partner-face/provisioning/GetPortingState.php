<?php

include_once('db.php');
include_once('db/htt_transition_log.php');
include_once('lib/underscore/underscore.php');
include_once('lib/messaging/functions.php');
include_once('lib/state_machine/functions.php');
include_once('lib/provisioning/functions.php');
require_once 'lib/inventory/functions.php';

// Get a customer's porting state
function provisioning__GetPortingState($partner_tag, $msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;

  dlog('', "($msisdn)");

  $success = FALSE;

  if ($always_succeed)
        return flexi_encode(fill_return($p,
            "provisioning__GetPortingState",
            func_get_args(),
            array("success"  => TRUE,
                "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "provisioning__GetPortingState", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
        return flexi_encode(fill_return($p,
            "provisioning__GetPortingState",
            func_get_args(),
            array("success" => $success,
                "errors"  => $errors)));

  $return = array(
    'success'          => TRUE,
    'errors'           => array(),
    'porting_requests' => array(),
    'request_counts'   => '',
    'porting_status'   => '',
    'resolution'       => ''
  );

  teldata_change_db(); // connect to the DB

  $customers = get_ultra_customers_from_msisdn($msisdn,array('customer_id','MVNE'));

  $redis_port = new \Ultra\Lib\Util\Redis\Port();

  { // MVNE2

    // check PORTIN_QUEUE
    $portInQueue = new \PortInQueue();

    $loadByMsisdnResult = $portInQueue->loadByMsisdn( $msisdn );

    $return['can_resubmit'] = FALSE;

    if ( $loadByMsisdnResult->is_success() )
    {
      $request_id = $redis_port->getRequestId( $portInQueue->customer_id , 'Port-In Requested' );

      if ( property_exists( $loadByMsisdnResult , 'port_status' )
        && $loadByMsisdnResult->port_status
        && ( ( $loadByMsisdnResult->port_status == 'COMPLETED' ) || ( $loadByMsisdnResult->port_status == 'COMPLETE' ) ) )
      {
        $return['request_counts']['complete'] = 1;

        $return['porting_requests'] = array(
          array(
            'request_id'     => $request_id,
            'status'         => 'complete',
            'type'           => 'MVNE',
            'created_epoch'  => $portInQueue->start_epoch
          )
        );

        $return['can_resubmit']   = FALSE;
        $return['resolution']     = array();
        $return['porting_status'] = array( "pending" => FALSE , "porting_resubmittable" => FALSE , "porting_final" => TRUE , "porting_pending" => FALSE , "porting_success" => TRUE , "porting_notice" => '' );
      }
      else
      {
        list(
          $port_success,
          $port_pending,
          $port_status,
          $port_resolution
        ) = interpret_port_status( $portInQueue );

        $return['request_counts']['pending'] = $port_pending;
        $return['porting_requests'] = array(
          array(
            'request_id'     => $request_id,
            'status'         => ( $port_pending ? 'pending' : 'RESOLUTION REQUIRED' ),
            'type'           => 'MVNE',
            'created_epoch'  => $portInQueue->start_epoch
          )
        );

        $return['porting_status'] = array(
          "pending"               => $port_pending ,
          "porting_resubmittable" => !$port_pending ,
          "porting_final"         => FALSE ,
          "porting_pending"       => $port_pending ,
          "porting_success"       => FALSE ,
          "porting_notice"        => $port_status
        );
        $return['resolution']     = array( $port_resolution );
        $return['can_resubmit'] = !$port_pending;

        if ( property_exists( $portInQueue , 'provstatus_error_msg' ) && $portInQueue->provstatus_error_msg )
          $redis_port->redis->set( 'ultra/port/port_query_status/' . $msisdn , $portInQueue->provstatus_error_msg , 60*60*24*7 );
      }
    }
    else
    {
      // $msisdn not found in PORTIN_QUEUE, let's see if there is a customer associated with it

      teldata_change_db(); // connect to the DB

      $customer = get_customer_from_msisdn($msisdn);

      if ( $customer )
      {
        // check the last transition

        $sql = htt_transition_log_select_query(
          array(
            'sql_limit'   => 1,
            'customer_id' => $customer->CUSTOMER_ID,
            'order_by'    => 'ORDER BY t.CREATED ASC'
          )
        );

        $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

        if ( ( is_array($query_result) ) && count($query_result) > 0 )
        {
          dlog('',"found state transition %s",$query_result[0]);

          $return['can_resubmit'] = FALSE;

          if ( $query_result[0]->STATUS == 'OPEN' )
          {
            $return['request_counts']['pending'] = TRUE;

            $return['porting_requests'] = array(
              array(
                'can_resubmit'   => FALSE,
                'request_id'     => $query_result[0]->TRANSITION_UUID,
                'status'         => 'INITIATED',
                'type'           => 'MVNE',
                'created_epoch'  => $query_result[0]->created_epoch,
                'porting_status' => array(
                  "pending"               => TRUE  ,
                  "porting_resubmittable" => FALSE ,
                  "porting_final"         => FALSE ,
                  "porting_pending"       => TRUE  ,
                  "porting_success"       => FALSE ,
                  "porting_notice"        => 'INITIATED'
                ),
                'resolution'     => array()
              )
            );
          }
          else
          {
            $return['request_counts']['pending'] = FALSE;

            $return['porting_requests'] = array(
              array(
                'can_resubmit'   => FALSE,
                'request_id'     => $query_result[0]->TRANSITION_UUID,
                'status'         => 'contact care',
                'type'           => 'MVNE',
                'created_epoch'  => $query_result[0]->created_epoch,
                'porting_status' => array(
                  "pending"               => FALSE ,
                  "porting_resubmittable" => FALSE ,
                  "porting_final"         => FALSE ,
                  "porting_pending"       => FALSE ,
                  "porting_success"       => FALSE ,
                  "porting_notice"        => 'contact care'
                ),
                'resolution'     => array()
              )
            );
          }
        }
        else
        {
          dlog('',"No transitions found for customer id ".$customer->CUSTOMER_ID);

          $return['success'] = FALSE;
          $return['errors']  = $loadByMsisdnResult->get_errors();
        }
      }
      else
      {
        dlog('',"No customer found for MSISDN ".$msisdn);

        $return['success'] = FALSE;
        $return['errors']  = 'Port-In attempt not found for MSISDN '.$msisdn;
      }
    }
  }

  return flexi_encode(fill_return($p,
        "provisioning__GetPortingState",
        func_get_args(),
        $return));
}


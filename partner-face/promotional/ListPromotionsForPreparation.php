<?php

// Lists the state of current promotions with regards to preparation.
function promotional__ListPromotionsForPreparation($partner_tag)
{
  global $p;
  global $mock;
  global $always_succeed;
  
  $warnings   = array();
  $promotions = array();
  
  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "promotional__ListPromotionsForPreparation",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "promotional__ListPromotionsForPreparation", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "promotional__ListPromotionsForPreparation",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));
  }
  
  try
  {
    teldata_change_db();
    
    $sql = ultra_promotional_plans_select_query( array() );
  
    $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));
    
    if ( ( ! $query_result ) || ( ! is_array($query_result) ) )
      throw new Exception("ERR_API_INTERNAL: DB Error");
    
    if ( count($query_result) )
    {
      // loop through query result
      foreach($query_result as $result_row)
      {
        dlog("","%s",$result_row);

        // total sims related to this promotion
        list($total_sims,$warning)      = htt_inventory_sim_count_by_range( $result_row->ICCID_START , $result_row->ICCID_END );
        
        if ( $warning )
          $warnings[] = $warning;
        
        // number of non-Neutral customers associated with this promotion
        list($ready_customers,$warning) = get_ready_customers_promotional_sim_count( $result_row->ULTRA_PROMOTIONAL_PLANS_ID );
        
        if ( $warning )
          $warnings[] = $warning;

        // number of active iccids in the sim range associated with this promotion
        list($ready_sims,$warning)      = htt_inventory_sim_count_by_range( $result_row->ICCID_START , $result_row->ICCID_END , TRUE );

        if ( $warning )
          $warnings[] = $warning;
        
        // number of Active customers associated with this promo id
        list($active_customers,$warning) = get_active_customers_promotional_sim_count( $result_row->ULTRA_PROMOTIONAL_PLANS_ID );

        if ( $warning )
          $warnings[] = $warning;

        $promotion = array(
          'id'              => $result_row->ULTRA_PROMOTIONAL_PLANS_ID,
          'promo_status'    => $result_row->PROMO_STATUS,
          'project_name'    => $result_row->PROJECT_NAME,
          'channel'         => $result_row->CHANNEL,
          'subchannel'      => $result_row->SUBCHANNEL,
          'microchannel'    => $result_row->MICROCHANNEL,
          'iccid_start'     => $result_row->ICCID_START,
          'iccid_end'       => $result_row->ICCID_END,
          'total_sims'      => $total_sims,
          'ready_customers' => $ready_customers,
          'ready_sims'      => $ready_sims,
          'active_customers' => $active_customers
        );

        $promotions[] = $promotion;
      }
    }
    else
    {
      $warnings[] = "ERR_API_INTERNAL: No data found";
    }
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }
  
  $success = ! count($errors);

  return flexi_encode(fill_return($p,
                                  "promotional__ListPromotionsForPreparation",
                                  func_get_args(),
                                  array("success"    => $success,
                                        "warnings"   => $warnings,
                                        "promotions" => $promotions,
                                        "errors"     => $errors)));
}

?>

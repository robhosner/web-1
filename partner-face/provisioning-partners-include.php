<?php

include_once('cosid_constants.php');
include_once('db.php');
include_once('db/htt_coverage_info.php');
include_once('db/htt_portin.php');
require_once 'db/ultra_session.php';
include_once('fraud.php');
include_once('lib/inventory/functions.php');
include_once('lib/messaging/functions.php');
include_once('lib/payments/functions.php');
include_once('lib/portal/functions.php');
include_once('lib/portin/functions.php');
include_once('lib/provisioning/functions.php');
include_once('lib/state_machine/aspider.php');
include_once('lib/state_machine/functions.php');
include_once('lib/transitions.php');
include_once('lib/util-common.php');
require_once 'Ultra/Lib/DB/Celluphone/functions.php';
include_once('partner-face/provisioning/checkPINCard.php');
include_once('partner-face/provisioning/checkPINCards.php');
include_once('partner-face/provisioning/requestPortFundedCustomerAsync.php');
include_once('partner-face/provisioning/requestProvisionNewCustomerAsync.php');
include_once('partner-face/provisioning/requestProvisionPortedCustomerAsync.php');
include_once('partner-face/provisioning/ProvisionBasicOrangeCustomerAsync.php');
include_once('partner-face/provisioning/ProvisionBasicNewCustomerAsync.php');
include_once('partner-face/provisioning/provision_check_transition.php');
include_once('partner-face/provisioning/CancelRequestedPort.php');
include_once('partner-face/provisioning/verifyProvisionNewCustomerAsync.php');
include_once('classes/postageapp.inc.php');


date_default_timezone_set("America/Los_Angeles");


# Important: for Async APIS, request_id is HTT_TRANSITION_LOG.TRANSITION_UUID

function validate_plan_amount($creditAmount,$targetPlan)
{
  $error = FALSE;

  $min_value = ( substr($targetPlan,-2) * 100 ); # L[12345]9 => [12345]900

  if ( $creditAmount < $min_value )
  { $error = "ERR_API_INVALID_ARGUMENTS: creditAmount is not sufficient for the given plan ($targetPlan)"; }

  return $error;
}


function verify_cc_number_propagation($result_status,$customer)
{
  # Wait for the credit card number to propagate to the CUSTOMERS table.

  $count = 5;

  while (
    ( ( ! $customer->CC_NUMBER ) || ( $customer->CC_NUMBER == '' ) || ( $customer->CC_NUMBER == ' ' ) )
    &&
    ( $count )
  )
  {
    sleep(2);

    # load the data we just saved in the $customer object
    $customer = get_customer_from_customer_id( $customer->CUSTOMER_ID );

    $count--;
  }

  if ( ! $customer->CC_NUMBER )
  {
    dlog('',"Failure: credit card number not found in CUSTOMERS table for CUSTOMER_ID ".$customer->CUSTOMER_ID);
    # Failure: credit card number is not in the CUSTOMERS table
    $result_status['errors'][] = "ERR_API_INTERNAL: DB error";
    $result_status['success'] = 0;
  }

  return array($result_status,$customer);
}


function validate_pins_to_apply($pinsToApply)
{
  $errors = array();

  $pin_validation = func_validate_pin_cards( array('pin_list' => explode(",",$pinsToApply) ) );

  if ( $pin_validation['at_least_one_not_found'] )
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: some of the given PINs are invalid";      }

  else if ( $pin_validation['at_least_one_customer_used'] )
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: some of the given PINs are already used"; }

  else if ( $pin_validation['at_least_one_at_foundry'] )
  { $errors[] = "ERR_API_INVALID_ARGUMENTS: the PIN is not active";                   }

  else if ( count($pin_validation['errors']) )
  { $errors = $pin_validation['errors'];                                              }

  return array($errors, $pin_validation);
}


?>

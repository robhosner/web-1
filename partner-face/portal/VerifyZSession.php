<?php

// Checks the zsession
function portal__VerifyZSession($partner_tag, $zsession, $customer)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__VerifyZSession",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  dlog('', "Passed zsession %s and customer %s", $zsession, $customer);

  $errors = validate_params($p, "portal__VerifyZSession", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__VerifyZSession",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  dlog('', 'We have customer %s', json_encode(get_zsession_customer()));

  return flexi_encode(fill_return($p,
                                  "portal__VerifyZSession",
                                  func_get_args(),
                                  array("success" => TRUE,
                                        "errors" => $errors)));
}

?>

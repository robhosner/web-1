<?php

// Obtain the rows in HTT_BILLING_HISTORY for a customer.
function portal__GetIDDHistory($zsession,$start_epoch,$end_epoch)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success     = FALSE;
  $idd_history = array();

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "portal__GetIDDHistory",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "portal__GetIDDHistory", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "portal__GetIDDHistory",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db(); // connect to the DB

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
    $errors = $data_zsession['errors'];
  else
  {
    $customer = $data_zsession['customer'];

    $idd_history_select_query = idd_history_select_query(
      array(
        'account_id'  => $customer->ACCOUNT_ID,
        'start_epoch' => $start_epoch,
        'end_epoch'   => $end_epoch
      )
    );

    dlog('',$idd_history_select_query);

    $query_result = mssql_fetch_all_objects(logged_mssql_query($idd_history_select_query));

    #$query_result = get_idd_history_from_account_id( $customer->ACCOUNT_ID , $start_epoch , $end_epoch );

    if ( ( is_array($query_result) ) && count($query_result) > 0 )
    {
      dlog('',"%s",$query_result);

      foreach( $query_result as $data )
      {
        # {"history_epoch_pst":1360601033,"destination":"INDIA MOBILE BSNL","destination_number":"919442873544","minutes":3,"rate":0.01}
        $idd_history[] = $data->history_epoch_pst;
        $idd_history[] = $data->destination;
        $idd_history[] = $data->destination_number;
        $idd_history[] = $data->minutes;
        $idd_history[] = $data->rate;
      }
    }
  }

  $success = ! count($errors);

  return flexi_encode(fill_return($p,
                                  "portal__GetIDDHistory",
                                  func_get_args(),
                                  array("success"     => $success,
                                        "idd_history" => $idd_history,
                                        "errors"      => $errors)));
}


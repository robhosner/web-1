<?php


include_once('lib/inventory/functions.php');


// Returns a list of all the shipwire order_ids associated with a customer logged in.
function portal__GetCustomerShipwireOrders($partner_tag, $zsession)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__GetCustomerShipwireOrders",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "portal__GetCustomerShipwireOrders", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "portal__GetCustomerShipwireOrders",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  teldata_change_db(); // connect to the DB

  $output_params = array(
    'success' => FALSE,
    'errors'  => $errors
  );

  # $customer = get_zsession_customer(); # BROKEN

  $data_zsession = get_customer_from_zsession($zsession);

  if ( count($data_zsession['errors']) )
  {
    $output_params['errors'] = $data_zsession['errors'];
  }
  else
  {
    $customer = $data_zsession['customer'];

    if ( $customer )
    {
      $output_params = get_shipwire_log_data_by_customer_id( $customer->CUSTOMER_ID );
    }
    else
    { $output_params['errors'][] = "ERR_API_INVALID_ARGUMENTS: invalid login"; }

  }

  return flexi_encode(fill_return($p,
                                  "portal__GetCustomerShipwireOrders",
                                  func_get_args(),
                                  $output_params));
}


?>

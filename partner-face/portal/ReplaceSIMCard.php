<?php

// Replaces SIM card for another.
function portal__ReplaceSIMCard($partner_tag, $zsession, $old_ICCID, $new_ICCID)
{
  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  $success  = FALSE;
  $warnings = array();

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "portal__ReplaceSIMCard",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  teldata_change_db(); // connect to the DB

  $errors = validate_params($p, "portal__ReplaceSIMCard", func_get_args(), $mock);

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** verify that old_ICCID and new_ICCID are valid *** */

    if ( ! validate_ICCID($old_ICCID,0) )
    { $errors[] = 'ERR_API_INVALID_ARGUMENTS: the old ICCID is invalid (1)';             }

    $newSim = get_htt_inventory_sim_from_iccid($new_ICCID);
    if ( ! $newSim || ! validate_ICCID($newSim,1) )
    { $errors[] = 'ERR_API_INVALID_ARGUMENTS: the given new_ICCID is invalid or already used'; }

    // block replacement ORANGE sim
    if (strtoupper($newSim->PRODUCT_TYPE) == 'ORANGE')
    { $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot replace with Orange SIM'; }
  }

  $data_zsession = get_customer_from_zsession($zsession);
  if ( count($data_zsession['errors']) )
    $errors = $data_zsession['errors'];
  else
  {
    /* *** verify that old_ICCID belongs to $customer_id in HTT_INVENTORY_SIM *** */
    $customer = $data_zsession['customer'];
    $htt_inventory_sim_select_query = htt_inventory_sim_select_query(
      array(
        "iccid_number" => $old_ICCID
      )
    );

    $htt_inventory_sim_result = mssql_fetch_all_objects(logged_mssql_query($htt_inventory_sim_select_query));

    if (! $htt_inventory_sim_result )
      $errors[] = "ERR_API_INVALID_ARGUMENTS: the old ICCID is invalid (2)";
    elseif ( ! is_array($htt_inventory_sim_result) )
      $errors[] = "ERR_API_INVALID_ARGUMENTS: the old ICCID is invalid (3)";
    elseif ( count($htt_inventory_sim_result) < 1 )
      $errors[] = "ERR_API_INVALID_ARGUMENTS: the old ICCID is invalid (4)";
    elseif ( $htt_inventory_sim_result[0]->CUSTOMER_ID != $customer->CUSTOMER_ID )
      $errors[] = "ERR_API_INVALID_ARGUMENTS: the old ICCID is invalid (5)";

    // PROD-511: prohibit modifications to in-Active subscribers
    if ($customer->plan_state !== 'Active')
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: Subscriber must be active to change SIM or phone number.';

    if ( ! count($errors))
    {
      /* *** check against HTT_CUSTOMERS_OVERLAY_ULTRA.CURRENT_ICCID_FULL *** */
      if ( $customer->CURRENT_ICCID_FULL != luhnenize($old_ICCID) )
      {
        $errors[] = "ERR_API_INVALID_ARGUMENTS: the given old_ICCID does not belong to customer_id $customer_id (3)";
      }
      else
      {
        if ( ! isSameBrandByICCID($old_ICCID, $new_ICCID))
          $errors[] = 'ERR_API_INVALID_ARGUMENTS: the given new_ICCID is of an INVALID BRAND type';
      }
    }
  }

  if ( ! ($errors && count($errors) > 0) )
  {
    /* *** do not allow SIM replacement during MISO tasks *** */

    if ( \Ultra\Lib\MVNE\exists_open_task( $customer->CUSTOMER_ID ) )
      $errors[] = "ERR_API_INTERNAL: The subscriber cannot currently change their details due to system alignment. Please try again in 10 minutes.";
  }

  $old_mvne = '1';
  $new_mvne = '1';

  if ( ! ($errors && count($errors) > 0) )
  {
    // determine ICCID MVNE: fail if not found or mismatch between old and new

    $old_mvne = $htt_inventory_sim_result[0]->MVNE;
    $new_mvne = \Ultra\Lib\DB\Getter\getScalar('ICCID', $new_ICCID, 'MVNE');

    if ( ! $old_mvne )
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot use SIM for this customer (1)';

    if ( ! $new_mvne )
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot use SIM for this customer (2)';

    if ( ( $old_mvne == '2' ) && ( $new_mvne == '1' ) )
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot use SIM for this customer (3)';
  }

  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  // verify new ICCID
  if ( ! ($errors && count($errors) > 0) )
    {
      $result = $mwControl->mwCanActivate(
        array(
          'actionUUID' => getNewActionUUID('dealerportal ' . time()),
          'iccid'  => $new_ICCID));

      if ($result->is_failure() || ! isset($result->data_array['available']) || $result->data_array['available'] != 'true')
        $errors[] = 'ERR_API_INVALID_ARGUMENTS: cannot activate this ICCID (2)';
    }

  // replace ICCID
  if ( ! count($errors) )
    {
      $result = $mwControl->mwChangeSIM(
        array(
          'actionUUID'         => getNewActionUUID('dealerportal ' . time()),
          'msisdn'             => normalize_msisdn($customer->current_mobile_number, TRUE),
          'old_iccid'          => $old_ICCID,
          'new_iccid'          => $new_ICCID,
          'customer_id'        => $customer->CUSTOMER_ID));

      dlog('',"mwChangeSIM result data = %s",$result->data_array);

      // the MW call failed
      if ( ! $result->is_success() )
      {
        $errors = $result->get_errors();

        dlog('',"mwChangeSIM result errors = %s",$errors);
      }
      // the MW ChangeSIM command failed
      elseif ( ! $result->data_array['success'] )
      {
        $errors = $result->data_array['errors'];

        dlog('',"mwChangeSIM result data errors = %s",$errors);
      }

      teldata_change_db(); // done with MVNE2 calls
    }

  if ( ! count($errors) )
    // side effects: HTT_CUSTOMERS_OVERLAY_ULTRA and HTT_INVENTORY_SIM
    $errors = callbackChangeSIM( $new_ICCID , $customer->CUSTOMER_ID , '' );

  // store row in HTT_BILLING_HISTORY
  if ( ! count($errors) )
  {
    $description = 'Replace SIM';
    $entry_type  = 'SIM_REPLACEMENT';

    if ( ( $old_mvne == '1' ) && ( $new_mvne == '2' ) )
    {
      $description = 'MVNEs SIM Swap';
      $entry_type  = 'MVNES_SIM_SWAP';
    }

    $history_params = array(
      "customer_id"            => $customer->CUSTOMER_ID,
      "date"                   => 'now',
      "cos_id"                 => $customer->COS_ID,
      "entry_type"             => $entry_type,
      "stored_value_change"    => 0,
      "balance_change"         => 0,
      "package_balance_change" => 0,
      "charge_amount"          => 0,
      "reference"              => 'portal__ReplaceSIMCard',
      "reference_source"       => 'PORTAL',
      "detail"                 => 'portal__ReplaceSIMCard',
      "description"            => $description,
      "result"                 => 'COMPLETE',
      "source"                 => $entry_type,
      "is_commissionable"      => 0,
      "terminal_id"            => '0'
    );
    $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );
    if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
      $success = TRUE;
    else
      $errors[] = "ERR_API_INTERNAL: Database write error (2)";
  }

  return flexi_encode(
    fill_return(
      $p,
      "portal__ReplaceSIMCard",
      func_get_args(),
      array(
        'success' => $success,
        'errors'  => $errors,
        'warnings' => $warnings)));
}


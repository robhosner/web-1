<?php

include_once('fraud.php');
include_once('lib/inventory/functions.php');
include_once('partner-face/inventory-partners-include.php');
include_once 'db/htt_inventory_sim.php';

/**
 * Makes the range of ICCID's hot.  If startICCID = endICCID, only makes one SIM Hot.
 *
 * @param string $partner_tag
 * @param string $username
 * @param iccid19 $startICCID
 * @param iccid19 $endICCID
 * @param integer $masteragent
 */
function inventory__AssignICCIDRangeToHot($partner_tag, $username, $startICCID, $endICCID, $masteragent)
{
    global $p;
    global $mock;
    global $always_succeed;

    if ($always_succeed)
        return flexi_encode(fill_return($p,
                "inventory__AssignICCIDRangeToHot",
                func_get_args(),
                array("success"  => TRUE,
                      "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

    $errors = validate_params($p, "inventory__AssignICCIDRangeToHot", func_get_args(), $mock);

    // This query only selects ORANGE Sims by the specified Master Agent -- first two validtions
    $rowSet = get_htt_inventory_sims_by_range_for_masterAgentId($startICCID, $endICCID, $masteragent, 'ORANGE'); // For the Orange Sim Project
    
    if( isset($rowSet) && count($rowSet) <= 0 )
    {
      $errMsg = sprintf("ERR_API_NO_ORANGE_SIM_FOR_MASTERAGENT: No Orange Sims found for Master Agent = %d", $masteragent );
      $errors[] = $errMsg;
    }
    // The next validation is to insure the ICCID numbers are non-interrupted in the contingious set of no more than 500 ICCID's returned
    
    $first        = true;
    $lastIccid    = null;
    $isContigious = true;  // Starts off as contigious to cover a ICCID range of 1
    $returnData   = array();
     
    foreach ( $rowSet as $row )
    {
      if ( ! $row->STORED_VALUE)
      {
        $errors[] = sprintf("ERR_API_INVALID_ARGUMENTS: ORANGE ICCID %s has no STORED_VALUE", $row->ICCID_NUMBER );
        break;
      }

      if ( $first )
      {
        $first = false;
        $lastIccid = $row->ICCID_NUMBER;
        $returnRow = array(
          'ICCID_FULL'     => $row->ICCID_FULL,
          'CUSTOMER_ID'    => $row->CUSTOMER_ID,
          'MASTERAGENT_ID' => $row->INVENTORY_MASTERAGENT,
          'SIM_HOT'        => $row->SIM_HOT,
          'PLAN_STATE'     => $row->PLAN_STATE
        );
        $returnData[] = $returnRow;
            continue;  // next one please
      }
      else
      {
        if ( $row->ICCID_NUMBER == $lastIccid + 1)
        {
          // Keep going
          $lastIccid = $row->ICCID_NUMBER;
          $isContigious = true;
          $returnRow = array(
            'ICCID_FULL'     => $row->ICCID_FULL,
            'CUSTOMER_ID'    => $row->CUSTOMER_ID,
            'MASTERAGENT_ID' => $row->INVENTORY_MASTERAGENT,
            'SIM_HOT'        => $row->SIM_HOT,
            'PLAN_STATE'     => $row->PLAN_STATE
          );
          $returnData[] = $returnRow;
        }
        else
        {
          $isContigious = false;
          $returnData   = array();
          $errMsg = sprintf("ERR_API_ICCID_NOT_CONTIGIOUS: ICCID range is not have consecutive values, ICCID's missing.  Last Value=%s, Current Value = %s", $lastIccid, $row->ICCID_NUMBER );
          $errors[] = $errMsg;
          break; // Done, no sense in going on, report it is not contigious error.
        }
      }
    }
                
    if ($errors && count($errors) > 0)
        return flexi_encode(fill_return($p,
                "inventory__AssignICCIDRangeToHot",
                func_get_args(),
                array("success" => FALSE,
                      "errors"  => $errors)));
    
    $updateData = set_master_agent_iccid_range_hotorcold( $startICCID, $endICCID, $masteragent, 1 );
    
    dlog( '', 'Update data = %d', $updateData );

    $success = ! count($errors);

    return flexi_encode(fill_return($p,
            "inventory__AssignICCIDRangeToHot",
            func_get_args(),
            array("success" => $success,
                  "errors"  => $errors)));
}


/**
 * Makes the range of ICCID's cold.  If startICCID = endICCID, only makes one SIM Cold.
 *
 * @param string $partner_tag
 * @param string $username
 * @param iccid19 $startICCID
 * @param iccid19 $endICCID
 * @param integer $masteragent
 */
function inventory__AssignICCIDRangeToCold($partner_tag, $username, $startICCID, $endICCID, $masteragent)
{
    global $p;
    global $mock;
    global $always_succeed;

    if ($always_succeed)
    {
        return flexi_encode(fill_return($p,
                "inventory__AssignICCIDRangeToCold",
                func_get_args(),
                array("success"  => TRUE,
                        "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
    }

    $errors = validate_params($p, "inventory__AssignICCIDRangeToCold", func_get_args(), $mock);

    // This query only selects ORANGE Sims by the specified Master Agent -- first two validtions
    $rowSet = get_htt_inventory_sims_by_range_for_masterAgentId($startICCID, $endICCID, $masteragent, 'ORANGE'); // For the Orange Sim Project
    
    if( isset($rowSet) && count($rowSet) <= 0 )
    {
        $errMsg = sprintf("ERR_API_NO_ORANGE_SIM_FOR_MASTERAGENT: No Orange Sims found for Master Agent = %d", $masteragent );
        $errors[] = $errMsg;
    }
    // The next validation is to insure the ICCID numbers are non-interrupted in the contingious set of no more than 500 ICCID's returned
    //
    
    $first        = true;
    $lastIccid    = null;
    $isContigious = true;  // Starts off as contigious to cover a ICCID range of 1
    $returnData   = array();
     
    foreach ( $rowSet as $row )
    {
    if( $first ) {
    $first = false;
        $lastIccid = $row->ICCID_NUMBER;
        $returnRow = array('ICCID_FULL' => $row->ICCID_FULL,
                'CUSTOMER_ID'    => $row->CUSTOMER_ID,
                'MASTERAGENT_ID' => $row->INVENTORY_MASTERAGENT,
                'SIM_HOT'        => $row->SIM_HOT,
                'PLAN_STATE'     => $row->PLAN_STATE
        );
        $returnData[] = $returnRow;
            continue;  // next one please
        }
        else {
            if( $row->ICCID_NUMBER == $lastIccid + 1) {
            // Keep going
                $lastIccid = $row->ICCID_NUMBER;
                $isContigious = true;
                $returnRow = array('ICCID_FULL'     => $row->ICCID_FULL,
                                   'CUSTOMER_ID'    => $row->CUSTOMER_ID,
                                   'MASTERAGENT_ID' => $row->INVENTORY_MASTERAGENT,
                                   'SIM_HOT'        => $row->SIM_HOT,
                                   'PLAN_STATE'     => $row->PLAN_STATE
                );
                $returnData[] = $returnRow;
                }
                else {
                $isContigious = false;
                $returnData   = array();
                $errMsg = sprintf("ERR_API_ICCID_NOT_CONTIGIOUS: ICCID range is not have consecutive values, ICCID's missing.  Last Value=%s, Current Value = %s", $lastIccid, $row->ICCID_NUMBER );
                $errors[] = $errMsg;
                break; // Done, no sense in going on, report it is not contigious error.
            }
        }
    }
    
    if ($errors && count($errors) > 0)
    {
        // we use errors[] and warnings[] for more nuance instead of SoapFaults
        //throw new SoapFault("Server","Invalid parameters.");
        return flexi_encode(fill_return($p,
                "inventory__AssignICCIDRangeToCold",
                func_get_args(),
                array("success" => FALSE,
                        "errors"  => $errors)));
    }

    $updateData = set_master_agent_iccid_range_hotorcold( $startICCID, $endICCID, $masteragent, 0 );
    
    dlog( '', 'Update data = %d', $updateData );
    
    $success = ! count($errors);

    return flexi_encode(fill_return($p,
            "inventory__AssignICCIDRangeToCold",
            func_get_args(),
            array("success" => $success,
                    "errors"  => $errors)));
}


/**
 * Obtains the range of SIMS by Master Agent
 *
 * @param string $partner_tag
 * @param iccid19 $startICCID
 * @param iccid19 $endICCID
 * @param integer $masteragent
 */
function inventory__GetSIMSByMasteragent($partner_tag, $startICCID, $endICCID, $masteragent)
{
    global $p;
    global $mock;
    global $always_succeed;

    if ($always_succeed)
    {
        return flexi_encode(fill_return($p,
                "inventory__GetSIMSByMasteragent",
                func_get_args(),
                array("success"  => TRUE,
                        "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
    }

    $errors = validate_params($p, "inventory__GetSIMSByMasteragent", func_get_args(), $mock);

    if ($errors && count($errors) > 0)
    {
        // we use errors[] and warnings[] for more nuance instead of SoapFaults
        //throw new SoapFault("Server","Invalid parameters.");
        return flexi_encode(fill_return($p,
                "inventory__GetSIMSByMasteragent",
                func_get_args(),
                array("success" => FALSE,
                        "errors"  => $errors)));
    }

    // This query only selects ORANGE Sims by the specified Master Agent -- first two validtions
    $rowSet = get_htt_inventory_sims_by_range_for_masterAgentId($startICCID, $endICCID, $masteragent, 'ORANGE'); // For the Orange Sim Project
    
    if( isset($rowSet) && count($rowSet) <= 0 )
    {
        $errMsg = sprintf("ERR_API_NO_ORANGE_SIM_FOR_MASTERAGENT: No Orange Sims found for Master Agent = %d", $masteragent );
        $errors[] = $errMsg;
    }
    // The next validation is to insure the ICCID numbers are non-interrupted in the contingious set of no more than 500 ICCID's returned
    //
    
    $first        = true;
    $lastIccid    = null;
    $isContigious = true;  // Starts off as contigious to cover a ICCID range of 1
    $returnData   = array();
   
    foreach ( $rowSet as $row ) 
    {
        if( $first ) {
          $first = false;
          $lastIccid = $row->ICCID_NUMBER;
          $returnRow = array('ICCID_FULL'     => $row->ICCID_FULL,
                  'CUSTOMER_ID'    => $row->CUSTOMER_ID,
                  'MASTERAGENT_ID' => $row->INVENTORY_MASTERAGENT,
                  'SIM_HOT'        => $row->SIM_HOT,
                  'PLAN_STATE'     => $row->PLAN_STATE
          );
          $returnData[] = $returnRow;
          continue;  // next one please
        } 
        else {
            if( $row->ICCID_NUMBER = $lastIccid + 1) {
                // Keep going
                $lastIccid = $row->ICCID_NUMBER;
                $isContigious = true;
                $returnRow = array('ICCID_FULL'     => $row->ICCID_FULL, 
                                   'CUSTOMER_ID'    => $row->CUSTOMER_ID,
                                   'MASTERAGENT_ID' => $row->INVENTORY_MASTERAGENT,
                                   'SIM_HOT'        => $row->SIM_HOT,
                                   'PLAN_STATE'     => $row->PLAN_STATE
                                   );
                $returnData[] = $returnRow;
            }
            else {
                $isContigious = false;
                $returnData   = array();
                $errMsg = sprintf("ERR_API_ICCID_NOT_CONTIGIOUS: ICCID range is not have consecutive values, ICCID's missing.  Last Value=%s, Current Value = %s", $lastIccid, $row->ICCID_NUMBER );
                $errors[] = $errMsg;
                break; // Done, no sense in going on, report it is not contigious error.
            }
        }
    }
    
    $success = ! count($errors);

    return flexi_encode(fill_return($p,
            "inventory__GetSIMSByMasteragent",
            func_get_args(),
            array("success"                => $success,
                  "masteragent_iccid_info" => $returnData,
                  "errors"                 => $errors)));
}


/**
 * inventory__UpdateSIMInventory
 *
 * For less critical SIM Inventory details, this command is used.
 *
 * @param string $partner_tag
 * @param string $startICCID
 * @param string $endICCID
 * @param epoch $request_epoch
 * @param string $startICCIDchecksum
 * @param string $updated_by
 * @param string $action
 */
function inventory__UpdateSIMInventory($partner_tag, $startICCID, $endICCID, $request_epoch, $startICCIDchecksum, $updated_by, $action)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "inventory__UpdateSIMInventory",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "inventory__UpdateSIMInventory", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "inventory__UpdateSIMInventory",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  teldata_change_db(); // connect to the DB

  // make sure startICCID exist
  $query = htt_inventory_sim_select_query(array("iccid" => $startICCID));
  if ( ! find_first($query) )
    return flexi_encode(fill_return($p, "inventory__UpdateSIMInventory", func_get_args(), array("success" => FALSE, "errors" => array('ERR_API_INVALID_ARGUMENTS: startICCID does not exist'))));

  // make sure endICCID exist
  $query = htt_inventory_sim_select_query(array("iccid" => $endICCID  ));
  if ( ! find_first($query) )
    return flexi_encode(fill_return($p, "inventory__UpdateSIMInventory", func_get_args(), array("success" => FALSE, "errors" => array('ERR_API_INVALID_ARGUMENTS: endICCID does not exist'))));

  // update HTT_INVENTORY_SIM
  $errors = batch_update_htt_inventory_sim(
    array(
      'startICCID'       => $startICCID,
      'endICCID'         => $endICCID,
      'inventory_status' => $action
    )
  );

  if ( ! ($errors && count($errors) > 0) )
    $success = TRUE;

  return flexi_encode(fill_return($p,
                                  "inventory__UpdateSIMInventory",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors" => $errors)));
}


function verify_luhn_start_end( $startICCID , $endICCID )
{
  if ( ! luhn_checksum_check( $startICCID ) )
  {
    $errors[] = "ERR_INV_INVALID_ARGUMENTS: startICCID is not valid";
  }

  if ( ! luhn_checksum_check( $endICCID ) )
  {
    $errors[] = "ERR_INV_INVALID_ARGUMENTS: endICCID is not valid";
  }

  return $errors;
}


function verify_ICCID_presence( $startICCID , $endICCID )
{
  $error = '';

  $htt_inventory_sim_select_query = htt_inventory_sim_select_query(
    array(
      "iccid_numbers" => array( $startICCID , $endICCID )
    )
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_inventory_sim_select_query));

  if ( is_array($query_result) )
  {
    if ( count($query_result) < 2 )
    {
      $error = "ERR_INV_INVALID_ARGUMENTS: Some of the ICCIDs are missing";
    }
  }
  else
  {
    $error = "ERR_API_INTERNAL: Error while reading from DB";
  }

  return $error;
}


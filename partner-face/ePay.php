<?php

require_once 'cosid_constants.php';
require_once 'db.php';
require_once 'db/htt_billing_actions.php';
require_once 'db/htt_customers_overlay_ultra.php';
require_once 'db/ultra_activation_history.php';
require_once 'fraud.php';
require_once 'partner-face/ePay/CheckMobileBalance.php';
require_once 'partner-face/ePay/SubmitRealtimeReload.php';
require_once 'partner-face/webpospayments/WebPosActivateCustomer.php';
require_once 'partner-face/webpospayments/WebPosCancelCustomerTransaction.php';
require_once 'partner-face/webpospayments/WebPosPortInCustomer.php';
require_once 'session.php';
require_once 'Ultra/Lib/Util/Redis/WebPos.php';
require_once 'Ultra/Lib/Util/Validator.php';
require_once 'lib/portin/functions.php';

date_default_timezone_set("America/Los_Angeles");


// Allows you to void a balance application. Must be submitted within 120 seconds of the first request. 
function externalpayments__CancelRealtimeReload($partner_tag, $phone_number, $request_epoch, $cancel_type, $ultra_payment_trans_id, $load_amount, $provider_trans_id)
{
  global $p;
  global $mock;
  global $always_succeed;
  $provider = 'EPAY'; // VYT @ 2015-01-06: this API is used by EPAY only

# TODO: fix void/cancel bevhavior ($ultra_payment_trans_id is not currently used here)

  $success = FALSE;

  $fraud_data = array(
    'load_amount'       => $load_amount,
    'provider'          => $provider,
    'cancel_type'       => $cancel_type,
    'provider_transid'  =>$provider_trans_id);

  $customer = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "externalpayments__CancelRealtimeReload",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "externalpayments__CancelRealtimeReload", func_get_args(), $mock);

  $customer_balance = '';

  if ( ! ($errors && count($errors) > 0) )
  {
    teldata_change_db(); // connect to the DB

    $customer = get_customer_from_msisdn($phone_number);

    if ( $customer )
    {
      $plan_name = get_plan_name( $customer );

      if ( $plan_name )
      {
        $customer_balance = floor($customer->BALANCE * 100) / 100;

        # not necessary
        # $errors = validate_ultra_amount($customer, $load_amount);

        # fraud_event($customer, 'externalpayments', 'CancelRealtimeReload', 'error', $fraud_data);
      }
      else
        array("ERR_API_INVALID_ARGUMENTS: plan not found from MSISDN $phone_number");
    }
    else
      $errors = array("ERR_API_INVALID_ARGUMENTS: customer not found from MSISDN $phone_number");

  }

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "externalpayments__CancelRealtimeReload",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));

  // update HTT_BILLING_ACTIONS row to $cancel_type

  $htt_billing_actions_update_query = htt_billing_actions_update_query(
    array(
      "status_from"    => 'OPEN',
      "status_to"      => $cancel_type, # TODO: this should fork in 2 different cases
      "customer_id"    => $customer->CUSTOMER_ID,
      "transaction_id" => $provider_trans_id,
      "amount"         => $load_amount
    )
  );

  $check = is_mssql_successful(logged_mssql_query($htt_billing_actions_update_query));

  if ( ! $check )
  {
    $errors[] = "ERR_API_INTERNAL: Database write error";

    # fraud_event($customer, 'externalpayments', 'CancelRealtimeReload', 'error', $fraud_data);
  }
  else
  {
    # check if we voided the transaction successfully

    $htt_billing_actions_select_query = htt_billing_actions_select_query(
      array(
        "customer_id"    => $customer->CUSTOMER_ID,
        "transaction_id" => $provider_trans_id
      )
    );

    $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_billing_actions_select_query));

    if ( ( is_array($query_result) ) && count($query_result) > 0 )
    {
      $new_status = $query_result[0]->STATUS;

      if ( $new_status == $cancel_type )
      {
        $success = TRUE;

        # fraud_event($customer, 'externalpayments', 'CancelRealtimeReload', 'success', $fraud_data);
      }
      else
      {
        $errors[] = "ERR_API_INTERNAL: Transaction \"$provider_trans_id\" could not be voided";

        # fraud_event($customer, 'externalpayments', 'CancelRealtimeReload', 'error', $fraud_data);
      }

    }
    else
    {
      # we could not find the transaction we tried to void

      $errors[] = "ERR_API_INVALID_ARGUMENTS: Transaction \"$provider_trans_id\" not found";

      $provider_trans_id = '';

      # fraud_event($customer, 'externalpayments', 'CancelRealtimeReload', 'error', $fraud_data);
    }
  }

  $output_array = fill_return($p,
                              "externalpayments__CancelRealtimeReload",
                              func_get_args(),
                              array("success" => $success,
                                    "errors" => $errors));

  $output_array['provider_trans_id'] = $provider_trans_id;
  $output_array['customer_balance']  = $customer_balance;
  if ( $success )
    $output_array['phone_number']     = $phone_number;

  return flexi_encode($output_array);
}


function get_plan_name($customer)
{
  $plan_name = '';

  $allowed_cos_ids = allowed_cos_ids();

  if ( isset( $allowed_cos_ids[ $customer->cos_id ] ) )
    $plan_name = $allowed_cos_ids[ $customer->cos_id ]['name'];

  return $plan_name;
}


function validate_ultra_amount($customer, $load_amount)
{
  $errors = array();

  $allowed_cos_ids = allowed_cos_ids();

  if ( isset( $allowed_cos_ids[ $customer->cos_id ] ) )
  {
    if ( $customer->cos_id != COSID_ULTRA_STANDBY ) # no amount limitation for COSID_ULTRA_STANDBY
    {
      if ( $allowed_cos_ids[ $customer->cos_id ]['cost'] != ( $load_amount ) )
        $errors = array( "ERR_API_INVALID_ARGUMENTS: invalid amount for COS ".$customer->cos_id );
    }
  }

  return $errors;
}

/**
 * validate_customer_plan
 * @param object customer
 * @param string product_id ('ULTRA_ADD_BALANCE' or 'ULTRA_PLAN_RECHARGE')
 * @param string subproduct_id (e.g. 'TWENTY_NINE')
 */
function validate_customer_plan($customer, $product_id, $subproduct_id, $load_amount)
{
  $errors = array();
  $plan_cost = 0; // plan cost without bolt ons, in cents
  $total_cost = 0; // plan cost including bolt ons, in cents
  $cos_id = $customer->MONTHLY_RENEWAL_TARGET ? get_cos_id_from_plan($customer->MONTHLY_RENEWAL_TARGET) : $customer->cos_id;

  $allowed_cos_ids = allowed_cos_ids();
  if ( isset( $allowed_cos_ids[ $cos_id ] ) )
  {
    // BOLT-30: calculate plant cost including bolt ons but excluding taxes
    $plan_cost = $allowed_cos_ids[$cos_id]['cost'];
    if ($product_id == 'ULTRA_PLAN_RECHARGE' && $plan_cost > $load_amount)
    {
      dlog('', "plan cost $plan_cost is greater than load amount $load_amount");
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: Invalid amount';
    }

    $bolt_ons = get_bolt_ons_info_from_customer_options($customer->CUSTOMER_ID);
    $total_cost = $plan_cost;
    foreach ($bolt_ons as $bolt_on)
      $total_cost += $bolt_on['cost'] * 100;
    dlog('', 'plan cost: $%d, full cost: $%d, load amount: $%d', $plan_cost / 100, $total_cost / 100, $load_amount / 100);
  }
  else
    $errors[] = "ERR_API_INVALID_ARGUMENTS: COS $cos_id does not match with the allowed plans for this API call";

  // factored subproduct can fund provisioned and port-in-requested only
  $subproduct = Ultra\UltraConfig\getPaymentProviderSubproductInfo($subproduct_id);
  if ($subproduct && ! $subproduct['commissionable'] && ! in_array($customer->plan_state, array(STATE_PROVISIONED, STATE_PORT_IN_REQUESTED)))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: customer state is invalid for the given call';

  return array($errors, $plan_cost, $total_cost);
}


function allowed_cos_ids()
{
  // currently allowed plans for externalpayments::*

  // [ COS_ID => [ name => DESCRIPTION, cost => COST, subproduct_id, SHORT_NAME ], .. ]
  $planConfig = PlanConfig::Instance();
  return $planConfig->partnerFaceEpayAllowedCosIds();

  return array(
    COSID_ULTRA_NINETEEN           => array('name' => COSID_ULTRA_NINETEEN_NAME,          'cost' => COSID_ULTRA_NINETEEN_COST,          'subproduct_id' => 'L19'),
    COSID_ULTRA_TWENTY_FOUR        => array('name' => COSID_ULTRA_TWENTY_FOUR_NAME,       'cost' => COSID_ULTRA_TWENTY_FOUR_COST,       'subproduct_id' => 'L24'),
    COSID_ULTRA_STWENTY_THREE      => array('name' => COSID_ULTRA_STWENTY_THREE_NAME,     'cost' => COSID_ULTRA_STWENTY_THREE_COST,     'subproduct_id' => 'S23'),
    COSID_ULTRA_TWENTY_NINE        => array('name' => COSID_ULTRA_TWENTY_NINE_NAME,       'cost' => COSID_ULTRA_TWENTY_NINE_COST,       'subproduct_id' => 'L29'),
    COSID_ULTRA_THIRTY_FOUR        => array('name' => COSID_ULTRA_THIRTY_FOUR_NAME,       'cost' => COSID_ULTRA_THIRTY_FOUR_COST,       'subproduct_id' => 'L34'),
    COSID_ULTRA_THIRTY_NINE        => array('name' => COSID_ULTRA_THIRTY_NINE_NAME,       'cost' => COSID_ULTRA_THIRTY_NINE_COST,       'subproduct_id' => 'L39'),
    COSID_ULTRA_DTHIRTY_NINE       => array('name' => COSID_ULTRA_DTHIRTY_NINE_NAME,      'cost' => COSID_ULTRA_DTHIRTY_NINE_COST,      'subproduct_id' => 'D39'),
    COSID_ULTRA_FORTY_FOUR         => array('name' => COSID_ULTRA_FORTY_FOUR_NAME,        'cost' => COSID_ULTRA_FORTY_FOUR_COST,        'subproduct_id' => 'L44'),
    COSID_ULTRA_FORTY_NINE         => array('name' => COSID_ULTRA_FORTY_NINE_NAME,        'cost' => COSID_ULTRA_FORTY_NINE_COST,        'subproduct_id' => 'L49'),
    COSID_ULTRA_FIFTY_NINE         => array('name' => COSID_ULTRA_FIFTY_NINE_NAME,        'cost' => COSID_ULTRA_FIFTY_NINE_COST,        'subproduct_id' => 'L59'),
    COSID_ULTRA_STANDBY            => array('name' => COSID_ULTRA_STANDBY_NAME,           'cost' => COSID_ULTRA_STANDBY_COST,           'subproduct_id' => ''),
    COSID_UNIVISION_TWENTY         => array('name' => COSID_UNIVISION_TWENTY_NAME,        'cost' => COSID_UNIVISION_TWENTY_COST,        'subproduct_id' => 'UV20'),
    COSID_UNIVISION_THIRTY         => array('name' => COSID_UNIVISION_THIRTY_NAME,        'cost' => COSID_UNIVISION_THIRTY_COST,        'subproduct_id' => 'UV30'),
    COSID_UNIVISION_THIRTYFIVE     => array('name' => COSID_UNIVISION_THIRTYFIVE_NAME,    'cost' => COSID_UNIVISION_THIRTYFIVE_COST,    'subproduct_id' => 'UV35'),
    COSID_UNIVISION_FORTYFIVE      => array('name' => COSID_UNIVISION_FORTYFIVE_NAME,     'cost' => COSID_UNIVISION_FORTYFIVE_COST,     'subproduct_id' => 'UV45'),
    COSID_UNIVISION_FIFTY          => array('name' => COSID_UNIVISION_FIFTY_NAME,         'cost' => COSID_UNIVISION_FIFTY_COST,         'subproduct_id' => 'UV50'),
    COSID_UNIVISION_FIFTYFIVE      => array('name' => COSID_UNIVISION_FIFTYFIVE_NAME,     'cost' => COSID_UNIVISION_FIFTYFIVE_COST,     'subproduct_id' => 'UV55'),

    COSID_UNIVISION_UVSNINETEEN    => array('name' => COSID_UNIVISION_UVSNINETEEN_NAME,   'cost' => COSID_UNIVISION_UVSNINETEEN_COST,   'subproduct_id' => 'UVS19'),
    COSID_UNIVISION_UVSTWENTYFOUR  => array('name' => COSID_UNIVISION_UVSTWENTYFOUR_NAME, 'cost' => COSID_UNIVISION_UVSTWENTYFOUR_COST, 'subproduct_id' => 'UVS24'),
    COSID_UNIVISION_UVSTWENTYNINE  => array('name' => COSID_UNIVISION_UVSTWENTYNINE_NAME, 'cost' => COSID_UNIVISION_UVSTWENTYNINE_COST, 'subproduct_id' => 'UVS29'),
    COSID_UNIVISION_UVSTHIRTYFOUR  => array('name' => COSID_UNIVISION_UVSTHIRTYFOUR_NAME, 'cost' => COSID_UNIVISION_UVSTHIRTYFOUR_COST, 'subproduct_id' => 'UVS34'),
    COSID_UNIVISION_UVSTHIRTYNINE  => array('name' => COSID_UNIVISION_UVSTHIRTYNINE_NAME, 'cost' => COSID_UNIVISION_UVSTHIRTYNINE_COST, 'subproduct_id' => 'UVS39'),
    COSID_UNIVISION_UVSFORTYFOUR   => array('name' => COSID_UNIVISION_UVSFORTYFOUR_NAME,  'cost' => COSID_UNIVISION_UVSFORTYFOUR_COST,  'subproduct_id' => 'UVS44'),
    COSID_UNIVISION_UVSFORTYFIVE   => array('name' => COSID_UNIVISION_UVSFORTYFIVE_NAME,  'cost' => COSID_UNIVISION_UVSFORTYFIVE_COST,  'subproduct_id' => 'UVS45'),
    COSID_UNIVISION_UVSFIFTYFOUR   => array('name' => COSID_UNIVISION_UVSFIFTYFOUR_NAME,  'cost' => COSID_UNIVISION_UVSFIFTYFOUR_COST,  'subproduct_id' => 'UVS54'),

    COSID_ULTRA_STWENTY_NINE       => array('name' => COSID_ULTRA_STWENTY_NINE_NAME,      'cost' => COSID_ULTRA_STWENTY_NINE_COST,      'subproduct_id' => 'S29'),
    COSID_ULTRA_STHIRTY_NINE       => array('name' => COSID_ULTRA_STHIRTY_NINE_NAME,      'cost' => COSID_ULTRA_STHIRTY_NINE_COST,      'subproduct_id' => 'S39'),
    COSID_ULTRA_SFORTY_FOUR        => array('name' => COSID_ULTRA_SFORTY_FOUR_NAME,       'cost' => COSID_ULTRA_SFORTY_FOUR_COST,       'subproduct_id' => 'S44'),
    #COSID_ULTRA_SFORTY_FIVE        => array('name' => COSID_ULTRA_SFORTY_FIVE_NAME,       'cost' => COSID_ULTRA_SFORTY_FIVE_COST,       'subproduct_id' => 'S45'),
    COSID_ULTRA_SFIFTY_FOUR        => array('name' => COSID_ULTRA_SFIFTY_FOUR_NAME,       'cost' => COSID_ULTRA_SFIFTY_FOUR_COST,       'subproduct_id' => 'S54'),
  );
}


/**
 * secondaryValidation
 *
 * validate given parameters more extensively against DB and cfengine
 * the function will update or normalize the following successfully validated parameters
 * @param String ICCID: will be updated to Object (HTT_IVENTORY_SIM row)
 * @param Array bolt on names: will be changed to Array of bolt on definition
 * @return String first error message encountered or NULL on success
 */
function secondaryValidation(&$iccid = NULL, $zip = NULL, &$boltOns = NULL, $provider = NULL, $sku = NULL, $amount = NULL, $plan = NULL, $subproduct_id = NULL, $brand_short_name = NULL)
{
  // validate ICCID status only
  if ($iccid)
  {
    $iccid = luhnenize($iccid);

    // check given brand_short_name against BRAND_ID in HTT_INVENTORY_SIM
    if ( ! \Ultra\Lib\Util\validateICCIDBrand( $iccid , $brand_short_name ) )
      return 'ERR_API_INVALID_ARGUMENTS: ICCID provided does not correlate with ICCID brand';

    // validate status
    $status = get_ICCID_user_status($iccid, TRUE, $iccid);
    if ($status != 'VALID')
      return 'ERR_API_INVALID_SIM: The given ICCID cannot be activated';
  }

  // valide that zip code is in coverage
  if ($zip)
  {
    list($error, $code) = \Ultra\Lib\Util\validatorZipcode('zipcode', $zip, 'in_coverage');
    if ( ! empty($error))
      return $error;
  }

  // validate all bolt ons and replace them with their definitions
  if (count($boltOns))
  {
    $definitions = array();
    foreach ($boltOns as $boltOn)
      if ( ! empty($boltOn))
      {
        if ( ! $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo($boltOn))
          return "ERR_API_INVALID_ARGUMENTS: invalid bolt on $boltOn";
        $definitions[$boltOn] = $boltOnInfo;
      }
    $boltOns = $definitions;
  }

  // validate SKU against provider
  if ($provider && $sku)
    if ( ! verifyProviderSku($provider, $sku))
      return "ERR_API_INVALID_ARGUMENTS: invalid SKU $sku for provider $provider";

  // verify amount against SKU
  if ($provider && $sku && $amount)
    if ( ! verifyProviderSkuAmount($provider, $sku, $amount))
      return "ERR_API_INVALID_ARGUMENTS: invalid amount $amount for $provider SKU $sku";

  // validate plan name
  if ($plan)
  {
    $config = Ultra\UltraConfig\getUltraPlanConfiguration($plan);
    if ( ! $config['aka'])
      return "ERR_API_INVALID_ARGUMENTS: invalid plan $plan";

    // also validate that bolt ons are allowed on this plan
    if (count($boltOns))
    {
      $allowed = Ultra\UltraConfig\getBoltOnsByPlan($config['aka']);
      foreach ($boltOns as $name => $definition)
        if ( ! in_array($name, $allowed))
          return "ERR_API_INVALID_ARGUMENTS: bolt on $name cannot be used with plan $plan";
    }
  }

  // validate subproduct
  if ( ! $subproduct_info = Ultra\UltraConfig\getPaymentProviderSubproductInfo($subproduct_id))
    return "ERR_EPY_INVALID_SUBPRODUCT_ID: $subproduct_id is not a valid subproduct_id";

  // all parameters have been validated
  return NULL;
}


<?php

require_once 'classes/PHPUnitBase.php';

class WebPosPortInCustomerTest extends PHPUnitBase
{
  
  public function test__externalpayments__WebPosPortInCustomer()
  {
    // API setup
    list($test, $partner, $command) = explode('__', __FUNCTION__);
    $this->setOptions(array(
      'api'       => "{$partner}__{$command}",
      'bath'      => 'soap',
      'version'   => 1,
      'partner'   => 'webpospayments',
      'wsdl'      => 'partner-meta/webpospayments.wsdl',
      'debug'     => TRUE));
/*
    // all missing parameters
    $params = array();
    $result = $this->callApi($params);
    # print_r($result);
    $this->assertTrue( ! $result->success);

    // Port-In Requested
    $params = array(
      'partner_tag'           => 'tag',
      'request_epoch'         => time(),
      'iccid'                 => '101010101010101058',
      'zipcode'               => '11249',
      'provider_name'         => 'EPAY',
      'provider_trans_id'     => rand(111111111, 999999999),
      'port_account_number'   => '1234567890',
      'port_account_password' => '12345',
      'port_account_zipcode'  => '11211',
      'port_phone_number'     => '4243983036',
      'sku'                   => '00843788021269', // $19.00
      'load_amount'           => '1900',
      'plan_name'             => 'NINETEEN',
      'bolt_on_intl'          => 'IDDCA_6.25_5',
      'bolt_on_data'          => 'DATA_500_5',
      'subproduct_id'         => 'PQ19',
      'tmo_id'                => 'abc123-4',
      'brand'                 => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INTERNAL: {$params['port_phone_number']} is being ported", $result->errors);

    // Provisioned
    $params = array(
      'partner_tag'           => 'tag',
      'request_epoch'         => time(),
      'iccid'                 => '101010101010101058',
      'zipcode'               => '11249',
      'provider_name'         => 'EPAY',
      'provider_trans_id'     => rand(111111111, 999999999),
      'port_account_number'   => '1234567890',
      'port_account_password' => '12345',
      'port_account_zipcode'  => '11211',
      'port_phone_number'     => '6572897693',
      'sku'                   => '00843788021269', // $19.00
      'load_amount'           => '1900',
      'plan_name'             => 'NINETEEN',
      'bolt_on_intl'          => 'IDDCA_6.25_5',
      'bolt_on_data'          => 'DATA_500_5',
      'subproduct_id'         => 'PQ19',
      'tmo_id'                => 'abc123-4',
      'brand'                 => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INTERNAL: {$params['port_phone_number']} is provisioned and awaiting payment", $result->errors);

    // Neutral
    $params = array(
      'partner_tag'           => 'tag',
      'request_epoch'         => time(),
      'iccid'                 => '101010101010101058',
      'zipcode'               => '11249',
      'provider_name'         => 'EPAY',
      'provider_trans_id'     => rand(111111111, 999999999),
      'port_account_number'   => '1234567890',
      'port_account_password' => '12345',
      'port_account_zipcode'  => '11211',
      'port_phone_number'     => '3033508490',
      'sku'                   => '00843788021269', // $19.00
      'load_amount'           => '1900',
      'plan_name'             => 'NINETEEN',
      'bolt_on_intl'          => 'IDDCA_6.25_5',
      'bolt_on_data'          => 'DATA_500_5',
      'subproduct_id'         => 'PQ19',
      'tmo_id'                => 'abc123-4',
      'brand'                 => 'ULTRA');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INTERNAL: port denied due to customer state", $result->errors);

    // Active same plan/brand
    $params = array(
      'partner_tag'           => 'tag',
      'request_epoch'         => time(),
      'iccid'                 => '101010101010101058',
      'zipcode'               => '11249',
      'provider_name'         => 'EPAY',
      'provider_trans_id'     => rand(111111111, 999999999),
      'port_account_number'   => '1234567890',
      'port_account_password' => '12345',
      'port_account_zipcode'  => '11211',
      'port_phone_number'     => '3128718745',
      'sku'                   => '00843788021269', // $19.00
      'load_amount'           => '1900',
      'plan_name'             => 'NINETEEN',
      'bolt_on_intl'          => 'IDDCA_6.25_5',
      'bolt_on_data'          => 'DATA_500_5',
      'subproduct_id'         => 'PQ19',
      'brand'                 => 'ULTRA',
      'tmo_id'                => 'abc123-4');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INTERNAL: customer is already on plan {$params['plan_name']}", $result->errors);

    // different plan, same brand
    $params = array(
      'partner_tag'           => 'tag',
      'request_epoch'         => time(),
      'iccid'                 => '101010101010101058',
      'zipcode'               => '11249',
      'provider_name'         => 'EPAY',
      'provider_trans_id'     => rand(111111111, 999999999),
      'port_account_number'   => '1234567890',
      'port_account_password' => '12345',
      'port_account_zipcode'  => '11211',
      'port_phone_number'     => '6572476527',
      'sku'                   => '00843788021269', // $19.00
      'load_amount'           => '1900',
      'plan_name'             => 'NINETEEN',
      'bolt_on_intl'          => 'IDDCA_6.25_5',
      'bolt_on_data'          => 'DATA_500_5',
      'subproduct_id'         => 'PQ19',
      'brand'                 => 'ULTRA',
      'tmo_id'                => 'abc123-4');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);

    // wrong account
    $params = array(
      'partner_tag'           => 'tag',
      'request_epoch'         => time(),
      'iccid'                 => '101010101010101058',
      'zipcode'               => '11249',
      'provider_name'         => 'EPAY',
      'provider_trans_id'     => rand(111111111, 999999999),
      'port_account_number'   => '1234567890',
      'port_account_password' => '12345',
      'port_account_zipcode'  => '11211',
      'port_phone_number'     => '6572713998',
      'sku'                   => '00843788021269', // $19.00
      'load_amount'           => '1900',
      'plan_name'             => 'NINETEEN',
      'bolt_on_intl'          => 'IDDCA_6.25_5',
      'bolt_on_data'          => 'DATA_500_5',
      'subproduct_id'         => 'PQ19',
      'brand'                 => 'ULTRA',
      'tmo_id'                => 'abc123-4');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains("ERR_API_INTERNAL: invalid account", $result->errors);

    // MVNE un-portable
    $params = array(
      'partner_tag'           => 'tag',
      'request_epoch'         => time(),
      'iccid'                 => '101010101010101058',
      'zipcode'               => '11249',
      'provider_name'         => 'EPAY',
      'provider_trans_id'     => rand(111111111, 999999999),
      'port_account_number'   => '1234567890',
      'port_account_password' => '12345',
      'port_account_zipcode'  => '11211',
      'port_phone_number'     => '5555555555',
      'sku'                   => '00843788021269', // $19.00
      'load_amount'           => '1900',
      'plan_name'             => 'NINETEEN',
      'bolt_on_intl'          => 'IDDCA_6.25_5',
      'bolt_on_data'          => 'DATA_500_5',
      'subproduct_id'         => 'PQ19',
      'brand'                 => 'ULTRA',
      'tmo_id'                => 'abc123-4');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: The given MSISDN is not eligible to port in', $result->errors);
*/
    // intra brand port
    $params = array(
      'partner_tag'           => 'tag',
      'request_epoch'         => time(),
      'iccid'                 => '101010120151120127',
      'zipcode'               => '11249',
      'provider_name'         => 'EPAY',
      'provider_trans_id'     => rand(111111111, 999999999),
      'port_phone_number'     => '7203979824',
      'port_account_number'   => '836053533187',
      'port_account_password' => '12345',
      'port_account_zipcode'  => '11211',
      'sku'                   => '00843788035495',
      'load_amount'           => '3000',
      'plan_name'             => 'UVTHIRTY',
      'bolt_on_intl'          => 'IDDCA_6.25_5',
      'bolt_on_data'          => 'DATA_500_5',
      'subproduct_id'         => 'PQ30',
      'brand'                 => 'UNIVISION',
      'tmo_id'                => 'abc123-4');
    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue( ! $result->success);
  }
}

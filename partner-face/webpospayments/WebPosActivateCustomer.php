<?php

/**
 * externalpayments__WebPosActivateCustomer
 * activate given ICCID for TMO dealer via ePay POS
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Epay+WebPos+Integration+to+Support+EPP+Program
 * @see https://issues.hometowntelecom.com:8443/browse/API-244
 */
function externalpayments__WebPosActivateCustomer($partner_tag, $request_epoch, $iccid, $zipcode, $bolt_on_data, $bolt_on_intl, $language, $provider_name, $provider_trans_id, $sku, $load_amount, $plan_name, $subproduct_id, $tmo_id, $brand)
{
  // globals
  global $p; // partner definition
  global $always_succeed;

  // API return values
  $args = func_get_args();
  $result = [
    'success'                 => FALSE,
    'errors'                  => array(),
    'warnings'                => array(),
    'ultra_payment_trans_id'  => NULL,
    'provider_trans_id'       => NULL,
    'iccid'                   => NULL
  ];
  $boltOns = array($bolt_on_data, $bolt_on_intl);

  try
  {
    if ($always_succeed)
    {
      $result['success'] = TRUE;
      $result['warnings'][] = 'ERR_API_INTERNAL: always_succeed';
      throw new Exception();
    }

    // validate parameters according to JSON definition
    $errors = validate_params($p, __FUNCTION__, $args);
    if (count($errors))
    {
      $result['errors'] = $errors;
      throw new Exception();
    }

    // get dealer info and validate if active
    if ( ! $dealer = Ultra\Lib\DB\DealerPortal\getDealerByTmoCode($tmo_id))
      throw new Exception("ERR_API_INVALID_ARGUMENTS: No celluphone dealer found for TMO code $tmo_id");
    if ( ! $dealer->ACTIVEFLAG)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: Dealer is not active');

    // perform more extensive parameter valdiation
    teldata_change_db();

    $cache = new Ultra\Lib\Util\Redis\WebPos;

    if ( ! $cache->lockICCID($iccid, 60))
      throw new Exception('ERR_API_INVALID_ARGUMENTS: This ICCID is currently being processed');

    $customer = get_ultra_customer_from_iccid($iccid, array('CUSTOMER_ID'));
    if ($customer)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: Customer with this ICCID already exists');

    if ($error = secondaryValidation($iccid, $zipcode, $boltOns, $provider_name, $sku, $load_amount, $plan_name, $subproduct_id, $brand))
      throw new Exception($error);
    if ( ! mvneCanActivate($iccid->ICCID_FULL))
      throw new Exception("ERR_API_INVALID_ARGUMENTS: ICCID {$iccid->ICCID_FULL} did not validate");

    // check if this SIM is already in the queue (duplicate API call)
    $queue = ultra_webpos_actions_select(array('ICCID' => $iccid->ICCID_FULL, 'STATUS' => 'OPEN'));
    if ( ! empty($queue))
      throw new Exception('ERR_API_INVALID_ARGUMENTS: This SIM has been already used');

    // prepare subscribers initialization parameters
    $cos = get_cos_id_from_plan($plan_name);
    $planCost = get_plan_cost_by_cos_id($cos);
    $boltOnsCost = array_sum(collectPropertyValues($boltOns, 'cost'));
    $language = empty($language) ? 'EN' : $language; // v1 JSON default is broken
    logInfo("plan: $plan_name, COS: $cos, plan cost: $planCost, bolt ons cost: $boltOnsCost, language: $language");
    $params = [
      'preferred_language'    => $language,
      'cos_id'                => get_cos_id_from_plan(PLAN_STANDBY),
      'activation_cos_id'     => $cos,
      'postal_code'           => $zipcode,
      'country'               => 'USA',
      'plan_state'            => 'Neutral',
      'plan_started'          => 'NULL',
      'plan_expires'          => 'NULL',
      'customer_source'       => 'WEBPOS',
      'current_iccid'         => $iccid->ICCID_NUMBER,
      'current_iccid_full'    => $iccid->ICCID_FULL,
      'promised_amount'       => $planCost + $boltOnsCost,
      'dealer'                => $dealer->DEALERSITEID
    ];

    // add master and sub distributor if the latter is present
    if ( ! empty($dealer->PARENTMASTERID) && $dealer->MASTERID != $dealer->PARENTMASTERID)
    {
      $params['distributor'] = $dealer->MASTERID;
      $params['masteragent'] = $dealer->PARENTMASTERID;
    }
    else
    {
      $params['distributor'] = NULL;
      $params['masteragent'] = $dealer->MASTERID;
    }

    // create subscriber in Neutral state
    $response = create_ultra_customer_db_transaction($params);
    if (empty($response['customer_id']))
      throw new Exception('ERR_API_INTERNAL: An unexpected database error has occurred');
     $customer = $response['customer'];

    // configure bolt ons
    if (count($boltOns))
    {
      $config = array_combine(collectPropertyValues($boltOns, 'option_attribute'), collectPropertyValues($boltOns, 'cost'));
      $response = update_bolt_ons_values_to_customer_options($customer->CUSTOMER_ID, array(), $config, 'DPORTAL');
      if ($response->is_failure())
        throw new Exception('ERR_API_INTERNAL: DB error (1)');
    }

    $subproduct_id = 'PQ' . preg_replace("/[^0-9,.]/", "", $subproduct_id);

    // create new row in ULTRA.WEBPOS_ACTIONS
    $webposActionsInsertParams = [
      'CUSTOMER_ID'           => $customer->CUSTOMER_ID,
      'STATUS'                => 'OPEN',
      'CREATED'               => 'NOW',
      'TYPE'                  => 1, // 1 = Activation; 2 = Port-In
      'TRANSACTION_ID'        => $provider_trans_id,
      'SUBPRODUCT_ID'         => $subproduct_id,
      'PLAN_NAME'             => $plan_name,
      'AMOUNT'                => $load_amount / 100,
      'TMOBILE_CODE'          => $tmo_id,
      'PROVIDER_NAME'         => $provider_name,
      'BOLT_ON_DATA'          => $bolt_on_data,
      'BOLT_ON_INTL'          => $bolt_on_intl,
      'ICCID'                 => $iccid->ICCID_FULL,
      'ZIPCODE'               => $zipcode,
      'LANGUAGE'              => $language
    ];

    if ( ! $uuid = ultra_webpos_actions_insert($webposActionsInsertParams))
      throw new Exception('ERR_API_INTERNAL: DB error (2)');

    // set activation attribution information to be stored into HTT_ACTIVATION_LOG at activation time
    set_redis_provisioning_values(
      $customer->CUSTOMER_ID,
      $params['masteragent'],
      $dealer->DEALERSITEID,
      $params['distributor'],
      $dealer->DEALERSITEID,
      0,
      NULL
    );

    // add transaction to cache with the proper delay
    $cache->addUUID($uuid, time() + Ultra\UltraConfig\getPaymentProviderDelay($provider_name));

    // success
    $result['success'] = TRUE;
    $result['iccid'] = $iccid->ICCID_FULL;
    $result['ultra_payment_trans_id']  = $uuid;
    $result['provider_trans_id'] = $provider_trans_id;
  }

  catch(Exception $e)
  {
    // in certain cases (such as warning or multiple errors) we do not have an exception message
    if ($message = $e->getMessage())
    {
      logError($message);
      $result['errors'][] = $message;
    }
  }

  return flexi_encode(fill_return($p, __FUNCTION__, $args, $result));
}


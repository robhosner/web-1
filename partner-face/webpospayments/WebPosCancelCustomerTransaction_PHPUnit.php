<?php

require_once 'classes/PHPUnitBase.php';
require_once 'test/api/test_api_include.php';

class WebPosCancelCustomerTransactionTest extends PHPUnitBase
{
  public function setUp()
  {
    parent::setUp();
    $this->createDummyData();
  }

  private function createDummyData()
  {
    teldata_change_db();

    // this is an ``always succeed`` test ICCID
    $this->testBogusICCID = '1010101010101010903';

    // release test ICCID from ULTRA.WEBPOS_ACTIONS
    $sql = "UPDATE ULTRA.WEBPOS_ACTIONS SET ICCID = '0' WHERE ICCID = '".$this->testBogusICCID."'";

    logged_mssql_query($sql);

    // release test ICCID from HTT_CUSTOMERS_OVERLAY_ULTRA
    $sql = "UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET CURRENT_ICCID = NULL , CURRENT_ICCID_FULL = NULL WHERE CURRENT_ICCID_FULL = '".$this->testBogusICCID."'";

    logged_mssql_query($sql);

    // release test ICCID from HTT_INVENTORY_SIM
    $sql = "UPDATE HTT_INVENTORY_SIM SET SIM_ACTIVATED = 0 , CUSTOMER_ID = NULL , LAST_TRANSITION_UUID = NULL , RESERVATION_TIME = NULL WHERE ICCID_FULL = '".$this->testBogusICCID."'";

    logged_mssql_query($sql);

    $time = time();

    $this->dummyUUID          = 'webpos_test_' . $time;
    $this->dummyTransactionID = 'webpos_trans_' . $time;

    $status = 'OPEN';

    // add an OPEN WebPos Action
    $sql = "
    INSERT INTO ULTRA.WEBPOS_ACTIONS (
      CUSTOMER_ID, UUID, STATUS, CREATED, TYPE, TRANSACTION_ID, SUBPRODUCT_ID, PLAN_NAME, AMOUNT, TMOBILE_CODE, PROVIDER_NAME, ICCID, ZIPCODE, LANGUAGE
    ) VALUES (
      31,
      '" . $this->dummyUUID . "',
      '" . $status . "',
      GETUTCDATE(),
      1,
      '" . $this->dummyTransactionID . "',
      'L19',
      'NINETEEN',
      19,
      'test_tmo_code',
      'test_provider',
      '1010101010101010101',
      '92683',
      'EN'
    )";

    logged_mssql_query($sql);

    $this->dummyDoneUUID          = 'webpos_test_' . ($time + 5);
    $this->dummyDoneTransactionID = 'webpos_trans_' . ($time + 5);

    $status = 'DONE';

    // add a DONE WebPos Action
    $sql = "
    INSERT INTO ULTRA.WEBPOS_ACTIONS (
      CUSTOMER_ID, UUID, STATUS, CREATED, TYPE, TRANSACTION_ID, SUBPRODUCT_ID, PLAN_NAME, AMOUNT, TMOBILE_CODE, PROVIDER_NAME, ICCID, ZIPCODE, LANGUAGE
    ) VALUES (
      31,
      '" . $this->dummyDoneUUID . "',
      '" . $status . "',
      GETUTCDATE(),
      1,
      '" . $this->dummyDoneTransactionID . "',
      'L19',
      'NINETEEN',
      19,
      'test_tmo_code',
      'test_provider',
      '1010101010101010101',
      '92683',
      'EN'
    )";

    logged_mssql_query($sql);

  }

  public function tearDown()
  {
    $sql = 'DELETE FROM ULTRA.WEBPOS_ACTIONS WHERE UUID = \'' . $this->dummyUUID . '\'';
    logged_mssql_query($sql);

    $sql = 'DELETE FROM ULTRA.WEBPOS_ACTIONS WHERE UUID = \'' . $this->dummyDoneUUID . '\'';
    logged_mssql_query($sql);
  }

  /**
   * test activation and cancellation afterwards
   */
  public function test_activation_and_cancellation()
  {
    $this->setOptions(
      array(
        'api'       => "externalpayments__WebPosActivateCustomer",
        'bath'      => 'soap',
        'version'   => 1,
        'partner'   => 'webpospayments',
        'wsdl'      => 'partner-meta/webpospayments.wsdl'
      )
    );

    $params = array(
      'request_epoch'          => time(),
      'iccid'                  => $this->testBogusICCID,
      'zipcode'                => '11249',
      'provider_name'          => 'EPAY',
      'provider_trans_id'      => 'Q'.$this->dummyTransactionID,
      'ultra_payment_trans_id' => 'Q'.$this->dummyUUID,
      'sku'                    => '00843788021269', // $19.00
      'load_amount'            => '1900',
      'plan_name'              => 'NINETEEN',
      'subproduct_id'          => 'PQ19',
      'tmo_id'                 => 'abc123-4'
    );

    // brand has no matches
    $params['brand'] = 'GOOGIE';
    $result = $this->callApi($params);
    print_r($result);
    $this->assertFalse( ! ! $result->success);

    // incorrect brand
    $params['brand'] = 'UNIVISION';
    $result = $this->callApi($params);
    print_r($result);
    $this->assertFalse( ! ! $result->success);

    // correct brand
    $params['brand'] = 'ULTRA';
    $result = $this->callApi($params);
    print_r($result);
    $this->assertNotEmpty($result->ultra_payment_trans_id);

    sleep(2);

    $this->setOptions(array('api' => "externalpayments__WebPosCancelCustomerTransaction"));

    $params = array(
      'partner_tag'            => 'tag',
      'request_epoch'          => time(),
      'iccid'                  => $this->testBogusICCID,
      'cancel_type'            => 'VOID',
      'provider_trans_id'      => 'Q'.$this->dummyTransactionID,
      'ultra_payment_trans_id' => $result->ultra_payment_trans_id
    );

    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
  }

  /**
   * test API externalpayments__WebPosCancelCustomerTransaction
   */
  public function test__externalpayments__WebPosCancelCustomerTransaction()
  {
    // API setup
    list($test, $partner, $api) = explode('__', __FUNCTION__);
    $this->setOptions(array(
      'api'       => "{$partner}__{$api}",
      'bath'      => 'rest',
      'version'   => 1)
    );

    // iccid mismatch
    $this->setOptions(array('partner' => 'univision'));
    $params = array(
      'partner_tag'            => 'tag',
      'request_epoch'          => time(),
      'iccid'                  => '1010101010101010101',
      'cancel_type'            => 'VOID',
      'provider_trans_id'      => 1234567, // $this->dummyTransactionID,
      'ultra_payment_trans_id' => 1234567 // $this->dummyUUID
    );
    $result = $this->callApi($params);
    print_r($result);
    $this->assertFalse($result->success);

    // provider_trans_id mismatch
    $params = array(
      'partner_tag'            => 'tag',
      'request_epoch'          => time(),
      'iccid'                  => '1010101010101010101',
      'cancel_type'            => 'VOID',
      'provider_trans_id'      => 'xxx',
      'ultra_payment_trans_id' => $this->dummyUUID
    );

    $result = $this->callApi($params);
    print_r($result);
    $this->assertFalse($result->success);

    // is not open
    $params = array(
      'partner_tag'            => 'tag',
      'request_epoch'          => time(),
      'iccid'                  => '1010101010101010101',
      'cancel_type'            => 'VOID',
      'provider_trans_id'      => $this->dummyDoneTransactionID,
      'ultra_payment_trans_id' => $this->dummyDoneUUID
    );

    $result = $this->callApi($params);
    print_r($result);
    $this->assertFalse($result->success);

    // success
    $params = array(
      'partner_tag'            => 'tag',
      'request_epoch'          => time(),
      'iccid'                  => '1010101010101010101',
      'cancel_type'            => 'VOID',
      'provider_trans_id'      => $this->dummyTransactionID,
      'ultra_payment_trans_id' => $this->dummyUUID
    );

    $result = $this->callApi($params);
    print_r($result);
    $this->assertTrue($result->success);
  }
}


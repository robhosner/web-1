<?php

// Change State
function internal__ChangeState($partner_tag, $customer_id, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__ChangeState",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__ChangeState", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__ChangeState",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  teldata_change_db();

  $context = array(
    'customer_id' => $customer_id
    );

  /* note we know the errors array is empty here, so we don't need to copy it into $change! */
  $change = change_state($context, $resolve_now, $plan, $state_name, $dry_run, $max_path_depth);

  global $out;
  return flexi_encode(array_merge($out,
                                  fill_return($p,
                                              "internal__ChangeState",
                                              func_get_args(),
                                              $change)));
}

?>

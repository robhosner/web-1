<?php


include_once('lib/state_machine/aspider.php');


// internal__QueryMSISDN
function internal__QueryMSISDN($partner_tag, $customer_id, $msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success  = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "internal__QueryMSISDN",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = array("ERR_API_INTERNAL: this API has been disabled");

  return flexi_encode(fill_return($p,
                                  "internal__QueryMSISDN",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));
}

?>

<?php

// Get customer state
function internal__GetState($partner_tag, $customer_id)
{
  global $p;
  global $mock;
  global $always_succeed;
  $success = FALSE;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__GetState",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__GetState", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__GetState",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors" => $errors)));
  }

  teldata_change_db();

  $state = internal_func_get_state_from_customer_id($customer_id);

  // check further if state is invalid: this should only happen if DB is inconsistent
  if ( ! $state)
  {
    // check if customer exists
    $sql = customers_select_query(array( 'customer_id' => $customer_id ));
    if ( ! mssql_has_rows( $sql ) )
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: customer does not exist';

    // also check if Ultra customer exists
    $sql = htt_customers_overlay_ultra_select_query(array( 'customer_id' => $customer_id ));
    if ( ! mssql_has_rows( $sql ) )
      $errors[] = 'ERR_API_INVALID_ARGUMENTS: Ultra customer does not exist';
  }
  else
    $success = TRUE;

  return flexi_encode(fill_return($p,
                                  "internal__GetState",
                                  func_get_args(),
                                  array(
                                    "success" => $success,
                                    "errors" => $errors,
                                    'state_valid' => ! empty($state['print']),
                                    'plan_name' => empty($state['plan']) ? NULL : $state['plan'],
                                    'plan_desc_name' => empty($state['info']['name']) ? NULL : $state['info']['name'],
                                    'state_name' => empty($state['state']) ? NULL : $state['state'],
                                    'state_desc_name' => empty($state['print']) ? NULL : $state['print'],
                                    )));
}

?>

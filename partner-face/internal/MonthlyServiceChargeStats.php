<?php

include_once('db/monthly_service_charge_log.php');

// Monthly Service Charge statistics
function internal__MonthlyServiceChargeStats($partner_tag, $stats_date)
{
  global $p;
  global $mock;
  global $always_succeed;

  $success = FALSE;
  $stats   = '';

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "internal__MonthlyServiceChargeStats",
                                    func_get_args(),
                                    array("success" => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "internal__MonthlyServiceChargeStats", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "internal__MonthlyServiceChargeStats",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors" => $errors)));
  }

  teldata_change_db();

  $result = monthly_service_charge_log_stats($stats_date);

  $errors  = $result->get_errors();
  $success = ! ! $result->is_success();

  if ( $success )
  {
    $stats = json_encode($result->data_array['monthly_service_charge_log_stats']);
  }

  return flexi_encode(fill_return($p,
                                  "internal__MonthlyServiceChargeStats",
                                  func_get_args(),
                                  array("success" => $success,
                                        "stats"   => $stats,
                                        "errors"  => $errors)));
}

?>

<?php

require_once 'classes/Flex.php';

// Cancels a customer due to Fraud suspicion.
function internal__CancelFraudCustomer($partner_tag, $customer_id, $msisdn, $reason, $type)
{
  global $p;
  global $mock;
  global $always_succeed;

  $warnings = array();

  $success = FALSE;

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "internal__CancelFraudCustomer",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "internal__CancelFraudCustomer", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "internal__CancelFraudCustomer",
                                    func_get_args(),
                                    array("success" => $success,
                                          "errors"  => $errors)));

  teldata_change_db();

  try
  {
    if ( ! $msisdn )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: msisdn is a required parameter");

    $customer = get_customer_from_customer_id( $customer_id );

    if ( ! $customer )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: customer not found");

    if ( $msisdn != $customer->current_mobile_number )
      throw new Exception("ERR_API_INVALID_ARGUMENTS: customer_id and msisdn do not match");

    // Cancel Customer

    $context = array(
      'customer_id' => $customer_id
    );

    $resolve_now     = 1;
    $max_path_depth  = 1;
    $dry_run         = 0;

    // PROD-16: determine correct transition for cancellation; this fix does not cover all possible states/transitions
    switch ($customer->plan_state)
    {
      case 'Active':
        $transition_name = ($type == 'FRAUD') ? 'Cancel Active due to fraud' : 'Cancel Active';
        break;
      case 'Provisioned':
        $transition_name = 'Cancel Inactive';
        break;
      case 'Suspended':
        $transition_name = ($type == 'FRAUD') ? 'Cancel Suspended due to fraud' : 'Cancel Suspended';
        break;
      default:
        throw new Exception('ERR_API_INTERNAL: could not cancel customer due to current state');
    }

    $result = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    dlog('',"change_state : %s",$result);

    if ( ! $result['success'] )
      throw new Exception("ERR_API_INTERNAL: could not cancel customer");

    // add row to HTT_CANCELLATION_REASONS
    $check = htt_cancellation_reasons_add(
      array(
        'customer_id' => $customer->CUSTOMER_ID,
        'reason'      => $reason,
        'type'        => $type,
        'msisdn'      => $customer->current_mobile_number,
        'iccid'       => $customer->CURRENT_ICCID_FULL,
        'status'      => $customer->plan_state,
        'agent'       => 'Fraud GUI'
      )
    );

    if ( ! $check )
      $warnings[] = 'Failed to add to HTT_CANCELLATION_REASONS';

    // add row to HTT_CUSTOMERS_NOTES
    $sql = htt_customers_notes_insert_query(
      array(
        'customer_id'  => $customer_id,
        'type'         => $type,
        'source'       => 'webtool',
        'user'         => $_SERVER['PHP_AUTH_USER'],
        'contents'     => $reason
      )
    );

    if ( ! is_mssql_successful(logged_mssql_query($sql)) )
      $warnings[] = "ERR_API_INTERNAL: could not insert into HTT_CUSTOMERS_NOTES";

    $result = \Ultra\Lib\Flex::removeFromFamily($customer->CUSTOMER_ID);
    if ($result->is_failure())
      \logError("ESCALATION FLEX error removing cancelled customer from family {$customer->CUSTOMER_ID}");

    $success = TRUE;
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }

  return flexi_encode(fill_return($p,
                                  "internal__CancelFraudCustomer",
                                  func_get_args(),
                                  array("success"  => $success,
                                        "warnings" => $warnings,
                                        "errors"   => $errors)));
}


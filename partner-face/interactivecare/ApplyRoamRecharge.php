<?php

function interactivecare__ApplyRoamRecharge($partner_tag, $customer_id, $roam_soc_id)
{
  $retval = json_decode(interactivecare__doApplyRoamRecharge($partner_tag, $customer_id, $roam_soc_id), TRUE);

  $clean_error = count($retval['errors']);

  if (count($retval['errors']))
    $clean_error = preg_replace ('/ERR_API_[^:]+: /', '', $retval['errors'][0]);

  $retval['clean_error'] = $clean_error;

  return json_encode($retval);
}

function interactivecare__doApplyRoamRecharge($partner_tag, $customer_id, $roam_soc_id)
{
  global $p;
  global $mock;
  global $always_succeed;

  $apiResult = new \VersionOneResult($p, 'interactivecare__ApplyRoamRecharge');
  $apiResult->setArgs(func_get_args(), $mock);

  if ($apiResult->hasErrors())
    return $apiResult->getJSON();
  
  try
  {
    teldata_change_db();

    $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo($roam_soc_id);
    if ( ! $boltOnInfo )
      throw new Exception('ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid');

    dlog('', 'boltOnInfo = %s', $boltOnInfo);

    $customer = get_ultra_customer_from_customer_id($customer_id, [
      'CUSTOMER_ID',
      'current_mobile_number',
      'plan_state',
      'BRAND_ID'
    ]);

    $account = get_account_from_customer_id($customer->CUSTOMER_ID, [
      'COS_ID',
      'BALANCE'
    ]);

    if ( ! $customer || ! $account)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: invalid customer id');

    if ($customer->plan_state != STATE_ACTIVE)
      throw new Exception('ERR_API_INVALID_ARGUMENTS: customer state is not active');

    $redis = new \Ultra\Lib\Util\Redis;

    if ( get_bolt_on_semaphore( $redis , $customer->CUSTOMER_ID, $boltOnInfo['product'] ) )
      throw new Exception('ERR_API_INTERNAL: a bolt on has been processed less than 15 minutes ago.');

    if ( ! \Ultra\Lib\BoltOn\validateImmediateBoltOn($account->COS_ID, $roam_soc_id))
      throw new Exception('ERR_API_INVALID_ARGUMENTS: invalid operation for current customer plan');

    if ( $boltOnInfo['cost'] > $account->BALANCE )
      throw new Exception('ERR_API_INTERNAL: not enough money to perform this operation');

    list( $error , $error_code ) = \Ultra\Lib\BoltOn\addBoltOnImmediate($customer_id, $boltOnInfo, 'SPEND', __FUNCTION__, NULL);
    if ( $error )
      throw new Exception($error);

    set_bolt_on_semaphore($redis, $customer_id, $boltOnInfo['product']);

    $apiResult->succeed();
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    $apiResult->addError($error);
  }

  return $apiResult->getJSON();
}

<?php

/**
 * Handle INTL_FIXED campaign
 *
 * if now > '8-31-2014', send SMS to subscriber : 'Sorry but we cannot perform this request because the program is no longer active.' and end.
 * REMOVED: If customer_id is > 757532 (to be changed.), send SMS to subscriber : 'You are not eligible for this promotion.' and end.
 * If customer_id is not in ULTRA.INTL_FIXED, send SMS to subscriber : 'You are not eligible for this promotion.' and end.
 * If customer already has the HTT_BILLING_HISTORY entry inserted below, send SMS to subscriber : 'We are sorry but this offer can only be redeemed once. Our records show that we already credited your account. ' and end.
 * Else :
 * Add $2.50 to the subscribers wallet. Pass '$2.50 INTL Fixed Wallet Credit' into the description field of the Add Balance
 * send SMS to subscriber : 'Thanks for your loyalty to Ultra. Your Ultra Wallet has been credited with $2.50'
 */
function interactivecare__ApplyInternationalFixedPromo($partner_tag, $msisdn)
{
  global $p;
  global $mock;
  global $always_succeed;
  global $request_id;

  $warnings        = array();
  $success         = FALSE;
  #$max_customer_id = 757532;
  $date_time_limit = 1408473481; // Sat, 06 Sep 2014 00:00:00 GMT
  $reason          = '$2.50 INTL Fixed Wallet Credit';

  if ($always_succeed)
    return flexi_encode(fill_return($p,
                                    "interactivecare__ApplyInternationalFixedPromo",
                                    func_get_args(),
                                    array('success'  => TRUE,
                                          'warnings' => array("ERR_API_INTERNAL: always_succeed"))));

  $errors = validate_params($p, "interactivecare__ApplyInternationalFixedPromo", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
    return flexi_encode(fill_return($p,
                                  "interactivecare__ApplyInternationalFixedPromo",
                                    func_get_args(),
                                    array('success' => $success,
                                          'errors'  => $errors)));

  try
  {
    teldata_change_db(); // connect to the DB

    $customer = get_customer_from_msisdn($msisdn);

    if ( ! $customer )
      throw new \Exception("ERR_API_INTERNAL: MSISDN not found.");

    if ( $customer->plan_state == 'Cancelled' )
      throw new \Exception("ERR_API_INTERNAL: Invalid customer state.");

    $sql = htt_billing_history_select_query(
      array(
        'customer_id'       => $customer->CUSTOMER_ID,
        'entry_type'        => 'LOAD',
        'description'       => $reason
      )
    );

    $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( ( is_array($query_result) ) && count($query_result) > 0 )
    {
      // If customer already has the HTT_BILLING_HISTORY row, send SMS to subscriber: 'We are sorry but this offer can only be redeemed once. Our records show that we already credited your account.'

      $warnings[] = 'We are sorry but this offer can only be redeemed once. Our records show that we already credited your account.';

      $result = funcSendExemptCustomerSMSPromotionBlock(
        array(
          'customer' => $customer
        )
      );

      if ( ! $result['success'] )
        $warnings[] = 'ERR_API_INTERNAL: SMS delivery error';
    }
// if ( $customer->CUSTOMER_ID > $max_customer_id )
    elseif( ! ultra_intl_fixed_customer_allowed( $customer->CUSTOMER_ID ) )
    {
      // send SMS to subscriber : 'You are not eligible for this promotion.'

      $warnings[] = 'You are not eligible for this promotion.';

      $result = funcSendExemptCustomerSMSPromotionNotEligible(
        array(
          'customer' => $customer
        )
      );

      if ( ! $result['success'] )
        $warnings[] = 'ERR_API_INTERNAL: SMS delivery error';
    }
    elseif( $date_time_limit < time() )
    {
      // if now > '8-31-2014', send SMS to subscriber : 'Sorry but we cannot perform this request because the program is no longer active.'

      $warnings[] = 'Sorry but we cannot perform this request because the program is no longer active.';

      $result = funcSendExemptCustomerSMSPromotionInactive(
        array(
          'customer' => $customer
        )
      );

      if ( ! $result['success'] )
        $warnings[] = 'ERR_API_INTERNAL: SMS delivery error';
    }
    else
    {
      // Add $2.50 to the subscribers wallet. Pass '$2.50 INTL Fixed Wallet Credit' into the description field of the Add Balance

      $return = func_courtesy_add_balance(
        array(
          'reason'      => $reason,
          'terminal_id' => '0',
          'reference'   => $request_id,
          'amount'      => 2.5,
          'customer'    => $customer,
          'detail'      => __FUNCTION__
        )
      );

      dlog('',"func_courtesy_add_balance result = %s",$return);

      if ( count($return['errors']) )
        throw new \Exception("ERR_API_INTERNAL: DB error.");

      // send SMS to subscriber : 'Thanks for your loyalty to Ultra. Your Ultra Wallet has been credited with $2.50'
      $result = funcSendExemptCustomerSMSPromotionCredit(
        array(
          'customer' => $customer,
          'amount'   => '2.50'
        )
      );

      if ( ! $result['success'] )
        $warnings[] = 'ERR_API_INTERNAL: SMS delivery error';
    }

    $success = TRUE;
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());
    $errors[] = $e->getMessage();
  }

  return flexi_encode(fill_return($p,
                                  "interactivecare__ApplyInternationalFixedPromo",
                                  func_get_args(),
                                  array('success'  => $success,
                                        'warnings' => $warnings,
                                        'errors'   => $errors)));
}

?>

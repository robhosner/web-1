<?php

define('DEFAULT_NAMESPACE', 'Ultra');

// Amdocs / MW

define('PRIMARY_WHOLESALE_PLAN',       'W-PRIMARY');
define('SECONDARY_WHOLESALE_PLAN',     'W-SECONDARY');
define('MINT_WHOLESALE_PLAN',          'W-MINT');
define('UV_PRIMARY_WHOLESALE_PLAN',    'W-UV-PRIMARY');
define('UV_SECONDARY_WHOLESALE_PLAN',  'W-UV-SECONDARY');
define('MRC_WHOLESALE_PLAN',           'W-MRC');
define('DEFAULT_ACC_DATA_ADD_ON',      'A-DATA-BLK');
define('DEFAULT_ACC_MINT_DATA_ADD_ON', 'A-DATA-MINT');
define('MINT_DATA_ADD_ON_1',           'A-DATA-MINT-1');
define('MINT_DATA_ADD_ON_3',           'A-DATA-MINT-3');
define('VOICEMAIL_ENGLISH',            'V-ENGLISH');
define('VOICEMAIL_SPANISH',            'V-SPANISH');
define('RETAIL_PLAN_LIMITED',          '12007');
define('RETAIL_PLAN_UNLIMITED',        '12006');
define('B_VOICE_FEATURE_ID',           '12701');
define('B_SMS_FEATURE_ID',             '12702');

define('ACC_OPTION_ADDRESS_E911', 'ACC.ADDRESS_E911');
define('ACC_WIFI_CALLING_ENABLED', 'ACC.WIFI_CALLING_ENABLED');

// plan states

define('STATE_ACTIVE',            'Active');
define('STATE_CANCELLED',         'Cancelled');
define('STATE_NEUTRAL',           'Neutral');
define('STATE_PORT_IN_DENIED',    'Port-In Denied');
define('STATE_PORT_IN_REQUESTED', 'Port-In Requested');
define('STATE_PRE_FUNDED',        'Pre-Funded');
define('STATE_PROMO_UNUSED',      'Promo Unused');
define('STATE_PROVISIONED',       'Provisioned');
define('STATE_SUSPENDED',         'Suspended');

define('FINAL_STATE_COMPLETE', 'Completed'); // MVNO-2302

define ('DEFAULT_PROVISIONING_DEALER_ID', 23); // MVNO-2695: default CRM/IVR activating dealer

define('PLAN_L19', 'NINETEEN');
define('PLAN_L24', 'TWENTY_FOUR');
define('PLAN_L29', 'TWENTY_NINE');
define('PLAN_M29', 'MULTI_TWENTY_NINE');
define('PLAN_L34', 'THIRTY_FOUR');
define('PLAN_L39', 'THIRTY_NINE');
define('PLAN_D39', 'DTHIRTY_NINE');
define('PLAN_L44', 'FORTY_FOUR');
define('PLAN_L49', 'FORTY_NINE');
define('PLAN_L59', 'FIFTY_NINE');
define('PLAN_STANDBY', 'STANDBY');

define('PLAN_S39', 'STHIRTY_NINE');
define('PLAN_S45', 'SFORTY_FIVE');
define('PLAN_S49', 'SFORTY_NINE');
define('PLAN_S54', 'SFIFTY_FOUR');

define('PLAN_A19', 'ANINETEEN');
define('PLAN_A29', 'ATWENTY_NINE');
define('PLAN_A39', 'ATHIRTY_NINE');
define('PLAN_A49', 'AFORTY_NINE');
define('PLAN_B19', 'BNINETEEN');
define('PLAN_B29', 'BTWENTY_NINE');
define('PLAN_B39', 'BTHIRTY_NINE');
define('PLAN_B49', 'BFORTY_NINE');

define('PLAN_UV20', 'UVTWENTY');
define('PLAN_UV30', 'UVTHIRTY');
define('PLAN_UV35', 'UVTHIRTYFIVE');
define('PLAN_UV45', 'UVFORTYFIVE');
define('PLAN_UV50', 'UVFIFTY');
define('PLAN_UV55', 'UVFIFTYFIVE');

define('PLAN_UVS19', 'UVSNINETEEN');
define('PLAN_UVS24', 'UVSTWENTY_FOUR');
define('PLAN_UVS29', 'UVSTWENTY_NINE');
define('PLAN_UVS34', 'UVSTHIRTY_FOUR');
define('PLAN_UVS44', 'UVSFORTY_FOUR');
define('PLAN_UVS45', 'UVSFORTY_FIVE');
define('PLAN_UVS49', 'UVSFORTY_NINE');
define('PLAN_UVS54', 'UVSFIFTY_FOUR');

define('PLAN_M01S', 'MINT_ONE_MONTH_S');
define('PLAN_M03S', 'MINT_THREE_MONTHS_S');
define('PLAN_M06S', 'MINT_SIX_MONTHS_S');
define('PLAN_M12S', 'MINT_TWELVE_MONTHS_S');
define('PLAN_M01M', 'MINT_ONE_MONTH_M');
define('PLAN_M03M', 'MINT_THREE_MONTHS_M');
define('PLAN_M06M', 'MINT_SIX_MONTHS_M');
define('PLAN_M12M', 'MINT_TWELVE_MONTHS_M');
define('PLAN_M01L', 'MINT_ONE_MONTH_L');
define('PLAN_M03L', 'MINT_THREE_MONTHS_L');
define('PLAN_M06L', 'MINT_SIX_MONTHS_L');
define('PLAN_M12L', 'MINT_TWELVE_MONTHS_L');

// TSIP account status types (ACCOUNT_STATUS_TYPES.ACCOUNT_STATUS_TYPE -> ACCOUNTS.ACCOUNT_STATUS_TYPE) for version 3.12
define('ACCOUNT_STATUS_TYPE_PROVISIONED', 1);
define('ACCOUNT_STATUS_TYPE_IN_TRANSIT',  2);
define('ACCOUNT_STATUS_TYPE_IN_STOCK',    3);
define('ACCOUNT_STATUS_TYPE_ACTIVE',      4);
define('ACCOUNT_STATUS_TYPE_ACTIVATED',   5);
define('ACCOUNT_STATUS_TYPE_EXPIRED',     6);
define('ACCOUNT_STATUS_TYPE_SUSPENDED',   7);
define('ACCOUNT_STATUS_TYPE_DEACTIVATED', 8);

// default value for CUSTOMERS.COUNTRY
define('DEFAULT_ULTRA_COUNTRY', 'USA');

// seconds in common time measurements
define('SECONDS_IN_MINUTE', 60);
define('SECONDS_IN_HOUR',   3600);
define('SECONDS_IN_DAY',    86400);
define('SECONDS_IN_WEEK',   604800);

// the following are short names used in MongoDB documents
define('MDB_EVENT_CUSTOMER_ID',         'eCID'      );
define('MDB_EVENT_TIMESTAMP',           'eTi'       );
define('MDB_EVENT_CATEGORY',            'eCa'       );
define('MDB_EVENT_TYPE',                'eTy'       );
define('MDB_EVENT_OBJECT',              'eOb'       );
define('MDB_EVENT_CLASS',               'eCl'       );
define('MDB_EVENT_STATUS',              'eSt'       );
define('MDB_EVENT_SOURCE',              'eSo'       );
define('MDB_EVENT_DESCRIPTION',         'eDe'       );
define('MDB_EVENT_APPLICATION',         'eAp'       );
define('MDB_EVENT_PRIORITY',            'ePr'       );
define('MDB_EVENT_NAME',                'eNa'       );
define('MDB_EVENT_MSISDN',              'eMi'       );
define('MDB_EVENT_IP',                  'eIp'       );
define('EVENT_CATEGORY_OPENING',        0           );
define('EVENT_CATEGORY_CLOSING',        1           );
define('EVENT_CATEGORY_PENDING',        2           );
define('EVENT_TYPE_SCHEDULED',          0           );
define('EVENT_TYPE_UNSCHEDULED',        1           );
define('EVENT_SOURCE_API',              'API'       );
define('EVENT_CLASS_ACCOUNT',           'ACCOUNT'   );
define('EVENT_CLASS_BILLING',           'BILLING'   );
define('EVENT_CLASS_REFERRALS',         'REFERRALS' );
define('EVENT_STATUS_IMMEDIATE',        0           );
define('EVENT_STATUS_DELAYED',          1           );
define('EVENT_STATUS_SCHEDULED',        2           );
define('EVENT_PRIORITY_LOW',            0           );
define('EVENT_PRIORITY_NORMAL',         1           );
define('EVENT_PRIORITY_HIGH',           2           );

define("BOLTON_OPTION_ATTRIBUTE_IDDCA",      'BOLTON.MRC_CALLANYWHERE');
define("BOLTON_OPTION_ATTRIBUTE_DATA",       'BOLTON.MRC_ADDITIONALDATA');
define('BOLTON_OPTION_ATTRIBUTE_GLOBE',      'BOLTON.UP_GLOBE');
define('BOLTON_OPTION_ATTRIBUTE_SHAREDDATA', 'BOLTON.SHAREDDATA');
define('BOLTON_OPTION_ATTRIBUTE_SHAREDILD',  'BOLTON.SHAREDILD');

// ULTRA.CUSTOMER_OPTIONS.OPTION_ATTRIBUTE
define('BILLING_OPTION_ATTRIBUTE_BOGO_MONTH', 'BILLING.MRC_BOGO_MONTH');
define('BILLING_OPTION_ATTRIBUTE_7_11_BOGO',  'BILLING.MRC_7_11_BOGO');
define('BILLING_OPTION_ATTRIBUTE_7_11',       'BILLING.MRC_7_11');
define('BILLING_OPTION_MULTI_MONTH',          'BILLING.MRC_MULTI_MONTH');
define('BILLING_OPTION_DEMO_LINE',            'BILLING.MRC_DEALER_PROMO'); // dealer demo line
define('BILLING_OPTION_PROMO_PLAN',           'BILLING.MRC_PROMO_PLAN');
define('BRAND_OPTION_SUBPLAN_A',              'BRAND.UV.SUBPLAN.A');
define('BRAND_OPTION_SUBPLAN_B',              'BRAND.UV.SUBPLAN.B');
define('MRC_HOUSE_ACCOUNT',                   'BILLING.MRC_HOUSE_ACCOUNT');
define('MIGRATION_EXTENSION_PLAN_START_DATE', 'MEP.START_DATE');

// HTT_TRANSITION_LOG.STATUS
define('STATUS_OPEN',    'OPEN');
define('STATUS_CLOSED',  'CLOSED');
define('STATUS_ABORTED', 'ABORTED');
define('STATUS_RUNNING', 'RUNNING');

define('SMS_QUALITY_OF_SERVICE_UNICODE', 801);
define('SMS_QUALITY_OF_SERVICE_ASCII',   865);
define('SMS_MAX_LENGTH', 160);

// customer credit transaction destinations
define('WALLET',  'balance');       // ACCOUNTS.BALANCE: money can be used for any purpose
define('MONTHLY', 'stored_value');  // HTT_CUSTOMERS_OVERLAY_ULTRA.STORED_VALUE: money to be used for plan renewal only

define('ACC_SUCCESS_RESULTCODE', 100);


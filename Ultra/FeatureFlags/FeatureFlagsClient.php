<?php
namespace Ultra\FeatureFlags;

use LaunchDarkly\LDClient;
use LaunchDarkly\LDUser;
use LaunchDarkly\LDUserBuilder;
use Ultra\Configuration\Configuration;

/**
 * Class FeatureFlagsClient
 * @package Ultra\FeatureFlags
 */
class FeatureFlagsClient
{
  /**
   * @const
   */
  const SHOW_FLEX_PLANS = 'show-flex-plans';
  const SHOW_AUGUST_PLANS = 'show-august-plans';
  CONST DISABLE_RATE_LIMIT = 'disable-rate-limit';
  
  /**
   * @var null|LDClient
   */
  private $ldClient = null;

  /**
   * @var null|FeatureFlagsClient
   */
  private static $instance = null;

  /**
   * @var LDUser
   */
  private $user;

  /**
   * @var array
   */
  private $allowedIps = [
    '162.222.65.27', // VPN
    '64.60.37.226',  // CM
    '100.38.216.12', // LIC
    '162.222.64.80', // web-dev-01
  ];

  /**
   * FeatureFlagsClient constructor.
   *
   * The LaunchDarkly sdk documentation requires the LDClient to be implemented as a singleton.
   * @see http://docs.launchdarkly.com/docs/php-sdk-reference
   */
  private function __construct(){}

  /**
   * @return FeatureFlagsClient
   */
  public static function getInstance()
  {
    return self::$instance = self::$instance === null ? new FeatureFlagsClient() : self::$instance;
  }

  /**
   * @return mixed
   */
  private function getIpAddress()
  {
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      return $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if (!empty($_SERVER['REMOTE_ADDR'])) {
      return $_SERVER['REMOTE_ADDR'];
    } else {
      return $_SERVER['SERVER_ADDR'];
    }
  }

  /**
   * Check to see if the flex plans should be displayed.
   *
   * @return bool
   */
  public function showFlexPlans()
  {
    return (bool) $this->getFlagStatus(self::SHOW_FLEX_PLANS, $this->getUser(), false);
  }

  /**
   * Check to see if the flex plans should be displayed.
   *
   * @return bool
   */
  public function showRefreshPlans()
  {
    return (bool) $this->getFlagStatus(self::SHOW_AUGUST_PLANS, $this->getUser(), false);
  }

  /**
   * Check to see if purchase rate limits should be dsiabled
   *
   * @return bool
   */
  public function disableRateLimit($customerID)
  {
    $user = new LDUserBuilder('customer_id_' . $customerID);
    $user = $user->build();
    return (bool) $this->getFlagStatus(self::DISABLE_RATE_LIMIT, $user, false);
  }

  /**
   * @param $flagKey
   * @param LDUser $user
   * @param bool $default
   * @return mixed
   */
  public function getFlagStatus($flagKey, LDUser $user, $default = false)
  {
    return $this->getClient()->variation($flagKey, $user, $default);
  }

  /**
   * @param LDUserBuilder $userBuilder
   * @return LDUser
   */
  public function setUser(LDUserBuilder $userBuilder)
  {
    return $this->user = $userBuilder->ip($this->getIpAddress())->build();
  }

  /**
   * @return LDUser
   */
  private function getUser()
  {
    return empty($this->user) ? $this->setUser(new LDUserBuilder(hash('md5', 'api'))) : $this->user;
  }

  /**
   * @param LDClient $client
   * @return LDClient
   */
  public function setClient(LDClient $client)
  {
    if (!empty(self::$instance->ldClient)) {
      return self::$instance->ldClient;
    }

    dlog('', 'SETTING INSTANCE OF LDClient');
    return self::$instance->ldClient = $client;
  }

  /**
   * @return LDClient
   */
  private function getClient()
  {
    return empty(self::$instance->ldClient) && !(self::$instance->ldClient instanceof LDClient) ?
      $this->setClient(new LDClient((new Configuration())->getLaunchDarklySdkKey(), ['connect_timeout' => 2, 'timeout' => 2])) :
      self::$instance->ldClient;
  }
}

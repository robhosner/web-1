<?php
namespace Ultra\FeatureFlags;

class FeatureFlagsClientWrapper
{
  public function showFlexPlans()
  {
    return FeatureFlagsClient::getInstance()->showFlexPlans();
  }

  public function showRefreshPlans()
  {
    return FeatureFlagsClient::getInstance()->showRefreshPlans();
  }

  public function disableRateLimits($customerID)
  {
    return FeatureFlagsClient::getInstance()->disableRateLimit($customerID);
  }
}

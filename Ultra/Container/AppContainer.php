<?php
namespace Ultra\Container;

use Ultra\Accounts\Interfaces\AccountsRepository;
use Ultra\Activations\Interfaces\ActivationHistoryRepository;
use Ultra\Billing\Interfaces\BillingHistoryRepository;
use Ultra\Commander\DefaultCommandBus;
use Ultra\Commander\Interfaces\CommandBus;
use Ultra\Configuration\Interfaces\ConfigurationRepository;
use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Dealers\Interfaces\DealersRepository;
use Ultra\Lib\DB\Merchants\MerchantInterface;
use Ultra\Lib\DB\Merchants\MeS;
use Ultra\Mvne\MakeItSo\Interfaces\MakeItSoRepositoryInterface;
use Ultra\Mvne\MakeItSo\Repositories\Mssql\MakeItSoRepository;
use Ultra\Orders\Interfaces\OrderRepository;
use Ultra\Plans\Interfaces\PlansRepository;
use Ultra\Sims\Interfaces\SimRepository;

/**
 * Class AppContainer
 * @package Ultra\Container
 */
class AppContainer extends Container
{
  /**
   * AppContainer constructor.
   */
  public function __construct()
  {
    return $this->buildContainerBindings($this);
  }

  /**
   * Set application container bindings.
   *
   * @param AppContainer $container
   * @return AppContainer
   */
  public function buildContainerBindings(AppContainer $container)
  {
    /*
    |--------------------------------------------------------------------------
    | ****** CONTAINER BINDING EXAMPLES ******
    |--------------------------------------------------------------------------
    */

    // Bind a "template" class to the container
    // $container->bind('template', new Acme\Template());

    // Bind a "mailer" class to the container. Use a callback to set additional settings
    /*
      $container->bind('mailer', function ($container) {
        $mailer = new Acme\Mailer;
        $mailer->username = 'username';
        $mailer->password = 'password';
        $mailer->from = 'foo@bar.com';
        return $mailer;
      });
    */

    // Bind a singleton "database" class to the container
    // Use a callback to set additional settings
    /*
      $container->singleton('database', function ($container) {
        return new Acme\Database('username', 'password', 'host', 'database');
      });
    */

    // Bind an existing instance as shared in the container
    /*
      $auth = new Acme\Authentication;
      $container->instance('auth', $auth);
    */

    // Automatic resolution where the container automatically creates an instance of the requested controller,
    // including all of its class dependencies
    // $container->make('Acme\Controller\SomeControllerNotExplicitlyBoundToContainer');

    // Bind Interface To Implementation
    // $container->bind('App\Contracts\EventPusher', 'App\Services\RedisEventPusher');

    // Fetch class from container
    // $container->make('App\Contracts\EventPusher');

    $container->bind(BillingHistoryRepository::class, \Ultra\Billing\Repositories\Mssql\BillingHistoryRepository::class);
    $container->bind(CustomerRepository::class, \Ultra\Customers\Repositories\Mssql\CustomerRepository::class);
    $container->bind(ActivationHistoryRepository::class, \Ultra\Activations\Repositories\Mssql\ActivationHistoryRepository::class);
    $container->bind(DealersRepository::class, \Ultra\Dealers\Repositories\Mssql\DealersRepository::class);
    $container->bind(CreditCardRepository::class, \Ultra\CreditCards\Repositories\Mssql\CreditCardRepository::class);
    $container->bind(OrderRepository::class, \Ultra\Orders\Repositories\Mssql\OrderRepository::class);
    $container->bind(SimRepository::class, \Ultra\Sims\Repositories\Mssql\SimRepository::class);
    $container->bind(ConfigurationRepository::class, \Ultra\Configuration\Repositories\Cfengine\ConfigurationRepository::class);
    $container->bind(PlansRepository::class, \Ultra\Plans\Repositories\Cfengine\PlansRepository::class);
    $container->bind(AccountsRepository::class, \Ultra\Accounts\Repositories\Mssql\AccountsRepository::class);
    $container->bind(MerchantInterface::class, MeS::class);
    $container->bind(CommandBus::class, DefaultCommandBus::class);
    $container->bind(MakeItSoRepositoryInterface::class, MakeItSoRepository::class);


    return $container;
  }
}





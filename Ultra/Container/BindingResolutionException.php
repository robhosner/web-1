<?php
/**
 * "name": "illuminate/container"
 * "description": "The Illuminate Container package."
 * "license": "MIT"
 * "homepage": "http://laravel.com"
 * https://github.com/illuminate/container
 */
namespace Ultra\Container;

use Exception;

class BindingResolutionException extends Exception
{
  //
}
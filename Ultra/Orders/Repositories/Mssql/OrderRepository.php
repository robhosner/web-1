<?php
namespace Ultra\Orders\Repositories\Mssql;

require_once __DIR__ . '/../../Interfaces/OrderRepository.php';

/**
 * Class OrderRepository
 * @package Ultra\Orders\Repositories\Mssql
 */
class OrderRepository implements \Ultra\Orders\Interfaces\OrderRepository
{
  /**
   * Get an order by iccid.
   * 
   * @param $iccid
   * @param array $selectFields
   * @return bool|mixed
   */
  public function getOrderByIccid($iccid, array $selectFields)
  {
    $order = false;
    $fields = '*';
    
    if (!empty($selectFields))
    {
      $fields = implode(',', $selectFields);
    }
    
    $orders = mssql_fetch_all_objects(
      logged_mssql_query(
        sprintf(
          "SELECT TOP 1 $fields 
            FROM ULTRA.ONLINE_ORDERS WITH (NOLOCK)
            WHERE ICCID_FULL = %s",
          mssql_escape_with_zeroes($iccid)
        )
      )
    );

    if (is_array($orders) && count($orders))
    {
      $order = $orders[0];
    }

    return $order;
  }

  /**
   * Add an order.
   * 
   * @param array $data
   * @return mixed
   */
  public function addOrder(array $data)
  {
    $insertSql = sprintf(
      "INSERT INTO ULTRA.ONLINE_ORDERS
       (
          ORDER_NUM,
          ICCID_FULL,
          IMEI,
          SKU,
          FIRST_NAME,
          LAST_NAME,
          EMAIL,
          ADDRESS1,
          ADDRESS2,
          CITY,
          STATE,
          POSTAL_CODE,
          TOKEN,
          BIN,
          LAST_FOUR,
          EXPIRES_DATE,
          CVV_VALIDATION,
          AVS_VALIDATION,
          GATEWAY,
          MERCHANT_ACCOUNT,
          AUTO_ENROLL,
          CUSTOMER_IP,
          PREFERRED_LANGUAGE,
          TRACK_TRACE
       )
       VALUES
       (
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s,
          %s
       )
      ",
      mssql_escape_with_zeroes($data["ordernum"]),
      mssql_escape_with_zeroes($data["iccid"]),
      null_or_mssql_escape_string($data, "imei"),
      mssql_escape_with_zeroes($data["SKU"]),
      null_or_mssql_escape_string($data, "first_name"),
      null_or_mssql_escape_string($data, "last_name"),
      null_or_mssql_escape_string($data, "email"),
      null_or_mssql_escape_string($data, "address1"),
      null_or_mssql_escape_string($data, "address2"),
      null_or_mssql_escape_string($data, "city"),
      null_or_mssql_escape_string($data, "state"),
      mssql_escape_with_zeroes($data["postal_code"]),
      mssql_escape_with_zeroes($data["token"]),
      mssql_escape_with_zeroes($data["bin"]),
      mssql_escape_with_zeroes($data["last_four"]),
      mssql_escape_with_zeroes($data["expires_date"]),
      null_or_mssql_escape_string($data, "cvv_validation"),
      null_or_mssql_escape_string($data, "avs_validation"),
      mssql_escape_with_zeroes($data["gateway"]),
      mssql_escape_with_zeroes($data["merchant_account"]),
      mssql_escape_with_zeroes($data["auto_enroll"]),
      mssql_escape_with_zeroes($data["customer_ip"]),
      mssql_escape_with_zeroes($data["preferred_language"]),
      mssql_escape_with_zeroes($data["track_trace"])
    );

    return is_mssql_successful(logged_mssql_query($insertSql));
  }
}
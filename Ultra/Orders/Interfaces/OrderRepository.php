<?php
namespace Ultra\Orders\Interfaces;

/**
 * Interface OrderRepository
 * @package Ultra\Orders\Interfaces
 */
interface OrderRepository
{
  /**
   * Get an order by iccid.
   *
   * @param $iccid
   * @param array $selectFields
   * @return bool|mixed
   */
  public function getOrderByIccid($iccid, array $selectFields);
  
  /**
   * Add an order.
   *
   * @param array $data
   * @return mixed
   */
  public function addOrder(array $data);
}

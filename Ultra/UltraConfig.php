<?php

namespace Ultra\UltraConfig;

require_once 'classes/SkuHandler.php';

function find_config($token)
{
  $value = find_credential($token);

  if ( $value || ( $value != '' ) )
    return $value;

  global $e_config;

  if ( isset($e_config) && isset($e_config[ $token ]) )
    return $e_config[ $token ];

  return '';
}

/**
 * loggerFlag
 *
 * Determines the behavior of the logger
 *
 * @return integer
 */
function loggerFlag()
{
  return find_config('logger/log4php');
}

/**
 * splunkLoggerFlag
 *
 * Determines if the logger should output in ``splunk-friendly`` mode
 *
 * @return boolean
 */
function splunkLoggerFlag()
{
  return ( loggerFlag() == 2 );
}

/**
 * acc_notification_wsdl
 *
 * WSDL for inbound ACC Notifications
 *
 * @return string
 */
function acc_notification_wsdl()
{
  return find_credential('amdocs/notifications/soap/wsdl');
}

/**
 * acc_control_wsdl
 *
 * WSDL for outbound ACC call
 *
 * @return string
 */
function acc_control_wsdl()
{
  return find_credential('amdocs/control/soap/wsdl');
}

/**
 * acc_certificate_info
 *
 * ACC certificate info
 *
 * @return array
 */
function acc_certificate_info()
{
  return array(
    find_credential('amdocs/certificate/filename'),
    find_credential('amdocs/certificate/password')
  );
}

/**
 * ultra_api_internal_info
 *
 * End point and credentials for APIs invoked internally (example: from middleware)
 *
 * @return array
 */
function ultra_api_internal_info()
{
  return array(
    find_credential('ultra/api/internal/domain'),
    find_credential('ultra/api/internal/user'),
    find_credential('ultra/api/internal/password')
  );
}

/**
 * ultra_api_epay_info
 *
 * End point and credentials for EPAY APIs invoked internally
 *
 * @return array
 */
function ultra_api_epay_info()
{
  return array(
    find_credential('ultra/api/epay/domain'),
    find_credential('ultra/api/epay/user'),
    find_credential('ultra/api/epay/password')
  );
}

/**
 * amdocs_sms_qualityofservice
 *
 * Quality Of Service allowed for outbound SMS
 *
 * @return atring
 */
function amdocs_sms_qualityofservice()
{
  return find_credential('amdocs/sendsms/qualityofservice');
}

/**
 * amdocs_sms_ssl_enabled
 *
 * Checks if SSL is disabled for outbouns SendSMS API calls to Amdocs after PHP 5.6 introduction
 *
 * @return boolean
 */
function amdocs_sms_ssl_enabled()
{
  $value = find_credential_or_config('amdocs/sendsms/ssl/disabled');

  return ! ! ( ! empty( $value ) && ( $value == 1 ) );
}

/**
 * univisionImportEnabled
 *
 * @return boolean
 */
function univisionImportEnabled()
{
  $settings = getSettingsObject();

  $value = $settings->checkUltraSettings('projectw/import/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

/**
 * mongoDBReadEnabled
 *
 * @return boolean
 */
function mongoDBReadEnabled()
{
  $settings = getSettingsObject();

  $value = $settings->checkUltraSettings('mongodb/read/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

/**
 * mongoDBWriteEnabled
 *
 * @return boolean
 */
function mongoDBWriteEnabled()
{
  $settings = getSettingsObject();

  $value = $settings->checkUltraSettings('mongodb/write/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

/**
 * getSettingsObject
 *
 * @return string
 */
function getSettingsObject()
{
  $namespace = getConfigNamespace();

  $class = '\\'.$namespace.'\Lib\Util\Settings';

  return new $class;
}

/**
 * getConfigNamespace
 *
 * @return string
 */
function getConfigNamespace()
{
  $namespace = getenv('OVERRIDE_NAMESPACE');

  if ( empty( $namespace ) )
    $namespace = find_credential('config/namespace');

  if ( empty( $namespace ) )
    return DEFAULT_NAMESPACE;

  return $namespace;
}

function middleware_enabled_amdocs()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('amdocs/runner/middleware/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

function amdocs_query_only()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('amdocs/query_only');

  return ! ! ( ( $value == '1' ) || ( $value == 'TRUE' ) );
}

function amdocs_autofail()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('amdocs/autofail');

  return ! ! ( ( $value == '1' ) || ( $value == 'TRUE' ) );
}

function cc_transactions_enabled()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('ultra/cc_transactions/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

function messaging_runner_enabled()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('runner/messaging/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

function promo_sms_campaign_enabled()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('runner/promo-sms-campaign/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

function invocations_resync_enabled()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('ultra/invocations_resync/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

function makeitso_enabled()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('amdocs/makeitso/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

function dealer_portal_enabled_uat()
{
  $settings = new \Ultra\Lib\Util\Settings;

  return $settings->checkUltraSettings('ultra/dealer_portal/enabled_uat');
}

function dealer_portal_enabled_stage()
{
  $settings = new \Ultra\Lib\Util\Settings;

  return $settings->checkUltraSettings('ultra/dealer_portal/enabled_stage');
}

function dealer_portal_enabled_prod()
{
  $settings = new \Ultra\Lib\Util\Settings;

  return $settings->checkUltraSettings('ultra/dealer_portal/enabled_prod');
}

function dealer_portal_disabled_msg()
{
  $settings = new \Ultra\Lib\Util\Settings;

  return $settings->checkUltraSettings('ultra/dealer_portal/disabled_message');
}

/**
 * dealer_portal_enabled_tsp
 *
 * return a list of dealer portal external payment providers and their availability
 * @see RTRIN-82
 * @return Array (PROVIDER => BOOLEAN)
 */
function dealer_portal_enabled_tsp()
{
  $settings = new \Ultra\Lib\Util\Settings;
  $result = array();
  foreach (array('epay', 'incomm', 'tcetra') as $provider)
    $result[$provider] = $settings->checkUltraSettings("ultra/dealer_portal/tsp/$provider");
  return $result;
}


function dealer_portal_activations_disabled_msg()
{
  $settings = new \Ultra\Lib\Util\Settings;

  return $settings->checkUltraSettings('ultra/activations/dealer_portal/disabled_message');
}

function are_dealer_portal_activations_enabled()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('ultra/activations/dealer_portal/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

/**
 * dealer_portal_demo_line_dealers
 *
 * return list of dealer codes eligible to participate in Demo Line programm
 * @see DEMO-1
 */
function dealer_portal_demo_line_dealers()
{
  $dealers = find_credential('ultra/dealer_portal/demo_line_dealers');
  if ( ! empty($dealers))
    return json_decode($dealers);
  else
    return NULL;
}


function is_monthly_charge_enabled()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('runner/monthly_charge/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

// This function returns how many days should we wait before cancelling a customer in $plan_state state.
function monthly_charge_cancellation_delay($plan_state)
{
  // lowercase + remove non-word characters
  $plan_state = strtolower( preg_replace("/\W/", "", $plan_state) );

  // get configuration from ht.cf
  $monthly_charge_cancellation_delay = find_credential('runner/monthly_charge/cancellation/days/'.$plan_state);

  // 60 days is default
  return ( $monthly_charge_cancellation_delay ? $monthly_charge_cancellation_delay : 60 );
}

function monthly_charge_exec_minutes()
{
  $value = find_credential('runner/monthly_charge/exec/minutes');
  if ( empty( $value ) )
  {
    $value = 120;   // default value
    \logError('runner/monthly_charge/exec/minutes is not set');
  }

  return $value;
}

function monthly_groups_enabled()
{
  $settings = new \Ultra\Lib\Util\Settings;

  $value = $settings->checkUltraSettings('runner/monthly_groups/enabled');

  return ! ( ( $value == '0' ) || ( $value == 'FALSE' ) );
}

function password_hash_enabled()
{
  return ! ! find_credential('ultra/password/hash/enabled');
}

function ccAlwaysFailTokens()
{
  return array(
    'Ultrasmith',
    find_credential('cc/always_fail/bin'),
    find_credential('cc/always_fail/last_four'),
    find_credential('cc/always_fail/expires_date')
  );
}

function ccAlwaysSucceedTokens()
{
  return array(
    'Ultrasmith',
    find_credential('cc/always_succeed/bin'),
    find_credential('cc/always_succeed/last_four'),
    find_credential('cc/always_succeed/expires_date')
  );
}

function msisdnAlwaysSucceed()
{
  return '1001001000';
}

// the Primary merchant/gateway which is chosen to process sales
function getPrimaryCCGateway()
{
  return 'MeS';
}

// no Secondary Gateway for the moment
function getSecondaryCCGateway()
{
  return NULL;
}

function getCCCredentialsURL( $gateway )
{
  return find_credential('cc/credentials/url/'.$gateway);
}

function getCCCredentialsProfile( $gateway )
{
  // This value must be cleaned up because cfengine returns long numeric strings in a scientific notation

  $profile = find_credential('cc/credentials/profile/'.$gateway);

  $profile = str_replace('_','',$profile);

  return $profile;
}

function getCCCredentialsKey( $gateway )
{
  return find_credential('cc/credentials/key/'.$gateway);
}

function getDBName()
{
  return find_credential('db/name');
}

function getTSIPDBVersion()
{
  return find_config('db/TSIP');
}

function isProductionEnvironment()
{
  $gitBranch = find_config('git/branch');

  return ! ! ( preg_match("/prod/", $gitBranch ) && ! isDevelopmentDB() );
}

function isDevelopmentHost()
{
  $hostName = gethostname();

  return ! ! preg_match("/dev/", $hostName );
}

function isDevelopmentDB()
{
  $dbName = getDBName();

  return ! ! ( ( $dbName == 'ULTRA_DEVELOP_TEL' ) || ( $dbName == 'INTL_DEV' ) );
}

function incommURL()
{
  return find_credential('incomm/url');
}

function incommPartnerName()
{
  return find_credential('incomm/partnername');
}

function redisClusterHosts()
{
  return explode( " " , find_credential_or_config('redis/cluster/hosts') );
}

function redisClusterInfo()
{
  return array(
    'hosts'    => redisClusterHosts(),
    'name'     => find_credential_or_config('redis/cluster/name'),
    'password' => find_credential_or_config('redis/cluster/password')
  );
}

function solrCredentials()
{
  return array(
    'hostname' => explode( ' ' , find_credential_or_config('solr/host' , TRUE ) ),
    'port'     => find_credential_or_config('solr/port'    , TRUE ),
    'path'     => find_credential_or_config('solr/db_path' , TRUE ),
    'login'    => find_credential_or_config('solr/db_user' , TRUE ),
    'password' => find_token_from_file('solr/db_pfile')
  );
}

function amazonS3Credentials()
{
  return [
    'bucket'       => find_credential_or_config('amazon/s3/bucket'       , TRUE ),
    'read_key'     => find_credential_or_config('amazon/s3/read/key'     , TRUE ),
    'read_secret'  => find_credential_or_config('amazon/s3/read/secret'  , TRUE ),
    'write_key'    => find_credential_or_config('amazon/s3/write/key'    , TRUE ),
    'write_secret' => find_credential_or_config('amazon/s3/write/secret' , TRUE )
  ];
}

function redisHost()
{
  return find_config('redis/host');
}

function redisPassword()
{
  return find_config('redis/password');
}

function channelsDefaultReplyTimeoutSeconds()
{
  return find_config('redis/channels/reply_timeout_seconds');
}

function celluphoneDb()
{
  return find_config('celluphone/db');
}

function celluphoneUltrasessionOverride()
{
  return find_config('celluphone/ultrasession/override');
}

function celluphoneUltrasessionExpiration()
{
return 240; // TODO: WTF cfengine ...
  return find_config('celluphone/ultrasession/expiration_seconds');
}

function maxDuplicateCCCount()
{
  return find_config('constants/max_cc_duplicates');
}

function allowInfiniteCCCount()
{
  return find_config('f/ok_cc_dup');
}

function allowInfiniteEmailCount()
{
  return find_config('f/ok_email_dup');
}

function accessToken3ci()
{
  return find_config('sms/3ci/access_token');
}

function accessTokenExacaster()
{
  return find_config('exacaster/access_token');
}

function promo_7_11_enabled()
{
  return find_config('ultra/promo/enabled/7_11');
}

function tmowatcher_pidlimit()
{
  return find_config('ultra/TMOWatcher/pidlimit');
}

function notifications_propagate_enabled()
{
  return find_config('acc/notifications/propagate/enabled');
}

function notifications_propagate_all_enabled()
{
  return find_config('acc/notifications/propagate/all/enabled');
}

/**
 * call_anywhere_additional_credit_options
 *
 * ``bonus_percent`` is always 25%
 *
 * @return array
 */
function call_anywhere_additional_credit_options()
{
  $call_anywhere_additional_credit_options = find_config('plans/call_anywhere_additional');

  if ( $call_anywhere_additional_credit_options )
    $call_anywhere_additional_credit_options = json_decode( $call_anywhere_additional_credit_options );

  if ( is_array( $call_anywhere_additional_credit_options ) )
  {
    $data = array();

    for ( $i = 0 ; $i < count($call_anywhere_additional_credit_options) ; $i++ )
    {
      $data[ $i ] = new \stdClass();
      $data[ $i ]->cost          = $call_anywhere_additional_credit_options[ $i ];
      $data[ $i ]->bonus_percent = 0.25;
      $data[ $i ]->value         = ( $data[ $i ]->cost * ( 1 + $data[ $i ]->bonus_percent ) );
    }

    return $data;
  }
  else
  {
    dlog('',"missing or invalid configuration token 'plans/call_anywhere_additional'");

    return NULL;
  }
}

/**
 * getAccSocsPlanConfigSwitch
 *
 * $plan must be one of the following: 'NINETEEN', 'TWENTY_NINE', 'THIRTY_NINE', 'FORTY_NINE', 'FIFTY_NINE'
 * Provides a guideline to switch from $plan_from to $plan_to
 *
 * @return array
 */
function getAccSocsPlanConfigSwitch( $plan_from , $plan_to )
{
  $accSocsPlanConfigSwitch = array(
    'ADD'    => array(),
    'RETAIN' => array(),
    'REMOVE' => array()
  );

  $configFrom = \Ultra\MvneConfig\getAccSocsUltraPlanConfig( $plan_from );
  $configTo   = \Ultra\MvneConfig\getAccSocsUltraPlanConfig( $plan_to   );

  foreach( $configFrom as $nameFrom => $valueFrom )
  {
    $status = array();

    foreach( $configTo as $nameTo => $valueTo )
    {
      if ( ( $nameFrom == $nameTo ) && ( $valueFrom == $valueTo ) )
      {
        // this SOC is unchanged.
        $status = array( 'RETAIN' , $nameTo , $valueTo );
      }
      elseif ( ( $nameFrom == $nameTo ) && ( $valueFrom != $valueTo ) )
      {
        // this SOC value is changed. TODO: ask Riz. -- INCREMENT, DECREMENT, RESET, ADD, REMOVE, RETAIN
        $status = array( '~ADD' , $nameTo , $valueTo );
      }
    }

    // this SOC is only in the old plan
    if ( !count( $status ) )
    {
      $accSocsPlanConfigSwitch[ 'REMOVE' ][] = array( $nameFrom => $valueFrom );
    }
    else
    {
      $accSocsPlanConfigSwitch[ $status[0] ][] = array( $status[1] => $status[2] );
    }
  }

  foreach( $configTo as $nameTo => $valueTo )
  {
    $found = 0;

    foreach( $configFrom as $nameFrom => $valueFrom )
      if ( $nameFrom == $nameTo )
        $found = 1;

    // this SOC is only in the new plan
    if ( !$found )
      $accSocsPlanConfigSwitch[ 'ADD' ][] = array( $nameTo => $valueTo );
  }

  return $accSocsPlanConfigSwitch;
}

/**
 * getAccBalanceConfig
 *
 * Convert Amdocs CheckBalance output to Ultra Balances
 * $type corresponds to BalanceValueList::BalanceValue::Type
 * $name corresponds to BalanceValueList::BalanceValue::Name
 *
 * @return string
 */
function getAccBalanceConfig($type,$name='')
{
  $accBalanceConfig = find_config('amdocs/balance/type/'.$type);

  if ( !isset($accBalanceConfig) || ( $accBalanceConfig == '' ) )
    $accBalanceConfig = find_config('amdocs/balance/type/'.$type.'/'.$name);

  return $accBalanceConfig;
}


function getAccSocByPlanId( $planId )
{
  $accSoc = array(
    'plan_id'            => 'Unknown-' . $planId ,
    'parameter'          => 'NULL' ,
    'ultra_service_name' => 'Unknown' ,
    'description'        => 'Unknown Service ID'
  );

  list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

  foreach( $accSocsDefinition as $definition )
    if ( $definition['plan_id'] == $planId )
      $accSoc = $definition;

  return $accSoc;
}

function getAccSocByName( $planName )
{
  $accSoc = array(
    'plan_id'            => 'Unknown' ,
    'parameter'          => 'NULL' ,
    'ultra_service_name' => 'Unknown' ,
    'description'        => 'Unknown Service ID'
  );

  list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

  foreach( $accSocsDefinition as $definition )
    if ( $definition['ultra_service_name'] == $planName )
      $accSoc = $definition;

  return $accSoc;
}

/**
 * getwholesalePlanIdFromName
 *
 * @return string
 */
function getwholesalePlanIdFromName( $wholesalePlanName )
{ 
  $wholesalePlanNameMap = getWholesalePlanIdMap();

  if ( empty( $wholesalePlanNameMap[ $wholesalePlanName ] ) )
    return NULL;
  
  return $wholesalePlanNameMap[ $wholesalePlanName ];
}

/**
 * getWholesalePlanIdMap
 *
 * @return array
 */
function getWholesalePlanIdMap()
{
  return [
    PRIMARY_WHOLESALE_PLAN      => 1,
    SECONDARY_WHOLESALE_PLAN    => 2,
    UV_PRIMARY_WHOLESALE_PLAN   => 3,
    UV_SECONDARY_WHOLESALE_PLAN => 4,
    MINT_WHOLESALE_PLAN         => 5,
    MRC_WHOLESALE_PLAN          => 6
  ];
}

/**
 * getWholesalePlanNameFromId
 *
 * @return string
 */
function getWholesalePlanNameFromId( $wholesalePlanId )
{
  $wholesalePlanName = NULL;

  $wholesalePlanNameMap = getWholesalePlanIdMap();

  foreach( $wholesalePlanNameMap as $name => $id )
    if ( $id == $wholesalePlanId )
      $wholesalePlanName = $name;

  return $wholesalePlanName;
}

/**
 * given plan name return corresponding options
 *
 * @param: plan string, e.g. 'NINETEEN'
 * @return: array of plan options, e.g. 'cos_id' => '98280'
 */
function getUltraPlanConfiguration($plan)
{
  // set defaults - change to empty keys
  $result = array_fill_keys(
    array(
    'cos_id',
    'name',
    'aka',
    'cost',
    'talk_minutes',
    'intl_minutes',
    'unlimited_intl_minutes',
    'monthly',
    'has_calling_card_balance',
    'rechargeable',
    'taxable',
    'data_recharge',
    'voice_recharge',
    'bolt_ons',
    'wholesale',
    'brand_id'
    ),
    NULL
  );

  // get actual values
  $planConfig = \PlanConfig::Instance();
  return $planConfig->getPlanConfig(get_plan_name_from_short_name($plan));
}

/**
 * getBoltOnsByPlan
 *
 * Get all available Bolt Ons for the given plan
 *
 * @return array
 */
function getBoltOnsByPlan($plan)
{
  $plan_name_from_short_name = get_plan_name_from_short_name($plan);

  if ($plan_name_from_short_name)
    $plan = $plan_name_from_short_name;

  $planConfig = \PlanConfig::Instance();
  $boltOns = $planConfig->getPlanConfigItem(get_cos_id_from_plan($plan), 'bolt_ons');

  if ( $boltOns )
    return explode( ',' , $boltOns );

  return array();
}

/**
 * getBoltOnInfo
 *
 * Get information about a single Bolt On
 *
 * @return array or NULL
 */
function getBoltOnInfo( $boltOn )
{
  $map = getBoltOnsMapping();

  if ( ! empty( $map[ $boltOn ] ) )
  {
    $map[$boltOn]['id'] = $boltOn;
    return $map[ $boltOn ];
  }

  return NULL;
}

/**
 * getBoltOnInfoByUpgradePlanSoc
 *
 * Get information about a single Bolt On
 *
 * @return array or NULL
 */
function getBoltOnInfoByUpgradePlanSoc( $upgradePlanSoc )
{
  $map = getBoltOnsMapping();

  foreach( $map as $boltOn => $info )
    if ( ( $info['upgrade_plan_soc'] == $upgradePlanSoc ) )
      return $info;

  return NULL;
}

/**
 * getBoltOnInfoByOption
 *
 * Get information about a single Bolt On, given option attribute and option value
 *
 * @return array
 */
function getBoltOnInfoByOption( $attribute , $value )
{
  $map = getBoltOnsMapping();

  $bolt_on_info = NULL;

  foreach( $map as $boltOn => $info )
    if ( ( $info['option_attribute'] == $attribute ) && ( $info['cost'] == $value ) )
      $bolt_on_info = $info;

  return $bolt_on_info;
}

/**
 * getBoltOnInfoByPlan
 *
 * Get information about a Bolt On, by a given plan
 * @param String full plan name (NINETEEEN, TWENTY_FOUR etc)
 * @return Array of allowed bolt ons
 */
function getBoltOnInfoByPlan($plan)
{
  $data = NULL;

  $planConfig = \PlanConfig::Instance();
  if ($bolt_ons = $planConfig->getPlanConfigItem(get_cos_id_from_plan($plan), 'bolt_ons'))
  {
    $bolt_on_list = explode(",", $bolt_ons);
    foreach ($bolt_on_list as $bolt_on)
    {
      $bolt = getBoltOnInfo($bolt_on);
      $bolt['id'] =   $bolt['product'].'_'.$bolt['get_value'].'_'.$bolt['cost'];

      if (is_array($bolt))
        $data[$bolt['product']][] = $bolt;
    }
  }
  return $data;
}

/**
 * getBoltOnsOptionAttributes
 *
 * Get all the option attributes for all Bolt Ons
 *
 * @return array
 */
function getBoltOnsOptionAttributes()
{
  $map = getBoltOnsMapping();

  $optionAttributes = array();

  foreach( $map as $boltOn => $info )
    if ( ! in_array( $info['option_attribute'] , $optionAttributes ) )
      $optionAttributes[] = $info['option_attribute'];

  return $optionAttributes;
}

/**
 * getBoltOnsMappingByType
 *
 * Get a complete mapping of all available Bolt Ons, arranged by type
 *
 * @return array
 */
function getBoltOnsMappingByType()
{
  $map = getBoltOnsMapping();

  $mapByType = array();

  dlog('',"map = %s",$map);

  foreach( $map as $bolt_on => $info )
  {
    $data = explode( '_' , $bolt_on );

    if ( ! isset( $mapByType[ $data[0] ] ) )
      $mapByType[ $data[0] ] = array();

    $mapByType[ $data[0] ][] = $info;
    $mapByType[ $data[0] ][ count($mapByType[ $data[0] ]) - 1 ]['id'] = $bolt_on;
  }

  return $mapByType;
}

/**
 * getUltraPlanConfigurationItem
 *
 * Get ultra plan configuration item
 *
 * @return string
 */
function getUltraPlanConfigurationItem($plan, $item)
{
  if ($long_plan = get_plan_name_from_short_name($plan))
    $plan = $long_plan;

  $planConfig = \PlanConfig::Instance();
  return ($cred = $planConfig->getPlanConfigItem(get_cos_id_from_plan($plan), $item)) ? $cred : '';
}

/**
 * getBoltOnsMapping
 *
 * Get a complete mapping of all available Bolt Ons
 *
 * @return array
 */
function getBoltOnsMapping()
{
  $ui_product_names = array(
    'IDDCA'      => 'UpIntl',
    'DATA'       => 'UpData',
    'VOICE'      => 'UpVoice',
    'MDATA'      => 'UpData',
    'ROAM'       => 'UpRoam',
    'GLOBE'      => 'UpGlobe',
    'SHAREDILD'  => 'SharedILD',
    'SHAREDDATA' => 'SharedData'
  );

  $map = array(
    'IDDCA_6.25_5'  => array(
      'upgrade_plan_soc' => '',
      'sku'              => '$6.25@$5.00',
      'description'      => '$6.25 UpINTL Credit',
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_IDDCA,
      'type'             => NULL
    ),
    'IDDCA_12.5_10' => array(
      'upgrade_plan_soc' => '',
      'sku'              => '$12.50@$10.00',
      'description'      => '$12.50 UpINTL Credit',
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_IDDCA,
      'type'             => NULL
    ),
    'IDDCA_25_20'   => array(
      'upgrade_plan_soc' => '',
      'sku'              => '$25.00@$20.00',
      'description'      => '$25.00 UpINTL Credit',
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_IDDCA,
      'type'             => NULL
    ),
    'MDATA_1024_10' => array(
      'upgrade_plan_soc' => 'A-DATA-MINT|1',
      'sku'              => 'MINT1GB@$10.00',
      'description'      => '4G LTE UpData Mint - 1GB',
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_DATA,
      'type'             => 'WPRBLK25'
    ),
    'MDATA_3072_20' => array(
      'upgrade_plan_soc' => 'A-DATA-MINT|3',
      'sku'              => 'MINT3GB@$20.00',
      'description'      => '4G LTE UpData Mint - 3GB',
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_DATA,
      'type'             => 'WPRBLK20'
    ),
    'DATA_500_5'    => array(
      'upgrade_plan_soc' => 'A-DATA-BLK|500',
      'sku'              => '500MB@$5.00',
      'description'      => '4G LTE UpData - 500MB',
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_DATA,
      'type'             => NULL
    ),
    'DATA_1024_10'  => array(
      'upgrade_plan_soc' => 'A-DATA-BLK|1024',
      'sku'              => '1GB@$10.00',
      'description'      => '4G LTE UpData - 1GB',
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_DATA,
      'type'             => NULL
    ),
    'DATA_2048_20'  => array(
      'upgrade_plan_soc' => 'A-DATA-BLK|2048',
      'sku'              => '2GB@$20.00',
      'description'      => '4G LTE UpData - 2GB',
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_DATA,
      'type'             => NULL
    ),
    // DATAQ-113: Nationwide Voice Bolt Ons
    'VOICE_500_5'  => array(
      'upgrade_plan_soc' => 'B-VOICE|500',
      'sku'              => '500@$5.00',
      'description'      => '500 UpVoice minutes',
      'option_attribute' => NULL,
      'type'             => NULL
    ),
    'VOICE_1000_10'  => array(
      'upgrade_plan_soc' => 'B-VOICE|1000',
      'sku'              => '1000@$10.00',
      'description'      => '1000 UpVoice minutes',
      'option_attribute' => NULL,
      'type'             => NULL
    ),
    'VOICE_2000_20'  => array(
      'upgrade_plan_soc' => 'B-VOICE|2000',
      'sku'              => '2000@$20.00',
      'description'      => '2000 UpVoice minutes',
      'option_attribute' => NULL,
      'type'             => NULL
    ),
    // UpRoam SOCs
    'ROAM_500_5' => array(
      'upgrade_plan_soc' => 'ROAM|500',
      'sku'              => '500@$5.00',
      'description'      => '$5 UpRoam credit',
      'option_attribute' => NULL,
      'type'             => NULL
    ),
    'ROAM_1000_10' => array(
      'upgrade_plan_soc' => 'ROAM|1000',
      'sku'              => '1000@$10.00',
      'description'      => '$10 UpRoam credit',
      'option_attribute' => NULL,
      'type'             => NULL
    ),
    'ROAM_2000_20' => array(
      'upgrade_plan_soc' => 'ROAM|2000',
      'sku'              => '2000@$20.00',
      'description'      => '$20 UpRoam credit',
      'option_attribute' => NULL,
      'type'             => NULL
    ),
    // UpGlobe
    'GLOBE_10_10' => array(
      'upgrade_plan_soc' => '',
      'sku'              => 'GLOBE@$10.00',
      'description'      => 'Globe Unlimited',
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_GLOBE,
      'type'             => NULL
    )
  );

  // Shared ILD
  $sharedILD = [
    ['5.00',  '5'],
    ['10.00','10'],
    ['20.00','20'],
    ['50.00','50']
  ];
  foreach ($sharedILD as $item)
    $map["SHAREDILD_{$item[0]}_{$item[1]}"] = [
      'upgrade_plan_soc' => '',
      'sku'              => "\${$item[0]}@\${$item[1]}.00",
      'description'      => "\${$item[0]} Shared ILD Credit",
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_SHAREDILD,
      'type'             => NULL
    ];

  // Shared Data
  $sharedData = [
    ['250',    '0',  '250MB'],
    ['500',    '0',  '500MB'],
    ['500',    '5',  '500MB'],
    ['750',    '0',  '750MB'],
    ['1024',   '0',    '1GB'],
    ['1024',  '10',    '1GB'],
    ['1280',   '0', '1280MB'],
    ['1536',   '0', '1536MB'],
    ['1792',   '0', '1792MB'],
    ['2048',   '0',    '2GB'],
    ['2048',  '20',    '2GB'],
    ['2304',   '0', '2304MB'],
    ['2560',   '0', '2560MB'],
    ['2816',   '0', '2816MB'],
    ['3072',   '0',    '3GB'],
    ['3328',   '0', '3328MB'],
    ['3584',   '0', '3584MB'],
    ['3840',   '0', '3840MB'],
    ['4096',   '0',    '4GB'],
    ['5120',  '45',    '5GB'],
    ['10240', '80',   '10GB']
  ];
  foreach ($sharedData as $item)
    $map["SHAREDDATA_{$item[0]}_{$item[1]}"] = [
      'upgrade_plan_soc' => "A-DATA-POOL-1|{$item[0]}",
      'sku'              => "{$item[2]}@\${$item[1]}.00",
      'description'      => "4G LTE Shared UpData - {$item[2]}",
      'option_attribute' => BOLTON_OPTION_ATTRIBUTE_SHAREDDATA,
      'type'             => NULL
    ];

  while ( list( $key , $value ) = each($map) )
  {
    $data = explode( '_' , $key );

    $map[ $key ]['product']   = $data[0];
    $map[ $key ]['get_value'] = $data[1];
    $map[ $key ]['cost']      = $data[2];
    $map[ $key ]['ui_product_name'] = $ui_product_names[ $data[0] ];
  }

  return $map;
}

function mapLanguageToVoicemailSoc( $language )
{
  $language = strtolower( $language );

  if ( is_english( $language ) )
    return 'v_english';

  if ( ( $language == 'es-us' ) || ( $language == 'es' ) )
    return 'v_spanish';

  dlog('',"Could not map language < $language > to Voicemail Soc");

  return '';
}

function isVoicemailSoc( $soc )
{
  return in_array($soc, [
    'v_english',
    'v_spanish',
    'v_english_vvm',
    'v_spanish_vvm'
  ]);
}

function isWifiCallingSoc( $soc )
{
  return ($soc == 'n_wfc');
}

/**
 * returns boolean, if soc is roaming soc
 */
function isRoamingSoc( $soc )
{
  return in_array($soc, [
    'n_roam_latam',
    'n_roam_smpglb',
    12811,
    12814
  ]);
}

/**
 * returns boolean, if soc is roaming wallet soc
 */
function isRoamingWalletSoc( $soc )
{
  return in_array($soc, [
    'a_voicesmswallet_ir',
    12707
  ]);
}

function roamingSocFeatureIdByBrandId($brand_id)
{
  $brandName = getShortNameFromBrandId($brand_id);
  $featureIdMap = [
    'ULTRA'     => 12814, // smpglb
    'UNIVISION' => 12811, // latam
    'MINT'      => 12814
  ];

  return isset($featureIdMap[$brandName]) ? $featureIdMap[$brandName] : null;
}

function isAddOnDataSoc( $soc )
{
  return in_array($soc, [
    'a_data_blk',
    'a_data_blk_dr',
    'a_data_blk1024',
    'a_data_blk1536'
  ]);
}

/**
 * getDefaultMongoDBEventsCredentials
 *
 * Mongo DB credentials for Event Tracker
 *
 * @return array
 */
function getDefaultMongoDBEventsCredentials()
{
  $password = find_token_from_file('mongodb/events/pfile');

  return array(
    'db'         => find_credential_or_config('mongodb/events/db'),
    'username'   => find_credential_or_config('mongodb/events/username'),
    'password'   => $password,
    'hosts'      => explode( ' ' , find_credential_or_config('mongodb/events/hosts') ),
    'replicaset' => find_credential_or_config('mongodb/events/replicaset')
  );
}

/**
 * getDefaultMongoDBCredentials
 *
 * Mongo DB credentials
 *
 * @return array
 */
function getDefaultMongoDBCredentials()
{
  return array(
    'host'     => find_credential_or_config('mongodb/host'),
    'port'     => find_credential_or_config('mongodb/port'),
    'db_name'  => find_credential_or_config('mongodb/db_name'),
    'username' => find_credential_or_config('mongodb/username'),
    'password' => find_credential_or_config('mongodb/password'),
    'connection_string' => find_credential_or_config('mongodb/connection_string')
  );
}

/**
 * getMiddlewareDBCredentials
 *
 * Middleware DB credentials
 *
 * @return array
 */
function getMiddlewareDBCredentials()
{
  $host     = find_credential_or_config('mw/db/host');
  $db_name  = find_credential_or_config('mw/db/name');
  $username = find_credential_or_config('mw/db/user');
  $password = find_credential_or_config('mw/db/pfile__retrieved');
  $password = !empty($password) ? $password : find_token_from_file('mw/db/pfile');

  if ( ! $host )
    \logFatal("Missing MW DB Host name in environment configuration");

  return array(
    'host'     => ( ( $host     ) ? $host     : ''                                ),
    'db_name'  => ( ( $db_name  ) ? $db_name  : 'ULTRA_ACC'                       ),
    'username' => ( ( $username ) ? $username : find_credential('db/user')        ),
    'password' => ( ( $password ) ? $password : find_token_from_file('db/pfile')  )
  );
}

/**
 * zeroUsedMinutes
 *
 * MVNO-2280
 * return number of used minutes based on plan name
 * @param string plan: long plan name (e.g. NINETEEN, TWENTY_NINE...)
 * @return int number of minutes used
 */
function zeroUsedMinutes( $plan )
{
  $planConfig = \PlanConfig::Instance();
  return $planConfig->zeroUsedMinutes($plan);

  // return find_config('plans/Ultra/' . $plan . '/zero_used_minutes');
}

/**
 * getPaymentProviderSkuMap
 *
 * return a map of external payment provider's SKU (T-Cetra, inComm) which can be used for submitting a payment to Ultra
 * @see TCET-1, RTRIN-1
 * @param String provider TCETRA or INCOMM
 * @return Array map of UPC or SKU codes:
 *   UPC => array(MIN, MAX, DESTINATION, SOURCE, BRAND) where
 *     MIN is minimum charge amount allowed for this UPC in cents
 *     MAX is maximum charge amount allowed for this UPC in cents (this value is same as MIN for fixed value UPC)
 *     DESTINATION is one of the following
 *       MONTHLY: HTT.CUSTOMERS_OVERLAY_ULTRA.STORED_VALUE
 *       WALLET: ACCOUNTS.BALANCE
 *       SMART: decided at runtime - if amount matches subscriber's base plan with or without bolt ons then maps to MONTHLY otherwise to WALLET
 *     SOURCE: value for HTT_BILLING_HISTORY.SOURCE (if NULL then default to provider's name) and transaction delay time
 *     BRAND: ULTRA or UNIVISION
 */
function getPaymentProviderSkuMap($provider)
{
  $skuHandler = new \SkuHandler();
  $result = $skuHandler->getPaymentProviderSkuMap($provider);

  if ($result->is_failure())
    return null;

  $skuMap = $result->get_data_key('sku_map');

  return ($skuMap && ! empty($skuMap))
    ? $skuMap
    : null;
}

/**
 * getPaymentProviderSubproductInfo
 * return info array for a payment provider subproduct
 * @param String subproduct
 * @return Array (value, commissionable, brand) or NULL when not found
 */
function getPaymentProviderSubproductInfo($subproduct)
{
  $skuHandler = new \SkuHandler();
  $result = $skuHandler->getPaymentProviderSubproductInfo($subproduct);

  if ($result->is_failure())
    return null;

  $info = $result->get_data_key('subproduct_info');

  return ($info) ? $info : null;
}

/**
 * getPaymentProviderCredentials
 *
 * return payment provider username and password
 * @param String provider name
 * @return Array (username, password) or NULL on failure
 */
function getPaymentProviderCredentials($provider)
{
  $provider = strtolower($provider);
  if ($username = find_credential("$provider/credentials/username") and $password = find_credential("$provider/credentials/password"))
    return array($username, $password);
  else
  {
    dlog('', "missing credentials for provider $provider");
    return NULL;
  }
}

/**
 * getPaymentProviderDelay
 *
 * return seconds delay to wait before processing a payment provider's transaction
 * @param String provider name
 * @return Integer delay, seconds
 * @see RTRIN-37
 */
function getPaymentProviderDelay($provider)
{
  $skuHandler = new \SkuHandler();
  return $skuHandler->getPaymentProviderDelay($provider);
}

/**
 * getPaymentProviderSkuAttribute
 *
 * return specific value of the requested attribue for the given provider's SKU or UPC
 * @param String provider name (TCETRA, EPAY, INCOMM)
 * @param String SKU or UPC (see getPaymentProviderSkuMap)
 * @param String attribute name ('maximum', 'minimum', 'destination', 'source')
 * @return String attribute value or NULL if not found
 */
function getPaymentProviderSkuAttribue($provider, $sku, $attribute)
{
  $skuHandler = new \SkuHandler();
  $result = $skuHandler->getPaymentProviderSkuAttribute($provider, $sku, $attribute);

  if ($result->is_failure())
    return null;

  $return = $result->get_data_key('attribute');
  return ( ! empty($return)) ? $return : null;
}

/**
 * isPaymentProviderHandoffEnabled
 *
 * return current status of account handoff to an external payment provider
 * @see RTRIN-35
 * @param String provider name
 * @return Boolean TRUE when enabled, FALSE otherwise
 */
function isPaymentProviderHandoffEnabled($provider)
{
  $provider = strtolower($provider);
  return (Boolean)find_credential("$provider/handoff");
}

/**
 * isPaymentProviderBrandValidationEnabled
 * return current status of payment provider brand validation
 * @see API-346
 * @return Boolean
 */
function isPaymentProviderBrandValidationEnabled($provider)
{
  if ($provider == 'EPAY')
    return ! (time() > strtotime('2016-02-01') && time() < strtotime('2016-02-11')); // migration window
  else
    return TRUE;
}

/**
 * getAccCanActivateMessages
 *
 * get ACC messages returned by GetNetworkDetails call for an available (can activate) ICCID
 * @param Integer ResultCode returned by ACC QuerySubscriber
 * @return Array of messages
 */
function getAccCanActivateMessages($error)
{
  $messages = find_credential("amdocs/canactivate/querysubscriber/$error");

  // preserve compatability with pre 2015-02-21 TMO release by providing default values
  switch ($error)
  {
    case 200:
      $messages = $messages ? json_decode($messages) : array('396 : The ICCID/IMSI does not belong to the Channel ID');
      break;
    case 202:
      $messages = $messages ? json_decode($messages) : array('1000161 : SIM status invalid', '1042 : SIM status invalid');
      break;
    default:
      dlog('', "unknown error code $error");
      $messages = NULL;
  }
  
  dlog('', "error: $error -> messages: %s", $messages);
  return $messages;
}

/**
 * getTMOEncryptionKey
 *
 * @return string
 */
function getTMOEncryptionKey()
{
  return find_credential('tmo/encryption/key');
}

/**
 * getSessionTokenEncryptionKey
 *
 * @return string
 */
function getSessionTokenEncryptionKey()
{
  return find_credential('session/token/encryption/key');
}

/**
 * getSecurityEnvironment
 *
 * @return string
 */
function getSecurityEnvironment()
{
  return find_credential_or_config('security/env');
}

/**
 * getAllowedPartners
 *
 * Returns the list of allowed API partners for the current environment
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/API+Standard+Guide+1.0
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/API+Standard+Guide+2.0
 *
 * @return array
 */
function getAllowedPartners()
{
  $allowed = find_config('partner/api/allow');
  if ( empty( $allowed ) )
    return array();
  else
    return explode( ' ', $allowed );
}

/**
 * find_credential_or_config
 *
 * Finds a token value from credentials or configuration
 *
 * @return string
 */
function find_credential_or_config( $token , $warn_if_empty=FALSE )
{
  $value = find_credential( $token );

  if ( empty( $value ) )
    $value = find_config( $token );

  if ( $warn_if_empty && empty( $value ) )
    \logWarn( "MISSING ENVIRONMENT CONFIGURATION TOKEN : ".$token );

  return $value;
}

/**
 * find_token_from_file
 *
 * Extract a value from a file
 *
 * @return string
 */
function find_token_from_file( $token )
{
  $value = NULL;

  $file = find_credential_or_config( $token );

  if ( empty( $file ) )
    return $value;

  try
  {
    $value = trim(file_get_contents($file),"\n\r");
  }
  catch( \Exception $e )
  {
    \logFatal( "Could not open file $file - token $token - " . $e->getMessage() );
  }

  return $value;
}

/**
 * getTransitionDelayTime
 *
 * @return integer
 */
function getTransitionDelayTime($transition_label)
{
  $map = array(
    'internal_func_delayed_ensure_suspend' => 60
  );

  $delay = NULL;
  foreach ($map as $label => $seconds)
  {
    if ($label == $transition_label)
      $delay = $seconds;
  }

  return ($delay) ? $delay : 0;
}

/**
 * getValidBOGOTypes
 *
 * @return array
 */
function getValidBOGOTypes()
{
  return array(BILLING_OPTION_ATTRIBUTE_BOGO_MONTH, BILLING_OPTION_ATTRIBUTE_7_11_BOGO);
}

/**
 * getMessengerRunnerDelayTimes
 *
 * @return array
 */
function getMessengerRunnerDelayTimes()
{
  return array(
    0 => SECONDS_IN_MINUTE,
    1 => SECONDS_IN_MINUTE * 5,
    2 => SECONDS_IN_MINUTE * 30,
    3 => SECONDS_IN_HOUR,
    4 => SECONDS_IN_HOUR * 2,
    5 => SECONDS_IN_HOUR * 4
  );
}

/**
 * getEmuSwitchDate
 *
 * @return string
 */
function getEmuSwitchDate()
{
  return '2015-09-23';
}

/**
 * getInsufficientFundsErrorCodes
 *
 * @return array
 */
function getInsufficientFundsErrorCodes()
{
  return array('051','005');
}

/**
 * existsBrandId
 *
 * check if the given brand ID exists in this environment
 *
 * @return Boolean
 */
function existsBrandId($id)
{
  // get brand definitions
  $brands = find_credential('brands');

  if (empty($brands))
  {
    \logFatal('missing brand defititions');
    return FALSE;
  }

  $existsBrandId = FALSE;

  foreach( $brands as $brand )
    if ( $brand['id'] == $id )
      $existsBrandId = TRUE;

  return $existsBrandId;
}

/**
 * isBrandAllowedByAPIPartner
 *
 * check if provided brand ID is allowed in this environment, for the given API partner
 *
 * @return Boolean TRUE if allowed, FALSE otherwise
 */
function isBrandAllowedByAPIPartner($id,$partner)
{
  // get brand definitions
  $brands = find_credential('brands');

  if (empty($brands))
  {
    \logError('missing brand defititions');
    return TRUE;
  }

  $isBrandAllowedByAPIPartner = FALSE;

  // check given brand ID against the list of allowed brand definitions ( if any )
  foreach( $brands as $brand )
    if ( $brand['id'] == $id )
      $isBrandAllowedByAPIPartner = empty($brand['api_partners']) || ! ! in_array($partner , explode( ' ' , $brand['api_partners'] ) );

  return $isBrandAllowedByAPIPartner;
}

/**
 * isBrandIdAllowed
 *
 * check if provided brand ID is allowed in this environment
 * @see API-275
 * @param  Integer brand ID
 * @return Boolean TRUE if allowed, FALSE otherwise
 */
function isBrandIdAllowed($id)
{
  // all brands are allowed in dev enviros
  if (isDevelopmentDB() || isDevelopmentHost())
    return TRUE;

  // get allowed brands: if not defined then all brands allowed
  $allowed = find_credential('partner/brands/allow');
  if (empty($allowed))
    return TRUE;

  // get brand definitions
  $brands = find_credential('brands');
  if (empty($brands))
  {
    logError('missing brand defititions');
    return FALSE;
  }

  // check given brand ID against the list of allowed brand definitions
  foreach (explode(' ', $allowed) as $name)
    if ( ! empty($brands[$name]['id']) && $brands[$name]['id'] == $id)
      return TRUE;

  // brand ID not found
  return FALSE;
}

/**
 * isBrandNameAllowed
 *
 * check if provided brand name is allowed in this environment
 * @see API-275
 * @param  String  brand name
 * @return Boolean TRUE if allowed, FALSE otherwise
 */
function isBrandNameAllowed($name)
{
  // all brands are allowed in dev enviros
  if (isDevelopmentDB() || isDevelopmentHost())
    return TRUE;

  // get allowed brands: if not defined then all brands allowed
  $allowed = find_credential('partner/brands/allow');
  if (empty($allowed))
    return TRUE;

  if (in_array($name, explode(' ', $allowed)))
    return TRUE;

  // brand name not found
  return FALSE;
}

/**
 * getBrandFromBrandId
 *
 * @param  Integer $brand_id
 * @return Array   of brand info
 */
function getBrandFromBrandId($brand_id)
{
  // get brand definitions
  $brands = find_credential('brands');

  if (is_array($brands))
  {
    foreach ($brands as $short_name => $brand)
    {
      if ( ! empty($brand['id']) && $brand['id'] == $brand_id)
      {
        $brand['short_name'] = $short_name;
        return $brand;
      }
    }
  }
  else
    \logFatal('Brands configuration missing');

  return NULL;
}

/**
 * getBrandIdFromBrand
 *
 * @param  String $brand (MINT, ULTRA, ...)
 * @return Integer $brand_id
 */
function getBrandIdFromBrand( $brand_short_name )
{
  $brand_id = NULL;

  // get brand definitions
  $brands = find_credential('brands');

  if (is_array($brands))
  {
    foreach ($brands as $short_name => $brand)
      if ( $short_name == $brand_short_name )
        $brand_id = $brand['id'];
  }
  else
    \logFatal('Brands configuration missing');

  return $brand_id;
}

/**
 * getBrandIdFromBrandName
 *
 * @param  String $brand_name (Mint, Univision, ...)
 * @return Integer $brand_id
 */
function getBrandIdFromBrandName( $brand_name )
{
  $brand_id = NULL;

  // get brand definitions
  $brands = find_credential('brands');

  if (is_array($brands))
  {
    foreach ($brands as $short_name => $brand)
      if ( $brand_name == $brand['brand_name'] )
        $brand_id = $brand['id'];
  }
  else
    \logFatal('Brands configuration missing');

  return $brand_id;
}

/**
 * getShortNameFromBrandId
 *
 * @param  Integer $brand_id
 * @return String  brand short name
 */
function getShortNameFromBrandId($brand_id)
{
  $brand = getBrandFromBrandId($brand_id);
  return ( $brand && ! empty($brand['short_name'])) ? $brand['short_name'] : NULL;
}

/**
 * getNameFromBrandId
 *
 * @param  Integer $brand_id
 * @return String  brand long name
 */
function getNameFromBrandId($brand_id)
{
  $brand = getBrandFromBrandId($brand_id);
  return ( $brand && ! empty($brand['brand_name'])) ? $brand['brand_name'] : NULL;
}

/**
 * getTwilioCredentials
 *
 * Returns Twilio Credentials
 *
 * @return array
 */
function getTwilioCredentials()
{
  $config = [
    'account_sid' => find_credential_or_config('twilio/sid'),
    'auth_token'  => find_credential_or_config('twilio/token'),
    'from_msisdn' => find_credential_or_config('twilio/from_msisdn')
  ];

  if (empty($config['account_sid']) || empty($config['auth_token']) || empty($config['from_msisdn'])) {
    return null;
  } else {
    return $config;
  }
}

/**
 * getEnvironment
 *
 * Returns the current environment
 *
 * @return string
 */
function getEnvironment()
{
  $environment = find_credential_or_config('environment/stage');

  if ( ! $environment)
    dlog('','ERROR ESCALATION ALERT DAILY - we could not determine environment/stage');

  return $environment;
}

/**
 * isDevEnvironment
 * @return Boolean
 */
function isDevEnvironment()
{
  return (getEnvironment() == 'DEV');
}

/**
 * isQAEnviroment
 * @return Boolean
 */
function isQaEnvironment()
{
  return (getEnvironment() == 'QA');
}

/**
 * isProdEnvironment
 * @return Boolean
 */
function isProdEnvironment()
{
  return (getEnvironment() == 'PROD');
}

/**
 * getDevMiddlewareEndpoint
 *
 * returns middleware endpoint for development
 *
 * @return string
 */
function getDevMiddlewareEndpoint()
{
  return find_credential('acc/notifications/propagate/endpoint/DEV');
}

/**
 * getQAMiddlewareEndpoint
 *
 * returns middleware endpoint for QA
 *
 * @return string
 */
function getQAMiddlewareEndpoint()
{
  return find_credential('acc/notifications/propagate/endpoint/QA');
}

/**
 * getCrmIpAddresses
 * return a list of CRM IP addresses (hosted by Shore as of Jan 2016)
 * this data is used to validate CRM access to sensitive API calls
 * @return Array IP v4
 */
function getCrmIpAddresses()
{
  return explode(' ', find_credential('shore/ip_addresses')); // space delimited IP v4 addresses
}

/**
 * getMobileLoginUrl
 * given a brand ID return mobile site URL for login action (e.g. 'https://m.ultra.me/#/t')
 * used in conjuction with Session security token V3 to login subscriber without entering credentials
 * @param Integer brand ID
 * @return String URL on success or NULL on failure
 */
function getMobileLoginUrl($brandId)
{
  $brand = getBrandFromBrandId(empty($brandId) ? 1 : $brandId); // brand defaults to Ultra
  return find_credential("mobile/login_url/{$brand['short_name']}");
}

/**
 * getMinutesBySubplan
 * returns modified [intl_minutes,unlimited_intl_minutes]
 * @param  string $plan    ex. UV30
 * @param  string $subplan ex. B
 * @return array [intl_minutes,unlimited_intl_minutes]
 */
function getMinutesBySubplan($plan, $subplan)
{
  $map = array(
    // intl_minutes, unlimited_intl_minutes
    'UV30' => array('A' => array(1100, 10000)),
    'UV35' => array('A' => array(1350, 10000)),
    'UV45' => array('A' => array(750,  10000)),
    'UV55' => array('A' => array(1275, 10000))
  );

  return (isset($map[$plan][$subplan])) ? $map[$plan][$subplan] : null;
}

/**
 * getDealerLocatorThresholds
 * returns settings which control Dealer Store Locator user experience
 * @see API-381
 * @return Array
 */
function getDealerLocatorThresholds()
{
  return array
  (
    'LOCATOR_ACTIVATION_THRESHOLD'  => find_credential('ultra/locator/activation/threshold'),
    'LOCATOR_MILE_THRESHOLD'        => find_credential('ultra/locator/mile/threshold'),
    'LOCATOR_DAYS_THRESHOLD'        => find_credential('ultra/locator/days/threshold')
  );
}

/**
 * getMindDataAddonThreshold
 *
 * Percentage of data add on left, under which we can remove Mint data add on SOCs
 *
 * @return integer
 */
function getMindDataAddonThreshold()
{
  return find_credential('mint/data/add_on/threshold');
}

/**
 * Get the renewal days for 12 month plans from months utilized.
 *
 * @param $month
 * @return bool|mixed
 */
function mintTwelveMonthRenewalOffsets($month)
{
  $offsets = [
    1  => 30,
    2  => 31,
    3  => 30,
    4  => 31,
    5  => 30,
    6  => 31,
    7  => 30,
    8  => 31,
    9  => 30,
    10 => 31,
    11 => 30,
    12 => 30
  ];

  if (array_key_exists($month, $offsets))
  {
    return $offsets[$month];
  }

  return false;
}

function mintMonthIntToString($monthInt)
{
  $monthIntToString = [
    1 => 'ONE',
    3 => 'THREE',
    6 => 'SIX',
    12 => 'TWELVE'
  ];

  if (array_key_exists($monthInt, $monthIntToString))
  {
    return $monthIntToString[$monthInt];
  }

  return false;
}

function planMonthsByCosId($cos_id)
{
  $map = \plan_months_map();
  return (isset($map[$cos_id])) ? $map[$cos_id] : null;
}

function planDaysByCosId($cos_id)
{
  $total_months = \Ultra\UltraConfig\planMonthsByCosId($cos_id);
  return ($total_months == 12) ? 365 : $total_months * 30;
}

function isMintPlan($cos_id)
{
  return (in_array($cos_id,
    [
      COSID_MINT_ONE_MONTH_S,
      COSID_MINT_ONE_MONTH_M,
      COSID_MINT_ONE_MONTH_L,
      COSID_MINT_THREE_MONTHS_S,
      COSID_MINT_THREE_MONTHS_M,
      COSID_MINT_THREE_MONTHS_L,
      COSID_MINT_SIX_MONTHS_S,
      COSID_MINT_SIX_MONTHS_M,
      COSID_MINT_SIX_MONTHS_L,
      COSID_MINT_TWELVE_MONTHS_S,
      COSID_MINT_TWELVE_MONTHS_M,
      COSID_MINT_TWELVE_MONTHS_L
    ]
  ));
}

function isBPlan($cos_id)
{
  return (in_array($cos_id,
    [
      COSID_ULTRA_BNINETEEN,
      COSID_ULTRA_BTWENTY_NINE,
      COSID_ULTRA_BTHIRTY_NINE,
      COSID_ULTRA_BFORTY_NINE
    ]
  ));
}

function isABPlan($cos_id)
{
  return in_array($cos_id, [
    '100000',
    '100001',
    '100002',
    '100003',
    '100004',
    '100005',
    '100006',
    '100007'
  ]);
}

function isUnlimitedABPlan($cos_id)
{
  return in_array($cos_id, [
    '100003',
    '100004',
    '100007'
  ]);
}

function getMintTestSimRanges()
{
  $arr = [];

  $ranges = explode('|', find_credential('mint/sim/test/list'));
  foreach ($ranges as $range)
    $arr[] = explode(',', $range);

  return $arr;
}

function getMintTestSimWhiteList()
{
  $arr = [];

  $ranges = explode('|', find_credential('mint/sim/test/whitelist'));
  foreach ($ranges as $range)
    $arr[] = explode(',', $range);

  return $arr;
}

function getMintTestSimTTL()
{
  return find_credential('mint/sim/test/delete/after/days');
}

function getDefaultThrottleSpeed()
{
  return 'full';
}

function throttleSpeedMarketingTerms($id)
{
  $map = [
    'full' => 'Full Speed',
    '1536' => 'Optimized - 1536 kbps',
    '1024' => 'Super Saver - 1024 kbps'
  ];

  return (isset($map[$id])) ? $map[$id] : null;
}

function getUpGlobeMOU()
{
  // TODO: cfgengine value
  return 240000;
}

function getCCProcessors()
{
  $value = find_credential('cc/processors');
  return !empty($value) ? json_decode($value, true) : [];
}

function getCCProcessorByName($name)
{
  $name = strtolower($name);
  $processors = getCCProcessors();
  return !empty($processors[$name]) ? $processors[$name] : [];
}

function getCCProcessorIDByName($name)
{
  $name = strtolower($name);
  $processors = getCCProcessors();
  return !empty($processors[$name]) && !empty($processors[$name]['id']) ? $processors[$name]['id'] : null;
}

function getPaymentAPIConfig()
{
  $value = find_credential('services/payment/config');
  return !empty($value) ? json_decode($value, true) : [];
}

function getOnlineSalesAPIConfig()
{
  $value = find_credential('services/online-sales/config');
  return !empty($value) ? json_decode($value, true) : [];
}

function getLaunchDarklySdkKey()
{
  //TODO add launchdarkly/sdk-key to cfengine.
  $value = find_credential('launchdarkly/sdk-key');

  if (empty($value)) {
    dlog('', 'The LaunchDarkly sdk key is missing from cfengine.');
    $value = '';
    $value = 'sdk-14178f5e-7b36-4489-8039-89484caf8998';
    return $value;
  }

  return $value;
}

function getFamilyAPIConfig()
{
  $value = find_credential('services/family/config');
  return !empty($value) ? json_decode($value, true) : [];
}

function getSharedILDConfig()
{
  $value = find_credential('services/shared_ild/config');
  return !empty($value) ? json_decode($value, true) : [];
}

function getSharedDataConfig()
{
  $value = find_credential('services/shared_data/config');
  return !empty($value) ? json_decode($value, true) : [];
}

function getUserDeviceAPIConfig()
{
  $value = find_credential('services/user_device/config');
  return !empty($value) ? json_decode($value, true) : [];
}

function isFlexPlan($check)
{
  return in_array($check, [
    'ULTRA_FLEX_FIFTEEN',
    'UF15',
    '1000000090'
  ]);
}

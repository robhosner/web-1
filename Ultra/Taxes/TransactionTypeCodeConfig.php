<?php
namespace Ultra\Taxes;

use Ultra\Configuration\Repositories\Cfengine\ConfigurationRepository;
use Ultra\Exceptions\MissingRequiredParametersException;

class TransactionTypeCodeConfig
{
  private $brandConf = [
    'ULTRA' => [
      'PLAN'        => null,
      'DATA'        => null,
      'IDDCA'       => null,
      'ROAM'        => null,
      'GLOBE'       => null,
      'ADD_BALANCE' => null,
      'LINE_CREDIT' => null,
      'REPLACE_SIM' => null
    ],
    'MINT' => [
      'PLAN'        => null,
      'DATA'        => null,
      'ROAM'        => null,
      'ADD_BALANCE' => null,
      'REPLACE_SIM' => null
    ]
  ];

  private $brandPrefix = [
    'ULTRA' => 'sureTax/TransactionTypeCode/ULTRA/',
    'MINT' => 'sureTax/TransactionTypeCode/MINT/',
  ];

  public function __construct(ConfigurationRepository $configurationRepository)
  {
    foreach ($this->brandConf as $brand => $items) {
      foreach ($items as $brandItem => $value) {
        $this->brandConf[$brand][$brandItem] = $configurationRepository->findConfigByKey($this->brandPrefix[$brand] . $brandItem);

        if (empty($this->brandConf[$brand][$brandItem])) {
          throw new MissingRequiredParametersException(__CLASS__, array_keys($this->brandConf[$brand]));
        }
      }
    }
  }

  public function toArray()
  {
    return $this->brandConf;
  }
}

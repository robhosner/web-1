<?php

require_once 'PHPUnit/Autoload.php';
require_once((getenv('HTT_CONFIGROOT') . '/e.php'));
require_once('web.php');
require_once('test/api/test_api_include.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');

/**
 * Tests for 'provisioning' partner APIs
 */
class ProvisioningTest extends PHPUnit_Framework_TestCase
{
  // a valid orange SIM ICCID
  const TEST_ORANGE_ICCID_19 = '8901260842116671844'; # SELECT TOP 1 ICCID_FULL FROM HTT_INVENTORY_SIM WHERE PRODUCT_TYPE = 'ORANGE' AND ACT_CODE IS NOT NULL AND CUSTOMER_ID IS NULL;

  // a valid purple SIM ICCID
  const TEST_PURPLE_ICCID_19 = '8901260842102000305'; # SELECT TOP 1 ICCID_FULL FROM HTT_INVENTORY_SIM WHERE PRODUCT_TYPE = 'PURPLE' AND ACT_CODE IS NULL AND CUSTOMER_ID IS NULL AND CREATED_BY != 'Raf testing';


  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';


  /**
   * The setUp() and tearDown() template methods are run once for each test method (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/pr/provisioning/1/ultra/api/provisioning__';

    // our credentials
    $this->apache_username = 'dougmeli';
    $this->apache_password = 'Flora';

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }


  /**
   **************************************************************
   * Test for provisioning__requestProvisionPortedCustomerAsync *
   **************************************************************
   */
  public function test__provisioning__requestProvisionPortedCustomerAsync ()
  {
    $url = $this->base_url . 'requestProvisionPortedCustomerAsync';
/*
    $defaultTester = new DefaultUltraAPITest( $url , $this );

    $defaultTester->runDefaultTests();


    $defaultParamsRequestProvisionPortedCustomerAsync = array(
      "loadPayment"            => "NONE",
      "targetPlan"             => "L29",
      "activation_masteragent" => "123",
      "activation_distributor" => "123",
      "activation_agent"       => "123",
      "activation_userid"      => "123",
      "numberToPort"           => "1234512345",
      "customerZip"            => "12345",
      'portAccountNumber'      => 'portAccountNumber',
      'portAccountPassword'    => 'portAccountPassword',
      "ICCID"                  => self::TEST_ORANGE_ICCID_19
    );

    $requiredParams = array(
      "loadPayment",
      "ICCID",
      "targetPlan",
      "numberToPort",
      'portAccountNumber',
      "customerZip",
      "activation_masteragent",
      "activation_agent",
      "activation_userid"
    );

    $defaultTester->runMissingParameterTests(
      $defaultParamsRequestProvisionPortedCustomerAsync,
      $requiredParams
    );

    // successful test with bolt ons
    $params = array(
      "loadPayment"            => "NONE",
      "targetPlan"             => "L29",
      "activation_masteragent" => "123",
      "activation_distributor" => "123",
      "activation_agent"       => "123",
      "activation_userid"      => "123",
      "numberToPort"           => "3475690034",
      "customerZip"            => "12345",
      'portAccountNumber'      => 'portAccountNumber',
      'portAccountPassword'    => '123456',
      "ICCID"                  => '',
      'bolt_ons'               => 'DATA_2048_20');
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
*/

    // DATAQ-104
    $params = array(
      "loadPayment"             => 'NONE',
      "targetPlan"              => "L34",
      "activation_masteragent"  => "123",
      "activation_distributor"  => "123",
      "activation_agent"        => "123",
      "activation_userid"       => "123",
      "numberToPort"            => "5617020745",
      "customerZip"             => "80130",
      'portAccountNumber'       => '838695269',
      'portAccountPassword'     => '2009',
      "ICCID"                   => '8901260842107317167',
      'bolt_ons'                => 'DATA_2048_20',
      "activation_masteragent"  => "54",
      "activation_agent"        => "28",
      "customerFirstName"       => "DC",
      "customerLastName"        => "SPG",
      "activation_userid"       => "67",
      "dealer_code"             => "UMTEST");
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";

  }


  /**
   ***********************************************************
   * Test for provisioning__requestProvisionNewCustomerAsync *
   ***********************************************************
   */
  public function test__provisioning__requestProvisionNewCustomerAsync ()
  {
    $url = $this->base_url . 'requestProvisionNewCustomerAsync';
/*
    $defaultTester = new DefaultUltraAPITest( $url , $this );

    $defaultTester->runDefaultTests();


    $defaultParamsRequestProvisionNewCustomer = array(
      "loadPayment"            => "NONE",
      "targetPlan"             => "L29",
      "activation_masteragent" => "123",
      "activation_distributor" => "123",
      "activation_agent"       => "123",
      "activation_userid"      => "123",
      "customerZip"            => "12345",
      "customerEMail"          => "null@null",
      "customerLastName"       => __CLASS__,
      "ICCID"                  => self::TEST_PURPLE_ICCID_19
    );


    // test non-purple ICCID
    ////////////////////////
    $params = array_merge(
      array(),
      $defaultParamsRequestProvisionNewCustomer
    );

    $params['ICCID'] = self::TEST_ORANGE_ICCID_19;

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! $decoded_response->success );
    $this->assertEquals( $decoded_response->errors[0] , "ERR_API_PRODUCT: This SIM cannot be activated by this process." );


    // test invalid loadPayment
    ///////////////////////////
    $params = array_merge(
      array(),
      $defaultParamsRequestProvisionNewCustomer
    );

    $params['loadPayment'] = 'invalid';

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! $decoded_response->success );
    $this->assertEquals( $decoded_response->errors[0] , "ERR_API_INVALID_ARGUMENTS: loadPayment value has no matches" );


    // test invalid targetPlan
    //////////////////////////
    $params = array_merge(
      array(),
      $defaultParamsRequestProvisionNewCustomer
    );

    $params['targetPlan'] = 'invalid';

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! $decoded_response->success );
    $this->assertEquals( $decoded_response->errors[0] , "ERR_API_INVALID_ARGUMENTS: targetPlan value has no matches" );


    // test missing parameters
    //////////////////////////
    $requiredParams = array(
      "ICCID",
      "loadPayment",
      "targetPlan",
      "customerZip",
      "activation_masteragent",
      "activation_agent",
      "activation_userid"
    );

    $defaultTester->runMissingParameterTests(
      $defaultParamsRequestProvisionNewCustomer,
      $requiredParams
    );
*/
    // DATAQ-104
    $params = array(
      'ICCID'                   => '8901260842107739543',
      "loadPayment"             => 'PINS',
      'pinsToApply'             => '7848219686',
      'targetPlan'              => 'L34',
      'customerZip'             => '11211',
      'activation_masteragent'  => 11,
      'activation_agent'        => 11,
      'activation_userid'       => 100);
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue($decoded_response->success);
/*
    // DATAQ-20: L19 activation
    $params = array(
      "ICCID"                   => '8901260842107739477',
      "loadPayment"             => 'PINS',
      'pinsToApply'             => '7848219686',
      "targetPlan"              => "L19",
      "activation_masteragent"  => "123",
      "activation_distributor"  => "123",
      "activation_agent"        => "123",
      "activation_userid"       => "123",
      "customerZip"             => "12345",
      "customerEMail"           => "null@null",
      "customerLastName"        => __CLASS__);
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertNotEmpty($json_result);
    $this->assertTrue($decoded_response->success);
*/
  }


  /**
   *********************************************************
   * Test for provisioning__ProvisionBasicNewCustomerAsync *
   *********************************************************
   */
  public function test__provisioning__ProvisionBasicNewCustomerAsync ()
  {
    $url = $this->base_url . 'ProvisionBasicNewCustomerAsync';

    $defaultTester = new DefaultUltraAPITest( $url , $this );

    $defaultTester->runDefaultTests();


    $defaultParamsProvisionBasicNewCustomerAsync = array(
      'customerZip'         => '12345',
      'targetPlan'          => 'L29',
      'ICCID'               => self::TEST_PURPLE_ICCID_19
    );


    // test non-purple ICCID
    ////////////////////////
    $params = array_merge(
      array(),
      $defaultParamsProvisionBasicNewCustomerAsync
    );

    $params['ICCID'] = self::TEST_ORANGE_ICCID_19;

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! $decoded_response->success );
    $this->assertEquals( $decoded_response->errors[0] , "ERR_API_PRODUCT: This SIM cannot be activated by this process." );


    // test invalid plan
    ////////////////////
    $params = array_merge(
      array(),
      $defaultParamsProvisionBasicNewCustomerAsync
    );

    $params['targetPlan'] = 'invalid';

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! $decoded_response->success );
    $this->assertEquals( $decoded_response->errors[0] , "ERR_API_INVALID_ARGUMENTS: targetPlan value has no matches" );


    // test invalid zip
    ///////////////////
    $params = array_merge(
      array(),
      $defaultParamsProvisionBasicNewCustomerAsync
    );

    $params['customerZip'] = 'invalid';

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! $decoded_response->success );
    $this->assertRegExp( '/ERR_API_INVALID_ZIP/', $decoded_response->errors[0] );


    // test invalid preferred_language
    //////////////////////////////////
    $params = array_merge(
      array(),
      $defaultParamsProvisionBasicNewCustomerAsync
    );

    $params['preferred_language'] = 'invalid';

    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);

    echo "\n$json_result\n";

    $decoded_response = json_decode($json_result);

    $this->assertNotEmpty( $json_result );
    $this->assertNotEmpty( $decoded_response );
    $this->assertTrue( ! $decoded_response->success );
    $this->assertEquals( $decoded_response->errors[0] , "ERR_API_INVALID_ARGUMENTS: preferred_language value has no matches" );

    // DATAQ-104
    $params = array(
      'ICCID'       => '1010101010101010390',
      'targetPlan'  => 'L34',
      'customerZip' => '10001');
    $json_result = curl_post($url,$params,$this->curl_options,NULL,240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue($decoded_response->success);
  }
}

?>

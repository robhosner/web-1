<?php

require_once('web.php');
require_once('Ultra/tests/API/TestConstants.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');
require_once('Ultra/tests/API/MvneTest.php');

/**
 * Tests for 'celluphone' partner APIs v1
 */
class CelluphoneTest extends PHPUnit_Framework_TestCase
{
  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';


  /**
   * The setUp() and tearDown() template methods are run once for each test method
   * (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // all APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url() . '/pr/celluphone/1/ultra/api/';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }


  /*
   * test_customercare__ReplaceSIMCardByMSISDN
   */
  public function test_customercare__ReplaceSIMCardByMSISDN()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // MVNE tests: we cannot run positive test because API changes DB
    $params = array(
      'msisdn' => '5555555555',
      'new_ICCID' => TEST_ICCID_MVNE_2,
      'store_id' => 1,
      'user_id' => 1001);
    $mvneTester = new MvneTest($url, $this);
    $mvneTester->invalidIccidTest($params);
  }

  /*
   * test_provisioning__CancelRequestedPort
   */
  public function test_provisioning__CancelRequestedPort()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // MVNE tests
    $mvneTester = new MvneTest($url, $this);
    $mvneTester->invalidIccidTest(array('ICCID' => TEST_ICCID_MVNE_2, 'numberToPort' => '5555555555'));
  }


  /*
   * test_provisioning__checkSIMCard
   */
  public function test_provisioning__checkSIMCard()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // MVNE tests
    $mvneTester = new MvneTest($url, $this);
    $mvneTester->invalidIccidTest(array('ICCID' => TEST_ICCID_MVNE_2));
  }

}

?>

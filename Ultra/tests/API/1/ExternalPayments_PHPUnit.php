<?php

require_once('web.php');
require_once('Ultra/tests/API/TestConstants.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');

/**
 * Tests for 'externalpayments' partner APIs v1
 */
class ExternalPaymentsTest extends PHPUnit_Framework_TestCase
{
  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';

  /**
   * The setUp() and tearDown() template methods are run once for each test method
   * (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/pr/ePay/1/ultra/api/';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }


  /*
   * test_externalpayments__CheckMobileBalance
   */
  public function test_externalpayments__CheckMobileBalance()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // invalud MSISDN
    $params = array(
      'phone_number'    => '9999999999',
      'request_epoch'   => time(),
      'store_zipcode'   => '11211',
      'terminal_id'     => 1);
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->AssertTrue( ! $response->success);

    // successful test
    $params['phone_number'] = '9739314917';
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\nURL: $url\nJSON: $result\n";
    $response = json_decode($result);
    $this->assertTrue($response->success);
    $this->assertNotEmpty($response->dealer_code);

  }

}

?>

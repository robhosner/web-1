<?php

require_once('web.php');
require_once('Ultra/tests/API/TestConstants.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');
require_once('Ultra/tests/API/MvneTest.php');

/**
 * Tests for 'api_public' partner APIs v1
 */
class ApiPublicTest extends PHPUnit_Framework_TestCase
{
  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';

  /**
   * The setUp() and tearDown() template methods are run once for each test method
   * (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // all APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url() . '/pr/api_public/1/ultra/api/';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }


  /**
   * test_provisioning__requestProvisionOrangeCustomerAsync
   */
  public function test_provisioning__requestProvisionOrangeCustomerAsync()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // failure due to zip coverage error
    $params = array(
      'actcode'            => '22222222220',
      'customerZip'        => '00001',
      'preferred_language' => 'ES',
      'activation_channel' => 'website',
      'activation_info'    => 'IP',
      'zsession'           => '03ED503E-3B54-FF13-32C4-2E024A0DF108'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );

    // failure due to SIM already used
    $params = array(
      'actcode'            => '22222222220',
      'customerZip'        => '10001',
      'preferred_language' => 'ES',
      'activation_channel' => 'website',
      'activation_info'    => 'IP',
      'zsession'           => '03ED503E-3B54-FF13-32C4-2E024A0DF108'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );
  }


  /**
   * test_portal__ReplaceSIMCard
   */
  public function test_portal__ReplaceSIMCard()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // test non existing ICCID
    $params = array(
      'zsession'  => createCustomerSession('sub3530', 'sub3530'),
      'old_ICCID' => '890126080000003187',
      'new_ICCID' => '8901260842107739337'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );

    // test in-Active subscriber
    $params = array(
      'zsession'  => createCustomerSession('sub3530', 'sub3530'),
      'old_ICCID' => '890126084210773187',
      'new_ICCID' => '8901260842107739337'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success );
    $this->assertContains('ERR_API_INVALID_ARGUMENTS: subscriber is not Active' , $decoded_response->errors );
  }


  /**
   * test_provisioning__requestActivateShippedNewCustomerAsync
   */
  public function test_provisioning__requestActivateShippedNewCustomerAsync()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // invalid iccid
    $params = array(
      'ICCID'    => '9901966665444431234',
      'zsession' => '1001'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse( $decoded_response->success );

    // already used iccid
    $params = array(
      'ICCID'    => '8901260842107552433',
      'zsession' => '1001'
    );
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse( $decoded_response->success );

    // MVNE tests: we cannot run positive test because API changes DB
    $params = array(
      'ICCID' => TEST_ICCID_MVNE_2,
      'zsession' => '1001'
    );
    $mvneTester = new MvneTest($url, $this);
    $mvneTester->invalidIccidTest($params);
  }


  /**
   * provisioning__requestPortFundedCustomerAsync
   */
  public function test_provisioning__requestPortFundedCustomerAsync()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // MVNE tests: we cannot run positive test because API changes DB
    $params = array(
      'ICCID' => TEST_ICCID_MVNE_2,
      'zsession' => 1,
      'portAccountNumber' => '1001',
      'numberToPort' => '5555555555');
    $mvneTester = new MvneTest($url, $this);
    $mvneTester->invalidIccidTest($params);
  }


  /**
   * test_provisioning__requestResubmitProvisionPortedCustomerAsync
   */
  public function test_provisioning__requestResubmitProvisionPortedCustomerAsync()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // successfull test
    $params = array(
      'ICCID'               => '8901260962103786700',
      'numberToPort'        => '9493944502',
      'portAccountNumber'   => '12345',
      'portAccountZipcode'  => '11211');
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    $decoded_response = json_decode($json_result);
    echo "\n$json_result\n";
    $this->assertTrue( $decoded_response->success );
    $this->assertNotEmpty($decoded_response->transitions);
  }

}


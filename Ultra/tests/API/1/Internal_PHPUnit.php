<?php

require_once('web.php');
require_once('Ultra/tests/API/TestConstants.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');
require_once('Ultra/tests/API/MvneTest.php');

/**
 * Tests for 'internal' partner APIs v1
 */
class InternalTest extends PHPUnit_Framework_TestCase
{
  public $base_url        = '';
  public $apache_username = '';
  public $apache_password = '';
  public $curl_options    = '';

  /**
   * The setUp() and tearDown() template methods are run once for each test method
   * (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // all APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url() . '/pr/internal/1/ultra/api/';

    // our credentials
    $this->apache_username = TEST_APACHE_USER;
    $this->apache_password = TEST_APACHE_PASS;

    // options for our curl commands
    $this->curl_options = array(
      CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
      CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }

  /**
   * test_internal__GetState
   */
  public function test_internal__GetState()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test_'));

    // default tests
    $defaultTester = new DefaultUltraAPITest( $url , $this );
    $defaultTester->runDefaultTests();

    // test for invalid subscriber
    $params = array('customer_id' => 123435);
    $result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$result\n";
    $json = json_decode($result);
    $this->assertTrue( ! $json->success);
  }

  
}

?>

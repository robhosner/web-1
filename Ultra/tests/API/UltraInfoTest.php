<?php

require_once 'PHPUnit/Autoload.php';
require_once((getenv('HTT_CONFIGROOT') . '/e.php'));
require_once('web.php');
require_once('test/api/test_api_include.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');

/**
 * Tests for 'ultrainfo' partner APIs
 */
class UltraInfoTest extends PHPUnit_Framework_TestCase
{
  /**
   * The setUp() and tearDown() template methods are run once for each test method (and on fresh instances) of the test case class.
   */
  public function setUp ()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/ultra_api.php?bath=rest&partner=ultrainfo&version=2&command=';

    // our credentials
    $this->apache_username = 'dougmeli';
    $this->apache_password = 'Flora';

    // options for our curl commands
    $this->curl_options = array(
        CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC);
  }

  /**
   * test__ultrainfo_GetOutageInfo
   */
  public function test__ultrainfo__GetOutageInfo()
  {
    // API setup
    $url = $this->base_url . substr(__FUNCTION__, strlen('test__'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url, $this);
    $defaultTester->runDefaultTests();

    // no parameters are required
    $params = array();
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue($decoded_response->success);
    $this->assertFalse($decoded_response->enabled);
    $this->assertNotEmpty($decoded_response->message);
    $this->assertFalse($decoded_response->comprehensive);
  }
}

?>

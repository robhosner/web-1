<?php

/*
Test for Ultra/Lib/MVNE/Adapter.php

Usage:
php Ultra/tests/MVNE/test_Adapter.php mvneChangeMSISDN        $OLD_MSISDN $ICCID $ZIPCODE $CUSTOMER_ID
php Ultra/tests/MVNE/test_Adapter.php mvneEnsureSuspend       $CUSTOMER_ID
php Ultra/tests/MVNE/test_Adapter.php mvneChangePlanImmediate $CUSTOMER_ID $PLAN
php Ultra/tests/MVNE/test_Adapter.php mvneEnsureDeactivate    $CUSTOMER_ID
*/


require_once 'db.php';
require_once 'lib/transitions.php';
require_once 'lib/util-common.php';
require_once 'Ultra/Lib/MVNE/Adapter.php';


teldata_change_db();


abstract class AbstractTestStrategy
{
  abstract function test( $argv );
}


class Test_mvneChangePlanImmediate extends AbstractTestStrategy
{
  function test( $argv )
  {
    $customer_id = $argv[2];
    $plan        = $argv[3];

    return mvneChangePlanImmediate( $customer_id , $plan );
  }
}

class Test_mvneChangeMSISDN extends AbstractTestStrategy
{
  function test( $argv )
  {
    $old_msisdn  = $argv[2];
    $iccid       = luhnenize( $argv[3] );
    $zipcode     = $argv[4];
    $customer_id = $argv[5];
    $source      = 'test';
    $account_id  = '1234';

    return mvneChangeMSISDN( $old_msisdn , $iccid , $zipcode , $customer_id , $source , $account_id );
  }
}


class Test_mvneActivateSIM extends AbstractTestStrategy
{
  function test( $argv )
  {
    die("TEST NOT IMPLEMENTED");
  }
}


/**
 * Test_mvneEnsureSuspend
 *
 * test function mvneEnsureSuspend
 */
class Test_mvneEnsureSuspend extends AbstractTestStrategy
{
  function test( $argv )
  {
    if (empty($argv[2]))
      die("ERROR: missing parameter CUSTOMER_ID \n");

    return mvneEnsureSuspend(getNewActionUUID('test ' . time()), $argv[2]);
  }
}

/**
 * Test_mvneEnsureDeactivate
 *
 * test function mvneEnsureDeactivate
 */
class Test_mvneEnsureDeactivate extends AbstractTestStrategy
{
  function test( $argv )
  {
    if (empty($argv[2]))
      die("ERROR: missing parameter CUSTOMER_ID \n");

    return mvneEnsureDeactivate(getNewActionUUID('test ' . time()), $argv[2]);
  }
}


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$result = $testObject->test( $argv );

print_r($result);


?>

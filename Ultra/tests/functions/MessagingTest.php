<?php

require_once 'db.php';

class MessagingTest extends PHPUnit_Framework_TestCase {
  const TEST_CUSTOMER = 1983;

  public function setUp () {
    require_once 'db.php';
    require_once 'lib/messaging/functions.php';

    teldata_change_db();
  }

  public function testSendActivationEmail () {
    send_activation_email (self::TEST_CUSTOMER);
  }

  public function testSendSMSResolutionRequired () {
    send_sms_resolution_required (self::TEST_CUSTOMER);
  }
}

?>

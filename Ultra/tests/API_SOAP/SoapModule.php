<?php
include_once 'lib/util-common.php';

/**
 *
 * @author john dohoney
 *        
 */
abstract class SoapModule
{

  public $client = null;

  // Authentication info
  private $username = 'dougmeli';
  private $password = 'Flora';

  /**
   *
   * @return the $client
   */
  public function getClient()
  {
    return $this->client;
  }

  /**
   *
   * @param string $test_wsdl          
   * @param string $childInterface          
   */
  function __construct($test_wsdl, $childInterface)
  {
    try
    {
      $this->client = new SoapClient($test_wsdl, array(
        'login' => $this->username,'password' => $this->password
      ));
      echo PHP_EOL . "Testing SOAP Interface for : " . $childInterface . PHP_EOL;
    }
    catch (SoapFault $e)
    {
      dlog("", $e->getTraceAsString());
    }
  }

  /**
   *
   * @param string $xml
   *          -- SOAP Response XML
   * @return string
   */
  function pretty($xml)
  {
    $domxml = new DOMDocument('1.0');
    $domxml->preserveWhiteSpace = false;
    $domxml->formatOutput = true;
    $domxml->loadXML($xml);
    return $domxml->saveXML();
  }

  abstract function params();

  abstract function execute(array $args=NULL);
}
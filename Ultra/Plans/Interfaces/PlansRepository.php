<?php
namespace Ultra\Plans\Interfaces;

use Ultra\Plans\Plan;

/**
 * Interface PlansRepository
 * @package Ultra\Plans\Interfaces
 */
interface PlansRepository
{
  /**
   * Get brand plans from cfengine.
   *
   * @param $brand
   * @return array|bool
   */
  public function getAllPlansByBrand($brand);

  /**
   * Get brand plan by planId from cfengine.
   *
   * @param $brand
   * @param $planId
   * @return Plan
   */
  public function getPlanByBrandAndPlanId($brand, $planId);
}
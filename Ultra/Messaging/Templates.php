<?php

namespace Ultra\Messaging\Templates;

require_once 'Ultra/Lib/Util/Locale.php';
require_once 'lib/JsonPretty.php';

define('MESSAGING_TEMPLATES_PATH', 'Ultra/Messaging/');

global $SMS_templates; // array by brand ID

// A very simple messages template system.
// Parameters are '__params__' . $param_name
// Languages currently supported: EN, ES, ZH

/**
 * SMS_by_language
 * Returns SMS template queried with $params[message_type] and $language
 *
 * @param array $params
 * @param string $language
 * @return string $SMS_template
 */
function SMS_by_language($params,$language='EN',$brand_id=1)
{
  // default language is English
  if ( empty($language) )
    $language='EN';

  global $SMS_templates;
  if (empty($SMS_templates[$brand_id]))
    $SMS_templates[$brand_id] = getAllTemplates(\Ultra\UltraConfig\getShortNameFromBrandId($brand_id));
  $brand_templates = $SMS_templates[$brand_id];

  // check if template is given or full message is present
  if (empty($params['message_type']) || empty($brand_templates[$params['message_type'] . '__' . $language]))
  {
    if (empty($params['message']))
    {
      if ($language != 'EN')
        return SMS_by_language($params, 'EN', $brand_id);

      dlog('', 'ERROR: unknown or missing SMS template and message');
      return NULL;
    }
    else
      return $params['message'];
  }

  // select template
  $SMS_template = $brand_templates[ $params['message_type'] . '__' . $language ];

  // fill template parameters
  $SMS_template = fill_template_values($params,$SMS_template);

  return $SMS_template;
}

/**
 * fill_template_values
 *
 * Returns SMS template queried with $params[message_type] and $language
 * Returned templates variables filled with values from $params
 *
 * @param  array $params kva
 * @param  string $SMS_template
 * @return string $SMS_template with vars filled
 */
function fill_template_values($params, $SMS_template)
{
  dlog('', "params = %s", $params);

  foreach($params as $name => $value)
  {
    // name of the template variable, example: __params__msisdn
    $template_param_name = '__params__' . $name;

    // replace the template variable with the $value given in $params
    if (preg_match('/' . $template_param_name . '/', $SMS_template))
    {
      // remove the empty placeholder space in the message if there is no value for the placeholder
      if ( ! isset($value) || $value === '')
        $SMS_template = str_replace(' ' . $template_param_name, '', $SMS_template);

      $value = preg_replace('/\$/', '\\\$', $value); // $ must be escaped
      $SMS_template = preg_replace('/' . $template_param_name . '/', $value, $SMS_template);
    }
  }

  // Obvious caveat: we cannot allow a param name to be a substring of another param name

  return $SMS_template;
}

/**
 * readJsonFile
 *
 * @return array
 */
function readJsonFile($brand='ULTRA')
{
  $templates = NULL;

  try
  {
    $templateFile = MESSAGING_TEMPLATES_PATH . $brand . '.json';
    if (file_exists($templateFile))
      $templates = json_decode(file_get_contents($templateFile));
    else
      \logError('Missing templates file ' . $templateFile);
  }
  catch (Exception $e)
  {
    dlog('', 'ERROR: ' . $e->getMessage());
  }

  return (is_null($templates)) ? array() : $templates;
}

/**
 * writeJsonFile
 * json encodes template array and saves to file
 *
 * @param  Array $templates
 * @return Boolean success?
 */
function writeJsonFile($templates, $brand='ULTRA')
{
  $success = FALSE;

  try
  {
    $templateFile = MESSAGING_TEMPLATES_PATH . $brand . '.json';
    $jsonPretty = new \Camspiers\JsonPretty\JsonPretty();
    $success = file_put_contents($templateFile, $jsonPretty->prettify($templates));
  }
  catch (Exception $e)
  {
    dlog('', 'ERROR: ' . $e->getMessage());
  }

  return ($success === FALSE) ? FALSE : TRUE;
}

/**
 * updateOrAddTemplate
 *
 * updates $template entry for $language with $text
 * writes to file
 *
 * @param  String $template
 * @param  String $language
 * @param  String $text
 * @return Boolean success?
 */
function updateOrAddTemplate($template, $language, $text)
{
  // validate parameters
  if (empty($template) || empty($language) || empty($text))
    return logError('invalid paramaters: ' . jsonEncode(func_get_args()));

  $templates = \Ultra\Messaging\Templates\readJsonFile();
  if ( ! count($templates))
    return logError('failed to load templates, possible JSON syntax error');

  // add object if a new template
  if (empty($templates->$template))
    $templates->$template = new \StdClass;

  $templates->$template->$language = is_language_encoded($language) ? convertUnicodeToJavaEscape($text) : $text;

  return \Ultra\Messaging\Templates\writeJsonFile($templates);
}

/**
 * removeTemplate
 *
 * removes $templates entry
 * writes to file
 * 
 * @param  String $template
 * @return Boolean success?
 */
function removeTemplate($template)
{
  // validate parameters
  if (empty($template))
    return logError('invalid paramaters: ' . jsonEncode(func_get_args()));

  $templates = \Ultra\Messaging\Templates\readJsonFile();
  if ( ! count($templates))
    return logError('failed to load templates, possible JSON syntax error');

  unset($templates->$template);

  return \Ultra\Messaging\Templates\writeJsonFile($templates);
}

/**
 * getTemplate
 *
 * retrieves text for $template entry in $language
 *
 * @param  String $template
 * @param  String $language
 */
function getTemplate($template, $language)
{
  $templates = \Ultra\Messaging\Templates\readJsonFile();

  return (isset($templates->$template->$language))
    ? convertUnicodeFromJavaEscape($templates->$template->$language)
    : NULL;
}

/**
 * getAllTemplates
 *
 * retrieves all templates
 *
 * @return Array $formatted templates
 */
function getAllTemplates($brand='ULTRA')
{
  $templates = \Ultra\Messaging\Templates\readJsonFile($brand);

  $formatted = array();

  foreach ($templates as $template => $langs)
  {
    foreach ($langs as $lang => $text)
      $formatted[$template . '__' . $lang] = convertUnicodeFromJavaEscape($text);
  }

  return $formatted;
}


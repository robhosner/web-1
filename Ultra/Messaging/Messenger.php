<?php
namespace Ultra\Messaging;

class Messenger
{
  public function enqueueConditionalSms($customerId, $messageType, $hoursDelay, array $messagingQueueParams, $daytime)
  {
    return \enqueue_conditional_sms($customerId, $messageType, $hoursDelay, $messagingQueueParams, $daytime);
  }

  public function enqueueDaytimeSms($customerId, $messageType, array $messagingQueueParams)
  {
    return \enqueue_daytime_sms($customerId, $messageType, $messagingQueueParams);
  }

  public function enqueueImmediateEmail($customerId, $templateName, array $messagingQueueParams)
  {
    return \enqueue_immediate_email($customerId, $templateName, $messagingQueueParams);
  }

  public function enqueueImmediateExternalSms($customerId, $messageType, array $messagingQueueParams)
  {
    return \enqueue_immediate_external_sms($customerId, $messageType, $messagingQueueParams);
  }

  public function enqueueImmediateSms($customerId, $messageType, array $messagingQueueParams)
  {
    return \enqueue_immediate_sms($customerId, $messageType, $messagingQueueParams);
  }

  public function enqueueOnceImmediateEmail($customerId, $messageType, array $messagingQueueParams, $hoursPeriod)
  {
    return \enqueue_once_immediate_email($customerId, $messageType, $messagingQueueParams, $hoursPeriod);
  }

  public function enqueueOnceImmediateExternalSms($customerId, $messageType, array $messagingQueueParams, $hoursPeriod)
  {
    return \enqueue_once_immediate_external_sms($customerId, $messageType, $messagingQueueParams, $hoursPeriod);
  }

  public function mvneBypassListAsynchronousSendSMS($message, $msisdn, $action)
  {
    return \mvneBypassListAsynchronousSendSMS($message, $msisdn, $action);
  }

  public function mvneBypassListSynchronousSendSMS($message, $msisdn, $action)
  {
    return \mvneBypassListSynchronousSendSMS($message, $msisdn, $action);
  }

  public function addToMessagingQueue(array $parameters)
  {
    return \htt_messaging_queue_add($parameters);
  }

  public function smsByLanguage(array $params, $language, $brand)
  {
    return \Ultra\Messaging\Templates\SMS_by_language($params, $language, $brand);
  }
}

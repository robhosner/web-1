<?php
namespace Ultra\Mvne\Commands;

use Ultra\Hydrator;
use Ultra\Mvne\UpgradePlanBase;

class RemoveWIFISocCommand extends UpgradePlanBase
{
  public $option;
  public $channel;

  use Hydrator;

  public function __construct(array $input)
  {
    $input['option'] = 'N-WFC|REMOVE';
    $this->channel = !empty($input['channel']) ? $input['channel'] : 'WIFI_CALLING.UNKNOWN';

    $this->hyrdateAllItems($input);
  }
}

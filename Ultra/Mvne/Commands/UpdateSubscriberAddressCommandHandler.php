<?php
namespace Ultra\Mvne\Commands;

use Ultra\Exceptions\MiddleWareException;
use Ultra\Lib\MiddleWare\Adapter\Control;

class UpdateSubscriberAddressCommandHandler extends MiddleWareCommandHandler
{
  /**
   * @var Control
   */
  private $middleWareAdapter;

  /**
   * AddWIFISocCommandHandler constructor.
   * @param Control $middleWareAdapter
   */
  public function __construct(Control $middleWareAdapter)
  {
    $this->middleWareAdapter = $middleWareAdapter;
  }

  /**
   * @param UpdateSubscriberAddressCommand $command
   * @throws MiddleWareException
   */
  public function handle(UpdateSubscriberAddressCommand $command)
  {
    $result = $this->middleWareAdapter->mwUpdateSubscriberAddress((array) $command);
    $this->handleMiddleWareResultCheck($result);
  }
}

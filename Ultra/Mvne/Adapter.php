<?php
namespace Ultra\Mvne;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\MiddleWare\Adapter\Control as AdapterControl;

class Adapter
{
  /**
   * @var AdapterControl
   */
  private $mwControl;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * Adapter constructor.
   * @param AdapterControl $mwControl
   * @param CustomerRepository $customerRepository
   * @param Configuration $configuration
   */
  public function __construct(AdapterControl $mwControl, CustomerRepository $customerRepository, Configuration $configuration)
  {
    $this->mwControl = $mwControl;
    $this->customerRepository = $customerRepository;
    $this->configuration = $configuration;
  }

  public function mvneMakeitsoUpdateThrottleSpeed($customer_id, $plan, $throttle_speed, $lteSelectMethod, $transition_uuid = NULL, $action_uuid = NULL)
  {
    return \mvneMakeitsoUpdateThrottleSpeed($customer_id, $plan, $throttle_speed, $lteSelectMethod, $transition_uuid, $action_uuid);
  }

  public function mvneMakeitsoRemoveFromBan($customerId, $plan, $actionUuid = null, $ban)
  {
    $customer = $this->customerRepository->getCustomerById($customerId, ['current_mobile_number', 'CURRENT_ICCID_FULL', 'preferred_language']);

    if (!$customer) {
      return ['success' => false , 'errors' => ['Could not load data for customer id ' . $customerId . ' in mvneMakeitsoRemoveFromBan']];
    }

    if (!$actionUuid) {
      $actionUuid = getNewActionUUID('mvne ' . time());
    }

    $planInfo = $this->configuration->getPlanConfiguration($plan);
    $preferred_language = (!$customer->preferred_language) ? 'EN' : $customer->preferred_language;

    $result = $this->mwControl->mwMakeitsoRemoveFromBan([
      'actionUUID'         => $actionUuid,
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'ultra_plan'         => $this->configuration->getBasePlanName($plan),
      'wholesale_plan'     => $planInfo['wholesale'],
      'preferred_language' => $preferred_language,
      'customer_id'        => $customerId,
      'option'             => 'A-DATA-POOL-1',
      'ban'                => $ban
    ]);

    // connect back to the default DB
    teldata_change_db();

    return ['success' => $result->is_success(), 'errors' => $result->get_errors()];
  }

  public function mvneMakeitsoAddToBan($customerId, $plan, $dataAmount, $actionUUID=null)
  {
    $customer = $this->customerRepository->getCustomerById($customerId, ['current_mobile_number', 'CURRENT_ICCID_FULL', 'preferred_language']);

    if (!$customer) {
      return ['success' => false , 'errors' => ['Could not load data for customer id ' . $customerId . ' in mvneMakeitsoRemoveFromBan']];
    }

    if (!$actionUUID) {
      $actionUUID = getNewActionUUID('mvne ' . time());
    }

    $planInfo = $this->configuration->getPlanConfiguration($plan);
    $preferred_language = (!$customer->preferred_language) ? 'EN' : $customer->preferred_language;

    $result = $this->mwControl->mwMakeitsoAddToBan([
      'actionUUID'         => $actionUUID,
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'ultra_plan'         => $this->configuration->getBasePlanName($plan),
      'wholesale_plan'     => $planInfo['wholesale'],
      'preferred_language' => $preferred_language,
      'customer_id'        => $customerId,
      'option'             => $dataAmount
    ]);

    // connect back to the default DB
    teldata_change_db();

    return ['success' => $result->is_success(), 'errors' => $result->get_errors()];
  }
}

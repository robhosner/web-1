<?php
namespace Ultra\Billing\Interfaces;

/**
 * Interface BillingHistoryRepository
 * @package Ultra\Billing\Interfaces
 */
interface BillingHistoryRepository
{
  /**
   * Get billing transaction history by customer id.
   *
   * @param $customerId
   * @return array
   */
  public function getBillingTransactionHistoryByCustomerId($customerId);

  /**
   * Get billing transaction history.
   *
   * @param $customerId
   * @param $startEpoch
   * @param $endEpoch
   * @return \array[], billing_transaction_history=>[])
   */
  public function getBillingTransactionHistory($customerId, $startEpoch, $endEpoch);

  /**
   * @param $customerId
   * @param array $selectFields
   * @param $description
   * @param int $selectAmount
   * @return array
   */
  public function getBillingTransactionsByDescription($customerId, array $selectFields, $description, $selectAmount = -1);
}
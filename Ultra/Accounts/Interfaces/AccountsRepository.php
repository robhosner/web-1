<?php
namespace Ultra\Accounts\Interfaces;

use Ultra\Accounts\Account;

/**
 * Interface AccountsRepository
 * @package Ultra\Accounts\Interfaces
 */
interface AccountsRepository
{
  /**
   * @param $customerId
   * @param array $selectFields
   * @return Account
   */
  public function getAccountFromCustomerId($customerId, array $selectFields);

   /**
   * @param $customerID
   * @param array $updateFields
   * @return bool
   */
  public function updateAccount($customerID, array $updateFields);
}

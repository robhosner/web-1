<?php
namespace Ultra\Exceptions;

require_once 'Ultra/Exceptions/CustomErrorCodeException.php';

class MissingRequiredParametersException extends CustomErrorCodeException
{
  /**
   * MissingRequiredParametersException constructor.
   * @param string $class
   * @param array $requiredParams
   * @param string $message
   * @param string $code
   * @param array $customUserErrors
   */
  public function __construct(
    $class = '',
    array $requiredParams = [],
    $message = 'Missing Required Parameters',
    $code = 'MP0001',
    array $customUserErrors = []
  )
  {
    $classPrefix = !empty($class) ? $class . ': ' : '';
    parent::__construct($classPrefix . $message . " " . implode(", ", $requiredParams), $code, null, $customUserErrors);
  }
}

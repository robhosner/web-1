<?php
namespace Ultra\Customers;

use Exception;
use Ultra\Configuration\Configuration;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Hydrator;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Utilities\Common;

require_once 'Ultra/Configuration/Configuration.php';
require_once 'Ultra/Exceptions/MissingRequiredParametersException.php';
require_once 'Ultra/Hydrator.php';
require_once 'Ultra/Lib/MiddleWare/Adapter/Control.php';
require_once 'Ultra/Utilities/Common.php';
require_once 'db/ultra_customer_options.php';

/**
 * Class Customer
 * @package Ultra\Customers
 */
class Customer
{
  use Hydrator;

  /**
   * @var
   */
  public $customer_id;

  /**
   * @var
   */
  public $preferred_language;

  /**
   * @var
   */
  public $plan_state;

  /**
   * @var
   */
  public $current_mobile_number;

  /**
   * @var
   */
  public $current_iccid_full;

  /**
   * @var
   */
  public $brand_id;

  /**
   * @var
   */
  public $throttleSpeed;

  /**
   * @var Configuration
   */
  public $config;

  /**
   * @var
   */
  public $current_iccid;

  /**
   * @var
   */
  public $cos_id;

  /**
   * @var
   */
  public $first_name;

  /**
   * @var
   */
  public $last_name;

  /**
   * @var
   */
  public $balance;

  /**
   * @var
   */
  public $packaged_balance1;

  /**
   * @var
   */
  public $packaged_balance2;

  /**
   * @var
   */
  public $packaged_balance3;

  /**
   * @var
   */
  public $packaged_balance4;

  /**
   * @var
   */
  public $packaged_balance5;

  /**
   * @var
   */
  public $stored_value;

  /**
   * @var
   */
  public $plan_started;

  /**
   * @var
   */
  public $plan_started_epoch;

  /**
   * @var
   */
  public $plan_expires;

  /**
   * @var
   */
  public $plan_expires_epoch;

  /**
   * @var
   */
  public $monthly_cc_renewal;

  /**
   * @var
   */
  public $monthly_renewal_target;

  /**
   * @var
   */
  public $plan_cost;

  /**
   * @var
   */
  public $account;

  /**
   * @var
   */
  public $postal_code;

  /**
   * @var
   */
  public $cc_postal_code;

  /**
   * @var
   */
  public $login_password;

  /**
   * @var Control
   */
  public $middleWareAdapter;

  /**
   * Customer constructor.
   * @param array $customerInfo
   * @throws Exception
   */
  public function __construct(array $customerInfo)
  {
    $this->hydrate($customerInfo, true);

    if (empty($this->customer_id))
    {
      throw new MissingRequiredParametersException(__CLASS__ . '::' . __FUNCTION__, ['customer_id']);
    }
  }

  /**
   * @return String
   */
  public function getWholesalePlan()
  {
    return \Ultra\Lib\DB\Customer\getWholesalePlan($this->customer_id);
  }

  /**
   * @return bool
   */
  public function hasCreditCard()
  {
    return (bool) count(\get_cc_info_and_tokens_by_customer_id($this->customer_id));
  }

  /**
   * @param array $settings
   * @return array
   */
  public function setMarketingSettings(array $settings)
  {
    return \set_marketing_settings($settings);
  }

  /**
   * @return object
   */
  public function optInVoicePreference()
  {
    return \opt_in_voice_preference($this->customer_id);
  }

  /**
   * @return object
   */
  public function optOutVoicePreference()
  {
    return \opt_out_voice_preference($this->customer_id);
  }

  /**
   * @param array $info
   * @return \Ultra\Lib\DB\Setter\Customer\Result
   */
  public function updateUltraInfo(array $info)
  {
    return \Ultra\Lib\DB\Setter\Customer\ultra_info($info);
  }

  /**
   * @return bool
   */
  public function acceptTermsOfService()
  {
    return \func_accept_terms_of_service($this->customer_id);
  }

  /**
   * @param $throttleSpeed
   * @return bool
   */
  public function canUpdateToThrottleSpeed($throttleSpeed)
  {
    $throttleSpeed = strtolower($throttleSpeed);
    $throttleSpeedChangeOptions = [
      'full' => [
        '1536',
        '1024',
      ],
      '1536' => [
        'full',
        '1024',
      ],
      '1024' => [
        'full',
        '1536',
      ],
    ];

    if (!in_array($throttleSpeed, array_keys($throttleSpeedChangeOptions))) {
      return false;
    } elseif (!in_array($throttleSpeed, $throttleSpeedChangeOptions[$this->getThrottleSpeed()])) {
      return false;
    } else {
      return true;
    }
  }

  public function getThrottleSpeed()
  {
    if (empty(($this->throttleSpeed))) {
      $sql = "SELECT TOP 1 OPTION_VALUE FROM ULTRA.CUSTOMER_OPTIONS WHERE CUSTOMER_ID = %d AND OPTION_ATTRIBUTE LIKE 'LTE_SELECT%%'";
      $result = find_first(sprintf($sql, $this->customer_id));
      $throttleSpeeds = [
        'Full Speed' => 'full',
        'Optimized - 1536 kbps' => '1536',
        'Super Saver - 1024 kbps' => '1024'
      ];

      if (empty($result->OPTION_VALUE)) {
        return $throttleSpeeds['Full Speed'];
      }

      return $result->OPTION_VALUE == "Full Speed" ? $throttleSpeeds['Full Speed'] : (int) $throttleSpeeds[$result->OPTION_VALUE];
    }

    return $this->throttleSpeed;
  }

  public function setConfiguration()
  {
    return empty($this->config) ? new Configuration() : $this->config;
  }

  public function getMiddleWareAdapter()
  {
    return empty($this->middleWareAdapter) ? $this->setMiddleWareAdapter(new Control()) : $this->middleWareAdapter;
  }

  public function setMiddleWareAdapter(Control $control)
  {
    return $this->middleWareAdapter = $control;
  }

  public function removeAccAddressE911()
  {
    return \remove_customer_options($this->customer_id, [ACC_OPTION_ADDRESS_E911]);
  }

  public function getAccAddressE911()
  {
    return \get_acc_address_e911($this->customer_id);
  }

  public function hasTheWifiSoc()
  {
    $reference = __CLASS__ . '::' . __FUNCTION__;

    if (empty($this->current_iccid) && empty($this->current_mobile_number)) {
      throw new MissingRequiredParametersException($reference, ['current_iccid', 'current_mobile_number']);
    }

    $result = $this->getMiddleWareAdapter()->mwQuerySubscriber([
      'actionUUID' => (new Common())->getNewActionUUID($reference . time()),
      'msisdn' => $this->current_mobile_number,
      'iccid' => $this->current_iccid
    ]);

    if ($result->is_success() && isset($result->data_array['body']->AddOnFeatureInfoList->AddOnFeatureInfo)) {
      foreach ($result->data_array['body']->AddOnFeatureInfoList->AddOnFeatureInfo as $soc) {
        if ($soc->FeatureID == 12809 && $soc->UltraServiceName == 'N-WFC') {
          return true;
        }
      }
    }

    return false;
  }

  public function createTemporaryPassword()
  {
    return func_create_temp_password($this->customer_id, 'reset_password', 60*60*24);
  }

  public function voidCreditCardInformation()
  {
    return \customer_set_null_credit_card_data(['SET_NULL_CREDIT_CARD_DATA' => true, 'customer_id' => $this->customer_id]);
  }

  public function disableCreditCard()
  {
    return \Ultra\Lib\DB\Setter\Customer\disable_cc_info(['customer_id' => $this->customer_id]);
  }

  public function setUltraCustomerOptionsByArray($customer_id, $pairs, $toRemove)
  {
    return \set_marketing_options_by_array($customer_id, $pairs, $toRemove);
  }
}

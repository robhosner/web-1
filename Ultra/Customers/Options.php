<?php
namespace Ultra\Customers;

class Options
{
  /**
   * @var Customer
   */
  private $customer;

  /**
   * Options constructor.
   * @param Customer $customer
   */
  public function __construct(Customer $customer)
  {
    $this->customer = $customer;
  }

  public function getE911Address()
  {
    return \get_acc_address_e911($this->customer->customer_id);
  }
}

<?php
namespace Ultra\Customers;

class StateActions
{
  public function getStateFromCustomerId($customerId)
  {
    return \internal_func_get_state_from_customer_id($customerId);
  }
}

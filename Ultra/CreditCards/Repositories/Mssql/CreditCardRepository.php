<?php

namespace Ultra\CreditCards\Repositories\Mssql;

use Ultra\CreditCards\CreditCard;
use Ultra\Exceptions\SaveMethodFailedException;
use Ultra\Lib\DB\Merchants\MeS;
use Ultra\Lib\DB\Merchants\Vantiv;

require_once __DIR__ . '/../../Interfaces/CreditCardRepository.php';

/**
 * Class CreditCardRepository
 * @package Ultra\CreditCards\Repositories\Mssql
 */
class CreditCardRepository implements \Ultra\CreditCards\Interfaces\CreditCardRepository
{
  public function getUltraCCHolderByDetails(array $params)
  {
    if (
         empty($params['customer_id'])
      || empty($params['bin'])
      || empty($params['last_four'])
      || empty($params['expires_date'])
    )
    {
      \logError('Missing Parameters');
      return FALSE;
    }

    $sql = sprintf("SELECT CUSTOMER_ID
      FROM  ULTRA.CC_HOLDERS WITH (NOLOCK)
      WHERE CUSTOMER_ID  = %d
      AND   BIN          = %s
      AND   LAST_FOUR    = %s
      AND   EXPIRES_DATE = %s",
      $params['customer_id'],
      mssql_escape_with_zeroes($params['bin']),
      mssql_escape_with_zeroes($params['last_four']),
      mssql_escape_with_zeroes($params['expires_date'])
    );

    return find_first($sql);
  }

  public function addToUltraCCHolderTokens(array $params)
  {
    if (
         empty($params['cc_holders_id'])
      || empty($params['gateway'])
      || empty($params['merchant_account'])
      || empty($params['token'])
    )
    {
      \logError('Missing Parameters');
      return FALSE;
    }

    $sql = sprintf('
      IF NOT EXISTS (
        SELECT CC_HOLDERS_ID
        FROM   ULTRA.CC_HOLDER_TOKENS
        WHERE  CC_HOLDERS_ID    = %d
        AND    GATEWAY          = %s
        AND    MERCHANT_ACCOUNT = %s
        AND    TOKEN            = %s
      )
      BEGIN
        INSERT INTO ULTRA.CC_HOLDER_TOKENS
        (
          CC_HOLDERS_ID,
          GATEWAY,
          MERCHANT_ACCOUNT,
          TOKEN,
          ENABLED,
          CUSTOMER_IP
        )
        VALUES
        (
          %d,
          %s,
          %s,
          %s,
          1,
          \'%s\'
        )
      END
      ',
      $params['cc_holders_id'],
      mssql_escape_with_zeroes($params['gateway']),
      mssql_escape_with_zeroes($params['merchant_account']),
      mssql_escape_with_zeroes($params['token']),
      $params['cc_holders_id'],
      mssql_escape_with_zeroes($params['gateway']),
      mssql_escape_with_zeroes($params['merchant_account']),
      mssql_escape_with_zeroes($params['token']),
      \Session::getClientIp()
    );

    $check = run_sql_and_check($sql);
    return ($check) ? get_scope_identity() : $check;
  }

  public function addToUltraCCHolders(array $params)
  {
    if (
         empty($params['customer_id'])
      || empty($params['bin'])
      || empty($params['last_four'])
      || empty($params['expires_date'])
    )
    {
      \logError('Missing Parameters');
      return FALSE;
    }

    if ( ! isset( $params['cvv_validation'] ) ) $params['cvv_validation'] = ' ';
    if ( ! isset( $params['avs_validation'] ) ) $params['avs_validation'] = ' ';

    $sql = sprintf('
      IF EXISTS (
        SELECT CUSTOMER_ID
        FROM   ULTRA.CC_HOLDERS WITH (NOLOCK)
        WHERE  CUSTOMER_ID  = %d
        AND    BIN          = %s
        AND    LAST_FOUR    = %s
        AND    EXPIRES_DATE = %s
      )
        BEGIN
          UPDATE ULTRA.CC_HOLDERS
          SET    ENABLED = 1
          WHERE  CUSTOMER_ID  = %d
          AND    BIN          = %s
          AND    LAST_FOUR    = %s
          AND    EXPIRES_DATE = %s
        END
      ELSE
        BEGIN
          INSERT INTO ULTRA.CC_HOLDERS
          (
            CUSTOMER_ID,
            BIN,
            LAST_FOUR,
            EXPIRES_DATE,
            CVV_VALIDATION,
            AVS_VALIDATION,
            CREATED_DATE_TIME,
            ENABLED
          )
          VALUES
          (
            %d,
            %s,
            %s,
            %s,
            %s,
            %s,
            GETDATE(),
            1
          )
        END',
      $params['customer_id'],
      mssql_escape_with_zeroes($params['bin']),
      mssql_escape_with_zeroes($params['last_four']),
      mssql_escape_with_zeroes($params['expires_date']),
      $params['customer_id'],
      mssql_escape_with_zeroes($params['bin']),
      mssql_escape_with_zeroes($params['last_four']),
      mssql_escape_with_zeroes($params['expires_date']),
      $params['customer_id'],
      mssql_escape_with_zeroes($params['bin']),
      mssql_escape_with_zeroes($params['last_four']),
      mssql_escape_with_zeroes($params['expires_date']),
      mssql_escape_with_zeroes($params['cvv_validation']),
      mssql_escape_with_zeroes($params['avs_validation'])
    );

    $check = run_sql_and_check($sql);
    return ($check) ? get_scope_identity() : $check;
  }

  /**
   * Get credit card info from customer id.
   *
   * @param $customerId
   * @return object
   */
  public function getCcInfoFromCustomerId($customerId)
  {
    return \get_cc_info_from_customer_id($customerId);
  }

  private function prepareInsertUltraCCHolderTokens(array $data)
  {
    return \ultra_cc_holder_tokens_insert_from_customer_id_query($data['customer_id'], $data, $data);
  }

  private function prepareInsertToUltraCCHolders(array $data)
  {
    return \ultra_cc_holders_insert_query($data);
  }

  private function prepareDisablePreviousUltraCCHolders(array $data)
  {
    return \ultra_cc_holders_update_query(
      array(
        'SET_enabled'      => '0',
        'customer_id'      => $data['customer_id'],
        'NOT_bin'          => $data['bin'],
        'NOT_last_four'    => $data['last_four'],
        'NOT_expires_date' => $data['expires_date']
      )
    );
  }

  private function prepareDisablePreviousUltraCCHolderTokens(array $data)
  {
    return \ultra_cc_holder_tokens_disable_query($data);
  }

  /**
   * Save credit card information.
   *
   * @param CreditCard $creditCard
   * @return bool
   * @throws SaveMethodFailedException
   */
  public function saveCreditCardInformation(CreditCard $creditCard)
  {
    $data = (array) $creditCard;

    $data['processor'] = !empty($data['processor']) ? $data['processor'] : 'mes';

    $processorConfig = \Ultra\UltraConfig\getCCProcessorByName($data['processor']);
    if (empty($processorConfig)) {
      throw new UnhandledCCProcessorException('Missing processor configuration', 'IN0002');
    }
    $merchantClass = '\\Ultra\\Lib\\DB\\Merchants\\' . $processorConfig['name'];
    if (!class_exists($merchantClass)) {
      throw new UnhandledCCProcessorException('Missing processor implementation', 'IN0002');
    }
    $merchantObj = new $merchantClass();

    $data += $merchantObj->merchantInfo();

    $sqlList = [
      $this->prepareInsertToUltraCCHolders($data),
      $this->prepareDisablePreviousUltraCCHolders($data),
      $this->prepareInsertUltraCCHolderTokens($data),
      $this->prepareDisablePreviousUltraCCHolderTokens($data),
    ];

    if (!\exec_queries_in_transaction($sqlList)['success']) {
      throw new SaveMethodFailedException('Failed to save credit card information');
    }

    return true;
  }

  /**
   * Get customer's credit card information including token.
   *
   * @param $customerId
   * @return CreditCard
   * @throws MissingCreditCardException
   */
  public function getCCInfoAndTokenByCustomerId($customerId)
  {
    $result = \get_cc_info_and_tokens_by_customer_id($customerId);
    return !empty($result) ? new CreditCard((array)$result[0]) : null;
  }
}
<?php
namespace Ultra\Sims;

use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Hydrator;

/**
 * Class Sim
 * @package Ultra\Sims
 */
class Sim
{
  /**
   * @var
   */
  public $iccid_number;

  /**
   * @var
   */
  public $imsi;

  /**
   * @var
   */
  public $sim_activated;

  /**
   * @var
   */
  public $inventory_masteragent;

  /**
   * @var
   */
  public $inventory_distributor;

  /**
   * @var
   */
  public $inventory_dealer;

  /**
   * @var
   */
  public $customer_id;

  /**
   * @var
   */
  public $inventory_status;

  /**
   * @var
   */
  public $last_changed_date;

  /**
   * @var
   */
  public $last_changed_by;

  /**
   * @var
   */
  public $created_by_date;

  /**
   * @var
   */
  public $created_by;

  /**
   * @var
   */
  public $iccid_batch_id;

  /**
   * @var
   */
  public $iccid_full;

  /**
   * @var
   */
  public $expires_date;

  /**
   * @var
   */
  public $last_transition_uuid;

  /**
   * @var
   */
  public $pin1;

  /**
   * @var
   */
  public $puk1;

  /**
   * @var
   */
  public $pin2;

  /**
   * @var
   */
  public $puk2;

  /**
   * @var
   */
  public $other;

  /**
   * @var
   */
  public $reservation_time;

  /**
   * @var
   */
  public $globally_used;

  /**
   * @var
   */
  public $act_code;

  /**
   * @var
   */
  public $product_type;

  /**
   * @var
   */
  public $stored_value;

  /**
   * @var
   */
  public $sim_hot;

  /**
   * @var
   */
  public $mvne;

  /**
   * @var
   */
  public $is_expired;

  /**
   * @var
   */
  public $offer_id;

  /**
   * @var
   */
  public $brand_id;

  /**
   * @var
   */
  public $tmoprofilesku;

  use Hydrator;

  /**
   * Sim constructor.
   * @param array $info
   * @throws InvalidObjectCreationException
   */
  public function __construct(array $info)
  {
    $this->hydrate($info, true);

    if (empty($this->iccid_number) || empty($this->iccid_full) || empty($this->brand_id)) {
      throw new InvalidObjectCreationException(__CLASS__ . ' Missing required parameters.', 'MP0001');
    }
  }

  /**
   * Check to see if sim allows adding wifi soc.
   *
   * @return bool
   */
  public function allowsWifiSoc()
  {
    if (in_array($this->brand_id, [2, 3]) || in_array($this->tmoprofilesku, ['ULTRASIM', 'ULTRASIM2', 'UltraUnknown'])) {
      return true;
    } else {
      return false;
    }
  }
}

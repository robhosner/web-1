<?php

namespace Ultra\Lib;


/**
 * Context class
 * customer notification session context
 * created once for every new SMS interaction session, hold the necessary session data, save to and restored from redis with every workflow breakpoint
 * @author VYT 2015
 */

class Context
{
  // constants
  const CONTEXT_DURATION = 1200; // 20 minutes per Product

  // variables
  private $msisdn;      // subscriber's MSISDN
  private $key;         // redis context key
  private $customer_id; // customer ID
  private $plan_state;  // subscriber's plan state
  private $message;     // inbound SMS message cleaned and parsed into Array or separate words
  private $language;    // subscribers preferred language
  private $brand;       // subscribers brand
  private $cos_id;
  private $workflow;    // workflow name to execute
  private $action;      // action to execute: 0 means execute first action (workflow is newly created), NULL means workflow is complete (no more actions)
  private $continue;    // continue workflow with next action
  private $custom;      // workflow custom data
  private $failure;     // number of workflow failures caused by invalid user response: terminate after MAX_CONTEXT_FAILURES to prevent being stuck in a workflow


  /*
   * constructor
   * create class instance and initialize defaults
   */
  public function __construct($msisdn)
  {
    if (empty($msisdn))
      return logError("invalid MSISDN value '$msisdn'");
    $this->msisdn = $msisdn;

    // hard coded defaults
    $this->language = 'EN';
    $this->brand = 1;
    $this->message = array();
    $this->custom = array();

    $this->key = strtolower(str_replace('\\', '/', __CLASS__)) . "/$msisdn";
  }


  /**
   * create
   * create new context
   * @param Object Cache instance
   * @return Boolean TRUE on success or FALSE on failure
   */
  public function create($cache)
  {
    if ( ! is_object($cache))
      return logError('invalid parameter cache');
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');

    // load customer record but only by a real MSISDN
    if (strlen($this->msisdn) == 10)
    {
      // load from DB cache for one hour
      if ( ! $customer = $cache->selectRow('HTT_CUSTOMERS_OVERLAY_ULTRA', 'CURRENT_MOBILE_NUMBER', $this->msisdn, SECONDS_IN_HOUR, FALSE))
        return logError("failed to find subscriber for MSISDN {$this->msisdn}");
      logInfo('customer ID: ' . $customer->CUSTOMER_ID);

      if ( ! $account = $cache->selectRow('ACCOUNTS', 'CUSTOMER_ID', $customer->CUSTOMER_ID, SECONDS_IN_HOUR, FALSE))
        return logError("failed to find subscriber for MSISDN {$this->msisdn}");

    print_r($account);

      $this->customer_id = $customer->CUSTOMER_ID;
      if ( ! empty(trim($customer->PREFERRED_LANGUAGE)))
        $this->language = $customer->PREFERRED_LANGUAGE;
      if ( ! empty(trim($customer->PLAN_STATE)))
        $this->plan_state = $customer->PLAN_STATE;
      if ( ! empty($customer->BRAND_ID))
        $this->brand = $customer->BRAND_ID;
      if ( ! empty($account->COS_ID))
        $this->cos_id = $account->COS_ID;
    }

    $this->workflow = NULL;
    $this->action = 0; // execute first action
    $this->failure = 0;
    logInfo("created new context for MSISDN {$this->msisdn}:");
    $this->log();
    return TRUE;
  }


  /**
   * load
   * load session context from redis
   * @param Object Redis instance
   * @return Boolean TRUE on success or FALSE on failure
   */
  public function load($redis)
  {
    if ( ! is_object($redis))
      return logError('invalid parameter redis');
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');

    if ( ! $data = $redis->get($this->key))
      return logInfo("no existing session for MSISDN {$this->msisdn}");

    foreach (json_decode($data, TRUE, 12, JSON_BIGINT_AS_STRING) as $property => $value)
      $this->$property = $value;
    logInfo("loaded existing context for MSISDN {$this->msisdn}:");
    $this->log();
    return TRUE;
  }


  /**
   * save
   * save context to redis
   * @param Object Redis instance
   */
  public function save($redis)
  {
    if ( ! is_object($redis))
      return logError('invalid parameter redis');
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');

    // exclude varibles set anew on each class instance
    $exclude = array('msisdn', 'key', 'message');
    $data = array();
    foreach ($this as $property => $value)
      if ( ! in_array($property, $exclude))
        $data[$property] = $value;
    $data = json_encode($data);

    logInfo("saving context for MSISDN {$this->msisdn}:");
    logInfo($data);
    $redis->set($this->key, $data, self::CONTEXT_DURATION);
  }


  /**
   * delete
   * remove context and customer record from cache
   * @param Object Redis instance
   */
  public function delete($redis, $cache)
  {
    if ( ! is_object($redis) || ! is_object($cache))
      return logError('invalid parameters');
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');

    logInfo("removing context for MSISDN {$this->msisdn}");
    $redis->del($this->key);
    $cache->purge('HTT_CUSTOMERS_OVERLAY_ULTRA', 'CURRENT_MOBILE_NUMBER', $this->msisdn);
  }


  /**
   * get
   * get property value via class magic method
   * generic method to cover all cases since reading properties is safe
   * @param String property name
   * @param Mixed property value or NULL on failure
   */
  public function __get($name)
  {
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');

    if (property_exists($this, $name))
      return $this->$name;

    // while isset is very fast it is not always applicable
    if (isset($this->custom[$name]) || array_key_exists($name, $this->custom))
      return $this->custom[$name];

    return logError("unknown property <$name>");
  }


  /**
   * getKeyword
   * return message keyword
   * @param Integer index
   * @param Boolean capitalize
   * @return String keyword or NULL on failure
   */
  public function getKeyword($index, $caps = FALSE)
  {
    $keyword = isset($this->message[$index]) ? $this->message[$index] : NULL;
    return $caps ? strtoupper($keyword) : $keyword;
  }


  /**
   * setMessage
   * set context SMS message
   * @param Array parsed message
   */
  public function setMessage($message)
  {
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');
    if (empty($message))
      return logError('invalid parameter value');

    $this->message = $message;
  }


  /**
   * setLanguage
   * set context language
   * @param String language code
   */
  public function setLanguage($language)
  {
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');
    if (empty($language))
      return logError('invalid parameter value');

    $this->language = $language;
  }


  /**
   * setCustom
   * set custom workflow property
   * @param String property name
   * @param Mixed property value
   */
  public function setCustom($name, $value)
  {
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');
    if (empty($name))
      return logError('invalid property name');

    $this->custom[$name] = $value;
  }


  /**
   * log
   * dump context data to log
   */
  public function log()
  {
    $data = array();
    foreach ($this as $property => $value)
      $data[$property] = $value;
    logInfo('context: ' . json_encode($data));
  }


  /**
   * setAction
   * save workflow action to execute
   * @param String action name
   */
  public function setAction($action)
  {
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');
    if (empty($action))
      return logError('invalid parameter value');

    $this->action = $action;
  }


  /**
   * setWorkflow
   * save workflow to execute
   * @param String workflow name
   */
  public function setWorkflow($workflow)
  {
    if (empty($this->key))
      return logError(__CLASS__ . ' class failed to initialize');
    if (empty($workflow))
      return logError('invalid parameter value');

    $this->workflow = $workflow;
  }


  /**
   * terminate
   * mark workflow as complete
   */
  public function terminate()
  {
    $this->action = NULL;
  }


  /**
   * pause
   * set workflow breakpoint
   */
  public function pause()
  {
    $this->continue = FALSE;
  }


  /**
   * resume
   * continue workflow execution
   */
  public function resume()
  {
    $this->continue = TRUE;
  }

  /**
   * failure
   * increase failure counter due to invalid user input
   */
  public function failure()
  {
    $this->failure++;
    logInfo("increased workflow failure count: {$this->failure}");
  }

}
<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowInterface.php';


/**
 * WorkflowBase
 * parent class used by all workflows
 * types of workflow actions:
 *   entry: first default action
 *   goto: conditional action; condition is usually checked by workflow or another preceeding action
 *   jump: immediate execution of an exact action on specific keyword (e.g. keword 'CHINESE': immediately set preferred language to Chinese without confirmation)
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/3ci+Replacement
 * @author VYT, 2015
 */
class WorkflowBase implements WorkflowInterface
{
  // list of workflow actions (specific for each workflow)
  protected $actions = array();
  protected $workflow = NULL; // final workflow class name after inheritance and aliasing


  /**
   * run
   * enter a workflow and loop through all actions until workflow is done or a break encountered
   * workflow typically processes actions sequentially but may jump (skip actions) or repeat actions conditionally
   * actions are factory pattern by action class name
   * @param Object Context instance
   * @return String ULTRA.USER_ERROR_MESSAGES.ERROR_CODE on error or NULL on success
   */
  public function run($context)
  {
    $context->log();

    // set first action upon inintial entry
    if ($context->action === 0)
      $context->setAction($this->actions[0]);

    // execute actions until workflow is complete or a break is encountered or max iterations is reached
    $interation = 0; // insurance against a programming error
    do
    {
      $interation++;

      // validate action as it may have changed
      $workflowClass = $this->workflow . $context->action;
      if ( ! class_exists($workflowClass))
      {
        logError("action {$this->workflow}{$context->action} does not exist");
        return 'IN0004'; // internal configuraiton error
      }

      // execute action
      logInfo("instantiating action $workflowClass");
      $action = new $workflowClass;
      if ($error = $action->perform($context))
        return $error;
      unset($action);

    } while  ($context->action && $context->continue && $interation < 10);

    // workflow completed or action set a breakpoint without errors
    return NULL;
  }

}


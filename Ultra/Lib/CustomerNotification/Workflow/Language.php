<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowBase.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/LanguageQuery.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/LanguageDecide.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/LanguageChange.php';


/**
 * workflow for SMS commands LANGUAGE, ENGLISH, SPANISH, CHINESE
 * query and optionally set subscriber's preferred communication language
 * workflow actions:
 *   Query: query subscriber for preferred language
 *   Decide: process subscriber's response and decide if language change is necessary
 *   Change: set subscriber's preferred language
 */
class Language extends WorkflowBase
{
  // list of actions in this workflow and keywords that trigger them
  protected $actions = array('Query', 'Decide', 'Change');
  protected $workflow = __CLASS__; // due to late static binding
}


// this workflow can also be called by the following additional keywords
class_alias(__NAMESPACE__ . '\\Language', __NAMESPACE__ . '\\lang');

// these workflow jumsp allow to set language immediately without prompts
class_alias(__NAMESPACE__ . '\\Language', __NAMESPACE__ . '\\chinese');
class_alias(__NAMESPACE__ . '\\Language', __NAMESPACE__ . '\\eng');
class_alias(__NAMESPACE__ . '\\Language', __NAMESPACE__ . '\\english');
class_alias(__NAMESPACE__ . '\\Language', __NAMESPACE__ . '\\esp');
class_alias(__NAMESPACE__ . '\\Language', __NAMESPACE__ . '\\espanol');
class_alias(__NAMESPACE__ . '\\Language', __NAMESPACE__ . '\\spanish');

<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * Goto Activate workflow action
 */
class GogoActivate extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * call Ultra API v2 interactivecare__RequestProvisionOrangeBySMS
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // finalize workflow since we cannot re-enter it even in case of errors
    $context->terminate();

    if ($context->customer_id) // VYT: cannot use empty since it does not work correctly on methods (!)
      return $context->plan_state == STATE_ACTIVE ? 'IN0009' : 'IN0008';

    // prepare API config
    $config = array(
      'command'   => 'interactivecare__RequestProvisionOrangeBySMS',
      'version'   => 2);

    // prepare API parameters
    $params = array('fake_msisdn' => $context->msisdn);
    if ($zip = $context->getKeyword(1))
      $params['zipcode'] = $zip;

    // call API and check result
    if ( ! $result = $this->callUltraApi($config, $params))
    {
      logError("call to Ultra API {$config['command']} failed");
      return 'IN0005';
    }

    // handle response
    return empty($result->error_codes) ? NULL : $result->error_codes[0];
  }
}


<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * BoltOns Determine workflow action
 */
class BoltOnsDetermine extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding

  /**
   * perform
   *
   * determine the type of bolt on purchase: data or international, correct misspelled keywords
   * Keywords: 'UPDATA', 'UPDATE', 'BUYDATA', '500MB', 'UPINTL', 'BUYINTL', 'BUYMINS', 'DATA', 'DATE', '500M', '500MB', 'INTL', 'INTO', 'INTERNATIONAL'
   *
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // keywords that contain complete action info
    $dataComplete  = array('UPDATA', 'UPDATE', 'BUYDATA', '500MB');
    $intlComplete  = array('UPINTL', 'BUYINTL', 'BUYMINS');
    $roamComplete  = array('UPROAM', 'BUYROAM');
    $globeComplete = ['UPGLOBE', 'BUYGLOBE'];

    // kewords that contain partial action info
    $dataPartial  = array('DATA', 'DATE', '500M', '500MB');
    $intlPartial  = array('INTL', 'INTO', 'INTERNATIONAL');
    $roamPartial  = array('ROAM');
    $globePartial = ['GLOBE'];

    // normalize keywords
    $first = $context->getKeyword(0, TRUE);
    $second = $context->getKeyword(1, TRUE);

    // analyze keywords
    if (in_array($first, $dataComplete)      || in_array($second, $dataPartial))
      $context->setCustom('purchase', 'DATA');
    elseif (in_array($first, $intlComplete)  || in_array($second, $intlPartial))
      $context->setCustom('purchase', 'INTL');
    elseif (in_array($first, $roamComplete)  || in_array($second, $roamPartial))
      $context->setCustom('purchase', 'ROAM');
    elseif (in_array($first, $globeComplete) || in_array($second, $globePartial))
      $context->setCustom('purchase', 'GLOBE');
    else // empty or unknown second keyword
    {
      // set breakpoint and query subscriber
      $context->failure();
      $context->pause();
      if ( ! $message = \Ultra\Messaging\Templates\SMS_by_language(array('message_type' => 'boltons_determine'), $context->language))
        return 'IN0004';
      return $this->sendSms($context, $message);
    }

    // proceed to next action
    $context->setAction('Query');
    $context->resume();
    return NULL;
  }

}

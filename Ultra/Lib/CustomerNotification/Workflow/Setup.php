<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowBase.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/SetupQuery.php';

/**
 * workflow for SMS command SETUP
 */
class Setup extends WorkflowBase
{
  // list of actions in this workflow
  protected $actions = array('Query');
  protected $workflow = __CLASS__; // due to late static binding
}

<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/Workflow/ActionBase.php';

/**
 * BoltOns Validate workflow action
 */
class BoltOnsValidate extends ActionBase
{
  protected $action = __CLASS__; // due to late static binding


  /**
   * perform
   * validate subscriber's input and send a confirmation message
   * @param Object Context instance
   * @return String error code or NULL on success
   */
  public function perform($context)
  {
    $context->log();

    // return to previous action on invalid input
    $selection = $context->getKeyword(0);
    if ( ! in_array($selection, array(1, 2, 3)))
    {
      $context->resume();
      $context->failure();
      $context->setAction('Query');
      return NULL;
    }

    // adjust selection to zero-based index of boltons array and save it
    $context->setCustom('selection', --$selection);

    // set breakpoint and next action
    $context->setAction('Apply');
    $context->pause();

    // prepare and send purchase confirmation message
    $boltOn = (object)$context->boltons[$selection];
    $params = array(
      'message_type'  => 'boltons_confirm',
      'description'   => $boltOn->description,
      'cost'          => '$' . $boltOn->cost
    );

    if ( ! $message = \Ultra\Messaging\Templates\SMS_by_language($params, $context->language))
      return 'IN0004';
    return $this->sendSms($context, $message);
  }

}

<?php

namespace Ultra\Lib\CustomerNotification;

require_once 'Ultra/Lib/CustomerNotification/WorkflowBase.php';
require_once 'Ultra/Lib/CustomerNotification/Workflow/CampaignTrigger.php';

/**
 * workflow for SMS promo campaigns
 * call Ultra v2 API interactivecare__TriggerPromoBroadcastShortCode
 * request -> response only, implements a single default method
 */
class Campaign extends WorkflowBase
{
  // list of actions in this workflow
  protected $actions = array('Trigger');
  protected $workflow = __CLASS__; // due to late static binding
}

<?php

namespace Ultra\Lib\Api;

require_once 'Ultra/Lib/Api/Base.php';

/**
 * Ultra API class for HTML presentation
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class HTML extends Base
{
  /**
   * getPresentation
   * 
   * Returns HTML
   * I simply copied Ted's code ( function make_html )
   * 
   * @return string
   */
  public function getPresentation ()
  {
    header("content-type: text/html");

    $code = '';

    $code .= "<style>
  a, a:visited { color: blue; }
  body { font-family: sans-serif; }
  h2 { border-top: 2px solid #000; padding-top: 16px; } 
  th { text-align: left; }
  td { padding: 4px; }
  tr:nth-child(even) { background-color: #f0f0f0; } 
  table { border-collapse: collapse; width: 100%; }
  </style>";

  foreach ($this->apiDefinition['commands'] as $command => $def)
    $code .= "<a href=\"#$command\">$command</a>" . "<br />";

    $codep = '<h3>Parameters</h3><table class="confluenceTable">
  <tbody>
  <tr>
  <th class="confluenceTh">Parameter Name</th>
  <th class="confluenceTh">Type</th>
  <th class="confluenceTh">Required</th>
  <th class="confluenceTh">Detail</th>
  <th class="confluenceTh">Validation Rules</th>
  </tr>';

    $coder = '<h3>Responses</h3><table class="confluenceTable">
  <tbody>
  <tr>
  <th class="confluenceTh">Parameter Name</th>
  <th class="confluenceTh">Type</th>
  <th class="confluenceTh">Detail</th>
  </tr>';

    foreach ($this->apiDefinition['commands'] as $command => $def)
    {
     if ( is_null( $this->command ) || !$this->command || ( $this->command == '' ) || ( $this->command == $command ) )
     {
      $code.= "<h2 id=\"$command\">$command</h2>";
      $code.= "<a href=\"#\" onclick=\"window.scroll(0,0);\">^ TOP</a>";

      $code.= $codep;
      foreach ($def['parameters'] as $pdef)
      {
        $code = $code . sprintf('
        <tr>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        </tr>',
        $pdef['name'],
        $pdef['type'],
        $pdef['required'] ? 'required' : 'no',
        $pdef['desc'],
        $pdef['validation'] ? str_replace(array('{','}'),'',json_encode($pdef['validation'])) : 'none');
      }
      $code .= "\n</tbody></table>";
      $code.= $coder;

      foreach ($def['returns'] as $pdef)
      {
        $code = $code . sprintf('
        <tr>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        </tr>',
        $pdef['name'],
        $pdef['type'],
        $pdef['desc']);
      }
      $code .= "\n</tbody></table>";
     }
    }

    $code .= "<p>";

    return $code;
  }

  public function output( $string )
  {
    return $string;
  }
}


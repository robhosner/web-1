<?php

namespace Ultra\Lib\Api\Partner\MvneCare;

require_once 'Ultra/Lib/Api/Partner/MvneCare.php';

class UpdateEscalation extends \Ultra\Lib\Api\Partner\MvneCare
{
  /**
   * mvnecare__UpdateEscalation
   *
   * Updates a Command Invocation row. Used to modify escalation for a given customer_id.
   *
   * @return Result object
   */
  public function mvnecare__UpdateEscalation ()
  {
    list ( $customer_id , $to_status , $user , $ticket_details ) = $this->getInputValues();

    $error_code = '';

    try
    {
      $commandInvocation = new \CommandInvocation();

      $result = $commandInvocation->loadPending(
        array(
          'customer_id' => $customer_id
        )
      );

      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();
        $this->errException( $errors[0] , 'CI0001' );
      }

      if ( ! $result->is_pending() )
        $this->errException( 'ERR_API_INTERNAL: no pending items found for customer_id '.$customer_id , 'CI0002' );

      dlog('',"%s",((array)$commandInvocation));

      $mandatory_members = array(
        'customer_id' , 'status' , 'command_invocations_id' , 'command'
      );

      foreach( $mandatory_members as $member )
        if( !property_exists( $commandInvocation , $member ) || !$commandInvocation->$member )
          $this->errException( 'ERR_API_INTERNAL: Command Invocation data inconsistency - no '.$member, 'CI0003' );

      $result = make_error_Result('Invalid status');

      if ( $to_status == 'REPORTED MISSING' )
        $result = $commandInvocation->escalationMissing(
          array(
            'from_status'            => $commandInvocation->status,
            'ticket_details'         => $ticket_details,
            'last_status_updated_by' => $user
          )
        );

      if ( $to_status == 'REPORTED ERROR' )
        $result = $commandInvocation->escalationError(
          array(
            'from_status'            => $commandInvocation->status,
            'ticket_details'         => $ticket_details,
            'last_status_updated_by' => $user
          )
        );

      if ( $to_status == 'RESOLVED' )
      {
        $result = $commandInvocation->escalationSuccess(
          array(
            'from_status'            => $commandInvocation->status,
            'last_status_updated_by' => $user,
            'ticket_details'         => $ticket_details
          )
        );

        if ( $result->is_success() )
        {
          // speed up pending MISO (if any) for this customer

          \Ultra\Lib\DB\ultra_acc_connect();

          $data = get_makeitso(
            array(
              'customer_id' => $customer_id,
              'status'      => 'PENDING'
            )
          );

          if ( $data && is_array($data) && count($data) )
          {
            $redis_makeitso = new \Ultra\Lib\Util\Redis\MakeItSo();

            // add to Redis queue
            $redis_makeitso->add( time() , $data[0]->MAKEITSO_QUEUE_ID );
          }

          teldata_change_db();
        }
      }

      if ( $result->is_failure() )
        $this->errException( 'ERR_API_INTERNAL: an error occurred while updating Command Invocation for customer_id '.$customer_id , 'CI0004' );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
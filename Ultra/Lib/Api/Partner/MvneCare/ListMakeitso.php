<?php

namespace Ultra\Lib\Api\Partner\MvneCare;

require_once 'Ultra/Lib/Api/Partner/MvneCare.php';

class ListMakeitso extends \Ultra\Lib\Api\Partner\MvneCare
{
  const MAX_MAKEITSO_RECORDS = 1000; // max mumber of records to return from MAKEITSO_QUEUE
  
  /**
   * mvnecare__ListMakeitso
   *
   * return makeitso records with a specific status
   *
   * @return Result object
   */
  public function mvnecare__ListMakeitso()
  {
    // init
    list ($status, $created_days_ago, $type) = $this->getInputValues();
    $this->addToOutput('records', array());
    $this->addToOutput('record_count', 0);

    try
    {
      // validate $status
      if (is_array($status) && count($status))
      {
        foreach ($status as $item)
          if ( ! in_array($item, array('DONE', 'FAILED', 'PENDING', 'WITHDRAWN')))
            $this->errException('ERR_API_INVALID_ARGUMENTS: unknown status value', 'VV0019');
      }
      else // default status is all open
        $status = array('PENDING', 'FAILED');

      $params = array('STATUS' => $status);

      // how old
      if ( ! empty($created_days_ago))
        $params[] = sprintf('DATEDIFF(d, CREATED_DATE_TIME, GETUTCDATE()) < %d', $created_days_ago);

      // validate $type
      if (is_array($type) && count($type))
      {
        foreach ($type as $item)
          if ( ! in_array($item, array('UpgradePlan', 'RenewPlan', 'Deactivate', 'Suspend')))
            $this->errException('ERR_API_INVALID_ARGUMENTS: unknown type value', 'VV0019');

        $params['MAKEITSO_TYPE'] = $type;
      }

      // default order
      $params['order_by'] = 'CREATED_DATE_TIME DESC';

      // get MISO data
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');
      $data = get_makeitso($params, self::MAX_MAKEITSO_RECORDS);
      $count = count($data);

      // check results and add to response
      if ($count)
      {
        // AMDOCS-374: post-process dates
        $schema = \Ultra\Lib\DB\getTableSchema('MAKEITSO_QUEUE', 'datetime');

        $result_data = array();

        foreach ($data as &$row)
        {
          foreach ($schema as $column => $type)
            $row->$column = date_sql_to_usa($row->$column, TRUE);

          // also process ATTEMPT_HISTORY
          if (! empty($row->ATTEMPT_HISTORY))
          {
            if (strpos($row->ATTEMPT_HISTORY, 'HIDDEN') === FALSE)
            {
              $pairs = explode(';', $row->ATTEMPT_HISTORY); // split into 'ACTION|DATE' pairs
              $row->ATTEMPT_HISTORY = $pairs[count($pairs) - 1];
              $result_data[] = $row;
            }
          }
        }

        $this->addToOutput('records', $result_data);
        $this->addToOutput('record_count', count($result_data));
      }

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage ());
    }
    return $this->result;
  }
}

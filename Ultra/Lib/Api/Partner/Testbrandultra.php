<?php

namespace Ultra\Lib\Api\Partner;

require_once 'Ultra/Lib/Api/PartnerBase.php';

/**
 * Testbrandultra Partner classes
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Testbrandultra extends \Ultra\Lib\Api\PartnerBase
{

}


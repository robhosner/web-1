<?php

namespace Ultra\Lib\Api\Partner;

use Ultra\Customers\Customer;

require_once 'Ultra/Lib/Api/PartnerBase.php';

class InternalAmdocs extends \Ultra\Lib\Api\PartnerBase
{
  const MAX_SOAP_LOG_RECORDS = 250; // max number of SOA_LOG records to fetch
  const MAX_SOAP_LOG_RANGE = 7776000; // change back to 5184000 when fixed AMDOCS-267; // max log range is 60 days

  /**
   * internal__GetMVNEDetails
   *
   * ACC adapter command GetMVNEDetails, which includes data from CheckBalance, QuerySubscriber, GetNetworkDetails.
   *
   * @return Result object
   */
  public function internal__GetMVNEDetails ()
  {
    list ( $ICCID , $MSISDN ) = $this->getInputValues();

    $error_code = '';

    $this->addArrayToOutput(array(
      'QuerySubscriber_CurrentAsyncService' =>'',
      'QuerySubscriber_wholesalePlan' =>'',
      'QuerySubscriber_IMSI' =>'',
      'QuerySubscriber_IMEI' =>'',
      'QuerySubscriber_AddOnFeatureInfoList' =>'',
      'QuerySubscriber_PlanEffectiveDate' =>'',
      'QuerySubscriber_MSISDN' =>'',
      'QuerySubscriber_AutoRenewalIndicator' =>'',
      'QuerySubscriber_SIM' =>'',
      'QuerySubscriber_PlanId' =>'',
      'QuerySubscriber_SubscriberStatus' =>'',
      'QuerySubscriber_PlanExpirationDate' =>'',

      'GetNetworkDetails_MSStatus' =>'',
      'GetNetworkDetails_NAPStatus' =>'',
      'GetNetworkDetails_SIMStatus' =>'',
      'GetNetworkDetails_INStatus' =>'',
      'GetNetworkDetails_SIM' =>'',
      'GetNetworkDetails_HLRFeatureList' =>'',
      'GetNetworkDetails_APNList' =>'',
      'GetNetworkDetails_MSISDN' =>'',
      'GetNetworkDetails_NAPFeatureList' =>'',
      'GetNetworkDetails_IMEI' =>'',
      'GetNetworkDetails_IMSI' =>'',

      'CheckBalance_MSISDN' =>'',
      'CheckBalance_BalanceValueList' =>'',

      'customer_id' => NULL
    ));

    try
    {
      if ( !$ICCID && !$MSISDN )
      {
        $error_code = 'NS0001';
        throw new \Exception("ERR_API_INVALID_ARGUMENTS: please provide ICCID or MSISDN");
      }

      $ICCID = luhnenize($ICCID);

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwGetMVNEDetails(
        array(
          'actionUUID' => $this->getRequestId(),
          'msisdn'     => $MSISDN,
          'iccid'      => $ICCID
        )
      );

      dlog('',"mwGetMVNEDetails result = %s",$result);

      // did mwGetMVNEDetails fail?
      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        $error_code = 'MW0001';
        throw new \Exception("ERR_API_INTERNAL: ".$errors[0]);
      }

      // did GetMVNEDetails fail?
      if ( ! $result->data_array['success'] )
      {
        $error_code = 'MW0001';
        throw new \Exception("ERR_API_INTERNAL: ".$result->data_array['errors'][0]);
      }

      if ( ! empty($result->data_array['QuerySubscriber']->wholesalePlan))
        $this->addToOutput('QuerySubscriber_wholesalePlan', $result->data_array['QuerySubscriber']->wholesalePlan);
      if ( ! empty($result->data_array['QuerySubscriber']->IMSI))
        $this->addToOutput('QuerySubscriber_IMSI', $result->data_array['QuerySubscriber']->IMSI);
      if ( ! empty($result->data_array['QuerySubscriber']->IMEI))
        $this->addToOutput('QuerySubscriber_IMEI', $result->data_array['QuerySubscriber']->IMEI);
      if ( ! empty($result->data_array['QuerySubscriber']->AddOnFeatureInfoList))
        $this->addToOutput('QuerySubscriber_AddOnFeatureInfoList', $result->data_array['QuerySubscriber']->AddOnFeatureInfoList);
      if ( ! empty($result->data_array['QuerySubscriber']->PlanEffectiveDate))
        $this->addToOutput('QuerySubscriber_PlanEffectiveDate',$result->data_array['QuerySubscriber']->PlanEffectiveDate);
      if ( ! empty($result->data_array['QuerySubscriber']->MSISDN))
        $this->addToOutput('QuerySubscriber_MSISDN', $result->data_array['QuerySubscriber']->MSISDN);
      if ( ! empty($result->data_array['QuerySubscriber']->AutoRenewalIndicator))
        $this->addToOutput('QuerySubscriber_AutoRenewalIndicator', $result->data_array['QuerySubscriber']->AutoRenewalIndicator);
      if ( ! empty($result->data_array['QuerySubscriber']->SIM))
        $this->addToOutput('QuerySubscriber_SIM', $result->data_array['QuerySubscriber']->SIM);
      if ( ! empty($result->data_array['QuerySubscriber']->PlanId))
        $this->addToOutput('QuerySubscriber_PlanId', $result->data_array['QuerySubscriber']->PlanId);
      if ( ! empty($result->data_array['QuerySubscriber']->SubscriberStatus))
        $this->addToOutput('QuerySubscriber_SubscriberStatus', $result->data_array['QuerySubscriber']->SubscriberStatus);
      if ( ! empty($result->data_array['QuerySubscriber']->PlanExpirationDate))
        $this->addToOutput('QuerySubscriber_PlanExpirationDate', $result->data_array['QuerySubscriber']->PlanExpirationDate);
      if ( ! empty($result->data_array['QuerySubscriber']->CurrentAsyncService))
        $this->addToOutput('QuerySubscriber_CurrentAsyncService', $result->data_array['QuerySubscriber']->CurrentAsyncService);
      if ( ! empty($result->data_array['GetNetworkDetails']->MSStatus))
        $this->addToOutput('GetNetworkDetails_MSStatus', $result->data_array['GetNetworkDetails']->MSStatus);
      if ( ! empty($result->data_array['GetNetworkDetails']->NAPStatus))
        $this->addToOutput('GetNetworkDetails_NAPStatus',$result->data_array['GetNetworkDetails']->NAPStatus);
      if ( ! empty($result->data_array['GetNetworkDetails']->SIMStatus))
        $this->addToOutput('GetNetworkDetails_SIMStatus', $result->data_array['GetNetworkDetails']->SIMStatus);
      if ( ! empty($result->data_array['GetNetworkDetails']->INStatus))
        $this->addToOutput('GetNetworkDetails_INStatus', $result->data_array['GetNetworkDetails']->INStatus);
      if ( ! empty($result->data_array['GetNetworkDetails']->SIM))
        $this->addToOutput('GetNetworkDetails_SIM', $result->data_array['GetNetworkDetails']->SIM);
      if ( ! empty($result->data_array['GetNetworkDetails']->HLRFeatureList))
        $this->addToOutput('GetNetworkDetails_HLRFeatureList', $result->data_array['GetNetworkDetails']->HLRFeatureList);
      if ( ! empty($result->data_array['GetNetworkDetails']->APNList))
        $this->addToOutput('GetNetworkDetails_APNList', $result->data_array['GetNetworkDetails']->APNList);
      if ( ! empty($result->data_array['GetNetworkDetails']->MSISDN))
        $this->addToOutput('GetNetworkDetails_MSISDN', $result->data_array['GetNetworkDetails']->MSISDN);
      if ( ! empty($result->data_array['GetNetworkDetails']->NAPFeatureList))
        $this->addToOutput('GetNetworkDetails_NAPFeatureList', $result->data_array['GetNetworkDetails']->NAPFeatureList);
      if ( ! empty($result->data_array['GetNetworkDetails']->IMEI))
        $this->addToOutput('GetNetworkDetails_IMEI', $result->data_array['GetNetworkDetails']->IMEI);
      if ( ! empty($result->data_array['GetNetworkDetails']->IMSI))
        $this->addToOutput('GetNetworkDetails_IMSI', $result->data_array['GetNetworkDetails']->IMSI);
      if ( ! empty($result->data_array['CheckBalance']->MSISDN))
        $this->addToOutput('CheckBalance_MSISDN', $result->data_array['CheckBalance']->MSISDN);
      if ( ! empty($result->data_array['CheckBalance']->BalanceValueList))
        $this->addToOutput('CheckBalance_BalanceValueList', $result->data_array['CheckBalance']->BalanceValueList);

      // API-40
      // Check if plan effective date is after new Data SOCs release date
      if (strtotime($this->result->data_array['QuerySubscriber_PlanEffectiveDate']) > strtotime(\Ultra\UltraConfig\getEmuSwitchDate()))
      {
        $this->result->data_array['CheckBalance_BalanceValueList']->BalanceValue = \Ultra\MvneConfig\parseEmuSwitchBalanceValueList(
          $this->result->data_array['CheckBalance_BalanceValueList']->BalanceValue,
          'internal'
        );
      }

      // prepare SELECT query for possibly multiple customers
      $params = array('select_fields' => array('customer_id', 'brand_id'));
      if ($ICCID)
        $params['iccid'] = $ICCID;
      if ($MSISDN)
        $params['current_mobile_number'] = $MSISDN;

      // get customer data and check for possible inconsistency
      teldata_change_db();
      $customers = get_ultra_customers_from_multiple($params);
      if (count($customers) == 1)
        $this->addToOutput('customer_id', $customers[0]->customer_id);
      elseif (count($customers) > 1)
        dlog('', "ERROR ESCALATION ALERT DAILY $MSISDN $ICCID DB inconsistency - " .
          $customers[0]->customer_id . ' : ' . $customers[1]->customer_id);

      if (!empty($result->data_array['CheckBalance']->BalanceValueList)) {
        if ($customers[0]->brand_id != 3) {
          $balanceValueList = $result->data_array['CheckBalance']->BalanceValueList->BalanceValue;
          $i = 0;
          $planThrottles = [
            'WPRBLK20',
            'WPRBLK31S',
            'WPRBLK33S',
            'WPRBLK35',
            'WPRBLK35S',
            'WPRBLK37S',
            'WPRBLK39S',
          ];

          $boltOnThrottles = [
            '7007',
            'WPRBLK32S',
            'WPRBLK34S',
            'WPRBLK36S',
            'WPRBLK38S',
          ];

          $usedThrottleSpeed = 0;
          $showThrottleData = true;

          $plan = get_plan_from_cos_id(get_account_from_customer_id($customers[0]->customer_id, ['COS_ID'])->COS_ID);

          foreach ($balanceValueList as $item) {
            if ($item->UltraName == 'Data Trigger Action' && $item->UltraType == 'Block') {
              $showThrottleData = false;
            }

            if ($item->UltraName == 'Voice Minutes') {
              $item->UltraName = 'Voice';
              $item->UltraType = 'Minutes Available';
            }

            if ($item->UltraName == 'SMS Messages') {
              $item->UltraName = 'SMS';
              $item->UltraType = 'Messages Available';
            }

            if ($plan == 'UV30' && $item->Type == 'WPRBLK20') {
              unset($balanceValueList[$i]);
              continue;
            }

            if ($item->UltraName == 'Plan Included Data Total' || (in_array($item->Type, $planThrottles) && $item->UltraType == $item->Type . ' Limit')) {
              $item->UltraName = (in_array($item->Type, ['WPRBLK39S'])) ? 'High Speed Data' : 'Plan Data';
              $item->UltraType = 'Limit';

              foreach ($balanceValueList as $item2) {
                if ($item2->UltraName == 'Plan Included Data Used' || (in_array($item2->Type, $planThrottles) && $item2->UltraType == $item->Type . ' Used')) {
                  $item2->UltraName = (in_array($item2->Type, ['WPRBLK39S'])) ? 'High Speed Data' : 'Plan Data';
                  $item2->UltraType = 'Remaining';

                  if ($item2->Unit == 'KB') {
                    $item2->Value = $item2->Value / 1000;
                    $item2->Unit = 'MB';
                  }

                  $item2->Value = $item->Value - $item2->Value;
                }
              }
            }

            if ($item->UltraName == 'Data Add-on Total' || (in_array($item->Type, $boltOnThrottles) && $item->UltraType == $item->Type . ' Limit')) {
              $item->UltraName = 'Bolt-on Data';
              $item->UltraType = 'Limit';

              foreach ($balanceValueList as $item2) {
                if ($item2->UltraName == 'Data Add-on Used' || (in_array($item2->Type, $boltOnThrottles) && $item2->UltraType == $item2->Type . ' Used')) {
                  $item2->UltraName = 'Bolt-on Data';
                  $item2->UltraType = 'Remaining';

                  if ($item2->Unit == 'KB') {
                    $item2->Value = $item2->Value / 1000;
                    $item2->Unit = 'MB';
                  }

                  $item2->Value = $item->Value - $item2->Value;
                }
              }
            }

            if ($item->UltraType == "Base Data Usage Used" || $item->UltraType == "WPRBLK40 Used") {
              $usedThrottleSpeed += $item->Unit == 'KB' ? $item->Value / 1000 : $item->Value;
            }

            if ($item->UltraType == 'Total Data Usage Used' || $item->UltraType == 'IN Rated Data Usage Used' ||
                $item->UltraType == "Base Data Usage Used" || $item->UltraType == "Base Data Usage Limit" ||
                $item->UltraType == "WPRBLK40 Used" || $item->UltraType == "WPRBLK40 Limit"
            ) {
              unset($balanceValueList[$i]);
            }

            $i++;
          }

          $throttleSpeeds = [
            'full' => 'Full Speed',
            '1536' => 'Optimized',
            '1024' => 'Super Saver'
          ];

          $lteSelect = new \stdClass();
          $lteSelect->UltraName = 'LTE Select Speed';
          $lteSelect->UltraType = $plan == 'UV30' ? 'N/A' : $throttleSpeeds[(new Customer((array) $customers[0]))->getThrottleSpeed()];
          $lteSelect->Name = '';
          $lteSelect->Type = '';
          $lteSelect->Unit = '';
          $lteSelect->Value = '';
          $balanceValueList[] = $lteSelect;

          if ($showThrottleData) {
            $usedThrottle = new \stdClass();
            $usedThrottle->UltraName = 'Throttle Data';
            $usedThrottle->UltraType = 'Used';
            $usedThrottle->Name = '';
            $usedThrottle->Type = '';
            $usedThrottle->Unit = 'MB';
            $usedThrottle->Value = $usedThrottleSpeed;
            $balanceValueList[] = $usedThrottle;
          }

          $result->data_array['CheckBalance']->BalanceValueList->BalanceValue = array_values($balanceValueList);
        }
        $this->addToOutput('CheckBalance_BalanceValueList', $result->data_array['CheckBalance']->BalanceValueList);
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }

  /**
   * internal__ProxyNotification
   *
   * Use to redirect notifications from one end-point (typically PROD) to another end-point (typically DEV)
   * Invoked by runners/amdocs/acc_notification_server_prod.pl
   *
   * @return Result object
   */
  public function internal__ProxyNotification ()
  {
    list ( $operation , $data ) = $this->getInputValues();

    $decoded_data = json_decode( $data );

    dlog('',"decoded_data = %s",$decoded_data );

    $error_code = '';

    try
    {
      /*
            $wsdl = \Ultra\UltraConfig\acc_notification_wsdl();

            $wsdl = str_replace('production','development',$wsdl);

            define('WSDL', 'runners/amdocs/'.$wsdl);

            $allowed_operations = array(
              'NotificationReceived',
              'PortOutDeactivation',
              'PortOutRequest',
              'ProvisioningStatus',
              'ThrottlingAlert',
            );

            if ( in_array( $operation , $allowed_operations ) )
            {
              // create SOAP client instance
              $client = new \SoapClient(WSDL);

              $response = $client->$operation($decoded_data);

              dlog('',"response = %s",$response);
            }
            else
              $this->errException('ERR_API_INTERNAL: operation currently not handled : '.$operation, '------');
      */
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage());
    }

    return $this->result;
  }


  /**
   * internal__PortOutDeactivation
   *
   * Deactivates a ported out subscriber.
   * Invoked by our notification server for PortOutDeactivation.
   *
   * @return Result object
   */
  public function internal__PortOutDeactivation ()
  {
    list ( $MSISDN , $portOutCarrierID , $portOutCarrierName , $requestID ) = $this->getInputValues();

    $error_code = '';

    try
    {
      teldata_change_db();

      // get customer id from msisdn
      $sql = make_find_ultra_customer_query_from_msisdn( $MSISDN );

      $customers = mssql_fetch_all_objects(logged_mssql_query($sql));

      if ( ! ( $customers && is_array( $customers ) && count( $customers ) ) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      if ( count( $customers ) > 1 )
        $this->errException('ERR_API_INVALID_ARGUMENTS: Found multiple customers for msisdn '.$MSISDN, 'VV0068');

      $ever_active = ( ( $customers[0]->plan_state == 'Active' ) || ( $customers[0]->plan_state == 'Suspended' ) ) ? 1 : 0 ;

      $result = htt_cancellation_reasons_add(
        array(
          'customer_id'              => $customers[0]->CUSTOMER_ID,
          'reason'                   => 'Port Out',
          'type'                     => 'PORTOUT',
          #'portout_ocn'              => $ocn,
          #'portout_carrier_name'     => $carrier_name,
          'portout_ocn_request_date' => 'getutcdate()',
          #'portout_spid'             => $data_row['carrier_spid'],
          #'deactivation_date'        => $data_row['deactivation_date'],
          'carrier_name'             => $portOutCarrierName,
          'carrier_id'               => $portOutCarrierID,
          'msisdn'                   => $MSISDN,
          'iccid'                    => $customers[0]->CURRENT_ICCID_FULL,
          'cos_id'                   => $customers[0]->cos_id,
          'status'                   => $customers[0]->plan_state,
          'ever_active'              => $ever_active,
          'agent'                    => 'internal__PortOutDeactivation'
        )
      );

      if ( ! $result->is_success() )
        $this->errException( "ERR_API_INTERNAL: DB write error." , 'DB0001' );

      $context = array(
        'customer_id' => $customers[0]->CUSTOMER_ID
      );

      // transition the customer to 'Cancelled'
      $result_status = cancel_account( $customers[0] , $context , NULL , 'internal__PortOutDeactivation' , 'Ported Out' , 'Port Out API' );

      if ( count($result_status['errors']) )
      {
        dlog('',"%s",$result_status['errors']);
        $this->errException( "ERR_API_INTERNAL: state transition error" , 'SM0001' );
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }


  /**
   * internal__CheckBalance
   *
   * ACC adapter command for CheckBalance; used by BARK tool
   *
   * @return Result object
   */
  public function internal__CheckBalance()
  {
    list ($iccid, $msisdn) = $this->getInputValues();
    $this->addToOutput('customer_id', NULL);
    $this->addToOutput('ICCID', NULL);
    $this->addToOutput('MSISDN', NULL);
    $this->addToOutput('BalanceValueList', NULL);
    $this->addToOutput('XML', NULL);
    $customer = NULL;

    try
    {
      // check input and init
      if (! $iccid && !$msisdn)
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing ICCID or MSISDN', 'NS0001');
      $iccid = $iccid ? luhnenize($iccid) : NULL;
      $msisdn = $msisdn ? normalize_msisdn($msisdn, FALSE) : NULL;

      // get customer record and check result
      teldata_change_db();
      $clause = ($iccid ? array('CURRENT_ICCID_FULL' => $iccid) : array('CURRENT_MOBILE_NUMBER' => $msisdn));
      $query = \Ultra\Lib\DB\makeSelectQuery('HTT_CUSTOMERS_OVERLAY_ULTRA', 2,
        array('CUSTOMER_ID', 'CURRENT_MOBILE_NUMBER', 'CURRENT_ICCID_FULL', 'MVNE'), $clause);
      $customers = mssql_fetch_all_objects(logged_mssql_query($query));
      if (count($customers) == 1)
      {
        $customer = $customers[0];
        $this->addToOutput('customer_id', $customer->CUSTOMER_ID);
      }
      if (count($customers) < 1)
        $this->AddWarning('ERR_API_INVALID_ARGUMENTS: the customer does not exist');
      if (count($customers) > 1)
        $this->addWarning('ERR_API_INVALID_ARGUMENTS: found multiple customers');

      // get ICCID if missing
      if (! $iccid && $customer)
        $iccid = $customer->CURRENT_ICCID_FULL ? $customer->CURRENT_ICCID_FULL : luhnenize($customer->CURRENT_ICCID);
      $this->addToOutput('ICCID', $iccid);

      // get MSISDN if missing: API cannot continue without it
      if (! $msisdn)
      {
        if ($customer && $customer->CURRENT_MOBILE_NUMBER)
          $msisdn = $customer->CURRENT_MOBILE_NUMBER;
        else
          $this->errException('ERR_API_INVALID_ARGUMENTS: customer missing MSISDN', 'VV0031');
      }
      $this->addToOutput('MSISDN', $msisdn);

      // verify MVNE2
      if ($customer && $customer->MVNE != self::MVNE2)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer MVNE', 'MV0001');

      // call middleware
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $result = $mwControl->mwCheckBalance(
        array(
          'actionUUID' => $this->getRequestId(),
          'msisdn'     => $msisdn));

      // check results
      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: MW error (1)', 'MW0001');
      if ( ! $result->data_array['success'] || empty($result->data_array['body']) || empty($result->data_array['body']->BalanceValueList))
        $this->errException('ERR_API_INTERNAL: ' .
          (empty($result->data_array['ResultMsg']) ? 'ACC errors - ' : $result->data_array['ResultMsg']),
          'MW0001');

      // extract balance values
      $this->addToOutput('BalanceValueList', $result->data_array['body']->BalanceValueList);

      // get XML
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');
      $log = get_soap_log(array('session_id' => $result->data_array['body']->serviceTransactionId));
      if (count($log))
        $this->addToOutput('XML', $log[0]->xml);

      // we are done
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'ERROR: ' . $e->getMessage());
    }

    return $this->result;
  }

  
  /**
   * internal__GetNetworkDetails
   *
   * ACC adapter command for GetNetworkDetails; used by BARK tool
   *
   * @return Result object
   */
  public function internal__GetNetworkDetails()
  {
    list ($iccid, $msisdn) = $this->getInputValues();
    $this->addToOutput('customer_id', NULL);
    $this->addToOutput('ICCID', NULL);
    $this->addToOutput('MSISDN', NULL);
    $this->addToOutput('XML', NULL);
    $customer = NULL;

    try
    {
      // check input and init
      if (! $iccid && !$msisdn)
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing ICCID or MSISDN', 'NS0001');
      $iccid = $iccid ? luhnenize($iccid) : NULL;
      $msisdn = $msisdn ? normalize_msisdn($msisdn, FALSE) : NULL;

      // get customer record and check result
      teldata_change_db();
      $clause = ($iccid ? array('CURRENT_ICCID_FULL' => $iccid) : array('CURRENT_MOBILE_NUMBER' => $msisdn));
      $query = \Ultra\Lib\DB\makeSelectQuery('HTT_CUSTOMERS_OVERLAY_ULTRA', 2,
        array('CUSTOMER_ID', 'CURRENT_MOBILE_NUMBER', 'CURRENT_ICCID_FULL', 'MVNE'), $clause);
      $customers = mssql_fetch_all_objects(logged_mssql_query($query));
      if (count($customers) == 1)
      {
        $customer = $customers[0];
        $this->addToOutput('customer_id', $customer->CUSTOMER_ID);
      }
      if (count($customers) < 1)
        $this->AddWarning('ERR_API_INVALID_ARGUMENTS: the customer does not exist');
      if (count($customers) > 1)
        $this->addWarning('ERR_API_INVALID_ARGUMENTS: found multiple customers');

      // verify MVNE
      if ($customer && $customer->MVNE != self::MVNE2)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid customer MVNE', 'MV0001');

      // ICCID is required to proceed, get it from customer if missing
      if (! $iccid && $customer)
        $iccid = $customer->CURRENT_ICCID_FULL ? $customer->CURRENT_ICCID_FULL : luhnenize($customer->CURRENT_ICCID);
      if (! $iccid)
        $this->errException('ERR_API_INVALID_ARGUMENTS: subscriber is missing ICCID', 'NS0001');
      $this->addToOutput('ICCID', $iccid);

      // call middleware
      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $params = array('actionUUID' => $this->getRequestId(), 'iccid' => $iccid);
      $result = $mwControl->mwGetNetworkDetails($params);
  
      // check results
      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: MW error (1)', 'MW0001');
      if (! empty($result->data_array['ResultCode']) && $result->data_array['ResultCode'] != '100')
        $this->errException('ERR_API_INTERNAL: ACC errors - ' . $result->data_array['errors'][0], 'MW0001');
      if ( ! $result->data_array['success'] || empty($result->data_array['body']))
        $this->errException('ERR_API_INTERNAL: MW error (2)', 'MW0001');
  
      // extract values if present: some may be missing depending on subscriber state
      $properties = array('MSISDN', 'MSStatus', 'NAPStatus', 'SIMStatus', 'INStatus', 'HLRFeatureList', 'APNList', 'NAPFeatureList',
        'IMEI', 'IMSI');
      foreach ($properties as $property)
        $this->addToOutput($property, empty($result->data_array['body']->$property) ? NULL : $result->data_array['body']->$property);

      // get XML
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');
      $log = get_soap_log(array('session_id' => $result->data_array['body']->serviceTransactionId));
      if (count($log))
        $this->addToOutput('XML', $log[0]->xml);

      // we are done
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'ERROR: ' . $e->getMessage());
    }

    return $this->result;
  }
}

?>

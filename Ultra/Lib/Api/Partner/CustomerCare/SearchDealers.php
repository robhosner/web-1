<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class SearchDealers extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__SearchDealers
   *
   * given a search criteria return a list of matching dealer records
   * @param string business name (or part of it)
   * @return object Result
   */
  public function customercare__SearchDealers()
  {
    // initialize
    list ($business_name) = $this->getInputValues();
    $dealers = array();

    try
    {
      teldata_change_db();
      $dealers = search_dealers_by_business_name($business_name);
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('' ,'EXCEPTION: ' . $e->getMessage());
    }

    $this->addToOutput('dealers', $dealers);
    $this->addToOutput('record_count', count($dealers));
    return $this->result;
  }
}

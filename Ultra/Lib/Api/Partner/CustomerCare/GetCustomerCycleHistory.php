<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class GetCustomerCycleHistory extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__GetCustomerCycleHistory
   *
   * return a list containing the cycle history associated with a given subscriber
   * @param int customer_id
   * @return object Result
   */
  public function customercare__GetCustomerCycleHistory()
  {
    // initialize
    list ( $customer_id ) = $this->getInputValues();
    $history = array();

    try
    {
      // get customer record
      teldata_change_db();

      if ( ! $customer = get_ultra_customer_from_customer_id( $customer_id, array( 'customer_id' ) ) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // get plans
      $sql = htt_plan_tracker_select_query(array('customer_id' => $customer_id));
      $plans = mssql_fetch_all_objects(logged_mssql_query($sql));
      if ( ! $plans || ! is_array($plans) || ! count($plans) )
        $this->errException('ERR_API_INTERNAL: no data found', 'ND0001');

      // process each row into array(plan, period, plan_started, plan_expires)
      date_default_timezone_set('UTC');
      foreach ($plans as $plan)
        $history[] = array(
          'plan'          => get_plan_from_cos_id($plan->cos_id),
          'period'        => $plan->period,
          'plan_started'  => strtotime($plan->plan_started),
          'plan_expires'  => strtotime($plan->plan_expires));

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('' , 'EXCEPTION: ' . $e->getMessage());
    }

    $this->addToOutput('cycle_history', $history);
    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';
require_once 'Ultra/Lib/Services/FamilyAPI.php';
require_once 'classes/Flex.php';

class ForceJoinFamily extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__ForceJoinFamily
   *
   * @param int customer_id
   * @return object Result
   */
  public function customercare__ForceJoinFamily()
  {
    list ($parent_customer_id, $child_customer_id) = $this->getInputValues();

    try
    {
      // get basic info
      $familyApi = new \Ultra\Lib\Services\FamilyAPI();

      // get parent family info
      $parentFamily = $getFamilyByCustomerIDResult = $familyApi->getFamilyByCustomerID($parent_customer_id);

      if ( ! $parentFamily->is_success())
        $this->errException('ERR_API_INTERNAL: error retrieving child_customer_id family info', 'DB0001');

      $parentFamilyId   = $parentFamily->data_array['id'];
      $parentInviteCode = $parentFamily->data_array['inviteCode'];

      // get child family info
      $childFamily = $getFamilyByCustomerIDResult = $familyApi->getFamilyByCustomerID($child_customer_id);

      $childFamilyId = ($childFamily->is_success()) ? $childFamily->data_array['id'] : null;

      // maybe remove child from its family
      $removedFromFamily = false;
      if ($childFamilyId && $childFamilyId != $parentFamilyId)
      {
        $removeFamiylResult = $familyApi->removeFamilyMember($childFamilyId, $child_customer_id);
        if ( ! $removeFamiylResult->is_success())
          $this->errException('ERR_API_INTERNAL: error removing child_customer_id from family', 'DB0001');

        $removedFromFamily = true;
      }

      // join child_customer_id to parent_customer_id family
      $joinFamilyResult = $familyApi->joinFamily($child_customer_id, $parentInviteCode);
      if ( ! $joinFamilyResult->is_success())
      {
        $but = ($removedFromFamily) ? ' but was removed from family' : '';
        $this->errException('ERR_API_INTERNAL: error joining child_customer_id to new family' . $but, 'DB0001');
      }

      $activateFamilyMembersResult = \Ultra\Lib\Flex::activateFamilyMembers($child_customer_id);
      if ( ! $activateFamilyMembersResult)
        $this->errException('ERR_API_INTERNAL: error activating child_customer_id to new family', 'IN0001');

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\CustomerCare;
use Ultra\Lib\Api\Traits\TaxesAndFees;
use Ultra\Taxes\TaxesAndFeesCalculator;

class CalculateTaxesAndFees extends CustomerCare
{
  /**
   * @var TaxesAndFeesCalculator
   */
  private $taxesAndFeesCalculator;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Configuration
   */
  private $configuration;

  use TaxesAndFees;

  public function __construct(TaxesAndFeesCalculator $taxesAndFeesCalculator, CustomerRepository $customerRepository, Configuration $configuration)
  {
    $this->taxesAndFeesCalculator = $taxesAndFeesCalculator;
    $this->customerRepository = $customerRepository;
    $this->configuration = $configuration;
  }

  public function customercare__CalculateTaxesAndFees()
  {
    list ($msisdn, $charge_amount, $zip, $product_type) = $this->getInputValues();

    try {
      teldata_change_db();

      $customer = $this->customerRepository->getCustomerFromMsisdn($msisdn, ['u.customer_id'], true);

      if (!$customer) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }

      $result = $this->calculateTaxesAndFees(
        $this->customerRepository,
        $this->taxesAndFeesCalculator,
        $this->configuration,
        $customer->customer_id,
        $zip,
        $charge_amount,
        $product_type
      );

      $this->addToOutput('sales_tax', $result->data_array['sales_tax'] + $result->data_array['mts_tax']);
      $this->addToOutput('recovery_fee', $result->data_array['recovery_fee']);

      $this->succeed();
    } catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

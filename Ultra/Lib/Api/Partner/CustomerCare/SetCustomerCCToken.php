<?php
namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\Lib\Api\Traits\CCHandler;
use Ultra\Configuration\Configuration;
use Ultra\CreditCards\CreditCardValidator;
use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Exceptions\SaveMethodFailedException;
use Ultra\Exceptions\UnhandledCCProcessorException;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Lib\Api\Partner\CustomerCare;
use Ultra\Utilities\Common;

/**
 * Class SetCustomerCCToken
 * @package Ultra\Lib\Api\Partner\CustomerCare
 */
class SetCustomerCCToken extends CustomerCare
{
  use CCHandler;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  private $customer;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var CreditCardRepository
   */
  private $cardRepository;

  /**
   * @var CreditCardValidator
   */
  private $creditCardValidator;
  
  /**
   * @var Common
   */
  private $utilities;

  /**
   * SetCustomerCCToken constructor.
   * @param Common $utilities
   * @param CustomerRepository $customerRepository
   * @param Configuration $configuration
   * @param CreditCardRepository $cardRepository
   */
  public function __construct(
    Common $utilities,
    CustomerRepository $customerRepository,
    Configuration $configuration,
    CreditCardRepository $cardRepository
  )
  {
    $this->utilities = $utilities;
    $this->customerRepository = $customerRepository;
    $this->cardRepository = $cardRepository;
    $this->configuration = $configuration;
  }

  /**
   * @return NULL
   */
  public function customercare__SetCustomerCCToken()
  {
    try {
      // connect to the DB
      $this->utilities->teldataChangeDb();

      $this->validateAndSaveToken(
        $this->configuration,
        $this->customerRepository,
        $this->cardRepository,
        $this->getNamedInputValues()
      );

      $this->succeed();

    } catch (InvalidObjectCreationException $e) {
      if (is_object($this->customer) && !empty($e->getCustomUserErrors()[$this->customer->preferred_language])) {
        $this->addError($e->getMessage(), $e->code(), $e->getCustomUserErrors()[$this->customer->preferred_language]);
      } elseif (!empty($e->getCustomUserErrors()[0])) {
        $this->addError($e->getMessage(), $e->code(), $e->getCustomUserErrors()[0]);
      } else {
        $this->addError($e->getMessage(), $e->code());
      }
    } catch (SaveMethodFailedException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), 'DB0001');
    } catch(CustomErrorCodeException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), $e->code(), $e->getMessage());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

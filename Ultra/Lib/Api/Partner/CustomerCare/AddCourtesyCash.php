<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class AddCourtesyCash extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__AddCourtesyCash
   *
   * Gives the customer increased $ that can be used for anything; but will not be commissioned.
   *
   * @param string reason user visible
   * @param string agent_name
   * @param int customercare_event_id
   * @param int customer_id
   * @param int amount
   * @return object Result
   */
  public function customercare__AddCourtesyCash()
  {
    list ($reason, $agent_name, $customercare_event_id, $customer_id, $amount) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_customer_from_customer_id($customer_id);
      if (!$customer)
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }

      $result = func_courtesy_add_balance(
        array(
          'reason'      => $reason,
          'terminal_id' => $agent_name,
          'reference'   => $customercare_event_id,
          'amount'      => ( $amount / 100 ),
          'customer'    => $customer,
          'detail'      => __FUNCTION__
        )
      );

      if (count($result['errors']))
      {
        $this->errException('ERR_API_INTERNAL: ' . $result['errors'][0], 'DB0001');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

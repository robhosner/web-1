<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Lib\Api\Partner\CustomerCare;
use Ultra\Payments\CreditCardCharger;

class ChargeCreditCard extends CustomerCare
{
  /**
   * @var CreditCardCharger
   */
  private $creditCardCharger;

  public function __construct(CreditCardCharger $creditCardCharger)
  {
    $this->creditCardCharger = $creditCardCharger;
  }

  public function customercare__ChargeCreditCard()
  {
    // init
    list ($customer_id, $charge_amount, $destination, $product_type) = $this->getInputValues();

    try {
      teldata_change_db();
      $this->creditCardCharger->chargeCreditCard($customer_id, $charge_amount, $destination, $product_type, __FUNCTION__, $this->getRequestId());
      $this->succeed();
    } catch (CustomErrorCodeException $e) {
      dlog('' , $e->getMessage());
      $this->addError($e->getMessage(), $e->code());
    } catch(\Exception $e) {
      dlog('' , $e->getMessage());
    }

    return $this->result;
  }
}

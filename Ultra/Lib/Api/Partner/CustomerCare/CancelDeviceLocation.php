<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class CancelDeviceLocation extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__CancelDeviceLocation
   *
   * Use msisdn and iccid_full and do a canceldevicelocation . No need to wait for it to finish.
   *
   * @param integer customer_id
   * @return object Result
   */
  public function customercare__CancelDeviceLocation()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id,
        array("customer_id","current_mobile_number","mvne","current_iccid_full"));

      if (!$customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $result = $mwControl->mwCancelDeviceLocation(
        array(
          'actionUUID' => $this->getRequestId(),
          'msisdn'     => $customer->current_mobile_number,
          'iccid'      => $customer->current_iccid_full
        )
      );

      dlog('',"mwCancelDeviceLocation result = %s",$result);

      if ($result->has_errors())
      {
        $errors = $result->get_errors();
        $this->errException('ERR_API_INTERNAL:' . $errors[0], 'MW0001');
      }
      else
      {
        if ( isset( $result->data_array['errors'] )
          && is_array($result->data_array['errors'])
          && count( $result->data_array['errors'] ) )
        {
          $this->errException('ERR_API_INTERNAL:' . $result->data_array['errors'][0], 'MW0001');
        }
        elseif ( isset( $result->data_array['errors'] )
          && !is_array($result->data_array['errors']) )
        {
          $this->errException('ERR_API_INTERNAL:' . $result->data_array['errors'], 'MW0001');
        }
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

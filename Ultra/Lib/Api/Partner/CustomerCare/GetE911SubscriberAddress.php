<?php
namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\CustomerCare;

/**
 * Class GetE911SubscriberAddress
 * @package Ultra\Lib\Api\Partner\Portal
 */
class GetE911SubscriberAddress extends CustomerCare
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * GetE911SubscriberAddress constructor.
   * @param CustomerRepository $customerRepository
   */
  public function __construct(CustomerRepository $customerRepository)
  {
    $this->customerRepository = $customerRepository;
  }

/**
   * customercare__GetE911SubscriberAddress
   *
   * Retrieve customer E911 Subscriber Address
   *
   * @param  Integer $customer_id
   * @return Result object
   */
  public function customercare__GetE911SubscriberAddress()
  {
    list($customer_id) = $this->getInputValues();

    try {
      $address = $this->customerRepository->getCustomerById($customer_id, [], true)->getAccAddressE911();

      if (empty($address)) {
       return $this->errException('ERR_API_INTERNAL: No address found.', 'ND0001');
      }

      $this->addToOutput('address', $address);
      $this->succeed();
    } catch (MissingRequiredParametersException $e) {
      $this->addError($e->getMessage(), 'VV0031');
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

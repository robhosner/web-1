<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class EligibleChangeNumber extends \Ultra\Lib\Api\Partner\CustomerCare
{
  private static $lastDayThreshold = 7;

  /**
   * @param int customer_id
   * @return object Result
   */
  public function customercare__EligibleChangeNumber()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      // get plan state and current_mobile_number
      $customer = get_ultra_customer_from_customer_id($customer_id, ['plan_state','current_mobile_number']);
      if ( ! $customer)
        $this->errException('ERR_API_INTERNAL: customer does not exist', 'VV0031');

      // plan state must be active
      if ($customer->plan_state != STATE_ACTIVE)
        $this->errException('ERR_API_INTERNAL: invalid plan state', 'IN0001');

      // check for recent MSISDN replacement, returns NULL if none
      $item = htt_billing_history_get_recent_msisdn_replacement($customer_id, self::$lastDayThreshold);
      if ($item)
        $this->errException('ERR_API_INTERNAL: phone number recently changed', 'IN0002');

      // ineligible if open transition pending
      $open_transition_count = count_customer_open_transitions($customer_id);

      if ( $open_transition_count )
        $this->errException('ERR_API_INTERNAL: Subscriber has open transition pending.', 'CI0005');

      // ineligible if open MISO pending
      if ( \Ultra\Lib\MVNE\exists_open_task( $customer_id ) )
        $this->errException('ERR_API_INTERNAL: Subscriber has open MISO pending.', 'CI0005');

      // ask ACC database if current_mobile_number was ported in
      \Ultra\Lib\DB\ultra_acc_connect();
      $item = get_portin_queue_by_msisdn($customer->current_mobile_number);
      teldata_change_db();
      if ($item)
        $this->errException('ERR_API_INTERNAL: phone number was ported in', 'IN0002');

      // if reached here, is eligible
      $this->addToOutput('eligible', 1);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
      $this->addToOutput('eligible', 0);
    }

    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class VerifyInternationalDirectDialSettings extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__VerifyInternationalDirectDialSettings
   *
   * Get all allowed Bolt Ons
   *
   * @return Result object
   */
  public function customercare__VerifyInternationalDirectDialSettings ()
  {
    list ( $customer_id ) = $this->getInputValues();

    try
    {
      $error_code = '';

      // connect to the DB
      teldata_change_db();
      $customer = $this->getCustomerById($customer_id);

      if (!$this->accountAliasesAreOK($customer_id, $customer->current_mobile_number))
      {
        $this->errException('ERR_API_INTERNAL: ACCOUNT_ALIASES is inconsistent', 'UN0002', 'Could not find the correct account alias for the customer');
      }

      if (!$this->destinationOriginMapIsOk($customer_id, $customer->current_mobile_number))
      {
        $this->errException('ERR_API_INTERNAL: DESTINATION_ORIGIN_MAP is inconsistent', 'UN0003', 'Could not find the correct prefix for the customer');
      }

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

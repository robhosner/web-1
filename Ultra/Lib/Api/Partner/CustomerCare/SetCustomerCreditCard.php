<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class SetCustomerCreditCard extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * SetCustomerCreditCard
   *
   * Updates account Credit Card for a user
   * @return object Result
   */
  public function customercare__SetCustomerCreditCard()
  {
    // initialize
    list ( $customer_id, $account_cc_exp, $account_cc_cvv, $account_cc_number, $account_zipcode ) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer      = get_ultra_customer_from_customer_id($customer_id, array('CUSTOMER_ID'));
      $customers_row = customers_get_customer_by_customer_id($customer_id, array('LAST_NAME'));

      if ( $customer && $customers_row )
      {
        // validate and store credit card info - this includes tokenization
        $result = \Ultra\Lib\DB\Setter\Customer\cc_info(
          array(
            'customer_id'    => $customer_id,
            'cc_number'      => $account_cc_number,
            'cvv'            => $account_cc_cvv,
            'expires_date'   => $account_cc_exp,
            'cc_postal_code' => $account_zipcode
          ),
          $customers_row->LAST_NAME
        );

        if ( $result->is_failure() )
        {
          $errors = $result->get_errors();
          $this->errException( $errors[0] , $result->data_array['error_codes'][0] );
        }
      }
      else
      {
        $this->errException( 'ERR_API_INVALID_ARGUMENTS: customer not found', 'VV0031' );
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('' ,'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }
}
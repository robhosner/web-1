<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

class LoginForCustomer extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__LoginForCustomer
   *
   * generate login URL and token to a Customer Care Representative to allow login on customer's behalf
   * @see API-384
   * @return Result object
   */
  public function customercare__LoginForCustomer()
  {
  
    $params = $this->getNamedInputValues(); // customer_id iccid agent_name
    $login_url = NULL;

    try
    {
      // initialize
      $session = new \Session();
      $cache = new \Ultra\Lib\DB\Cache();

      // get caller's IP address and log all input values
      $params['ip'] = $session::getClientIp();
      logInfo('parameters: ' . json_encode($params));

      // validate IP address
      $crmIps = \Ultra\UltraConfig\getCrmIpAddresses();
      if (empty($crmIps))
        $this->errException('ERR_API_INTERNAL: Internal configuration error', 'IN0004');
      if ( ! isIpAddressInArray($params['ip'], $crmIps))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // vaidate subscriber
      if ( ! $customer = $cache->selectRow('HTT_CUSTOMERS_OVERLAY_ULTRA', 'CUSTOMER_ID', $params['customer_id']))
        $this->errException('ERR_API_INVALID_ARGUMENTS: The customer does not exist', 'VV0031');

      // validate ICCID (extra security check)
      if ($customer->CURRENT_ICCID_FULL != luhnenize($params['iccid']))
        $this->errException('ERR_API_INVALID_ARGUMENTS: This SIM is not valid', 'VV0013');

      // create authorization token
      if ( ! $token = $session::encryptToken($customer->CURRENT_MOBILE_NUMBER, $session::TOKEN_V3, array($session::SYSTEM_TOKEN)))
        $this->errException('ERR_API_INTERNAL: Could not generate a security token', 'SE0001');

      // get login URL based on subscriber's brand
      if ( ! $url = \Ultra\UltraConfig\getMobileLoginUrl($customer->BRAND_ID))
        $this->errException('ERR_API_INTERNAL: Internal configuration error', 'IN0004', 'Invalid mobile login URL');

      // success
      $login_url = "$url/$token/";
      $this->succeed();
    }

    catch (\Exception $e)
    {
      logError($e->getMessage());
    }

    $this->addToOutput('login_url', $login_url);
    return $this->result;
  }
}

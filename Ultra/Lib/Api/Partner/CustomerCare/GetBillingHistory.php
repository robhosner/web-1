<?php
namespace Ultra\Lib\Api\Partner\CustomerCare;

use Ultra\Lib\Api\Partner\CustomerCare;
use Ultra\Billing\Interfaces\BillingHistoryRepository;
use Ultra\Utilities\Common;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

/**
 * Class GetBillingHistory
 * @package Ultra\Lib\Api\Partner\CustomerCare
 */
class GetBillingHistory extends CustomerCare
{
  /**
   * @var BillingHistoryRepository
   */
  private $billingHistoryRepository;
  
  /**
   * @var Common
   */
  private $commonUtilities;

  /**
   * GetBillingHistory constructor.
   * @param BillingHistoryRepository $billingHistoryRepository
   * @param Common $commonUtilities
   */
  public function __construct(BillingHistoryRepository $billingHistoryRepository, Common $commonUtilities)
  {
    $this->billingHistoryRepository = $billingHistoryRepository;
    $this->commonUtilities = $commonUtilities;
  }

  /**
   * customercare__GetBillingHistory
   *
   * Returns billing history for a given customer.
   *
   * @param integer customer_id
   * @return object Result
   */
  public function customercare__GetBillingHistory()
  {
    list ($customer_id) = $this->getInputValues();
    
    try
    {
      if (!$customer_id) 
      { 
        return $this->errException('ERR_API_INVALID_ARGUMENTS: missing customer_id parameter', 'MP0001'); 
      }

      $history = $this->billingHistoryRepository->getBillingTransactionHistoryByCustomerId($customer_id);
      $count = count($history);
      $billingHistory = [];

      if (!$count)
      {
        $this->addWarning('No Data Found');
      }

      for ($i = 0, $details = [], $j = $count; $i < $j; $i++)
      {
        // save surcharge detail
        $row = $history[$i];

        if ($row->SURCHARGE_HISTORY_ID)
        {
          $details[] = sprintf('$%.2f %s', $row->AMOUNT, strtolower($row->SURCHARGE_TYPE));
        }

        // check if we are at the end of result set or current transaction
        if ($i == ($j - 1) || $row->order_id != $history[$i + 1]->order_id)
        {
          // append surcharge details to description
          if (count($details))
          {
            $row->DESCRIPTION .= ' (includes ' . implode(', ', $details) . ')';
            unset($details);
            $details = [];
          }
        }

        $billingHistory[] = implode("|", [
          $this->commonUtilities->getDateFromFullDate($row->TRANSACTION_DATE),
          $row->ENTRY_TYPE,
          $row->STORED_VALUE_CHANGE,
          $row->BALANCE_CHANGE,
          $row->DESCRIPTION,
          $row->SOURCE,
        ]);
      }

      $this->addToOutput('billing_history', $billingHistory);
      $this->succeed();
    }
    catch (\Exception $e)
    {
      $this->dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

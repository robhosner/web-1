<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class UpdatePortInformation extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__UpdatePortInformation
   *
   * @return object Result
   */
  public function customercare__UpdatePortInformation()
  {
    list ($numberToPort, $portAccountNumber, $portAccountPassword, $portAccountZipcode) = $this->getInputValues();

    try {
      # trim spaces
      $portAccountPassword = trim($portAccountPassword);
      $portAccountNumber   = trim($portAccountNumber);

      $request_id = NULL;
      $success    = FALSE;

      /* Disabled verification because there's no reliable way to find out
       * if the MSISDN and ICCID are the same, and in theory a second
       * port-in with the same MSISDN should not be possible.  Also see
       * MVNO-945. */

      teldata_change_db(); // connect to the DB

      save_port_account( $numberToPort , $portAccountNumber , $portAccountPassword );

      # $numberToPort must match the first port request and is used to find the correct customer_id

      $customer = NULL;

      $portInQueue = new \PortInQueue();

      $loadByMsisdnResult = $portInQueue->loadByMsisdn( $numberToPort );

      teldata_change_db(); // connect back to default DB

      $errors = $loadByMsisdnResult->get_errors();

      if ( ! count($errors) )
        if ( !$portInQueue->customer_id )
          $errors[] = 'ERR_API_INVALID_ARGUMENTS: previous porting attempt not found (2)';
        else
        {
          $query = make_find_ultra_customer_query_from_customer_id( $portInQueue->customer_id );
          $customer = find_customer( $query );
        }

      if (count($errors)) {
        foreach ($errors as $error) {
          $this->addError($error, 'IN0002', $error);
        }

        throw new \Exception();
      }

      /* *** get current state *** */
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

      if ( $state )
      {

        if ( $state['state'] == 'Port-In Requested' )
        {

          ##==-- add action for mvneUpdatePortIn --==##

          $action_seq  = NULL;
          $action_type = 'funcall';
          $action_name = 'mvneUpdatePortIn';
          $params      = array(
            $customer->CUSTOMER_ID,
            $numberToPort,
            $customer->CURRENT_ICCID_FULL,
            $portAccountNumber,
            $portAccountPassword,
            $portAccountZipcode,
            "__this_transition__",
            "__this_action__",
            "__this_action_seq__",
          );

          $context = array(
            'customer_id' => $customer->CUSTOMER_ID
          );

          $result_status = append_action($context,
            array('type' => $action_type,
              'name' => $action_name,
              'transaction' => NULL,
              'seq' => $action_seq
            ),
            $params,
            FALSE
          );

          if ( ! empty($result_status['transitions'][0]))
            unreserve_transition_uuid_by_pid($request_id = $result_status['transitions'][0]);

          if ( $result_status['success'] == 1 )
          {
            // memorize $request_id in Redis by customer_id
            $redis_port = new \Ultra\Lib\Util\Redis\Port();
            $redis_port->setRequestId( $customer->CUSTOMER_ID , $request_id );

            $success = TRUE;
          }
          else # append_action error
          { $errors[] = "ERR_API_INTERNAL: state transition error (0)"; }

        }
        else # customer not in correct state
        { $errors[] = "ERR_API_INTERNAL: customer state not valid"; }

      }
      else # cannot determine customer state
      { $errors[] = "ERR_API_INTERNAL: customer state could not be determined"; }

      if (count($errors)) {
        foreach ($errors as $error) {
          $this->addError($error, 'IN0002', $error);
        }

        throw new \Exception();
      }

      $this->succeed();
    } catch (\Exception $e) {

    }

    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class FlushStoredBalance extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__FlushStoredBalance
   *
   * Set stored balance to zero.
   *
   * @param integer customer_id
   * @return object Result
   */
  public function customercare__FlushStoredBalance()
  {
    list ($reason, $agent_name, $customercare_event_id, $customer_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_customer_from_customer_id($customer_id);

      if (!$customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // MVNO-2528: prevent flushing of zero balance
      if (!$stored_value = $customer->stored_value)
        $this->errException('ERR_API_INVALID_ARGUMENTS: stored balance is empty', 'VV0115');

      $return = func_flush_stored_value($customer_id);

      $success = $return['success'];
      $errors  = $return['errors'];

      if ($success)
      {
        $source = 'CSCOURTESY';

        $reference_source = get_reference_source($source);

        $history_params = array(
          "customer_id"            => $customer_id,
          "date"                   => 'now',
          "cos_id"                 => $customer->COS_ID,
          "entry_type"             => 'FLUSH',
          "stored_value_change"    => ( - $stored_value ),
          "balance_change"         => 0,
          "package_balance_change" => 0,
          "charge_amount"          => 0,
          "reference"              => create_guid('PHPAPI'),
          "reference_source"       => 'CS',
          "detail"                 => 'FlushStoredBalance',
          "description"            => $reason,
          "result"                 => 'COMPLETE',
          "source"                 => $source,
          "is_commissionable"      => 0
        );

        $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

        if (!is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)))
          $this->errException('ERR_API_INTERNAL: Error writing to HTT_BILLING_HISTORY', 'DB0001');
      }

      $fraud_data = array(
        'source' => $customercare_event_id,
        'dest' => $customer_id,
        'channel' => $agent_name,
        'channel_type' => 'customercare',
        'request_id' => $customercare_event_id
      );

      $fraud_status = ($errors) ? 'error' : 'success' ;

      fraud_event($customer, 'customercare', 'FlushStoredBalance', $fraud_status, $fraud_data);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

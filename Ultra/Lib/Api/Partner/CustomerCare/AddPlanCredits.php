<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';
require_once 'classes/LineCredits.php';

class AddPlanCredits extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * customercare__AddPlanCredits
   *
   * @param int customer_id
   * @return object Result
   */
  public function customercare__AddPlanCredits()
  {
    list ($customer_id, $amount) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $account = \get_account_from_customer_id($customer_id, ['cos_id']);

      if ( ! $account)
        throw new \Exception('ERR_API_INTERNAL: error pulling account info', 'DB0001');

      if ( ! \Ultra\Lib\Flex::isFlexPlan($account->cos_id))
        throw new \Exception('ERR_API_INTERNAL: not a flex customer', 'IN0002');

      $lineCredits = new \LineCredits();

      // do line credit transfer
      list ($result, $error_code) = $lineCredits->doIncrement($customer_id, $amount);
      if ( ! $result->is_success())
      {
        $errors = $result->get_errors();
        $this->errException("API_INVALID_ARGUMENTS: {$errors[0]}", $error_code);
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\CustomerCare;

require_once 'Ultra/Lib/Api/Partner/CustomerCare.php';

class SearchCustomers extends \Ultra\Lib\Api\Partner\CustomerCare
{
  /**
   * SearchCustomers
   *
   * given a search criteria return a list of matching customer records
   * @param string business name (or part of it)
   * @return object Result
   */
  public function customercare__SearchCustomers()
  {
    // initialize
    list ( $msisdn, $iccid, $name, $email, $actcode ) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customers_list = array();

      // try actcode
      if ( $actcode != '' )
      {
        if ( ( strlen($actcode) == 11 ) && is_numeric($actcode) )
        {
          $customer = get_customer_from_actcode($actcode);

          if ( $customer )
            $customers_list[] = $customer;
        }
        else
        {
          $this->errException( 'ERR_API_INVALID_ARGUMENTS: actcode is not valid', 'VV0020' );
        }
      }

      // try msisdn
      if ( strlen($msisdn) == 11 )
      {
        $msisdn = substr($msisdn,1);
      }

      if ( ( $msisdn != '' ) && is_numeric($msisdn) )
      {
        $customer = get_customer_from_msisdn($msisdn);

        if ( $customer )
          $customers_list[] = $customer;
      }

      // try iccid
      if ( ( $iccid != '' ) && is_numeric($iccid) && ( ! count($customers_list) ) )
      {
        $find_ultra_customer_query_from_iccid = make_find_ultra_customer_query_from_iccid($iccid);

        dlog('',$find_ultra_customer_query_from_iccid);

        $customer = find_customer($find_ultra_customer_query_from_iccid);

        if ( $customer )
          $customers_list[] = $customer;
      }

      // try name
      if ( ( $name != '' ) && ( ! count($customers_list) ) )
      {
        $customers_list = get_customers_from_name($name);
      }

      // try email
      if ( ( $email != '' ) && ( ! count($customers_list) ) )
        $customers_list = get_customers_from_email($email);

      if ( count($customers_list) )
      {
        // prepare output
        foreach($customers_list as $customer)
        {
          $customers[] = array(
            'customer_id'           => $customer->CUSTOMER_ID,
            'plan_state'            => $customer->plan_state,
            'plan_started'          => $customer->plan_started_epoch,
            'current_mobile_number' => $customer->current_mobile_number,
            'current_iccid_full'    => $customer->CURRENT_ICCID_FULL,
            'actcode'               => get_act_code_from_iccid( $customer->ACTIVATION_ICCID ),
            'MVNE'                  => $customer->MVNE
          );
        }
      }
      else
        $this->errException( 'ERR_API_INTERNAL: no customers found', 'VV0031' );

      $this->addToOutput( 'customers', $customers );

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('' ,'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }
}


<?php

namespace Ultra\Lib\Api\Partner;

require_once 'classes/TransactionMonitor.php';
require_once 'Ultra/Lib/Api/PartnerBase.php';

/**
 * Provisioning partner class
 *
 * @project ultra.me
 * @author bwalters@ultra.me
 */
class Provisioning extends \Ultra\Lib\Api\PartnerBase
{

  /**
   * provisioning__CheckOrangeActCode
   *
   * This API call is used to validate and check the status of an Orange Actcode.
   *
   * @return Result object
   */
  public function provisioning__CheckOrangeActCode()
  {
    try
    {
      list ($actcode, $mode, $brand) = $this->getInputValues();
      $error_code = '';

      $this->connectToDatabase();
      $this->throttleIfRequired();

      $sim        = $this->getSimFromActCode($actcode);
      $request_id = $this->getRequestIdFromICCID($sim->ICCID_FULL);

      if (\Ultra\UltraConfig\getShortNameFromBrandId($sim->BRAND_ID) != $brand)
      {
        $this->errException('ERR_API_INTERNAL: Invalid Activation Code', 'IC0004');
      }

      $offerIdMap = get_orange_plan_map();

      $this->addToOutput('sim_hot',           $sim->SIM_HOT);
      $this->addToOutput('sim_used',          $sim->SIM_ACTIVATED);
      $this->addToOutput('request_id',        $request_id);
      $this->addToOutput('stored_value',      $sim->STORED_VALUE);
      $this->addToOutput('reservation_time',  $sim->RESERVATION_TIME);
      $this->addToOutput('duration',          $sim->DURATION);
      $this->addToOutput('iccid',             $sim->ICCID_FULL);
      $this->addToOutput('plan_id',     get_plan_from_cos_id($offerIdMap[$sim->OFFER_ID][0]));

      // Each chunk of this IF block should be given a
      // specific method to encapsulate code for testing
      // application flow.
      if ($sim->CUSTOMER_ID)
      {
        $customer             = $this->getCustomerFromActCode($actcode);
        $user_state           = $customer->plan_state;
        $verified             = $this->verifySessionByCustomer($customer->CUSTOMER);
        $_REQUEST['zsession'] = session_save($customer);

        $this->addToOutput('user_state',      $user_state);
        $this->addToOutput('mins_since_act',  $customer->mins_since_act);
        $this->addToOutput('customer_id',     $customer->CUSTOMER_ID);

        $this->addToOutput('zsession',  $verified[2]);
        // $output_array['zsessionC'] = $customer->CUSTOMER;
        setcookielive('zsession',       $verified[2], $verified[1], '/', $_SERVER["SERVER_NAME"]);
        setcookielive('zsessionC',      $customer->CUSTOMER, $verified[1], '/', $_SERVER["SERVER_NAME"]);

        if ($user_state == 'Active')
        {
          $this->addToOutput('msisdn', (($customer->current_mobile_number > 0) ? $customer->current_mobile_number : 0));
        }

        if ($mode == 'PREACTIVATION')
        {
          $this->addToOutput('sim_ready_activate', FALSE);
        }
      }
      else
      {
        if ($mode == 'PREACTIVATION')
        {
          $sim_ready_activate = !!(($sim->SIM_HOT) && !($sim->SIM_ACTIVATED));
          $this->addToOutput('sim_ready_activate', $sim_ready_activate);

          if ($sim_ready_activate)
          {
            $result = $this->getSimAvailabilityFromMiddlewareControl($sim->ICCID_FULL);
            $this->addToOutput('sim_ready_activate', $result);

            // if the SIM comes back as ready to activate set a Redis flag with ttl = 5m noting that sim is 'good to activate'
            $redis = new \Ultra\Lib\Util\Redis;
            $redis->set( 'iccid/good_to_activate/' . $sim->ICCID_FULL, 1, 60 * 20);
          }
        }
      }

      $customer = $this->getCustomerFromICCID($sim->ICCID_FULL);
      if ($customer && is_object($customer) && $customer->CUSTOMER_ID)
      {
        $_REQUEST['zsession'] = session_save($customer);
        $verified             = $this->verifySessionByCustomer($customer->CUSTOMER);
        $this->addToOutput('customer_id', $customer->CUSTOMER_ID);
        $this->addToOutput('zsession',    $verified[2]);
        // $output_array['zsessionC'] = $customer->CUSTOMER;
        setcookielive('zsession',         $verified[2], $verified[1], '/', $_SERVER["SERVER_NAME"]);
        setcookielive('zsessionC',        $customer->CUSTOMER, $verified[1], '/', $_SERVER["SERVER_NAME"]);

        if ($customer->plan_state == 'Active')
        {
          $this->addToOutput('msisdn', (($customer->current_mobile_number > 0) ? $customer->current_mobile_number : 0));
        }
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  protected function throttleIfRequired()
  {
    // Bypass the throttling check, if the Partner is Shore, env = rainmaker_dev or env = rainmaker_prod
    // all others, fall through
    dlog('', "Throttling Environment  = %s", trim(strtolower(get_htt_env())));

    if ((trim(strtolower(get_htt_env())) != 'rainmaker_dev') && (trim(strtolower(get_htt_env())) != 'rainmaker_prod'))
    {
      // Check to see if the calling partner requires throttling
      $serverAddress = $_SERVER['REMOTE_ADDR'];
      dlog('', "Validating remote server = %s", $serverAddress);

      $redis = new \Ultra\Lib\Util\Redis;

      if (filter_var($serverAddress, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
      {
        // First check our cache to see if the Transaction Manager Object is present
        $key      = "TransactionManager/" . $serverAddress;
        $mcObject = $redis->get($key);
        $server   = null;

        if ($mcObject !== false) // We have a Transaction Manager object
        {
           $redis->del($key);
           $server = unserialize($mcObject);
           if (is_object($server))
           {
             $server->setAccessList(time()); // Set this access into our object

             if ($server->needsThrottling($key))
             {
               // Save off our incremented object, then inform the user they have been throttled.
               $redis->set($key, serialize($server));
               dlog('', "Rate Throttling remote server = %s, Key = %s", $serverAddress, $key);
               throw new \Exception("ERR_API_THROTTLE: You have made too many entries, please try again later.");
             }
             else
             {
               // Does not need throttling, just save off, and fall through to other logic.
               $redis->set($key, serialize($server));
             }
           }
           else
           {
             $server = new \TransactionMonitor($key);
             dlog('', "creating new key=%s", $key);
             $server->setAccessList(time()); // Set this access into our object
             $redis->set($key, serialize($server));
           }
         }
         else
         {
           // First request--at least on this reboot
           $server = new \TransactionMonitor($key);
           dlog('', "creating new key=%s", $key);
           $server->setAccessList(time()); // Set this access into our object
           $redis->set($key, serialize($server));
        }
      }
      else
      {
        // An invalid IP address
        dlog('',"POSSIBLE SECURITY VIOLATION--Invalid IP Address:remote server = %s", $serverAddress);
        throw new Exception("ERR_API_INTERNAL: Invalid IP/or Spoofed IP address detected.");
      }
    }
  }

  protected function getSimFromActCode($actcode)
  {
    $sim = get_htt_inventory_sim_from_actcode( $actcode );
    if ((!$sim) || (!is_array($sim)))
    {
      throw new \Exception("ERR_API_INTERNAL: DB Error");
    }

    if (!count($sim))
    {
      throw new \Exception("ERR_API_INTERNAL: actcode not found in DB");
    }

    // Strip parent array
    $sim = $sim[0];

    // add master_agent output before any errors
    $this->addToOutput('master_agent', $sim->INVENTORY_MASTERAGENT);

    if ($sim->INVENTORY_MASTERAGENT == '709')
      $this->errException('ERR_API_INTERNAL: CampusSim ICCID, being redirected', 'IC0004');

    if ($sim->PRODUCT_TYPE != 'ORANGE')
      throw new \Exception("ERR_API_INTERNAL: product type not valid", 'VV0065');

    dlog('', "SIM_HOT = %s , SIM_ACTIVATED = %s", $sim->SIM_HOT, $sim->SIM_ACTIVATED);

    return $sim;
  }

  protected function getRequestIdFromICCID($iccid)
  {
    return get_open_activation_transition_uuid_from_iccid($iccid);
  }

  protected function getCustomerFromActCode($actcode)
  {
    $customer = get_customer_from_actcode($actcode);

    if (!$customer)
    {
      throw new \Exception("ERR_API_INTERNAL: Customer not found");
    }

    return $customer;
  }

  protected function verifySessionByCustomer($customer)
  {
    return verify_session($customer);
  }

  protected function getMiddleWareControl()
  {
    return new \Ultra\Lib\MiddleWare\Adapter\Control();
  }

  protected function getSimAvailabilityFromMiddlewareControl($iccid = false, $middleware = false)
  {
    if (!$iccid)
    {
      throw new \Exception('ERR_API_INTERNAL: Missing ICCID for getSimAvailabilityFromMiddlewareControl()');
    }

    if (!$middleware)
    {
      $middleware = $this->getMiddleWareControl();
    }

    $result = $middleware->mwCanActivate(
      array(
        'actionUUID'  => getNewActionUUID('provisioning ' . time()),
        'iccid'       => $iccid
      )
    );

    if (!isset($result->data_array['available']))
    {
      $this->addToOutput('sim_ready_activate', FALSE);
      throw new \Exception('ERR_API_INTERNAL: MW did not return SIM availability');
    }

    dlog('', "mvne result data = %s", $result->data_array);

    if ($result->is_timeout())
    {
      $this->addToOutput('sim_ready_activate', FALSE);
      throw new \Exception("ERR_API_INTERNAL: middleware timeout");
    }

    if ($result->is_failure())
    {
      $this->addToOutput('sim_ready_activate', FALSE);
      throw new \Exception("ERR_API_INTERNAL: : the given ICCID cannot be activated");
    }

    return $result;
  }

  protected function getCustomerFromICCID($iccid)
  {
    return get_customer_from_iccid($iccid);
  }

}


<?php

namespace Ultra\Lib\Api\Partner;

require_once 'Ultra/Lib/Api/PartnerBase.php';

/**
 * Ultrainfo partner class
 * @project ultra.me
 */
class Ultrainfo extends \Ultra\Lib\Api\PartnerBase
{
  /**
   * ultrainfo__GetOutageInfo
   * return information about current outage status
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Commands%3A+UltraInfo#Commands:UltraInfo-ultrainfo__GetOutageInfo
   */
  public function ultrainfo__GetOutageInfo()
  {
    // init return falues (on the safe side in case of DB/network problems)
    $enabled = TRUE;
    $message = NULL;
    $comprehensive = TRUE;

    try
    {
      // get values from the Settings object
      $settings = new \Ultra\Lib\Util\Settings;
      $enabled = convert_value_to_boolean($settings->checkUltraSettings('ultra_me/outage/enabled'));
      $message = $settings->checkUltraSettings('ultra_me/outage/message');
      $comprehensive = convert_value_to_boolean($settings->checkUltraSettings('ultra_me/outage/comprehensive'));
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('enabled', $enabled);
    $this->addToOutput('message', $message);
    $this->addToOutput('comprehensive', $comprehensive);
    return $this->result;
  }

  /**
   * ultrainfo__InternationalCallingRates
   *
   * Provides the calling card rates for the specified product
   *
   * @param string $product_name
   * @return object Result
   */
  public function ultrainfo__InternationalCallingRates()
  {
    list ($product_name) = $this->getInputValues();

    $rates_list = array();

    try
    {
      teldata_change_db();

      $rates_data = htt_card_calling_rates_get_rates_data($product_name, TRUE);

      if ($rates_data && is_array($rates_data) && count($rates_data))
      {
        $rates_list = $rates_data;
      }
      else
        $this->errException('ERR_API_INTERNAL: data not found', 'ND0001');

      $this->addToOutput('rates_list', $rates_list);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Format the locations information.
   *
   * @param array $locations
   * @author bwalters@ultra.me
   */
  protected function formatLocations(array $locations)
  {
    if ( $locations && is_array($locations) && count($locations) )
    {
      // encode as UTF-8 to avoid json_encode errors
      foreach ($locations as &$location)
      {
        $location->RETAILER_NAME = utf8_encode($location->RETAILER_NAME);
        $location->ADDRESS1 = utf8_encode($location->ADDRESS1);
        $location->ADDRESS2 = utf8_encode($location->ADDRESS2);
      }
    }
    else
    {
      $warnings[] = "ERR_API_INTERNAL: data not found";
    }

    return $locations;
  }

  /**
   * ultrainfo__SMSCountryList
   *
   * Provides a list of all the international locations an Ultra user can SMS.
   *
   * @return object Result
   */
  public function ultrainfo__SMSCountryList()
  {
    $sms_list = array();

    try
    {
      teldata_change_db();

      $country_list = htt_country_sms_carriers_get_country_list();

      if ($country_list && is_array($country_list) && count($country_list))
      {
        $sms_list = $country_list;
      }
      else
      {
        $this->errException('ERR_API_INTERNAL: data not found', 'ND0001');
      }

      $this->addToOutput('sms_list', $sms_list);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Provides the coverage levels of Ultra Service in the specified Zipcode
   *
   * @param string $zipcode
   * @return object Result
   */
  public function ultrainfo__ZIPCoverageInfo()
  {
    list ($zipcode) = $this->getInputValues();

    $quality = 0;

    try
    {
      if (!verify_request_source())
      {
        $this->errException('ERR_API_ACCESS_DENIED: request source rejected', 'SE0004');
      }

      $redis = new \Ultra\Lib\Util\Redis;

      $quality_key = "coverage_info_quality/" . $zipcode;

      $quality = $redis->get($quality_key);

      if (!isset($quality) || $quality == '')
      {
        # Redis does not contain a value for the given zip code, we must query the DB

        teldata_change_db();

        $coverage_data = htt_coverage_info_get_coverage_data($zipcode);

        if ( $coverage_data && is_array($coverage_data) && count($coverage_data) )
        {
          $quality = $coverage_data[0]->quality;
        }
        else
        {
          $quality = 0;
        }

        # save the result in Redis.
        $redis->set($quality_key, $quality, 3*60*60*24);
      }

      $this->addToOutput('quality', $quality);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}


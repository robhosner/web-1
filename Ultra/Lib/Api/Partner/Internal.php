<?php

namespace Ultra\Lib\Api\Partner;

if (!getenv("UNIT_TESTING"))
{
  require_once 'Ultra/Lib/Api/Partner/InternalAmdocs.php';
  require_once 'Ultra/Lib/Util/Redis.php';
  require_once 'Ultra/Lib/Util/Redis/Transition.php';
  require_once 'Ultra/Lib/DB/Getter.php';
  require_once 'classes/PromoBroadcast.php';
  include_once 'lib/crone-common.php';
  include_once 'lib/util-common.php';

  include_once 'db/htt_customers_overlay_ultra.php';
  include_once 'db/htt_inventory_sim.php';
  require_once 'db/ultra_activation_log_override.php';
  require_once 'Ultra/Lib/DB/Celluphone/functions.php';
}

/**
 * Internal Partner classes
 *
 * Examples:
 * curl -i 'https://rgalli3-dev.uvnv.com/ultra_api.php?bath=rest&partner=internal&partner_tag=abc&version=2&command=internal__TrackOpenTransitions&format=&input1=1&input2=2' -u dougmeli:Flora
 * curl -i 'https://rgalli3-dev.uvnv.com/ultra_api.php?bath=rest&partner=internal&version=2&format=&command=internal__TestValidation' -u dougmeli:Flora -d 'test_integer=a'
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Internal extends \Ultra\Lib\Api\Partner\InternalAmdocs
{
  private $intake_data_file = '/var/tmp/Ultra/Intake/intake_database.json';

  /**
   * internal__TrackRedisTransition
   *
   * Transition in Redis.
   *
   * @return Result object
   */
  public function internal__TrackRedisTransition ()
  {
    list ( $transition_uuid ) = $this->getInputValues();

    $error_code = '';

    $this->addArrayToOutput(array(
      'customer_id' => '',
      'from_cos_id' => '',
      'from_plan_state' => '',
      'to_cos_id' => '',
      'to_plan_state' => '',
      'htt_environment' => '',
      'transition_label' => '',
      'priority' => '',
      'created' => ''
    ));

    try
    {
      $redis_transition = new \Ultra\Lib\Util\Redis\Transition();

      $transition_data = $redis_transition->getObjectAsArray( $transition_uuid );

      if ( ( ! $transition_data ) || ( ! is_array($transition_data) ) )
      {
        $error_code = 'ND0001';
        throw new \Exception("ERR_API_INTERNAL: data not found.");
      }

      $this->addArrayToOutput(array(
        'customer_id'     => $transition_data['customer_id'],
        'from_cos_id'     => $transition_data['from_cos_id'],
        'from_plan_state' => $transition_data['from_plan_state'],
        'to_cos_id'       => $transition_data['to_cos_id'],
        'to_plan_state'   => $transition_data['to_plan_state'],
        'htt_environment' => $transition_data['htt_environment'],
        'transition_label'=> $transition_data['transition_label'],
        'priority'        => $transition_data['priority'],
        'created'         => $transition_data['created']
      ));

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }


  /**
   * internal__TrackOpenTransitions
   *
   * Track Open Transitions in Redis.
   *
   * @return Result object
   */
  public function internal__TrackOpenTransitions ()
  {
    // comma-separated list of environments
    list ( $environments ) = $this->getInputValues();

    $error_code = '';

    $open_transitions = array();
    $transitions      = array();

    $redis = new \Ultra\Lib\Util\Redis;

    $redis_transition = new \Ultra\Lib\Util\Redis\Transition( $redis );

    $environments_list = explode(',',$environments);

    $priorities = range( 1 , 9 );

    // loop through environments
    foreach( $environments_list as $environment )
    {
      $environment = trim($environment);

      // loop through priorities
      foreach( $priorities as $priority )
      {
        // get transition sorted set (by time) identified by $environment and $priority
        $transition_list = $redis_transition->getListByEnvironmentAndPriority( $environment , $priority );

        if ( $transition_list && is_array($transition_list) && count($transition_list) )
        {
          dlog('',"environment = %s , priority = %s , transition_list = %s",$environment,$priority,$transition_list);

          if ( ! isset($transitions[ $priority ]) )
            $transitions[ $priority ] = array( $priority );

          $transitions[ $priority ] = array_merge( $transitions[ $priority ] , $transition_list );
        }
      }
    }

    foreach ( $transitions as $priority => $list )
    {
      $open_transitions[] = implode(",",$list);
    }

    dlog('',"open_transitions = %s",$open_transitions);

    $this->addToOutput('open_transitions',$open_transitions);

    $this->succeed ();

    return $this->result;
  }

  /**
   * Will return all intake data. If file does not exist, will create it with an empty array.
   * @return array
   */
  protected function getJSONIntakeData()
  {
    if (!file_exists($this->intake_data_file))
    {
      file_put_contents($this->intake_data_file, json_encode(array()));
    }

    return json_decode(file_get_contents($this->intake_data_file));
  }

  /**
   * Will store intake data, appending new data to the end of the array.
   * @param array @intake_data
   */
  protected function storeJSONIntakeData(array $intake_data)
  {
    $storable_object = array();

    foreach ($intake_data as $data)
    {
      $json_data = json_decode($data['value']);
      $field = str_replace(":", "", $data['name']);
      $value = ($json_data) ? $json_data : $data['value'];

      $storable_object[$field] = $value;
    }

    $intake_data = $this->getJSONIntakeData();
    $intake_data[] = $storable_object; // Append to array of intake data

    file_put_contents($this->intake_data_file, json_encode($intake_data));
  }
}


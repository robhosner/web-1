<?php

namespace Ultra\Lib\Api\Partner\Internal;

class LookUpDemoLine extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__LookUpDemoLine
   *
   * gets info on demo line
   *
   * @param  String  $dealer_code
   * @param  Integer $customer_id
   * @param  String  $msisdn
   * @return Result  object
   */
  
  public function internal__LookUpDemoLine()
  {
    list ($dealer_code, $customer_id, $msisdn) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $params = array();

      if     ( ! empty($dealer_code)) $params['dealer_code'] = $dealer_code;
      elseif ( ! empty($customer_id)) $params['customer_id'] = $customer_id;
      elseif ( ! empty($msisdn))      $params['msisdn']      = $msisdn;

      if (count($params) == 0)
        $this->errException('ERR_API_INTERNAL: One or more required parameters are missing', 'MP0001');

      $result = ultra_demo_line_get_info($params);

      if ( ! $result )
        $this->errException('ERR_API_INTERNAL: Demo line does not exist', 'DB0001');

      $this->addToOutput('demo_line_info', $result);

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

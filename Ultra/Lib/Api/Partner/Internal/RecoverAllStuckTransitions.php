<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class RecoverAllStuckTransitions extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__RecoverAllStuckTransitions
   *
   * Recover all transitions stuck for too much time.
   *
   * @return Result object
   */
  public function internal__RecoverAllStuckTransitions ()
  {
    $error_code = '';

    $this->addToOutput('transitions','');

    try
    {
      teldata_change_db();

      $stuck_transitions_data = get_stuck_transitions();

      if ( count($stuck_transitions_data) )
      {
        $stuck_transitions = array();

        foreach( $stuck_transitions_data as $transition_uuid => $data )
        {
          $error = reset_redis_transition( $transition_uuid );

          if ( $error )
            $this->addWarning( $error );
        }

        $this->addToOutput('transitions',$stuck_transitions);
      }

      transition_garbage_collector();

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}

?>

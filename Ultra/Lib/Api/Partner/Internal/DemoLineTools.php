<?php

namespace Ultra\Lib\Api\Partner\Internal;

class DemoLineTools extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__DemoLineTools
   *
   * Tools to edit demo line entries
   *
   * @param  Integer $demo_line_id
   * @param  Integer $customer_id
   * @param  String  $status
   * @param  Integer $activations_needed
   * @return Result object
   */
  
  public function internal__DemoLineTools()
  {
    list ($action, $demo_line_id, $customer_id, $status, $activations_needed) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.DEMO_LINE', NULL, NULL, array('DEMO_LINE_ID' => $demo_line_id), NULL, NULL, TRUE);
      if (! $demoLine = find_first($sql))
        $this->errException('ERR_API_INTERNAL: Demo line does not exist.', 'MP0001');

      $function = "\Ultra\Lib\DemoLine\\" . $action;
      if ( ! function_exists($function))
        $this->errException('ERR_API_INTERNAL: cannot execute action: ' . $action, 'IN0002');

      $params = array(
        'demo_line_id'       => $demo_line_id,
        'customer_id'        => $customer_id,
        'status'             => $status,
        'activations_needed' => $activations_needed
      );

      $result = $function($params);

      if ( ! $result->is_success())
      {
        $errors = $result->get_errors();
        $error  = count($errors) ? $errors[0] : 'Unexpected internal error';
        $this->errException('ERR_API_INTERNAL: ' . $error, 'IN0002');
      }

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

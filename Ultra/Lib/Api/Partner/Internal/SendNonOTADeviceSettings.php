<?php
namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Internal;
use Ultra\Messaging\Messenger;

class SendNonOTADeviceSettings extends Internal
{
  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  public $message = <<<'EOT'
1.Select "Settings" > "Cellular" > "Cellular Data" > "Cellular Data Option" > "APN"\n
2.Enter "alpha" > MMS \n
3.Select "APN"\n
4. Enter "wholesale"\n
5.Select "MMSC" \n
6.Enter "http://wholesale.mmsmvno.com/mms/wapenc"\n
7.Select "MMS Max Message Size"\n
8.Enter "1048576"\n
9.Select "MMS UA Prof URL" \n
10.Enter "http://www.apple.com/mms/uaprof.rdf"\n
11.Restart your iPhone
EOT;

  /**
   * SendApnSettings constructor.
   * @param Messenger $messenger
   * @param CustomerRepository $customerRepository
   */
  public function __construct(Messenger $messenger, CustomerRepository $customerRepository)
  {
    $this->messenger = $messenger;
    $this->customerRepository = $customerRepository;
  }

  public function internal__SendNonOTADeviceSettings()
  {
    list($msisdn) = $this->getInputValues();

    try {
      $customer = $this->customerRepository->getCustomerFromMsisdn($msisdn, ['c.CUSTOMER_ID', 'u.BRAND_ID', 'u.preferred_language']);

      if (!$customer) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer does not exist', 'VV0031');
      }

      $message = $this->messenger->smsByLanguage(
        ['message_type' => 'setup_apn_ios'],
        empty($customer->preferred_language) ? 'EN' : $customer->preferred_language,
        $customer->BRAND_ID
      );

      $smsResult = $this->messenger->mvneBypassListSynchronousSendSMS($message, $msisdn, getNewActionUUID(__FUNCTION__ . ' ' . time()));

      if ($smsResult->is_failure()) {
        return $this->errException('ERR_API_INTERNAL: SMS delivery error.', 'SM0002');
      }

      $smsResult = $this->messenger->mvneBypassListAsynchronousSendSMS($this->message, $msisdn, getNewActionUUID(__FUNCTION__ . ' ' . time()));

      if ($smsResult->is_failure()) {
        return $this->errException('ERR_API_INTERNAL: SMS delivery error.', 'SM0002');
      }

      $this->succeed();
    }
    catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
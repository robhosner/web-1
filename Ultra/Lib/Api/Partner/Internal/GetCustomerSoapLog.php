<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetCustomerSoapLog extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetCustomerSoapLog
   *
   * query ULTRA_ACC..SOAP_LOG and return result record set
   *
   * @return Result object
   */
  public function internal__GetCustomerSoapLog()
  {
    // collect input
    list ($date_from, $date_to, $iccid, $msisdn, $type_id, $tag, $soap_log_id) = $this->getInputValues();

    // initialize output
    $this->addToOutput('records', array());

    try
    {
      // fail if both ICCID and MSISDN are not given
      if (! $iccid && ! $msisdn)
        $this->errException('ERR_API_INTERNAL: missing ICCID or MSISDN parameter', 'NS0001');

      // fail is dates are more than MAX_SOAP_LOG_RANGE apart
      if (abs(strtotime($date_to) - strtotime($date_from)) > self::MAX_SOAP_LOG_RANGE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: date range is too large', 'VV0033');

      // convert 'DD-MM-YYYY' to 'Mon DD YYYY'
      $date_from = date_to_datetime($date_from);
      $date_to = date_to_datetime($date_to, TRUE);
      if (empty($date_from) || empty($date_to))
        $this->errException('ERR_API_INVALID_ARGUMENTS: failed to convert input dates', 'VV0033');

      // prepare query parameters
      $params = array(
        'sql_limit' => self::MAX_SOAP_LOG_RECORDS,
        'soap_date' => array($date_from, $date_to));
      if ($iccid)
        $params['iccid'] = $iccid;
      if ($msisdn)
        $params['msisdn'] = $msisdn;
      if ($type_id)
        $params['type_id'] = $type_id;
      $params['tag'] = $tag;
      if ($soap_log_id)
        $params['soap_log_id'] = $soap_log_id;

      // get records
      $connection = \Ultra\Lib\DB\ultra_acc_connect();
      if (! $connection)
        $this->errException('ERR_API_INTERNAL: failed to connect to database', 'DB0001');
      $result = get_soap_log($params);

      // AMDOCS-392: post-process dates
      $schema = \Ultra\Lib\DB\getTableSchema('SOAP_LOG', 'datetime');
      foreach ($result as &$row)
        foreach ($schema as $column => $type)
          $row->$column = date_sql_to_usa($row->$column, TRUE);
      
      // PROD-46: format XML nicely
      foreach ($result as &$row)
      {
        $dom = new \DOMDocument;
        $dom->loadXML($row->xml);
        $dom->preserveWhiteSpace = FALSE;
        $dom->formatOutput = TRUE;
        $row->xml = str_replace('<?xml version="1.0"?>', NULL, $dom->saveXML()); // remove XML tag added by DOMDocument
      }

      // add to output
      if ($result && is_array($result) && count($result))
        $this->addToOutput('records', $result);

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage());
    }

    return $this->result;
  } 
}

?>

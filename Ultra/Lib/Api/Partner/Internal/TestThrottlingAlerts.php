<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/MiddleWare/ACC/Notification.php';

class TestThrottlingAlerts extends \Ultra\Lib\Api\Partner\Internal
{
  public function internal__TestThrottlingAlerts()
  {
    list ($msisdn, $value, $event, $counterName) = $this->getInputValues();

    // $value  = '102400';
    // $msisdn = '9495278324';
    // $event  = 80;
    // $counterName = 'WPRBLK33S';

    $serviceGrade = 'SG801';
    $messageType  = 'ASCII'; // <--

    try
    {
      $timeStamp    = date('c');
      $endpoint     = \Ultra\UltraConfig\getDevMiddlewareEndpoint();

      $data = array(
        'UserData'    => array("senderId" => "MVNEACC", "timeStamp" => $timeStamp),
        'MSISDN'      => $msisdn,
        'Value'       => $value,
        'Event'       => $event,
        'CounterName' => $counterName
      );

      // get correct WSDL to use
      $wsdl = \Ultra\UltraConfig\acc_notification_wsdl();
      $wsdl = ($wsdl) ? 'runners/amdocs/' . $wsdl : NULL;

      $result = \Ultra\Lib\MiddleWare\ACC\Notification::divertNotification(
        $wsdl,
        $endpoint,
        'ThrottlingAlert',
        $data
      );

      if ($result->is_success())
        $this->succeed();
      else
        $this->errException('ERR_API_INTERNAL: error sending notification to MW endpoint', 'MW0001');
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
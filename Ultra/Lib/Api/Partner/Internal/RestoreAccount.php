<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class RestoreAccount extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__RestoreAccount
   *
   * Reactivate a suspended Ultra customer without involving a state transition.
   *
   * @param string customer_id
   * @param string reason
   * @return Result object
   */
  public function internal__RestoreAccount()
  {
    list ($customer_id, $reason) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id);

      if ($customer)
      {
        if (in_array($customer->BRAND_ID, [3]))
          $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

        if ($customer->plan_state == STATE_SUSPENDED)
        {
          $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

          $result = $mwControl->mwRestoreSubscriber(
            array(
              'actionUUID'  => $this->getRequestId(),
              'iccid'       => $customer->CURRENT_ICCID_FULL,
              'msisdn'      => $customer->current_mobile_number,
              'customer_id' => $customer->customer_id
            )
          );

          if ($errors = $result->get_errors())
            $this->errException($errors[0], 'MW0001');

          if (isset($result->data_array['errors']) && count($result->data_array['errors']))
            $this->errException($result->data_array['errors'][0], 'MW0001');

          $params = array(
            'customer_id' => $customer->customer_id,
            'plan_state'  => STATE_ACTIVE
          );

          if (!is_mssql_successful(logged_mssql_query(htt_customers_overlay_ultra_update_query($params))))
          {
            $this->errException('ERR_API_INTERNAL: An unexpected database error occurred while writing to HTT_CUSTOMERS_OVERLAY_ULTRA', 'DB0001');
          }
        }
        else
        {
          $this->errException('ERR_API_INTERNAL: Invalid customer state for this command', 'IN0001');
        }
      }
      else
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: Ultra customer does not exist', 'VV0031');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>

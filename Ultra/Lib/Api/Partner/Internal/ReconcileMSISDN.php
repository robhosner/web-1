<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class ReconcileMSISDN extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__ReconcileMSISDN
   *
   * Fix the MSISDN for a customer.
   *
   * @param string customer_id
   * @return Result object
   */
  public function internal__ReconcileMSISDN()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id);

      if ($customer)
      {
        if ($customer->plan_state == STATE_ACTIVE)
        {
          $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

          $result = $mwControl->mwQuerySubscriber(
            array(
              'actionUUID' => $this->getRequestId(),
              'iccid' => $customer->CURRENT_ICCID_FULL
            )
          );

          if ($errors = $result->get_errors())
            $this->errException($errors[0], 'MW0001');

          if ( !empty($result->data_array['body']) && ($result->data_array['body']->ResultCode != 100) )
            $this->errException($result->data_array['body']->ResultMsg, 'MW0001');

          if ( !empty($result->data_array['body']) && ($result->data_array['body']->SubscriberStatus != 'Active'))
            $this->errException('ERR_API_INTERNAL: SubscriberStatus is not Active.', 'MW0001');

          if ( !empty($result->data_array['body']) && (empty($result->data_array['body']->MSISDN)) )
            $this->errException('ERR_API_INTERNAL: MSISDN is empty.', 'MW0001');

          if ( !empty($result->data_array['body']) && ( strlen($result->data_array['body']->MSISDN) != 10) )
            $this->errException('ERR_API_INTERNAL: MSISDN is formatted incorrectly.', 'MW0001');

          if ( !empty($result->data_array['errors']) && count( $result->data_array['errors'] ) )
            $this->errException($result->data_array['errors'][0], 'MW0001');

          if ($customer->current_mobile_number != $result->data_array['body']->MSISDN)
          {
            $params = array(
              'current_mobile_number' => $result->data_array['body']->MSISDN,
              'customer_id' => $customer_id
            );

            if (!is_mssql_successful(logged_mssql_query(htt_customers_overlay_ultra_update_query($params))))
            {
              $this->errException('ERR_API_INTERNAL: An unexpected database error occurred while writing to HTT_CUSTOMERS_OVERLAY_ULTRA', 'DB0001');
            }

            if (!add_to_htt_ultra_msisdn($customer_id, $result->data_array['body']->MSISDN, 1, $this->getRequestId(), 2))
            {
              $this->errException('ERR_API_INTERNAL: An unexpected database error occurred while writing to HTT_ULTRA_MSISDN', 'DB0001');
            } 
          }
          else
          {
            $this->addWarning('Nothing to do');
          }
        }
        else
        {
          $this->errException('ERR_API_INTERNAL: Invalid customer state for this command', 'IN0001');
        }
      }
      else
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: Ultra customer does not exist', 'VV0031');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>

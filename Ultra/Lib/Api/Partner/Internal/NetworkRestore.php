<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class NetworkRestore extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__NetworkRestore
   *
   * Restore a suspended customer on the network using mwRestoreSubscriber
   *
   * @return Result object
   */
  public function internal__NetworkRestore ()
  {
    list($iccid, $msisdn, $customer_id) = $this->getInputValues();
    $error_code = NULL;

    try
    {
      dlog('', 'authenticated user: %s', empty($_SERVER['PHP_AUTH_USER']) ? 'NULL' : $_SERVER['PHP_AUTH_USER']);

      // check parameters and compose SQL clause
      if ( ! empty($customer_id))
        $clause = array('CUSTOMER_ID' => $customer_id);
      elseif ( ! empty($msisdn))
        $clause = array('CURRENT_MOBILE_NUMBER' => normalize_msisdn($msisdn));
      elseif ( ! empty($iccid))
        $clause = array('CURRENT_ICCID_FULL' => luhnenize($iccid));
      else
        $this->errException('ERR_API_INVALID_ARGUMENTS: at least one parameter is required', 'MP0001');

      // get customer info
      teldata_change_db();
      $sql = \Ultra\Lib\DB\makeSelectQuery('HTT_CUSTOMERS_OVERLAY_ULTRA', 2, array('CUSTOMER_ID', 'CURRENT_MOBILE_NUMBER', 'CURRENT_ICCID_FULL', 'PLAN_STATE', 'BRAND_ID'), $clause, NULL, NULL, TRUE);
      $customers = mssql_fetch_all_objects(logged_mssql_query($sql));
      if (count($customers) != 1)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      $customer = $customers[0];

      if (in_array($customer->BRAND_ID, [3]))
        $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

      // MVNO-2477: cannot restore on MVNE if not already active in Ultra
      if ($customer->PLAN_STATE != STATE_ACTIVE)
        $this->errException('ERR_API_INTERNAL: subscriber is not Active', 'IN0001');

      // fill in missing values
      $iccid = $customer->CURRENT_ICCID_FULL;
      $msisdn = $customer->CURRENT_MOBILE_NUMBER;
      $customer_id = $customer->CUSTOMER_ID;
      dlog('', "ICCID: $iccid, MSISDN: $msisdn, customer_id: $customer_id");

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $result = $mwControl->mwRestoreSubscriber(
        array(
          'actionUUID'  => $this->getRequestId(),
          'msisdn'      => $msisdn,
          'iccid'       => $iccid,
          'customer_id' => $customer_id));

      // did mwRestoreSubscriber fail?
      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();
        $error_code = 'MW0001';
        throw new \Exception("ERR_API_INTERNAL: ".$errors[0]);
      }

      // did RestoreSubscriber fail?
      if ( ! $result->data_array['success'] )
      {
        $error_code = 'MW0001';
        throw new \Exception("ERR_API_INTERNAL: ".$result->data_array['errors'][0]);
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}

?>

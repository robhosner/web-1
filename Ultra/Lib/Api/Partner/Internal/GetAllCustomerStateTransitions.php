<?php

namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Lib\Api\Partner\Internal;

/**
 * Class GetAllCustomerStateTransitions
 * @package Ultra\Lib\Api\Partner\Internal
 */
class GetAllCustomerStateTransitions extends Internal
{
  private $state_transitions = [];
  private $customer;
  private $account;
  private $customer_id;

  /**
   * internal__GetAllCustomerStateTransitions
   *
   * Get all state transitions for a given customer
   *
   * @param integer $customer_id
   * @return \Result
   */
  public function internal__GetAllCustomerStateTransitions()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      $this->init($customer_id);

      if ($this->customer && $this->account)
      {
        $result = get_all_customer_state_transitions($this->customer_id, TRUE);
        $this->state_transitions = $this->internalParseStateTransitions($result['state_transitions']);

        if (count($result['errors']) == 0)
        {
          $this->addOutputData();
        }

        $this->addErrors($result['errors']);
        $this->addWarnings($result['warnings']);
      }
      else
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found', 'ND0001');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Initialize class.
   *
   * @param $customer_id
   */
  private function init($customer_id)
  {
    $this->customer_id = $customer_id;

    $this->customer = get_ultra_customer_from_customer_id(
      $this->customer_id,
      ['current_mobile_number', 'CURRENT_ICCID_FULL', 'stored_value']
    );

    $this->account = get_account_from_customer_id(
      $this->customer_id,
      ['BALANCE', 'PACKAGED_BALANCE1', 'PACKAGED_BALANCE2', 'PACKAGED_BALANCE3']
    );
  }

  /**
   * Add api output.
   */
  private function addOutputData()
  {
    $this->addAccountAliasOutput();
    $this->addDestinationOriginMapOutput();
    $this->addSomeCustomerDataOutput();
  }

  /**
   * Add account alias output.
   */
  private function addAccountAliasOutput()
  {
    $account_aliases_select_query = account_aliases_select_query(['customer_id' => $this->customer_id]);
    $account_aliases_data = mssql_fetch_all_objects(logged_mssql_query($account_aliases_select_query));
    $accountAlias = '';

    if ($account_aliases_data && is_array($account_aliases_data) && count($account_aliases_data))
    {
      $accountAlias = $account_aliases_data[0]->ALIAS;
    }

    $this->addToOutput('account_alias', $accountAlias);
  }

  /**
   * Add destination origin map output.
   */
  private function addDestinationOriginMapOutput()
  {
    $destination_origin_map_select_query = destination_origin_map_select_query(['destination' => '1' . $this->customer->current_mobile_number]);
    $destination_origin_map_data = mssql_fetch_all_objects(logged_mssql_query($destination_origin_map_select_query));
    $originMapAccount = '';
    $originMapOrigin = '';

    if ($destination_origin_map_data && is_array($destination_origin_map_data) && count($destination_origin_map_data))
    {
      $originMapAccount = $destination_origin_map_data[0]->ACCOUNT;
      $originMapOrigin = $destination_origin_map_data[0]->ORIGIN;
    }

    $this->addToOutput('destination_origin_map_account', $originMapAccount);
    $this->addToOutput('destination_origin_map_origin', $originMapOrigin);
  }

  /**
   * Add some customer data output.
   */
  private function addSomeCustomerDataOutput()
  {
    $this->addToOutput('state_transitions', $this->state_transitions);
    $this->addToOutput('balance', $this->account->BALANCE);
    $this->addToOutput('packaged_balance1', $this->account->PACKAGED_BALANCE1);
    $this->addToOutput('packaged_balance2', $this->account->PACKAGED_BALANCE2);
    $this->addToOutput('packaged_balance3', $this->account->PACKAGED_BALANCE3);
    $this->addToOutput('customer_stored_balance', $this->customer->stored_value);
    $this->addToOutput('msisdn', $this->customer->current_mobile_number);
    $this->addToOutput('iccid', $this->customer->CURRENT_ICCID_FULL);
  }

  /**
   * Parses out PDO Database result set of state transitions into a PHP array.
   *
   * @param object $state_transitions_data
   * @return array $state_transitions
   */
  private function internalParseStateTransitions($state_transitions_data)
  {
    $state_transitions = [];

    if ($state_transitions_data && is_array($state_transitions_data) && count($state_transitions_data))
    {
      foreach($state_transitions_data as $n => $data)
      {
        $state_transitions[] = $data->TRANSITION_UUID;
        $state_transitions[] = $data->STATUS;
        $state_transitions[] = $data->FROM_PLAN_STATE;
        $state_transitions[] = $data->TO_PLAN_STATE;
        $state_transitions[] = $data->TRANSITION_LABEL;
        // PLace the Created and Closed Dates in Local Time for ease by Customer Service Troubleshooter -- Requested by Kelvin
        $state_transitions[] = date('m/d/y h:i:s A', strtotime($data->CREATED_PST));
        $state_transitions[] = date('m/d/y h:i:s A', strtotime($data->CLOSED_PST));

        $state_transition_errors = '';

        if (isset($data->ACTION_RESULT ) && ( $data->ACTION_RESULT != ''))
        {
          $action_result = json_decode($data->ACTION_RESULT);

          if ($action_result && is_array($action_result) && count($action_result) && $action_result[0]->errors)
          {
            $state_transition_errors = implode(" ; ", $action_result[0]->errors );
          }
        }

        $state_transitions[] = $state_transition_errors;
      }
    }

    return $state_transitions;
  }
}
<?php
namespace Ultra\Lib\Api\Partner\Internal;

use PortInQueue;
use Ultra\Lib\Api\Partner\Internal;
use Ultra\Lib\MiddleWare\Adapter\Control;

/**
 * Class QueryPortIn
 * @package Ultra\Lib\Api\Partner\Internal
 */
class QueryPortIn extends Internal
{
  /**
   * @var
   */
  private $msisdn;
  /**
   * @var PortInQueue
   */
  private $portInQueue;

  /**
   * @var Control
   */
  private $control;

  /**
   * @var array
   */
  private $outputArray = [
    "status" => '',
    "r_code" => '',
    "provision_status_date" => '',
    "query_message"         => '',
    "query_status"          => '',
    "carrier_name"          => '',
    "port_query_status"     => '',
    "port_due_time"         => '',
    "old_service_provider"  => '',
  ];

  /**
   * internal__QueryPortIn
   *
   * Invokes QueryPortIn on the network.
   *
   * @param int $customer_id
   * @param string $msisdn
   *
   * @return \Result
   */
  public function internal__QueryPortIn()
  {
    try
    {
      // initialize
      list ($customer_id, $msisdn) = $this->getInputValues();

      $this->init($msisdn);

      if (!$customer_id && !$msisdn)
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer_id or msisdn required.', 'MP0001');
      }

      if (!empty($customer_id))
      {
        $this->msisdn = $this->getCustomerMsisdnById($customer_id);
      }

      if (!empty($this->msisdn))
      {
        $customer = $this->getCustomerByMsisdn(normalize_msisdn_10($this->msisdn));
        $sim = get_htt_inventory_sim_from_iccid($customer->CURRENT_ICCID_FULL);

        if ($sim && ($customer->BRAND_ID != $sim->BRAND_ID))
        {
          $this->outputArray['query_status'] = ($customer->plan_state != STATE_ACTIVE) ? 'ERROR' : 'COMPLETE';
          $this->outputArray['status'] = $this->outputArray['query_status'];
          $this->outputArray['provision_status_date'] = time();
          $this->outputArray['carrier_name'] = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);
          $this->outputArray['old_service_provider'] = $this->outputArray['carrier_name'];
        }
        else
        {
          $this->checkPortInQueueLoad();
          $this->checkPortInStatus();
        }
      }

      $this->addArrayToOutput($this->outputArray);
      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Get the customer's msisdn.
   *
   * @param $customer_id
   * @throws \Exception
   *
   * @return mixed
   */
  private function getCustomerMsisdnById($customer_id)
  {
    $customers = mssql_fetch_all_objects(
      logged_mssql_query(
        htt_customers_overlay_ultra_select_query(
          [
            'select_fields' => ["current_mobile_number"],
            'customer_id'   => $customer_id
          ]
        )
      )
    );

    if (!count($customers))
    {
      $this->errException("ERR_API_INTERNAL: customer_id $customer_id not found.", 'ND0001');
    }

    if (!$customers[0]->current_mobile_number)
    {
      $this->errException("ERR_API_INTERNAL: customer_id $customer_id has no phone number.", 'ND0001');
    }

    return $customers[0]->current_mobile_number;
  }

  /**
   * Get a customer by msisdn
   *
   * @param $msisdn
   * @throws \Exception
   *
   * @return mixed
   */
  private function getCustomerByMsisdn($msisdn)
  {
    $customers = get_ultra_customers_from_msisdn($msisdn, [
      'plan_state', 'CURRENT_ICCID_FULL', 'BRAND_ID'
    ]);

    if (!count($customers))
    {
      $this->errException("ERR_API_INTERNAL: no customer found with msisdn $msisdn.", 'VV0031');
    }

    return $customers[0];
  }

  /**
   * Check the port-in queue load.
   *
   * @throws \Exception
   */
  private function checkPortInQueueLoad()
  {
    $result = $this->portInQueue->loadByMsisdn($this->msisdn);

    if ($result->is_failure() || !property_exists($this->portInQueue, 'msisdn') || !$this->portInQueue->msisdn)
    {
      $this->errException("ERR_API_INTERNAL: No data found in PORTIN_QUEUE for $this->msisdn.", 'ND0001');
    }

    if (property_exists($this->portInQueue, 'provstatus_error_msg'))
    {
      $this->outputArray['port_query_status'] = $this->portInQueue->provstatus_error_msg;
    }

    if (property_exists($this->portInQueue, 'carrier_name'))
    {
      $this->outputArray['carrier_name'] = $this->portInQueue->carrier_name;
    }
  }

  /**
   * Check the port-in status.
   *
   * @throws \Exception
   */
  private function checkPortInStatus()
  {
    $mwQueryStatus = $this->control->mwQueryStatus(
      [
        'actionUUID' => getNewActionUUID('internal ' . time()),
        'msisdn'     => $this->msisdn
      ]
    );

    $this->addWarnings($mwQueryStatus->get_warnings());

    if ($mwQueryStatus->is_success())
    {
      $errors = $mwQueryStatus->data_array['errors'];

      if (isset($errors) && is_array($errors) && count($errors))
      {
        $this->addErrors($errors);
        $this->errException("ERR_API_INTERNAL: " . $errors[0], 'ND0001');
      }
      elseif(isset($mwQueryStatus->data_array['body']) && is_object($mwQueryStatus->data_array['body']))
      {
        // check PORTIN_QUEUE
        $loadByMsisdnResult = $this->portInQueue->loadByMsisdn($this->msisdn);

        if ($loadByMsisdnResult->is_success())
        {
          $error_code = (property_exists($this->portInQueue, 'provstatus_error_code')) ? $this->portInQueue->provstatus_error_code : '';
          $error_msg  = (property_exists($this->portInQueue, 'provstatus_error_msg')) ? $this->portInQueue->provstatus_error_msg : '';

          $this->outputArray['port_query_status'] = ($error_code && $error_msg) ? $error_code . '-' . $error_msg : '' ;
          $this->outputArray['status'] = $this->portInQueue->provstatus_description;
          $this->outputArray['query_status'] = (property_exists($this->portInQueue, 'querystatus_portstatus')) ? $this->portInQueue->querystatus_portstatus : '';
          $this->outputArray['query_message'] = (property_exists($this->portInQueue, 'querystatus_errormsg')) ? $this->portInQueue->querystatus_errormsg : '';
          $this->outputArray['provision_status_date'] = $this->portInQueue->provstatus_epoch;
          $this->outputArray['port_due_time'] = (isset($this->portInQueue->due_date_time)) ? $this->portInQueue->due_date_time : null;
          $this->outputArray['old_service_provider'] = $this->portInQueue->old_service_provider;
        }
        else
        {
          $errors = $loadByMsisdnResult->get_errors();

          if (count($errors))
          {
            $this->addErrors($errors);
            $this->errException("ERR_API_INTERNAL: " . $errors[0], 'ND0001');
          }
        }
      }
    }
    else
    {
      $errors = $mwQueryStatus->get_errors();

      if (count($errors)) {
        $this->addErrors($errors);
        $this->errException("ERR_API_INTERNAL: " . $errors[0], 'MW0001');
      }
    }
  }

  /**
   * Set Class dependencies.
   *
   * @param $msisdn
   */
  public function init($msisdn)
  {
    $this->msisdn = $msisdn;
    $this->setPortInQueue(new PortInQueue());
    $this->setMwControl(new Control());
  }

  /**
   * Set PortInQueue dependency.
   *
   * @param PortInQueue $portInQueue
   */
  private function setPortInQueue(PortInQueue $portInQueue)
  {
    $this->portInQueue = $portInQueue;
  }

  /**
   * Set Control dependency.
   *
   * @param Control $control
   */
  private function setMwControl(Control $control)
  {
    $this->control = $control;
  }
}
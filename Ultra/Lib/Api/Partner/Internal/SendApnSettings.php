<?php
namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Internal;
use Ultra\Lib\Services\UserDeviceAPI;
use Ultra\Messaging\Messenger;

class SendApnSettings extends Internal
{
  /**
   * @var UserDeviceAPI
   */
  private $userDeviceAPI;

  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * SendApnSettings constructor.
   * @param UserDeviceAPI $userDeviceAPI
   * @param Messenger $messenger
   * @param CustomerRepository $customerRepository
   */
  public function __construct(UserDeviceAPI $userDeviceAPI, Messenger $messenger, CustomerRepository $customerRepository)
  {
    $this->userDeviceAPI = $userDeviceAPI;
    $this->messenger = $messenger;
    $this->customerRepository = $customerRepository;
  }

  public function internal__SendApnSettings()
  {
    list($msisdn) = $this->getInputValues();

    try {
      $customer = $this->customerRepository->getCustomerFromMsisdn($msisdn, ['c.CUSTOMER_ID']);

      if (!$customer) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer does not exist', 'VV0031');
      }

      $result = $this->userDeviceAPI->sendApnSettings($msisdn);

      if (!$result->is_success()) {
        $message = "Unable to send apn settings to $msisdn";
        $this->addToOutput('message', $message);
        return $this->errException($message, "IN0002", $message);
      }

      $this->succeed();
    }
    catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

<?php
namespace Ultra\Lib\Api\Partner\Internal;

use Exception;
use Ultra\Configuration\Interfaces\ConfigurationRepository;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Internal;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Utilities\Common;

/**
 * Class CancelCustomersWithinTestRange
 * @package Ultra\Lib\Api\Partner\Internal
 */
class CancelCustomersWithinTestRange extends Internal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var array
   */
  private $simRanges = [];

  /**
   * @var array|mixed
   */
  private $doNotDeleteSims = [];

  /**
   * @var array
   */
  private $cancellationReasons = [
    'Activation',
    'Port-In',
    'Intra-port',
    'SIM Swap',
    'Bolt-On Purchase',
    'Plan Change',
    'Monthly Renewal',
    'International Calling',
    'Data Use',
    'MSISDN Change',
    'Port-Out',
  ];
  
  /**
   * @var array
   */
  private $customersList;

  /**
   * @var int
   */
  private $simRangeArrayCount;

  /**
   * @var array
   */
  private $msisdnList = [];

  /**
   * @var array
   */
  private $iccidList = [];

  /**
   * @var array
   */
  private $notIccidOrMsisdn = [];

  /**
   * @var array
   */
  private $iccidsOutOfRange = [];

  /**
   * @var array
   */
  private $cancelledSims = [];

  /**
   * @var array
   */
  private $failedCancelledSims = [];

  /**
   * @var array
   */
  private $customersFromMsisdns = [];

  /**
   * @var array
   */
  private $customersFromIccids = [];
  
  /**
   * @var ConfigurationRepository
   */
  private $configuration;

  /**
   * @var array
   */
  private $customersWithNoCustomerIds = [];

  /**
   * @var Control
   */
  private $control;

  /**
   * @var int
   */
  private $simWhiteListArrayCount;

  /**
   * @var Common
   */
  private $utilities;

  /**
   * CancelCustomersWithinTestRange constructor.
   *
   * @param CustomerRepository $customerRepository
   * @param ConfigurationRepository $configurationRepository
   * @param Control $control
   * @param Common $utilities
   */
  public function __construct(
    CustomerRepository $customerRepository,
    ConfigurationRepository $configurationRepository,
    Control $control,
    Common $utilities
  )
  {
    $this->customerRepository = $customerRepository;
    $this->configuration = $configurationRepository;
    $this->simRanges = $this->loadSimRanges();
    $this->doNotDeleteSims = $this->loadWhiteListedSims();
    $this->simRangeArrayCount = count($this->simRanges);
    $this->simWhiteListArrayCount = count($this->doNotDeleteSims);
    $this->control = $control;
    $this->utilities = $utilities;
  }

  /**
   * @return NULL
   */
  public function internal__CancelCustomersWithinTestRange()
  {
    list($customersList) = $this->getInputValues();

    $this->customersList = $this->formatCustomerList($customersList);
    
    try
    {
      $this->setUpSortedLists($this->customersList);

      for ($i = 0; $i < count($this->iccidList); $i++)
      {
        if (!$this->checkIfIccidIsWithinTestRange($this->iccidList[$i]))
        {
          $this->iccidsOutOfRange[] = $this->iccidList[$i];
        }
      }

      $this->throwExceptionIfIccidsAreOutOfRange($this->iccidsOutOfRange);
      
      if (!empty($this->msisdnList))
      {
        $this->customersFromMsisdns = $this->customerRepository->getCustomersFromMsisdns(
          $this->msisdnList,
          ['customer_id', 'cos_id', 'current_iccid', 'plan_state', 'current_mobile_number']
        );

        if (!$this->customersFromMsisdns)
        {
          $this->customersFromMsisdns = [];
        }

        $this->customersWithNoCustomerIds = $this->getCustomersWithNoCustomerId($this->customersFromMsisdns, $this->msisdnList, false);

        foreach ($this->customersFromMsisdns as $customer)
        {
          if (!$this->checkIfIccidIsWithinTestRange($customer->current_iccid))
          {
            $this->iccidsOutOfRange[] = $customer->current_iccid;
          }
        }

        $this->throwExceptionIfIccidsAreOutOfRange($this->iccidsOutOfRange);
      }

      if (!empty($this->iccidList))
      {
        $this->customersFromIccids = $this->customerRepository->getCustomersFromIccids(
          $this->iccidList,
          ['customer_id', 'cos_id', 'current_iccid', 'plan_state', 'current_mobile_number']
        );

        if (!$this->customersFromIccids)
        {
          $this->customersFromIccids = [];
        }

        $this->customersWithNoCustomerIds = $this->getCustomersWithNoCustomerId($this->customersFromIccids, $this->iccidList, true);
      }
      
      $this->cancelledSims[] = $this->cancelCustomers(array_merge($this->customersFromMsisdns, $this->customersFromIccids), $this->customersWithNoCustomerIds);
      
      $this->succeed();
    }
    catch(Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Format input into an array
   * 
   * @param $customersList
   * @return array
   */
  private function formatCustomerList($customersList)
  {
    $customersList = explode(",", $customersList);
    
    return array_map(function($customersList) {
      return explode(":", $customersList);
    }, $customersList);
  }

  /**
   * Check to see if the provided iccid is within the defined test range
   * 
   * @param $iccid
   * @return bool
   */
  private function checkIfIccidIsWithinTestRange($iccid)
  {
    $isInTestRange = false;
    
    for ($i = 0; $i < $this->simRangeArrayCount; $i++)
    {
      if ($iccid >= $this->simRanges[$i][0] && $iccid <= $this->simRanges[$i][1])
      {
        $isInTestRange = true;
        break;
      }
    }

    for ($i = 0; $i < $this->simWhiteListArrayCount; $i++)
    {
      if ($iccid >= $this->doNotDeleteSims[$i][0] && $iccid <= $this->doNotDeleteSims[$i][1])
      {
        $isInTestRange = false;
        break;
      }
    }

    return $isInTestRange;
  }

  /**
   * Cancel customers
   *
   * @param array $customerList
   * @param array $customersNotInDbList
   */
  private function cancelCustomers(array $customerList, array $customersNotInDbList)
  {
    foreach ($customerList as $customer)
    {
      $result = $this->customerRepository->cancelCustomer($customer, $customer->customer_id);
      
      if ($result['success'])
      {
        $this->cancelledSims[] = $customer->current_iccid;

        $check = $this->customerRepository->addCancellationReason(
          [
            'customer_id' => $customer->customer_id,
            'reason' => $this->getCancellationReason($customer),
            'msisdn' => $customer->current_mobile_number,
            'iccid' => $this->utilities->luhnenize($customer->current_iccid),
            'cos_id' => $customer->cos_id,
            'type' => 'TEST',
            'status' => $customer->plan_state,
          ]
        );

        if (!$check)
        {
          dlog('', "Cannot insert into HTT_CANCELLATION_REASONS");
        }
      }
      else
      {
        $this->failedCancelledSims[] = $customer->current_iccid;
      }
    }

    foreach ($customersNotInDbList as $customer)
    {
      $result = $this->customerRepository->ensureCancelCustomer($customer->msisdn, $customer->iccid, null, null, -1);

      if (!$result->is_failure())
      {
        $this->cancelledSims[] = $customer->iccid;

        dlog('', "Cancelled Mint test sim $customer->iccid.");
      }
      else
      {
        $this->failedCancelledSims[] = $customer->iccid;
      }
    }

    $this->addToOutput('cancelled_test_sims', $this->cancelledSims);
    $this->addToOutput('failed_cancelled_test_sims', $this->failedCancelledSims);
  }

  /**
   * Set up the sorted lists
   * 
   * @param $customersList
   * @return NULL
   * @throws Exception
   */
  private function setUpSortedLists($customersList)
  {
    foreach ($customersList as $customer)
    {
      $msisdnOrIccidLength = strlen($customer[0]);

      if (!ctype_digit($customer[0]))
      {
        $this->notIccidOrMsisdn[] = $customer[0];
      }
      else if ($msisdnOrIccidLength >= 10 && $msisdnOrIccidLength <= 11)
      {
        $this->msisdnList[] = $customer[0];
      }
      else if ($msisdnOrIccidLength >= 18 && $msisdnOrIccidLength <= 19)
      {
        if ($msisdnOrIccidLength == 19)
        {
          $customer[0] = substr($customer[0], 0, -1);
        }
        $this->iccidList[] = $customer[0];
      }
      else
      {
        $this->notIccidOrMsisdn[] = $customer[0];
      }
      
      if (empty($customer[1]))
      {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: Missing cancellation reasons', 'VV0116');
      }
    }

    if (!empty($this->notIccidOrMsisdn))
    {
      $this->addToOutput('invalid_msisdn_or_iccids', $this->notIccidOrMsisdn);
      return $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid msisdn or iccids', 'VV0116');
    }
  }

  /**
   * Throw an exception if there are iccids out of test range
   * 
   * @param array $iccidsOutOfRange
   * @return NULL
   * @throws Exception
   */
  private function throwExceptionIfIccidsAreOutOfRange(array $iccidsOutOfRange)
  {
    if (!empty($iccidsOutOfRange))
    {
      $this->addToOutput('not_test_sims', $iccidsOutOfRange);
      return $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid or White listed test sims', 'VV0236', 'Invalid or White listed ICCID range');
    }
  }

  /**
   * Find the cancellation reason
   * 
   * @param $customer
   * @return string
   */
  private function getCancellationReason($customer)
  {
    $count = count($this->customersList);
    $cancellationReason = '';

    for ($i = 0; $i < $count; $i++)
    {
      if ($this->customersList[$i][0] == $customer->current_iccid || $this->customersList[$i][0] == $customer->current_mobile_number)
      {
        $cancellationReason = $this->customersList[$i][1];
      }
    }

    return $cancellationReason;
  }

  /**
   * Load the mint test sim ranges.
   *
   * @return mixed
   * @throws Exception
   */
  private function loadSimRanges()
  {
    $list = $this->configuration->findConfigByKey('mint/sim/test/list');

    if (empty($list))
    {
      throw new Exception('The range of permissible sims to cancel has not been set.');
    }

    return $this->formatSimRange($list);
  }

  /**
   * Load the mint white listed sims.
   *
   * @return mixed
   */
  private function loadWhiteListedSims()
  {
    return $this->formatSimRange($this->configuration->findConfigByKey('mint/sim/test/whitelist'));
  }

  /**
   * Format sim list from string.
   *
   * @param $range
   * @return array
   */
  private function formatSimRange($range)
  {
    if (empty($range))
    {
      return [];
    }

    $range = explode("|", $range);

    return array_map(function($range) {
      return explode(",", $range);
    }, $range);
  }

  /**
   * Get customers from MVNE by iccid or msisdn
   *
   * @param $customerDbList
   * @param $list
   * @param $isIccidList
   * @return array|NULL
   * @throws Exception
   */
  private function getCustomersWithNoCustomerId($customerDbList, $list, $isIccidList)
  {
    if (!empty($customerDbList))
    {
      foreach ($customerDbList as $customer)
      {
        unset($list[array_search($customer->current_iccid, $list)]);
      }
    }
    
    $customersNotInDb = array_values($list);
    $customersNotInDbCount = count($customersNotInDb);

    for ($i = 0; $i < $customersNotInDbCount; $i++)
    {
      if (!$isIccidList)
      {
        $params = [
          'actionUUID' => $this->getRequestId(),
          'msisdn' => $customersNotInDb[$i],
          'iccid' => ''
        ];
      }
      else
      {
        $params = [
          'actionUUID' => $this->getRequestId(),
          'msisdn' => '',
          'iccid' => $this->utilities->luhnenize($customersNotInDb[$i])
        ];
      }

      $result = $this->control->mwGetMVNEDetails($params);

      if ($result->is_failure() || !$result->data_array['success'])
      {
        return $this->errException("ERR_API_INTERNAL: " . $result->data_array['ResultMsg'], 'MW0001', 'No account found for ' . $customersNotInDb[$i]);
      }
      
      if (empty($result->data_array['QuerySubscriber']->MSISDN || empty($result->data_array['QuerySubscriber']->SIM)))
      {
        return $this->errException("ERR_API_INTERNAL: Middleware did not respond with msisdn and iccid", 'MW0001', 'Middleware did not respond with msisdn and iccid for ' . $customersNotInDb[$i]);
      }
      
      if (!$isIccidList && !$this->checkIfIccidIsWithinTestRange($result->data_array['QuerySubscriber']->SIM))
      {
        return $this->throwExceptionIfIccidsAreOutOfRange([$result->data_array['QuerySubscriber']->SIM]);
      }

      $this->customersWithNoCustomerIds[] = (object) [
        'msisdn' => $result->data_array['QuerySubscriber']->MSISDN,
        'iccid' => $result->data_array['QuerySubscriber']->SIM
      ];
    }
    
    return $this->customersWithNoCustomerIds;
  }
}

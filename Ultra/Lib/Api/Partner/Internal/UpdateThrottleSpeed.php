<?php
namespace Ultra\Lib\Api\Partner\Internal;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Internal;
use Ultra\Mvne\Adapter;

/**
 * Class UpdateThrottleSpeed
 * @package Ultra\Lib\Api\Partner\Internal
 */
class UpdateThrottleSpeed extends Internal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Adapter
   */
  private $adapter;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * UpdateThrottleSpeed constructor.
   * @param CustomerRepository $customerRepository
   * @param Adapter $adapter
   * @param Configuration $configuration
   */
  public function __construct(
    CustomerRepository $customerRepository,
    Adapter $adapter,
    Configuration $configuration
  )
  {
    $this->customerRepository = $customerRepository;
    $this->adapter = $adapter;
    $this->configuration = $configuration;
  }

  public function internal__UpdateThrottleSpeed()
  {
    list($customer_id, $target_speed, $channel) = $this->getInputValues();

    try
    {
      $customer = $this->customerRepository->getCombinedCustomerByCustomerId($customer_id);

      if (!$customer) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found', 'VV0031');
      }

      if ($customer->plan_state != 'Active') {
        return $this->errException("ERR_API_INVALID_ARGUMENTS: customer $customer_id is inactive", 'VV0031', "customer $customer_id is inactive");
      }

      $result = $this->adapter->mvneMakeitsoUpdateThrottleSpeed(
        $customer_id,
        $this->configuration->getPlanFromCosId($customer->cos_id),
        $target_speed,
        empty($channel) ? 'LTE_SELECT.GW_GUI' : 'LTE_SELECT.' . $channel
      );

      if (!$result['success'])
      {
        $error = count($result['errors']) ? $result['errors'][0] : '';
        return $this->errException('The mvneMakeitsoUpdateThrottleSpeed result failed.', 'IN0002', $error);
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

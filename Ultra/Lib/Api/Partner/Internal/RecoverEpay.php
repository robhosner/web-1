<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

require_once 'Ultra/Lib/Billing/functions.php';
require_once 'Ultra/Lib/Util/Redis/Billing.php';

class RecoverEpay extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__RecoverEpay
   *
   * Recovers zombie epay transactions
   *
   * @return Result object
   */
  public function internal__RecoverEpay()
  {
    // init
    try
    {
      teldata_change_db();

      $query_result = htt_billing_actions_zombie_transactions_select();

      foreach($query_result as $row)
      {
        $redisBilling = new \Ultra\Lib\Util\Redis\Billing();
        $result = \Ultra\Lib\Billing\billingTask( $redisBilling , $row[0] );

        dlog('',"result = %s", $result);
      }

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('' , $e->getMessage ());
    }
    return $this->result;
  }
}

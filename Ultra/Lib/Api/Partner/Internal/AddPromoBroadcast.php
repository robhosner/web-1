<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class AddPromoBroadcast extends \Ultra\Lib\Api\Partner\Internal
{
 /**
   * internal__AddPromoBroadcast
   * create a new broadcast SMS campaigns
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/SMS+Broadcast+and+Promotions+Framework
   * @return Result object
   * @author: VYT, 2014-09-22
   */
  public function internal__AddPromoBroadcast()
  {
    // initialize
    list ($name, $message_en, $message_es, $message_zh, $started_date, $promo_shortcode, $promo_description, $promo_expires_date, 
          $promo_action, $promo_value, $promo_value_mint_data_addon, $promo_enabled, $earliest_delivery_time, $latest_delivery_time, $messages_per_hour,
          $send_to_active, $send_to_suspended, $send_to_multi_month, $send_to_opt_out, $send_to_auto_renewal, $brand) = $this->getInputValues();

    $campaign_id = NULL;

    try
    {
      $invalid_characters = array(',','&');
      foreach (array($message_en, $message_es, $message_zh) as $message)
      {
        foreach ($invalid_characters as $invalid_character)
        {
          if (strpos($message, $invalid_character) !== FALSE)
            $this->errException('ERR_API_INVALID_ARGUMENTS: message cannot contain character: ' . $invalid_character, 'VV0255');
        }
      }

      // check dates
      if ( ! $started_date_time = timestamp_to_date($started_date, MSSQL_DATE_FORMAT, 'UTC'))
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid started_date', 'VV0033', 'Campaign start date is invalid');

      if ($promo_expires_date)
      {
        if ( ! $promo_expires_date_time = timestamp_to_date($promo_expires_date, MSSQL_DATE_FORMAT, 'UTC'))
          $this->errException('ERR_API_INVALID_ARGUMENTS: invalid promo_expires_date', 'VV0033', 'Campaign expiration date is invalid');
        if ($promo_expires_date <= time())
          $this->errException('ERR_API_INVALID_ARGUMENTS: invalid promo_expires_date', 'VV0033', 'Campaign expiration date already passed');
        if ($started_date > $promo_expires_date)
          $this->errException('ERR_API_INVALID_ARGUMENTS: invalid promo_expires_date', 'VV0033', 'Campaign expiration date must be greater than campaign start date');
      }

      $brand = \Ultra\BrandConfig\getBrandFromShortName($brand);
      if (!$brand)
        $this->errException('ERR_API_INVALID_ARGUMENTS: The API is not available for this brand', 'FA0004');

      // check if campaign with this name already exists
      $campaign = get_ultra_promo_broadcast_campaign(array('name' => $name));
      if ($campaign->is_success() && count($campaign->data_array))
        $this->errException('ERR_API_INTERNAL: campaign name already exists', 'DB0001');

      if ( ! in_array($send_to_multi_month, array(0, 1)) )
        $send_to_multi_month = 0;

      // assemble campaign param
      $parameters = array(
        'name'                 => $name,
        'message_en'           => $message_en,
        'message_es'           => $message_es,
        'message_zh'           => $message_zh,
        'started_date'         => $started_date_time,
        'promo_shortcode'      => '',
        'promo_description'    => $promo_description,
        'promo_expires_date'   => '',
        'promo_action'         => '',
        'promo_value'          => 0,
        'promo_enabled'        => $promo_enabled,
        'messages_per_hour'    => $messages_per_hour,
        'send_to_active'       => $send_to_active,
        'send_to_suspended'    => $send_to_suspended,
        'send_to_multi_month'  => $send_to_multi_month,
        'send_to_opt_out'      => $send_to_opt_out,
        'send_to_auto_renewal' => $send_to_auto_renewal,
        'brand_id'             => $brand['id']
      );

      // MVNO-2725: check if campaign keyword is not currently used
      if ($promo_enabled)
      {
        $parameters['promo_action']       = $promo_action;
        $parameters['promo_shortcode']    = $promo_shortcode;
        $parameters['promo_expires_date'] = $promo_expires_date_time;
        $parameters['promo_value']        = $promo_value ? $promo_value : $promo_value_mint_data_addon ;

        $promotional = get_ultra_promo_broadcast_campaign(array('promo_enabled' => 1, 'promo_shortcode' => $promo_shortcode));
        if ($promotional->is_success() && count($promotional->data_array))
          foreach ($promotional->data_array as $campaign)
            if ( ! in_array($campaign->STATUS, array('ERROR','ABORTED')) && ! ($campaign->STATUS == 'COMPLETED' && $campaign->is_expired))
              $this->errException('ERR_API_INTERNAL: campaign keyword already used in campaign ID ' . $campaign->PROMO_BROADCAST_CAMPAIGN_ID, 'DB0001');
      }

      if ($earliest_delivery_time)
      {
        if (intval($earliest_delivery_time) > 500 && intval($earliest_delivery_time) < 2400)
          $parameters['earliest_delivery_time'] = $earliest_delivery_time;
        else
          $this->errException('ERR_API_INVALID_ARGUMENTS: invalid earliest_delivery_time', 'VV0033');
      }

      if ($latest_delivery_time)
      {
        if (intval($latest_delivery_time) > 500 && intval($latest_delivery_time) < 2400)
          $parameters['latest_delivery_time'] = $latest_delivery_time;
        else
          $this->errException('ERR_API_INVALID_ARGUMENTS: invalid latest_delivery_time', 'VV0033');
      }

      if ($earliest_delivery_time && $latest_delivery_time && ($earliest_delivery_time > $latest_delivery_time))
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: latest_delivery_time must be greater than earliest_delivery_time', 'VV0033');
      }

      // create new campaign
      teldata_change_db();
      $broadcast = new \PromoBroadcast();
      $result = $broadcast->addCampaign($parameters);
      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: failed to create new campaign', 'DB0001');

      // get campaing_id
      $campaign = get_ultra_promo_broadcast_campaign(array('name' => $name));
      if ($campaign->is_failure() || count($campaign->data_array) != 1)
        $this->errException('ERR_API_INTERNAL: failed to get campaign ID', 'DB0001');
      $campaign_id = $campaign->data_array[0]->PROMO_BROADCAST_CAMPAIGN_ID;

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('campaign_id', $campaign_id);
    return $this->result;
  }
}

?>

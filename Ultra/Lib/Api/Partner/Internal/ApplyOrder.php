<?php
namespace Ultra\Lib\Api\Partner\Internal;

use Exception;
use Ultra\Lib\Api\Partner\Internal;
use Ultra\Plans\Flex;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;

class ApplyOrder extends Internal
{
  /**
   * @var Flex
   */
  private $flex;

  private $customerRepo;

  /**
   * ApplyOrder constructor.
   * @param Flex $flex
   */
  public function __construct(Flex $flex, CustomerRepository $customerRepo)
  {
    $this->flex = $flex;
    $this->customerRepo = $customerRepo;
  }

  public function internal__ApplyOrder()
  {
    list ($customer_id) = $this->getInputValues();

    try {
      // check if customer is active to apply bolt ons immediately
      $customer = $this->customerRepo->getCustomerById($customer_id, ['plan_state']);
      if (!$customer) {
        throw new \Exception('Failed to lookup CUSTOMER record where customer_id = ' . $customer_id);
      }
      $withBoltOns = $customer->plan_state == STATE_ACTIVE;

      $result = $this->flex->applyCustomerOrder($customer_id, $withBoltOns);
      if (!$result->is_success()) {
        foreach ($result->get_errors() as $e) {
          $this->addError($e, 'IN0002', $e);
        }
        throw new Exception('Failed to apply customer order');
      }

      $this->succeed();
    } catch(Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

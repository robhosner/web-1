<?php

namespace Ultra\Lib\Api\Partner\Internal;

class SetE911SubscriberAddress extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__SetE911SubscriberAddress
   *
   * Set customer E911 Subscriber Address
   *
   * @param  Integer $customer_id
   * @param  String  $address
   * @return Result  object
   */
  
  public function internal__SetE911SubscriberAddress()
  {
    list ($customer_id, $address) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id, array(
        'CURRENT_ICCID_FULL',
        'current_mobile_number',
        'plan_state'
      ));
      
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Ultra customer does not exist', 'VV0031');

      if ($customer->plan_state != STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active.', 'IN0001');

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $params = array(
        'customer_id' => $customer_id,
        'actionUUID'  => $this->getRequestId(),
        'msisdn'      => $customer->current_mobile_number,
        'iccid'       => $customer->CURRENT_ICCID_FULL,
        'E911Address' => array(
          'addressLine1' => (!empty($address['addressLine1'])) ? $address['addressLine1'] : '',
          'addressLine2' => (!empty($address['addressLine1'])) ? $address['addressLine2'] : '',
          'City'         => (!empty($address['city']))         ? $address['city']         : '',
          'State'        => (!empty($address['state']))        ? $address['state']        : '',
          'Zip'          => (!empty($address['zip']))          ? $address['zip']          : ''
        )
      );
      $result = $mwControl->mwUpdateSubscriberAddress($params);

      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: MW error (1)', 'MW0001');

      if ( ! empty($result->data_array['ResultCode']) && $result->data_array['ResultCode'] != '100')
        $this->errException('ERR_API_INTERNAL: ACC errors - ' . $result->data_array['errors'][0], 'MW0001');

      if ( ! $result->data_array['success'] || empty($result->data_array['body']))
        $this->errException('ERR_API_INTERNAL: MW error (2)', 'MW0001');

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

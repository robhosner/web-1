<?php

namespace Ultra\Lib\Api\Partner\Internal;

class RedisRead extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__RedisRead
   *
   * Read from redis given operator and key
   *
   * @return Result object
   */
  
  public function internal__RedisRead()
  {
    list ($operator, $key) = $this->getInputValues();

    try
    {
      $redis = new \Ultra\Lib\Util\Redis;

      if ( ( $operator == 'get' ) || ( $operator == 'smembers' ) || ( $operator == 'zcard' ) )
      {
        $val = $redis->$operator($key);
        $val = (is_scalar($val)) ? $val : json_encode($val);
        $this->addToOutput('value', $val);
      }
      elseif ( $operator == 'zrange' )
      {
        $val = $redis->zrange($key,0,-1);
        $this->addToOutput('value', json_encode($val));
      }
      else
        $this->errException('Operator Not Allowed', 'IN0002');

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}


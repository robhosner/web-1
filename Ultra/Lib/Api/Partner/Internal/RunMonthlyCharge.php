<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class RunMonthlyCharge extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__RunMonthlyCharge
   *
   * runs monthly charge on a SINGLE customer id
   *
   * @return Result object
   */
  public function internal__RunMonthlyCharge()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      require_once 'lib/mrc/functions.php';

      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id, ['plan_state']);
      if ( ! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');

      if ($customer->plan_state != STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active.', 'IN0001');

      $params = array( 'customer_id' => $customer_id, 'environments' => 'rgalli2_dev' );
      $result = internal_func_resolve_pending_transitions($params);

      $input_values = [
        'daily_init'        => FALSE,
        'date'              => 'today',
        'cancellation'      => FALSE,
        'customer_id_list'  => array($customer_id),
        'groups'            => array(-1),
        'max_customers'     => 1,
        'retry'             => FALSE,
        'skip_cc'           => FALSE,
        'legacy'            => FALSE
      ];

      main( $input_values );

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

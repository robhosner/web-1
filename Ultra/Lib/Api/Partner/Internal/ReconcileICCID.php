<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class ReconcileICCID extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__ReconcileICCID
   *
   * Fix the MSISDN for a customer.
   *
   * @param string customer_id
   * @return Result object
   */
  public function internal__ReconcileICCID()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      teldata_change_db();

      $customer = get_ultra_customer_from_customer_id($customer_id);

      if ($customer)
      {
        if ($customer->plan_state == STATE_ACTIVE)
        {
          $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

          $result = $mwControl->mwQuerySubscriber(
            array(
              'actionUUID' => $this->getRequestId(),
              'msisdn' => $customer->current_mobile_number
            )
          );

          if ($errors = $result->get_errors())
            $this->errException($errors[0], 'MW0001');

          if ($result->data_array['body']->ResultCode != 100)
            $this->errException($result->data_array['body']->ResultMsg, 'MW0001');

          if ($result->data_array['body']->SubscriberStatus != 'Active')
            $this->errException('ERR_API_INTERNAL: SubscriberStatus is not Active.', 'MW0001');

          if (empty($result->data_array['body']->SIM))
            $this->errException('ERR_API_INTERNAL: SIM is empty.', 'MW0001');

          if (strlen($result->data_array['body']->SIM) != 19)
            $this->errException('ERR_API_INTERNAL: SIM is formatted incorrectly.', 'MW0001');

          if ($customer->CURRENT_ICCID_FULL != $result->data_array['body']->SIM)
          {
            $params = array(
              'iccid_current' => $result->data_array['body']->SIM,
              'customer_id' => $customer_id
            );

            if (!is_mssql_successful(logged_mssql_query(htt_customers_overlay_ultra_update_query($params))))
            {
              $this->errException('ERR_API_INTERNAL: An unexpected database error occurred while writing to HTT_CUSTOMERS_OVERLAY_ULTRA', 'DB0001');
            }

            $params = array(
              'sim_activated' => 1,
              'globally_used' => 1,
              'customer_id' => $customer_id,
              'batch_sim_set' => array($result->data_array['body']->SIM)
            );

            if (!is_mssql_successful(logged_mssql_query(htt_inventory_sim_update_query($params))))
            {
              $this->errException('ERR_API_INTERNAL: An unexpected database error occurred while writing to HTT_INVENTORY_SIM', 'DB0001');
            }
          }
          else
          {
            $this->addWarning('Nothing to do');
          }
        }
        else
        {
          $this->errException('ERR_API_INTERNAL: Invalid customer state for this command', 'IN0001');
        }
      }
      else
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: Ultra customer does not exist', 'VV0031');
      }

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

?>

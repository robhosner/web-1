<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetEnvironmentInfo extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetEnvironmentInfo
   *
   * Show some environment info
   *
   * @return Result object
   */
  public function internal__GetEnvironmentInfo ()
  {
    $this->addToOutput('db_name',   \Ultra\UltraConfig\getDBName());

    // PROD-1989: check if we are in clustered enviro
    $host = implode(' ', \Ultra\UltraConfig\redisClusterHosts());
    if (empty($host))
      $host = \Ultra\UltraConfig\redisHost();
    $this->addToOutput('redis_host', $host);

    $this->succeed ();

    return $this->result;
  }
}


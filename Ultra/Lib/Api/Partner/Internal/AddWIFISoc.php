<?php

namespace Ultra\Lib\Api\Partner\Internal;

class AddWIFISoc extends \Ultra\Lib\Api\Partner\Internal
{
/**
   * internal__AddWIFISoc
   *
   * Adds WIFI Soc to customer account
   *
   * @return Result object
   */
  
  public function internal__AddWIFISoc()
  {
    list ($customer_id) = $this->getInputValues();

    try
    {
      $account = get_account_from_customer_id($customer_id, array('COS_ID'));
      $customer = get_ultra_customer_from_customer_id($customer_id, array(
        'CUSTOMER_ID',
        'current_mobile_number',
        'CURRENT_ICCID_FULL',
        'preferred_language',
        'plan_state',
        'BRAND_ID'
      ));

      if ( ! $customer || ! $account)
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');

      if (in_array($customer->BRAND_ID, [3]))
        $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

      if ($customer->plan_state != STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active.', 'IN0001');

      $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

      $mwResult = $mwControl->mwQuerySubscriber(array(
        'actionUUID' => $this->getRequestId(),
        'iccid'      => $customer->CURRENT_ICCID_FULL,
        'msisdn'     => $customer->current_mobile_number
      ));

      // throws exception on error
      $this->handleMWResultCheck($mwResult);

      $alreadyApplied = FALSE;
      $addOns = $mwResult->data_array['body']->AddOnFeatureInfoList->AddOnFeatureInfo;
      foreach ($addOns as $addOn)
      {
        if ($addOn->UltraServiceName == 'N-WFC')
        {
          $alreadyApplied = TRUE;
          break;
        }
      }

      if ($alreadyApplied)
      {
        $this->AddWarning('ERR_API_INVALID_ARGUMENTS: Customer already has N-WFC applied');
      }
      else
      {
        $result = $mwControl->mwMakeitsoUpgradePlan(
          array(
            'actionUUID'         => $this->getRequestId(),
            'msisdn'             => $customer->current_mobile_number,
            'iccid'              => $customer->CURRENT_ICCID_FULL,
            'customer_id'        => $customer->CUSTOMER_ID,
            'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
            'ultra_plan'         => get_plan_from_cos_id( $account->COS_ID ), # L[12345]9,
            'preferred_language' => $customer->preferred_language,
            'option'             => 'N-WFC|ADD'
          )
        );

        if ($errors = $result->get_errors())
          $this->errException('ERR_API_INTERNAL: ' . $errors[0], 'MW0001');
      }

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }

  private function handleMWResultCheck($mwResult)
  {
    // check results
    if ($mwResult->is_failure())
      $this->errException('ERR_API_INTERNAL: MW error (1)', 'MW0001');

    if (! empty($mwResult->data_array['ResultCode']) && $mwResult->data_array['ResultCode'] != '100')
      $this->errException('ERR_API_INTERNAL: ACC errors - ' . $mwResult->data_array['errors'][0], 'MW0001');

    if ( ! $mwResult->data_array['success'] || empty($mwResult->data_array['body']))
      $this->errException('ERR_API_INTERNAL: MW error (2)', 'MW0001');
  }
}

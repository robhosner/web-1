<?php

namespace Ultra\Lib\Api\Partner\Internal;

require_once 'Ultra/Lib/Api/Partner/Internal.php';

class GetAllStuckTransitions extends \Ultra\Lib\Api\Partner\Internal
{
  /**
   * internal__GetAllStuckTransitions
   *
   * Retrieve all transitions stuck for too much time.
   *
   * @return Result object
   */
  public function internal__GetAllStuckTransitions ()
  {
    $error_code = '';

    $this->addToOutput('stuck_transitions','');

    try
    {
      teldata_change_db();

      $stuck_transitions_data = get_stuck_transitions();

      if ( count($stuck_transitions_data) )
      {
        $stuck_transitions = array();

        foreach( $stuck_transitions_data as $transition_uuid => $data )
          $stuck_transitions[] = array( $transition_uuid , $data['customer_id'], $data['created'] , $data['from_state'] , $data['to_state'] , $data['environment'] );

        $this->addToOutput('stuck_transitions',$stuck_transitions);
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}

?>

<?php

namespace Ultra\Lib\Api\Partner;

if (!getenv("UNIT_TESTING"))
{
  include_once('db/call_session_log.php');
}

/**
 * Customer Care Partner classes
 * http://wiki.hometowntelecom.com:8090/display/SPEC/Commands+-+CustomerCare
 */
class CustomerCare extends \Ultra\Lib\Api\PartnerBase
{
  /**
   * Given a mobile_number, will query a subscriber's portin status.
   * @param string $mobile_number
   * @return stdclass
   * @throws \Exception
   * @author bwalters@ultra.me
   */
  protected function queryStatusByMobileNumber($mobile_number = false)
  {
    if (!$mobile_number) $this->errException("ERR_API_INTERNAL: mobile_number is required", "MP0003");

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $result = $mwControl->mwQueryStatus(
      array(
        'actionUUID'         => getNewActionUUID('customercare ' . time()),
        'msisdn'             => $mobile_number
      )
    );

    // Failure
    if (!$result->is_success())
    {
      $errors = $result->get_errors();
      foreach ($errors as $error)
      {
        $this->errException($error, 'VV0031');
      }
    }

    // Success but w/errors?
    if ( isset($result->data_array['errors']) && is_array($result->data_array['errors']) && count($result->data_array['errors']) )
    {
      $errors = $result->get_errors();
      foreach ($errors as $error)
      {
        $this->errException($error, 'VV0031');
      }
    }
    elseif ( !isset( $result->data_array['body'] ) && !is_object($result->data_array['body']) )
    {
      $errors = $result->get_errors();
      foreach ($errors as $error)
      {
        $this->errException($error, 'VV0031');
      }
    }

    return $result;
  }

  /**
   * Given a mobile_number, will add number to portin queue?
   * @param integer $customer_id
   * @param array(string) $select_fields
   * @return stdclass
   * @throws \Exception
   * @author bwalters@ultra.me
   */
  protected function portInFromCarrier($mobile_number = false)
  {
    if (!$mobile_number) $this->errException("ERR_API_INTERNAL: mobile_number is required", "MP0003");

    $portInQueue = new \PortInQueue();

    $result = $portInQueue->loadByMsisdn( $mobile_number );

    if ( !$result->is_success() )
    {
      $errors  = $result->get_errors();
      foreach ($errors as $error)
      {
        $this->errException($error, 'VV0031');
      }
    }

    return $result;
  }
}


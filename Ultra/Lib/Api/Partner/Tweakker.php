<?php

namespace Ultra\Lib\Api\Partner;

require_once 'Ultra/Lib/Api/PartnerBase.php';

/**
 * Tweakker Partner class
 *
 * @project MVNE2
 * @author VYT, 2014-03-06
 */
class Tweakker extends \Ultra\Lib\Api\PartnerBase
{
  /**
   * tweakker__ValidateMsisdnMvne
   *
   * AMDOCS-204: given MSISDN return its MVNE either from HTT_ULTRA_MSISDN.MVNE or HTT_CUSTOMERS_OVERLAY_ULTRA.MVNE
   * @param string MSISDN
   * @return object Result
   * @author VYT, 2014-03-06
   */
  public function tweakker__ValidateMsisdnMvne()
  {
    // init
    list ($access_token, $msisdn) = $this->getInputValues();
    $msisdn = normalize_msisdn($msisdn, FALSE);
    $this->addToOutput('MVNE', NULL);

    try
    {
      // verify access token
      if (empty($access_token) || $access_token != 'e13a66R0ce9633787b3127Q7K48b6cb9')
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid access token', 'SE0006');

      // get MVNE from HTT_ULTRA_MSISDN table
      $mvne = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $msisdn, 'MVNE', NULL, 'HTT_ULTRA_MSISDN');

      // if not found then get it from HTT_CUSTOMERS_OVERLAY_ULTRA
      if (empty($mvne))
        $mvne = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $msisdn, 'MVNE', NULL, 'HTT_CUSTOMERS_OVERLAY_ULTRA');

      // fail if still not found or invalid
      if ($mvne != parent::MVNE1 && $mvne != parent::MVNE2)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid mobile number', 'MV0001');

      // success
      $this->addToOutput('MVNE', $mvne);
      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}


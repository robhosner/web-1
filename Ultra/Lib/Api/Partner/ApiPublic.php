<?php

namespace Ultra\Lib\Api\Partner;

require_once 'db/htt_ultra_locations.php';
require_once 'Ultra/Lib/Api/PartnerBase.php';
include_once 'db/htt_card_calling_rates.php';

/**
 * ApiPublic partner class
 *
 * @project ultra.me
 * @author bwalters@ultra.me
 */
class Apipublic extends \Ultra\Lib\Api\PartnerBase
{

  /**
   * Format the locations information.
   *
   * @param array $locations
   * @author bwalters@ultra.me
   */
  protected function formatLocations(array $locations)
  {
    if ( $locations && is_array($locations) && count($locations) )
    {
      // encode as UTF-8 to avoid json_encode errors
      foreach ($locations as &$location)
      {
        $location->RETAILER_NAME = utf8_encode($location->RETAILER_NAME);
      }
    }
    else
    {
      $warnings[] = "ERR_API_INTERNAL: data not found";
    }

    return $locations;
  }
}


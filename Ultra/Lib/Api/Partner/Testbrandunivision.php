<?php

namespace Ultra\Lib\Api\Partner;

require_once 'Ultra/Lib/Api/PartnerBase.php';

/**
 * Testbrandunivision Partner classes
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Testbrandunivision extends \Ultra\Lib\Api\PartnerBase
{

}


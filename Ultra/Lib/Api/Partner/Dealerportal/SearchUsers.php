<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class SearchUsers extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__SearchUsers
   *
   * Returns a list of users that belong to the dealer. Requires a valid security token.
   *
   * @return Result object
   */
  public function dealerportal__SearchUsers ()
  {
    list ( $security_token , $active ) = $this->getInputValues();

    $this->addToOutput('record_count', 0);
    $this->addToOutput('users', array());

    try
    {
      // retrieve and validate session
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      $result = \Ultra\Lib\DB\DealerPortal\getUsers($session_data['user_id'], $active);

      if ($result->is_success())
      {
        if (count($result->data_array))
        {
          $this->addToOutput('record_count', count($result->data_array));
          $this->addToOutput('users', $result->data_array);
        }
      }
      else
      {
        $errors = $result->get_errors();
        $this->errException('IN0002', 'API_INVALID_ARGUMENTS: ' . $errors[0]);
      }

      // success
      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ConfigureBoltOns extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ConfigureBoltOns
   * Overwrite Customer's Recurring Bolt Ons Configuration.
   * See http://wiki.hometowntelecom.com:8090/display/SPEC/Bolt+Ons+APIs
   * @return Result object
   */
  
  public function dealerportal__ConfigureBoltOns()
  {
    list ($security_token, $customer_id, $bolt_ons) = $this->getInputValues();
    $bolt_ons = empty($bolt_ons) ? array() : (is_array($bolt_ons) ? $bolt_ons : array($bolt_ons));

    try
    {
      // retrieve and validate session from $security_token
      teldata_change_db();
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // get customer info
      $account  = get_account_from_customer_id( $customer_id, array('CUSTOMER_ID','COS_ID') );

      if ( ! $account )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found' , 'VV0031');

      $state = internal_func_get_state_from_customer_id($account->CUSTOMER_ID);

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $state['state'], __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // PROD-741: must be Active or Suspended
      if ( ! in_array($state['state'], array(STATE_ACTIVE, STATE_SUSPENDED)))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state must be Active or Suspended.', 'IN0001');

      $plan = get_plan_from_cos_id($account->COS_ID);
      if (empty($plan))
        $this->errException('ERR_API_INVALID_ARGUMENTS: plan cannot be found.', 'IN0001');

      $customer = get_ultra_customer_from_customer_id( $customer_id , array('preferred_language', 'CUSTOMER_ID', 'MONTHLY_RENEWAL_TARGET'));

      if ( ! $customer )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found' , 'VV0031');

      $account_cos_id = ($customer->MONTHLY_RENEWAL_TARGET ? get_cos_id_from_plan($customer->MONTHLY_RENEWAL_TARGET) : $account->COS_ID);

      #if ( ! \Ultra\Lib\BoltOn\validateRecurringBoltOn($account_cos_id, $bolt_on_id))
      #  $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan.', 'VV0105');
      
      // get total cost of all bolt ons
      $total_cost = 0;

      $new_bolt_on_configuration = array();

      $new_bolt_on_products = array();

      $underscoreObject = new \__ ;

      foreach($bolt_ons as $bolt_on_id)
      {
        if ( ! \Ultra\Lib\BoltOn\validateRecurringBoltOn($account_cos_id, $bolt_on_id))
          $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan.', 'VV0105');

        $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo($bolt_on_id);
        if ( ! $boltOnInfo)
          $this->errException('ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid', 'VV0235');
        dlog('', "%s : boltOnInfo = %s",$bolt_on_id,$boltOnInfo);

        $total_cost += $boltOnInfo['cost'];
        $new_bolt_on_configuration[$boltOnInfo['option_attribute']] = $boltOnInfo['cost'];
        $new_bolt_on_products[] = $boltOnInfo['product'];
      }

      // verify that there are no duplicate bolt on products
      if ( count( $underscoreObject->uniq($new_bolt_on_products) ) != count( $new_bolt_on_products ) )
        $this->errException("ERR_API_INTERNAL: duplicate bolt on products", 'VV0234');

      dlog('',"total bolt on cost = %s",$total_cost);

      // configure bolt-ons
      $current_bolt_ons = \get_bolt_ons_values_from_customer_options($customer_id);
      dlog('', "current_bolt_ons = %s", $current_bolt_ons);
      dlog('', "new_bolt_on_configuration = %s", $new_bolt_on_configuration);

      // modify configuration in ULTRA.CUSTOMER_OPTIONS and log into ULTRA.BOLTON_TRACKER
      $result = \update_bolt_ons_values_to_customer_options($customer_id, $current_bolt_ons, $new_bolt_on_configuration, 'PORTAL');
      if ($result->is_failure())
        $this->errException('ERR_API_INTERNAL: DB error', 'DB0001');

      $this->succeed ();
    }
    catch(\Exception $e)
    {
      dlog( '' , $e->getMessage());
    }

    return $this->result;
  }
}


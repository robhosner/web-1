<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ReportActivationActivity extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ReportActivationActivity
   *
   * The activation activity report provides a listing of activations performed.
   *
   * @return Result object
   */
  public function dealerportal__ReportActivationActivity ()
  {
    list ( $security_token , $page_size , $page_ord , $date_from , $date_to ) = $this->getInputValues();

    if ( ! $page_size ) $page_size = 100;
    if ( ! $page_ord  ) $page_ord  = 1;

    $this->addToOutput('page_size',$page_size);
    $this->addToOutput('page_ord',$page_ord);
    $this->addToOutput('record_count','');
    $this->addToOutput('customers','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $redis = new \Ultra\Lib\Util\Redis;

      $search_array = array(
        'date_from'  => $date_from,
        'date_to'    => $date_to
      );

      // Role Context Check

      // if ( ( $session_data['role'] == self::ROLE_NAME_DEALER_EMPLOYEE ) || ( $session_data['role'] == self::ROLE_NAME_DEALER_OWNER ) )
      //   $search_array['dealer']      = $session_data['dealer'];

      if (in_array($session_data['role'], array(self::ROLE_NAME_SUB_ALL , self::ROLE_NAME_SUB_LIMITED, self::ROLE_NAME_SUB_EMPLOYEE)))
        $search_array['distributor'] = $session_data['distributor'];

      if (in_array($session_data['role'], array(self::ROLE_NAME_MASTER_ALL, self::ROLE_NAME_MASTER_LIMITED, self::ROLE_NAME_MASTER_EMPLOYEE))) 
        $search_array['masteragent'] = $session_data['masteragent'];

      dlog('',"search_array = %s",$search_array);

      $search_key = md5( json_encode ( $search_array ) );

      $redis_key = 'ultra/api/dealerportal/cache/' . __FUNCTION__ . '/' . $search_key;

      dlog('',"redis_key = $redis_key");

      // get data from cache

      $cached_result = $redis->get( $redis_key );

      if ( $cached_result )
      {
        // no need to query the DB

        $decoded_result = json_decode( $cached_result );

        // extract only the needed data

        $paginated_output = paginate_output( $decoded_result , $page_size , $page_ord );

        if ( ! count( $paginated_output ) )
        {
          $this->addToOutput('record_count',0);

          $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
        }

        $this->addToOutput('customers',$paginated_output);
        $this->addToOutput('record_count',count( $decoded_result ));
      }
      else
      {
        // get data from DB

        $reportActivationActivity = get_report_activation_activity( $search_array );

        if ( ( $reportActivationActivity->is_failure() )
          || ( ( ! $reportActivationActivity->data_array ) || ( ! count( $reportActivationActivity->data_array ) ) )
        )
        {
          $this->addToOutput('record_count',0);
          $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
        }
        else
        {
          // extract only the needed data

          $paginated_output = paginate_output( $reportActivationActivity->data_array , $page_size , $page_ord );

          if ( ! count( $paginated_output ) )
          {
            $this->addToOutput('record_count',0);

            $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
          }

          $plan_name_map = get_plan_name_map();

          // MVNO-2353: post-process all dates
          $schema = array('funding_date', 'created_date');
          foreach ($paginated_output as &$row)
          {
            $row->plan_name = $plan_name_map[$row->cosId];
            unset($row->cosId);

            foreach ($schema as $column)
              $row->$column = date_sql_to_usa($row->$column, TRUE);

            $row->BRAND = \Ultra\UltraConfig\getShortNameFromBrandId($row->BRAND_ID);
            unset($row->BRAND_ID);
          }

          $this->addToOutput('customers',$paginated_output);
          $this->addToOutput('record_count',count($reportActivationActivity->data_array));
          // cache result in Redis

          //$redis->set( $redis_key , json_encode( $customers_search_result->data_array ) , 15 * 60 * 60 );
        }
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
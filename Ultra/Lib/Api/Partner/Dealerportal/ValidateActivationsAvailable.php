<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ValidateActivationsAvailable extends \Ultra\Lib\Api\Partner\Dealerportal
{
    /**
   * dealerportal__ValidateActivationsAvailable
   *
   * Confirms Ultra activations availability.
   *
   * @return Result object
   */
  public function dealerportal__ValidateActivationsAvailable ()
  {
    list ( $security_token ) = $this->getInputValues();

    $this->addToOutput('activations_enabled','');
    $this->addToOutput('message','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $activations_enabled = activations_enabled(TRUE);

      $this->addToOutput('activations_enabled',$activations_enabled);

      if ( ! $activations_enabled )
        $this->addToOutput('message', \Ultra\UltraConfig\dealer_portal_activations_disabled_msg());

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
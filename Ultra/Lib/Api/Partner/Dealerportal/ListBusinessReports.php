<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class ListBusinessReports extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ListBusinessReports
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @see DEAL-36
   * @return Result object
   */
  public function dealerportal__ListBusinessReports()
  {
    // initialization
    $params = $this->getNamedInputValues(); // security_token, business_type, business_code, request_type

    if (empty($params['business_code']))
      $params['business_code'] = NULL;
    $reports = array();
    $record_count = 0;

    try
    {
      // verify session
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($params['security_token'], __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      $params = $this->overwriteIdentityParameters($params, $session_data, 'MVNO');

      // get available reports
      $params['user_id'] = $session_data['user_id'];
      list($reports, $error) = \Ultra\Lib\DB\DealerPortal\listBusinessReports($params);
      if ($error)
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('reports', $reports);
    $this->addToOutput('record_count', count($reports));
    return $this->result;
  }
}

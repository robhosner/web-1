<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class CreateMiscellaneousAdjustment extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CreateMiscellaneousAdjustment
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @see DEAL-234
   * @return Result object
   */
  public function dealerportal__CreateMiscellaneousAdjustment()
  {
    // initialization
    $params = $this->getNamedInputValues(); // security_token, business_type, business_code, period_id, amount, comment

    try
    {
      // verify session
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($params['security_token'], __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // add default DB values
      $params['author_id'] = $session_data['user_id'];
      if ($error = \Ultra\Lib\DB\DealerPortal\createMiscellaneousAdjustment($params))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

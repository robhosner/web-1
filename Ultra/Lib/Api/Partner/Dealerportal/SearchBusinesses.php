<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class SearchBusinesses extends \Ultra\Lib\Api\Partner\Dealerportal
{
/**
   * dealerportal__SearchBusinesses
   *
   * Returns a list of businesses visible to the logged in user: MVNO can search everyone,
   * Master/SubDistributor can only see associated dealers, Dealers cannot search anyone. Requires a valid security token.
   *
   * @return Result object
   */
  
  public function dealerportal__SearchBusinesses()
  {
    list ( $security_token , $business_type, $business_code, $business_name, $active ) = $this->getInputValues();
    $output = array('record_count' => 0, 'businesses'   => array());

    try
    {
      // retrieve and validate session
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      $params = array(
        'user_id' => $session_data['user_id'],
        'business_type' => $business_type);

      if ($business_code)
        $params['business_code'] = $business_code;

      if ($business_name)
        $params['business_name'] = $business_name;

      $result = \Ultra\Lib\DB\DealerPortal\getBusinesses($params, $active);

      if ($result->is_success() && count($result->data_array))
      {
        // convert dates to UNIX time
        foreach ($result->data_array as $business)
        {
          $business->effective_date = date_to_epoch($business->effective_date);
          $business->business_name  = preg_replace('/[^[:print:]]/', '', $business->business_name); // clean ASCII printable only
          $output['businesses'][] = $business;
        }
        $output['record_count'] = count($result->data_array);
      }

      else
        $this->errException('API_INVALID_ARGUMENTS: No data found', 'ND0001', count($errors = $result->get_errors()) ? $errors[0] : NULL);

      // success
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    $this->addArrayToOutput($output);
    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class SearchActivationHistoryDealer extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__SearchActivationHistoryDealer
   *
   * Check status of customers from ULTRA.HTT_ACTIVATION_HISTORY
   *
   * @return Result object
   */
  public function dealerportal__SearchActivationHistoryDealer ()
  {
    list ( $security_token , $page_size , $page_ord , $epoch_from , $epoch_to , $date_selected ) = $this->getInputValues();

    if ( ! $page_size ) $page_size = 100;
    if ( ! $page_ord  ) $page_ord  = 1;

    $this->addToOutput('page_size',$page_size);
    $this->addToOutput('page_ord',$page_ord);
    $this->addToOutput('record_count','');
    $this->addToOutput('customers','');

    try
    {
      teldata_change_db(); // connect to the DB

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      // get data from ULTRA.HTT_ACTIVATION_HISTORY

      $customers_search_result = find_ultra_activation_history(
        array(
          'dealer'        => $session_data['dealer'],
          'epoch_from'    => $epoch_from,
          'epoch_to'      => $epoch_to,
          'date_selected' => $date_selected
        )
      );

      // PROD-977: no data is not an error
      if ($customers_search_result->is_failure() || ! $customers_search_result->data_array || ! count($customers_search_result->data_array))
      {
        $this->addToOutput('record_count', 0);
        $this->addToOutput('customers', array());
        dlog('', 'no data not found');
      }
      else
      {

        // MVNO-2302 phase 1: replace all final_state = Activated with final_state = Completed
        foreach($customers_search_result->data_array as $row)
        {
          if ( $row->final_state == 'Activated' )
            $row->final_state = FINAL_STATE_COMPLETE;
  
          $plan_cost = get_plan_from_cos_id( $row->cos_id ); # L[12345]9
          $plan_cost = substr( $plan_cost , -2 ); # L[12345]9 => [12345]9
  
          // cos_id
          $row->plan_name   = cos_id_plan_description( $row->cos_id ); // Example: 'Ultra $19'
          $row->plan_amount = $plan_cost;
        }
  
        // extract only the needed data
        $paginated_output = paginate_output( $customers_search_result->data_array , $page_size , $page_ord );
  
        if ( ! count( $paginated_output ) )
        {
          $this->addToOutput('record_count',0);
  
          $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
        }
  
        $redis = new \Ultra\Lib\Util\Redis();
  
        $redis_port = new \Ultra\Lib\Util\Redis\Port( $redis );
  
        // add next_action and port_request_id for each customer
        foreach( $paginated_output as $row_output )
        {
          $row_output->last_attempted_transition_state = $redis->get( 'ultra/transition/by_customer_id/'.$row_output->customer_id.'/status' );
  
          $pending_transition = ! ! ( $row_output->last_attempted_transition_state == 'OPEN' );
  
          $row_output->updated_seconds_ago = 0;
  
          $update_timestamp                = 0;
  
          if (
            ( ( $row_output->final_state == 'Port-In Denied' ) || ( $row_output->final_state == 'Port-In Requested' ) )
            &&
            ( ( $row_output->plan_state  == 'Port-In Denied' ) || ( $row_output->plan_state  == 'Port-In Requested' ) )
          )
          {
            // port related attributes are:
            // - port_status
            // - port_resolution
            // - updated_seconds_ago
  
            $row_output->port_status         = $redis->get( 'ultra/port/status/' . $row_output->msisdn );
            $row_output->port_resolution     = $redis->get( 'ultra/port/provstatus_error_msg/' . $row_output->msisdn );
            $update_timestamp                = $redis->get( 'ultra/port/update_timestamp/' . $row_output->msisdn );
  
            if ( $update_timestamp )
              $row_output->updated_seconds_ago = time() - $update_timestamp;
          }
          else
          {
            $row_output->port_status         = '';
            $row_output->port_resolution     = '';
          }
  
          // maybe the Redis cache expired ...
          if (
            ( ( $row_output->final_state == 'Port-In Denied' ) || ( $row_output->final_state == 'Port-In Requested' ) )
            &&
            ( ( $row_output->plan_state  == 'Port-In Denied' ) || ( $row_output->plan_state  == 'Port-In Requested' ) )
            && !$row_output->port_resolution
            && !$row_output->port_status
            && !$update_timestamp
          )
          {
              // retrieve data from ULTRA_ACC..PORTIN_QUEUE
  
              // load the latest port attempt for the given msisdn (if any)
              $portInQueue = new \PortInQueue();
  
              $loadPortInQueueResult = $portInQueue->loadByCustomerId( $row_output->customer_id );

              $row_output->port_request_id = $redis_port->getRequestId( $row_output->customer_id , $row_output->final_state );

              teldata_change_db();

              $transition_result = provision_check_transition($row_output->port_request_id);

              if ( $loadPortInQueueResult->is_success() && $portInQueue->portin_queue_id )
              {
                list(
                  $port_success,
                  $port_pending,
                  $port_status,
                  $port_resolution
                ) = interpret_port_status( $portInQueue, $transition_result );
  
                $row_output->port_status     = $port_status;
                $row_output->port_resolution = $port_resolution;
  
                if ( property_exists( $portInQueue , 'updated_seconds_ago' ) && $portInQueue->updated_seconds_ago )
                  $row_output->updated_seconds_ago = $portInQueue->updated_seconds_ago;
              }
  
              teldata_change_db();
          }
  
          unset($row_output->MVNE);
  
          $row_output->next_action = get_next_action_from_plan_state( $row_output->final_state , $pending_transition , $row_output->port_status , $row_output->created_seconds_ago , $row_output->updated_seconds_ago );
        }
  
        $this->addToOutput('customers', $paginated_output);
        $this->addToOutput('record_count',count($customers_search_result->data_array));
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

// TODO: RBAC output filter

    return $this->result;
  }
}
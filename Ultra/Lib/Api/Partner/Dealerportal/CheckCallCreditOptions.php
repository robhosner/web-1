<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class CheckCallCreditOptions extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__CheckCallCreditOptions
   *
   * Returns possible purchase options for buying Additional Call Anywhere Credit.
   * ``bonus_percent`` is fixed at 25%
   * ``value``         is computed from ``cost`` and ``bonus_percent``
   *
   * @return Result object
   */
  public function dealerportal__CheckCallCreditOptions ()
  {
    list ( $security_token , $customer_id ) = $this->getInputValues();

    $this->addToOutput('call_credit_options','');

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer data
      $customer = get_customer_from_customer_id( $customer_id );

      if ( ! $customer )
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer( $session_data , $customer->CUSTOMER_ID , $customer->plan_state , __FUNCTION__ ) )
        $this->errException( "ERR_API_INTERNAL: API execution not allowed for the given caller" , 'SE0005' );

      // verify Active state for customer
      if ( $customer->plan_state != 'Active' )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active', 'IN0001');

      // retrieve purchase options for buying Additional Call Anywhere Credit
      $call_anywhere_additional_credit_options = \Ultra\UltraConfig\call_anywhere_additional_credit_options();

      if ( ! $call_anywhere_additional_credit_options || ! is_array( $call_anywhere_additional_credit_options ) )
        $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );

      $this->addToOutput( 'call_credit_options' , $call_anywhere_additional_credit_options );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
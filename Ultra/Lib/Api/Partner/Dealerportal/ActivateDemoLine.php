<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ActivateDemoLine extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ActivateDemoLine
   *
   * activate an ICCID as a dealer demo line
   * @see: DEMO-10
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Automation+of+Demo+Line+Program
   * @return Result object
   */
  public function dealerportal__ActivateDemoLine()
  {
    list($security_token, $iccid, $zipcode, $preferred_language) = $this->getInputValues();
    $this->addToOutput('request_id', NULL);
    $this->addToOutput('customer_id', NULL);

    // target plans by brand_id => plan
    $target_plans = array(1 => 'A39');

    try
    {
      teldata_change_db();

      // verify that $zipcode is allowed (in coverage)
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode',$zipcode,'in_coverage');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // validate ICCID
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID',$iccid,'VALID');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // retrieve and validate session
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);
      $dealerId = $session_data['dealer'];

      // DEMO-36: activating dealer is 'UMWeb' rather than logged in dealer
      if ( ! $activation_dealer = \Ultra\Lib\DB\Celluphone\getDealerInfo('UMWeb'))
        $this->errException('ERR_API_INTERNAL: DB error', 'DB0001');

      // DEMO-37: clear existing cache
      \Ultra\Lib\DemoLine\flushRedisKeyByDealerId($dealerId);

      // check that the authenticated dealer is eligibile for a demo line
      $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.DEMO_LINE', 1, array('DEMO_LINE_ID', 'ACTIVATIONS_NEEDED'), array('DEALER_ID' => $dealerId, 'STATUS' => DEMO_STATUS_ELIGIBLE), NULL, NULL, TRUE);
      $lines = mssql_fetch_all_objects(logged_mssql_query($sql));
      if ( ! count($lines))
        $this->errException('ERR_API_INVALID_ARGUMENTS: No eligibile demo lines available', 'VV0247');
      $line = $lines[0];
      dlog('', 'activating DEMO_LINE_ID %d for dealer ID %d', $line->DEMO_LINE_ID, $dealerId);

      // get SIM info
      if ( ! $sim = get_htt_inventory_sim_from_iccid($iccid))
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid SIM card', 'VV0065');

      // get target plan from ICCID->BRAND_ID
      if ( ! isset($target_plans[$sim->BRAND_ID]))
        $this->errException('ERR_API_INVALID_ARGUMENTS: ICCID requested has an INVALID brand type', 'IC0003');

      $target_plan = $target_plans[$sim->BRAND_ID];

      // validate ICCID
      if ( ! validate_ICCID($sim, 1))
        $this->errException('ERR_API_INVALID_ARGUMENTS: the given ICCID is invalid or already used', 'VV0065');

      // check if the ICCID is good to activate
      $redis = new \Ultra\Lib\Util\Redis;
      $key = "iccid/good_to_activate/$iccid";
      if ($redis->get($key))
        $redis->del($key);
      else
      {
        // check if we can activate
        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
        $result = $mwControl->mwCanActivate(
          array(
            'iccid'      => $iccid,
            'actionUUID' => getNewActionUUID('dealerportal ' . time())));
        if ($result->is_failure())
          $this->errException('ERR_API_INVALID_ARGUMENTS: the given ICCID cannot be activated', 'VV0066');
      }

      $params = array(
        "preferred_language"    => $preferred_language,
        "cos_id"                => get_cos_id_from_plan('STANDBY'),
        "activation_cos_id"     => get_cos_id_from_plan($target_plan),
        "postal_code"           => $zipcode,
        "country"               => 'USA',
        "plan_state"            => 'Neutral',
        "plan_started"          => 'NULL',
        "plan_expires"          => 'NULL',
        "customer_source"       => 'DEALER',
        "current_iccid"         => substr($iccid, 0, -1), // important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
        "current_iccid_full"    => $iccid, // important: this is not (yet) in ACTIVATION_ICCID (MVNO-517)
        'userid'                => 0,
        'promised_amount'       => get_plan_cost_by_cos_id(get_cos_id_from_plan($target_plan)),
        'masteragent'           => $sim->INVENTORY_MASTERAGENT,
        'distributor'           => $sim->INVENTORY_DISTRIBUTOR,
        'dealer'                => $activation_dealer->dealerId);

      $result = create_ultra_customer_db_transaction($params);
      if ( ! $customer = $result['customer'])
        $this->errException('ERR_API_INTERNAL: DB error - could not create customer', 'DB0001');
      $this->addToOutput('customer_id', $customer->CUSTOMER_ID);
      customer_reset_first_last_name($customer->CUSTOMER_ID);

      $result = func_courtesy_add_stored_value(array(
        'terminal_id'   => 0,
        'amount'        => substr($target_plan, -2),
        'customer'      => $customer,
        'reference'     => 0,
        'reason'        => 'dealer demo line activation',
        'detail'        => __FUNCTION__));
      if (count($result['errors']))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $result['errors'][0]);

      // save transition info
      $email = get_dealer_email_from_dealer_site_id($dealerId);
      set_redis_provisioning_values($customer->CUSTOMER_ID, $sim->INVENTORY_MASTERAGENT, $activation_dealer->dealerId, $sim->INVENTORY_DISTRIBUTOR, $activation_dealer->dealerId, 0, $redis);
      $context          = array(
        'customer_id'   => $customer->CUSTOMER_ID,
        'demo_line'     => $line->DEMO_LINE_ID,
        'email'         => $email ? $email : NULL,
        'activations'   => $line->ACTIVATIONS_NEEDED);
      $resolve_now      = FALSE;
      $dry_run          = TRUE;
      $plan             = get_plan_name_from_short_name($target_plan);
      $transition_name  = "Activate Dealer Demo $plan";

      // test State Transition
      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, 1);
      if ( ! $result_status['success'] )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);
        $this->errException( "ERR_API_INTERNAL: state transition error (1)" , 'SM0001' );
      }

      // initiate State Transition
      $dry_run = FALSE;
      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, 1);
      if ( ! $result_status['success'] )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);
        $this->errException( "ERR_API_INTERNAL: state transition error (2)" , 'SM0001' );
      }

      // update ULTRA.HTT_ACTIVATION_HISTORY
      $sql = ultra_activation_history_update_query(
        array(
          'customer_id'     => $customer->CUSTOMER_ID,
          'activation_type' => 'NEW'));

      if ( ! is_mssql_successful(logged_mssql_query($sql)))
        dlog('', 'ULTRA.HTT_ACTIVATION_HISTORY update failed');

      flush_ultra_activation_history_cache_by_customer_id($customer->CUSTOMER_ID);
      $this->addToOutput('request_id', $result_status['transitions'][0]);

      // insert the row into ULTRA.CUSTOMER_OPTIONS with OPTION_ATTRIBUTE = 'BILLING.MRC_DEALER_PROMO'
      if (empty($session_data['dealer_code']))
      {
        if ( ! $info = \Ultra\Lib\DB\Celluphone\getDealerInfo(NULL, $dealerId))
          $this->errException('ERR_API_INTERNAL: No celluphone dealer info found', 'ND0003');
        $session_data['dealer_code'] = $info->dealerCode;
      }
      if ( ! ultra_customer_options_insert(BILLING_OPTION_DEMO_LINE, $session_data['dealer_code'], $customer->CUSTOMER_ID))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001');

      $this->succeed ();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}


<?php
namespace Ultra\Lib\Api\Partner\Dealerportal;

use Session;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Lib\Api\Partner\Dealerportal;
use Ultra\Sims\Interfaces\SimRepository;

class ValidateGBASim extends Dealerportal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * ValidateGBASim constructor.
   * @param CustomerRepository $customerRepository
   * @param SimRepository $simRepository
   */
  public function __construct(CustomerRepository $customerRepository, SimRepository $simRepository)
  {
    $this->customerRepository = $customerRepository;
    $this->simRepository = $simRepository;

    parent::__construct();
  }

  /**
   * dealerportal__ValidateGBASim
   *
   * @param int customer_id
   * @return object Result
   */
  public function dealerportal__ValidateGBASim()
  {
    list ( $security_token , $iccid_full ) = $this->getInputValues();

    try
    {
      teldata_change_db();

      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      $sim = $this->simRepository->getSimInventoryAndBatchInfoByIccid($iccid_full);

      $this->addToOutput('valid', $sim->allowsWifiSoc());
      $this->succeed();
    }
    catch(InvalidObjectCreationException $e)
    {
      $this->addError($e->getMessage(), $e->code());
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

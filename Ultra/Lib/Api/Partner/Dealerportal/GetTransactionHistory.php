<?php
namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\Lib\Api\Partner\Dealerportal;
use Ultra\Billing\Interfaces\BillingHistoryRepository;
use Ultra\Customers\Interfaces\CustomerRepository;

/**
 * Class GetTransactionHistory
 * @package Ultra\Lib\Api\Partner\Dealerportal
 */
class GetTransactionHistory extends Dealerportal
{
  /**
   * @var
   */
  private $securityToken;

  /**
   * @var
   */
  private $customerId;

  /**
   * @var
   */
  private $dateFrom;

  /**
   * @var
   */
  private $dateTo;

  /**
   * @var array
   */
  private $transactionHistory = [];

  /**
   * @var
   */
  private $command;

  /**
   * @var BillingHistoryRepository
   */
  private $billingHistoryRepository;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * GetTransactionHistory constructor.
   * @param BillingHistoryRepository $billingHistoryRepository
   * @param CustomerRepository $customerRepository
   */
  public function __construct(BillingHistoryRepository $billingHistoryRepository, CustomerRepository $customerRepository)
  {
    $this->billingHistoryRepository = $billingHistoryRepository;
    $this->customerRepository       = $customerRepository;

    parent::__construct();
  }


  /**
   * dealerportal__GetTransactionHistory
   *
   * return customer's transaction history ( DB table HTT_BILLING_HISTORY )
   *
   * @param int customer_id
   * @param string date_from: date in DD-MM-YYYY in PST TZ
   * @param string date-to: date in DD-MM-YYYY in PST TZ
   * @author VYT, 14-03-03
   */
  public function dealerportal__GetTransactionHistory()
  {
    // init
    list ($security_token, $customer_id, $date_from, $date_to) = $this->getInputValues();

    $this->addToOutput('transaction_history', $this->transactionHistory);

    $this->init($security_token, $customer_id, $date_from, $date_to);
    $this->command = __FUNCTION__;

    try
    {
      $this->validateApi();

      // get billing history
      $history = $this->billingHistoryRepository->getBillingTransactionHistory($this->customerId, $this->dateFrom, $this->dateTo);

      // flatten the result into an array
      if (count($history['errors']) > 0) { $this->errException('ERR_API_INTERNAL: ' . $history['errors'][0], 'DB0001');}

      foreach($history['billing_transaction_history'] as $id => $data)
      {
        $this->transactionHistory[] = $data->history_epoch; // date returned in PST
        $this->transactionHistory[] = $data->order_id;
        $this->transactionHistory[] = $data->type;
        $this->transactionHistory[] = $data->history_source;
        $this->transactionHistory[] = $data->AMOUNT;
        $this->transactionHistory[] = $data->description;
      }

      $this->addToOutput('transaction_history', $this->transactionHistory);
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   * Setup class
   *
   * @param $securityToken
   * @param $customerId
   * @param $dateFrom
   * @param $dateTo
   */
  private function init($securityToken, $customerId, $dateFrom, $dateTo)
  {
    $this->securityToken = $securityToken;
    $this->customerId = $customerId;
    $this->dateFrom = $dateFrom;
    $this->dateTo = $dateTo;
  }

  /**
   * Validate API.
   *
   * @throws \Exception
   */
  private function validateApi()
  {
    // retrieve and validate security token
    list($session_data, $error_code, $error) = $this->getValidUltraSessionData($this->securityToken, $this->command);

    if ($error_code)
    {
      return $this->errException($error, $error_code);
    }

    $customer = $this->customerRepository->getCustomerById($this->customerId, ['plan_state']);

    if (!$customer)
    {
      return $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
    }

    if (!$this->dateFrom)
    {
      return $this->errException('ERR_API_INVALID_ARGUMENTS: missing date_from', 'MP0001');
    }

    if (!$this->dateTo)
    {
      return $this->errException('ERR_API_INVALID_ARGUMENTS: missing date_to', 'MP0001');
    }

    // check if the caller is allowed to execute this API
    if (!$this->checkUltraSessionAllowedAPICustomer($session_data, $this->customerId, $customer->plan_state, $this->command))
    {
      return $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');
    }

    // convert PST input dates to Unix epoch
    date_default_timezone_set('America/Los_Angeles');

    $this->dateFrom = strtotime($this->dateFrom);
    $this->dateTo = strtotime($this->dateTo);

    if (!$this->dateFrom || !$this->dateTo)
    {
      return $this->errException('ERR_API_INTERNAL: failed to interpret input dates', 'VV0035');
    }

    return null;
  }
}

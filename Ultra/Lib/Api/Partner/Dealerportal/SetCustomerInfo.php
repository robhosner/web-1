<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Messaging\Messenger;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class SetCustomerInfo extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * @var CreditCardRepository
   */
  private $cardRepository;

  public $customer;

  /**
   * SetCustomerInfo constructor.
   * @param Messenger $messenger
   * @param CreditCardRepository $cardRepository
   */
  public function __construct(Messenger $messenger, CreditCardRepository $cardRepository)
  {
    parent::__construct();
    $this->messenger = $messenger;
    $this->cardRepository = $cardRepository;
  }

  /**
   * dealerportal__SetCustomerInfo
   *
   * Update customer information to the DB.
   *
   * @return Result object
   */
  public function dealerportal__SetCustomerInfo ()
  {
    list (
      $security_token ,
      $customer_id ,
      $auto_recharge ,        // HTT_CUSTOMERS_OVERLAY_ULTRA.MONTHLY_CC_RENEWAL
      $first_name ,           // CUSTOMERS.FIRST_NAME
      $last_name ,            // CUSTOMERS.LAST_NAME
      $cc_name ,              // CUSTOMERS.CC_NAME
      $cc_address1 ,          // CUSTOMERS.CC_ADDRESS1
      $cc_address2 ,          // CUSTOMERS.CC_ADDRESS2
      $cc_city ,              // CUSTOMERS.CC_CITY
      $cc_country ,           // CUSTOMERS.CC_COUNTRY
      $cc_state_or_region ,   // CUSTOMERS.CC_STATE_REGION
      $cc_postal_code ,       // CUSTOMERS.CC_POSTAL_CODE
      $account_number_phone , // CUSTOMERS.LOCAL_PHONE
      $account_number_email , // CUSTOMERS.E_MAIL
      $account_cc_exp ,       // CUSTOMERS.CC_EXP_DATE
      $account_cc_cvv ,       // CUSTOMERS.CCV
      $account_cc_number ,    // tokenized
      $address1 ,             // CUSTOMERS.ADDRESS1
      $address2 ,             // CUSTOMERS.ADDRESS2
      $city ,                 // CUSTOMERS.CITY
      $country ,              // CUSTOMERS.COUNTRY
      $state_or_region ,      // CUSTOMERS.STATE_REGION
      $zipcode ,              // CUSTOMERS.POSTAL_CODE
      $preferred_language     // HTT_CUSTOMERS_OVERLAY_ULTRA.PREFERRED_LANGUAGE
    ) = $this->getInputValues();

    try
    {
      teldata_change_db(); // connect to the DB

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $customer = get_customer_from_customer_id( $customer_id );

      if ( ! $customer )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no customer found." , 'VV0031' );

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer( $session_data , $customer_id, $customer->plan_state, __FUNCTION__ ) )
        $this->errException( "ERR_API_INTERNAL: API execution not allowed for the given caller" , 'SE0005' );

      $this->customer = $customer;

      // prepare fields which will be updated ( TODO: OBSOLETE - TO REMOVE - MVNO-1779 )

      $fields = array();

      if ( $first_name           ) { $fields['account_first_name']      = $first_name; }
      if ( $last_name            ) { $fields['account_last_name']       = $last_name; }
      if ( $account_number_email ) { $fields['e_mail']                  = $account_number_email; }

      // update statements wrapped in a SQL transaction

      if ( ! start_mssql_transaction() )
        $this->errException( "ERR_API_INTERNAL: DB write error." , 'DB0001' );

      $fields_update_results = fields(
        $fields,
        $customer,
        FALSE,
        array(
          'f/ok_email_dup' => TRUE, // allow infinite duplicate emails
          'f/ok_cc_dup'    => TRUE  // allow infinite duplicate Credit Cards
        )
      );

      $all_validated     = FALSE;
      $all_db_queries_ok = TRUE;

      // validation error
      $validation_error = '';

      foreach( $fields_update_results as $result_id => $result ) // loop through result array
      {
        if ( is_array( $result ) )
        {
          if ( isset($result['all_validated'] ) )
          {
            if ( $result['all_validated'] )
            {
              $all_validated = TRUE;
            }
          }
          else if ( isset($result['validated'] ) && is_array($result['validated']) )
          {
            if (
              ( ! $result['validated']['ok'] )
              &&
              ( $result['validated']['validation_msg'] )
            )
            {
              dlog('',"validated = %s",$result['validated']);

              $error_code = $result['validated']['error_code'];

              $validation_error = "ERR_API_INVALID_ARGUMENTS: ".$result['validated']['validation_msg'];

              dlog('',"ERR_API_INVALID_ARGUMENTS: %s",$result['validated']['validation_msg']);
            }
          }
          else if ( isset($result['sql'] ) )
          {
            if ( $result['sql'][1] != 1 )
            {
              dlog('',"Database write error ( 1 )");

              $all_db_queries_ok = FALSE;
            }
          }
        }
      }

      if ( $preferred_language || ( $auto_recharge === '0' ) || ( $auto_recharge === '1' ) )
      {
        // htt_customers_overlay_ultra data

        $update_params = array(
          'customer_id'        => $customer->CUSTOMER_ID,
          'preferred_language' => $preferred_language
        );

        if ( ( $auto_recharge === '0' ) || ( $auto_recharge === '1' ) )
          $update_params['monthly_cc_renewal'] = $auto_recharge;

        // PROD-1068: set easypay_activted if auto rechange is enabled and recently activated with a credit card
        if ($auto_recharge)
        {
          if ( ! $redis = new \Ultra\Lib\Util\Redis)
            $this->errException('ERR_API_INTERNAL: Generic internal error', 'IN0002');
          if ($redis->get(self::EASYPAY_ACTIVATION_KEY . $customer->CUSTOMER_ID))
            $update_params['easypay_activated'] = 1;
        }

        $result = \Ultra\Lib\DB\Setter\Customer\ultra_info( $update_params );

        if ( $result->is_failure() )
        {
          rollback_mssql_transaction();

          $all_db_queries_ok = FALSE;

          $errors = $result->get_errors();

          $this->errException( $errors[0] , $result->data_array['error_codes'][0] );
        }
      }

      if ( $all_validated && $all_db_queries_ok )
      {
        if ( commit_mssql_transaction() )
          $success = TRUE;
        else
          $this->errException( "ERR_API_INTERNAL: DB write error." , 'DB0001' );
      }
      else if ( ! rollback_mssql_transaction() )
      {
        $this->errException( "ERR_API_INTERNAL: DB write error." , 'DB0001' );
      }

      if ( ! $all_validated )
      {
        $error_code = '------';

        switch ( $validation_error )
        {
          case "ERR_API_INVALID_ARGUMENTS: account_cc_number is not a valid credit card":
            $error_code = 'VV0063';
            break;
          default:
            dlog('',"Missing error code for -> ".$validation_error);
            break;
        }

        $this->errException( $validation_error , $error_code );
      }

      if ( ! $all_db_queries_ok )
        $this->errException( "ERR_API_INTERNAL: DB write error." , 'DB0001' );

      // update voicemail language if requested
      if ($preferred_language)
      {
        if ( ! mvneSetVoicemailLanguage($customer, $preferred_language))
          $this->addWarning('Failed to set voicemail language');
        teldata_change_db();
      }

      if ($auto_recharge) {
        $this->sendAutoRechargeSms();
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

// TODO: RBAC output filter

    return $this->result;
  }

  private function sendAutoRechargeSms()
  {
    $creditCardInfo = $this->cardRepository->getCcInfoFromCustomerId($this->customer->customer_id);
    $lastFour = empty($creditCardInfo->LAST_FOUR) ? '' : $creditCardInfo->LAST_FOUR;

    $result = $this->messenger->enqueueImmediateSms($this->customer->customer_id, 'auto_recharge_active', ['last_four' => $lastFour]);

    if ($result->is_failure())
    {
      return $this->errException('ERR_API_INTERNAL: SMS delivery error.', 'SM0002');
    }
  }
}

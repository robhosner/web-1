<?php
namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\CreditCards\CreditCard;
use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Dealerportal;
use Ultra\Lib\Util\Redis;
use Ultra\Utilities\Common;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Exceptions\MissingCreditCardException;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;

/**
 * Class ChargeAndSaveCustomerCC
 * @package Ultra\Lib\Api\Partner\Dealerportal
 */
class ChargeAndSaveCustomerCC extends Dealerportal
{
  /**
   * @var Common
   */
  private $commonUtilities;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Redis
   */
  protected $redis;

  /**
   * @var CreditCardRepository
   */
  private $creditCardRepository;

  /**
   * @var CreditCard
   */
  private $creditCard;

  /**
   * ChargeAndSaveCustomerCC constructor.
   * @param Common $commonUtilities
   * @param CustomerRepository $customerRepository
   * @param Redis $redis
   * @param CreditCardRepository $creditCardRepository
   */
  public function __construct(
    Common $commonUtilities,
    CustomerRepository $customerRepository,
    Redis $redis,
    CreditCardRepository $creditCardRepository,
    FeatureFlagsClientWrapper $flagsObj
  ) {
    $this->commonUtilities = $commonUtilities;
    $this->customerRepository = $customerRepository;
    $this->redis = $redis;
    $this->flagsObj = $flagsObj;

    parent::__construct();
    $this->creditCardRepository = $creditCardRepository;
  }

  /**
   * dealerportal__ChargeAndSaveCustomerCC
   *
   * Tokenizes a card so that it can be used for a charge. Attempts charge. Removes CC info from DB if customer doesn't wish to save it.
   *  - $charge_amount goes to balance
   *  - non-taxable
   *  - attempts to transition the customer to Active
   * $charge_amount is in cents
   *
   * @return Result object
   */
  public function dealerportal__ChargeAndSaveCustomerCC()
  {
    list ($security_token, $customer_id, $remove_card, $charge_amount) = $this->getInputValues();

    try {
      // connect to the DB
      $this->commonUtilities->teldataChangeDb();

      // retrieve and validate session from $security_token
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);

      if ($error_code) {
        return $this->errException($error, $error_code);
      }

      // get customer data
      $customer = $this->customerRepository->getCombinedCustomerByCustomerId($customer_id);

      if (!$customer) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }

      // check if the caller is allowed to execute this API
      if (!$this->checkUltraSessionAllowedAPICustomer($session_data, $customer->customer_id, $customer->plan_state, __FUNCTION__)) {
        return $this->errException("ERR_API_INTERNAL: API execution not allowed for the given caller", 'SE0005');
      }

      // charge customer's credit card and attempt to transition the customer to Active
      $this->creditCard = $this->creditCardRepository->getCCInfoAndTokenByCustomerId($customer_id);
      if (empty($this->creditCard)) {
        throw new MissingCreditCardException('No credit card token found', 'CC0005');
      }

      $disableFraudCheck = $this->flagsObj->disableRateLimits($customer_id);
      $result = $this->creditCard->initAddBalanceFromTokenizedCreditCard(
        $customer,
        ($charge_amount / 100),
        __FUNCTION__,
        'Initial Activation Charge',
        'Initial Activation Charge',
        $this->getRequestId(),
        $disableFraudCheck,
        0
      );

      if ($result->is_failure()) {
        $error_code = 'DB0001';
        $errors = $result->get_errors();

        if (isset($result->data_array['error_codes']) && count($result->data_array['error_codes'])) {
          $error_code = $result->data_array['error_codes'][0];
        }

        return $this->errException($errors[0], $error_code);
      }

      if (!in_array($customer->plan_state, [STATE_ACTIVE, STATE_PORT_IN_DENIED, STATE_SUSPENDED])) {
        $this->redis->set(self::EASYPAY_ACTIVATION_KEY . $customer->customer_id, TRUE, self::EASYPAY_ACTIVATION_PERIOD);
      }

      // removes CC info from DB if customer doesn't wish to save it.
      if ($remove_card) {
        $result = $this->creditCard->disableCreditCard();

        if ($result->is_failure()) {
          return $this->errException($result->get_errors()[0], 'DB0001');
        }
      }

      $this->succeed();
    } catch(CustomErrorCodeException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), $e->code(), $e->getMessage());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

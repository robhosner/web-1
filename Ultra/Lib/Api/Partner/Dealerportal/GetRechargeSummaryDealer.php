<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class GetRechargeSummaryDealer extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__GetRechargeSummaryDealer
   *
   * API output will contain an array, recharge_periods. This output array should return info from all recharge_periods in ULTRA.SUMMARY_DEALER_RECHARGE_RATE
   *
   * @return Result object
   */
  public function dealerportal__GetRechargeSummaryDealer ()
  {
    list ( $security_token ) = $this->getInputValues();

    $this->addToOutput('recharge_periods', array());

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      // is the result already cached?
      $redis = new \Ultra\Lib\Util\Redis;
      $cached_summary_dealer_recharge_data = $redis->get( 'ultra/summary_dealer_recharge_data/' . $session_data['dealer'] );

      if ( $cached_summary_dealer_recharge_data )
        $cached_summary_dealer_recharge_data = json_decode( $cached_summary_dealer_recharge_data );

      if ( $cached_summary_dealer_recharge_data )
        $this->addToOutput('recharge_periods',$cached_summary_dealer_recharge_data);
      else
      {
        // get all relevant dealer_ids due to the parent-child relationship between dealers
        $dealer_ids = get_parent_dealer_children( $session_data['dealer'] );

        // get data from ULTRA.SUMMARY_DEALER_RECHARGE_RATE
        $summary_dealer_recharge_data = get_summary_dealer_recharge_rate_by_dealers( array_merge( array( $session_data['dealer'] ) , $dealer_ids ) );

        if ( count($summary_dealer_recharge_data) )
          $this->addToOutput('recharge_periods',$summary_dealer_recharge_data);

        // cache result in Redis for 30 minutes
        $redis->set( 'ultra/summary_dealer_recharge_data/' . $session_data['dealer'] , json_encode( $summary_dealer_recharge_data ) , 30 * 60 );
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
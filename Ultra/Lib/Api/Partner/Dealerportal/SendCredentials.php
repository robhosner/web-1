<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class SendCredentials extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__SendCredentials
   * send forgotton username or temporary password
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__SendCredentials()
  {
    // initialization
    $params = $this->getNamedInputValues(); // username, email, business_type, business_code

    try
    {
      // branch on input
      if ($params['username'] || $params['email'])
      {
        // get user record
        list($users, $error) = \Ultra\Lib\DB\DealerPortal\getUser($params);
        dlog('', 'users: %s, error: %s', $users, $error);
        if ( ! count($users) || $error)
          $this->errException('API_INVALID_ARGUMENTS: No data found', 'ND0001', $error);

        // validate user record
        $validateFunc  = ($params['email']) ? 'validateEmail'  : 'validateUser';
        $validateFunc  = "\\Ultra\\Lib\\DB\\DealerPortal\\" . $validateFunc;
        $validateParam = ($params['email']) ? $params['email'] : $params['username'];

        list($validatedUser, $error) = $validateFunc($validateParam);
        if ( ! $validatedUser)
        {
          dlog('', "WARNING: $error");
          $this->errException('ERR_API_INTERNAL: Invalid login information', 'SE0007', $error);
        }

        // DEAL-150: only send to the most recently created user if multiple users are found
        $user = $users[0];

        // PROD-1808: check if user is active
        if ( ! $user['active'])
          $this->errException('API_INVALID_ARGUMENTS: Invalid login information', 'SE0007');

        // common email tempate parameters
        $params['first_name'] = $user['first_name'];
        $params['last_name'] = $user['last_name'];
        $params['email'] = trim($user['email']);

        // when username is given we generate and send a temporary password
        if ($params['username'])
        {
          $params['password'] = func_create_temp_password($params['username'], self::TEMP_PASSWORD_KEY);
          $template = 'dealer-forgot-password';
        }
        else // when email is given we send forgotten username
        {
          $params['username'] = $user['username'];
          $template = 'dealer-send-username';
        }
      }
      elseif ($params['business_type'] || $params['business_code'])
      {
        // get business owner
        list($owner, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessOwnerOld($params);

        // PROD-1808: check if user is active
        if ( ! $owner->is_active_flag)
          $this->errException('API_INVALID_ARGUMENTS: Your retailer code is inactive. Please contact your Master Agent to get reinstated and begin selling Ultra Mobile again.', 'SE0007');

        // common parameters
        $params['business_name'] = $owner->BusinessName;
        $params['email'] = array(trim($owner->email_address));
        $params['username'] = $owner->user_name;
        $params['password'] = $owner->password;

        if ($params['business_type'] == 'Master')
          $template = 'dealer-new-master';
        else // Dealer
        {
          $template = 'dealer-new-retailer';

          // PROD-1848: get dealer's master name
          list($master, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessRecord('Master', NULL, $owner->MasterID);
          if ($error)
            $this->errException('ERR_API_INTERNAL: No celluphone master info found', 'ND0004');

          // PROD-2262: get dealer's master details [email to CC]
          list($masterDetails, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessOwnerOld(array(
            'business_type' => 'Master', 
            'business_code' => $master->MASTERCD
          ));
          if ($error)
            $this->errException('ERR_API_INTERNAL: celluphone master details not found', 'ND0004');

          $params['master_name'] = $master->BUSINESSNAME;

          $params['email'][] = $masterDetails->email_address;
          $params['email'][] = RETAILER_CC_EMAIL;
        }

      }
      else
        $this->errException('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing', 'MP0001');

      // email is nullable in DB but required to proceeed
      if (empty($params['email']))
        $this->errException('API_INVALID_ARGUMENTS: The given e-mail address is not valid', 'EM0001', 'No email address on file');

      // send email
      if ($error = \Ultra\Messaging\Email\DealerPortal\sendTemplateEmail($template, $params))
        $this->errException('ERR_API_INTERNAL: Generic internal error', 'IN0002', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';
require_once 'classes/LineCredits.php';

class GetPlanCreditBalance extends \Ultra\Lib\Api\Partner\Dealerportal
{
  public function __construct()
  {
    parent::__construct();
  }

  public function dealerportal__GetPlanCreditBalance()
  {
    list ($security_token, $customerId) = $this->getInputValues();

    try
    {
      // validate dealer portal session
      list ($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code) $this->errException($error, $error_code);

      $lineCredits = new \LineCredits();
      $this->addToOutput('credits', $lineCredits->getBalance($customerId));

      $this->succeed();
    }
    catch (\Exception $e)
    {
      \dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

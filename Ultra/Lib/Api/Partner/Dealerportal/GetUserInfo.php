<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class GetUserInfo extends \Ultra\Lib\Api\Partner\Dealerportal
{
/**
   * dealerportal__GetUserInfo
   *
   * Retrieves detailed info for a given user. Requires a valid security token. Must be logged in as master agent, dealer or user self.
   *
   * @return Result object
   */
  
  public function dealerportal__GetUserInfo()
  {
    list ( $security_token , $user_id ) = $this->getInputValues();
    $output = array(
      'user_id'         => NULL,
      'first_name'      => NULL,
      'last_name'       => NULL,
      'phone_number'    => NULL,
      'email'           => NULL,
      'username'        => NULL,
      'note'            => NULL,
      'active'          => NULL,
      'effective_date'  => NULL,
      'expiration_date' => NULL,
      'access_role'     => NULL);

    try
    {
      // retrieve and validate session
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get use data
      list($users, $error) = \Ultra\Lib\DB\DealerPortal\getUser(array('user_id' => $user_id));
      if ($error)
        $this->errException('API_INVALID_ARGUMENTS: No data found', 'IN0002', $error);
      if (count($users) > 1)
        $this->errException('API_INVALID_ARGUMENTS: No data found', 'IN0002', 'Multile records found');

      // success
      $user = $users[0];
      foreach ($output as $name => $value)
        if ( ! empty($user[$name]))
          $output[$name] = $user[$name];
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    $this->addArrayToOutput($output);
    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class UpdateUser extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__UpdateUser
   * update given user info
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__UpdateUser()
  {
    // initialize
    $parameters = (object)$this->getNamedInputValues();
    $this->addToOutput('user_id', NULL);

    try
    {
      // retrieve and validate session from security token
      list($session, $errorCode, $errorMessage) = $this->getValidUltraSessionData($parameters->security_token, __FUNCTION__);
      if ($errorCode)
        $this->errException($errorMessage, $errorCode);

      // update user
      if ($errorMessage = \Ultra\Lib\DB\DealerPortal\updateUser($session['user_id'], $parameters))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0003', $errorMessage);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

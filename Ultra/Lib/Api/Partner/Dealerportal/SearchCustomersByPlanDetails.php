<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class SearchCustomersByPlanDetails extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__SearchCustomersByPlanDetails
   *
   * Search for subscribers based on plan details.
   * - If the caller role is Dealer, the customers returned should have been activated by this dealer
   *
   * @return Result object
   */
  public function dealerportal__SearchCustomersByPlanDetails ()
  {
    list ( $security_token , $page_size , $page_ord , $plan_state , $date_from , $date_to , $sort_by ) = $this->getInputValues();

    if ( ! $page_size ) $page_size = 100;
    if ( ! $page_ord  ) $page_ord  = 1;

    $this->addToOutput('page_size',$page_size);
    $this->addToOutput('page_ord',$page_ord);
    $this->addToOutput('record_count','');
    $this->addToOutput('customers',array());

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $redis = new \Ultra\Lib\Util\Redis;

      // validate $sort_by
      if ( is_array($sort_by) && count($sort_by) )
      {
        $invalid = $this->validate_search_sort_by( $sort_by );

        if ( $invalid )
          $this->errException( "ERR_API_INVALID_ARGUMENTS: invalid sorting parameters." , 'VV0036' );
      }
      else
      {
        $sort_by_plan_state_map = array(
          "Neutral"           => array( 'CREATION_DATE_TIME desc' ),
          "Cancelled"         => array( 'DEACTIVATION_DATE  desc' ),
          "Active"            => array( 'PLAN_STARTED       desc' ),
          "Provisioned"       => array( 'CREATION_DATE_TIME desc' ),
          "Suspended"         => array( 'PLAN_EXPIRES       desc' ),
          "Port-in Attempted" => array( 'PORT_STATUS_DATE   desc','CREATION_DATE_TIME desc' ),
          "Port-In Requested" => array( 'PORT_STATUS_DATE   desc','CREATION_DATE_TIME desc' ),
          "Port-In Denied"    => array( 'PORT_STATUS_DATE   desc' ),
          "Pre-Funded"        => array( 'CREATION_DATE_TIME desc' ),
          "Promo Unused"      => array( 'CREATION_DATE_TIME desc' )
        );

        $sort_by = $sort_by_plan_state_map[ $plan_state ];
      }

      $search_array = array(
        'plan_state' => $plan_state,
        'date_from'  => $date_from,
        'date_to'    => $date_to,
        'sort_by'    => $sort_by
      );

      // Role Context Check : currently we only support ROLE_NAME_ADMIN , ROLE_NAME_DEALER_OWNER , ROLE_NAME_DEALER_EMPLOYEE
      if (in_array($session_data['role'], array(self::ROLE_NAME_DEALER_OWNER, self::ROLE_NAME_DEALER_MANAGER, self::ROLE_NAME_DEALER_EMPLOYEE)))
        $search_array['dealer'] = $session_data['dealer'];

      dlog('',"search_array = %s",$search_array);

      $search_key = md5( json_encode ( $search_array ) );

      $redis_key = 'ultra/api/dealerportal/cache/' . __FUNCTION__ . '/' . $search_key;

      dlog('',"redis_key = $redis_key");

      // get data from cache

      $cached_result = $redis->get( $redis_key );

      if ( $cached_result )
      {
        // no need to query the DB

        $decoded_result = json_decode( $cached_result );

        // extract only the needed data

        $paginated_output = paginate_output( $decoded_result , $page_size , $page_ord );

        if ( ! count( $paginated_output ) )
        {
          $this->addToOutput('record_count',0);

          $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
        }

        $this->addToOutput('customers',$paginated_output);
        $this->addToOutput('record_count',count( $decoded_result ));
      }
      else
      {
        // get data from DB

        // DEACTIVATION_DATE is obtained from DEACTIVATION_DATE only if the given $plan_state is 'Cancelled'
        $cancelled_date_column = "''";
        if ( $plan_state == 'Cancelled' )
          $cancelled_date_column = "CASE WHEN DEACTIVATION_DATE  IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', DEACTIVATION_DATE  ) END";

// Port info must be obtained from Redis
        $search_array['select_attributes'] =
          array(
            $cancelled_date_column." cancelled_date",
            "CASE WHEN CREATION_DATE_TIME IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', CREATION_DATE_TIME ) END created_date",
            "CASE WHEN PLAN_EXPIRES       IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', PLAN_EXPIRES       ) END service_expires_date",
            "CASE WHEN port_status_date   IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', port_status_date   ) END port_status_date",
            "CASE WHEN plan_started       IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', plan_started       ) END plan_started",
            'u.CUSTOMER_ID         customer_id',
            'CURRENT_MOBILE_NUMBER msisdn',
            'CURRENT_ICCID_FULL    iccid',
            'E_MAIL                email',
            'first_name',
            'last_name',
            'a.COS_ID',
            'plan_state',
            'balance',
            'stored_value',
            'monthly_cc_renewal',
            'p.port_status',
            'p.port_query_status',
          );

        $customers_search_result = find_ultra_customers_from_plan_and_date_range( $search_array );

        if ( ( $customers_search_result->is_failure() )
          || ( ( ! $customers_search_result->data_array ) || ( ! count( $customers_search_result->data_array ) ) )
        )
        {
          $this->addToOutput('record_count',0);

          $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
        }
        else
        {
          // extract only the needed data

          $paginated_output = paginate_output( $customers_search_result->data_array , $page_size , $page_ord );

          if ( ! count( $paginated_output ) )
          {
            $this->addToOutput('record_count',0);

            $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
          }

          $this->addToOutput('customers',$paginated_output);
          $this->addToOutput('record_count',count($customers_search_result->data_array));

          // cache result in Redis for 30 seconds
          $redis->set( $redis_key , json_encode( $customers_search_result->data_array ) , 30 );
        }
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

// TODO: RBAC output filter

    return $this->result;
  }
}
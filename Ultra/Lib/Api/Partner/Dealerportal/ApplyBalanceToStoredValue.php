<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ApplyBalanceToStoredValue extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ApplyBalanceToStoredValue
   *
   * transfer some of customer's wallet balance to stored_value
   *
   * @author VYT, 14-03-03
   */
  public function dealerportal__ApplyBalanceToStoredValue()
  {
    // init
    list ($security_token, $customer_id, $amount) = $this->getInputValues();

    try
    {
      // retrieve and validate security token
      teldata_change_db();
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // get customer
      $customer = get_customer_from_customer_id($customer_id);
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // check that customer is in allowed plan state
      $state = internal_func_get_state_from_customer_id($customer_id);
      if ( ! $state || empty($state['state']))
        $this->errException('ERR_API_INTERNAL: failed to determine customer plan state', 'UN0001');
      if ($state['state'] != 'Active' && $state['state'] != 'Suspended')
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');

      // prepare parameters: optional values we could use are currently set to NULL
      $params = array(
        'customer'      => $customer,
        'sweep_value'   => $amount / 100, // amount parameter is in cents
        'reference'     => 'BalanceToStoredValue',
        'source'        => 'BalanceToStoredValue',
        'store_zipcode' => NULL,
        'store_id'      => NULL,
        'clerk_id'      => NULL,
        'terminal_id'   => NULL);

      // move amount from balance to stored value
      $result = func_sweep_balance_to_stored_value($params);
      if (! $result['success'])
      {
        foreach ($result['errors'] as $error)
          $this->addError($error, 'VV0001');
        $this->errException('ERR_API_INTERNAL: failed to transfer amount to stored value', 'DB0001');
      }

      // all is well if are here
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage ());
    }

    return $this->result;
  }
}
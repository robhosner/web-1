<?php
namespace Ultra\Lib\Api\Partner\Dealerportal;

use Ultra\Accounts\Account;
use Ultra\Accounts\Interfaces\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Dealerportal;
use Ultra\Mvne\Adapter;

class UpdateThrottleSpeed extends Dealerportal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  private $customer;

  /**
   * @var Adapter
   */
  private $adapter;

  /**
   * @var AccountsRepository
   */
  private $accountsRepository;

  /**
   * @var Account
   */
  private $account;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * UpdateThrottleSpeed constructor.
   * @param CustomerRepository $customerRepository
   * @param Adapter $adapter
   * @param AccountsRepository $accountsRepository
   * @param Configuration $configuration
   */
  public function __construct(
    CustomerRepository $customerRepository,
    Adapter $adapter,
    AccountsRepository $accountsRepository,
    Configuration $configuration
  )
  {
    $this->customerRepository = $customerRepository;
    parent::__construct();
    $this->adapter = $adapter;
    $this->accountsRepository = $accountsRepository;
    $this->configuration = $configuration;
  }

  public function dealerportal__UpdateThrottleSpeed()
  {
    list($security_token, $customer_id, $targetSpeed) = $this->getInputValues();

    try
    {
      // retrieve and validate session
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);

      if ($error_code) {
        return $this->errException($error, $error_code);
      }

      if (!$this->customer = $this->customerRepository->getCustomerById($customer_id, [], true)) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found', 'VV0031');
      }

      if (!$this->account = $this->accountsRepository->getAccountFromCustomerId($customer_id, ['COS_ID'])) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found', 'VV0031');
      }

      try {
        if ($this->customer->canUpdateToThrottleSpeed($targetSpeed)) {
          $result = $this->adapter->mvneMakeitsoUpdateThrottleSpeed(
            $customer_id,
            $this->configuration->getPlanFromCosId($this->account->cos_id),
            $targetSpeed,
            'LTE_SELECT.DEALER_PORTAL'
          );

          if (!$result['success']) {
            return $this->errException('The mvneMakeitsoUpdateThrottleSpeed result failed.', 'IN0002', 'The mvneMakeitsoUpdateThrottleSpeed result failed.');
          }
        } else {
          return $this->errException('unable to update to throttle speed ' . $targetSpeed, 'IN0002', 'unable to update to throttle speed ' . $targetSpeed);
        }
      } catch (\Exception $e) {
        return $this->errException('ERR_API_INTERNAL: ' . $e->getMessage(), 'IN0002', $e->getMessage());
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

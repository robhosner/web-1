<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class DeleteMiscellaneousAdjustment extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__DeleteMiscellaneousAdjustments
   * Deletes a misc adjustment from the database
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__DeleteMiscellaneousAdjustment()
  {
    // initialization
    $params = $this->getNamedInputValues();

    try
    {
      // retrieve and validate session from security token
      list($session, $errorCode, $errorMessage) = $this->getValidUltraSessionData($params['security_token'], __FUNCTION__);
      if ($errorCode)
        $this->errException($errorMessage, $errorCode);

      if ($error = \Ultra\Lib\DB\DealerPortal\deleteMiscellaneousAdjustment($params['adjustment_id']))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

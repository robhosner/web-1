<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';
require_once 'classes/LineCredits.php';
require_once 'classes/Flex.php';

class AddPlanCredits extends \Ultra\Lib\Api\Partner\Dealerportal
{
  public function __construct()
  {
    parent::__construct();
  }

  public function dealerportal__AddPlanCredits()
  {
    list ($security_token, $customerId, $amount) = $this->getInputValues();

    try
    {
      // validate dealer portal session
      list ($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code) $this->errException($error, $error_code);

      // connects to line credit database
      $lineCredits = new \LineCredits();

      // do line credit transfer
      list ($result, $error_code) = $lineCredits->doIncrement($customerId, $amount);
      if ( ! $result->is_success())
      {
        $errors = $result->get_errors();
        $this->errException("API_INVALID_ARGUMENTS: {$errors[0]}", $error_code);
      }

      $result = \Ultra\Lib\Flex::activateFamilyMembers($customerId);
      // ignore result

      $this->result->add_warnings($result->get_warnings());
      $this->succeed();
    }
    catch (\Exception $e)
    {
      \dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ListCustomerICCID extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ListCustomerICCID
   *
   * Search for subscribers by ICCID.
   * Open to all access roles.
   *
   * @return Result object
   */
  public function dealerportal__ListCustomerICCID ()
  {
    list ( $security_token , $ICCID ) = $this->getInputValues();

    $this->addArrayToOutput([
      'customer_id'          => '',
      'msisdn'               => '',
      'iccid'                => '',
      'created_date'         => '',
      'last_name'            => '',
      'first_name'           => '',
      'email'                => '',
      'plan_name'            => '',
      'plan_state'           => '',
      'balance'              => '',
      'stored_value'         => '',
      'plan_started'         => '',
      'service_expires_date' => '',
      'monthly_cc_renewal'   => '',
      'cancelled_date'       => '',
      'port_status'          => '',
      'port_query_status'    => '',
      'port_status_date'     => '',
      'has_access'           => FALSE
    ]);

    $has_access = FALSE;

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $select_attributes = "
        a.CUSTOMER_ID,
        current_mobile_number,
        CURRENT_ICCID_FULL,
        LAST_NAME,
        FIRST_NAME,
        E_MAIL,
        a.COS_ID,
        plan_state,
        BALANCE,
        stored_value,
        monthly_cc_renewal,
        u.BRAND_ID,
        CASE WHEN CREATION_DATE_TIME IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', CREATION_DATE_TIME ) END CREATION_DATE_EPOCH,
        CASE WHEN plan_expires       IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', plan_expires       ) END plan_expires_epoch,
        CASE WHEN plan_started       IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', plan_started       ) END plan_started_epoch
      ";

      // load customer from DB
      $customer = get_customer_from_iccid( $ICCID , $select_attributes );
      if ( ! $customer )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: no customer found." , 'VV0031' );

      if (in_array($customer->BRAND_ID, [3]))
        $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

      // get access level to customer
      $has_access = $this->checkUltraSessionAllowedAPICustomer($session_data, $customer->CUSTOMER_ID, $customer->plan_state, __FUNCTION__);

      if (empty($customer->BRAND_ID)) {
        $customer->BRAND_ID = get_brand_id_from_iccid($customer->CURRENT_ICCID_FULL);
      }

      $this->addArrayToOutput(array(
        'customer_id'          => $customer->CUSTOMER_ID,
        'msisdn'               => $customer->current_mobile_number,
        'iccid'                => $customer->CURRENT_ICCID_FULL,
        'created_date'         => $customer->CREATION_DATE_EPOCH,
        'last_name'            => $customer->LAST_NAME,
        'first_name'           => $customer->FIRST_NAME,
        'email'                => $customer->E_MAIL,
        'plan_name'            => get_plan_from_cos_id( $customer->COS_ID ),
        'plan_state'           => $customer->plan_state,
        'balance'              => $customer->BALANCE,
        'stored_value'         => $customer->stored_value,
        'plan_started'         => $customer->plan_started_epoch,
        'service_expires_date' => $customer->plan_expires_epoch,
        'monthly_cc_renewal'   => ( ! ! $customer->monthly_cc_renewal ) ,
        'has_access'           => $has_access,
        'brand'                => ucfirst(strtolower(\Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID))),
      ));

      if ( $customer->current_mobile_number &&
         ( ( $customer->plan_state == 'Port-In Requested' ) || ( $customer->plan_state == 'Port-In Denied' ) )
      )
      {
        // Port Info

        $redis = new \Ultra\Lib\Util\Redis;

        $port_status       = $redis->get( 'ultra/port/status/'            . $customer->current_mobile_number );
        $port_query_status = $redis->get( 'ultra/port/port_query_status/' . $customer->current_mobile_number );
        $port_status_date  = $redis->get( 'ultra/port/update_timestamp/'  . $customer->current_mobile_number );

        // Maybe the cache expired? Let's check the DB.
        if ( !$port_status && !$port_query_status && !$port_status_date )
        {
            $portInQueue = new \PortInQueue();

            $loadPortInQueueResult = $portInQueue->loadByCustomerId( $customer->CUSTOMER_ID );

            if ( $loadPortInQueueResult->is_success() && $portInQueue->portin_queue_id )
            {
              list(
                $port_success,
                $port_pending,
                $port_status,
                $port_resolution
              ) = interpret_port_status( $portInQueue );

              $provstatus_error_code = property_exists( $portInQueue , 'provstatus_error_code' ) ? $portInQueue->provstatus_error_code : '' ;
              $provstatus_error_msg  = property_exists( $portInQueue , 'provstatus_error_msg'  ) ? $portInQueue->provstatus_error_msg  : '' ;

              $port_query_status = $provstatus_error_code . '-' . $provstatus_error_msg;

              if ( property_exists( $portInQueue , 'updated_seconds_ago' ) && $portInQueue->updated_seconds_ago )
                $port_status_date = $portInQueue->updated_seconds_ago;
            }

            teldata_change_db();
        }

        $this->addToOutput('port_status',      $port_status);
        $this->addToOutput('port_query_status',$port_query_status);
        $this->addToOutput('port_status_date', $port_status_date);
      }

      if ( $customer->plan_state == 'Cancelled' )
      {
        $cancelled_date_epoch = get_cancelled_date_epoch( $customer->CUSTOMER_ID );

        if ( $cancelled_date_epoch )
          $this->addToOutput('cancelled_date',$cancelled_date_epoch);
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    // RBAC output filter
    if (! $has_access)
      $this->removeRestrictedOutput();

    return $this->result;
  }
}


<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class UpdateBusiness extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__UpdateBusiness
   * update an existing business entity
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__UpdateBusiness()
  {
    // initialization
    $parameters = $this->getNamedInputValues();
    $addresses = NULL;

    try
    {
      // retrieve and validate session from security token
      list($session, $errorCode, $errorMessage) = $this->getValidUltraSessionData($parameters['security_token'], __FUNCTION__);
      if ($errorCode)
        $this->errException($errorMessage, $errorCode);

      // dealer specific logic
      if ($parameters['business_type'] == 'Dealer')
      {
        // DEAL-226: Dealer active/inactive logic
        if ($parameters['active'] == '0')
        {
          if (empty($parameters['inactive_reason_id']))
            $this->errException('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing', 'MP0001', 'Missing required invactive_reason');
          $parameters['inactive_date'] = time();
        }
        elseif ($parameters['active'] == '1')
        {
          $parameters['inactive_reason_id'] = 'NULL';
          $parameters['inactive_date'] = 'NULL';
        }
      }

      // validate address if provided
      if ($parameters['street'] || $parameters['city'] || $parameters['state'] || $parameters['zipcode'])
      {
        // all fields are required when updating address
        if (empty($parameters['street']) || empty($parameters['city']) || empty($parameters['state']) || empty($parameters['zipcode']))
          $this->errException('ERR_API_INVALID_ARGUMENTS: Address (1) is not valid', 'VV0053', 'All address fields are required');

        if ($parameters['validate'])
        {
          // fail API if validation was requested and failed
          list($addresses, $error) = \Ultra\Lib\Util\validateStreetAddress($parameters);
          if ($error)
            $this->errException('ERR_API_INVALID_ARGUMENTS: Address (1) is not valid', 'VV0047', $error);

          // validation succeeded: copy corrected address
          foreach ($addresses[0] as $name => $value)
            if (array_key_exists($name, $parameters))
              $parameters[$name] = $value ? $value : 'NULL';
          dlog('', 'updated parameters: %s', $parameters);
        }
      }

      // update business
      $parameters['author_id'] = $session['user_id'];
      $parameters['modification_date'] = 'NOW';
      if ($error = \Ultra\Lib\DB\DealerPortal\updateBusiness($parameters))
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('address_matches', $addresses);
    return $this->result;
  }
}

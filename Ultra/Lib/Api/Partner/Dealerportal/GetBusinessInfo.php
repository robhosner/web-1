<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class GetBusinessInfo extends \Ultra\Lib\Api\Partner\Dealerportal
{
/**
   * dealerportal__GetBusinessInfo
   *
   * Returns details for the given business. Requires a valid security token.
   * Must be logged in either as administrator or master agent or self dealer.
   *
   * @return Result object
   */
  
  public function dealerportal__GetBusinessInfo()
  {
    // initialization
    $params = $this->getNamedInputValues();
    $result = array();
    $output = array('business_type', 'business_code', 'business_name', 'business_id', 'active', 'parent_business_type', 'parent_business_code', 'emida_id', 'epay_id', 'tmo_id','note', 'first_name', 'last_name', 'phone_number', 'email', 'effective_date', 'expiration_date', 'street', 'suite', 'city','state', 'zipcode', 'latitude', 'longitude', 'store_locator', 'inactive_reason', 'inactive_date', 'product_type', 'product_description', 'master_id', 'master_business_code', 'master_business_name');

    try
    {
      // retrieve and validate session
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($params['security_token'], __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // get business info
      $params['user_id'] = $session_data['user_id'];
      list($business, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessInfo($params);
      if ($error)
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      // success
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    // extract needed parameters since getBusinessInfo may return a bit more than we need
    foreach ($output as $key)
      $result[$key] = isset($business[$key]) ? $business[$key] : NULL;
    $this->addArrayToOutput($result);

    return $this->result;
  }
}

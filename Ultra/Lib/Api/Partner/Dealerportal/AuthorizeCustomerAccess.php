<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class AuthorizeCustomerAccess extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__AuthorizeCustomerAccess
   *
   * validates temp SMS password and creates a session for dealer to access a customer
   *
   * @param int customer_id
   * @param string password preiously sent via dealerportal__SendCustomerPasswordSMS
   * @return Result object
   * @author VYT, 14-02-18
   */
  public function dealerportal__AuthorizeCustomerAccess()
  {
    // init
    list ($security_token, $customer_id, $given_password) = $this->getInputValues();

    try
    {
      // retrieve and validate security token
      teldata_change_db();
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($security_token, __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // get customer
      $account = get_account_from_customer_id($customer_id , array('ACCOUNT'));
      if (! $account)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      // get customer state
      $state = internal_func_get_state_from_customer_id($customer_id);
      if ( ! $state )
        $this->errException('ERR_API_INTERNAL: customer state could not be determined' , 'UN0001' );

      // reject Cancelled customers
      if ( $state['state'] == 'Cancelled' )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer account has been cancelled', 'IN0001');

      // check for fraud
      if (fraud_detect_customer_login())
        $this->errException('ERR_API_ACCESS_DENIED: please try again later', 'AP0001');

      // get stored temp password
      $stored_password = func_read_temp_password($customer_id);
      if (! $stored_password)
        $this->errException('ERR_API_INTERNAL: login session expired', 'SE0003');

      // verify password
      if ($stored_password != $given_password)
        $this->errException('ERR_API_INVALID_ARGUMENTS: incorrect login information', 'SE0007');

      // create customer session
      $ultrasession = new \Ultra\Lib\Util\Redis\UltraSession;
      $customer_token = $ultrasession->addObject(array(
        'customer_id' => $customer_id,
        'account'     => $account->ACCOUNT,
        'role'        => 'Customer',
        'masteragent' => $session_data['masteragent'],
        'distributor' => $session_data['distributor'],
        'dealer'      => $session_data['dealer']));
      if (! $customer_token)
        $this->errException('ERR_API_INTERNAL: Could not generate a security token.' , 'SE0001');

      // set cookie
      if (! setcookie(parent::SUBSCRIBER_COOKIE_NAME . $customer_id, $customer_token, time() + parent::SUBSCRIBER_COOKIE_TTL))
        dlog('', 'ERROR: failed to set cookie');

      // all done
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
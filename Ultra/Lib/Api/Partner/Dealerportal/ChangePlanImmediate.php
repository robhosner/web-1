<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class ChangePlanImmediate extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ChangePlanImmediate
   *
   * change the plan of a given customer
   *
   * @param: customer_id
   * @param: target_plaln
   * @return Result object
   * @author: VYT, 14-01-27, adopted from v1
   */
  public function dealerportal__ChangePlanImmediate()
  {
    list ( $security_token , $customer_id, $target_plan ) = $this->getInputValues();

    $this->addToOutput('account_balance', '');

    try
    {
      teldata_change_db(); // connect to the DB

      // retrieve and validate session
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get customer info
      $customer = get_customer_from_customer_id($customer_id);
      if (! $customer)
        $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');

      if ( ! isSameBrand(get_plan_from_cos_id($customer->cos_id), $target_plan))
        $this->errException('ERR_API_INVALID_ARGUMENTS: previous and future plan brands do not match', 'MP0011');

      // check if the caller is allowed to execute this API
      if ( ! $this->checkUltraSessionAllowedAPICustomer($session_data, $customer_id, $customer->plan_state, __FUNCTION__))
        $this->errException('ERR_API_INTERNAL: API execution not allowed for the given caller', 'SE0005');

      // check customer state
      $state = get_customer_state( $customer );
      if (count($state['errors']))
        $this->errException('ERR_API_INTERNAL: customer state could not be determined', 'IN0002');
      if ( $state['state']['state'] != 'Active' )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer state is not Active', 'IN0001');

      // verify billing_mrc_plan_info
      $billing_mrc_plan_info = \get_ultra_customer_options_by_customer_id( $customer_id );

      if ( $billing_mrc_plan_info && is_array( $billing_mrc_plan_info ) )
      {
        if ($billing_mrc_plan_info[0] == 'BILLING.MRC_PROMO_PLAN' || $billing_mrc_plan_info[0] == BILLING_OPTION_MULTI_MONTH)
          $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid operation for current customer plan', 'VV0105');
      }

      // check current plan
      $current_plan = get_plan_from_cos_id( $customer->COS_ID );
      if ( $current_plan == $target_plan )
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer already in plan $target_plan', 'VV0001');

      // target_plan must be higher than the current plan
      $dollar_target_plan = substr($target_plan, -2);
      $dollar_current_plan = substr($current_plan, -2);
      if ( $dollar_current_plan > $dollar_target_plan )
        $this->errException('ERR_API_INVALID_ARGUMENTS: cannot change plan to a lower one', 'VV0002');

      $total_balance = $customer->BALANCE + $customer->stored_value;
      if ($total_balance < $dollar_target_plan - $dollar_current_plan)
        $this->errException('ERR_API_INTERNAL: not enough money to perform this operation', 'VV0103');

      $bolt_ons_cost = get_bolt_ons_costs_by_customer_id($customer_id);
      if ($total_balance < $bolt_ons_cost + $dollar_target_plan - $dollar_current_plan)
        $this->addWarning('Not enough in stored_value + balance to cover change in plan + bolt ons');

      // transition to new plan
      $result_status = transition_customer_plan($customer, $current_plan, $target_plan, 'Mid-Cycle');
      if (! $result_status['success'])
        $this->errException($result_status['errors'][0], 'SM0001');

      // refresh customer data and return balance in output
      $customer = get_customer_from_customer_id( $customer_id );
      if ( $customer && is_object($customer) )
        $this->addToOutput('account_balance', $customer->BALANCE);

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}


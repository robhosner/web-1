<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class ListMiscellaneousAdjustments extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ListMiscellaneousAdjustments
   * returns a list of misc adjustments
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__ListMiscellaneousAdjustments()
  {
    // initialization
    $params = $this->getNamedInputValues();
    $adjustments = array();

    try
    {
      // retrieve and validate session from security token
      list($session, $errorCode, $errorMessage) = $this->getValidUltraSessionData($params['security_token'], __FUNCTION__);
      if ($errorCode)
        $this->errException($errorMessage, $errorCode);

      // non-MVNO may only query itself
      $params = $this->overwriteIdentityParameters($params, $session, 'MVNO');

      list($adjustments, $error) = \Ultra\Lib\DB\DealerPortal\listMiscellaneousAdjustments($params);
      if ($error)
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('record_count', count($adjustments));
    $this->addToOutput('adjustments', $adjustments);
    return $this->result;
  }
}

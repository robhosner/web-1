<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class RequestProvisionPortedCustomerUpdate extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__RequestProvisionPortedCustomerUpdate
   *
   * Used to resume an already failed port. Funding and plan already exists for the customer; the only information that is necessary is a resubmission of some data.
   * - fails if the customer's state is not 'Port-In Requested'
   * - fails if $number_to_port is not in HTT_PORTIN_LOG
   *
   * @return Result object
   */
  public function dealerportal__RequestProvisionPortedCustomerUpdate ()
  {
    list ( $security_token , $ICCID , $number_to_port , $port_account_number , $port_account_password , $zipcode ) = $this->getInputValues();

    $this->addToOutput('request_id','');
    $this->addToOutput('customer_id','');

    try
    {
      teldata_change_db(); // connect to the DB

      // verify that $zipcode is allowed (in coverage)
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode',$zipcode,'in_coverage');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );
      if ( $error_code )
        $this->errException( $error , $error_code );

      // get ICCID info
      $sim = get_htt_inventory_sim_from_iccid($ICCID);
      if (! $sim)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid SIM card', 'VV0065');

      // check for intra-brand port
      $customer = get_customer_from_msisdn($number_to_port, 'u.BRAND_ID,plan_state');
      if ($customer && $customer->BRAND_ID != $sim->BRAND_ID)
      {
        // then is intra-brand port
        $this->errException('ERR_API_INTERNAL: intra-port requests cannot be updated', 'IN0002');
      }

      save_port_account( $number_to_port , $port_account_number , $port_account_password );

      // copied verbatim from provisioning__requestResubmitProvisionPortedCustomerAsync

      // $numberToPort must match the first port request and is used to find the correct customer_id

      $customer   = NULL;
      $request_id = NULL;

        $portInQueue = new \PortInQueue();

        $loadByMsisdnResult = $portInQueue->loadByMsisdn( $number_to_port );

        dlog('',"portInQueue = %s",$portInQueue);

        $errors = $loadByMsisdnResult->get_errors();

        teldata_change_db(); // connect back to default DB

        if ( count($errors) )
          $this->errException('ERR_API_INVALID_ARGUMENTS: previous porting attempt not found', 'PO0002');
        else
          if ( !$portInQueue->customer_id )
            $this->errException('ERR_API_INVALID_ARGUMENTS: previous porting attempt not found', 'PO0002');
          else
          {
            $query = make_find_ultra_customer_query_from_customer_id( $portInQueue->customer_id );
            $customer = find_customer( $query );

            if ( luhnenize($ICCID) != $customer->CURRENT_ICCID_FULL )
              $this->errException("ERR_API_INVALID_ARGUMENTS: $ICCID is not associated with $number_to_port",'UN0001');
          }

      // get current state
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);

      if ( ! $state )
        $this->errException( "ERR_API_INTERNAL: customer (".$customer->CUSTOMER_ID.") state could not be determined" , 'UN0001' );

      if ( $state['state'] != 'Port-In Requested' )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command" , 'IN0001' );

      // add action for mvneUpdatePortIn

      $params = array(
        $customer->CUSTOMER_ID,
        $number_to_port,
        $ICCID,
        $port_account_number,
        $port_account_password,
        $zipcode,
        "__this_transition__",
        "__this_action__",
        "__this_action_seq__",
      );

      $context = array(
        'customer_id' => $customer->CUSTOMER_ID
      );

      $result_status = append_action($context,
                                     array('type'        => 'funcall',
                                           'name'        => 'mvneUpdatePortIn',
                                           'transaction' => NULL,
                                           'seq'         => NULL
                                     ),
                                     $params,
                                     FALSE
      );

      if ( $result_status['success'] != 1 )
        $this->errException( "ERR_API_INTERNAL: state transition error" , 'SM0001' );

      if ( ! empty($result_status['transitions'][0]))
      {
        $request_id = $result_status['transitions'][0];
        unreserve_transition_uuid_by_pid($request_id);
      }

      // update ULTRA.HTT_ACTIVATION_HISTORY
      $activation_history = get_ultra_activation_history( $customer->CUSTOMER_ID );

      if ( $activation_history
        && ( $activation_history->FINAL_STATE != 'Cancelled'   )
        && ( $activation_history->FINAL_STATE != FINAL_STATE_COMPLETE )
        && ( $activation_history->FINAL_STATE != 'Provisioned' )
      )
        log_datetime_activation_history( $customer->CUSTOMER_ID );

      flush_ultra_activation_history_cache_by_customer_id( $customer->CUSTOMER_ID );

      $this->addToOutput('request_id',$request_id);
      $this->addToOutput('customer_id',$customer->CUSTOMER_ID);

      // memorize $request_id in Redis by customer_id for dealerportal__SearchActivationHistoryDealer
      $redis_port = new \Ultra\Lib\Util\Redis\Port();

      $redis_port->setRequestId( $customer->CUSTOMER_ID , $request_id );

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}


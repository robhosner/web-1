<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

require_once 'Ultra/Lib/Api/Partner/Dealerportal.php';

class SearchCustomersByDetails extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__SearchCustomersByDetails
   *
   * Search for subscribers based on various fields.
   * - If the caller role is Dealer, the customers returned should have been activated by this dealer
   *
   * @return Result object
   */
  public function dealerportal__SearchCustomersByDetails ()
  {
    list ( $security_token , $page_size , $page_ord , $first_name , $last_name , $city , $email , $address , $zipcode , $sort_by ) = $this->getInputValues();

    if ( ! $page_size ) $page_size = 100;
    if ( ! $page_ord  ) $page_ord  = 1;

    $this->addToOutput('page_size',$page_size);
    $this->addToOutput('page_ord',$page_ord);
    $this->addToOutput('record_count','');
    $this->addToOutput('customers',array());

    try
    {
      // connect to the DB
      teldata_change_db();

      // retrieve and validate session from $security_token
      list( $session_data , $error_code , $error ) = $this->getValidUltraSessionData( $security_token , __FUNCTION__ );

      if ( $error_code )
        $this->errException( $error , $error_code );

      $redis = new \Ultra\Lib\Util\Redis;

      if ( ( $first_name == '' )
        && ( $last_name  == '' )
        && ( $city       == '' )
        && ( $email      == '' )
        && ( $zipcode    == '' )
      )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: not enough search criterias provided." , 'NS0001' );

      if ( ( $address    != '' )
        && ( $zipcode    == '' )
        && ( $city       == '' )
      )
        // Address Line 1 - Required to search with either zip code or city
        $this->errException( "ERR_API_INVALID_ARGUMENTS: not enough search criterias provided." , 'NS0001' );

      if ( preg_match("/\%/", $first_name ) && !preg_match("/^[\w ]{1,}\%$/", $first_name ) )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: invalid search criterias provided (first_name)." , 'NS0002' );

      if ( preg_match("/\%/", $last_name ) && !preg_match("/^[\w ]{1,}\%$/", $last_name ) )
        $this->errException( "ERR_API_INVALID_ARGUMENTS: invalid search criterias provided (last_name)." , 'NS0002' );

      if (preg_match("/\%/", $city) && ! preg_match("/^[\w ]{3,}\%$/", $city))
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid search criterias provided (city).' , 'NS0002' );
      if (preg_match('/[' . preg_quote('"*=:;~\/', '/') . ']/', $city))
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid characters in search criteria city.' , 'NS0002' );

      // validate $sort_by
      if ( is_array($sort_by) && count($sort_by) )
      {
        $invalid = $this->validate_search_sort_by( $sort_by );

        if ( $invalid )
          $this->errException( "ERR_API_INVALID_ARGUMENTS: invalid sorting parameters." , 'VV0036' );
      }
      else
      {
        $sort_by = array('msisdn');
      }

      $search_array = array(
        'first_name' => $first_name,
        'last_name'  => $last_name,
        'city'       => $city,
        'email'      => $email,
        'address'    => $address,
        'zipcode'    => $zipcode,
        'sort_by'    => $sort_by
      );

      // Role Context Check : currently we only support ROLE_NAME_ADMIN, ROLE_NAME_DEALER_OWNER, ROLE_NAME_DEALER_EMPLOYEE
      if (in_array($session_data['role'], array(self::ROLE_NAME_DEALER_OWNER, self::ROLE_NAME_DEALER_MANAGER, self::ROLE_NAME_DEALER_EMPLOYEE)))
        $search_array['dealer'] = $session_data['dealer'];

      dlog('',"search_array = %s",$search_array);

      $search_key = md5( json_encode ( $search_array ) );

      $redis_key = 'ultra/api/dealerportal/cache/' . __FUNCTION__ . '/' . $search_key;

      dlog('',"redis_key = $redis_key");

      // get data from cache

      $cached_result = $redis->get( $redis_key );

      if ( $cached_result )
      {
        // no need to query the DB

        $decoded_result = json_decode( $cached_result );

        // extract only the needed data

        $paginated_output = paginate_output( $decoded_result , $page_size , $page_ord );

        if ( ! count( $paginated_output ) )
        {
          $this->addToOutput('record_count',0);

          $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
        }

        $this->addToOutput('customers',$paginated_output);
        $this->addToOutput('record_count',count( $decoded_result ));
      }
      else
      {
        // get data from DB

        $search_array['select_attributes'] = $this->search_api_select_attributes();

        $customers_search_result = find_ultra_customers_from_details( $search_array );

        if ( ( $customers_search_result->is_failure() )
          || ( ( ! $customers_search_result->data_array ) || ( ! count( $customers_search_result->data_array ) ) )
        )
        {
          $this->addToOutput('record_count',0);

          $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
        }
        else
        {
          // add extra output value per MVNO-1907
          for ($i = 0, $max = count($customers_search_result->data_array); $i < $max; $i++)
            $customers_search_result->data_array[$i]->has_access = TRUE;

          // extract only the needed data
          $paginated_output = paginate_output( $customers_search_result->data_array , $page_size , $page_ord );

          if ( ! count( $paginated_output ) )
          {
            $this->addToOutput('record_count',0);

            $this->errException( "ERR_API_INTERNAL: data not found." , 'ND0001' );
          }

          $this->addToOutput('customers',$paginated_output);
          $this->addToOutput('record_count',count($customers_search_result->data_array));

          // cache result in Redis for 30 seconds
          $redis->set( $redis_key , json_encode( $customers_search_result->data_array ) , 30 );
        }
      }

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

// TODO: RBAC output filter

    return $this->result;
  }
}
<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class ListReportPeriods extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__ListReportPeriods
   * list available reporting periods
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @see DEAL-36
   * @return Result object
   */
  public function dealerportal__ListReportPeriods()
  {
    // initialization
    $params = $this->getNamedInputValues(); // security_token, business_type, business_code, request_type
    $periods = array();
    $record_count = 0;

    try
    {
      // verify session
      list($session_data, $error_code, $error) = $this->getValidUltraSessionData($params['security_token'], __FUNCTION__);
      if ($error_code)
        $this->errException($error, $error_code);

      // non-MVNO may only query itself
      $params = $this->overwriteIdentityParameters($params, $session_data, 'MVNO');

      // get available reporting periods
      list($periods, $error) = \Ultra\Lib\DB\DealerPortal\listReportPeriods($params);
      if ($error)
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001', $error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('periods', $periods);
    $this->addToOutput('record_count', count($periods));
    return $this->result;
  }
}

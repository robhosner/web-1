<?php

namespace Ultra\Lib\Api\Partner\Dealerportal;

class LoginUser extends \Ultra\Lib\Api\Partner\Dealerportal
{
  /**
   * dealerportal__LoginUser
   * validate user by given username and password, create session and return user and organization info
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/API+specifications
   * @return Result object
   */
  public function dealerportal__LoginUser()
  {
    // initialization
    list ($username, $password) = $this->getInputValues();
    foreach ($dbValues = array('user_id', 'first_name', 'last_name', 'access_role', 'business_code', 'business_name', 'business_type') as $name)
      $this->addToOutput($name, NULL);
    $security_token = NULL;
    $resetPassword = FALSE;

    try
    {
      // throttle API use to prohibit password cracking
      $redis = new \Ultra\Lib\Util\Redis;
      if (checkApiAbuseByIdentifier(__FUNCTION__, $username, self::MAX_FAILED_LOGINS, $redis, FALSE))
      {
        dlog('', 'WARNING: denied API %s from IP %s for user %s and password %s', __FUNCTION__,  \Session::getClientIp(), $username, $password);
        $this->errException('ERR_API_INTERNAL: This command has been currently disabled. Please try again later.', 'AP0002');
      }

      // get user record
      list($user, $error) = \Ultra\Lib\DB\DealerPortal\validateUser($username, $password);
      if ( ! $user)
      {
        dlog('', "WARNING: $error");
        checkApiAbuseByIdentifier(__FUNCTION__, $username, self::MAX_FAILED_LOGINS, $redis, TRUE); // prevenet brute force username cracking
        $this->errException('ERR_API_INTERNAL: Invalid login information', 'SE0007', $error);
      }

      // PROD-1812: truncate input password to 12 chars and compare ingoring case
      $password = substr($password, 0, 12);
      if (strcasecmp($user->PASSWORD, $password))
      {
        // DB validation failed, check if we have a temporary password
        if ( ! $tempPassword = $redis->get(self::TEMP_PASSWORD_KEY . '/' . $username))
        {
          dlog('', 'WARNING: failed password for user %s and password %s', $username, $password);
          checkApiAbuseByIdentifier(__FUNCTION__, $username, self::MAX_FAILED_LOGINS, $redis, TRUE); // prevent brute force password cracking
          $this->errException('ERR_API_INTERNAL: Invalid login information', 'SE0007');
        }

        // validate against found temporary password
        if ($password != $tempPassword)
        {
          checkApiAbuseByIdentifier(__FUNCTION__, $username, self::MAX_FAILED_LOGINS, $redis, TRUE); // prevent temp password cracking
          $this->errException('ERR_API_INTERNAL: Invalid login information', 'SE0007');
        }

        // temporary password validation succeeded
        dlog('', "user $username successfully logged in with temporary password $password");
        $resetPassword = TRUE;
      }
      dlog('', 'user info: %s', $user);

      // create security token
      $security_token = $this->ultrasession->addObject(
        array(
          'user_id'         => $user->USER_ID,
          'role'            => $user->BUSINESS_TYPE . ' ' . $user->ACCESS_ROLE,
          'business_code'   => $user->BUSINESS_CODE,
          'business_name'   => $user->BUSINESS_NAME,
          'dealer'          => $user->DEALER,
          'masteragent'     => $user->MASTER,
          'reset_password'  => $resetPassword));
      if ( ! $security_token)
        $this->errException('ERR_API_INTERNAL: Could not generate a security token.', 'SE0001');

      // add return values
      foreach ($dbValues as $name)
        $this->addToOutput($name, $user->{strtoupper($name)});

      // DEAL-312: log successfull login
      if ($error = \Ultra\Lib\DB\DealerPortal\logUserAccess($user))
        logError($error);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('reset_password', $resetPassword);
    $this->addToOutput('security_token', $security_token);
    return $this->result;
  }
}

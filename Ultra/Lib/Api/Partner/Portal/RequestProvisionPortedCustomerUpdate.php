<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class RequestProvisionPortedCustomerUpdate extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__RequestProvisionPortedCustomerUpdate
   *
   * used to resume an already failed port
   * funding and plan already exists for the customer; the only information that is necessary is a resubmission of some data
   * fails if the customer's state is not 'Port-In Requested'
   * fails if $number_to_port is not in HTT_PORTIN_LOG
   *
   * @return Result object
   */
  public function portal__RequestProvisionPortedCustomerUpdate()
  {
    // init
    list ($ICCID, $number_to_port, $port_account_number, $port_account_password, $zipcode) = $this->getInputValues();
    $this->addToOutput('request_id', NULL);
    $this->addToOutput('customer_id', null);

    try
    {
      teldata_change_db();

      // verify that $zipcode is allowed (in coverage)
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode',$zipcode,'in_coverage');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // get subscriber from zsession cookie
      if (empty($_COOKIE['zsession']))
        $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');
      $zsession = $_COOKIE['zsession'];
      if (! $customer = getCustomerFromSession($zsession))
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');

      // get ICCID info
      $sim = get_htt_inventory_sim_from_iccid($ICCID);
      if (! $sim)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid SIM card', 'VV0065');

      // check for intra-brand port
      if ($customer && $customer->BRAND_ID != $sim->BRAND_ID)
      {
        // then is intra-brand port
        $this->errException('ERR_API_INTERNAL: intra-port requests cannot be updated', 'VV0065');
      }

      $this->setOutputLanguage($customer->preferred_language);
      save_port_account($number_to_port, $port_account_number, $port_account_password);

      // $numberToPort must match the first port request and is used to confirm the correct customer_id
      $portInQueue = new \PortInQueue();
      $loadByMsisdnResult = $portInQueue->loadByMsisdn( $number_to_port );
      $errors = $loadByMsisdnResult->get_errors();
      teldata_change_db(); // connect back to default DB

      // check for errors and integrity
      if (count($errors))
        $this->errException('ERR_API_INVALID_ARGUMENTS: previous porting attempt not found (1)', 'PO0002');
      if ( ! $portInQueue->customer_id)
        $this->errException('ERR_API_INVALID_ARGUMENTS: previous porting attempt not found (2)', 'PO0002');
      if ($customer->CUSTOMER_ID != $portInQueue->customer_id)
        $this->errException('ERR_API_INVALID_ARGUMENTS: previous porting attempt not found (3)', 'PO0002');
      if (luhnenize($ICCID) != $customer->CURRENT_ICCID_FULL)
        $this->errException("ERR_API_INVALID_ARGUMENTS: $ICCID is not associated with $number_to_port", 'UN0001');

      // get current state
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);
      if ( ! $state )
        $this->errException('ERR_API_INTERNAL: customer state could not be determined', 'UN0001');
      if ($state['state'] != 'Port-In Requested')
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command' , 'IN0001');

      // add action for mvneUpdatePortIn
      $params = array(
        $customer->CUSTOMER_ID,
        $number_to_port,
        $ICCID,
        $port_account_number,
        $port_account_password,
        $zipcode,
        "__this_transition__",
        "__this_action__",
        "__this_action_seq__");
      $context = array('customer_id' => $customer->CUSTOMER_ID);
      $append = array(
        'type'        => 'funcall',
        'name'        => 'mvneUpdatePortIn',
        'transaction' => NULL,
        'seq'         => NULL);
      $result_status = append_action($context, $append, $params, FALSE);

      // check result
      if ( $result_status['success'] != 1 )
        $this->errException('ERR_API_INTERNAL: state transition error', 'SM0001');
      if ( ! empty($result_status['transitions'][0]))
      {
        $this->addToOutput('request_id', $result_status['transitions'][0]);
        unreserve_transition_uuid_by_pid($result_status['transitions'][0]);
      }

      // update ULTRA.HTT_ACTIVATION_HISTORY
      $activation_history = get_ultra_activation_history($customer->CUSTOMER_ID);

      if ( $activation_history
        && ( $activation_history->FINAL_STATE != 'Cancelled'   )
        && ( $activation_history->FINAL_STATE != FINAL_STATE_COMPLETE )
        && ( $activation_history->FINAL_STATE != 'Provisioned' )
      )
        log_datetime_activation_history($customer->CUSTOMER_ID);
      flush_ultra_activation_history_cache_by_customer_id($customer->CUSTOMER_ID);

      $this->addToOutput('customer_id', $customer->CUSTOMER_ID);

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , 'ERROR: ' . $e->getMessage());
    }

    return $this->result;
  }
}


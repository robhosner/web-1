<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Customers\Customer;
use Ultra\Customers\Options;
use Ultra\Exceptions\MissingRequiredParametersException;
use function Ultra\UltraConfig\isBPlan;

require_once 'Ultra/Lib/Api/Partner/Portal.php';
require_once 'classes/Flex.php';

class CustomerDetail extends \Ultra\Lib\Api\Partner\Portal
{
  public $customer = null;
  public $detail   = array();
  public $options  = array();

  /**
   * portal__CustomerDetail
   * provides all the useful details on a customer based upon a customer cookie session
   * @param string mode
   * @return object Result
   */
  public function portal__CustomerDetail()
  {
    // initialize
    list ($mode) = $this->getInputValues();
    $detail = array();

    try
    {
      teldata_change_db();

      // get subscriber from session
      $session = new \Session();
      if ( ! $session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');

      // get customer
      if ( ! $customer = get_customer_from_customer_id($session->customer_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');

      $this->customer = $customer;
      $this->options  = new \CustomerOptions($customer->CUSTOMER_ID, true);

      // set API output language
      $this->setOutputLanguage($customer->preferred_language);

      // functions set $this->detail
      $this->setCustomerDetail();
      $this->setCustomerCCDetail();
      $this->setMultiMonthDetail();
      $this->setPlanDetails();
      $this->setWifiCallingDetail();

      // set throttle speed
      $this->detail['throttle_speed'] = $this->options->getThrottleSpeed();

      // BOOL if count open transitions
      $this->detail['transition_in_progress'] = ( count_customer_open_transitions( $customer->CUSTOMER_ID ) > 0 ? '1' : '0' );


      //
      // set data usage details
      $this->detail['4g_lte_remaining'] = null;
      $this->detail['3g_remaining']     = null;

      $this->detail['4g_lte_usage'] = '0MB';
      $this->detail['3g_usage']     = '0MB';

      // if DATA_USAGE, CheckBalance MW Call
      if ( $mode == 'DATA_USAGE' )
        $this->setDataUsageDetail();

      $this->detail['4g_lte_usage']     = beautify_4g_lte_string( $this->detail['4g_lte_usage'], TRUE );
      $this->detail['4g_lte_remaining'] = beautify_4g_lte_string( $this->detail['4g_lte_remaining'], FALSE );
      // end set data usage details
      //


      $this->addArrayToOutput($this->detail);

      $this->succeed();
    }
    catch(MissingRequiredParametersException $e)
    {
      $this->addError($e->getMessage(), $e->code());
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }

  /**
   *
   */
  public function setMultiMonthDetail()
  {
    $duration    = null;
    $paidThrough = null;
    $planId = get_plan_from_cos_id($this->customer->cos_id);
    $isMultiMonthPlan = false;

    if (isBPlan($this->customer->cos_id) || \Ultra\UltraConfig\isMintPlan($this->customer->cos_id) || $planId == 'M29') {
      $isMultiMonthPlan = true;
    }

    $this->detail['is_multi_month_plan'] = $isMultiMonthPlan;

    // if MINT, grab detail from MULTI_MONTH_OVERLAY
    if (\Ultra\UltraConfig\isMintPlan($this->customer->cos_id))
    {
      $months    = (int) substr(get_plan_from_cos_id($this->customer->cos_id), 1, 2);
      $dateTime  = new \DateTime(date('y-m-d', $this->customer->plan_started_epoch));

      $this->detail['total_plan_months']       = $months;
      $this->detail['days_into_billing_cycle'] = $dateTime->diff(new \DateTime())->days;
      $this->detail['billing_cycle_length']    = $months == 12 ? 365 : $months * 30;

      // DB Call
      $overlayRow  = ultra_multi_month_overlay_from_customer_id($this->customer->CUSTOMER_ID);

      $paidThrough = strtotime($overlayRow->CYCLE_EXPIRES);
      $duration    = $overlayRow->TOTAL_MONTHS;
    } else if (isBPlan($this->customer->cos_id)) {
      $overlayRow  = ultra_multi_month_overlay_from_customer_id($this->customer->CUSTOMER_ID);
      $paidThrough = strtotime($overlayRow->CYCLE_EXPIRES);
      $duration    = $overlayRow->TOTAL_MONTHS;
    }
    // else grab from CUSTOMER_OPTIONS
    else
    {
      $multi_month_options = $this->options->multiMonthInfo();

      $paidThrough = $multi_month_options ? strtotime("+{$multi_month_options['months_left']} month", $this->customer->plan_expires_epoch) : NULL;
      $duration    = $multi_month_options ? $multi_month_options['months_total'] : NULL;
    }

    // 
    $this->detail = array_merge($this->detail, [ 'paid_through' => $paidThrough, 'duration' => $duration ]);
  }

  /**
   *
   */
  public function setCustomerDetail()
  {
    $detail = [
      'balance'            => $this->customer->BALANCE,
      'current_msisdn'     => $this->customer->current_mobile_number,
      'customer'           => $this->customer->CUSTOMER,
      'customer_id'        => $this->customer->CUSTOMER_ID,
      'customer_loginname' => $this->customer->LOGIN_NAME,
      'customer_status'    => $this->customer->plan_state,
      'preferred_language' => $this->customer->preferred_language,

      'tos_accepted' => ( ! ! $this->customer->tos_accepted),

      'packaged_balance1'  => $this->customer->PACKAGED_BALANCE1,
      'plan_expires'       => $this->customer->plan_expires_epoch,
      'stored_value'       => (int) $this->customer->stored_value,

      'monthly_renewal_target' => $this->customer->MONTHLY_RENEWAL_TARGET,

      'email'        => $this->customer->E_MAIL,

      'first_name'   => $this->customer->FIRST_NAME,
      'last_name'    => $this->customer->LAST_NAME,

      'address1'     => $this->customer->ADDRESS1,
      'address2'     => $this->customer->ADDRESS2,
      'city'         => $this->customer->CITY,
      'state_region' => $this->customer->STATE_REGION,
      'postal_code'  => $this->customer->POSTAL_CODE,
      'country'      => $this->customer->COUNTRY
    ];

    // globe
    $detail['globe_minutes'] = $this->customer->PACKAGED_BALANCE3;

    // set iccid
    $detail['iccid'] = $this->customer->CURRENT_ICCID_FULL;
    $detail['iccid'] = luhnenize($detail['iccid']);
    $detail['iccid'] = trim($detail['iccid']);

    // set master_agent
    $iccidQuery = htt_inventory_sim_select_query(['iccid_full' => $detail['iccid']], true);
    $iccid = mssql_fetch_all_objects(logged_mssql_query($iccidQuery));
    if (is_array($iccid) && count($iccid)) {
      $detail['master_agent'] = $iccid[0]->INVENTORY_MASTERAGENT;
    } else {
      $detail['master_agent'] = null;
    }
    
    // 
    $this->detail = array_merge($this->detail, $detail);
  }

  /**
   *
   */
  public function setCustomerCCDetail()
  {
    $detail = [
      'auto_recharge'    => ( $this->customer->monthly_cc_renewal == 1 ),

      'cc_name'          => $this->customer->CC_NAME,
      'cc_address1'      => $this->customer->CC_ADDRESS1,
      'cc_address2'      => $this->customer->CC_ADDRESS2,
      'cc_city'          => $this->customer->CC_CITY,
      'cc_state_region'  => $this->customer->CC_STATE_REGION,
      'cc_postal_code'   => $this->customer->CC_POSTAL_CODE
    ];

    // CCCP-15: get CC info
    $cc_info = get_cc_info_from_customer_id($this->customer->CUSTOMER_ID);

    // TODO: cache
    $detail['cc_exp_date'] = empty($cc_info->EXPIRES_DATE) ? NULL : $cc_info->EXPIRES_DATE;
    $detail['cc_last_4']   = empty($cc_info->LAST_FOUR) ? NULL : $cc_info->LAST_FOUR;

    // 
    $this->detail = array_merge($this->detail, $detail);
  }

  /**
   *
   */
  public function setPlanDetails()
  {
    $plan = get_plan_from_cos_id($this->customer->cos_id);

    $detail = [
      'brand'        => \Ultra\UltraConfig\getShortNameFromBrandId($this->customer->BRAND_ID),
      'cos'          => cos_id_plan_description($this->customer->cos_id),
      'plan'         => $plan,
      'zero_minutes' => int_positive_or_zero(1000 - \Ultra\UltraConfig\zeroUsedMinutes(get_plan_name_from_short_name($plan)) - $this->customer->PERIOD_MINUTES_TO_DATE_BILLED),
    ];

    $detail['plan_cost'] = get_plan_cost_by_cos_id($this->customer->cos_id) * 100;
    $detail['full_cost'] = $detail['plan_cost'];

    $detail['bolt_ons'] = $this->options->getBoltOnsInfo( $this->customer->BRAND_ID );

    if (\Ultra\Lib\Flex::isFlexPlan($plan)) {
      $detail['plan_credits'] = (int) (new \LineCredits())->getBalance($this->customer->CUSTOMER_ID);
      teldata_change_db();
    }

    // add bolt on cost to full_cost
    foreach ($detail['bolt_ons'] as $bolt_on)
      $detail['full_cost'] += $bolt_on['cost'] * 100;

    // 
    $this->detail = array_merge($this->detail, $detail);
  }

  /**
   *
   */
  public function setWifiCallingDetail()
  {
    $detail = array();

    $e911_address = $this->options->getE911Address();

    $detail['e911_address'] = $e911_address;
    $detail['has_wifi_soc'] = ($e911_address) ? true : false;

    // 
    $this->detail = array_merge($this->detail, $detail);
  }

  /**
   * is only called if mode == DATA_USAGE
   * invoke mwCheckBalance to check 4G LTE data usage for this customer
   * @return null
   */
  public function setDataUsageDetail()
  {
    if ( $this->customer->current_mobile_number )
    {
      $detail = array();

      $redis = new \Ultra\Lib\Util\Redis();

      list(
        $remaining,
        $usage,
        $mintAddOnRemaining,
        $mintAddOnUsage,
        $breakDown,
        $mvneError,
        $data3G
      ) = mvneGet4gLTE($this->getRequestId(), $this->customer->current_mobile_number, $this->customer->CUSTOMER_ID, $redis);

      $detail['4g_lte_remaining'] = $remaining;
      $detail['3g_remaining']     = beautify_4g_lte_string($data3G['remaining3G'], false);

      $detail['4g_lte_usage'] = $usage;
      $detail['3g_usage']     = beautify_4g_lte_string($data3G['used3G'], true);

      teldata_change_db();

      // 
      $this->detail = array_merge($this->detail, $detail);
    }
  }
}

      // $detail['originalsimproduct']  = strlen($customer->CURRENT_ICCID_FULL) == 19 ? get_product_type_from_iccid($customer->CURRENT_ICCID_FULL) : '';

      /*
      if ( ! isset($detail['porting_request_id']) || ( ! $detail['porting_request_id'] )  )
      {
        $htt_transition_log_select_query = htt_transition_log_select_query(
          array(
            'sql_limit'   => 1,
            'status'      => 'OPEN',
            'customer_id' => $customer->CUSTOMER_ID,
            'order_by'    => 'ORDER BY t.CREATED ASC'
            )
          );

        $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_select_query));

        if ( ( is_array($query_result) ) && count($query_result) > 0 )
          $detail['porting_request_id'] = $query_result[0]->TRANSITION_UUID;
      }
      */

      // MVNO-2554: format data string regardless of value
      

      // $detail['4g_lte_usage']     = beautify_4g_lte_string( $detail['4g_lte_usage'], TRUE );

      // $detail['4g_lte_usage']        = NULL;
      // $detail['3g_usage']            = null;
      // $detail['4g_lte_last_applied'] = 0;
      // $detail['4g_lte_bolton_percent_used'] = 0;
      // $detail['monthly_data_purchases'] = 0;
      // $detail['customer_plan_start'] = $customer->plan_started_epoch;
      // $detail['local_phone']         = $customer->LOCAL_PHONE;
      
      // $detail['activate_request_id'] = get_activation_request_id($customer->CUSTOMER_ID);
      // $detail['porting_request_id']  = get_porting_request_id($customer->CUSTOMER_ID);
      // $detail['last_transition_name'] = get_last_transition_name_by_customer_id($customer->CUSTOMER_ID);
      
/*
      { // get Port-in data for MVNE2
        $portInQueue = new \PortInQueue();

        $loadByCustomerIdResult = $portInQueue->loadByCustomerId( $customer->CUSTOMER_ID );

        if ( $loadByCustomerIdResult->is_success() && $portInQueue->portin_queue_id )
        {
          if ( property_exists( $portInQueue , 'msisdn') )
            $detail['porting_msisdn']           = $portInQueue->msisdn;

          if ( property_exists( $portInQueue , 'account_number') )
            $detail['porting_account_number']   = $portInQueue->account_number;

          if ( property_exists( $portInQueue , 'account_password') )
            $detail['porting_account_password'] = $portInQueue->account_password;

          if ( property_exists( $portInQueue , 'account_zipcode') )
            $detail['porting_account_zipcode']  = $portInQueue->account_zipcode;

          $redis_port = new \Ultra\Lib\Util\Redis\Port();

          $detail['porting_request_id'] = $redis_port->getRequestId( $customer->CUSTOMER_ID , $customer->plan_state );
        }

        teldata_change_db();
      }
*/

/*
      $detail['voice_minutes'] = 0;

      if ( ( $mode == 'OCS_BALANCE' ) && ( $plan == 'L34' || $plan == 'L19' ) )
      {
        $detail['voice_minutes'] = mvneGetVoiceMinutes($customer->CUSTOMER_ID);
      }
*/


  /*
  public function setDataUsageDetail()
  {
    $redis = new \Ultra\Lib\Util\Redis();

    // get '4g_lte_last_applied' from HTT_DATA_EVENT_LOG
    // $latest_data_event_log = get_latest_data_event_log( $customer->CUSTOMER_ID );
    // if ( $latest_data_event_log )
    //   $detail['4g_lte_last_applied'] = $latest_data_event_log->event_date_epoch;

    // invoke mwCheckBalance to check 4G LTE data usage for this customer
    if ( $customer->current_mobile_number )
    {
      list( $remaining , $usage , $mintAddOnRemaining , $mintAddOnUsage , $breakDown, $mvneError, $data3G) = mvneGet4gLTE($this->getRequestId(), $customer->current_mobile_number, $customer->CUSTOMER_ID, $redis);

      if ( ! empty( $mintAddOnUsage )
        && ! empty( $mintAddOnUsage + $mintAddOnRemaining )
      )
      {
        $detail['4g_lte_bolton_percent_used'] = ceil( ( $mintAddOnUsage * 100 ) / ( $mintAddOnUsage + $mintAddOnRemaining ) );
      }

      $detail['4g_lte_remaining'] = $remaining;
      $detail['3g_remaining']     = beautify_4g_lte_string($data3G['remaining3G'], false);

      // $detail['4g_lte_usage']     = $usage;
      // $detail['3g_usage'] = beautify_4g_lte_string($data3G['used3G'], true);

      teldata_change_db();

      // MVNO-2612: add the count of data purchases within the current cycle
      // $detail['monthly_data_purchases'] = count(get_current_cycle_purchase_history($customer->CUSTOMER_ID, 'SPEND', 'DATA Purchase'));
    }
  }
  */
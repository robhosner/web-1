<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Lib\CheckBalance;

class CheckBalances extends Portal
{
  /**
   * @var Session
   */
  private $session;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var CheckBalance
   */
  private $checkBalance;

  /**
   * CheckBalances constructor.
   * @param Session $session
   * @param CustomerRepository $customerRepository
   * @param CheckBalance $checkBalance
   */
  public function __construct(Session $session, CustomerRepository $customerRepository, CheckBalance $checkBalance)
  {
    $this->session = $session;
    $this->customerRepository = $customerRepository;
    $this->checkBalance = $checkBalance;
  }

  /**
   * portal__CheckBalances
   *
   * @param int customer_id
   * @return object Result
   */
  public function portal__CheckBalances()
  {
    $balances = [];

    try {
      teldata_change_db();

      if (!$this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');
      }

      $customer = $this->customerRepository->getCustomerById($this->session->customer_id, ['current_mobile_number'], true);
      if (!$customer || !$customer->current_mobile_number) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: no customer found', 'VV0031');
      }

      $balances = $this->checkBalance->byMSISDN($customer->current_mobile_number);
      $this->succeed();
    } catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('balances', $balances);
    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class ChargeCustomerProvidedCC extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * test__portal__ChargeCustomerProvidedCC
   */
  public function test__portal__ChargeCustomerProvidedCC()
  {
    $url = $this->base_url . '&command=' . substr(__FUNCTION__, strlen('test__'));

    // default tests
    $defaultTester = new DefaultUltraAPITest($url , $this);
    $defaultTester->runDefaultTests();

    // test missing parameters
    $params = array();
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue( ! $decoded_response->success);
    $this->assertContains('API_ERR_PARAMETER: Missing portal__ChargeCustomerProvidedCC REST parameter account_cc_exp!', $decoded_response->errors );
    $this->assertContains('API_ERR_PARAMETER: Missing portal__ChargeCustomerProvidedCC REST parameter account_cc_cvv!', $decoded_response->errors );
    $this->assertContains('API_ERR_PARAMETER: Missing portal__ChargeCustomerProvidedCC REST parameter account_cc_number!', $decoded_response->errors );
    $this->assertContains('API_ERR_PARAMETER: Missing portal__ChargeCustomerProvidedCC REST parameter cc_name!' , $decoded_response->errors );
    $this->assertContains('API_ERR_PARAMETER: Missing portal__ChargeCustomerProvidedCC REST parameter cc_postal_code!', $decoded_response->errors );
    $this->assertContains('API_ERR_PARAMETER: Missing portal__ChargeCustomerProvidedCC REST parameter charge_amount!', $decoded_response->errors );
    $this->assertContains('API_ERR_PARAMETER: Missing portal__ChargeCustomerProvidedCC REST parameter reason!', $decoded_response->errors );

    // test missing zsession
    $params['account_cc_number'] = '4067129148899676';
    $params['account_cc_exp'] = '1214';
    $params['account_cc_cvv'] = '110';
    $params['cc_name'] = 'Rina Ultrasmith';
    $params['cc_postal_code'] = '92650';
    $params['charge_amount'] = '1000';
    $params['reason'] = 'balanceadd_recharge';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertFalse($decoded_response->success);
    $this->assertContains('ERR_API_INTERNAL: login session has expired', $decoded_response->errors);

    // test always succeed CC
    $zsession = createCustomerSession('Rickey', 'Jugal12345');
    $this->curl_options[CURLOPT_COOKIE] = "zsession=$zsession";
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue($decoded_response->success);
    $this->assertTrue(! count($decoded_response->errors));
    $this->assertTrue(! count($decoded_response->warnings));
    $this->assertTrue(! count($decoded_response->cc_errors));

    // test always fail CC
    $params['account_cc_number'] = '4195473398654406';
    $params['account_cc_exp'] = '1214';
    $params['account_cc_cvv'] = '990';
    $params['cc_name'] = 'Rina Ultrasmith';
    $params['cc_postal_code'] = '92650';
    $params['charge_amount'] = '2900';
    $params['reason'] = 'balanceadd_recharge';
    $json_result = curl_post($url, $params, $this->curl_options, NULL, 240);
    echo "\n$json_result\n";
    $decoded_response = json_decode($json_result);
    $this->assertTrue(! $decoded_response->success);
    $this->assertContains('ERR_API_INTERNAL: credit card transaction not successful', $decoded_response->errors);
  }
}

?>
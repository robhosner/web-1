<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Customers\StateActions;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Payments\Payment;
use Ultra\Utilities\Common;
use Ultra\Messaging\Messenger;


class ApplyBalanceToStoredValue extends Portal
{
  /**
   * @var Common
   */
  private $utilities;

  /**
   * @var Session
   */
  private $session;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  public $customer;

  /**
   * @var Payment
   */
  private $payment;

  /**
   * @var StateActions
   */
  private $stateActions;

  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * AcceptTermsOfService constructor.
   * @param Common $utilities
   * @param Session $session
   * @param CustomerRepository $customerRepository
   * @param Payment $payment
   * @param StateActions $stateActions
   */
  public function __construct(
    Common $utilities,
    Session $session,
    CustomerRepository $customerRepository,
    Payment $payment,
    StateActions $stateActions,
    Messenger $messenger
  )
  {
    $this->utilities = $utilities;
    $this->session = $session;
    $this->customerRepository = $customerRepository;
    $this->payment = $payment;
    $this->stateActions = $stateActions;
    $this->messenger = $messenger;
  }

  /**
   * portal__ApplyBalanceToStoredValue
   * transfer a given amount from ACCONTS.BALANCE to HTT_CUSTOMERS_OVERLAY_ULTRA.STORED_VALUE for the purpose of monthly recharge
   * @see MOB-113
   * @param integer amount
   * @return object Result
   */
  public function portal__ApplyBalanceToStoredValue()
  {
    // initialize
    list ($amount) = $this->getInputValues();

    try
    {
      // verify session
      if (!$customer_id = $this->session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');

      // get subscriber object
      $this->utilities->teldataChangeDb();
      if ( ! $customer = $this->customerRepository->getCombinedCustomerByCustomerId($customer_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
      $this->setOutputLanguage($customer->preferred_language);

      // check subscriber state: Active only
      $state = $this->stateActions->getStateFromCustomerId($customer_id);
      if ( ! $state || empty($state['state']))
        $this->errException('ERR_API_INTERNAL: failed to determine customer plan state', 'UN0001');
      if ($state['state'] != STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');

      // move amount from balance to stored value
      $result = $this->payment->funcSweepBalanceToStoredValue([
        'customer'      => $customer,
        'sweep_value'   => $amount / 100,
        'reference'     => __FUNCTION__,
        'detail'        => __FUNCTION__,
        'source'        => 'BalanceToStoredValue'
      ]);

      if ( ! $result['success'])
      {
        foreach ($result['errors'] as $error)
          $this->addError($error, 'VV0001');
        $this->errException('ERR_API_INTERNAL: failed to transfer amount to stored value', 'DB0001');
      }
      else
      {
        // send SMS
        $result_object = $this->messenger->enqueueImmediateSms(
          $customer_id,
          'balanceadd_recharge',
          [
            'reload_amount' => '$' . ($amount/100),
            'renewal_date'  => date('M d Y', strtotime($customer->plan_expires))
          ]
        );

        if ($result_object->get_errors()) {
          $this->addWarning('Failed to send SMS');
        }

        $this->succeed();
      }
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal;

class ClearCreditCard extends Portal
{
  /**
   * @var Session
   */
  private $session;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  private $customer;

  /**
   * ClearCreditCard constructor.
   * @param Session $session
   * @param CustomerRepository $customerRepository
   */
  public function __construct(Session $session, CustomerRepository $customerRepository)
  {
    $this->session = $session;
    $this->customerRepository = $customerRepository;
  }

  /**
   * portal__ClearCreditCard
   * Remove customer credit card info.
   * @return object Result
   */
  public function portal__ClearCreditCard()
  {
    try {
      // get customer from session
      if (!$this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');
      }

      teldata_change_db();
      $this->customer = $this->customerRepository->getCustomerById($this->session->customer_id, ['PREFERRED_LANGUAGE'], true);
      if (!$this->customer) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
      }

      $this->setOutputLanguage($this->customer->preferred_language);

      if ($this->customer->voidCreditCardInformation()) {
        // CCCP-18: also update ULTRA.CC_HOLDERS
        $ccResult = $this->customer->disableCreditCard();
        if ($ccResult->is_failure()) {
          return $this->errException('ERR_API_INTERNAL: failed to disable credit card', 'DB0001');
        }
      } else {
        return $this->errException('ERR_API_INTERNAL: DB error (2)', 'DB0001');
      }

      $this->succeed();
    } catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

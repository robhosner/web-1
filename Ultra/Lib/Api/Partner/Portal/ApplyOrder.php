<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Configuration\Configuration;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Lib\Flex;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;

class ApplyOrder extends Portal
{
  /**
   * @var Session
   */
  private $session;

  /**
   * @var Configuration
   */
  private $configuration;

  /**
   * @var CustomerRepository
   */
  private $customerRepo;

  /**
   * SaveCreditCard constructor.
   * @param Session $session
   * @param Configuration $configuration
   * @param FamilyAPI $familyAPI
   * @param OnlineSalesAPI $ordersAPI
   */
  public function __construct(
    Session $session,
    Configuration $configuration,
    CustomerRepository $customerRepo
  ) {
    $this->session = $session;
    $this->configuration = $configuration;
    $this->customerRepo = $customerRepo;
  }

  public function portal__ApplyOrder()
  {
    try {
      // verify session
      if (!$customer_id = $this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');
      }
      
      // check if customer is active to apply bolt ons immediately
      $customer = $this->customerRepo->getCustomerById($customer_id, ['plan_state']);
      if (!$customer) {
        throw new \Exception('Failed to lookup CUSTOMER record where customer_id = ' . $customer_id);
      }
      $withBoltOns = $customer->plan_state == STATE_ACTIVE;

      $result = Flex::applyCustomerOrder($customer_id, $withBoltOns);
      if (!$result->is_success()) {
        foreach ($result->get_errors() as $e) {
          $this->addError($e, 'IN0002', $e);
        }
        throw new \Exception('Failed to apply customer order');
      }

      $this->succeed();
    } catch(CustomErrorCodeException $e) {
      dlog('', $e->getMessage());
      $this->addError($e->getMessage(), $e->code(), $e->getMessage());
    } catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

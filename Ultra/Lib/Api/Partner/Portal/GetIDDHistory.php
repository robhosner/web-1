<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class GetIDDHistory extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__GetIDDHistory
   * Obtain the rows in HTT_BILLING_HISTORY for a customer.
   * @param epoch start_epoch
   * @param epoch end_epoch
   * @param integer recent
   * @return Result object
   */
  public function portal__GetIDDHistory()
  {
    // initialize
    list ($start_epoch, $end_epoch, $recent) = $this->getInputValues();
    $idd_history = array();

    try
    {
      $session = new \Session();
      if ( ! $session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');

      teldata_change_db();
      if ( ! $customer = get_ultra_customer_from_customer_id($session->customer_id, array('preferred_language')))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');

      if ( ! $account = get_account_from_customer_id($session->customer_id , array('ACCOUNT_ID')))
        $this->errException("ERR_API_INVALID_ARGUMENTS: account not found for customer : {$session->customer_id}");

      $this->setOutputLanguage($customer->preferred_language);

      $recent = empty($recent) ? 50 : $recent;
      $dateStatement = "@Start_Date = '" . date('Y-m-d H:i:s', (empty($end_epoch) ? time() : $end_epoch)) . "',";
      $query = logged_mssql_query("
        DECLARE @Return_Status BIT,
          @Return_Message varchar(8000);
        EXEC @Return_Status = ultra.Get_IDD_History
          @Top = $recent,
          @Account_ID = $account->ACCOUNT_ID,
          @Customer_id = $session->customer_id,
          $dateStatement
          @Error_Message = @Return_Message OUTPUT"
      );

      $result = [];

      while ($row = mssql_fetch_object($query)) {
        $item = [];
        $item['history_epoch_pst'] = strtotime($row->START_DATE_TIME_PT);
        $item['destination'] = $row->DESTINATION;
        $item['destination_number'] = $row->DESTINATION_NUMBER;
        $item['minutes'] = $row->MINUTES;
        $item['rate'] = $row->RATE;
        $item['end_epoch'] = strtotime($row->START_DATE_TIME_PT); // THIS IS NOT NEEDED
        $result[] = $item;
      }

      // MOBIII-23: return as array of objects
      if ((is_array($result)) && count($result) > 0)
        $this->addToOutput('idd_history', $result);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}


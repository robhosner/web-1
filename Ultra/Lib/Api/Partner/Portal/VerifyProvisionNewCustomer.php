<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class VerifyProvisionNewCustomer extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__VerifyProvisionNewCustomer
   *
   * this is the API to be invoked after portal__RequestProvisionNewCustomer in order to get the complete response
   * behaves like provisioning__verifyProvisionNewCustomerAsync
   *
   * @param string zsession
   * @param string request_id transition UUID
   * @return Result object
   */
  public function portal__VerifyProvisionNewCustomer()
  {
    // init
    list ($zsession, $request_id) = $this->getInputValues();

    $this->addToOutput('provision_pending', NULL);
    $this->addToOutput('phone_number', NULL);
    $this->addToOutput('provision_status', NULL);

    try
    {
      // get customer from zsession
      teldata_change_db();
      if (! $customer = getCustomerFromSession($zsession))
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');

      $this->setOutputLanguage($customer->preferred_language);

      // check transition
      $transition_result = provision_check_transition($request_id);
      $errors = append_transition_failure_reason($transition_result['errors'], $request_id);

      // set return values
      $this->addToOutput('provision_pending', $transition_result['pending']);
      $this->addToOutput('phone_number', $transition_result['phone_number']);
      $this->addToOutput('provision_status', $transition_result['status']);

      if ($errors && is_array($errors) && count($errors))
        $this->errException('ERR_API_INTERNAL: provisioning errors - ' . implode('; ', $errors), 'PR0001');

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'ERROR: ' . $e->getMessage());
    }

    return $this->result;
  }

}

?>
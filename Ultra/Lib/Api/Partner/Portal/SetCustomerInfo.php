<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Utilities\Common;

/**
 * Class SetCustomerInfo
 * @package Ultra\Lib\Api\Partner\Portal
 */
class SetCustomerInfo extends Portal
{
  /**
   * @var Session
   */
  private $session;
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Customer
   */
  private $customer;
  /**
   * @var Common
   */
  private $utilities;

  /**
   * SetCustomerInfo constructor.
   * @param Session $session
   * @param CustomerRepository $customerRepository
   * @param Common $utilities
   */
  public function __construct(Session $session, CustomerRepository $customerRepository, Common $utilities)
  {
    $this->session = $session;
    $this->customerRepository = $customerRepository;
    $this->utilities = $utilities;
  }

  /**
   * portal__SetCustomerInfo
   * update customer personal information
   * @see MOBTO-41
   */
  public function portal__SetCustomerInfo()
  {
    list (
      $first_name ,           // CUSTOMERS.FIRST_NAME
      $last_name ,            // CUSTOMERS.LAST_NAME
      $e_mail ,               // CUSTOMERS.E_MAIL
      $login_password         // CUSTOMERS.LOGIN_PASSWORD
    ) = $this->getInputValues();

    try {
      // verify session
      if (!$this->session->customer_id) {
        return $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007');
      }

      // get subscriber object
      teldata_change_db();
      $this->customer = $this->customerRepository->getCustomerById($this->session->customer_id, ['plan_state'], true);

      if (!$this->customer) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');
      }

      // cannot modify Cancelled subscriber
      if ($this->customer->plan_state == STATE_CANCELLED) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');
      }

      // update CUSTOMERS table
      $values = [];
      foreach (['first_name', 'last_name', 'e_mail', 'login_password'] as $name) {
        if ($value = trim($$name)) {
          $values[$name] = $value;

          if ($name == 'login_password') {
            $values[$name] = $this->utilities->encryptPasswordHS($value);
          }
        }
      }

      if (empty($values)) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing', 'MP0001');
      }

      if (!$this->customerRepository->updateCustomerByCustomerId($this->customer->customer_id, $values)) {
        return $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred (2)', 'DB0001');
      }

      $this->succeed();
    }
    catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

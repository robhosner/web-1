<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

use Ultra\Lib\Api\Traits\FlexHandler;
use Ultra\Lib\Flex;
use Ultra\Lib\Services\FamilyAPI;

class RequestProvisionNewCustomer extends \Ultra\Lib\Api\Partner\Portal
{
  use FlexHandler;

  private $familyAPI;

  public function __construct(FamilyAPI $familyAPI)
  {
    $this->familyAPI = $familyAPI;
  }

  /**
   * portal__RequestProvisionNewCustomer
   *
   * create a customer provision the SIM (activate and suspend)
   *
   * @param string ICCID
   * @param string zipcode
   * @param string target_plan
   * @param preferred_language
   * @return Result object
   */
  public function portal__RequestProvisionNewCustomer()
  {
    list ( $ICCID , $zipcode , $target_plan , $preferred_language , $bolt_ons , $invite_code ) = $this->getInputValues();
    $bolt_ons = empty($bolt_ons) ? array() : (is_array($bolt_ons) ? $bolt_ons : array($bolt_ons));

    $masteragent = self::DEFAULT_ACTIVATION_MASTERAGENT;
    $distributor = self::DEFAULT_ACTIVATION_DISTRIBUTOR;
    $dealer = self::DEFAULT_ACTIVATION_DEALER;
    $userid = self::DEFAULT_ACTIVATION_USER_ID;

    $this->addToOutput('request_id', NULL);
    $this->addToOutput('zsession', NULL);
    $this->addToOutput('customer_id', null);

    try
    {
      // connect to the DB
      teldata_change_db();

      $isFlexPlan = Flex::isFlexPlan($target_plan);

      // verify that activations are enabled
      if ( ! activations_enabled())
        $this->errException('ERR_API_INTERNAL: activations disabled', 'AP0001', 'Activations are unavailable, please try again later');

      // validate ICCID
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccid19userstatus('ICCID',$ICCID,'VALID');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // verify that $zipcode is allowed (in coverage)
      list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode('zipcode',$zipcode,'in_coverage');
      if ( ! empty( $error ) )
        $this->errException( $error , $error_code );

      // check invite code, if given
      if ($isFlexPlan && $invite_code) {
        $this->validateInviteCode($invite_code);
      }

      // get Bolt Ons info
      $new_bolt_on_configuration = array();

      // loop through all provided Recurring Bolt On Ids
      $bolt_ons_cost = 0;
      foreach ($bolt_ons as $bolt_on_id)
        if ($bolt_on_id = trim($bolt_on_id))
        {
          $boltOnInfo = \Ultra\UltraConfig\getBoltOnInfo( $bolt_on_id );

          if ( ! $boltOnInfo )
            $this->errException('ERR_API_INVALID_ARGUMENTS: bolt_on_id not valid', 'VV0101');

          dlog('',"%s : boltOnInfo = %s",$bolt_on_id,$boltOnInfo);

          $new_bolt_on_configuration[ $boltOnInfo['option_attribute'] ] = $boltOnInfo['cost'];
          $bolt_ons_cost += $boltOnInfo['cost'];
        }

      // get SIM info
      $sim = get_htt_inventory_sim_from_iccid($ICCID);
      if (! $sim)
        $this->errException('ERR_API_INVALID_ARGUMENTS: invalid SIM card', 'VV0065');

      // validate SIM brand
      \logit( "API partner = {$this->partner} ; BRAND_ID = {$sim->BRAND_ID}" );

      if ( ! \Ultra\UltraConfig\isBrandAllowedByAPIPartner( $sim->BRAND_ID , $this->partner ) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: The SIM Brand is invalid', 'IC0003');

      // validate SIM
      if (! validate_ICCID($sim, TRUE))
        $this->errException('ERR_API_INVALID_ARGUMENTS: SIM cannot be used', 'VV0065');

      // check if the SIM is good to activate
      $redis = new \Ultra\Lib\Util\Redis;
      if ($redis->get('iccid/good_to_activate/' . $ICCID))
        $redis->del('iccid/good_to_activate/' . $ICCID);
      else
      {
        $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
        $result = $mwControl->mwCanActivate(
          array(
            'iccid'      => $ICCID,
            'actionUUID' => getNewActionUUID(__FUNCTION__ . time())));

        if ($result->is_failure())
          $this->errException('ERR_API_INVALID_ARGUMENTS: ICCID cannot be activated', 'VV0066');
      }

      // create ULTRA customer in DB
      $params = array(
        'preferred_language'    => empty($preferred_language) ? 'EN' : $preferred_language,
        'cos_id'                => get_cos_id_from_plan('STANDBY'),
        'activation_cos_id'     => get_cos_id_from_plan($target_plan),
        'postal_code'           => $zipcode,
        'country'               => 'USA',
        'plan_state'            => 'Neutral',
        'plan_started'          => 'NULL',
        'plan_expires'          => 'NULL',
        'customer_source'       => 'PORTAL',
        'current_iccid'         => substr($ICCID, 0, -1),
        'current_iccid_full'    => $ICCID,
        'masteragent'           => $sim->INVENTORY_MASTERAGENT ? $sim->INVENTORY_MASTERAGENT : $masteragent,
        'distributor'           => $sim->INVENTORY_DISTRIBUTOR ? $sim->INVENTORY_DISTRIBUTOR : $distributor,
        'dealer'                => $dealer,
        'userid'                => $userid,
        'promised_amount'       => get_plan_cost_by_cos_id(get_cos_id_from_plan($target_plan)) + $bolt_ons_cost);
      $result = create_ultra_customer_db_transaction($params);
      $customer = $result['customer'];

      if ( ! $customer)
        $this->errException('ERR_API_INTERNAL: failed to create customer' , 'DB0001');

      customer_reset_first_last_name($customer->CUSTOMER_ID);

      $this->addToOutput('customer_id', $customer->CUSTOMER_ID);

      // handle Flex plan
      if ($isFlexPlan && !empty($invite_code)) {
        $this->joinFamily($customer->CUSTOMER_ID, $invite_code);
      }

      // configure Recurring Bolt Ons

      dlog('',"new_bolt_on_configuration = %s",$new_bolt_on_configuration);

      // modify configuration in ULTRA.CUSTOMER_OPTIONS and log into ULTRA.BOLTON_TRACKER
      $result_bolt_ons = \update_bolt_ons_values_to_customer_options( $customer->CUSTOMER_ID , array() , $new_bolt_on_configuration , 'PORTAL' );

      // $result_bolt_ons failures will be ignored

      // prepare Neutral->Provisioned transition
      $context          = array('customer_id' => $customer->CUSTOMER_ID);
      $resolve_now      = FALSE;
      $dry_run          = TRUE;
      $plan             = get_plan_name_from_short_name($target_plan);
      $transition_name  = "Provision $plan";
      $max_path_depth   = 1;
      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

      if ($result_status['success'] != 1)
        $this->errException('ERR_API_INTERNAL: state transition error (1)', 'SM0001');

      // save transition info
      // PROD-868: HTT_ACTIVATION_LOG.ACTIVATED_MASTERAGENT and DISTRIBUTOR differ from HTT_ACTIVATION_HISTORY
      if ( ! $dealer_info = \Ultra\Lib\DB\Celluphone\getDealerInfo(NULL, $dealer))
        $this->errException('ERR_API_INTERNAL: No celluphone dealer info found', 'ND0003');
      set_redis_provisioning_values($customer->CUSTOMER_ID, $dealer_info->masterId, $dealer, $dealer_info->distributorId, $dealer, $userid, $redis);

      // initiate transition, but don't wait for it to resolve
      $dry_run = FALSE;
      $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);
      if ( ! $result_status['success'] )
      {
        // clear activation attribution info from redis
        clear_redis_provisioning_values($customer->CUSTOMER_ID);

        $this->errException( 'ERR_API_INTERNAL: state transition error (2)', 'SM0001');
      }

      reserve_iccid($ICCID);

      // update ULTRA.HTT_ACTIVATION_HISTORY
      $sql = ultra_activation_history_update_query(
        array(
          'customer_id'     => $customer->CUSTOMER_ID,
          'activation_type' => 'NEW'));
      if ( ! is_mssql_successful(logged_mssql_query($sql)))
        dlog('', 'ERROR: failed to update ULTRA.HTT_ACTIVATION_HISTORY');
      flush_ultra_activation_history_cache_by_customer_id($customer->CUSTOMER_ID);
      $this->addToOutput('request_id', $result_status['transitions'][0]);

      // handle Flex
      if ($isFlexPlan && empty($invite_code)) {
        $this->createFamily($customer->CUSTOMER_ID);
      }

      // create zsession
      $_REQUEST['zsession'] = session_save($customer, NULL, $redis);
      $verified = verify_session($customer->CUSTOMER);
      if (! $verified)
        $this->errException('ERR_API_INTERNAL: failed to create user session', 'SE0001');
      $this->addToOutput('zsession', $verified[2]);
      setcookielive('zsession', $verified[2], $verified[1], '/', $_SERVER['SERVER_NAME']);

      $session = new \Session();

      if (!$session->confirm($customer->CUSTOMER_ID))
      {
        throw new \Exception('ERR_API_INTERNAL: A problem occurred when trying to create a session.');
      }

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog('', 'ERROR: ' . $e->getMessage());
    }

    $fraudStatus = $this->isSuccess() ? 'success' : 'error';
    $data = array('source' => json_encode($this->getInputValues()));
    $customer = (isset($customer) && is_object($customer)) ? $customer : null;

    fraud_event($customer, 'portal', 'RequestProvisionNewCustomer', $fraudStatus, $data);

    return $this->result;
  }
}


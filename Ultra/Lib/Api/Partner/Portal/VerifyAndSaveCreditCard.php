<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class VerifyAndSaveCreditCard extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__VerifyAndSaveCreditCard
   * Set credit card fields for a customer after appropriate tokenization and validation
   * this API shall is based on and shall retire portal__VerifyAndSaveCustomerCC due to latter's improper use
   * @return Result object
   */
  public function portal__VerifyAndSaveCreditCard()
  {
    // initialize
    list ($cc_name, $cc_address1, $cc_address2, $cc_city, $cc_country, $cc_state_or_region, $cc_postal_code, $account_cc_exp, $account_cc_cvv, $account_cc_number, $token, $processor) = $this->getInputValues();

    try
    {
      $session = new \Session();
      if ( ! $session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');

      teldata_change_db();
      if ( ! $customer = get_customer_from_customer_id($session->customer_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');

      $this->setOutputLanguage($customer->preferred_language);

      // default to MeS processor
      $processor = !empty($processor) ? strtolower($processor) : 'mes';

      $result = \Ultra\Lib\DB\Setter\Customer\cc_info(
        array(
          'customer_id'     => $customer->CUSTOMER_ID,
          'cc_number'       => $account_cc_number,
          'cvv'             => $account_cc_cvv,
          'expires_date'    => $account_cc_exp,
          'cc_address1'     => $cc_address1,
          'cc_address2'     => $cc_address2,
          'cc_postal_code'  => $cc_postal_code,
          'cc_name'         => $cc_name,
          'cc_city'         => $cc_city,
          'cc_country'      => $cc_country,
          'cc_state_region' => $cc_state_or_region,
          'token'           => $token,
          'processor'       => $processor
        ));

      if ( $result->is_failure())
      {
        $errors = $result->get_errors();
        if ( isset($result->data_array['error_codes']) && count( $result->data_array['error_codes']))
          $this->errException( $errors[0] , $result->data_array['error_codes'][0]);
        else
          $this->errException( $errors[0], 'DB0001');
      }

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog( '' , $e->getMessage ());
    }

    return $this->result;
  }
}

?>
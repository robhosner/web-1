<?php

namespace Ultra\Lib\Api\Partner\Portal;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class CheckSession extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__CheckSession
   * checks cookie based session status of currently logged in customer
   * @return Result object
   */
  public function portal__CheckSession()
  {
    // initialize
    $expiration = NULL;

    try
    {
      $session = new \Session();
      $redis = new \Ultra\Lib\Util\Redis;

      // this API has potential for brute force break in, therefore check abuse
      if (checkApiAbuseByIdentifier(__FUNCTION__, $session->ip, self::MAX_FAILED_CHECKS, $redis, FALSE))
        $this->errException('ERR_API_INTERNAL: This command has been currently disabled. Please try again later.', 'AP0001', 'Your login failed too many times.');

      if ( ! $session->customer_id)
      {
        checkApiAbuseByIdentifier(__FUNCTION__, $session->ip, self::MAX_FAILED_LOGINS, $redis); // record failed attempt
        $this->errException('ERR_API_INTERNAL: Login session expired.', 'SE0003', 'Your login has expired.');
      }

      $expiration = $session->expiration;
      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('expiration', $expiration);
    return $this->result;
  }
}


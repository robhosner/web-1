<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal;
use Ultra\Messaging\Messenger;
use Ultra\Utilities\SessionUtilities;

class SendToken extends Portal
{
  /**
   * @var SessionUtilities
   */
  private $sessionUtilities;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Messenger
   */
  private $messenger;

  /**
   * SendToken constructor.
   * @param CustomerRepository $customerRepository
   * @param SessionUtilities $sessionUtilities
   * @param Messenger $messenger
   */
  public function __construct(CustomerRepository $customerRepository, SessionUtilities $sessionUtilities, Messenger $messenger)
  {
    $this->customerRepository = $customerRepository;
    $this->sessionUtilities = $sessionUtilities;
    $this->messenger = $messenger;
  }

  /**
   * portal__SendToken
   * create a new pre-authorization token and send it to the given MSISDN via SMS
   * if no MSISDN and expired token is passed, re-send a new token to the MSISDN embedded in it
   * @see MOB-40
   */
  public function portal__SendToken()
  {
    // initialize
    list ($url, $route, $msisdn, $token, $error_language, $brand) = $this->getInputValues();

    try {
      $session = $this->sessionUtilities->getSession($msisdn, $token);
      teldata_change_db();

      // either MSISDN or token is required
      if (empty($msisdn) && empty($token)) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing.', 'MP0001');
      }

      // check API abuse by IP
      if ($this->sessionUtilities->checkApiAbuseByIdentifier(__FUNCTION__, $session->ip, self::MAX_FAILED_LOGINS, false)) {
        return $this->errException('ERR_API_INTERNAL: This command has been currently disabled. Please try again later.', 'AP0001');
      }

      // get session pre-authorization token
      $token = $session->token;
      if (!$token) {
        return $this->errException('ERR_API_INTERNAL: could not generate a security token.', 'SE0001');
      }

      // retrieve subscriber by MSISDN
      $customer = $this->customerRepository->getCustomerFromMsisdn($session->msisdn);
      if (!$customer) {
        $this->sessionUtilities->checkApiAbuseByIdentifier(__FUNCTION__, $session->ip, self::MAX_FAILED_LOGINS); // record failed attempt
        return $this->errException('ERR_API_INTERNAL: The customer does not exist.', 'VV0031');
      }

      $result = $this->messenger->enqueueImmediateSms($customer->CUSTOMER_ID, 'send_login_token', ['url' => "$url/$token/$route"]);

      if ($result->is_failure()) {
        return $this->errException('ERR_API_INTERNAL: SMS delivery error.', 'VV0031');
      }

      // PROD-1791: reset failed login counter (subscriber may login either with MSISDN or username)
      $this->sessionUtilities->resetCounter('portal__Login', $customer->current_mobile_number);

      if (!empty($customer->LOGIN_NAME)) {
        $this->sessionUtilities->resetCounter('portal__Login', $customer->LOGIN_NAME);
      }

      $this->succeed();
    }

    catch (\Exception $e) {
      dlog('', $e->getMessage());
    }

    $this->addToOutput('token', $token);
    return $this->result;
  }
}

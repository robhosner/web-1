<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Customers\Customer;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal;

class UnsubscribeFromEmail extends Portal
{
  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * UnsubscribeFromEmail constructor.
   * @param CustomerRepository $customerRepository
   */
  public function __construct(CustomerRepository $customerRepository)
  {
    $this->customerRepository = $customerRepository;
  }

  /**
   * @var Customer
   */
  private $customer;

  /**
   * portal__UnsubscribeFromEmail
   */
  public function portal__UnsubscribeFromEmail()
  {
    list($customer_id, $msisdn) = $this->getInputValues();

    try {
      teldata_change_db();

      // MSISDN for $customer_id must match input msisdn
      $this->customer = $this->customerRepository->getCustomerById($customer_id, ['current_mobile_number'], true);
      if (!$this->customer || $this->customer->current_mobile_number != $msisdn) {
        return $this->errException('ERR_API_INVALID_ARGUMENTS: This unsubscribe request is invalid.', 'VV0031');
      }

      // returns empty array or array of errors
      $errors = $this->customer->setMarketingSettings(['customer_id' => $customer_id, 'marketing_email_option' => 0]);

      if (count($errors)) {
        return $this->errException('ERR_API_INTERNAL: could not update marketing settings', 'DB0001');
      }

      $this->succeed();
    }
    catch(\Exception $e) {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}

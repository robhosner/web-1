<?php

namespace Ultra\Lib\Api\Partner\Portal;

use SalesTaxCalculator;
use function Ultra\UltraConfig\isBPlan;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class CalculateTaxesFees extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * @var SalesTaxCalculator
   */
  private $salesTaxCalculator;

  /**
   * CalculateTaxesFees constructor.
   * @param SalesTaxCalculator $salesTaxCalculator
   */
  public function __construct(SalesTaxCalculator $salesTaxCalculator)
  {
    $this->salesTaxCalculator = $salesTaxCalculator;
  }

  /**
   * portal__CalculateTaxesFees
   *
   * Determine the Sales Tax and Regulatory fees that would be charged for a potential Credit Card purchase, provided a purchase amount.
   *
   * @param int charge_amount amount of potential credit card purchase
   * @param string zip if provided, calculate based on value
   * @param boolean check_taxes subject to sales tax?
   * @param boolean check_recovery_fee subject to recovery fee?
   * @return Result object
   */
  public function portal__CalculateTaxesFees()
  {
    list ($charge_amount, $zip, $check_taxes, $check_recovery_fee) = $this->getInputValues();

    $sales_tax    = 0;
    $recovery_fee = 0;

    try
    {
      // get customer from cookie Session
      $session = new \Session();
      if ( ! $session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');

      teldata_change_db();

      $customer         = get_ultra_customer_from_customer_id($session->customer_id, array('customer_id', 'preferred_language', 'MONTHLY_RENEWAL_TARGET'));
      $accounts_result  = get_account_from_customer_id($session->customer_id, array('ACCOUNT', 'COS_ID', 'ACCOUNT_ID'));
      $customers_result = customers_select_customer(array('customer_id' => $session->customer_id,
        'select_fields' => array('CC_POSTAL_CODE', 'POSTAL_CODE')));

      if ( ! $customer || ! is_object($customer)
        || (!isset($customers_result->CC_POSTAL_CODE) || !isset($customers_result->POSTAL_CODE)) 
        || (!isset($accounts_result->ACCOUNT))
      )
      {
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found', 'VV0031');
      }
      else
      {
        $customer->ACCOUNT        = $accounts_result->ACCOUNT;
        $customer->CC_POSTAL_CODE = $customers_result->CC_POSTAL_CODE;
        $customer->POSTAL_CODE    = $customers_result->POSTAL_CODE;
      }

      $this->setOutputLanguage($customer->preferred_language);

      // get zip code
      if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
        $zip = $customer->CC_POSTAL_CODE;

      if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
        $zip = $customer->POSTAL_CODE;

      if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
        $this->errException('ERR_API_INVALID_ARGUMENTS: No zip code found for this customer', 'MP0007');

      if ($charge_amount >= 5700 && !empty($customer->MONTHLY_RENEWAL_TARGET)) {
        $cosId = get_cos_id_from_plan($customer->MONTHLY_RENEWAL_TARGET);

        if (isBPlan($cosId)) {
          $accounts_result->COS_ID = $cosId;
        }
      }

      if ( $check_taxes )
      {
        // calculate taxes and fees if 'check_taxes' is TRUE
        $this->salesTaxCalculator->setBillingInfoByCustomerId($customer->customer_id);
        $this->salesTaxCalculator->setAccount($accounts_result->ACCOUNT);
        $this->salesTaxCalculator->setCosId($accounts_result->COS_ID);
        $this->salesTaxCalculator->setBillingZipCode($zip);
        $this->salesTaxCalculator->setAmount($charge_amount);

        $result = $this->salesTaxCalculator->calculateTaxesFees();

        if ( $result->is_failure() )
        {
          $errors = $result->get_errors();
          $this->errException('ERR_API_INVALID_ARGUMENTS: ' . $errors[0], 'MP0007');
        }

        $sales_tax = $result->data_array['sales_tax'] + $result->data_array['mts_tax'];

        if ( $check_recovery_fee )
          $recovery_fee = $result->data_array['recovery_fee'];
      }
      elseif( $check_recovery_fee )
        // calculate fees
        $recovery_fee = $this->salesTaxCalculator->getRecoveryFee($charge_amount, $accounts_result->COS_ID)['recovery_fee'];

      $this->addToOutput('sales_tax', $sales_tax);
      $this->addToOutput('recovery_fee', $recovery_fee);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}


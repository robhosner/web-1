<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Lib\DB\Customer as Customer;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

class VerifyAndSaveCustomerCC extends \Ultra\Lib\Api\Partner\Portal
{
  /**
   * portal__VerifyAndSaveCustomerCC
   *
   * Set credit card fields for a customer after appropriate tokenization and validation.
   *
   * @return Result object
   */
  public function portal__VerifyAndSaveCustomerCC()
  {
    list ( $zsession , $cc_name , $cc_address1 , $cc_address2 , $cc_city , $cc_country , $cc_state_or_region , $cc_postal_code , $account_cc_exp , $account_cc_cvv , $account_cc_number, $enroll_auto_recharge)
      = $this->getInputValues();

    $error_code = '';

    try
    {
      teldata_change_db();

      // get customer from zsession
      if (! $customer = getCustomerFromSession($zsession))
        $this->errException('ERR_API_INTERNAL: user not logged in', 'SE0007');

      $this->setOutputLanguage($customer->preferred_language);

      $result = \Ultra\Lib\DB\Setter\Customer\cc_info(
        array(
          'customer_id'    => $customer->CUSTOMER_ID,
          'cc_number'      => $account_cc_number,
          'cvv'            => $account_cc_cvv,
          'expires_date'   => $account_cc_exp,
          'cc_address1'    => $cc_address1,
          'cc_address2'    => $cc_address2,
          'cc_postal_code' => $cc_postal_code,
          'cc_name'        => $cc_name,
          'cc_city'        => $cc_city,
          'cc_country'     => $cc_country,
          'cc_state_region' => $cc_state_or_region
        )
      );

      if ( $result->is_failure() )
      {
        $errors = $result->get_errors();

        $userErrors = isset($result->data_array['user_errors'][$customer->preferred_language]) ? $result->data_array['user_errors'][$customer->preferred_language] : null;

        if ( isset($result->data_array['error_codes']) && count( $result->data_array['error_codes'] ) )
          $this->errException( $errors[0] , $result->data_array['error_codes'][0] , $userErrors);
        else
          $this->errException( $errors[0] , 'DB0001' );
      }

      if ($enroll_auto_recharge || ($customer->monthly_cc_renewal == 1 && $enroll_auto_recharge == "0"))
      {
        Customer\updateAutoRecharge($customer->CUSTOMER_ID, $enroll_auto_recharge);
      }

      $this->addToOutput('enroll_auto_recharge', $enroll_auto_recharge);

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}
?>
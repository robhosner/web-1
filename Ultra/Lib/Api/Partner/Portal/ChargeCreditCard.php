<?php

namespace Ultra\Lib\Api\Partner\Portal;

use Ultra\Lib\DB\Customer as Customer;

require_once 'Ultra/Lib/Api/Partner/Portal.php';

use Ultra\FeatureFlags\FeatureFlagsClientWrapper;

class ChargeCreditCard extends \Ultra\Lib\Api\Partner\Portal
{
  private $flagsObj;

  public function __construct(FeatureFlagsClientWrapper $flagsObj)
  {
    $this->flagsObj = $flagsObj;
  }

  /**
   * portal__ChargeCreditCard
   * charge customers credit card on file previously saved with portal__VerifyAndSaveCreditCard
   * @return Result object
   */
  public function portal__ChargeCreditCard()
  {
    // init
    list ($charge_amount, $destination) = $this->getInputValues();

    try
    {
      $session = new \Session();
      if ( ! $customer_id = $session->customer_id)
        $this->errException('ERR_API_INTERNAL: user not logged in.', 'SE0007', 'Please log in first.');

      teldata_change_db();
      if ( ! $customer = get_customer_from_customer_id($session->customer_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: customer not found.', 'VV0031');

      $this->setOutputLanguage($customer->preferred_language);

      // block if customer called this API more than 5 times in an hour
      if (checkApiAbuseByIdentifier(__FUNCTION__, $customer->CUSTOMER_ID, 5))
        $this->errException('ERR_API_INTERNAL: command disabled; please try again later', 'AP0001');

      $redis = new \Ultra\Lib\Util\Redis();
      if (!$this->flagsObj->disableRateLimits($customer->CUSTOMER_ID))
        if ($redis->get("creditcardcharge/$customer_id/$charge_amount"))
          $this->errException('ERR_API_INTERNAL: duplicate charge; please try again later', 'AP0001');

      // fail if customer does not have credit card info in the DB
      if ( ! customer_has_credit_card($customer))
        $this->errException('ERR_API_INVALID_ARGUMENTS: No credit card on file.', 'CC0005', 'You do not have a credit card on file.');

      // get current customer state
      $state = internal_func_get_state_from_customer_id($customer->CUSTOMER_ID);
      if ( ! $state )
        $this->errException('ERR_API_INTERNAL: customer state could not be determined', 'UN0001');
      if ( ! isset($state['state']) || $state['state'] == STATE_CANCELLED)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');

      if ($destination == 'WALLET')
      {
        // maximum allowed wallet balance is 200
        // https://issues.hometowntelecom.com:8443/browse/API-1168
        if ( (($charge_amount / 100) + $customer->BALANCE) > 200 )
          $this->errException('ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet', 'CH0001');
      }
      else
      {
        // set the customer's maximum allowed stored_value to be the greater of their current plan amount and future plan amount
        // https://issues.hometowntelecom.com:8443/browse/API-1168

        // plan cost returns in cents
        $plan_cost     = get_plan_cost_from_cos_id($customer->cos_id);
        $bolt_ons_cost = get_bolt_ons_costs_by_customer_id( $customer->CUSTOMER_ID );

        // target cost in cents
        $target_cost = ($customer->MONTHLY_RENEWAL_TARGET)
          ? get_plan_cost_from_cos_id(get_cos_id_from_plan($customer->MONTHLY_RENEWAL_TARGET))
          : 0;

        // use greater of two costs
        $plan_cost = ($plan_cost > $target_cost) ? $plan_cost : $target_cost;

        if ( (($charge_amount / 100) + $customer->stored_value) > ($plan_cost / 100) + $bolt_ons_cost )
          $this->errException('ERR_API_INVALID_ARGUMENTS: Charge would exceed maximum allowed stored value amount', 'VV0115');
      }

      // charge customer's credit card
      $disableFraudCheck = $this->flagsObj->disableRateLimits($customer->CUSTOMER_ID);
      $result = func_add_funds_by_tokenized_cc(
        array(
          'customer'            => $customer,
          'charge_amount'       => $charge_amount / 100,
          'detail'              => __FUNCTION__,
          'include_taxes_fees'  => TRUE,
          'description'         => $destination == 'WALLET' ? 'Wallet Balance Added Using Credit Card' : 'Plan Recharge Using Credit Card',
          'reason'              => $destination == 'WALLET' ? 'ADD_BALANCE' : 'RECHARGE',
          'session'             => $this->getRequestId(),
          'fund_destination'    => $destination == 'WALLET' ? 'balance' : 'stored_value'
        ),
        $disableFraudCheck
      );
      if ( $result->is_failure())
      {
        dlog('', 'ERRORS: %s', $result->get_errors());
        $this->errException('ERR_API_INTERNAL: credit card transaction not successful', 'CC0001');
      }

      $redis->set("creditcardcharge/$customer_id/$charge_amount", 1, 5 * 60);

      // send SMS for stored value load
      if ($destination != 'WALLET') {
        $messenger = new \Ultra\Messaging\Messenger();
        $result_object = $messenger->enqueueImmediateSms(
          $customer_id,
          'balanceadd_recharge',
          [
            'reload_amount' => '$' . ($charge_amount/100),
            'renewal_date'  => date('M d Y', strtotime($customer->plan_expires))
          ]
        );

        if ($result_object->get_errors()) {
          $this->addWarning('Failed to send SMS');
        }

      }

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage());
    }

    return $this->result;
  }
}

?>
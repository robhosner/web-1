<?php
namespace Ultra\Lib\Api\Partner\Portal;

use Session;
use Ultra\Lib\Api\Partner\Portal;

use Ultra\CreditCards\Repositories\Mssql\CreditCardRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Orders\Repositories\Mssql\OrderRepository;
use Ultra\Sims\Repositories\Mssql\SimRepository;

class ClaimCreditCardByActCode extends Portal
{
    /**
   * @var SimRepository
   */
  private $simRepository;

  /**
   * @var OrderRepository
   */
  private $orderRepository;

  /**
   * @var CreditCardRepository
   */
  private $creditCardRepository;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * @var Session
   */
  private $session;

  /**
   * ClaimCreditCardByActCode constructor.
   * @param SimRepository $simRepository
   * @param OrderRepository $orderRepository
   * @param CreditCardRepository $creditCardRepository
   * @param CustomerRepository $customerRepository
   * @param Session $session
   */
  public function __construct(
    SimRepository $simRepository,
    OrderRepository $orderRepository,
    CreditCardRepository $creditCardRepository,
    CustomerRepository $customerRepository,
    Session $session)
  {
    $this->simRepository        = $simRepository;
    $this->orderRepository      = $orderRepository;
    $this->creditCardRepository = $creditCardRepository;
    $this->customerRepository   = $customerRepository;
    $this->session = $session;
  }

  /**
   * @return Int $customer_id
   */
  public function getCustomerIdFromSession()
  {
    $customer_id = null;

    if ( ! $customer_id = $this->session->customer_id)
    {
      if ( ! empty($_COOKIE['zsession']))
      {
        $customer    = $this->customerRepository->getCustomerFromSession($_COOKIE['zsession']);
        $customer_id = $customer->customer_id;
      }
    }

    return $customer_id;
  }

  /**
   * portal__ClaimCreditCardByActCode
   * @return Result object
   */
  public function portal__ClaimCreditCardByActCode()
  {
    // init
    list ($customer_id, $actcode, $postal_code, $auto_enroll) = $this->getInputValues();

    try
    {
      $customer_id = $this->getCustomerIdFromSession();
      if ( ! $customer_id )
        $this->errException('ERR_API_INTERNAL: login session has expired', 'SE0003');

      if (empty($actcode))
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing actcode', 'MP0001');

      if (empty($postal_code))
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing postal_code', 'MP0001');

      if (empty($auto_enroll) && (int)$auto_enroll !== 0 )
        $this->errException('ERR_API_INVALID_ARGUMENTS: missing auto_enroll', 'MP0001');

      teldata_change_db();

      $sim = $this->simRepository->getSimFromActCode($actcode);

      if ( ! $sim)
        $this->errException('ERR_API_INTERNAL: invalid actcode', 'ND0001');

      $online_sale = $this->orderRepository->getOrderByIccid($sim->ICCID_FULL,
        [
          'postal_code',
          'bin',
          'last_four',
          'expires_date',
          'cvv_validation',
          'avs_validation',
          'gateway',
          'merchant_account',
          'token',
          'customer_ip',
          'first_name',
          'last_name',
          'address1',
          'address2',
          'city',
          'state'
        ]
      );

      if ( ! $online_sale)
        $this->errException('ERR_API_INTERNAL: cannot find online sale for actcode', 'IC0001');

      if ($online_sale->postal_code != $postal_code)
        $this->errException('ERR_API_INTERNAL: invalid postal_code', 'VV0051');

      // add warning if credit card already registered by user
      if ($this->creditCardRepository->getUltraCCHolderByDetails([
          'customer_id'   => $customer_id,
          'bin'           => $online_sale->bin,
          'last_four'     => $online_sale->last_four,
          'expires_date'  => $online_sale->expires_date
        ]))
      {
        $this->result->add_warning('Credit card already registered with CUSTOMER_ID ' . $customer_id);
      }
      else
      {
        // returns scope identity on true
        $check_cc_holders = $this->creditCardRepository->addToUltraCCHolders(
          [
            'customer_id'    => $customer_id,
            'bin'            => $online_sale->bin,
            'last_four'      => $online_sale->last_four,
            'expires_date'   => $online_sale->expires_date,
            'cvv_validation' => $online_sale->cvv_validation,
            'avs_validation' => $online_sale->avs_validation
          ]
        );


        if ( ! $check_cc_holders)
          $this->errException('ERR_API_INTERNAL: error inserting cc holder', 'DB0001');

        if ($cc_holders_id = $check_cc_holders)
        {
          $check_cc_holder_tokens = $this->creditCardRepository->addToUltraCCHolderTokens(
            [
              'cc_holders_id'    => $cc_holders_id,
              'gateway'          => $online_sale->gateway,
              'merchant_account' => $online_sale->merchant_account,
              'token'            => $online_sale->token
            ]
          );
        }

        if ( ! $check_cc_holder_tokens)
          $this->errException('ERR_API_INTERNAL: error inserting cc holder token', 'DB0001');
      }

      // throws exception on error
      if ($auto_enroll)
        $this->customerRepository->updateAutoRecharge($customer_id, $auto_enroll);

      // update credit card info in customers table
      $check_update_customer = $this->customerRepository->updateCustomerByCustomerId($customer_id, [
        'cc_name'         => $online_sale->first_name . ' ' . $online_sale->last_name,
        'cc_address1'     => $online_sale->address1,
        'cc_address2'     => $online_sale->address2,
        'cc_city'         => $online_sale->city,
        'cc_state_region' => $online_sale->state,
        'cc_postal_code'  => $online_sale->postal_code,
        'cc_exp_date'     => $online_sale->expires_date
      ]);

      if ( ! $check_update_customer)
        $this->errException('ERR_API_INTERNAL: error updating customer info', 'DB0001');

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('' , 'EXCEPTION: ' . $e->getMessage());
    }

    return $this->result;
  }
}

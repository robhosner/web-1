<?php

namespace Ultra\Lib\Api\Partner\Mvneinternal;

require_once 'Ultra/Lib/Api/Partner/Mvneinternal.php';

class RemoveBoltonData extends \Ultra\Lib\Api\Partner\Mvneinternal
{

  /**
   * mvneinternal__RemoveBoltonData
   * invokes mwUpdatePlanAndFeaturesRaw ##
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Arch+Specs+for+Stair-Step+Throttle+and+RESETLIMIT
   * @return Result object
   */
  public function mvneinternal__RemoveBoltonData()
  {
    // initialization
    list ($customer_id, $amount) = $this->getInputValues();
    teldata_change_db();

    try
    {
      // validate subscriber
      if ( ! $customer = get_customer_from_customer_id($customer_id))
        $this->errException('ERR_API_INVALID_ARGUMENTS: Please enter the customer unique identifier', 'MP0020', 'Invalid subscriber ID');

      if (in_array($customer->BRAND_ID, [3]))
        $this->errException( "ERR_API_INVALID_ARGUMENTS: The API is not available for this brand." , 'FA0004' );

      if ( ! $customer->current_mobile_number)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Please enter a valid phone number', 'MP0003', 'Subscriber has no phone number');
      if ( ! $customer->CURRENT_ICCID_FULL)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Please enter a valid ICCID', 'MP0004', 'Subscriber has no ICCID');
      if ($customer->plan_state != STATE_ACTIVE)
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001', "Subsriber is {$customer->plan_state}");

      // perform CheckBalance
      $middleware = new \Ultra\Lib\MiddleWare\Adapter\Control;
      $balance = $this->checkBalance($middleware, $customer->current_mobile_number);

      // get total and usage values
      list($limit, $usage) = $this->extractBoltonInfo($balance);
      if ($limit === NULL || $usage === NULL)
        $this->errException('ERR_API_INVALID_ARGUMENT: Invalid Bolt On Id', 'VV0235', 'Subscriber has no data bolt on');

      $remain = $limit - $usage;
      \logInfo("data bolt on limit: $limit MB, usage: $usage MB, remaining: $remain");

      // validate and calculate amount to remove
      if ($amount > $remain)
        $this->errException('ERR_API_INVALID_ARGUMENT: Invalid amount', 'VV0115', "Amount $amount MB exceeds bolt on remaining $remain MB value");

      // perform RESETLIMIT on A-DATA-BLK
      $this->updatePlanAndFeatures($middleware, $customer, $amount);

      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }


  /**
   * checkBalance
   * throws exception
   * @param Object ACC MW instalnce
   * @param String MSISDN
   * @results Array of balance values
   */
  private function checkBalance($mw, $msisdn)
  {
    $result = $mw->mwCheckBalance(array(
      'actionUUID'  => $this->getRequestId(),
      'msisdn'      => $msisdn));

    // check for errors
    if ($result->is_timeout())
      $this->errException('ERR_API_INTERNAL: Middleware timeout', 'MW0002');
    if ($result->is_failure())
      $this->errException('ERR_API_INTERNAL: Middleware error', 'MW0001', implode(', ', $result->get_errors()));
    if (empty($result->data_array['ResultCode']) || $result->data_array['ResultCode'] != '100' || ! $result->data_array['success'])
      $this->errException('ERR_API_INTERNAL:  Middleware error', 'MW0001', $result->data_array['errors'][0]);
    if (empty($result->data_array['body']) || empty($result->data_array['body']->BalanceValueList) || empty($result->data_array['body']->BalanceValueList->BalanceValue))
      $this->errException('ERR_API_INTERNAL: Middleware error', 'MW0001', 'Invalid balance value list');

    return $result->data_array['body']->BalanceValueList->BalanceValue;
  }


  /**
   * extractBoltonInfo
   * @param Array of balance values
   * @returns Array (Integer limit or NULL on failure, Integer Usage or NULL on failure) in MB
   */
  private function extractBoltonInfo($list)
  {
    $result = array(NULL, NULL);

    foreach ($list as $balance)
    {
      if ($balance->UltraName == 'Data Add-on Total' || in_array($balance->UltraType, ['WPRBLK33S Limit','WPRBLK34S Limit','WPRBLK35S Limit','WPRBLK36S Limit']))
        $result[0] = $balance->Unit == 'KB' ? $balance->Value / 1024 : ($balance->Unit == 'GB' ? $balance->Value * 1024 : $balance->Value);
      if ($balance->UltraName == 'Data Add-on Used'  || in_array($balance->UltraType, ['WPRBLK33S Used','WPRBLK34S Used','WPRBLK35S Used','WPRBLK36S Used']))
        $result[1] = $balance->Unit == 'KB' ? $balance->Value / 1024 : ($balance->Unit == 'GB' ? $balance->Value * 1024 : $balance->Value);
    }

    return $result;
  }


  /**
   * updatePlanAndFeatures
   * throws exception on failure
   * @param Object ACC MW instalnce
   * @param Array parameters
   */
  private function updatePlanAndFeatures($middleware, $customer, $amount)
  {
    // perform update
    $result = $middleware->mwUpdatePlanAndFeatures(array(
      'msisdn'             => $customer->current_mobile_number,
      'iccid'              => $customer->CURRENT_ICCID_FULL,
      'customer_id'        => $customer->customer_id,
      'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->customer_id),
      'ultra_plan'         => get_monthly_plan_from_cos_id($customer->COS_ID),
      'preferred_language' => $customer->preferred_language,
      'option'             => "A-DATA-BLK|-$amount",
      'keepDataSocs'       => TRUE));

    if ($result->is_failure())
      $this->errException('ERR_API_INTERNAL: Middleware error', 'MW0001', implode(', ', $result->get_errors()));
    if (isset($result->data_array['success']) && ! $result->data_array['success'])
      $this->errException('ERR_API_INTERNAL: Middleware error', 'MW0001', $result->data_array['errors'][0]);
  }

}


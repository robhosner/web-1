<?php

namespace Ultra\Lib\Api\Partner\Settings;

require_once 'Ultra/Lib/Api/Partner/Settings.php';

class GetAllSettings extends \Ultra\Lib\Api\Partner\Settings
{
  /**
   * settings__GetAllSettings
   *
   * Retrieve All Deep switches.
   *
   * @return Result object
   */
  public function settings__GetAllSettings ()
  {
    list ( $username ) = $this->getInputValues();

    $error_code = '';

    $this->addToOutput('setting_fields',      '');
    $this->addToOutput('setting_values',      '');
    $this->addToOutput('setting_default',     '');
    $this->addToOutput('setting_descriptions','');
    $this->addToOutput('value_type',          '');

    try
    {
      teldata_change_db();

// TODO: $username check + auditing
// TODO: any IP check?

      $ultraSettingsData = getUltraSettings( NULL );

      if ( ! ( $ultraSettingsData && is_array($ultraSettingsData) && count($ultraSettingsData) ) )
      {
        $error_code = 'ND0001';
        throw new \Exception("ERR_API_INTERNAL: data not found.");
      }

      $setting_fields       = array();
      $setting_values       = array();
      $setting_descriptions = array();
      $setting_default      = array();
      $value_type           = array();

      foreach( $ultraSettingsData as $settingsData )
      {
        $setting_fields[]       = $settingsData->NAME;
        $setting_values[]       = $settingsData->VALUE;
        $setting_descriptions[] = $settingsData->DESCRIPTION;
        $setting_default[]      = $settingsData->DEFAULT;
        $value_type[]           = $settingsData->VALUE_TYPE;
      }

      $this->addToOutput('setting_fields',      $setting_fields);
      $this->addToOutput('setting_values',      $setting_values);
      $this->addToOutput('setting_descriptions',$setting_descriptions);
      $this->addToOutput('setting_default',     $setting_default);
      $this->addToOutput('value_type',          $value_type);

      $this->succeed ();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}


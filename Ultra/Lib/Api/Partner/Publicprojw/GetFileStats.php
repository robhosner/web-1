<?php

namespace Ultra\Lib\Api\Partner\Publicprojw;

require_once 'Ultra/Lib/Api/Partner/Publicprojw.php';
require_once 'classes/ProjectW.php';

class GetFileStats extends \Ultra\Lib\Api\Partner\Publicprojw
{
  /**
   * publicprojw__GetFileStats
   *
   * Retrieves the statuses and counts for an import file
   *
   * @return Result object
   */
  public function publicprojw__GetFileStats()
  {
    list( $filename ) = $this->getInputValues();
    $error_code = '';

    $this->addToOutput( 'statuses', '' );

    try
    {
      teldata_change_db();

      $statuses = \get_univision_import_statuses_by_filename( $filename );
      if ( count( $statuses ) == 0 )
        $this->errException( 'No records found', 'ND0001' );

      $this->addToOutput( 'statuses', $statuses );

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
      $this->addError( $e->getMessage () , $error_code );
    }

    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\Publicprojw;

require_once 'Ultra/Lib/Api/Partner/Publicprojw.php';
require_once 'classes/ProjectW.php';

require_once 'db/univision/univision_import.php';

class ExportCDR extends \Ultra\Lib\Api\Partner\Publicprojw
{
  /**
   * publicprojw__ExportCDR
   *
   * Retrieve import information for a given file
   *
   * @return Result object
   */
  public function publicprojw__ExportCDR()
  {
    list( $import_date ) = $this->getInputValues();

    $this->addToOutput('file_data', $file_data = array());

    $import_date = date('M j Y', strtotime($import_date));

    \logit('import_date ' . $import_date);

    try
    {
      teldata_change_db();

      $records = get_univision_import_by_date($import_date);

      \logit('RECORDS ' . json_encode($records));

      foreach ($records as $record)
      {
        $routingId   = ($record->STATUS == UNIVISION_IMPORT_STATUS_ACTIVATED) ? '4' : '3';
        $file_data[] = $record->IMEI . ";$routingId;";
      }

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    $this->addToOutput('file_data', $file_data);

    return $this->result;
  }
}

<?php

namespace Ultra\Lib\Api\Partner\Publicprojw;

require_once 'Ultra/Lib/Api/Partner/Publicprojw.php';
require_once 'classes/ProjectW.php';

class ExportFile extends \Ultra\Lib\Api\Partner\Publicprojw
{
  /**
   * publicprojw__ExportFile
   *
   * Retrieve import information for a given file
   *
   * @return Result object
   */
  public function publicprojw__ExportFile()
  {
    list( $filename ) = $this->getInputValues();

    $this->addToOutput( 'file_data', array() );

    try
    {
      teldata_change_db();

      // select univision_import records
      $records = \select_univision_import_file_by_filename( $filename );
      if ( empty( $records ) )
        $this->errException( 'No records found', 'ND0001' );

      $returnFields = array(
        'SUBS_NUMBER', 'SUBS_ID', 'STATUS', 'LAST_ERROR', 'LAST_ERROR_DATE', 'IMPORT_HISTORY', 'ICCID_FULL', 'TEMP_ICCID_FULL', 'MSISDN', 'IMEI'
      );
      $result = array();
      foreach ( $records as $r )
      {
        $rowResult = array();
        foreach ( $returnFields as $field )
          $rowResult[$field] = $r->$field;

        $result[] = $rowResult;
      }

      $this->addToOutput( 'file_data', $result );

      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( '' , $e->getMessage () );
    }

    return $this->result;
  }
}

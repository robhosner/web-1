<?php

namespace Ultra\Lib\Api\Partner\ApiPublic;

require_once 'Ultra/Lib/Api/Partner/ApiPublic.php';

class SMSCountryList extends \Ultra\Lib\Api\Partner\ApiPublic
{
  /**
   * ultrainfo__SMSCountryList
   *
   * Provides a list of all the international locations an Ultra user can SMS.
   *
   * @return object Result
   */
  public function ultrainfo__SMSCountryList()
  {
    $sms_list = array();

    try
    {
      teldata_change_db();

      $country_list = htt_country_sms_carriers_get_country_list();

      if ($country_list && is_array($country_list) && count($country_list))
      {
        $sms_list = $country_list;
      }
      else
      {
        $this->errException('ERR_API_INTERNAL: data not found', 'ND0001');
      }

      $this->addToOutput('sms_list', $sms_list);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
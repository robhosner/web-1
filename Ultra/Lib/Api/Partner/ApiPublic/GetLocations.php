<?php

namespace Ultra\Lib\Api\Partner\ApiPublic;

require_once 'Ultra/Lib/Api/Partner/ApiPublic.php';

class GetLocations extends \Ultra\Lib\Api\Partner\ApiPublic
{
  /**
   * Provides a list of the topup-only, and dealer locations,
   * registered with Ultra.
   *
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Commands%3A+UltraInfo#Commands:UltraInfo-ultrainfo::GetLocations
   */
  public function ultrainfo__GetLocations()
  {
    list( $latitude , $longitude , $radius , $max_entries , $location_type ) = $this->getInputValues();

    try
    {
      $error_code = '';

      $parameters = array(
        'latitude'      => $latitude,
        'longitude'     => $longitude,
        'max_entries'   => $max_entries,
        'location_type' => $location_type,
        'radius'        => $radius
      );

      $this->connectToDatabase();

      $query          = htt_ultra_locations_select_query($parameters);
      $locations      = mssql_fetch_all_objects(logged_mssql_query($query));
      $location_list  = $this->formatLocations($locations);

      $this->addToOutput('location_list', $location_list);
      $this->succeed();
    }
    catch( \Exception $e )
    {
      dlog( 'asdf' , $e->getMessage() );
      $this->addError( $e->getMessage() , $error_code );
    }

    return $this->result;
  }
}
<?php

namespace Ultra\Lib\Api\Partner\Inventory;

require_once 'Ultra/Lib/Api/Partner/Inventory.php';

class GetPINRangeStatus extends \Ultra\Lib\Api\Partner\Inventory
{
  /**
   * inventory__GetPINRangeStatus
   *
   * Get PIN cards range status.
   *  'AT_MASTER' = Hot
   *  'AT_FOUNDRY = Cold
   *
   * @param string startPinSerialNumber
   * @param string endPinSerialNumber
   * @return object Result
   */
  public function inventory__GetPINRangeStatus()
  {
    list ($startPinSerialNumber, $endPinSerialNumber) = $this->getInputValues();

    $pin_cards_data = array();

    try
    {
      teldata_change_db();

      $result = htt_inventory_pin_get_pin_range_status($startPinSerialNumber, $endPinSerialNumber);

      foreach ($result as $row)
      {
        $pin_cards_data[] = array(
          'fromPinSerialNumber' => $row->max_serial - $row->count + 1,
          'toPinSerialNumber' => $row->max_serial,
          'status' => $row->status,
          'count' => $row->count
        );
      }

      $this->addToOutput('pin_cards_data', $pin_cards_data);

      $this->succeed();
    }
    catch (\Exception $e)
    {
      dlog('', $e->getMessage());
    }

    return $this->result;
  }
}
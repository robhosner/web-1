<?php

namespace Ultra\Lib\Api\Traits;

use Ultra\Plans\Interfaces\PlansRepository;

trait PlansHandler
{
  protected abstract function errException($error, $error_code, $user_error = false);
  protected abstract function addToOutput($field, $value);

  /**
   * @param PlansRepository $plansRepo
   * @param $brand
   * @param $plan_id
   * @return null
   */
  public function getPlans(PlansRepository $plansRepo, $brand, $plan_id)
  {
    if ($brand && !empty($plan_id))
    {
      try
      {
        $plan = $plansRepo->getPlanByBrandAndPlanId($brand, $plan_id);

        if (!$plan)
        {
          $this->errException('ERR_API_INTERNAL: No plan information found.', 'ND0001', 'No plan information found.');
        }

        $this->addToOutput('plans', [$plan]);
      }
      catch (\Exception $e)
      {
        $this->errException($e->getMessage(), 'ND0001', $e->getMessage());
      }
    }
    else
    {
      $plans = $plansRepo->getAllPlansByBrand($brand);

      if (!$plans)
      {
        $this->errException('ERR_API_INTERNAL: No plan information found.', 'ND0001', 'No plan information found.');
      }

      $this->addToOutput('plans', $plans);
    }

    return null;
  }
}
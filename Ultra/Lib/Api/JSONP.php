<?php

namespace Ultra\Lib\Api;

require_once 'Ultra/Lib/Api/Base.php';

/**
 * Ultra API class for JSONP output
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class JSONP extends Base
{
  /**
   * Disable Presentation mode
   */
  public function isPresentation ()
  {
    return FALSE;
  }

  /**
   * JSONP output
   */
  public function output ( $apiExecutionResult )
  {
    return $this->jsonpCallback . '('.json_encode( $apiExecutionResult ).');';
  }
}

?>

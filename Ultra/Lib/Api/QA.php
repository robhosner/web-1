<?php

namespace Ultra\Lib\Api;

require_once 'Ultra/Lib/Api/Base.php';

/**
 * Ultra API class for HTML presentation
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class QA extends Base
{
  /**
   * getPresentation
   * 
   * Returns HTML
   * I simply copied Ted's code ( function make_html )
   * 
   * @return string
   */
  public function getPresentation ()
  {
    header("content-type: text/json");

    return json_encode($this->apiDefinition['commands']);
  }

  public function output( $string )
  {
    return $string;
  }
}


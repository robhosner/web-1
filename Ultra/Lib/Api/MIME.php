<?php

namespace Ultra\Lib\Api;

require_once 'Ultra/Lib/Api/Base.php';

/**
 * Ultra API class for generating MIME output
 *
 * take standard API return values and convert them to a specific MIME type (such as CVS file)
 *
 * @author VYT 14-04-15
 * @project MVNO-2335
 */
class MIME extends Base
{
  /**
   * this class does not display its presentation
   */
  public function isPresentation()
  {
    return FALSE;
  }


  /**
   * take API return values and generate MIME-specific output data
   */
  public function output($apiExecutionResult, $api, $mime)
  {
    // get HTTP headers
    $headers = $this->configHeaders($mime);
    if ( ! $headers)
    {
      dlog('', "ERROR: unknown headers for MIME '$mime'");
      return NULL;
    }
    
    // set HTTP headers
    foreach ($headers as $header => $value)
      header("$header: $value");

    // get API configuration
    $config = $this->apiConfig($api);
    if ( ! $config)
    {
      dlog('', "ERROR: unknown configuration for API '$api'");
      return NULL;
    }

    // generate output
    return $this->$config['method']($apiExecutionResult, $config);
  }


  /**
   * configHeaders
   *
   * return HTTP headers for a specified MIME format
   */
  private function configHeaders($mime)
  {
    $config = array
    (
      // MVNO-2335, see http://jqueryfiledownload.apphb.com/
      'jtable' => array
      (
        'Set-Cookie'            =>  'fileDownload=true; path=/',
        'Cache-Control'         =>  'max-age=60, must-revalidate',
        'Content-type'          =>  'text/csv',
        'Content-Disposition'   =>  "attachment; filename=ActivationReport.xls"
      ),

      'text' => array
      (
        'Content-type'          =>  'text/csv'
      ),

      // API-419: generic Comma Separated Values spreadsheet output
      'csv' => array
      (
        'Content-type'          =>  'text/csv',
        'Content-Disposition'   =>  "attachment; filename=report.csv"
      )
    );

    if (isset($config[$mime]))
      return $config[$mime];
    else
      return NULL;
  }

  /**
   * apiConfig
   *
   * return parameters (i.e. where to find data and how to generate MIME output) for a specific API
   *
   * @param string API name
   * @return string 'source': API output array member that contains result data
   * @return string 'method': data conversion function
   * @return array  'map': optional map of database schema names to user friendly names
   */
  private function apiConfig($api)
  {
    $config = array
    (
      'dealerportal__ReportActivationActivity' => array
      (
        'source'    =>  'customers',
        'method'    =>  'htmlTable',
        'map'       =>  array(
          'Master Agent'      =>  'master_agent_name',
          'Distributor Name'  =>  'distrbutor_name',
          'Retailer (Inv)'    =>  'retailer_inventory',
          'Retailer (Act)'    =>  'retailer_activation',
          'SIM'               =>  'iccid',
          'MTN'               =>  'msisdn',
          'Rate Plan Descr'   =>  'plan_name',
          'Funding Date'      =>  'funding_date',
          'Provision Date'    =>  'created_date',
          'Product Type'      =>  'product_type')
      ),

      'portal__GetIDDHistory' => array
      (
        'source'    =>  'idd_history',
        'method'    =>  'csvFile',
      ),

      'portal__GetTransactionHistory' => array
      (
        'source'    =>  'transaction_history',
        'method'    =>  'csvFile',
      ),

      'publicprojw__ExportErrors' => array
      (
        'source'    =>  'import_errors',
        'method'    =>  'csvFile',
      ),

      'publicprojw__ExportFile' => array
      (
        'source'    =>  'file_data',
        'method'    =>  'csvFile',
      ),

      'publicprojw__ExportCDR' => array
      (
        'source' => 'file_data',
        'method' => 'cdrFile'
      )
    );

    if (isset($config[$api]))
      return $config[$api];
    else
      return NULL;
  }


  /**
   * textfile
   */
  private function textfile($data, $config)
  {
    // check input
    if (empty($data[$config['source']]))
    {
      dlog('', "ERROR: missing API data '{$config['source']}'");
      return NULL;
    }
    elseif( ! is_array( $data[$config['source']] ) )
    {
      dlog('', "ERROR: data is not an array");
    }
    else
      return implode("\n",$data[$config['source']]);
  }


  /**
   * htmlTable
   *
   * convert a 3D array into an HTML table
   */
  private function htmlTable($data, $config)
  {
    // check input
    if (empty($data[$config['source']]))
      return logError("ERROR: missing API data '{$config['source']}'");

    $array = $data[$config['source']];
    $output = '<table cellspacing="0" rules="all" border="1" style="border-collapse:collapse;">';
    if (count($array))
    {
      // make table headers
      $output .= '<tr>';
      $map = empty($config['map']) ? $array[0] : $config['map']; // identify user fiendly map (if present)
      foreach ($map as $property => $value)
        $output .= "<td>$property</td>"; // technically we should use <th> tag but jQuery Table does not understand it
      $output .= '</tr>';

      // generate table data
      foreach ($array as $member)
      {
        $output .= '<tr>';
        foreach($map as $pretty => $ugly)
        {
          $item = empty($member->$ugly) ? NULL : $member->$ugly;
          if (strlen($item) > 8 && ctype_digit($item))
            $output .= "<td>&nbsp;$item&nbsp;</td>";  // force proper display of long numbers without scientific notation
          else
            $output .= "<td>$item</td>";
        }
        $output .= '</tr>';
      }
    }
    $output .= '</table>';

    return $output;
  }


  /**
   * csvFile
   * output given 3D associative array (as typically retrieved from a DB) into a a comma separated values spreadsheet
   * @param Array API result which contains output data element
   * @param Array output configuration
   * @return String CSV values suitable for output to browser client as MIME TEXT/CSV
   */
  private function csvFile($apiResult, $config)
  {
    // check input
    if (empty($apiResult[$config['source']]))
      return logError("ERROR: missing API data '{$config['source']}'");

    // extract data and create header row in CAPS
    $data = $apiResult[$config['source']];
    logInfo('processing ' . count($data) . ' rows');
    $keys = array_keys((array)$data[0]);
    $output = implode(',', array_map('strtoupper', $keys)) . PHP_EOL;

    // add each element's values in the same order as the header
    foreach ($data as $element)
    {
      $element = (array)$element;
      foreach ($keys as $key)
        $output .= str_replace(',', ' ', $element[$key]) . ',';
      $output .= PHP_EOL;
    }

    logInfo('result is ' . strlen($output) . ' bytes');
    return $output;
  }

  private function cdrFile($apiResult, $config)
  {
    // check input
    if (!array_key_exists($config['source'], $apiResult))
      return logError("ERROR: missing API data '{$config['source']}'");

    $output = '';

    $data = $apiResult[$config['source']];
    logInfo('processing ' . count($data) . ' rows');

    $data = $apiResult[$config['source']];
    foreach ($data as $element)
      $output .= "$element\n";
    $output . PHP_EOL;

    return $output;
  }
}

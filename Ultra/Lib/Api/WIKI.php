<?php

namespace Ultra\Lib\Api;

require_once 'Ultra/Lib/Api/Base.php';

/**
 * Ultra API class for wiki markup presentation
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class WIKI extends Base
{
  /**
   * getPresentation
   * 
   * Returns WIKI markup
   * I simply copied Ted's code ( function make_wiki )
   * 
   * @return string
   */
  public function getPresentation ()
  {
    header("content-type: text/plain");

    $code="";
    $codep = "||Parameter Name||Type||Required||Detail||Validation Rules||\n";
    $coder = "||Parameter Name||Type||Detail||\n";

    foreach ($this->apiDefinition['commands'] as $command => $def)
    {
      // Parameters
      $code .= "$this->partner - $command \n===\nInputs\n";
      $code .= $codep;
      foreach ($def['parameters'] as $pdef)
      {
        $code = $code . sprintf("|%s|%s|%s|%s|%s|\n",
                                $pdef['name'],
                                $pdef['type'],
                                $pdef['required'] ? 'required' : 'no',
                                $pdef['desc'],
                                $pdef['validation'] ? str_replace(array('{','}'),'',json_encode($pdef['validation'])) : 'none');
      }

      // Returns
      $code .= "\n\nReturns\n";
      $code .= $coder;
      foreach ($def['returns'] as $pdef)
      {
        $code = $code . sprintf("|%s|%s|%s|\n",
                                $pdef['name'],
                                $pdef['type'],
                                $pdef['desc']);
      }
      $code .= "\n\n===\n";
    }

    return $code;
  }
}

?>

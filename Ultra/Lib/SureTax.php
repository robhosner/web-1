<?php

namespace Ultra\Lib\SureTax;

// connection parameters to improve code robustness
define('SURETAX_CONNECT_TIMEOUT'  , 12); // give up if cannot connect, seconds
define('SURETAX_RESPONSE_TIMEOUT' , 18); // give up if cannot receive response after connecting, seconds

// amounts are in dollars

/**
 * get_header
 *
 * The only dynamic params are DataYear, DataMonth, TotalRevenue and ItemList
 *
 * @return array
 */
function get_header( $params )
{
  // ReturnFileCode Q retrieves quote without recording on SureTax
  if ( ! isset($params['return_file_code']) )
    $params['return_file_code'] = 'Q';

  return array(
    'ClientNumber'      => get_client_number(),
    'BusinessUnit'      => '',
    'ValidationKey'     => get_validation_key(),
    'DataYear'          => date("Y"),
    'DataMonth'         => sprintf( "%02s" , date("n") ),
    'TotalRevenue'      => $params['amount'], // total amount of purchase
    'ReturnFileCode'    => $params['return_file_code'],
    'ClientTracking'    => '',
    'IndustryExemption' => '',
    'ResponseGroup'     => '00',
    'ResponseType'      => 'D',
    'ItemList'          => array( $params['item_list'] )
  );
}

/**
 * get_item_list
 *
 * For our purposes, there will only be 1 "item" for each call.
 * The following params are dynamic:
 *   InvoiceNumber
 *   CustomerNumber
 *   Zipcode
 *   TransDate
 *   Revenue
 *
 * @return array
 */
function get_item_list( $params )
{
  if ( ! isset($params['invoice_number']) )
    $params['invoice_number'] = get_invoice_number();

  // if NULL use current default code
  if ( ! isset($params['transaction_type_code']))
    $params['transaction_type_code'] = '040105';

  return array(
    'LineNumber'     => '1',
    'InvoiceNumber'  => $params['invoice_number'],
    'CustomerNumber' => '0000000001', // temporary fix to rectify "Invalid Customer Number - Exceeds Character Limit (10)" error
    'OrigNumber'     => '',
    'TermNumber'     => '',
    'BillToNumber'   => '',
    'Zipcode'        => $params['zip_code'], // zip code associated with purchase (either passed into ultra API, or taken from customer cc_billing_zip)
    'Plus4'          => '',
    'P2PZipcode'     => '',
    'P2PPlus4'       => '',
    'TransDate'      => date("Y-m-d"), // date of transaction
    'Revenue'        => $params['amount'],
    'Units'          => 1,
    'UnitType'       => '00',
    'Seconds'        => 1,
    'TaxIncludedCode'      => '1',
    'TaxSitusRule'         => '05',
    'TransTypeCode'        => $params['transaction_type_code'],
    'SalesTypeCode'        => 'R',
    'RegulatoryCode'       => '05',
    'TaxExemptionCodeList' => array( '00' )
  );
}

function get_url()
{
  return 'https://api.taxrating.net/Services/V02/SureTax.asmx/PostRequest';
  #return 'https://testapi.taxrating.net/Services/V02/SureTax.asmx/PostRequest'; # DEV
}

function get_validation_key()
{
  return '48100d7d-912b-45f2-aa69-19883fa4c37a';
  #return '2861aaad-c887-4f35-961a-e97fb2f24701'; // test ValidationKey
}

function get_client_number()
{
  return '400142611';
  #return '000000469'; // our test ClientNumber
}

function get_invoice_number()
{
  list($usec, $sec) = explode(" ", microtime());

  return substr( time() , -6).getmypid().substr( $usec , 2 , 6);
}

/**
 * perform_call
 *
 * Invokes SureTax API
 *
 * @returns a Result object
 */
function perform_call( $params )
{
  dlog('', "(%s)", func_get_args());

  $result = new \Result;

  try
  {
    $suretax_item_list = \Ultra\Lib\SureTax\get_item_list( $params );

    $fields = \Ultra\Lib\SureTax\get_header(
      array(
        'item_list'   => $suretax_item_list,
        'amount'      => $params['amount']
      )
    );

    $request = json_encode($fields);

    dlog('',"request = %s",$request);

    // init HTTP session
    $ch = curl_init();
    if (! $ch)
      throw new \Exception('Failed session initialization');

    // configure session options
    $options = array(
      CURLOPT_POST            => TRUE,
      CURLOPT_POSTFIELDS      => 'request=' . $request,
      CURLOPT_RETURNTRANSFER  => TRUE,
      CURLOPT_SSL_VERIFYPEER  => TRUE,
      CURLOPT_URL             => get_url(),
      CURLOPT_CONNECTTIMEOUT  => SURETAX_CONNECT_TIMEOUT,
      CURLOPT_TIMEOUT         => SURETAX_RESPONSE_TIMEOUT,
      CURLOPT_SSLVERSION      => 5);
    if (! curl_setopt_array($ch, $options))
      throw new \Exception('Failed session configuration');

    $response = curl_exec( $ch );
    if (! $response)
      throw new \Exception('Invalid response from SureTax API call: ' . curl_error($ch), curl_errno($ch));
    dlog('',"response = %s",$response);

    // check HTTP result
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($code != 200)
      throw new \Exception("Failed SureTax API call with HTTP code $code");
    curl_close( $ch );

    // parse response
    $parser = xml_parser_create();
    if (! xml_parse_into_struct($parser, $response, $vals, $index))
      throw new \Exception('Failed to parse SureTax API response');

    // free the parser
    xml_parser_free($parser);

    if ( ! is_array( $vals ) || ! count( $vals ) || ! isset( $vals[0]['value'] ) )
      throw new \Exception('Invalid response from SureTax API call');

    dlog('',"vals = %s",$vals);

    $result->data_array = (array) json_decode($vals[0]['value']);

    $result->data_array['zip_code'] = $params['zip_code'];

    $result->succeed();
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());

    // handle transient network errors
    if ( in_array($e->getCode(),
      array(
        CURLE_COULDNT_RESOLVE_PROXY,
        CURLE_COULDNT_RESOLVE_HOST,
        CURLE_COULDNT_CONNECT,
        CURLE_OPERATION_TIMEDOUT)
      )
    )
      $result->timeout();

    $result->add_error( $e->getMessage() );
  }

  return $result;
}

?>

package Ultra::Lib::Aspider::Throttling::Prober;


use strict;
use warnings;


use Data::Dumper;
use LWP::UserAgent;
use Ultra::Lib::Util::Miscellaneous;
use XML::Compile::SOAP11;
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::WSDL11;


use base qw(Ultra::Lib::Util::XML);


=head4 _initialize

  Specific initialization on object instantiation.

=cut
sub _initialize
{
  my ($this, $data) = @_;

  $this->SUPER::_initialize($data);

  # this has to be executed after the superclass instantiates Ultra::Lib::Util::Config
  $this->{ WSDL } = XML::Compile::WSDL11->new( $data->{ WSDL_FILE } );
}

sub probeThrottling
{
  my ($this,$notification,$probeParams) = @_;

  my $return =
  {
    'outcome'      => '',
    'soapRequest'  => '',
    'soapResponse' => '',
  };

  my $notificationMapper =
  {
    'SendThrottlingNotification' => sub { return $this->notificationSendThrottlingNotification( @_ ); },
    'SendBalanceNotification'    => sub { return $this->notificationSendBalanceNotification( @_ );    },
  };

  if ( defined $notificationMapper->{ $notification } )
  {
    print STDERR "probeParams = ".Dumper($probeParams);

    my $soapCallResult = $notificationMapper->{ $notification }->( $notification , $probeParams );

    if ( $soapCallResult->{ error } )
    {
      $return->{ 'outcome' } = $soapCallResult->{ error };
    }
    else
    {
      # returns SOAP request
      $return->{ 'soapRequest' } = $this->prettyPrintXML( $soapCallResult->{ trace }->request()->content() );

      # returns SOAP response
      $return->{ 'soapResponse' } = $this->prettyPrintXML( $soapCallResult->{ trace }->response()->content() );

      $return->{ 'outcome' } = $soapCallResult->{ trace }->response()->status_line();
    }
  }
  else
  {
    $return->{ 'outcome' } = "Notification prober not implemented for '$notification'\n";
  }

  return $return;
}

sub notificationSendThrottlingNotification
{
  my ($this,$notification,$params) = @_;

  if ( ! defined $params )
  {
    my $timeStamp = Ultra::Lib::Util::Miscellaneous::getTimeStamp();

    $params = {}; # TODO
    # 'UserData' => { 'senderId' => 'ULTRA' , 'timeStamp' => $timeStamp } , 'Originator' => 'Prober' };
  }

  return $this->notificationCall($notification,$params);
}

sub notificationSendBalanceNotification
{
  my ($this,$notification,$params) = @_;

  if ( ! defined $params )
  {
    my $timeStamp = Ultra::Lib::Util::Miscellaneous::getTimeStamp();

    $params = {}; # TODO
  }

  return $this->notificationCall($notification,$params);
}

sub notificationCall
{
  my ($this,$notification,$params) = @_;

  print STDERR "notificationCall = $notification\n";

  my $result = { error => '', trace => undef, };

  my $ua = $this->notificationUserAgent( $notification );

  my $transport = XML::Compile::Transport::SOAPHTTP->new( timeout => 600 , address => $this->{ WSDL }->endPoint , user_agent => $ua );

  my $call = $this->{ WSDL }->compileClient( $notification , transport => $transport , action => $notification , operation => $notification );

  my ($answer, $trace);

  eval
  {
    ($answer, $trace) = $call->( $params );
  };

  if ( $@ )
  {
    $result->{ error } = $@;
  }
  elsif ( ! $trace )
  {
    $result->{ error } = "No trace available after $notification SOAP call";
  }
  else
  {
    $result->{ trace } = $trace;
  }

  return $result;
}

sub notificationUserAgent
{
  my ($this,$notification) = @_;

  my $ua = LWP::UserAgent->new;
  $ua->timeout(600);
  $ua->proxy('http', $this->{ WSDL }->endPoint );
  $ua->default_header('soapAction' => $notification);

  return $ua;
}

1;

__END__


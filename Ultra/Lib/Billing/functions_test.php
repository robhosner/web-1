<?php

require_once 'db.php';
require_once 'Ultra/Lib/Billing/functions.php';
require_once 'Ultra/Lib/Util/Redis/Billing.php';

teldata_change_db();

$customer = get_customer_from_customer_id(19250);

$real_balance = $customer->BALANCE;

// - - - - - - - -

$lock_result = array('lock_id' => 'test'.time(), 'attempt_count' => 1);
\Ultra\Lib\Billing\processCustomerCharge($customer, 5, $lock_result);

$customer->BALANCE = -5;

$lock_result = array('lock_id' => 'test'.time(), 'attempt_count' => 1);
\Ultra\Lib\Billing\processCustomerCharge($customer, 5, $lock_result);

// - - - - - - - -

$customer->BALANCE = 10;
print_r(\Ultra\Lib\Billing\handleBalanceLessThanZero($customer));

$customer->BALANCE = -5;
print_r(\Ultra\Lib\Billing\handleBalanceLessThanZero($customer));

$customer->BALANCE = -11;
print_r(\Ultra\Lib\Billing\handleBalanceLessThanZero($customer));

echo PHP_EOL;

// - - - - - - - -

print_r(\Ultra\Lib\Billing\getRejectionErrorCode(999999));

print_r(\Ultra\Lib\Billing\getRejectionErrorCode(1113));

echo PHP_EOL;

// - - - - - - - -

$customer->BALANCE = $real_balance;

\Ultra\Lib\Billing\handleRejectionErrorSMS($customer, '051', 1);
\Ultra\Lib\Billing\handleRejectionErrorSMS($customer, '051', 2);
\Ultra\Lib\Billing\handleRejectionErrorSMS($customer, '051', 3);
\Ultra\Lib\Billing\handleRejectionErrorSMS($customer, '051', 4);

\Ultra\Lib\Billing\handleRejectionErrorSMS($customer, '05111', 4);

\Ultra\Lib\Billing\handleRejectionErrorSMS($customer, '015', 1);

// - - - - - - - -

$redisBilling = new \Ultra\Lib\Util\Redis\Billing();

$uuid = '{5E1E868E-3B3A-A685-4C44-278B42614A36}';

$r = \Ultra\Lib\Billing\billingTask( $redisBilling , $uuid );

print_r($r);


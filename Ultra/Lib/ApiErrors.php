<?php
namespace Ultra\Lib;

class ApiErrors
{
  public function decodeToUserError($errorCode, $language)
  {
    return \Ultra\Lib\Api\Errors\decodeToUserError($errorCode, $language);
  }
}
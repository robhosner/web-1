<?php

namespace Ultra\Lib\Pattern\Iterator;

interface IterationInterface
{
  public function canExecute( $context );

  public function execute( $context );
}

?>

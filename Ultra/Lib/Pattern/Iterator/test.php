<?php

require_once 'Ultra/Lib/Pattern/Iterator.php';
require_once 'Ultra/Lib/Pattern/Iterator/IterationInterface.php';

class TestIteration1 implements \Ultra\Lib\Pattern\Iterator\IterationInterface
{
  public function canExecute( $context )
  {
    return TRUE;
  }

  public function execute( $context )
  {
    $context['x'] = 'a';

    return $context;
  }
}

class TestIteration2 implements \Ultra\Lib\Pattern\Iterator\IterationInterface
{
  public function canExecute( $context )
  {
    return TRUE;
  }

  public function execute( $context )
  {
    $context['y'] = 'b';

    return $context;
  }
}

$iterator = new \Ultra\Lib\Pattern\Iterator;

$iterator->load(
  array(
    new TestIteration1,
    new TestIteration2
  )
);

$context = array();

$context = $iterator->iterate( $context );

print_r( $context );

?>

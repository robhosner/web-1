<?php

namespace Ultra\Lib\Pattern;

/**
 * Simple Iterator class
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra
 */
class Iterator
{
  private $classList = array();

  /**
   * load
   *
   * @return NULL
   */
  public function load($classList)
  {
    $this->classList = $classList;
  }

  /**
   * iterate
   *
   * Iterates through the given classes, passing the $context array from the previous to the next.
   *
   * @return array
   */
  public function iterate( $context )
  {
    $n = count( $this->classList );

    for ( $i = 0 ; $i < $n ; $i++ )
    {
      if ( $this->classList[ $i ]->canExecute( $context ) )
        $context = $this->classList[ $i ]->execute( $context );
    }

    return $context;
  }
}

?>

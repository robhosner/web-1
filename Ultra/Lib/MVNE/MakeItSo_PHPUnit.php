<?php

require_once 'Ultra/Lib/MVNE/MakeItSo.php';

class MakeItSoTest extends PHPUnit_Framework_TestCase
{
  function setUp()
  {
    teldata_change_db();

    // load a brand 1 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 1 and CUSTOMER_ID is not null');
    $this->iccid_sim_1 = $res->ICCID_FULL;

    // load a brand 2 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 2 and CUSTOMER_ID is not null');
    $this->iccid_sim_2 = $res->ICCID_FULL;

    // load a brand 3 ICCID
    $res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 3 and CUSTOMER_ID is not null');
    $this->iccid_sim_3 = $res->ICCID_FULL;

    \Ultra\Lib\DB\ultra_acc_connect();
  }

  function test_validation()
  {
    //set up
    $miso = new Ultra\Lib\MVNE\MakeItSo;
    teldata_change_db();

    $miso->makeitso_options = new stdClass();
    $miso->makeitso_options->iccid              = '1';
    $miso->makeitso_options->msisdn             = '1';
    $miso->makeitso_options->wholesale_plan     = '1';
    $miso->makeitso_options->ultra_plan         = '1';
    $miso->makeitso_options->preferred_language = '1';

    // expects a MINT SIM (brand id 3)
    $errors = $miso->validateRenewEndMintPlanParams();
    $this->assertNotEmpty( $errors );

    $miso->makeitso_options->iccid = $this->iccid_sim_3;
    $errors = $miso->validateRenewEndMintPlanParams();
    $this->assertEmpty( $errors );


    // expects a MINT SIM (brand id 3)
    $errors = $miso->validateRenewMidMintPlanParams();
    $this->assertEmpty( $errors );

    $miso->makeitso_options->iccid = '1';
    $errors = $miso->validateRenewMidMintPlanParams();
    $this->assertNotEmpty( $errors );

    // expects a MINT SIM (brand id 3)
    $errors = $miso->validateRenewMintPlanParams();
    $this->assertNotEmpty( $errors );

    $miso->makeitso_options->iccid = $this->iccid_sim_3;
    $errors = $miso->validateRenewEndMintPlanParams();
    $this->assertEmpty( $errors );

    $errors = $miso->validateSuspendParams();
    $this->assertEmpty( $errors );

    // expects a non-MINT SIM (brand id 1 or 2)
    $miso->makeitso_options->iccid = $this->iccid_sim_1;
    $errors = $miso->validateRenewParams();
    $this->assertEmpty( $errors );

    $miso->makeitso_options->iccid = $this->iccid_sim_2;
    $errors = $miso->validateRenewParams();
    $this->assertEmpty( $errors );

    $miso->makeitso_options->iccid = $this->iccid_sim_3;
    $errors = $miso->validateRenewParams();
    $this->assertNotEmpty( $errors );
  }

  function test_processReactivate()
  {
    //set up
    $miso = new Ultra\Lib\MVNE\MakeItSo;

    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processReactivate');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = '1';
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';

    $miso->makeitso_options  = $makeitso_options;
    $miso->makeitso_queue_id = 1;
    $miso->attempt_count     = 1;
    $miso->customer_id       = 1;
    $miso->created_days_ago  = 1;
    $miso->attempt_history   = '';
    $miso->action_uuid       = 'TEST';

    // invoke private method processRenewEndMintPlan
    $r = $method->invoke(
      $miso
    );

    print_r($r);
  }

  function test_processUpgradePlan()
  {
    //set up
    $miso = new Ultra\Lib\MVNE\MakeItSo;

    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processUpgradePlan');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = '1';
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';
    $makeitso_options->option             = 'TEST';

    $miso->makeitso_options  = $makeitso_options;
    $miso->makeitso_queue_id = 1;
    $miso->attempt_count     = 1;
    $miso->customer_id       = 1;
    $miso->created_days_ago  = 1;
    $miso->attempt_history   = '';
    $miso->action_uuid       = 'TEST';

    // invoke private method processRenewEndMintPlan
    $r = $method->invoke(
      $miso
    );

    print_r($r);
  }

  function test_processDeactivate()
  {
    //set up
    $miso = new Ultra\Lib\MVNE\MakeItSo;

    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processDeactivate');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';

    $miso->makeitso_options  = $makeitso_options;
    $miso->makeitso_queue_id = 1;
    $miso->attempt_count     = 1;
    $miso->customer_id       = 1;
    $miso->created_days_ago  = 1;
    $miso->attempt_history   = '';
    $miso->action_uuid       = 'TEST';

    // invoke private method processRenewEndMintPlan
    $r = $method->invoke(
      $miso
    );

    print_r($r);
  }

  function test_processRenewPlan()
  {
    //set up
    $miso = new Ultra\Lib\MVNE\MakeItSo;

    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processRenewPlan');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = '1';
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';

    $miso->makeitso_options  = $makeitso_options;
    $miso->makeitso_queue_id = 1;
    $miso->attempt_count     = 1;
    $miso->customer_id       = 1;
    $miso->created_days_ago  = 1;
    $miso->attempt_history   = '';
    $miso->action_uuid       = 'TEST';

    // invoke private method processRenewEndMintPlan
    $r = $method->invoke(
      $miso
    );

    print_r($r);
  }

  function test_processRenewEndMintPlan()
  {
    //set up
    $miso = new Ultra\Lib\MVNE\MakeItSo;

    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processRenewEndMintPlan');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = '1';
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';

    $miso->makeitso_options  = $makeitso_options;
    $miso->makeitso_queue_id = 1;
    $miso->attempt_count     = 1;
    $miso->customer_id       = 1;
    $miso->created_days_ago  = 1;
    $miso->attempt_history   = '';
    $miso->action_uuid       = 'TEST';

    // invoke private method processRenewEndMintPlan
    $r = $method->invoke(
      $miso
    );

    print_r($r);
    $log = $r->get_log();
    // invalid SIM
    $this->assertEquals('Error: expected SIM brand is MINT',$log[0]);
  }

  function test_processRenewMidMintPlan()
  {
    //set up
    $miso = new Ultra\Lib\MVNE\MakeItSo;

    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processRenewMidMintPlan');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = MINT_WHOLESALE_PLAN;
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';

    $miso->makeitso_options  = $makeitso_options;
    $miso->makeitso_queue_id = 1;
    $miso->attempt_count     = 1;
    $miso->customer_id       = 1;
    $miso->created_days_ago  = 1;
    $miso->attempt_history   = '';
    $miso->action_uuid       = 'TEST';

    // invoke private method processRenewMidMintPlan
    $r = $method->invoke( $miso );

    print_r($r);
    $log = $r->get_log();
    // invalid SIM
    $this->assertEquals('Error: expected SIM brand is MINT',$log[0]);

    $miso->makeitso_options->iccid = $this->iccid_sim_1;

    // invoke private method processRenewMidMintPlan
    $r = $method->invoke( $miso );

    print_r($r);
    $log = $r->get_log();

    // invalid SIM Brand
    $this->assertEquals('Error: expected SIM brand is MINT',$log[0]);

    $miso->makeitso_options->iccid  = '8901260963152850710';
    $miso->makeitso_options->msisdn = '7204019399';

    // invoke private method processRenewMidMintPlan
    $r = $method->invoke( $miso );
    print_r($r);
  }

  function test_processRenewMintPlan()
  {
    //set up
    $miso = new Ultra\Lib\MVNE\MakeItSo;

    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processRenewMintPlan');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = '1';
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';

    $miso->makeitso_options  = $makeitso_options;
    $miso->makeitso_queue_id = 1;
    $miso->attempt_count     = 1;
    $miso->action_uuid       = 'TEST';

    // invoke private method processRenewMintPlan
    $r = $method->invoke( $miso );

    print_r($r);
    $log = $r->get_log();
    // invalid SIM
    $this->assertEquals('Error: expected SIM brand is MINT',$log[0]);

    $miso->makeitso_options->iccid = $this->iccid_sim_1;

    // invoke private method processRenewMintPlan
    $r = $method->invoke( $miso );

    print_r($r);
    $log = $r->get_log();
    // invalid SIM Brand
    $this->assertEquals('Error: expected SIM brand is MINT',$log[0]);
  }

  function test_processSuspend()
  {
    //set up
    $miso = new Ultra\Lib\MVNE\MakeItSo;

    $method = new ReflectionMethod('\Ultra\Lib\MVNE\MakeItSo', 'processSuspend');
    $method->setAccessible(true);

    $makeitso_options = new stdClass();
    $makeitso_options->iccid              = '1';
    $makeitso_options->msisdn             = '1';
    $makeitso_options->wholesale_plan     = '1';
    $makeitso_options->ultra_plan         = '1';
    $makeitso_options->preferred_language = '1';

    $miso->makeitso_options  = $makeitso_options;
    $miso->makeitso_queue_id = 1;
    $miso->attempt_count     = 1;
    $miso->customer_id       = 1;
    $miso->created_days_ago  = 1;
    $miso->attempt_history   = '';
    $miso->action_uuid       = 'TEST';

    // invoke private method processRenewMintPlan
    $r = $method->invoke( $miso );

    print_r($r);
  }

/*
  function test_checkBalance()
  {
    //set up
    $control = new \Ultra\Lib\MiddleWare\ACC\Control;
    $miso = new Ultra\Lib\MVNE\MakeItSo;
    $method = new ReflectionMethod('Ultra\Lib\MVNE\MakeItSo', 'checkBalance');
    $method->setAccessible(true);

    // check missing params
    $result = $method->invokeArgs($miso, array($control));
    $this->assertNull($result);

    // query by MSISDN without caching
    $miso->customer_id = 19407;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = new StdClass();
    $miso->makeitso_options->msisdn = '9495015442';
    $result = $method->invokeArgs($miso, array($control));
    print_r($result);
    $this->assertNotEmpty($result);

    // query with caching
    $miso->customer_id = 19407;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = new StdClass();
    $miso->makeitso_options->msisdn = '9495015442';
    $result = $method->invokeArgs($miso, array($control, TRUE));
    print_r($result);
    $this->assertNotEmpty($result);
  }


  function test_querySubscriber()
  {
    //set up
    $control = new \Ultra\Lib\MiddleWare\ACC\Control;
    $miso = new Ultra\Lib\MVNE\MakeItSo;
    $method = new ReflectionMethod('Ultra\Lib\MVNE\MakeItSo', 'querySubscriber');
    $method->setAccessible(true);

    // query by MSISDN without caching
    $miso->customer_id = 19407;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = new StdClass();
    $miso->makeitso_options->msisdn = '9495015442';
    list($result, $status) = $method->invokeArgs($miso, array($control));
    print_r($status);
    $this->assertNotEmpty($status->ResultCode);
    $this->assertNotEmpty($status->AddOnFeatureInfoList);
    $this->assertNotEmpty($status->AddOnFeatureInfoList->AddOnFeatureInfo);
    $this->assertNotEmpty(count($status->AddOnFeatureInfoList->AddOnFeatureInfo));
    $this->assertNotEmpty(count($status->AddOnFeatureInfoList->AddOnFeatureInfo[0]->FeatureID));

    // query with caching
    $miso->customer_id = 19407;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = new StdClass();
    $miso->makeitso_options->msisdn = '9495015442';
    list($result, $status) = $method->invokeArgs($miso, array($control, TRUE));
    print_r($status);
    $this->assertNotEmpty($status->ResultCode);
    $this->assertNotEmpty($status->AddOnFeatureInfoList);
    $this->assertNotEmpty($status->AddOnFeatureInfoList->AddOnFeatureInfo);
    $this->assertNotEmpty(count($status->AddOnFeatureInfoList->AddOnFeatureInfo));
    $this->assertNotEmpty(count($status->AddOnFeatureInfoList->AddOnFeatureInfo[0]->FeatureID));
  }


  function test_ensureSubscriberActive()
  {
    // setup
    $control = new \Ultra\Lib\MiddleWare\ACC\Control;
    $miso = new Ultra\Lib\MVNE\MakeItSo;
    $ensure = new ReflectionMethod('Ultra\Lib\MVNE\MakeItSo', 'ensureSubscriberActive');
    $ensure->setAccessible(true);
    $query = new ReflectionMethod('Ultra\Lib\MVNE\MakeItSo', 'querySubscriber');
    $query->setAccessible(true);

    // already Active subscriber
    $miso->customer_id = 19463;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = new StdClass();
    $miso->makeitso_options->msisdn = '6094960995';
    list($result, $status) = $query->invokeArgs($miso, array($control, TRUE));
    $this->assertEquals($status->SubscriberStatus, 'Active');
    list($proceed, $result) = $ensure->invokeArgs($miso, array($control, $status));
    $this->assertTrue($proceed);

    // Provisioned subscriber
    $miso->customer_id = 19371;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = new StdClass();
    $miso->makeitso_options->msisdn = '6148221377';
    $miso->makeitso_options->iccid = '8901260963131987401';
    $miso->makeitso_options->preferred_language ='EN';
    $miso->makeitso_options->ultra_plan = 'L19';
    $miso->makeitso_options->wholesale_plan = 'W-SECONDARY';
    list($result, $status) = $query->invokeArgs($miso, array($control, TRUE));
    print_r($status);
    $this->assertEquals($status->SubscriberStatus, 'Suspended');
    list($proceed, $result) = $ensure->invokeArgs($miso, array($control, $status));
    $this->assertTrue($proceed);
  }
*/

  function test_logBalance()
  {
    $miso = new Ultra\Lib\MVNE\MakeItSo;
    $control = new \Ultra\Lib\MiddleWare\ACC\Control;
    $log = new ReflectionMethod('Ultra\Lib\MVNE\MakeItSo', 'logBalance');
    $log->setAccessible(true);

    // missing plan tracker ID
    $miso->customer_id = 19338;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = (object)array('msisdn' => '9493864775');
    $log->invokeArgs($miso, array($control));
/*
    // plan tracker already logged
    $miso->customer_id = 19430;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = (object)array('msisdn' => '9492410322', 'plan_tracker_id' => 2106);
    $log->invokeArgs($miso, array($control));

    // suspended subscriber
    $miso->customer_id = 19436;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = (object)array('msisdn' => '7143603411', 'plan_tracker_id' => 111);
    $log->invokeArgs($miso, array($control));
*/
    // active subscriber
    $miso->customer_id = 19410;
    $miso->action_uuid = 'PHPUnit-' . time();
    $miso->makeitso_options = (object)array('msisdn' => '7144173586', 'plan_tracker_id' => 2076);
    $log->invokeArgs($miso, array($control));

  }


/*
  function test_withdrawByCustomerId()
  {
    $miso = new Ultra\Lib\MVNE\MakeItSo;
    $log = new ReflectionMethod('Ultra\Lib\MVNE\MakeItSo', 'withdrawByCustomerId');
    $log->setAccessible(true);

    // void plan
    $log->invokeArgs($miso, array(19628, 'UpgradePlan', 'CHANGE|UV60'));

    // valid plan
    $log->invokeArgs($miso, array(19628, 'UpgradePlan', 'CHANGE|UV45'));
  }


  function test_processUpgradePlan()
  {
    $miso = new Ultra\Lib\MVNE\MakeItSo(array('instantly' => TRUE));
    $log = new ReflectionMethod('Ultra\Lib\MVNE\MakeItSo', 'processUpgradePlan');
    $log->setAccessible(true);

    \Ultra\Lib\DB\ultra_acc_connect();
    $miso->getNext(3500);
    $this->assertNotEmpty($miso->customer_id);
    $log->invoke($miso);
  }
*/

}

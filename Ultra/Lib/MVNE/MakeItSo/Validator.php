<?php

namespace Ultra\Lib\MVNE\MakeItSo;

/**
 * validateMakeitsoOptions
 *
 * Generic function to validate $makeitso_options content
 *
 * @input array - list of required values from $makeitso_options object
 * @input object $makeitso_options
 * @return array - list of errors
 */
function validateMakeitsoOptions( array $expectedOptions , $makeitso_options )
{
  $errors = array();

  if ( ! isset($makeitso_options) || ! is_object($makeitso_options) )
    $errors[] = "mandatory options missing";
  else
    foreach( $expectedOptions as $expectedOption )
      if ( !property_exists($makeitso_options, $expectedOption ) || !$makeitso_options->$expectedOption )
        $errors[] = $expectedOption . ' is missing';

  return $errors;
}

/**
 * validateUdateThrottleSpeedParams
 *
 * Function to validate $makeitso_options content for MISO task UpdateThrottleSpeed
 *
 * @return array - list of errors
 */
function validateUpdateThrottleSpeedParams( $makeitso_options )
{
  teldata_change_db();

  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid',
      'wholesale_plan',
      'ultra_plan',
      'preferred_language'
    ],
    $makeitso_options
  );

  // fail if missing throttle_speed parameter
  if ( empty($makeitso_options->throttle_speed) )
    $errors[] = 'Error: missing option throttle_speed';

  return $errors;
}

/**
 * validateRenewMintPlanParams
 *
 * Function to validate $makeitso_options content for MISO task RenewMintPlan
 *
 * @return array - list of errors
 */
function validateRenewMintPlanParams( $makeitso_options )
{
  teldata_change_db();

  // fail if brand is NOT MINT
  if ( ! \Ultra\Lib\Util\validateICCIDBrand( $makeitso_options->iccid , 'MINT' ) )
    $brandError = 'Error: expected SIM brand is MINT';

  \Ultra\Lib\DB\ultra_acc_connect();

  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid',
      'wholesale_plan',
      'ultra_plan',
      'preferred_language'
    ],
    $makeitso_options
  );

  if ( ! empty( $brandError ) )
    $errors[] = $brandError;

  return $errors;
}

/**
 * validateRenewEndMintPlanParams
 *
 * Function to validate $makeitso_options content for MISO task RenewEndMintPlan
 *
 * @return array - list of errors
 */
function validateRenewEndMintPlanParams( $makeitso_options )
{
  teldata_change_db();

  // fail if brand is NOT MINT
  if ( ! \Ultra\Lib\Util\validateICCIDBrand( $makeitso_options->iccid , 'MINT' ) )
    $brandError = 'Error: expected SIM brand is MINT';

  \Ultra\Lib\DB\ultra_acc_connect();

  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid',
      'wholesale_plan',
      'ultra_plan',
      'preferred_language'
    ],
    $makeitso_options
  );

  if ( ! empty( $brandError ) )
    $errors[] = $brandError;

  return $errors;
}

/**
 * validateRenewMidMintPlanParams
 *
 * Function to validate $makeitso_options content for MISO task RenewMidMintPlan
 *
 * @return array - list of errors
 */
function validateRenewMidMintPlanParams( $makeitso_options )
{
  teldata_change_db();

  // fail if brand is NOT MINT
  if ( ! \Ultra\Lib\Util\validateICCIDBrand( $makeitso_options->iccid , 'MINT' ) )
    $brandError = 'Error: expected SIM brand is MINT';

  \Ultra\Lib\DB\ultra_acc_connect();

  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid',
      'wholesale_plan',
      'ultra_plan',
      'preferred_language'
    ],
    $makeitso_options
  );

  if ( ! empty( $brandError ) )
    $errors[] = $brandError;

  return $errors;
}

/**
 * validateSuspendParams
 *
 * Function to validate $makeitso_options content for ReactivateSubscriber and SuspendSubscriber MISO task
 *
 * @return array - list of errors
 */
function validateSuspendParams( $makeitso_options )
{
  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid',
      'wholesale_plan',
      'ultra_plan',
      'preferred_language'
    ],
    $makeitso_options
  );

  return $errors;
}

/**
 * validateRenewParams
 *
 * Function to validate $makeitso_options content for Renew MISO task (non-Mint plans)
 *
 * @return array - list of errors
 */
function validateRenewParams( $makeitso_options )
{
  teldata_change_db();

  // fail if brand is MINT
  if ( \Ultra\Lib\Util\validateICCIDBrand( $makeitso_options->iccid , 'MINT' ) )
    $brandError = 'Error: expected SIM brand is NOT MINT';

  \Ultra\Lib\DB\ultra_acc_connect();

  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid',
      'wholesale_plan',
      'ultra_plan',
      'preferred_language'
    ],
    $makeitso_options
  );

  if ( ! empty( $brandError ) )
    $errors[] = $brandError;

  return $errors;
}

/**
 * validateDeactivateParams
 *
 * Function to validate $makeitso_options content for Deactivate MISO task
 *
 * @return array - list of errors
 */
function validateDeactivateParams( $makeitso_options )
{
  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid'
    ],
    $makeitso_options
  );

  return $errors;
}

/**
 * validateUpgradePlanParams
 *
 * Function to validate $makeitso_options content for UpgradePlan MISO task
 *
 * @return array - list of errors
 */
function validateUpgradePlanParams( $makeitso_options )
{
  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid',
      'wholesale_plan',
      'ultra_plan',
      'preferred_language',
      'option'
    ],
    $makeitso_options
  );

  return $errors;
}

/**
 * validateReactivateParams
 *
 * Function to validate $makeitso_options content for Reactivate MISO task
 *
 * @return array - list of errors
 */
function validateReactivateParams( $makeitso_options )
{
  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid',
      'wholesale_plan',
      'ultra_plan',
      'preferred_language'
    ],
    $makeitso_options
  );

  return $errors;
}

/**
 * validateAddMintDataParams
 *
 * Function to validate $makeitso_options content for AddMintData MISO task
 *
 * @return array - list of errors
 */
function validateAddMintDataParams( $makeitso_options )
{
  $errors = \Ultra\Lib\MVNE\MakeItSo\validateMakeitsoOptions(
    [
      'msisdn',
      'iccid',
      'wholesale_plan',
      'ultra_plan',
      'preferred_language',
      'data_add_on'
    ],
    $makeitso_options
  );

  return $errors;
}


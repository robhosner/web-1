<?php

namespace Ultra\Lib\MVNE;

require_once 'classes/CommandInvocation.php';
require_once 'classes/PortInQueue.php';
require_once 'db/ultra_acc/makeitso.php';
require_once 'db/ultra_acc/mrc_usage.php';
require_once 'db/ultra_acc/soap_paid_event.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Control.php';
require_once 'Ultra/Lib/MQ/ControlChannel.php';
require_once 'Ultra/Lib/Util/Redis.php';
require_once 'Ultra/Lib/Util/Redis/MakeItSo.php';
require_once 'Ultra/Lib/DB/Customer.php';
require_once 'Ultra/Lib/MVNE/MakeItSo/functions.php';
require_once 'Ultra/Lib/MVNE/MakeItSo/Validator.php';
require_once 'Ultra/Lib/Util/Validator.php';
require_once 'lib/util-arrays.php';

require_once 'classes/LineCredits.php';
require_once 'Ultra/Lib/Services/FamilyAPI.php';

/**
 * Base class for MakeItSo logic (aka. Ensure)
 * The ``MakeItSo logic`` attempts to verify the status of a customer at MVNE level.
 *
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/MakeItSo+Process+and+State+Transitions
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project MVNE2
 */
class MakeItSo
{
  private $instantly; // TRUE if we should proceed immediately
  private $redis;
  private $redis_makeitso;

  const CHECK_BALANCE_KEY    = 'acc/checkbalance/';
  const CHECK_BALANCE_TTL    = 180;
  const QUERY_SUBSCRIBER_KEY = 'acc/querysubscriber/';
  const QUERY_SUBSCRIBER_TTL = 180;

  /**
   * Class constructor
   */
  public function __construct ( $params=array() )
  {
    // initialize private variables

    $this->redis = ( isset($params['redis']) )
                   ?
                   $params['redis']
                   :
                   new \Ultra\Lib\Util\Redis
                   ;

    $this->instantly = ( isset($params['instantly']) )
                       ?
                       $params['instantly']
                       :
                       FALSE
                       ;

    $this->redis_makeitso = new \Ultra\Lib\Util\Redis\MakeItSo( $this->redis );
  }

  /**
   * dbConnect
   *
   * Connect to ULTRA_ACC DB
   */
  public function dbConnect()
  {
    return \Ultra\Lib\DB\ultra_acc_connect();
  }

  /**
   * getCommandInvocationObject
   *
   * Instantiate a \CommandInvocation object
   *
   * @return CommandInvocation object
   */
  private function getCommandInvocationObject()
  {
    $commandInvocation = new \CommandInvocation(
      array(
        'customer_id' => $this->customer_id
      )
    );

    dlog('',"commandInvocation = %s",$commandInvocation);

    return $commandInvocation;
  }

  /**
   * withdrawByCustomerId
   *
   * Withdraw previous attempts for the same customer
   * The following tasks trigger a withdrawal of the customer's current MISO tasks:
   *  - Suspend
   *  - Deactivate
   *  - RenewPlan
   *  - Restore
   *  - UpgradePlan ( subtype 'V-ENGLISH' or 'V-SPANISH' ) withdraws same subtypes
   *  - UpgradePlan ( plan change ) withdraws same subtypes
   *
   * @return boolean
   */
  public function withdrawByCustomerId ( $customer_id , $type , $subtype )
  {
    $parts = explode('|', $subtype);

    if ( ( $type == 'Suspend' )
      || ( $type == 'Deactivate' )
      || ( $type == 'RenewPlan' )
      || ( $type == 'Restore' )
      || ( $type == 'Reactivate' )
    )
      return \withdraw_makeitso( $customer_id, NULL, ['IntraPort', 'FlexSetup'] );
    elseif ( $type == 'UpgradePlan' )
    {
      if ( ( $subtype == 'V-ENGLISH' ) || ( $subtype == 'V-SPANISH' ) )
        return \withdraw_makeitso( $customer_id , array('V-ENGLISH','V-SPANISH') );
      if ($parts[0] == 'CHANGE' && in_array($parts[1], get_ultra_plans_short()))
        return \withdraw_makeitso( $customer_id , 'CHANGE%' );
    }
    elseif ( $type == 'FlexSetup' )
    {
      // FlexSetup withdraw other FlexSetup
      return \withdraw_makeitso( $customer_id , 'FlexSetup' );
    }
    else
      dlog('',"withdrawByCustomerId not defined for type $type");

    // TRF-28: return SUCCESS for non-matching values
    return TRUE;
  }

  /**
   * enqueue
   *
   * Enqueue an item in the MakeItSo queue
   *
   * @return Result object
   */
  public function enqueue ( $params )
  {
    $this->dbConnect();

    $subtype = isset($params['subtype']) ? $params['subtype'] : NULL ;

    // withdraw previous attempts for the same customer
    $success = $this->withdrawByCustomerId( $params['customer_id'] , $params['type'] , $subtype );

    if ( ! $success )
      return \make_error_Result( "ERR_API_INTERNAL: an error occurred while withdrawing previous MakeItSo queue elements" );

    $success = \add_to_makeitso(
      array(
        'action_uuid'      => $params['action_uuid'],
        'customer_id'      => $params['customer_id'],
        'makeitso_options' => $params['options'], // context
        'makeitso_type'    => $params['type'],
        'makeitso_subtype' => $subtype
      )
    );

    if ( $success )
    {
      // load all PENDING MakeItSo tasks for this customer
      $pending_makeitso_tasks = get_pending_makeitso_by_customer_id( $params['customer_id'] );

      if ( !$pending_makeitso_tasks || !is_array($pending_makeitso_tasks) || !count($pending_makeitso_tasks) )
        return \make_error_Result( "ERR_API_INTERNAL: could not retrieve latest makeitso_queue_id for customer_id ".$params['customer_id'] );

      dlog('',"pending_makeitso_tasks = %s",$pending_makeitso_tasks);

      $makeitso_queue_id = $pending_makeitso_tasks[0]->MAKEITSO_QUEUE_ID;

      $delay = ( count($pending_makeitso_tasks) > 1 )
               ?
               // we have to delay this MakeItSo task if there is a ``younger`` one pending
               ( 10 * 60 * ( count($pending_makeitso_tasks) - 1 ) )
               :
               0
               ;

      dlog('',"delay = $delay");

      if ( $this->instantly )
        // we want to proceed immediately: do not add to Redis queue
        return $this->processNext( $makeitso_queue_id );
      else
        // add to Redis queue
        $this->redis_makeitso->add( time() + $delay , $makeitso_queue_id );

      return \make_ok_Result(array('makeitso_queue_id' => $makeitso_queue_id));
    }
    else
      \make_error_Result( "ERR_API_INTERNAL: an error occurred while inserting into the MakeItSo queue" );
  }

  /**
   * enqueueResetSharedDataLimit
   *
   * Enqueue a task to perform UpdatePlanAndFeatures call(s) to update ban limit
   *
   * @return Result object
   */
  public function enqueueResetSharedDataLimit($action_uuid, $customer_id, $msisdn, $iccid, $current_plan, $wholesale_plan, $preferred_language, $percent)
  {
    // mandatory parameters
    $options = [
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language,
      'percent'            => $percent
    ];

    return $this->enqueue([
      'type'        =>'ResetSharedDataLimit',
      'action_uuid' => $action_uuid,
      'customer_id' => $customer_id,
      'options'     => \json_encode($options)
    ]);
  }

  /**
   * enqueueUpdateThrottleSpeed
   *
   * Enqueue a task to perform UpdatePlanAndFeatures call(s) to update throttle speed
   *
   * @return Result object
   */
  public function enqueueUpdateThrottleSpeed($action_uuid , $customer_id , $msisdn , $iccid , $current_plan , $wholesale_plan , $preferred_language , $throttle_speed, $lte_select_method, $balance_values, $plan_tracker_id = NULL, $migratingFromMrc = false)
  {
    // mandatory parameters
    $options = [
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language,
      'throttle_speed'     => $throttle_speed,
      'lte_select_method'  => $lte_select_method,
      'balance_values'     => $balance_values,
      'migratingFromMrc'   => $migratingFromMrc
    ];

    if ( ! empty($plan_tracker_id) )
      $options['plan_tracker_id'] = $plan_tracker_id;

    return $this->enqueue(
      array(
        'type'             => 'UpdateThrottleSpeed',
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode($options)
      )
    );
  }

  /**
   * enqueueAddMintData
   *
   * Enqueue a task to perform UpdatePlanAndFeatures call(s) to add mint Add On SOCs
   *
   * @return Result object
   */
  public function enqueueAddMintData( $action_uuid , $customer_id , $msisdn , $iccid , $current_plan , $wholesale_plan , $preferred_language , $data_add_on )
  {
    // mandatory parameters
    $options = [
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language,
      'data_add_on'        => $data_add_on
    ];

    return $this->enqueue([
      'type'             => 'AddMintData',
      'action_uuid'      => $action_uuid,
      'customer_id'      => $customer_id,
      'options'          => \json_encode($options)
    ]);
  }

  /**
   * enqueueRenewEndMintPlan
   *
   * Enqueue a RenewPlan for a Mint SIM at the end of plan
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/MW+%3A+MakeitsoRenewPlan
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/ACC+Flows+to+Ensure+Correct+Billing
   *
   * @return Result object
   */
  public function enqueueRenewEndMintPlan( $action_uuid , $customer_id , $msisdn , $iccid , $current_plan , $wholesale_plan , $preferred_language, $plan_tracker_id=NULL )
  {
    // mandatory parameters
    $options = [
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language
    ];

    if ( ! empty($plan_tracker_id) )
        $options['plan_tracker_id'] = $plan_tracker_id;

    return $this->enqueue(
      array(
        'type'             => 'RenewEndMintPlan',
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode($options)
      )
    );
  }

  /**
   * enqueueRenewMidMintPlan
   *
   * Enqueue a RenewPlan for a Mint SIM in the middle of plan
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/MW+%3A+MakeitsoRenewPlan
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/ACC+Flows+to+Ensure+Correct+Billing
   *
   * @return Result object
   */
  public function enqueueRenewMidMintPlan( $action_uuid , $customer_id , $msisdn , $iccid , $current_plan , $wholesale_plan , $preferred_language, $plan_tracker_id=NULL )
  {
    // mandatory parameters
    $options = [
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language
    ];

    if ( ! empty($plan_tracker_id) )
        $options['plan_tracker_id'] = $plan_tracker_id;

    return $this->enqueue(
      array(
        'type'             => 'RenewMidMintPlan',
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode($options)
      )
    );
  }

  /**
   * enqueueRenewMintPlan
   *
   * Enqueue a Renew task for a Mint SIM when transitioning from Suspended to Active
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/MW+%3A+MakeitsoRenewPlan
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/ACC+Flows+to+Ensure+Correct+Billing
   *
   * @return Result object
   */
  public function enqueueRenewMintPlan(  $action_uuid , $customer_id , $msisdn , $iccid , $current_plan , $wholesale_plan , $preferred_language, $plan_tracker_id=NULL )
  {
    // mandatory parameters
    $options = [
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language
    ];

    if ( ! empty($plan_tracker_id) )
        $options['plan_tracker_id'] = $plan_tracker_id;

    return $this->enqueue([
      'type'             => 'RenewMintPlan',
      'action_uuid'      => $action_uuid,
      'customer_id'      => $customer_id,
      'options'          => \json_encode($options)
    ]);
  }

  /**
   * enqueueSuspendMintPlan
   *
   * Enqueue a SuspendMintPlan Makeitso item
   *
   * @return Result object
   */
  public function enqueueSuspendMintPlan( $action_uuid , $customer_id , $msisdn , $iccid , $ultra_plan , $wholesale_plan , $preferred_language, $plan_tracker_id )
  {
    return $this->enqueue(
      array(
        'type'             => 'SuspendMintPlan',
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode(
          array(
            'msisdn'             => $msisdn,
            'iccid'              => $iccid,
            'ultra_plan'         => $ultra_plan,
            'wholesale_plan'     => $wholesale_plan,
            'preferred_language' => $preferred_language,
            'plan_tracker_id'    => $plan_tracker_id
          )
        )
      )
    );
  }

  /**
   * enqueueRenewPlan
   *
   * Enqueue a RenewPlan Makeitso item
   *
   * @return Result object
   */
  public function enqueueRenewPlan( $action_uuid , $customer_id , $msisdn , $iccid , $current_plan , $wholesale_plan , $preferred_language, $plan_tracker_id, $subplan, $roam_credit_promo = 0)
  {
    // mandatory parameters
    $options = array(
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language,
      \Ultra\Lib\RoamCreditPromo::getParam() => $roam_credit_promo
    );

    // optional parameters
    foreach (array('plan_tracker_id', 'subplan') as $option)
      if ( ! empty($$option))
        $options[$option] = $$option;

    return $this->enqueue(
      array(
        'type'             => 'RenewPlan',
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode($options)
      )
    );
  }

  /**
   * enqueueUpgradePlan
   *
   * Enqueue an UpgradePlan Makeitso item
   *
   * @return Result object
   */
  public function enqueueUpgradePlan( $action_uuid , $customer_id , $msisdn , $iccid , $current_plan , $wholesale_plan , $preferred_language , $option, array $extra_options = [] )
  {
    $options = array(
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language,
      'option'             => $option
    );
    if (!empty($extra_options)) {
      // options takes precedence over extra_options
      $options = $options + $extra_options;
    }

    return $this->enqueue(
      array(
        'type'             => 'UpgradePlan',
        'subtype'          => $option,
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode($options)
      )
    );
  }

  /**
   * enqueueRestore
   *
   * Enqueue a Restore Makeitso item
   *
   * @return Result object
   */
  public function enqueueRestore( $action_uuid , $customer_id , $msisdn , $iccid )
  {
    return $this->enqueue(
      array(
        'type'             => 'Restore',
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode(
          array(
            'msisdn'             => $msisdn,
            'iccid'              => $iccid
          )
        )
      )
    );
  }

  /**
   * enqueueReactivate
   *
   * Enqueue a Reactivate Makeitso item
   *
   * @return Result object
   */
  public function enqueueReactivate( $action_uuid , $customer_id , $msisdn , $iccid, $ultra_plan, $wholesale_plan, $preferred_language )
  {
    return $this->enqueue(
      array(
        'type'             => 'Reactivate',
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode(
          array(
            'msisdn'             => $msisdn,
            'iccid'              => $iccid,
            'ultra_plan'         => $ultra_plan,
            'wholesale_plan'     => $wholesale_plan,
            'preferred_language' => $preferred_language
          )
        )
      )
    );
  }

  /**
   * enqueueDeactivate
   *
   * Enqueue a Deactivate Makeitso item
   *
   * @return Result object
   */
  public function enqueueDeactivate( $action_uuid , $customer_id , $msisdn , $iccid )
  {
    return $this->enqueue(
      array(
        'type'             => 'Deactivate',
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode(
          array(
            'msisdn'             => $msisdn,
            'iccid'              => $iccid
          )
        )
      )
    );
  }

  /**
   * enqueueSuspend
   *
   * Enqueue a Suspend Makeitso item
   *
   * @return Result object
   */
  public function enqueueSuspend( $action_uuid , $customer_id , $msisdn , $iccid , $ultra_plan , $wholesale_plan , $preferred_language, $plan_tracker_id )
  {
    return $this->enqueue(
      array(
        'type'             => 'Suspend',
        'action_uuid'      => $action_uuid,
        'customer_id'      => $customer_id,
        'options'          => \json_encode(
          array(
            'msisdn'             => $msisdn,
            'iccid'              => $iccid,
            'ultra_plan'         => $ultra_plan,
            'wholesale_plan'     => $wholesale_plan,
            'preferred_language' => $preferred_language,
            'plan_tracker_id'    => $plan_tracker_id
          )
        )
      )
    );
  }

  /**
   * enqueueFlexSetup
   *
   * Enqueue a FlexSetup makeitso
   *
   * @return Result object
   */
  public function enqueueFlexSetup( $action_uuid , $customer_id )
  {
    return $this->enqueue([
      'type'        => 'FlexSetup',
      'subtype'     => 'FlexSetup',
      'action_uuid' => $action_uuid,
      'customer_id' => $customer_id,
      'options'     => \json_encode([])
    ]);
  }

  /**
   * enqueueAddToBan
   *
   * Enqueue a task to perform UpdatePlanAndFeatures call(s) to add a customer to a ban.
   *
   * @return Result object
   */
  public function enqueueAddToBan($action_uuid, $customer_id, $msisdn, $iccid, $current_plan, $wholesale_plan, $dataAmount, $preferred_language)
  {
    // mandatory parameters
    $options = [
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language,
      'option'             => $dataAmount
    ];

    return $this->enqueue([
      'type'        =>'AddToBan',
      'action_uuid' => $action_uuid,
      'customer_id' => $customer_id,
      'options'     => \json_encode($options)
    ]);
  }

  public function processAddToBan()
  {
    // check command invocations
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    return $this->invokeUpdatePlanAndFeatures( new \Ultra\Lib\MiddleWare\ACC\Control() , 'AddToBan|' . $this->makeitso_options->option , TRUE , TRUE );
  }

  /**
   * enqueueRemoveFromBan
   *
   * Enqueue a task to perform UpdatePlanAndFeatures call(s) to remove a customer from a ban.
   *
   * @return Result object
   */
  public function enqueueRemoveFromBan($action_uuid, $customer_id, $msisdn, $iccid, $current_plan, $wholesale_plan, $preferred_language, $ban)
  {
    // mandatory parameters
    $options = [
      'msisdn'             => $msisdn,
      'iccid'              => $iccid,
      'wholesale_plan'     => $wholesale_plan,
      'ultra_plan'         => $current_plan,
      'preferred_language' => $preferred_language,
      'option'             => $ban
    ];

    return $this->enqueue([
      'type'        =>'RemoveFromBan',
      'action_uuid' => $action_uuid,
      'customer_id' => $customer_id,
      'options'     => \json_encode($options)
    ]);
  }

  /**
   * processRemoveFromBan
   *
   * Process RemoveFromBan
   *
   * @return Result object
   */
  private function processRemoveFromBan()
  {
    // check command invocations
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    return $this->invokeUpdatePlanAndFeatures( new \Ultra\Lib\MiddleWare\ACC\Control() , 'RemoveFromBan|' . $this->makeitso_options->option , TRUE , TRUE );
  }

  /**
   * reserve_customer_id
   *
   * Reserve a customer_id. Only a single MakeItSo thread can handle it.
   *
   * @return boolean
   */
  public function reserve_customer_id()
  {
    if ( !$this->customer_id )
      return FALSE;

    if ( $this->redis->get('ultra/makeitso/customer_id/'.$this->customer_id) )
      return FALSE;

    $this->redis->set( 'ultra/makeitso/customer_id/'.$this->customer_id , getmypid() , 60*5 );

    usleep(250000);

    return ! ! ( getmypid() == $this->redis->get('ultra/makeitso/customer_id/'.$this->customer_id) );
  }

  /**
   * unreserve_customer_id
   *
   * Release a customer_id.
   *
   * @return BOOLEAN
   */
  public function unreserve_customer_id()
  {
    if ( !$this->customer_id )
      return FALSE;

    return $this->redis->del( 'ultra/makeitso/customer_id/'.$this->customer_id );
  }

  /**
   * process
   *
   * Process item we just got from getNext.
   *
   * @return Result object
   */
  public function process()
  {
    if ( ! $this->reserve_customer_id() )
    {
      $defer_seconds = 60;

      // enqueue $this->makeitso_queue_id with defer_seconds delay
      $this->redis_makeitso->add( time() + $defer_seconds , $this->makeitso_queue_id );

      return \make_ok_Result( array() , NULL , 'Customer id '.$this->customer_id.' is currently being handled by another MakeItSo thread' );
    }

    $processMethod = 'process'.$this->makeitso_type;

    dlog('',"processMethod = $processMethod");

    if ( method_exists( $this , $processMethod ) )
    {
      // attempt to reserve $this->makeitso_queue_id

      if ( ! $this->makeitso_queue_id )
        return \make_error_Result( "ERR_API_INTERNAL: Invalid or missing MAKEITSO_QUEUE_ID" );

// TODO: we should not execute this MISO if there is an older PENDING one for the same customer id

      $return = $this->$processMethod();

      $this->unreserve_customer_id();

      return $return;
    }
    else
      // We attempted to process a makeitso_type which does not exist (or it's not handled by this code yet)
      return $this->fail( 'Type '.$this->makeitso_type.' not handled' );
  }

  /**
   * processResetSharedDataLimit
   *
   * Process a ResetSharedDataLimit item
   *
   * @return Result object
   */
  private function processResetSharedDataLimit()
  {
    // check command invocations
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    // important: this is the MW logic layer, not the adapter layer
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;

    return $this->invokeUpdatePlanAndFeatures( $accMiddleware , 'ResetSharedDataLimit|' . $this->makeitso_options->option , TRUE , TRUE );
  }

  /**
   * processFlexSetup
   *
   * Process a FlexSetup item
   *
   * @return Result object
   */
  private function processFlexSetup()
  {
    // check command invocations
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    $lineCredits = new \LineCredits();

    // check if customer already has plan credit
    $balance = $lineCredits->getBalance($this->customer_id);
    if ($balance > 0)
    {
      // use plan credit, fail on errors or warnings
      list ($result, $error_code) = $lineCredits->useLineCredit($this->customer_id);
      if ( ! $result->is_success())
      {
        \Ultra\Lib\DB\ultra_acc_connect();

        if ($errors   = $result->get_errors())
          return $this->fail(implode(',', $errors));

        if ($warnings = $result->get_warnings())
          return $this->fail(implode(',', $warnings));
      }
    }

    // get family information
    $familyApi = new \Ultra\Lib\Services\FamilyAPI();
    $familyApiResult = $familyApi->getFamilyByCustomerID($this->customer_id);

    if ($familyApiResult->is_success())
    {
      $parentCustomerId = $familyApiResult->data_array['parentCustomerId'];
      $payForFamily     = $familyApiResult->data_array['payForFamily'];
      $planCredits      = $familyApiResult->data_array['planCredits'];

      // if parent has family pay enabled
      if ($balance == 0 && $parentCustomerId && $payForFamily && $planCredits > 0)
      {
        // check customer is inactive before transfer
        if ($lineCredits->shouldTransfer($this->customer_id)) {
          // transfer plan credit, fail on errors or warnings
          list ($result, $error_code) = $lineCredits->doTransfer($parentCustomerId, $this->customer_id, 1);
          if ( ! $result->is_success())
          {
            \Ultra\Lib\DB\ultra_acc_connect();

            if ($errors   = $result->get_errors())
              return $this->fail(implode(',', $errors));

            if ($warnings = $result->get_warnings())
              return $this->fail(implode(',', $warnings));
          }
        }
      }
    }

    \Ultra\Lib\DB\ultra_acc_connect();

    return $this->succeed();
  }

  /**
   * processUpgradePlan
   *
   * Process a UpgradePlan item
   *
   * @return Result object
   */
  private function processUpgradePlan()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateUpgradePlanParams( $this->makeitso_options );
    if ( count($errors) )
      return $this->fail( implode( ",", $errors ) );

    // check command invocations
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    // important: this is the MW logic layer, not the adapter layer
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;

    // Perform a CheckBalance for plan change

    $checkBalanceInfo = array();

    $parts = explode('|', $this->makeitso_options->option);
    if (($parts[0] == 'CHANGE' || $parts[0] == 'CHANGEIMMEDIATE') && in_array($parts[1], get_ultra_plans_short()))
    {
      $checkBalanceResult = $accMiddleware->processControlCommand(
        array(
          'command'    => 'CheckBalance', // this is the ACC Command
          'actionUUID' => $this->action_uuid,
          'parameters' => array(
            'iccid'       => $this->makeitso_options->iccid,
            'msisdn'      => $this->makeitso_options->msisdn
          )
        )
      );

      // CheckBalance timeout

      if ( $checkBalanceResult->is_timeout() )    
        return $this->delay( 'CheckBalance timeout' , 'CheckBalance' , NULL );

      // CheckBalance failure

      if ( $checkBalanceResult->has_errors() )
      {
        dlog('',"errors = %s",$checkBalanceResult->get_errors());

        $resultCode = ( isset($checkBalanceResult->data_array['ResultCode']) ) ? $checkBalanceResult->data_array['ResultCode'] : NULL ;

        if ( $checkBalanceResult->data_array['fatal'] )
          return $this->fail( implode( ",", $checkBalanceResult->get_errors() ) , 'CheckBalance' , $resultCode );
        else
          return $this->delay( 'Cannot perform CheckBalance' , 'CheckBalance' , $resultCode );
      }

      $checkBalanceData = $accMiddleware->controlObject->extractDataBodyFromMessage( $checkBalanceResult->data_array['message'] );

      foreach( $checkBalanceData['BalanceValueList']->BalanceValue as $balanceValue )
        $checkBalanceInfo[ $balanceValue->UltraType ] = $balanceValue;

      dlog('',"checkBalanceInfo = %s",$checkBalanceInfo);
    }

    // Perform a QuerySubscriber

    $querySubscriberResult = $accMiddleware->processControlCommand(
      array(
        'command'    => 'QuerySubscriber', // this is the ACC Command
        'actionUUID' => $this->action_uuid,
        'parameters' => array(
          'iccid'       => $this->makeitso_options->iccid,
          'msisdn'      => $this->makeitso_options->msisdn
        )
      )
    );

    // QuerySubscriber timeout

    if ( $querySubscriberResult->is_timeout() )
      return $this->delay( 'QuerySubscriber timeout' , 'QuerySubscriber' , NULL );

    // QuerySubscriber failure

    if ( $querySubscriberResult->has_errors() )
    {
      dlog('',"errors = %s",$querySubscriberResult->get_errors());

      $resultCode = ( isset($querySubscriberResult->data_array['ResultCode']) ) ? $querySubscriberResult->data_array['ResultCode'] : NULL ;

      if ( $querySubscriberResult->data_array['fatal'] )
        return $this->fail( implode( ",", $querySubscriberResult->get_errors() ) , 'QuerySubscriber' , $resultCode );
      else
        return $this->delay( 'Cannot perform QuerySubscriber' , 'QuerySubscriber' , $resultCode );
    }

    // QuerySubscriber success

    $dataBody = $accMiddleware->controlObject->extractDataBodyFromMessage( $querySubscriberResult->data_array['message'] );

    dlog('',"resultQuerySubscriber dataBody = %s",$dataBody);
    dlog('',"option = %s",$this->makeitso_options->option);

    // true if the SOCs are set according to $this->makeitso_options->option
    $verified = FALSE;

    // true if the final call is updatePlanAndFeatures
    $isFinalCall = TRUE;

    if     ( $this->makeitso_options->option == 'V-ENGLISH' )
    {
      $verified    = $this->verifyVoiceMailSoc( $dataBody , 'v_english' );
      $isFinalCall = FALSE;
    }
    elseif ( $this->makeitso_options->option == 'V-SPANISH' )
    {
      $verified    = $this->verifyVoiceMailSoc( $dataBody , 'v_spanish' );
      $isFinalCall = FALSE;
    }
    elseif ( $parts[0] == 'N-WFC' )
    {
      teldata_change_db();

      // check for channel
      $channel = property_exists($this->makeitso_options, 'channel') ? $this->makeitso_options->channel : null;
      $verified    = $this->verifyWIFISoc($dataBody, 'n_wfc', $parts[1], $this->customer_id, $channel);
      $isFinalCall = FALSE;

      // send sms
      if ($verified) {
        \sendSMSWifiSocUpdated($this->customer_id, $parts[1]);
      }

      \Ultra\Lib\DB\ultra_acc_connect();
    }
    elseif ( $parts[0] == 'B-VOICE' )
      $verified = $this->verifyNonDataSoc( $dataBody , 'b_voice',   $parts[1]);
    elseif ( $parts[0] == 'ROAM' )
      $verified = $this->verifyNonDataSoc( $dataBody , 'roam',      $parts[1]);
    elseif ( $parts[0] == 'B-SMS' )
      $verified = $this->verifyNonDataSoc( $dataBody , 'b_sms',     $parts[1]);
    elseif ( $parts[0] == 'A-VOICE-DR' )
      $verified = $this->verifyNonDataSoc( $dataBody , 'a_voice_dr',$parts[1]);
    elseif ( $parts[0] == 'A-SMS-DR' )
      $verified = $this->verifyNonDataSoc( $dataBody , 'a_sms_dr',  $parts[1]);
    elseif (($parts[0] == 'CHANGE' || $parts[0] == 'CHANGEIMMEDIATE') && in_array($parts[1], get_ultra_plans_short()))
    {
      // DATAQ-22, DATAQ-29: ensure subscriber is on the target wholesale plan
      list($proceed, $result) = $this->ensureCorrectWholesalePlan($accMiddleware, $dataBody);
      if ( ! $proceed)
        return $result;

      $throttling    = new \Throttling($this->makeitso_options->ultra_plan);
      $throttleSpeed = $throttling->detectThrottleSpeedFromBalanceValues($checkBalanceData['BalanceValueList']->BalanceValue);

      $verified = $this->verifyChangePlan( $dataBody , $parts[1] , $checkBalanceInfo, $throttleSpeed, $parts[0] );
      $isFinalCall = FALSE;
    }
    elseif ( $parts[0] == 'A-DATA-BLK' )
      $verified    = $this->verifyDataSoc( $dataBody, 'a_data_blk',    $parts[1]);
    elseif ( $parts[0] == 'A-DATA-BLK-DR' )
      $verified    = $this->verifyDataSoc( $dataBody, 'a_data_blk_dr', $parts[1]);
    elseif ( $parts[0] == 'A-DATA-POOL-1' )
      $verified    = $this->verifyDataSoc( $dataBody, 'a_data_pool_1', $parts[1]);
    else
      return \make_error_Result( 'Not handled yet' );

    // we are done
    if ( $verified )
      return $this->succeed();

    return $this->invokeUpdatePlanAndFeatures( $accMiddleware , $this->makeitso_options->option , $isFinalCall , TRUE );
  }

  /**
   * verifyChangePlan
   *
   * Verifies if the plan SOCs are the expected ones for $newPlan, using the data we got from QuerySubscriber and CheckBalance.
   * $newPlan is one of get_ultra_plans_short
   * QuerySubscriber info is used to match SOCS.
   * CheckBalance info is used to match Data Base Bucket value.
   *
   * @return boolean
   */
  private function verifyChangePlan( $dataQuerySubscriber , $newPlan , $checkBalanceInfo, $throttleSpeed, $option = null )
  {
    $verifyChangePlan = TRUE;

    // SOCs in the new plan
    $accSocsPlanConfigNewPlan = \Ultra\MvneConfig\getAccSocsUltraPlanConfig($newPlan, $this->customer_id);

    $throttling = new \Throttling($newPlan);
    $throttling->setCurrentThrottleSpeed($throttleSpeed);
    $throttling->adjustPlanConfigForThrottle($accSocsPlanConfigNewPlan);

    list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

    // dlog('',"dataQuerySubscriber = %s",$dataQuerySubscriber); // data from QuerySubscriber
    // dlog('',"checkBalanceInfo = %s",$checkBalanceInfo); // data from CheckBalance
    // dlog('',"accSocsPlanConfigNewPlan = %s",$accSocsPlanConfigNewPlan); // SOCs configuration for new plan

    // API-178: new Plan Included Data SOC for all plans
    if ( ! empty($accSocsPlanConfigNewPlan['a_data_blk_plan']) && isset($checkBalanceInfo['WPRBLK20 Limit'])
      && ($accSocsPlanConfigNewPlan['a_data_blk_plan'] != $checkBalanceInfo['WPRBLK20 Limit']->Value))
    {
      dlog('',"Expected Plan Included Data = %s", $accSocsPlanConfigNewPlan['a_data_blk_plan']);
      return FALSE;
    }

    if ( ! empty($accSocsPlanConfigNewPlan['a_data_blk1536_plan']) && isset($checkBalanceInfo['WPRBLK33S Limit'])
      && ($accSocsPlanConfigNewPlan['a_data_blk1536_plan'] != $checkBalanceInfo['WPRBLK33S Limit']->Value))
    {
      dlog('',"Expected Plan Included Data = %s", $accSocsPlanConfigNewPlan['a_data_blk1536_plan']);
      return FALSE;
    }

    if ( ! empty($accSocsPlanConfigNewPlan['a_data_blk1024_plan']) && isset($checkBalanceInfo['WPRBLK35S Limit'])
      && ($accSocsPlanConfigNewPlan['a_data_blk1024_plan'] != $checkBalanceInfo['WPRBLK35S Limit']->Value))
    {
      dlog('',"Expected Plan Included Data = %s", $accSocsPlanConfigNewPlan['a_data_blk1024_plan']);
      return FALSE;
    }

    // DOP-109: Voice Base Bucket
    if ( ! empty($accSocsPlanConfigNewPlan['b_voice'])
      && isset($checkBalanceInfo['DA1'])
      && $accSocsPlanConfigNewPlan['b_voice'] != $checkBalanceInfo['DA1']->Value
      && $option != 'CHANGEIMMEDIATE' )
    {
      dlog('', 'Voice Base bucket: expected = %s , found = %s', $accSocsPlanConfigNewPlan['b_voice'], $checkBalanceInfo['DA1']->Value);
      return FALSE;
    }

    // DOP-109: SMS Base Bucket
    if ( ! empty($accSocsPlanConfigNewPlan['b_sms'])
      && isset($checkBalanceInfo['DA2'])
      && $accSocsPlanConfigNewPlan['b_sms'] != $checkBalanceInfo['DA2']->Value
      && $option != 'CHANGEIMMEDIATE' )
    {
      dlog('', 'SMS Base bucket: expected = %s , found = %s', $accSocsPlanConfigNewPlan['b_sms'], $checkBalanceInfo['DA2']->Value);
      return FALSE;
    }

    // loop through SOCs to be added
    foreach( $accSocsPlanConfigNewPlan as $ultraSocName => $featureValue )
    {
      $socFound = FALSE;

      // loop through SOCs in the current plan
      if ( isset($dataQuerySubscriber['AddOnFeatureInfoList']) && is_object($dataQuerySubscriber['AddOnFeatureInfoList']) )
        foreach( $dataQuerySubscriber['AddOnFeatureInfoList']->AddOnFeatureInfo as $addOnFeature )
          if ( $ultraSocName == $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
            $socFound = TRUE;

      // $ultraSocName is missing
      if ( ! $socFound )
        $verifyChangePlan = FALSE;
    }

    return $verifyChangePlan;
  }

  /**
   * verifyDataSoc
   *
   * Verifies $dataSoc according to the result of QuerySubscriber
   *
   * @return boolean
   */
  private function verifyDataSoc( $dataQuerySubscriber , $dataSoc , $value )
  {
    // ADD or RESET $dataSoc independently of the result of QuerySubscriber
    return FALSE;
  }

  /**
   * verifyNonDataSoc
   *
   * Verifies if we have $nonDataSoc according to the result of QuerySubscriber
   *
   * @return boolean
   */
  private function verifyNonDataSoc( $dataQuerySubscriber , $nonDataSoc , $value )
  {
    // ADD or INCREMENT $nonDataSoc independently of the result of QuerySubscriber
    return FALSE;
  }

  /**
   * verifyWIFISoc
   *
   * Verifies if we have $wifiSoc according to the result of QuerySubscriber
   *
   * @return boolean
   */
  private function verifyWIFISoc($dataQuerySubscriber, $wifiSoc, $action, $customerId, $channel=null)
  {
    $hasWifiSoc = FALSE;

    list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

    dlog('',"dataQuerySubscriber      = %s",$dataQuerySubscriber);
    dlog('',"wifiSoc                  = %s",$wifiSoc);

    // set default channel option attribute
    $channel = !empty($channel) ? $channel : 'WIFI_CALLING.UNKNOWN';

    // loop through SOCs in the current plan
    if ( ! empty($dataQuerySubscriber['AddOnFeatureInfoList']) && is_object( $dataQuerySubscriber['AddOnFeatureInfoList'] ) )
      foreach( $dataQuerySubscriber['AddOnFeatureInfoList']->AddOnFeatureInfo as $addOnFeature )
        if ( $wifiSoc == $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
          $hasWifiSoc = TRUE;

    if ($hasWifiSoc && $action == 'ADD') {
      set_wifi_calling_flag($customerId, true);
      set_wifi_calling_channel_flag($customerId, $channel, true);

      return true;
    }

    if (!$hasWifiSoc && $action == 'REMOVE') {
      if (get_wifi_calling_flag($customerId)) {
        set_wifi_calling_flag($customerId, false);
      }

      set_wifi_calling_channel_flag($customerId, $channel, false);

      return true;
    }

    return false;
  }

  /**
   * verifyVoiceMailSoc
   *
   * Verifies if we have $voiceMailSoc according to the result of QuerySubscriber
   * V-ENGLISH vs V-SPANISH
   *
   * @return boolean
   */
  private function verifyVoiceMailSoc( $dataQuerySubscriber , $voiceMailSoc )
  {
    $verifyVoiceMailSoc = FALSE;

    list( $accSocsDefinition , $accSocsDefinitionByUltraSoc , $ultraSocsByPlanId ) = \Ultra\MvneConfig\getAccSocsDefinitions();

    dlog('',"dataQuerySubscriber      = %s",$dataQuerySubscriber);
    dlog('',"voiceMailSoc             = %s",$voiceMailSoc);

    // loop through SOCs in the current plan
    if ( ! empty($dataQuerySubscriber['AddOnFeatureInfoList']) && is_object( $dataQuerySubscriber['AddOnFeatureInfoList'] ) )
      foreach( $dataQuerySubscriber['AddOnFeatureInfoList']->AddOnFeatureInfo as $addOnFeature )
        if ( $voiceMailSoc == $ultraSocsByPlanId[ $addOnFeature->FeatureID ] )
          $verifyVoiceMailSoc = TRUE;

    return $verifyVoiceMailSoc;
  }

  /**
   * processUpdateThrottleSpeed
   *
   * Update SOCs to chosen throttle speed using UpdatePlanAndFeatures
   *
   * @return boolean
   */
  private function processUpdateThrottleSpeed()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateUpdateThrottleSpeedParams($this->makeitso_options);

    if (count($errors))
      return $this->fail( implode( ",", $errors ) );

    $commandInvocation = $this->getCommandInvocationObject();

    if ( $commandInvocation->isOccupied() )
      return $this->delay( 'Command Invocation pending' );

    // perform a QuerySubscriber
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;
    if ( ! $statusData = $this->querySubscriber($accMiddleware))
      return \make_ok_Result();

    dlog('', 'QuerySubscriber data = %s', $statusData);

    // validate subscriber status
    if (empty($statusData->SubscriberStatus))
      return $this->fail("SubscriberStatus not available from QuerySubscriber response" , 'QuerySubscriber');

    // if not Active, fail
    if ( $statusData->SubscriberStatus != 'Active' )
      return $this->fail("SubscriberStatus is not Active : {$statusData->SubscriberStatus}");

    \logTrace("statusData = ".json_encode($statusData));

    $addOnFeatureInfo = ( isset($statusData->AddOnFeatureInfoList) )
      ? $statusData->AddOnFeatureInfoList->AddOnFeatureInfo
      : array(); // STANDBY

    // update plan and features if required

    $throttling = new \Throttling($this->makeitso_options->ultra_plan);

    if ($throttling->checkThrottleUpdateRequired($addOnFeatureInfo, $this->makeitso_options->throttle_speed))
    {
      $type = \Ultra\UltraConfig\isFlexPlan($this->makeitso_options->ultra_plan) ? 'THROTTLE-SPEED-BAN' : 'THROTTLE-SPEED';
      return $this->invokeUpdatePlanAndFeatures(
        $accMiddleware,
        "$type|" . $this->makeitso_options->throttle_speed,
        FALSE
      );
    }

    if (\Ultra\Lib\MVNE\MakeItSo\checkCommandInvocationCompleted($this->action_uuid, 'UpdatePlanAndFeatures'))
    {
      teldata_change_db();

      $term = \Throttling::throttleSpeedMarketingTerms($this->makeitso_options->throttle_speed);

      if ($option = customer_options_get_throttle_speed($this->customer_id))
        customer_options_update_throttle_speed($this->customer_id, $this->makeitso_options->lte_select_method, $term);
      else
        customer_options_insert_throttle_speed($this->customer_id, $this->makeitso_options->lte_select_method, $term);

      \Ultra\Lib\DB\ultra_acc_connect();

      // record previous values in mrc throttle updates
      $balance_values = json_decode($this->makeitso_options->balance_values);

      \logit('balance_values = ' . json_encode($balance_values));

      foreach ($balance_values as $soc => $values)
      {
        \logit("$soc : " . $values->Used . '/' . $values->Limit);
        mrcThrottleUpdatesInsert(
          $this->customer_id,
          $this->makeitso_options->plan_tracker_id,
          $soc,
          $values->Used,
          $values->Limit
        );
      }
    }
    else
    {
      // no update required
      dlog('', 'No throttle update required');
    }

    return $this->succeed();
  }

  /**
   * processAddMintData
   *
   * Process a AddMintData task for MINT
   *
   * @return Result object
   */
  private function processAddMintData()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateAddMintDataParams( $this->makeitso_options );

    if ( count($errors) )
      return $this->fail( implode( ",", $errors ) );

    // Check subscriber if Command Invocations has blocked this subscriber. If so, delay (Reason="Command Invocation pending").

    $commandInvocation = $this->getCommandInvocationObject();

    if ( $commandInvocation->isOccupied() )
      return $this->delay( 'Command Invocation pending' );

    // perform a QuerySubscriber
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;
    if ( ! $statusData = $this->querySubscriber($accMiddleware))
      return \make_ok_Result();

    dlog('', 'QuerySubscriber data = %s', $statusData);

    // validate subscriber status
    if (empty($statusData->SubscriberStatus))
      return $this->fail("SubscriberStatus not available from QuerySubscriber response" , 'QuerySubscriber');

    // if not Active, fail
    if ( $statusData->SubscriberStatus != 'Active' )
      return $this->fail("SubscriberStatus is not Active : {$statusData->SubscriberStatus}");

    \logTrace("statusData = ".json_encode($statusData));

    // If Add On Data SOC is present
    // a) compute usage threshold from CheckBalance data
    // b) log an error if usage threshold has not been met
    // c) remove Data Add On SOC if usage threshold has been met

    if ( $this->hasMintDataAddOnSOC($statusData, $this->makeitso_options->data_add_on) )
    {
      // compute usage threshold
      $dataSocInfo = \Ultra\UltraConfig\getBoltOnInfoByUpgradePlanSoc($this->makeitso_options->data_add_on);
      list( $mintDataAddOnUsagePercentage , $error ) = $this->getMintDataAddOnUsagePercentage($accMiddleware, $dataSocInfo['type']);

      if ( ! empty( $error ) )
        return $this->fail( $error );

      $mindDataAddonThreshold = \Ultra\UltraConfig\getMindDataAddonThreshold();

      \logInfo( "mintDataAddOnUsagePercentage = $mintDataAddOnUsagePercentage ; mindDataAddonThreshold = $mindDataAddonThreshold" );

      if ( $mintDataAddOnUsagePercentage < ( 100 - $mindDataAddonThreshold ) )
      {
        $error = "Usage Threshold not met ( $mintDataAddOnUsagePercentage % )";
        \logError( $error );
        return $this->fail( $error );
      }

      // remove Data Add On SOC
      return $this->invokeUpdatePlanAndFeatures( $accMiddleware , 'REMOVE-DATA-MINT|'.$this->makeitso_options->data_add_on );
    }

    // Add Data SOC and finish
    return $this->invokeUpdatePlanAndFeatures( $accMiddleware , $this->makeitso_options->data_add_on , TRUE , TRUE );
  }

  /**
   * processSuspendMint
   *
   * Process a Suspend task for MINT
   *
   * @return Result object
   */
  private function processSuspendMintPlan()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateSuspendParams( $this->makeitso_options );

    if ( count($errors) )
      return $this->fail( implode( ",", $errors ) );
    else
    {
      // Check subscriber if Command Invocations has blocked this subscriber. If so, delay (Reason="Command Invocation pending").

      $commandInvocation = $this->getCommandInvocationObject();

      if ( $commandInvocation->isOccupied() )
        return $this->delay( 'Command Invocation pending' );

      // perform a QuerySubscriber
      $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;
      if ( ! $statusData = $this->querySubscriber($accMiddleware))
        return \make_ok_Result();
      dlog('', 'QuerySubscriber data = %s', $statusData);

      // validate subscriber status
      if (empty($statusData->SubscriberStatus))
        return $this->fail("SubscriberStatus not available from QuerySubscriber response" , 'QuerySubscriber');

      // if 'Suspended', we are done
      if ( $statusData->SubscriberStatus == 'Suspended' )
        return $this->succeed();

      // fail is subscriber status is inactive
      if ( $statusData->SubscriberStatus == 'Inactive' )
        return $this->fail("SubscriberStatus Inactive during Suspend for MINT SIM");

      // if subscriber doesn't have R-LIMITED nor R-UNLIMITED, execute invoke UpdatePlanAndFeatures
      if (\Ultra\Lib\MVNE\MakeItSo\statusDataContainsSMSVoice( $statusData ))
        return $this->invokeUpdatePlanAndFeatures( $accMiddleware , 'MINT-SUSPEND' , FALSE , FALSE );

      // if 'Active', we must suspend the account
      if ( $statusData->SubscriberStatus == 'Active' )
      {
        $suspendSubscriberResult = $accMiddleware->processControlCommand(
          array(
            'command'         => 'SuspendSubscriber', // this is the ACC Command
            'actionUUID'      => $this->action_uuid,
            'makeitsoQueueId' => $this->makeitso_queue_id,
            'parameters' => array(
              'customer_id' => $this->customer_id,
              'iccid'       => $this->makeitso_options->iccid,
              'msisdn'      => $this->makeitso_options->msisdn
            )
          )
        );

        dlog('',"suspendSubscriberResult = %s",$suspendSubscriberResult);

        // SuspendSubscriber timeout
        if ( $suspendSubscriberResult->is_timeout() )
          return $this->delay( 'SuspendSubscriber timeout' , 'SuspendSubscriber' , NULL );

        // SuspendSubscriber failure
        if ( $suspendSubscriberResult->has_errors() )
        {
          dlog('',"errors = %s",$suspendSubscriberResult->get_errors());

          $resultCode = ( isset($suspendSubscriberResult->data_array['ResultCode']) ) ? $suspendSubscriberResult->data_array['ResultCode'] : NULL ;

          if ( !isset($suspendSubscriberResult->data_array['fatal']) || $suspendSubscriberResult->data_array['fatal'] )
            return $this->fail( implode( ",", $suspendSubscriberResult->get_errors() ) , 'SuspendSubscriber' , $resultCode );
          else
            return $this->delay( 'Biphasic command unavailable' , 'SuspendSubscriber' , $resultCode );
        }

        // SuspendSubscriber succeeded
        return $this->pending( 'Invoked SuspendSubscriber' );
      }

      return $this->fail("SubscriberStatus {$statusData->SubscriberStatus} not handled");
    }
  }

  /**
   * processSuspend
   *
   * Process a Suspend item
   *
   * @return Result object
   */
  private function processSuspend()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateSuspendParams( $this->makeitso_options );

    if ( count($errors) )
      return $this->fail( implode( ",", $errors ) );
    else
    {
      // Check subscriber if Command Invocations has blocked this subscriber. If so, delay (Reason="Command Invocation pending").

      $commandInvocation = $this->getCommandInvocationObject();

      if ( $commandInvocation->isOccupied() )
        return $this->delay( 'Command Invocation pending' );

      // perform a QuerySubscriber
      $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;
      if ( ! $statusData = $this->querySubscriber($accMiddleware))
        return \make_ok_Result();
      dlog('', 'QuerySubscriber data = %s', $statusData);

      // validate subscriber status
      if (empty($statusData->SubscriberStatus))
        return $this->fail("SubscriberStatus not available from QuerySubscriber response" , 'QuerySubscriber');

      // if 'Suspended', we are done
      if ( $statusData->SubscriberStatus == 'Suspended' )
        return $this->succeed();

      // if 'Inactive', we must Reactivate the account
      if ( $statusData->SubscriberStatus == 'Inactive' )
      {
        $reactivateSubscriberResult = $accMiddleware->processControlCommand(
          array(
            'command'         => 'ReactivateSubscriber', // this is the ACC Command
            'actionUUID'      => $this->action_uuid,
            'makeitsoQueueId' => $this->makeitso_queue_id,
            'parameters' => array(
              'customer_id'        => $this->customer_id,
              'iccid'              => $this->makeitso_options->iccid,
              'msisdn'             => $this->makeitso_options->msisdn,
              'ultra_plan'         => $this->makeitso_options->ultra_plan,
              'preferred_language' => $this->makeitso_options->preferred_language,
              'wholesale_plan'     => $this->makeitso_options->wholesale_plan
            )
          )
        );

        dlog('',"reactivateSubscriberResult = %s",$reactivateSubscriberResult);

        // ReactivateSubscriber timeout

        if ( $reactivateSubscriberResult->is_timeout() )
          return $this->delay( 'ReactivateSubscriber timeout' , 'ReactivateSubscriber' , NULL );

        // ReactivateSubscriber failure

        if ( $reactivateSubscriberResult->has_errors() )
        {
          dlog('',"errors = %s",$reactivateSubscriberResult->get_errors());

          $resultCode = ( isset($reactivateSubscriberResult->data_array['ResultCode']) ) ? $reactivateSubscriberResult->data_array['ResultCode'] : NULL ;

          if ( $reactivateSubscriberResult->data_array['fatal'] )
            return $this->fail( implode( ",", $reactivateSubscriberResult->get_errors() ) , 'ReactivateSubscriber' , $resultCode );
          else
            return $this->delay( 'Biphasic command unavailable' , 'ReactivateSubscriber' , $resultCode );
        }

        // ReactivateSubscriber success

        return $this->pending( 'Invoked ReactivateSubscriber' );
      }

      // API-156: Save current balance. Invoke CheckBalance if values are not cached.
      $this->logBalance($accMiddleware, $statusData);

      // if 'Active', we must suspend the account
      if ( $statusData->SubscriberStatus == 'Active' )
      {
        $suspendSubscriberResult = $accMiddleware->processControlCommand(
          array(
            'command'         => 'SuspendSubscriber', // this is the ACC Command
            'actionUUID'      => $this->action_uuid,
            'makeitsoQueueId' => $this->makeitso_queue_id,
            'parameters' => array(
              'customer_id' => $this->customer_id,
              'iccid'       => $this->makeitso_options->iccid,
              'msisdn'      => $this->makeitso_options->msisdn
            )
          )
        );

        dlog('',"suspendSubscriberResult = %s",$suspendSubscriberResult);

        // SuspendSubscriber timeout

        if ( $suspendSubscriberResult->is_timeout() )
          return $this->delay( 'SuspendSubscriber timeout' , 'SuspendSubscriber' , NULL );

        // SuspendSubscriber failure

        if ( $suspendSubscriberResult->has_errors() )
        {
          dlog('',"errors = %s",$suspendSubscriberResult->get_errors());

          $resultCode = ( isset($suspendSubscriberResult->data_array['ResultCode']) ) ? $suspendSubscriberResult->data_array['ResultCode'] : NULL ;

          if ( !isset($suspendSubscriberResult->data_array['fatal']) || $suspendSubscriberResult->data_array['fatal'] )
            return $this->fail( implode( ",", $suspendSubscriberResult->get_errors() ) , 'SuspendSubscriber' , $resultCode );
          else
            return $this->delay( 'Biphasic command unavailable' , 'SuspendSubscriber' , $resultCode );
        }

        // SuspendSubscriber succeeded
        return $this->pending( 'Invoked SuspendSubscriber' );
      }

      return $this->fail("SubscriberStatus {$statusData->SubscriberStatus} not handled");
    }
  }

  /**
   * processRenewEndMintPlan
   *
   * See http://wiki.hometowntelecom.com:8090/display/SPEC/MW+%3A+MakeitsoRenewPlan
   *
   * @return Result object
   */
  private function processRenewEndMintPlan()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateRenewEndMintPlanParams( $this->makeitso_options );
    if (count($errors))
      return $this->fail(implode(',', $errors));

    // Check subscriber if Command Invocations has blocked this subscriber. If so, delay (Reason="Command Invocation pending").
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    // perform QuerySubscriber
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;
    if ( ! $statusData = $this->querySubscriber($accMiddleware))
      return \make_ok_Result();
    dlog('', 'QuerySubscriber data = %s', $statusData);

    // validate subscriber status
    if (empty($statusData->SubscriberStatus))
      return $this->fail("SubscriberStatus not available from QuerySubscriber response" , 'QuerySubscriber');

    // API-156: Save current balance. Invoke CheckBalance if values are not cached.
//TODO: avoid performing this operation more than once
    $this->logBalance($accMiddleware, $statusData);

    // if 'Inactive' or 'Suspended', we fail
    if ( $statusData->SubscriberStatus != 'Active' )
      return $this->fail("MINT SIM {$statusData->SubscriberStatus} when attempting to renew a plan (end cycle)" , 'QuerySubscriber');

    // DATAQ-22, DATAQ-29: ensure subscriber is on the target wholesale plan
    list($proceed, $result) = $this->ensureCorrectWholesalePlan($accMiddleware, $statusData);
    if ( ! $proceed )
      return $result;

    $addOnFeatureInfo = ( isset($statusData->AddOnFeatureInfoList) )
      ? $statusData->AddOnFeatureInfoList->AddOnFeatureInfo
      : array(); // STANDBY

    $planCheck  = \Ultra\Lib\MVNE\MakeItSo\planCheck(
      $addOnFeatureInfo,
      $this->customer_id,
      'STANDBY',
      null
    );

    // check if AddOnData SOC is set - we must remove it if present
    $addOnDataSocId = $this->detectAddOnDataSocId( $addOnFeatureInfo );

    // we must remove AddOnData SOC
    if ( $addOnDataSocId )
      $planCheck = FALSE;

    if ( ! $planCheck )
    {
      // SOCs are not correct, we must fix with UpdatePlanAndFeatures

      // be sure retail plan id is set to R-LIMITED
      $this->makeitso_options->ultra_plan = 'STANDBY';
      return $this->invokeUpdatePlanAndFeatures( $accMiddleware , 'CHANGE|'.$this->makeitso_options->ultra_plan , FALSE , FALSE );
    }

    // if we don't have a complete RenewPlan invocation, run that
    if ( ! \Ultra\Lib\MVNE\MakeItSo\checkCommandInvocationCompleted($this->action_uuid, 'RenewPlan'))
    {
      $preferred_language = $this->detectLanguageSocId( $addOnFeatureInfo );

      // be sure retail plan id is set to R-LIMITED
      $this->makeitso_options->ultra_plan = 'STANDBY';
      return $this->invokeRenewPlan( $accMiddleware , $preferred_language, null, FALSE );
    }

    // fail if a PAID event exists for makeitso queue id
    if (\Ultra\Lib\MVNE\MakeItSo\exists_makeitso_paid_event($this->makeitso_queue_id))
      return $this->fail("A paid event already exists for MakeItSo Queue ID {$this->makeitso_queue_id}" , 'RenewEndMintPlan');

    return $this->invokeUpdatePlanAndFeatures( $accMiddleware , 'CHANGE|'.$this->makeitso_options->ultra_plan , TRUE , FALSE );
  }

  /**
   * processRenewMidMintPlan
   *
   * See http://wiki.hometowntelecom.com:8090/display/SPEC/MW+%3A+MakeitsoRenewPlan
   *
   * @return Result object
   */
  private function processRenewMidMintPlan()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateRenewMidMintPlanParams( $this->makeitso_options );
    if (count($errors))
      return $this->fail(implode(',', $errors));

    // Check subscriber if Command Invocations has blocked this subscriber. If so, delay (Reason="Command Invocation pending").
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    // perform QuerySubscriber
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;
    if ( ! $statusData = $this->querySubscriber($accMiddleware))
      return \make_ok_Result();
    dlog('', 'QuerySubscriber data = %s', $statusData);

    // validate subscriber status
    if (empty($statusData->SubscriberStatus))
      return $this->fail("SubscriberStatus not available from QuerySubscriber response" , 'QuerySubscriber');

    // API-156: Save current balance. Invoke CheckBalance if values are not cached.
//TODO: avoid performing this operation more than once
    $this->logBalance($accMiddleware, $statusData);

    // if 'Inactive' or 'Suspended', we fail
    if ( $statusData->SubscriberStatus != 'Active' )
      return $this->fail("MINT SIM {$statusData->SubscriberStatus} when attempting to renew a plan (mid cycle)" , 'QuerySubscriber');

    // DATAQ-22, DATAQ-29: ensure subscriber is on the target wholesale plan
    list($proceed, $result) = $this->ensureCorrectWholesalePlan($accMiddleware, $statusData);
    if ( ! $proceed )
      return $result;

    $addOnFeatureInfo = ( isset($statusData->AddOnFeatureInfoList) )
      ? $statusData->AddOnFeatureInfoList->AddOnFeatureInfo
      : array(); // STANDBY

    $planCheck  = \Ultra\Lib\MVNE\MakeItSo\planCheck(
      $addOnFeatureInfo,
      $this->customer_id,
      $this->makeitso_options->ultra_plan,
      null
    );

    // check if AddOnData SOC is set - we must remove it if present
    $addOnDataSocId = $this->detectAddOnDataSocId( $addOnFeatureInfo );

    // we must remove AddOnData SOC
    if ( $addOnDataSocId || $this->hasMintDataAddOnSOC($statusData))
      $planCheck = FALSE;

    if ( ! $planCheck )
    {
      // SOCs are not correct, we must fix with UpdatePlanAndFeatures
      return $this->invokeUpdatePlanAndFeatures( $accMiddleware , 'CHANGE|'.$this->makeitso_options->ultra_plan , FALSE , FALSE );
    }

    // SOCs are correct, we can invoke RenewPlan

    $preferred_language = $this->detectLanguageSocId( $addOnFeatureInfo );

    return $this->invokeRenewPlan( $accMiddleware , $preferred_language, null, TRUE );
  }

  /**
   * processRenewMintPlan
   *
   * Renew task for a Mint SIM when transitioning from Suspended to Active
   *
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/MW+%3A+MakeitsoRenewPlan
   * @see http://wiki.hometowntelecom.com:8090/display/SPEC/ACC+Flows+to+Ensure+Correct+Billin
   *
   * @return Result object
   */
  private function processRenewMintPlan()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateRenewMintPlanParams( $this->makeitso_options );
    if (count($errors))
      return $this->fail(implode(',', $errors));

    // Check subscriber if Command Invocations has blocked this subscriber. If so, delay (Reason="Command Invocation pending").
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    // perform QuerySubscriber
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;
    if ( ! $statusData = $this->querySubscriber($accMiddleware))
      return \make_ok_Result();
    dlog('', 'QuerySubscriber data = %s', $statusData);

    // validate subscriber status
    if (empty($statusData->SubscriberStatus))
      return $this->fail("SubscriberStatus not available from QuerySubscriber response" , 'QuerySubscriber');

    // if 'Inactive', we fail
    if ( $statusData->SubscriberStatus == 'Inactive' )
      return $this->fail('MINT SIM Inactive when attempting to renew a plan (mid cycle)' , 'QuerySubscriber');

    // check balance and log
    $this->logBalance($accMiddleware, $statusData);

    // if 'Suspended', we have to restore the account
    if ($statusData->SubscriberStatus == 'Suspended')
    {
      // be sure retail plan id is set to R-LIMITED
      $this->makeitso_options->ultra_plan = 'STANDBY';
      return $this->invokeRestoreSubscriber($accMiddleware);
    }

     // if not 'Active', we fail
    if ( $statusData->SubscriberStatus != 'Active' )
      return $this->fail("MINT SIM {$statusData->SubscriberStatus} when attempting to renew a mint plan" , 'QuerySubscriber');

    // DATAQ-22, DATAQ-29: ensure subscriber is on the target wholesale plan
    list($proceed, $result) = $this->ensureCorrectWholesalePlan($accMiddleware, $statusData);
    if ( ! $proceed )
      return $result;

    $addOnFeatureInfo = ( isset($statusData->AddOnFeatureInfoList) )
      ? $statusData->AddOnFeatureInfoList->AddOnFeatureInfo
      : array(); // STANDBY

    $planCheck = (\Ultra\Lib\MVNE\MakeItSo\checkForLimitedPlanSoc($addOnFeatureInfo) || \Ultra\Lib\MVNE\MakeItSo\checkForLimitedPlanId($statusData->PlanId));

    // check if AddOnData SOC is set - we must remove it if present
    $addOnDataSocId = $this->detectAddOnDataSocId( $addOnFeatureInfo );

    // we must remove AddOnData SOC
    if ( $addOnDataSocId )
      $planCheck = FALSE;

    if ( ! $planCheck )
    {
      // SOCs are not correct, we must fix with UpdatePlanAndFeatures

      // be sure retail plan id is set to R-LIMITED
      $this->makeitso_options->ultra_plan = 'STANDBY';
      return $this->invokeUpdatePlanAndFeatures( $accMiddleware , 'CHANGE|'.$this->makeitso_options->ultra_plan , FALSE , FALSE );
    }

    // if we don't have a complete RenewPlan invocation, run that
    if ( ! \Ultra\Lib\MVNE\MakeItSo\checkCommandInvocationCompleted($this->action_uuid, 'RenewPlan'))
    {
      // SOCs are correct, we can invoke RenewPlan

      $preferred_language = $this->detectLanguageSocId( $addOnFeatureInfo );

      // be sure retail plan id is set to R-LIMITED
      $this->makeitso_options->ultra_plan = 'STANDBY';
      return $this->invokeRenewPlan( $accMiddleware , $preferred_language, null, FALSE );
    }

    // fail if a PAID event exists for makeitso queue id
    if (\Ultra\Lib\MVNE\MakeItSo\exists_makeitso_paid_event($this->makeitso_queue_id))
      return $this->fail("A paid event already exists for MakeItSo Queue ID {$this->makeitso_queue_id}" , 'RenewMintPlan');

    // final UpdatePlanAndFeatures call
    return $this->invokeUpdatePlanAndFeatures( $accMiddleware , 'CHANGE|'.$this->makeitso_options->ultra_plan , TRUE , FALSE );
  }

  /**
   * processRenewPlan
   *
   * Process a RenewPlan item
   * See http://wiki.hometowntelecom.com:8090/display/SPEC/MW+%3A+MakeitsoRenewPlan
   * Used for:
   * - End of Month Renewal
   * - Provision to Active
   * - Suspended to Active
   * Brands: all except for Mint
   *
   * @return Result object
   */
  private function processRenewPlan()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateRenewParams( $this->makeitso_options );
    if (count($errors))
      return $this->fail(implode(',', $errors));

    // Check subscriber if Command Invocations has blocked this subscriber. If so, delay (Reason="Command Invocation pending").
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    // perform QuerySubscriber
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;
    if ( ! $statusData = $this->querySubscriber($accMiddleware))
      return \make_ok_Result();
    dlog('', 'QuerySubscriber data = %s', $statusData);

    // validate subscriber status
    if (empty($statusData->SubscriberStatus))
      return $this->fail("SubscriberStatus not available from QuerySubscriber response" , 'QuerySubscriber');

    // if 'Suspended', we have to restore the account
    if ($statusData->SubscriberStatus == 'Suspended')
    {
      $restoreSubscriberResult = $accMiddleware->processControlCommand(
        array(
          'command'    => 'RestoreSubscriber', // this is the ACC Command
          'actionUUID' => $this->action_uuid,
          'parameters' => array(
            'customer_id'        => $this->customer_id,
            'iccid'              => $this->makeitso_options->iccid,
            'msisdn'             => $this->makeitso_options->msisdn
          )
        )
      );

      dlog('',"restoreSubscriberResult = %s",$restoreSubscriberResult);

      // RestoreSubscriber timeout
      if ( $restoreSubscriberResult->is_timeout() )
        return $this->delay( 'RestoreSubscriber timeout' , 'RestoreSubscriber' , NULL );

      // RestoreSubscriber error
      if ( $restoreSubscriberResult->has_errors() )
      {
        dlog('',"errors = %s",$restoreSubscriberResult->get_errors());

        $resultCode = ( isset($restoreSubscriberResult->data_array['ResultCode']) ) ? $restoreSubscriberResult->data_array['ResultCode'] : NULL ;

        if ( $restoreSubscriberResult->data_array['fatal'] )
          return $this->fail( implode( ",", $restoreSubscriberResult->get_errors() ) , 'RestoreSubscriber' , $resultCode );
        else
          return $this->delay( 'Biphasic command unavailable' , 'RestoreSubscriber' , $resultCode );
      }

      // RestoreSubscriber success
      return $this->pending( 'Invoked RestoreSubscriber' );
    }

    // if 'Inactive', we must Reactivate the account
    if ($statusData->SubscriberStatus == 'Inactive')
    {
      $reactivateSubscriberResult = $accMiddleware->processControlCommand(
        array(
          'command'         => 'ReactivateSubscriber', // this is the ACC Command
          'actionUUID'      => $this->action_uuid,
          'makeitsoQueueId' => $this->makeitso_queue_id,
          'parameters' => array(
            'customer_id'        => $this->customer_id,
            'iccid'              => $this->makeitso_options->iccid,
            'msisdn'             => $this->makeitso_options->msisdn,
            'ultra_plan'         => $this->makeitso_options->ultra_plan,
            'preferred_language' => $this->makeitso_options->preferred_language,
            'wholesale_plan'     => $this->makeitso_options->wholesale_plan // VYT: re-activation wholesale plan does not have to correspond to the de-activation plan
          )
        )
      );
      dlog('',"reactivateSubscriberResult = %s",$reactivateSubscriberResult);

      // ReactivateSubscriber timeout
      if ( $reactivateSubscriberResult->is_timeout() )
        return $this->delay( 'ReactivateSubscriber timeout' , 'ReactivateSubscriber' , NULL );

      // ReactivateSubscriber error
      if ( $reactivateSubscriberResult->has_errors() )
      {
        dlog('',"errors = %s",$reactivateSubscriberResult->get_errors());

        $resultCode = ( isset($reactivateSubscriberResult->data_array['ResultCode']) ) ? $reactivateSubscriberResult->data_array['ResultCode'] : NULL ;

        if ( $reactivateSubscriberResult->data_array['fatal'] )
          return $this->fail( implode( ",", $reactivateSubscriberResult->get_errors() ) , 'ReactivateSubscriber' , $resultCode );
        else
          return $this->delay( 'Biphasic command unavailable' , 'ReactivateSubscriber' , $resultCode );
      }

      // ReactivateSubscriber success
      return $this->pending( 'Invoked ReactivateSubscriber' );
    }

    // API-156: Save current balance. Invoke CheckBalance if values are not cached.
    $this->logBalance($accMiddleware, $statusData);

    // if 'Active', we can check the SOCs and renew the plan
    if ( $statusData->SubscriberStatus == 'Active' )
    {
      // DATAQ-22, DATAQ-29: ensure subscriber is on the target wholesale plan
      list($proceed, $result) = $this->ensureCorrectWholesalePlan($accMiddleware, $statusData);
      if ( ! $proceed)
        return $result;

      $addOnFeatureInfo = ( isset($statusData->AddOnFeatureInfoList) )
        ? $statusData->AddOnFeatureInfoList->AddOnFeatureInfo
        : array(); // STANDBY

      // if TRUE, the SOCS are correctly set up for the plan we have to renew
      $planCheck  = \Ultra\Lib\MVNE\MakeItSo\planCheck(
        $addOnFeatureInfo,
        $this->customer_id,
        $this->makeitso_options->ultra_plan,
        isset($this->makeitso_options->subplan) ? $this->makeitso_options->subplan : null 
      );

      // check if AddOnData SOC is set - we must remove it if present
      $addOnDataSocId = $this->detectAddOnDataSocId( $addOnFeatureInfo );

      // we must remove AddOnData SOC
      if ( $addOnDataSocId )
        $planCheck = FALSE;

      if ( $planCheck )
      {
        // SOCs are correct, we can invoke RenewPlan
        $preferred_language = $this->detectLanguageSocId( $addOnFeatureInfo );

        // detect throttle speed
        $throttling     = new \Throttling($this->makeitso_options->ultra_plan);
        $throttle_speed = $throttling->detectThrottleSpeedFromAddOnFeatureInfo($addOnFeatureInfo);

        return $this->invokeRenewPlan( $accMiddleware , $preferred_language, $throttle_speed );
      }
      else
      {
        // SOCs are not correct, we must fix with UpdatePlanAndFeatures
        return $this->invokeUpdatePlanAndFeatures(
          $accMiddleware,
          'CHANGE|'.$this->makeitso_options->ultra_plan,
          FALSE,
          FALSE
        );
      }
    }
    else
      return $this->fail("SubscriberStatus {$statusData->SubscriberStatus} not handled" , 'QuerySubscriber' );
  }

  /**
   * detectAddOnDataSocId
   */
  public function detectAddOnDataSocId( $addOnFeatureInfo )
  {
    $addOnDataSocId = NULL;

    if ( $addOnFeatureInfo && is_array($addOnFeatureInfo) )
      foreach( $addOnFeatureInfo as $addOnFeature )
        if ( in_array($addOnFeature->FeatureID, [12907,12918,12920]) )
          $addOnDataSocId = $addOnFeature->FeatureID;

    dlog('',"addOnDataSocId = $addOnDataSocId");

    return $addOnDataSocId;
  }

  /**
   * detectLanguageSocId
   */
  public function detectLanguageSocId( $addOnFeatureInfo )
  {
    $preferred_language = 'en'; // just to give a default value to this variable

    if ( $addOnFeatureInfo && is_array($addOnFeatureInfo) )
      foreach( $addOnFeatureInfo as $addOnFeature )
        if ( $addOnFeature->FeatureID == 12803 )
          $preferred_language = 'es';

    return $preferred_language;
  }

  /**
   * invokeRestoreSubscriber
   */
  private function invokeRestoreSubscriber($accMiddleware)
  {
    $restoreSubscriberResult = $accMiddleware->processControlCommand([
      'command'    => 'RestoreSubscriber', // this is the ACC Command
      'actionUUID' => $this->action_uuid,
      'parameters' => [
        'customer_id'  => $this->customer_id,
        'iccid'        => $this->makeitso_options->iccid,
        'msisdn'       => $this->makeitso_options->msisdn,
        'ultra_plan'   => $this->makeitso_options->ultra_plan
      ]
    ]);

    dlog('',"restoreSubscriberResult = %s",$restoreSubscriberResult);

    // RestoreSubscriber timeout
    if ( $restoreSubscriberResult->is_timeout() )
      return $this->delay( 'RestoreSubscriber timeout' , 'RestoreSubscriber' , NULL );

    // RestoreSubscriber error
    if ( $restoreSubscriberResult->has_errors() )
    {
      dlog('',"errors = %s",$restoreSubscriberResult->get_errors());

      $resultCode = ( isset($restoreSubscriberResult->data_array['ResultCode']) ) ? $restoreSubscriberResult->data_array['ResultCode'] : NULL ;

      if ( $restoreSubscriberResult->data_array['fatal'] )
        return $this->fail( implode( ",", $restoreSubscriberResult->get_errors() ) , 'RestoreSubscriber' , $resultCode );
      else
        return $this->delay( 'Biphasic command unavailable' , 'RestoreSubscriber' , $resultCode );
    }

    // RestoreSubscriber success
    return $this->pending( 'Invoked RestoreSubscriber' );
  }

  /**
   * invokeRenewPlan
   */
  private function invokeRenewPlan( $accMiddleware , $preferred_language, $throttle_speed, $isFinalCall = TRUE )
  {
    $roamCreditPromoParam = \Ultra\Lib\RoamCreditPromo::getParam();

    $renewPlanResult = $accMiddleware->processControlCommand(
      array(
        'command'    => 'RenewPlan', // this is the ACC Command
        'actionUUID' => $this->action_uuid,
        'parameters' => array(
          'customer_id'        => $this->customer_id,
          'iccid'              => $this->makeitso_options->iccid,
          'msisdn'             => $this->makeitso_options->msisdn,
          'ultra_plan'         => $this->makeitso_options->ultra_plan,
          'preferred_language' => $preferred_language, // coming from QuerySubscriber
          'throttle_speed'     => $throttle_speed, // coming from QuerySubscriber
          'wholesale_plan'     => $this->makeitso_options->wholesale_plan,
          'subplan'            => empty($this->makeitso_options->subplan) ? NULL : $this->makeitso_options->subplan,
          // RoamCreditPromo
          $roamCreditPromoParam => isset($this->makeitso_options->{$roamCreditPromoParam}) 
            ? $this->makeitso_options->{$roamCreditPromoParam}
            : NULL
        )
      )
    );

    dlog('',"renewPlanResult = %s",$renewPlanResult);
    $renewPlanMessage = empty($renewPlanResult->data_array['message']) ? NULL : json_decode($renewPlanResult->data_array['message']);

    // RenewPlan timeout
    if ($renewPlanResult->is_timeout())
      return $this->delay( 'RenewPlan timeout' , 'RenewPlan' , NULL );

    // RenewPlan failure

    if ($renewPlanResult->has_errors() || ($renewPlanMessage && ! empty($renewPlanMessage->body) && ! $renewPlanMessage->body->success))
    {
      dlog('',"errors = %s %s", $renewPlanResult->get_errors(), empty($renewPlanMessage->body->errors) ? NULL : $renewPlanMessage->body->errors[0]);

      $resultCode = ( isset($renewPlanResult->data_array['ResultCode']) ) ? $renewPlanResult->data_array['ResultCode'] : NULL ;

      if ( isset($renewPlanResult->data_array['fatal']) && $renewPlanResult->data_array['fatal'] )
        return $this->fail( implode( ",", $renewPlanResult->get_errors() ) , 'RenewPlan' , $resultCode );
      else
        return $this->delay( 'Biphasic command unavailable' , 'RenewPlan' , $resultCode );
    }

    // RenewPlan succeeded, this is the last step for MAKEITSO_TYPE = RenewPlan
    if ($isFinalCall)
      return $this->succeed( 'Invoked RenewPlan' );
    else
      return $this->pending( 'Invoked RenewPlan' );
  }

  /**
   * invokeUpdatePlanAndFeatures
   *
   * Invoke UpdatePlanAndFeatures
   * Terminates the Makeitso with success if $isFinalCall, delay otherwise (more actions will be needed later).
   */
  private function invokeUpdatePlanAndFeatures( $accMiddleware , $option , $isFinalCall=FALSE , $keepDataSocs=TRUE )
  {
    $roamCreditPromoParam = \Ultra\Lib\RoamCreditPromo::getParam();

    // invoke UpdatePlanAndFeatures
    $updatePlanAndFeaturesResult = $accMiddleware->processControlCommand(
      array(
        'command'         => 'UpdatePlanAndFeatures', // this is the ACC Command
        'actionUUID'      => $this->action_uuid,
        'makeitsoQueueId' => $this->makeitso_queue_id,
        'parameters' => array(
          'customer_id'         => $this->customer_id,
          'iccid'               => $this->makeitso_options->iccid,
          'msisdn'              => $this->makeitso_options->msisdn,
          'ultra_plan'          => $this->makeitso_options->ultra_plan,
          'preferred_language'  => $this->makeitso_options->preferred_language,
          'wholesale_plan'      => $this->makeitso_options->wholesale_plan,
          'keepDataSocs'        => $keepDataSocs,
          'option'              => $option,
          'subplan'             => empty($this->makeitso_options->subplan) ? NULL : $this->makeitso_options->subplan,
          'migratingFromMrc'    => empty($this->makeitso_options->migratingFromMrc) ? NULL : $this->makeitso_options->migratingFromMrc,

          // RoamCreditPromo
          $roamCreditPromoParam => isset($this->makeitso_options->{$roamCreditPromoParam}) 
            ? $this->makeitso_options->{$roamCreditPromoParam}
            : NULL
        )
      )
    );

    dlog('',"updatePlanAndFeaturesResult = %s",$updatePlanAndFeaturesResult);

    // UpdatePlanAndFeatures timeout

    if ( $updatePlanAndFeaturesResult->is_timeout() )
      return $this->delay( 'UpdatePlanAndFeatures timeout' , 'UpdatePlanAndFeatures' , NULL );

    // UpdatePlanAndFeatures error

    if ( $updatePlanAndFeaturesResult->has_errors() )
    {
      dlog('',"errors = %s",$updatePlanAndFeaturesResult->get_errors());

      $resultCode = ( isset($updatePlanAndFeaturesResult->data_array['ResultCode']) ) ? $updatePlanAndFeaturesResult->data_array['ResultCode'] : NULL ;

      if ( isset($updatePlanAndFeaturesResult->data_array['fatal']) && $updatePlanAndFeaturesResult->data_array['fatal'] )
        return $this->fail( implode( ",", $updatePlanAndFeaturesResult->get_errors() ) , 'UpdatePlanAndFeatures' , $resultCode );
      else
        return $this->delay( 'Biphasic command unavailable' , 'UpdatePlanAndFeatures' , $resultCode );
    }

    // UpdatePlanAndFeatures succeeded

    if ( $isFinalCall )
      return $this->succeed();
    else
      return $this->pending( 'Invoked UpdatePlanAndFeatures' );
  }

  /**
   * processReactivate
   *
   * Process a Reactivate item
   *
   * @return Result object
   */
  private function processReactivate()
  {
    // check parameters
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateReactivateParams( $this->makeitso_options );
    if (count($errors))
      return $this->fail(implode(',', $errors));

    // check command invocations
    if ( ! $this->checkCommandInvocationsAvailable())
      return $this->delay('Command Invocation pending');

    // query subscriber
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;
    if ( ! $statusData = $this->querySubscriber($accMiddleware))
      return \make_ok_Result();

    // reactivate if necessary
    list($proceed, $result) = $this->ensureSubscriberActive($accMiddleware, $statusData);
    if ( ! $proceed)
      return $result;

    // update wholesale plan
    list($proceed, $result) = $this->ensureCorrectWholesalePlan($accMiddleware, $statusData);
    if ( ! $proceed)
      return $result;

    return $this->succeed();
  }

  /**
   * processDeactivate
   *
   * Process a Deactivate item
   *
   * @return Result object
   */
  private function processDeactivate()
  {
    $errors = \Ultra\Lib\MVNE\MakeItSo\validateDeactivateParams( $this->makeitso_options );

    if ( count($errors) )
      return $this->fail( implode( ",", $errors ) );

    // Check subscriber if Command Invocations has blocked this subscriber. If so, delay (Reason="Command Invocation pending").

    $commandInvocation = $this->getCommandInvocationObject();

    if ( $commandInvocation->isOccupied() )
      return $this->delay( 'Command Invocation pending' );

    // Perform a QuerySubscriber

    // important: this is the MW logic layer, not the adapter layer
    $accMiddleware = new \Ultra\Lib\MiddleWare\ACC\Control;

    $querySubscriberResult = $accMiddleware->processControlCommand(
      array(
        'command'    => 'QuerySubscriber', // this is the ACC Command
        'actionUUID' => $this->action_uuid,
        'parameters' => array(
          'iccid'       => $this->makeitso_options->iccid,
          'msisdn'      => $this->makeitso_options->msisdn
        )
      )
    );

    // QuerySubscriber timeout

    if ( $querySubscriberResult->is_timeout() )
      return $this->delay( 'QuerySubscriber timeout' , 'QuerySubscriber' , NULL );

    // QuerySubscriber failure

    if ( $querySubscriberResult->has_errors() )
    {
      dlog('',"errors = %s",$querySubscriberResult->get_errors());

      $resultCode = ( isset($querySubscriberResult->data_array['ResultCode']) ) ? $querySubscriberResult->data_array['ResultCode'] : NULL ;

      if ( !isset($querySubscriberResult->data_array['fatal']) || $querySubscriberResult->data_array['fatal'] )
        return $this->fail( implode( ",", $querySubscriberResult->get_errors() ) , 'QuerySubscriber' , $resultCode );
      else
        return $this->delay( 'Cannot perform QuerySubscriber' , 'QuerySubscriber' , $resultCode );
    }

    // QuerySubscriber success

    $dataBody = $accMiddleware->controlObject->extractDataBodyFromMessage( $querySubscriberResult->data_array['message'] );

    dlog('',"resultQuerySubscriber dataBody = %s",$dataBody);

    if ( !isset($dataBody['SubscriberStatus']) || !$dataBody['SubscriberStatus'] )
      return $this->fail( 'SubscriberStatus '.$dataBody['SubscriberStatus'].' not available from QuerySubscriber response' , 'QuerySubscriber' );

    dlog('','SubscriberStatus = '.$dataBody['SubscriberStatus']);

/*
      // if 'Suspended', we have to restore the account
      if ( $dataBody['SubscriberStatus'] == 'Suspended' )
      {
        $restoreSubscriberResult = $accMiddleware->processControlCommand(
          array(
            'command'    => 'RestoreSubscriber', // this is the ACC Command
            'actionUUID' => $this->action_uuid,
            'parameters' => array(
              'customer_id'        => $this->customer_id,
              'iccid'              => $this->makeitso_options->iccid,
              'msisdn'             => $this->makeitso_options->msisdn
            )
          )
        );

        dlog('',"restoreSubscriberResult = %s",$restoreSubscriberResult);

        if ( $restoreSubscriberResult->has_errors() )
        {
          dlog('',"errors = %s",$restoreSubscriberResult->get_errors());

          $resultCode = ( isset($restoreSubscriberResult->data_array['ResultCode']) ) ? $restoreSubscriberResult->data_array['ResultCode'] : NULL ;

          if ( !isset($restoreSubscriberResult->data_array['fatal']) || $restoreSubscriberResult->data_array['fatal'] )
            return $this->fail( implode( ",", $restoreSubscriberResult->get_errors() ) , 'RestoreSubscriber' , $resultCode );
          else
            return $this->delay( 'Biphasic command unavailable' , 'RestoreSubscriber' , $resultCode );
        }

        // RestoreSubscriber succeeded
        return $this->pending( 'Invoked RestoreSubscriber' );
      }
*/

    // if 'Active' or 'Suspended', we must deactivate the account
    if ( ( $dataBody['SubscriberStatus'] == 'Active' ) || ( $dataBody['SubscriberStatus'] == 'Suspended' ) )
    {
      $deactivateSubscriberResult = $accMiddleware->processControlCommand(
        array(
          'command'    => 'DeactivateSubscriber', // this is the ACC Command
          'actionUUID' => $this->action_uuid,
          'parameters' => array(
            'customer_id'        => $this->customer_id,
            'iccid'              => $this->makeitso_options->iccid,
            'msisdn'             => $this->makeitso_options->msisdn
          )
        )
      );

      dlog('',"deactivateSubscriberResult = %s",$deactivateSubscriberResult);

      // DeactivateSubscriber timeout

      if ( $deactivateSubscriberResult->is_timeout() )
        return $this->delay( 'DeactivateSubscriber timeout' , 'DeactivateSubscriber' , NULL );

      // DeactivateSubscriber error

      if ( $deactivateSubscriberResult->has_errors() )
      {
        dlog('',"errors = %s",$deactivateSubscriberResult->get_errors());

        $resultCode = ( isset($deactivateSubscriberResult->data_array['ResultCode']) ) ? $deactivateSubscriberResult->data_array['ResultCode'] : NULL ;

        if ( $deactivateSubscriberResult->data_array['fatal'] )
          return $this->fail( implode( ",", $deactivateSubscriberResult->get_errors() ) , 'DeactivateSubscriber' , $resultCode );
        else
          return $this->delay( 'Biphasic command unavailable' , 'DeactivateSubscriber' , $resultCode );
      }

      // DeactivateSubscriber success

      return $this->pending( 'Invoked DeactivateSubscriber' );
    }

    // if 'Inactive', we are done
    if ( $dataBody['SubscriberStatus'] == 'Inactive' )
      return $this->succeed();

    return $this->fail( 'SubscriberStatus '.$dataBody['SubscriberStatus'].' not handled' , 'QuerySubscriber' );
  }

  /**
   * retry
   *
   * Sometimes it's necessary to resurrect a MakeItSo task and try it again
   *
   * @return Result object
   */
  public function retry( $makeitso_queue_id )
  {
    if ( ! $makeitso_queue_id )
      return \make_error_Result( "makeitso_queue_id missing or invalid" );

    $this->dbConnect();

    // get data from ULTRA_ACC..MAKEITSO_QUEUE
    $data = \get_makeitso(
      array(
        'makeitso_queue_id' => $makeitso_queue_id
      )
    );

    if ( ! $data || ! is_array($data) || ! count($data) )
      return \make_error_Result( "no data found" );

    dlog('',"data = %s",$data);

    if ( $data[0]->STATUS != "FAILED" )
      return \make_error_Result( "the element is not in status FAILED" );

    // update status
    $success = \update_makeitso(
      array(
        'makeitso_queue_id'      => $makeitso_queue_id,
        'created_date_time'      => 'NOW', // to avoid timeout
        'status_not_withdrawn'   => TRUE,
        'status'                 => 'PENDING',
        'completed_date_time'    => 'NULL',
        'next_attempt_date_time' => 'GETUTCDATE()',
        'attempt_history'        => 'PENDING|' . date_from_pst_to_utc(date("Y-m-d h:i:s")),
        'attempt_count'          => 0,
        'last_error_code'        => 'NULL',
        'last_acc_api'           => 'NULL'
      )
    );

    if ( $success )
    {
      // enqueue $makeitso_queue_id
      $this->redis_makeitso->add( time() , $makeitso_queue_id );

      return \make_ok_Result();
    }
    else
      return \make_error_Result( "We could not update MAKEITSO_QUEUE" );
  }

  /**
   * redo
   *
   * Update MISO with changed parameters and re-try it
   * @see PROD-320
   * @param int MAKEITSO_QUEUE.MAKEITSO_QUEUE_ID
   * @return Result object
   */
  public function redo( $makeitso_queue_id )
  {
    if ( ! $makeitso_queue_id )
      return \make_error_Result( 'makeitso_queue_id missing or invalid' );

    $this->dbConnect();

    // get data from ULTRA_ACC..MAKEITSO_QUEUE
    $data = \get_makeitso(
      array(
        'makeitso_queue_id' => $makeitso_queue_id
      )
    );

    // and check result
    if ( ! $data || ! is_array($data) || ! count($data) )
      return \make_error_Result( 'no data found' );
    dlog('', 'data = %s', $data[0]);

    // load old MISO options
    $options = json_decode( $data[0]->MAKEITSO_OPTIONS );
    if (! ($options && is_object($options) && count($options)))
      return \make_error_Result("MISO $makeitso_queue_id has no options");

    // load customer data
    teldata_change_db();

    $customer = get_ultra_customer_from_customer_id($data[0]->CUSTOMER_ID, array(
      'current_mobile_number',
      'CURRENT_ICCID_FULL',
      'plan_state',
      'preferred_language'
    ));

    $account = get_account_from_customer_id($data[0]->CUSTOMER_ID, array('COS_ID'));

    // fail if customer has not been found or cancelled
    if ( ! $customer || ! $account || $customer->plan_state == 'Cancelled')
      return \make_error_Result("invalid or cancelled customer {$data[0]->CUSTOMER_ID}");

    // update each option
    $changed = FALSE; // flag: some option changed
    foreach($options as $name => $old)
    {
      switch ($name)
      {
        case 'msisdn':
          $new = trim($customer->current_mobile_number);
          break;
        case 'iccid':
          $new = trim($customer->CURRENT_ICCID_FULL);
          break;
        case 'wholesale_plan':
          $new = \Ultra\Lib\DB\Customer\getWholesalePlan($data[0]->CUSTOMER_ID);
          break;
        case 'ultra_plan':
          $new = get_plan_from_cos_id($account->COS_ID);
          break;
        case 'preferred_language':
          $new = $customer->preferred_language;
          break;
        default: // ignore unknown options
          $new = NULL;
      }

      // update if the option has changed and is valid
      if (! empty($new) && $old != $new)
      {
        $changed = TRUE;
        $options->$name = $new;
        dlog('', "updated option $name: '$old'->'$new'");
      }
    }

    if (! $changed)
      return \make_error_Result('no MISO options to change');

    // update MISO
    \Ultra\Lib\DB\ultra_acc_connect();
    $success = update_makeitso(
      array(
        'makeitso_queue_id' => $makeitso_queue_id,
        'makeitso_options'  => json_encode($options),
        'attempt_history'   => 'PENDING|' . date_from_pst_to_utc(date("Y-m-d h:i:s")),
        'attempt_count'     => 0));

    if (! $success)
      return \make_error_Result('failed to update MISO options');

    // retry updated MISO
    return $this->retry( $makeitso_queue_id );
  }

  /**
   * fail
   *
   * Close queue element with a fatal error
   *
   * @return Result object
   */
  private function fail( $error , $acc_api=NULL , $last_error_code=NULL )
  {
    dlog('', '(%s)', func_get_args());

    $this->dbConnect();

    $success = \update_makeitso_fatal_error(
      array(
        'makeitso_queue_id' => $this->makeitso_queue_id,
        'attempt_count'     => ( $this->attempt_count + 1 ),
        'error'             => $error,
        'last_error_code'   => $last_error_code,
        'last_acc_api'      => $acc_api
      )
    );

    // release $this->makeitso_queue_id
    $this->redis_makeitso->free( $this->makeitso_queue_id );

    return ( $success )
           ?
           \make_ok_Result( array() , $error , 'makeitso_fatal_error' )
           :
           \make_error_Result( "ERR_API_INTERNAL: an error occurred while updating the MakeItSo queue" )
           ;
  }

  /**
   * processNext
   *
   * Gets the next MakeItSo item to be processed, process it if found.
   *
   * @return Result object
   */
  public function processNext ( $makeitso_queue_id=NULL )
  {
    $success = $this->getNext ( $makeitso_queue_id );

    return ( $success )
           ?
           $this->process()
           :
           \make_ok_Result()
           ;
  }

  /**
   * getNext
   *
   * Gets the next MakeItSo item to be processed ( status = 'PENDING' ), but does not process it yet.
   *
   * @return boolean
   */
  public function getNext ( $makeitso_queue_id=NULL )
  {
    $data = NULL;

    if ( !$makeitso_queue_id && \Ultra\Lib\MVNE\MakeItSo\delayDueToMWQueueSize( $this->redis ) )
      return FALSE;

    // if we want to proceed immediately, $makeitso_queue_id is not in Redis queue
    if (!( $this->instantly && $makeitso_queue_id ))
      // extract next item from Redis priority queue
      $makeitso_queue_id = $this->redis_makeitso->popNext( $makeitso_queue_id );

    // there are no items to be processed at this time
    if ( !$makeitso_queue_id )
      return FALSE;

    $this->dbConnect();

    // get data from ULTRA_ACC..MAKEITSO_QUEUE
    $data = \get_makeitso(
      array(
        'makeitso_queue_id' => $makeitso_queue_id
      )
    );

    dlog('',"data = %s",$data);

    // load data as object members
    if ( $data && is_array($data) && count($data) )
    {
      // may have been withdrawn or overridden
      if ( $data[0]->STATUS != 'PENDING' )
      {
        dlog('',"Nothing to do for makeitso_queue_id $makeitso_queue_id : status is ".$data[0]->STATUS);
        return FALSE;
      }

      $this->status            = 'PENDING';
      $this->created_days_ago  = $data[0]->CREATED_DAYS_AGO;
      $this->makeitso_queue_id = $data[0]->MAKEITSO_QUEUE_ID;
      $this->action_uuid       = $data[0]->ACTION_UUID;
      $this->customer_id       = $data[0]->CUSTOMER_ID;
      $this->attempt_count     = $data[0]->ATTEMPT_COUNT;
      $this->attempt_history   = $data[0]->ATTEMPT_HISTORY;
      $this->makeitso_type     = $data[0]->MAKEITSO_TYPE;
      $this->makeitso_options  = $data[0]->MAKEITSO_OPTIONS;
      if ( $this->makeitso_options )
        $this->makeitso_options = json_decode($this->makeitso_options);
      $this->last_acc_api      = $data[0]->LAST_ACC_API;
      $this->last_error_code   = $data[0]->LAST_ERROR_CODE;

      dlog('',"%s",$this);

      return TRUE;
    }

    return FALSE;
  }

  /**
   * succeed
   *
   * Updates MAKEITSO_QUEUE due to completion
   *
   * @return Result object
   */
  public function succeed ($command = NULL)
  {
    $this->dbConnect();

    $success = \update_makeitso_success(
      array(
        'makeitso_queue_id'    => $this->makeitso_queue_id,
        'status_not_withdrawn' => TRUE, // do not update row if it has been withdrawn
        'attempt_count'        => ( $this->attempt_count + 1 ),
        'last_acc_api'         => $command
      )
    );

    // release $this->makeitso_queue_id
    $this->redis_makeitso->free( $this->makeitso_queue_id );

    return ( $success )
           ?
           \make_ok_Result()
           :
           \make_error_Result( "ERR_API_INTERNAL: an error occurred while updating the MakeItSo queue" )
           ;
  }

  /**
   * pending
   *
   * Updates MAKEITSO_QUEUE due to an additional attempt
   *
   * @return Result object
   */
  public function pending( $reason , $command=NULL )
  {
    $this->dbConnect();

    $defer_seconds = \Ultra\Lib\MVNE\MakeItSo\compute_defer_seconds( $this->attempt_history );

    $success = \update_makeitso_delay(
      array(
        'makeitso_queue_id' => $this->makeitso_queue_id,
        'attempt_count'     => ( $this->attempt_count + 1 ),
        'defer_seconds'     => $defer_seconds,
        'reason'            => $reason,
        'last_error_code'   => 'NULL',
        'last_acc_api'      => $command
      )
    );

    if ( !$success )
      return\make_error_Result( "ERR_API_INTERNAL: an error occurred while updating the MakeItSo queue" );

    // release $this->makeitso_queue_id
    $this->redis_makeitso->free( $this->makeitso_queue_id );

    // enqueue $this->makeitso_queue_id with defer_seconds delay
    $this->redis_makeitso->add( time() + $defer_seconds , $this->makeitso_queue_id );

    return \make_ok_Result( array() , $reason );
  }

  /**
   * delay
   *
   * Updates MAKEITSO_QUEUE due to a necessary delay
   *
   * @return Result object
   */
  public function delay ( $reason , $acc_api=NULL , $last_error_code=NULL )
  {
    $this->dbConnect();

    if ( $this->isTimeout() )
      return $this->timeout();

    // the next attempt after this one will be delayed after $defer_seconds seconds
    $defer_seconds = \Ultra\Lib\MVNE\MakeItSo\compute_defer_seconds( $this->attempt_history );

    // update ULTRA_ACC..MAKEITSO_QUEUE
    $success = \update_makeitso_delay(
      array(
        'makeitso_queue_id' => $this->makeitso_queue_id,
        'attempt_count'     => ( $this->attempt_count + 1 ),
        'defer_seconds'     => $defer_seconds,
        'reason'            => $reason,
        'last_error_code'   => $last_error_code,
        'last_acc_api'      => $acc_api
      )
    );

    if ( !$success )
      return\make_error_Result( "ERR_API_INTERNAL: an error occurred while updating the MakeItSo queue" );

    // release $this->makeitso_queue_id
    $this->redis_makeitso->free( $this->makeitso_queue_id );

    // enqueue $this->makeitso_queue_id with defer_seconds delay
    $this->redis_makeitso->add( time() + $defer_seconds , $this->makeitso_queue_id );

    return \make_ok_Result( array() , $reason );
  }

  /**
   * timeout
   *
   * Terminates with timeout
   *
   * @return Result object
   */
  public function timeout ()
  {
    return $this->fail( 'Timeout' );
  }

  /**
   * isTimeout
   *
   * @return boolean
   */
  private function isTimeout ()
  {
    // max 60 attempts
    if ( $this->attempt_count > 60 )
    {
      dlog('','Timeout: max attempt count reached : '.$this->attempt_count);
      return TRUE;
    }

    // not more than 4 days old
    if ( $this->created_days_ago > 4 )
    {
      dlog('','Timeout after '.$this->created_days_ago.' days');
      return TRUE;
    }

    list ( $parsedHistory , $latest_sequence_episode , $latest_sequence_length ) = \Ultra\Lib\MVNE\MakeItSo\parseHistory( $this->attempt_history );

    // The past 20 delays were "Async command unavailable"
    if ( ( $latest_sequence_episode == 'BiphasicUnavailable' ) && ( $latest_sequence_length >= 20 ) )
    {
      dlog('','Timeout after '.$latest_sequence_length.' failed Async commands');
      return TRUE;
    }

    // The past 20 delays were "Cannot Query Details"
    if ( ( $latest_sequence_episode == 'QuerySubscriberError' ) && ( $latest_sequence_length >= 20 ) )
    {
      dlog('','Timeout after '.$latest_sequence_length.' failed QuerySubscriber attempts');
      return TRUE;
    }

    // max 50 delays form pending Biphasic Commands
    if ( $parsedHistory['BiphasicPending'] >= 50 )
    {
      dlog('','Timeout after '.$parsedHistory['BiphasicPending'].' delays due to pending commands');
      return TRUE;
    }

    // max 5 Biphasic Commands invoked
    if ( $parsedHistory['BiphasicInvoked'] >= 5 )
    {
      dlog('','Timeout after '.$parsedHistory['BiphasicInvoked'].' commands invoked');
      return TRUE;
    }

    return FALSE;
  }

  /**
   * checkCommandInvocationsAvailable
   *
   * check that subscriber does not have any pending commands
   * @return Boolean TRUE if available (MISO can proceed) or FALSE otherwise
   */
  private function checkCommandInvocationsAvailable()
  {
    $commandInvocation = $this->getCommandInvocationObject();
    if ($commandInvocation->isOccupied())
      return FALSE;
    else
      return TRUE;
  }

  /**
   * ensureCorrectWholesalePlan
   *
   * check if subscriber is on the desired wholesale plan (as specified by MISO) and switch if necessary
   * delay this MISO if network update takes place
   * @see DATAQ-29, DATAQ-22
   * @returns Array (Boolean TRUE if the wholesale plan is current and MISO can proceed, FALSE otherwise; Object Result of action taken or NULL)
   */
  private function ensureCorrectWholesalePlan($accControl, $querySubscriber)
  {
    $querySubscriber = (object)$querySubscriber;
    if (empty($querySubscriber->wholesalePlan) || empty($this->makeitso_options->wholesale_plan) || empty($this->makeitso_options->msisdn))
    {
      dlog('', 'ERROR: invalid arguments %s', func_get_args());
      return array(FALSE, $this->fail('ERR_API_INTERNAL: invalid arguments'));
    }

    // determine current and target wholesale plans
    $currentPlanId = $querySubscriber->wholesalePlan;
    $targetPlan = \Ultra\UltraConfig\getAccSocByName($this->makeitso_options->wholesale_plan);
    $targetPlanId = $targetPlan['plan_id'];
    dlog('' ,'current wholesale plan ID: %d, target: %d', $currentPlanId, $targetPlanId);

    // nothing to do if plan is current
    if ($currentPlanId == $targetPlanId)
      return array(TRUE, \make_Ok_result());

    // change wholesale plan
    $command = 'UpdateWholesalePlan';
    $result = $accControl->processControlCommand(
      array(
        'command'           => $command,
        'actionUUID'        => $this->action_uuid,
        'parameters'        => array(
          'msisdn'          => $this->makeitso_options->msisdn,
          'wholesale_plan'  => $targetPlan['ultra_service_name']
        )
      )
    );

    // handle timeout
    if ($result->is_timeout())
      return array(FALSE, $this->delay("$command timeout", $command, NULL));

    // handle errors                              
    if ($result->has_errors())
    {
      $resultCode = empty($result->data_array['ResultCode']) ? NULL : $result->data_array['ResultCode'];
      if ( ! empty($result->data_array['fatal']))
        return array(FALSE, $this->fail(implode(',', $result->get_errors()), $command, $resultCode));
      else
        return array(FALSE, $this->delay("Cannot perform $command", $command, $resultCode));
    }

    // successfully updated on the network, also update in DB
    teldata_change_db();
    \Ultra\Lib\DB\Customer\setWholesalePlan($this->customer_id, $targetPlan['ultra_service_name']);
    \Ultra\Lib\DB\ultra_acc_connect();

    // delay MISO in order to prevent sequential network calls
    return array(FALSE, $this->pending("Invoked $command"));
  }

  /**
   * ensureSubscriberActive
   *
   * re-activate a subscriber if inactive
   * @param Object ACC Control instance
   * @param Array SubsriberQuery result
   * @retuns Array (Boolean TRUE if MISO can proceed, FALSE otherwise; Object Result)
   */
  private function ensureSubscriberActive($accControl, $statusData)
  {
    // check parameters
    if (empty($accControl) || empty($statusData) || empty($this->makeitso_options->msisdn))
    {
      dlog('', 'ERROR: invalid arguments %s', func_get_args());
      return array(FALSE, $this->fail('ERR_API_INTERNAL: invalid arguments'));
    }

    // confirm subscriber status is present
    if (empty($statusData->SubscriberStatus))
      return array(FALSE, $this->fail("SubscriberStatus not available from QuerySubscriber response", 'QuerySubscriber'));
    dlog('', "SubscriberStatus = {$statusData->SubscriberStatus}");

    // nothing to do if subscriber is already active or in some other status
    if ($statusData->SubscriberStatus == 'Active' || $statusData->SubscriberStatus != 'Inactive')
      return array(TRUE, \make_Ok_result());

    // if 'Inactive' then reactivate
    $command = 'ReactivateSubscriber';
    $result = $accControl->processControlCommand(array(
      'command'         => $command,
      'actionUUID'      => $this->action_uuid,
      'makeitsoQueueId' => $this->makeitso_queue_id,
      'parameters' => array(
        'customer_id'        => $this->customer_id,
        'iccid'              => $this->makeitso_options->iccid,
        'msisdn'             => $this->makeitso_options->msisdn,
        'ultra_plan'         => $this->makeitso_options->ultra_plan,
        'preferred_language' => $this->makeitso_options->preferred_language,
        'wholesale_plan'     => $this->makeitso_options->wholesale_plan)));

    // handle timeout
    if ($result->is_timeout())
      return array(FALSE, $this->delay("$command timeout", $command, NULL));

    // handle errors
    if ($result->has_errors())
    {
      $resultCode = empty($result->data_array['ResultCode']) ? NULL : $result->data_array['ResultCode'];
      if ($result->data_array['fatal'])
        return array(FALSE, $this->fail(implode(',', $result->get_errors()), $command, $resultCode));
      else
        return array(FALSE, $this->delay("Cannot perform $command", $command, $resultCode));
    }

    // success: delay MISO in order to prevent sequential network calls after reactivation
    return array(TRUE, $this->pending("Invoked $command"));
  }

  /**
   * querySubscriber
   *
   * execute MVNE QuerySubscriber, handle errors and return the result, will delay or fail MISO on error
   * @retuns Object query data on success or NULL on failure
   */
  private function querySubscriber($accControl, $cached = FALSE)
  {
    // retrieve from cache if allowed
    $key = self::QUERY_SUBSCRIBER_KEY . $this->customer_id;
    if ($cached)
      if ($status = $this->redis->get($key))
        return json_decode($status);

    // check parameters
    if (empty($accControl) || (empty($this->makeitso_options->iccid) && empty($this->makeitso_options->msisdn)))
    {
      dlog('', 'ERROR: invalid arguments %s', func_get_args());
      $this->fail('ERR_API_INTERNAL: invalid arguments');
      return NULL;
    }

    // prepare parameters
    $parameters = array();
    if ( ! empty($this->makeitso_options->iccid))
      $parameters['iccid'] = $this->makeitso_options->iccid;
    if ( ! empty($this->makeitso_options->msisdn))
      $parameters['msisdn'] = $this->makeitso_options->msisdn;

    // call ACC API QuerySubscriber
    $command = 'QuerySubscriber';
    $result = $accControl->processControlCommand(array(
      'command'    => $command,
      'actionUUID' => $this->action_uuid,
      'parameters' => $parameters));

    // handle timeout
    if ($result->is_timeout())
    {
      $this->delay("$command timeout" , $command);
      return NULL;
    }

    // handle errors
    if ($result->has_errors())
    {
      dlog('', 'errors = %s', $result->get_errors());
      $resultCode = isset($result->data_array['ResultCode']) ? $result->data_array['ResultCode'] : NULL;
      if ( ! empty($result->data_array['fatal']))
        $this->fail(implode(',', $result->get_errors()), $command, $resultCode);
      else
        $this->delay("Cannot perform $command" , $command, $resultCode);
      return NULL;
    }

    // extract and cache result
    $status = $accControl->controlObject->extractDataBodyFromMessage($result->data_array['message']);
    $this->redis->set($key, json_encode($status), self::QUERY_SUBSCRIBER_TTL);

    // objectify data for consistency with json_decode (it cannot preserve mixed dataypes: either objectify all associative arrays (default) or return all structures as arrays)
    $status = (object)$status;
    if ( ! empty($status->AddOnFeatureInfoList))
    {
      $status->AddOnFeatureInfoList = (object)$status->AddOnFeatureInfoList;
      if ( ! empty($status->AddOnFeatureInfoList->AddOnFeatureInfo))
      {
        $status->AddOnFeatureInfoList->AddOnFeatureInfo = (array)$status->AddOnFeatureInfoList->AddOnFeatureInfo;
        # foreach ($status->AddOnFeatureInfoList->AddOnFeatureInfo as &$feature)
        #   $reature = (object)$feature;
      }
    }

    return $status;
  }

  /**
   * checkBalance
   *
   * execute MVNE CheckBalance, will delay or fail MISO on error
   * @param Object ACC control channel class
   * @param Boolean use cached result if available
   * @retuns Object balance query data on success or NULL on failure
   */
  private function checkBalance($control, $cached = FALSE)
  {
    $balance = NULL;

    // check cache if allowed
    $key = self::CHECK_BALANCE_KEY . $this->customer_id;
    if ($cached)
      if ($balance = $this->redis->get($key))
        return json_decode($balance);

    // check parameters
    if (empty($control) || empty($this->makeitso_options->msisdn))
    {
      $this->fail('ERR_API_INTERNAL: invalid arguments');
      return NULL;
    }

    // execute ACC API CheckBalance
    $command = 'CheckBalance';
    $result = $control->processControlCommand(array(
      'command'     => $command,
      'actionUUID'  => $this->action_uuid,
      'parameters'  => array('msisdn' => $this->makeitso_options->msisdn)));

    // handle timeout
    if ($result->is_timeout())
    {
      $this->delay("$command timeout" , $command);
      return NULL;
    }

    // handle errors
    if ($result->has_errors())
    {
      dlog('', 'errors = %s', $result->get_errors());
      $resultCode = isset($result->data_array['ResultCode']) ? $result->data_array['ResultCode'] : NULL;
      if ( ! empty($result->data_array['fatal']))
        $this->fail(implode(',', $result->get_errors()), $command, $resultCode);
      else
        $this->delay("Cannot perform $command" , $command, $resultCode);
      return NULL;
    }

    // extract and cache balance values
    $message = $control->controlObject->extractDataBodyFromMessage($result->data_array['message']);
    $this->redis->set($key, json_encode($message), self::CHECK_BALANCE_TTL);

    // objectify data for consistency with json_decode (it cannot preserve mixed dataypes: either objectify all associative arrays (default) or return all structures as arrays)
    $message = (object)$message;
    if ( empty($message->BalanceValueList))
    {
      $message->BalanceValueList = (object)$message->BalanceValueList;
      if ( ! empty($message->BalanceValueList->BalanceValue))
      {
        $message->BalanceValueList->BalanceValue = (array)$message->BalanceValueList->BalanceValue;
        foreach ($message->BalanceValueList->BalanceValue as &$balance)
          $balance = (object)$balance;
      }
    }

    return $message;
  }

  /**
   * logBalance
   *
   * Save CheckBalance values into MRC_USAGE table.
   * Invoke CheckBalance if values are not cached
   *
   * @see API-156
   * @param Object ACC Control
   */
  private function logBalance($control, $statusData = NULL, $balanceData = NULL)
  {
    // skip logging if plan tracker is missing (new activations)
    if (empty($this->makeitso_options->plan_tracker_id))
      return \logInfo('plan tracker is not present, skip');

    // check if already logged
    if (mrcUsageExists($this->customer_id, $this->makeitso_options->plan_tracker_id))
      return \logInfo('usage already logged, skip');

    // get cached QuerySubscriber data if not provided
    if ( ! $statusData)
      if ( ! $statusData = $this->querySubscriber($control, TRUE))
        return \logError('failed QuerySubscriber');

    // validate QuerySubscriber data
    if (empty($statusData->AddOnFeatureInfoList))
      return \logError('invalid or empty AddOnFeatureInfoList');
    if (empty($statusData->AddOnFeatureInfoList->AddOnFeatureInfo))
      return \logError('invalid or empty AddOnFeatureInfo');
    if (empty($statusData->SubscriberStatus) || $statusData->SubscriberStatus != 'Active')
      return \logError("invalid subscriber status {$statusData->SubscriberStatus}");
    $features = $statusData->AddOnFeatureInfoList->AddOnFeatureInfo;

    // get cached CheckBalance data if not provided
    if ( ! $balanceData)
      if ( ! $balanceData = $this->checkBalance($control, TRUE))
        return \logError('failed CheckBalance');

    // validate CheckBalance data
    if (empty($balanceData->BalanceValueList))
      return \logError('invalid or empty BalanceValueList');
    if (empty($balanceData->BalanceValueList->BalanceValue))
      return \logError('invalid or empty BalanceValue');
    $values = $balanceData->BalanceValueList->BalanceValue;

    // verify new plan SOCs: new plans must have A-DATA-BLK-PLAN
    if ( ! findObjectByProperty($features, array('UltraServiceName' => 'A-DATA-BLK-PLAN')))
      return \logInfo("subscriber {$this->customer_id} has old SOCs, skip");

    // get SOC lookup values
    $lookups = mrcUsageLookup();
    if ( ! count($lookups))
      return \logError("failed to get lookup values");

    // create MRC_USAGE record
    if ( ! $mrcId = mrcUsageInsert($this->customer_id, $this->makeitso_options->plan_tracker_id, $balanceData->serviceTransactionId, $statusData->serviceTransactionId, $statusData->wholesalePlan))
      return \logError('failed to create MRC_USAGE record');

    // walk through all lookup SOCs and collect their values
    $usageValues = array();
    foreach ($lookups as $lookup)
    {
      // is there a cleaner way to map ACC counter name?
      if ($lookup->ULTRA_SOC == 'A-DATA-BLK')
        $lookup->TYPE = '7007';

      // locate corresponding balance value
      $balanceValue = findObjectByProperty($values, array('Type' => $lookup->TYPE, 'Name' => $lookup->NAME));

      // check that SOC is present in features
      if ($balanceValue && $lookup->TYPE == 'Base Data Usage')
        if ( ! $feature = findObjectByProperty($features, array('UltraServiceName' => $lookup->ULTRA_SOC)))
          $balanceValue = NULL;

      // write value to DB if found (some values must be always written)
      if ($balanceValue || in_array($lookup->TYPE, array('DA1', 'DA2', 'WPRBLK20')))
      {
        // convert data units to KB
        if ( ! empty($balanceValue->Unit))
        {
          if ($balanceValue->Unit == 'GB')
            $balanceValue->Value = round($balanceValue->Value * 1024 * 1024);
          elseif ($balanceValue->Unit == 'MB')
            $balanceValue->Value = round($balanceValue->Value * 1024);
        }

        $usageValues[] = array('mrc' => $mrcId, 'lookup' => $lookup->LOOKUP_ID, 'value' => $balanceValue ? ($balanceValue->Value) : NULL);
      }
    }

    // execute INSERT
    mrcUsageValuesInsert($usageValues);

  }

  /**
   * processIntraPort
   *
   * execute MVNE actions for intra-brand port out and port in: ChangeSIM, DeactivateSubscriber, ReactivateSubscriber
   * @see PJW-33, API-334
   * @param Array parameters
   * @return Object result
   */
  private function processIntraPort()
  {
    // log MISO state
    logInfo("MISO ID {$this->makeitso_queue_id}: status {$this->status}, attempts {$this->attempt_count}, last ACC API {$this->last_acc_api}, history '{$this->attempt_history}'");

    // fail if too may attempts
    if ($this->attempt_count > 30)
    {
      logError("failing MISO {$this->makeitso_queue_id} due to {$this->attempt_count} attempts");
      return $this->fail('Too many attemps');
    }

    // perform a QuerySubscriber
    $mvne = new \Ultra\Lib\MiddleWare\ACC\Control;
    if ( ! $status = $this->querySubscriber($mvne))
      return $this->delay('Cannot perform QuerySubscriber');

    // validate query result
    if (empty($status->ResultCode) || $status->ResultCode != 100 || empty($status->SubscriberStatus) || empty($status->SIM))
      return $this->delay('Cannot perform QuerySubscriber');

    // delay if previous API call is still pending
    if ( ! empty($status->CurrentAsyncService))
      return $this->delay("Pending command {$status->CurrentAsyncService}");
    logInfo("subscriber status: {$status->SubscriberStatus}, current ICCID: {$status->SIM}");

    // step 1: ChangeSIM if on the old one
    $command = 'ChangeSIM';
    if (luhnenize($status->SIM) == $this->makeitso_options->old_iccid)
    {
      // we cannot ChangeSIM for subs in other states
      if ($status->SubscriberStatus != 'Suspended' && $status->SubscriberStatus != 'Active')
        return $this->fail("Unable to ChangeSIM for SubscriberStatus {$status->SubscriberStatus}");

      // we must Restore if Suspended before attempting to ChangeSIM
      if ($status->SubscriberStatus == 'Suspended')
      {
        $command = 'RestoreSubscriber';
        $result = $mvne->processControlCommand(array(
          'command'    => $command,
          'actionUUID' => $this->action_uuid,
          'parameters' => array(
            'customer_id'        => $this->customer_id,
            'iccid'              => $this->makeitso_options->old_iccid,
            'msisdn'             => $this->makeitso_options->msisdn)
          )
        );
        return $this->handleAsyncResult($result, $command, FALSE);
      }

      // SubscriberStatus is Active: ChangeSIM
      $result = $mvne->processControlCommand(array(
        'command'         => $command,
        'actionUUID'      => $this->action_uuid,
        'parameters'      => array(
          'customer_id'     => $this->customer_id,
          'msisdn'          => $this->makeitso_options->msisdn,
          'old_iccid'       => $this->makeitso_options->old_iccid,
          'new_iccid'       => $this->makeitso_options->new_iccid)
        )
      );
      return $this->handleAsyncResult($result, $command, FALSE);
    }

    list($proceed, $result) = $this->ensureCorrectWholesalePlan($mvne, $status);
    if ( ! $proceed)
      return $result;

    // step 2: DeactivateSubscriber (sub is now on the new SIM)
    $command = 'DeactivateSubscriber';
    if ($status->SubscriberStatus == 'Active' && in_array($this->last_acc_api, array('ChangeSIM', $command)))
    {
      $result = $mvne->processControlCommand(array(
        'command'         => $command,
        'actionUUID'      => $this->action_uuid,
        'parameters'      => array(
          'customer_id'     => $this->customer_id,
          'iccid'           => $this->makeitso_options->new_iccid,
          'msisdn'          => $this->makeitso_options->msisdn)
        )
      );
      return $this->handleAsyncResult($result, $command, FALSE);
    }

    // step 3: ReactivateSubscriber
    $command = 'ReactivateSubscriber';
    if ($status->SubscriberStatus == 'Inactive')
    {
      $result = $mvne->processControlCommand(array(
        'command'         => $command,
        'actionUUID'      => $this->action_uuid,
        'makeitsoQueueId' => $this->makeitso_queue_id,
        'parameters'      => array(
          'customer_id'         => $this->customer_id,
          'iccid'               => $this->makeitso_options->new_iccid,
          'msisdn'              => $this->makeitso_options->msisdn,
          'ultra_plan'          => $this->makeitso_options->ultra_plan,
          'preferred_language'  => $this->makeitso_options->preferred_language,
          'wholesale_plan'      => $this->makeitso_options->wholesale_plan)
        )
      );

      // finish MISO if command succeeds and we have arrived to the final state
      return $this->handleAsyncResult($result, $command, $this->makeitso_options->state == STATE_ACTIVE);
    }

    // step 4: Suspend (provisiniong)
    $command = 'SuspendSubscriber';
    if ($status->SubscriberStatus == 'Active')
    {
      $result = $mvne->processControlCommand(array(
        'command'         => $command,
        'actionUUID'      => $this->action_uuid,
        'makeitsoQueueId' => $this->makeitso_queue_id,
        'parameters'      => array(
          'customer_id'         => $this->customer_id,
          'iccid'               => $this->makeitso_options->new_iccid,
          'msisdn'              => $this->makeitso_options->msisdn)
        )
      );
      return $this->handleAsyncResult($result, $command, TRUE);
    }

    // this should not not be possible
    return $this->fail('Unexpected end of MISO error');
  }

  /**
   * handleAsyncResult
   *
   * intepret async ACC API result and handle MISO accordingly: delay, mark as pending or fail
   * @param Object Result
   * @param String ACC API called
   * @param Boolean finalize MISO on success
   * @return Object Result
   */
  private function handleAsyncResult($result, $command, $final)
  {
    // do not log last ACC API on timeout
    if ($result->is_timeout())
      return $this->delay("$command timeout");

    $errors = $result->get_errors();
    if ( ! empty($errors))
    {
      $resultCode = isset($result->data_array['ResultCode']) ? $result->data_array['ResultCode'] : NULL;
      if ( ! empty($result->data_array['fatal']))
        return $this->fail(implode(',', $errors), $command, $resultCode);
      else
        return $this->delay(implode(',', $errors), $command, $resultCode);
     }

    $message = empty($result->data_array['message']) ? NULL : json_decode($result->data_array['message']);

    if ( ! empty($message->body->errors))
      return $this->delay(implode(',', $message->body->errors));

     // no errors
     return $final ? $this->succeed($command) : $this->pending("Invoked $command", $command);
  }

  /**
   * hasMintDataAddOnSOC
   *
   * Given an object containing the output from QuerySubscriber, returns TRUE if it contains a Mint Data Add On SOC
   *
   * @return boolean
   */
  public function hasMintDataAddOnSOC($statusData, $addOnDataSoc = null)
  {
    $hasMintDataAddOnSOC = FALSE;
    $addOnDataSoc = ($addOnDataSoc) ? str_replace('|', '-', $addOnDataSoc) : null;

    foreach( $statusData->AddOnFeatureInfoList->AddOnFeatureInfo as $addOnFeature )
    {
      // we are checking for specific data soc
      if ($addOnDataSoc)
      {
        if ($addOnFeature->UltraServiceName == $addOnDataSoc)
          $hasMintDataAddOnSOC = TRUE;
      }
      // any mint data soc
      else
      {
        if ( \Ultra\MvneConfig\isMintDataSOC($addOnFeature->UltraServiceName) )
          $hasMintDataAddOnSOC = TRUE;
      }
    }

    return $hasMintDataAddOnSOC;
  }

  /**
   * getMintDataAddOnUsagePercentage
   *
   * Compute Data Add On usage threshold from CheckBalance for a Mint customer
   *
   * @return array
   */
  protected function getMintDataAddOnUsagePercentage($accMiddleware, $type)
  {
    $mintDataAddOnUsagePercentage = 0;
    $error = '';

    // invoke CheckBalance SOAP call
    $checkBalanceResult = $accMiddleware->processControlCommand([
      'command'    => 'CheckBalance', // this is the ACC Command
      'actionUUID' => $this->action_uuid,
      'parameters' => [
        'iccid'       => $this->makeitso_options->iccid,
        'msisdn'      => $this->makeitso_options->msisdn
      ]
    ]);

    // CheckBalance timeout
    if ( $checkBalanceResult->is_timeout() )
      return [ 0 , 'CheckBalance timeout' ];

    // CheckBalance failure
    if ( $checkBalanceResult->has_errors() )
    {
      \logError( json_encode($checkBalanceResult->get_errors()) );

      $resultCode = ( isset($checkBalanceResult->data_array['ResultCode']) ) ? $checkBalanceResult->data_array['ResultCode'] : NULL ;

      if ( $checkBalanceResult->data_array['fatal'] )
        return [ 0 , implode( ",", $checkBalanceResult->get_errors() ) ];
      else
        return [ 0 , 'Cannot perform CheckBalance' ];
    }

    // compute Data Add On usage threshold

    $used  = 0; // in KB
    $limit = 0; // in KB

    $checkBalanceData = $accMiddleware->controlObject->extractDataBodyFromMessage( $checkBalanceResult->data_array['message'] );

    foreach( $checkBalanceData['BalanceValueList']->BalanceValue as $balanceValue )
    {
      \logTrace( json_encode($balanceValue) );

      // if ( in_array( $balanceValue->Type , ['WPRBLK25','WPRBLK20'] ) )
      if ( $balanceValue->Type == $type )
      {
        if ( $balanceValue->Name == 'Used' )
          $used  += ( ( $balanceValue->Unit == 'MB' ) ? ( $balanceValue->Value * 1024 ) : $balanceValue->Value );

        if ( $balanceValue->Name == 'Limit' )
          $limit += ( ( $balanceValue->Unit == 'MB' ) ? ( $balanceValue->Value * 1024 ) : $balanceValue->Value );
      }
    }

    \logInfo( "Data Add On KB limit = $limit ; usage = $used" );

    // can we perform the computation?
    if ( empty($limit) )
      $error = 'CheckBalance error : Plan Data Limit not found';
    else
      $mintDataAddOnUsagePercentage = ceil( ( $used * 100 ) / $limit );

    return [ $mintDataAddOnUsagePercentage , $error ];
  }


} // class MakeItSo


/**
 * delayDueToMWQueueSize
 *
 * Delay execution due to large number of pending elements in MW queue(s)
 * ULTRA_MW/OUTBOUND/SYNCH
 * ACC_MW/OUTBOUND/SYNCH
 *
 * @return boolean
 */
function delayDueToMWQueueSize ( $redis=NULL )
{
  if ( ! $redis )
    $redis = new \Ultra\Lib\Util\Redis;

  $max_mw_queue_size = 90;

  $mw_queue_size_redis_key = 'ultra/mw_queue/outbound/size';

  $mw_queue_size = $redis->get( $mw_queue_size_redis_key );

  #dlog('',"mw_queue_size = $mw_queue_size");

  // the result of this method is cached because we don't want to access Redis an excessive amount of times
  if ( is_null($mw_queue_size) )
  {
    $ttl = 60;

    $controlChannel = new \Ultra\Lib\MQ\ControlChannel;

    $messageQueue = $controlChannel->outboundACCMWControlChannel();

    $members_1 = $redis->smembers( $messageQueue );

    dlog('',"count = ".count($members_1)." members = %s",$members_1);

    $messageQueue = $controlChannel->outboundUltraMWControlChannel();

    $members_2 = $redis->smembers( $messageQueue );

    dlog('',"count = ".count($members_2)." members = %s",$members_2);

    $mw_queue_size = max( count($members_1) , count($members_2) );

    if ( $mw_queue_size )
      $redis->set( $mw_queue_size_redis_key , $mw_queue_size , $ttl );
    else
      $redis->set( $mw_queue_size_redis_key , "0"            , $ttl );
  }

  if( $mw_queue_size > $max_mw_queue_size )
  {
    dlog('',"DELAY DUE TO MW QUEUES SIZE = $mw_queue_size");

    return TRUE;
  }

  return FALSE;
}

/**
 * exists_open_task
 *
 * Returns true if there is a PENDING task for the given customer id
 *
 * @return boolean
 */
function exists_open_task( $customer_id )
{
  \Ultra\Lib\DB\ultra_acc_connect();

  $data = get_makeitso(
    array(
      'customer_id' => $customer_id,
      'status'      => 'PENDING'
    )
  );

  teldata_change_db();

  return ( $data && is_array($data) && count($data) );
}


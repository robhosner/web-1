<?php

require_once 'db.php';
require_once 'Ultra/Lib/MVNE/Adapter.php';


abstract class AbstractTestStrategy
{
  abstract function test($argv);
}


class Test_mvneBypassListSynchronousSendSMS extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('text','msisdn');

    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[2 + $i]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    return mvneBypassListSynchronousSendSMS( $text , $msisdn );
  }
}

class Test_mvneBypassListAsynchronousSendSMS extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('text','msisdn');

    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[2 + $i]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    return mvneBypassListAsynchronousSendSMS( $text , $msisdn );
  }
}


class Test_mvneBypassSendSMS extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('message','msisdn');

    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[2 + $i]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    return mvneBypassSendSMS( $message , $msisdn );
  }
}


class Test_mvneCancelPortIn  extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('msisdn');

    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[2 + $i]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('transition_uuid', 'action_uuid');

    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneCancelPortIn(
      $msisdn,
      $transition_uuid,
      $action_uuid
    );
  }
}


class Test_mvneCustomerActiveCheck extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('customer_id');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $customer = get_customer_from_customer_id($customer_id);
    if (empty($customer))
      return make_error_Result("no customer for ID $customer_id");

    return mvneCustomerActiveCheck($customer);
  }
}


class Test_mvneRequestPortIn extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('customer_id', 'numberToPort', 'ICCID', 'portUsername', 'portPassword', 'billingZip', 'cos_id');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[2 + $i]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('transition_uuid', 'action_uuid');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneRequestPortIn(
      $customer_id,
      $numberToPort,
      $ICCID,
      $portUsername,
      $portPassword,
      $billingZip,
      $cos_id,
      $transition_uuid,
      $action_uuid);
  }
}


class Test_mvneUpdatePortIn extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('customer_id', 'numberToPort', 'ICCID', 'portUsername', 'portPassword', 'billingZip');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('transition_uuid', 'action_uuid');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneUpdatePortIn(
      $customer_id,
      $numberToPort,
      $ICCID,
      $portUsername,
      $portPassword,
      $billingZip,
      $transition_uuid,
      $action_uuid);
  }
}


class Test_mvneMakeitsoUpgradePlan extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('customer_id', 'option');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('action_uuid');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneMakeitsoUpgradePlan($action_uuid, $customer_id, $option);
  }
}


class Test_mvneMakeitsoRenewPlan extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('customer_id', 'plan');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('transition_uuid', 'action_uuid');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneMakeitsoRenewPlan($customer_id, $plan, $transition_uuid, $action_uuid);
  }
}


class Test_mvneChangePlanImmediate extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('customer_id', 'plan');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('transition_uuid', 'action_uuid');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneChangePlanImmediate($customer_id, $plan, $transition_uuid, $action_uuid);
  }
}


class Test_mvneChangeMSISDN extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('old_msisdn', 'iccid', 'zipcode', 'customer_id', 'source', 'account_id');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    return mvneChangeMSISDN($old_msisdn, $iccid, $zipcode, $customer_id, $source, $account_id);
  }
}


class Test_mvneProvisionSIM extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('ICCID', 'zipcode', 'language', 'plan', 'customer_id', 'transition_uuid', 'action_uuid');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    return mvneProvisionSIM($ICCID, $zipcode, $language, $plan, $customer_id, $transition_uuid, $action_uuid);
  }
}


class Test_mvneActivateSIM extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('ICCID', 'zipcode', 'language', 'plan', 'customer_id', 'transition_uuid', 'action_uuid');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('provisioning');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneActivateSIM($ICCID, $zipcode, $language, $plan, $customer_id, $transition_uuid, $action_uuid, $provisioning);
  }
}


class Test_mvneEnsureCancel extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('MSISDN', 'ICCID');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('transition_uuid', 'action_uuid', 'customer_id');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneEnsureCancel($MSISDN, $ICCID, $transition_uuid, $action_uuid, $customer_id);
  }
}


class Test_mvneEnsureSuspend extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('actionUUID', 'customer_id');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('msisdn', 'iccid');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneEnsureSuspend($actionUUID, $customer_id, $msisdn, $iccid);
  }
}


class Test_mvneEnsureDeactivate extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('actionUUID', 'customer_id');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('msisdn', 'iccid');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneEnsureDeactivate($actionUUID, $customer_id, $msisdn, $iccid);
  }
}

class Test_mvneGet4gLTE extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('actionUUID', 'msisdn');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $customer_id = \Ultra\Lib\DB\Getter\getScalar('MSISDN', $msisdn, 'customer_id');

    list( $remaining , $usage , $mintAddOnRemaining , $mintAddOnUsage , $mvneError ) = mvneGet4gLTE($actionUUID, $msisdn, $customer_id);

    echo "remaining = $remaining ; usage = $usage ; mintAddOnRemaining = $mintAddOnRemaining ; mintAddOnUsage = $mintAddOnUsage ; mvneError = $mvneError\n";

    exit;
  }
}


class Test_mvneSetVoicemailLanguage
{

  function test($argv)
  {
    $customer = get_customer_from_customer_id($argv[2]);
    $language = $argv[3];
    $result = mvneSetVoicemailLanguage($customer, $language);
    print_r($result);
  }
}

class Test_mvneIntraPort
{
  function test($argv)
  {
    if (count($argv) < 7)
      return make_error_Result('missing arguments');
    $result = mvneIntraPort($argv[2], $argv[3], luhnenize($argv[4]), luhnenize($argv[5]), $argv[6], $argv[7], $argv[8]);
    print_r($result);
  }
}

class Test_mvneMakeitsoRenewEndMintPlan extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('customer_id', 'plan');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('transition_uuid', 'action_uuid');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneMakeitsoRenewEndMintPlan($customer_id, $plan, $transition_uuid, $action_uuid);
  }
}

class Test_mvneMakeitsoRenewMidMintPlan extends AbstractTestStrategy
{
  function test($argv)
  {
    $required = array('customer_id', 'plan');
    for ($i = 0, $j = count($required); $i < $j; $i++)
      if (! isset($argv[$i + 2]))
        return make_error_Result("missing parameter {$required[$i]}");
      else
        $$required[$i] = $argv[$i + 2];

    $optional = array('transition_uuid', 'action_uuid');
    for ($i = 0, $k = count($optional); $i < $k; $i++)
      if (isset($argv[2 + $j + $i]))
        $$optional[$i] = $argv[2 + $j + $i];
      else
        $$optional[$i] = NULL;

    return mvneMakeitsoRenewMidMintPlan($customer_id, $plan, $transition_uuid, $action_uuid);
  }
}

/**
 * main
 */
if (empty($argv[1]))
{
  echo 'Usage:
  php Ultra/Lib/MVNE/Adapter_test.php mvneBypassListSynchronousSendSMS $MESSAGE $MSISDN
  php Ultra/Lib/MVNE/Adapter_test.php mvneBypassListAsynchronousSendSMS $MESSAGE $MSISDN
  php Ultra/Lib/MVNE/Adapter_test.php mvneActivateSIM $ICCID $ZIPCODE $LANGUAGE $PLAN $CUSTOMER_ID $TRANSITION_UUID $ACTION_UUID [$PROVISIONING]
  php Ultra/Lib/MVNE/Adapter_test.php mvneCancelPortIn $MSISDN
  php Ultra/Lib/MVNE/Adapter_test.php mvneChangeMSISDN $OLD_MSISDN $ICCID $ZIP $CUSTOMER_ID $SOURCE $ACCOUNT_ID
  php Ultra/Lib/MVNE/Adapter_test.php mvneChangePlanImmediate $CUSTOMER_ID $PLAN [$TRANSITION_UUID] [$ACTION_UUID]
  php Ultra/Lib/MVNE/Adapter_test.php mvneCustomerActiveCheck $CUSTOMER_ID
  php Ultra/Lib/MVNE/Adapter_test.php mvneEnsureCancel $MSISDN $ICCID [$TRANSITION_UUID] [$ACTION_UUID] [$CUSTOMER_ID]
  php Ultra/Lib/MVNE/Adapter_test.php mvneEnsureDeactivate $ACTION_UUID $CUSTOMER_ID [$MSISDN] [$ICCID]
  php Ultra/Lib/MVNE/Adapter_test.php mvneEnsureSuspend $ACTION_UUID $CUSTOMER_ID [$MSISDN] [$ICCID]
  php Ultra/Lib/MVNE/Adapter_test.php mvneGet4gLTE $ACTION_UUID $MSISDN
  php Ultra/Lib/MVNE/Adapter_test.php mvneMakeitsoRenewPlan $CUSTOMER_ID $PLAN [$TRANSITION_UUID] [$ACTION_UUID]
  php Ultra/Lib/MVNE/Adapter_test.php mvneMakeitsoUpgradePlan $CUSTOMER_ID $OPTION [$ACTION_UUID]
  php Ultra/Lib/MVNE/Adapter_test.php mvneProvisionSIM $ICCID $ZIPCODE $LANGUAGE $PLAN $CUSTOMER_ID $TRANSITION_UUID $ACTION_UUID
  php Ultra/Lib/MVNE/Adapter_test.php mvneRequestPortIn $CUSTOMER_ID $NUMBER_TO_PORT $ICCID $USERNAME $PASSWORD $ZIP $COS_ID [$TRANSITION_UUID] [$ACTION_UUID]
  php Ultra/Lib/MVNE/Adapter_test.php mvneUpdatePortIn $CUSTOMER_ID $NUMBER_TO_PORT $ICCID $USERNAME $PASSWORD $ZIP [$TRANSITION_UUID] [$ACTION_UUID]
  php Ultra/Lib/MVNE/Adapter_test.php mvneIntraPort $CUSTOMER_ID $MSISDN $OLD_ICCID $NEW_ICCID $PLAN $PREFERRED_LANGUAGE $FINAL_STATE
  php Ultra/Lib/MVNE/Adapter_test.php mvneMakeitsoRenewEndMintPlan $CUSTOMER_ID $PLAN [$TRANSITION_UUID] [$ACTION_UUID]
  php Ultra/Lib/MVNE/Adapter_test.php mvneMakeitsoRenewMidMintPlan $CUSTOMER_ID $PLAN [$TRANSITION_UUID] [$ACTION_UUID]
  ';
}
else
{
  $testClass = "Test_{$argv[1]}";
  if (! class_exists ($testClass))
    $result = make_error_Result("ERROR: class $testClass is not defined");
  else
  {
    teldata_change_db();
    $testObject = new $testClass();
    $result = $testObject->test($argv);
  }

  if (is_object($result))
  {
    if (count($result->get_errors()))
      echo "ERRORS:\n" . print_r($result->get_errors(), TRUE) . "\n";
    else
      echo "RESULT:\n" . print_r($result, TRUE) . "\n";
  }
  elseif (is_array($result))
  {
    if (count($result['errors']))
      echo "ERRORS:\n" . print_r($result['errors'], TRUE) . "\n";
    else
      echo "RESULT:\n" . print_r($result, TRUE) . "\n";
  }
  else // something else
    print_r($result);
}
echo "\n";

?>

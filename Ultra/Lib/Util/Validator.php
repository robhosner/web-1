<?php

namespace Ultra\Lib\Util;

include_once 'cosid_constants.php';
include_once 'db/htt_coverage_info.php';
include_once 'db/htt_inventory_sim.php';
include_once 'db/ultra_promotional_plans.php';
include_once 'lib/inventory/functions.php';
include_once 'lib/util-common.php';
include_once 'Validate.php';
include_once 'Validate/Finance/CreditCard.php';
require_once 'Ultra/Lib/DB/Getter.php';
require_once 'Ultra/Lib/Util/Locale.php';

require_once 'classes/PlanConfig.php';

/**
 * Static class Validator
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra API
 */
class Validator
{

  /**
   * validate
   *
   * Validate $value of type $type, with additional validation rules defined by $validation
   *  - $name is the field/parameter name
   *  - $type is the data type
   *  - $validation is the set of rules to be used for validation
   *  - $value is the value to be validated
   *
   * @return array of strings
   */
  public static function validate($name, $type, $validation, $value)
  {
    $errors = array();
    $error_codes = array();

    // dlog('', "type = $type , validation = %s , value = $value", $validation);

    list ( $validationErrors , $validationErrorCodes ) = self::validateByConfiguration($name, $validation, $value);

    list ( $typeError , $typeErrorCode ) = self::validateByType($name, $type, $value);

    if (count($validationErrors))
    {
      $errors = $validationErrors;
      $error_codes = $validationErrorCodes;
    }

    if ($typeError)
    {
      $errors[]      = $typeError;
      $error_codes[] = $typeErrorCode;
    }

    return array( $errors , $error_codes );
  }

  /**
   * validateByConfiguration
   *
   * Validate $value according to rules defined by $validation
   *
   * @return array of strings
   */
  public static function validateByConfiguration($name, $validation, $value)
  {
    $errors = array();
    $error_codes = array();

    if ($validation && is_array($validation))
    {
      foreach ($validation as $validationName => $validationConfiguration)
      {
        #dlog('', "validationName = $validationName ; validationConfiguration = %s", $validationConfiguration);

        // function to validate according to $validationName rules
        $validatorFunction = '\Ultra\Lib\Util\validator' . ucfirst($validationName);

        $validatorFunction = str_replace('_', '', $validatorFunction);

        #dlog('', "validatorFunction = $validatorFunction");

        $error_code = "";

        if (is_callable($validatorFunction))
        {
          list( $error , $error_code ) = $validatorFunction($name, $value, $validationConfiguration);
        }
        else
        {
          $error      = "Validator ``$validationName`` not defined.";
          $error_code = "VV0099";
        }

        if ($error)
        {
          $errors[]      = $error;
          $error_codes[] = $error_code;
        }
      }
    }

    return array( $errors , $error_codes );
  }

  /**
   * validateByType
   *
   * Validate $value of type $type
   *
   * @return array
   */
  public static function validateByType($name, $type, $value)
  {
    $error = '';
    $error_code = '';

    // string[] => stringArray
    $type = preg_replace("/\[\]/",'Array',$type);

    // function to validate type $type
    $validationFunction = '\Ultra\Lib\Util\validateType' . ucfirst($type);

    // dlog('', "validationFunction = $validationFunction");

    if (is_callable($validationFunction))
    {
      list( $error , $error_code ) = $validationFunction($name, $value);
    }
    else
    {
      $error = "Type $type validation not handled.";
      $error_code = "TODO"; # TODO:
    }

    return array( $error , $error_code );
  }
}

/**
 * validateTypeStringArray
 *
 * An array of strings
 *
 * @return array
 */
function validateTypeStringArray($name, $value)
{
  $error = '';
  $error_code = '';

  if ( ( $value != '' ) && ( ! is_array($value) ) )
  {
    $error_code = 'VV0035';
    $error = "ERR_API_INVALID_ARGUMENTS: $name is not a list of strings";
  }

  return array( $error , $error_code );
}

/**
 * validateTypeString
 *
 * Simply a string
 *
 * @return array
 */
function validateTypeString($name, $value)
{
  $error = '';
  $error_code = '';

  if ($value && ! is_string($value))
  {
    $error_code = 'VV0001';
    $error = "ERR_API_INVALID_ARGUMENTS: $name is not a string";
  }

  return array( $error , $error_code );
}

/**
 * validateTypeInteger
 *
 * Simply an integer
 *
 * @return array
 */
function validateTypeInteger($name, $value)
{
  $error = '';
  $error_code = '';

  $value = trim($value); // remove any leading and trailing spaces, tabs and EOL
  if ($value && (! preg_match('/^[+-]?\d+$/', $value)))
  {
    $error_code = 'VV0002';
    $error = "ERR_API_INVALID_ARGUMENTS: $name is not an integer";
  }

  return array( $error , $error_code );
}

/**
 * validateTypeReal
 *
 * Decimal
 *
 * @return array
 */
function validateTypeReal($name, $value)
{
  $error = '';
  $error_code = '';

  $test = 0 + $value;

  if ($value && ( ! (abs("$test" - "$value") < 0.00001) || ! preg_match('/^[\d\.\-\+]+$/', $value) ) )
  {
    $error_code = 'VV0003';
    $error = "ERR_API_INVALID_ARGUMENTS: $name is not a real number";
  }

  return array( $error , $error_code );
}

/**
 * validateTypeEpoch
 *
 * Epoch is simply an integer
 *
 * @return array
 */
function validateTypeEpoch($name, $value)
{
  $error = '';
  $error_code = '';

  if ($value && (! preg_match('/^\d+$/', $value)))
  {
    $error_code = 'VV0004';
    $error = "ERR_API_INVALID_ARGUMENTS: $name is not an epoch";
  }

  return array( $error , $error_code );
}

/**
 * validateTypeBoolean
 *
 * Only accept 0 or 1 as a valid boolean
 *
 * @return array
 */
function validateTypeBoolean($name, $value)
{
  $error = '';
  $error_code = '';

  if ($value && (! preg_match('/^[01]$/', $value)))
  {
    $error_code = 'VV0005';
    $error = "ERR_API_INVALID_ARGUMENTS: $name is not a boolean";
  }

  return array( $error , $error_code );
}

/**
 * validatorActivePlan
 *
 * validates against active plans in configuration
 *
 * @return array
 */
function validatorActivePlan($name, $val, $config = null)
{
  $error_code = '';
  $error      = '';

  $planConfig = \PlanConfig::Instance();
  $activePlans = $planConfig->getActivePlansAka();

  if ($config == 2)
    $activePlans = array_merge($activePlans, $planConfig->getActiveLongNames());

  if ( ! in_array($val, $activePlans))
  {
    $error_code = 'VV0006';
    $error = "ERR_API_INVALID_ARGUMENTS: $name has no matches";
  }

  return array($error, $error_code);
}

/**
 * validatorPlanExists
 *
 * validates against every plan in configuration
 */
function validatorPlanExists($name, $val, $config = null)
{
  $error_code = '';
  $error      = '';

  $planConfig = \PlanConfig::Instance();
  $allPlans   = $planConfig->getAllPlansAka();
  if ( ! in_array($val, $allPlans))
  {
    $error_code = 'VV0006';
    $error = "ERR_API_INVALID_ARGUMENTS: $name has no matches";
  }

  return array($error, $error_code);
}


/**
 * validatorMatches
 *
 * { "matches": [ "0", "1" ] }
 *
 * @return array
 */
function validatorMatches($name, $value, $set)
{
  $error = '';
  $error_code = '';

  if ($value && ! in_array($value, $set))
  {
    $error_code = 'VV0006';
    $error = "ERR_API_INVALID_ARGUMENTS: $name has no matches";
  }

  return array( $error , $error_code );
}

/**
 * validatorMatchesLanguage
 *
 * Validate for HTT_CUSTOMERS_OVERLAY_ULTRA.PREFERRED_LANGUAGE allowed values
 *
 * @return array
 */
function validatorMatchesLanguage($name, $value)
{
  return validatorMatches(
    $name,
    $value,
    array( 'EN' , 'ES' , 'ZH' )
  );
}

/**
 * validatorMatchesPlanState
 *
 * Validate for HTT_CUSTOMERS_OVERLAY_ULTRA.PLAN_STATE allowed values
 *
 * @return array
 */
function validatorMatchesPlanState($name, $value)
{
  return validatorMatches(
    $name,
    $value,
    array( STATE_CANCELLED, STATE_NEUTRAL, STATE_PORT_IN_DENIED, STATE_PORT_IN_REQUESTED, STATE_PRE_FUNDED, STATE_PROMO_UNUSED, STATE_PROVISIONED, STATE_SUSPENDED, STATE_ACTIVE )
  );
}

/**
 * validatorMinstrlen
 *
 * { "min_strlen": 1 }
 *
 * @return array
 */
function validatorMinstrlen($name, $value, $length)
{
  $error = '';
  $error_code = '';

  if ($value && (strlen($value) < $length))
  {
    $error_code = 'VV0007';
    $error = "ERR_API_INVALID_ARGUMENTS: $name value is too short";
  }

  return array( $error , $error_code );
}

/**
 * validatorMaxstrlen
 *
 * { "max_strlen": 128 }
 *
 * @return array
 */
function validatorMaxstrlen($name, $value, $length)
{
  $error = '';
  $error_code = '';

  if ($value && (strlen($value) > $length))
  {
    $error_code = 'VV0008';
    $error = "ERR_API_INVALID_ARGUMENTS: $name value is too long";
  }

  return array( $error , $error_code );
}

/**
 * validatorMinvalue
 *
 * { "min_value": 1 }
 *
 * @return array
 */
function validatorMinvalue($name, $value, $min)
{
  $error = '';
  $error_code = '';

  if ($value !== '' && $value < $min)
  {
    $error_code = 'VV0009';
    $error = "ERR_API_INVALID_ARGUMENTS: $name value is under min_value";
  }

  return array( $error , $error_code );
}

/**
 * validatorMaxvalue
 *
 * { "max_value": 2000 }
 *
 * @return array
 */
function validatorMaxvalue($name, $value, $max)
{
  $error = '';
  $error_code = '';

  if ($value && ($value > $max))
  {
    $error_code = 'VV0010';
    $error = "ERR_API_INVALID_ARGUMENTS: $name value is over max_value";
  }

  return array( $error , $error_code );
}

/**
 * validatorMinuniquecharacters
 *
 * { "min_unique_characters": 4 }
 *
 * @return array
 */
function validatorMinuniquecharacters($name, $value, $min)
{
  $error = '';
  $error_code = '';

  $chars = str_split($value);
  $numUniqueChars = sizeof(array_unique($chars));

  if ($numUniqueChars < $min)
  {
    $error_code = 'VV0269';
    $error = "ERR_API_INVALID_ARGUMENTS: $name has less than $min unique characters";
  }

  return array( $error , $error_code );
}

/**
 * validatorMsisdn
 *
 * 10 digits MSISDN
 * $option ignored for now
 *
 * @return array
 */
function validatorMsisdn($name, $value, $option=NULL)
{
  $error = '';
  $error_code = '';

  if ($value && ( ! is_numeric($value) || (strlen($value) != 10) ) )
  {
    $error_code = 'MV0001';
    $error = "ERR_API_INVALID_ARGUMENTS: $name is incorrect";
  }

  return array( $error , $error_code );
}

/**
 * validatorIccidFormat
 *
 * validate ICCID format
 * $option ignored for now
 *
 * @return array
 */
function validatorIccidFormat($name, $value, $option=NULL)
{
  $error = '';
  $error_code = '';

  if ($value && ( ! is_numeric($value) || (strlen($value) != 19) ) )
  {
    $error_code = 'MP0004';
    $error = "ERR_API_INVALID_ARGUMENTS: $name is incorrect";
  }

  return array( $error , $error_code );
}

/**
 * validatorMvne
 *
 * validate MVNE parameter propery
 *
 * @param name: parameter name (e.g. customer_id, ICCID, msisdn)
 * @param value: parameter value (expected MVNE value)
 * @return: array(message, code)
 */
function validatorMvne($name, $value, $mvne)
{
  // defaults
  $error = '';
  $error_code = '';

  // get cached DB value
  $actual = \Ultra\Lib\DB\Getter\getScalar($name, $value, 'MVNE');
  if ($actual && $actual != $mvne) // empty MVNE is Ok
  {
    $error_code = 'MV0001';
    $error = "ERR_API_INVALID_ARGUMENTS: cannot perform operation on given $name MVNE mismatch";
  }

  return array($error, $error_code);
}

/**
 * validatorIccid
 *
 * { "iccid": "activate_type_purple" } ; { "iccid": "type_purple" } ; { "iccid": "brand_id_1" } ; { "iccid": "brand_id_2" } ; { "iccid": "brand_id_3" }
 *
 * @return array
 */
function validatorIccid($name, $value, $option)
{
  $error = '';
  $error_code = '';

  if ($option === 'type_purple') // { "iccid": "type_purple" }
  {
    $validateResult = validate_sim_product_type('PURPLE', $value);
    if ($validateResult->is_failure())
    {
      $validateErrors = $validateResult->get_errors();
      if (is_array($validateErrors) && count($validateErrors) && ($validateErrors[0] == "ERR_API_INVALID_ARGUMENTS: Incorrect ICCID Product type"))
      {
        $error_code = "VV0011";
        $error = "ERR_API_PRODUCT: This SIM is not valid for your current plan.";
      }
    }
  }
  elseif ($option === 'activate_type_purple') // { "iccid": "activate_type_purple" }
  {
    $validateResult = validate_sim_product_type('PURPLE', $value);
    if ($validateResult->is_failure())
    {
      $validateErrors = $validateResult->get_errors();
      if (is_array($validateErrors) && count($validateErrors) && ($validateErrors[0] == "ERR_API_INVALID_ARGUMENTS: Incorrect ICCID Product type"))
      {
        $error_code = "VV0012";
        $error = "ERR_API_PRODUCT: This SIM cannot be activated by this process.";
      }
    }
  }
  elseif ( preg_match("/^brand_id_(\d)$/",$option,$matches) ) // { "iccid": "brand_id_1" } ; { "iccid": "brand_id_2" } ; { "iccid": "brand_id_3" }
  {
    $brand_id = $matches[1];

    \logDebug("brand_id = $brand_id");

    if ( \Ultra\UltraConfig\existsBrandId( $brand_id ) )
    {
      if ( ! validateICCIDBrandId( $value , $brand_id ) )
      {
        $error_code = 'IC0003';
        $error      = 'Invalid ICCID brand type';
      }
    }
    else
      $error = "ERR_API_INVALID_ARGUMENTS: Unknown validation $option , $value for $name";
  }
  else
    $error = "ERR_API_INVALID_ARGUMENTS: Unknown validation $option , $value for $name";

  return array( $error , $error_code );
}

/**
 * validatorIccid19userstatus
 *
 * { "iccid_19_user_status" : "VALID" } ; { "iccid_19_user_status": "INVALID" }
 * { "iccid_19_user_status" : "USED" }  ; { "iccid_19_user_status": "RESERVED" }
 * Important: do not use in JSON API definition due to performance considerations
 *
 * @return array
 */
function validatorIccid19userstatus($name, $value, $status)
{
  $error = '';
  $error_code = '';

  if (in_array($status, array( "VALID","INVALID","USED","RESERVED" )))
  {
    $iccid_status = get_ICCID_user_status($value, true);

    if ($iccid_status != $status)
    {
      switch ($iccid_status)
      {
        case "INVALID":
          $error_code = "VV0013";
          $error = "ERR_API_INVALID_SIM: $name SIM $value is INVALID, instead of $status";
          break;
        case "USED":
          $error_code = "VV0014";
          $error = "ERR_API_INVALID_SIM: $name SIM $value is ALREADY USED, instead of $status";
          break;
        case "RESERVED":
          $error_code = "VV0015";
          $error = "ERR_API_INVALID_SIM: $name SIM $value is CURRENTLY BEING ACTIVATED, PORTED OR OTHERWISE RESERVED, instead of $status";
          break;
        default:
          $error_code = "VV0016";
          $error = "ERR_API_INVALID_SIM: $name user not $status";
          break;
      }
    }
  }
  else
  {
    $error = "ERR_API_INVALID_ARGUMENTS: Unknown status $status for $name ICCID validation";
  }

  return array( $error , $error_code );
}

/**
 * validatorPromostatus
 *
 * { "promo_status": ["VIRGIN","PREPARATION"] }
 *
 * @return array
 */
function validatorPromostatus($name, $value, $option)
{
  $error = '';
  $error_code = '';

  $promoStatusValidateResult = validate_ultra_promotional_plans_status($value, $option);

  if ( ! $promoStatusValidateResult->is_success() )
  {
    $error_code = "VV0017";
    $error = "ERR_API_INVALID_ARGUMENTS: $name value fails status validation";
  }

  return array( $error , $error_code );
}

/**
 * validatorRegexp
 *
 * { "regexp" : "^[A-Za-z0-9]+$" }
 *
 * @return array
 */
function validatorRegexp($name, $value, $regexp)
{
  $error = '';
  $error_code = '';

  if ( !preg_match("/$regexp/", $value) )
  {
    $error_code = "VV0116";
    $error = "ERR_API_INVALID_ARGUMENTS: $name value is invalid";
  }

  return array( $error , $error_code );
}

/**
 * validatorMaxsecondsage
 *
 * { "max_seconds_age": 300 }
 *
 * @return array
 */
function validatorMaxsecondsage($name, $value, $seconds)
{
  $error = '';
  $error_code = '';

  // TODO:

  return array( $error , $error_code );
}

/**
 * validatorDateformat
 *
 * { "date_format": "dd-mm-yyyy" }
 * { "date_format": "mm-dd-yyyy" }
 * { "date_format": "mmyy" }
 *
 * @return array
 */
function validatorDateformat($name, $value, $format)
{
  $error = '';
  $error_code = '';

  if ( $format == 'mmyy' )
  {
    if ( preg_match("/^(\d\d)(\d\d)$/", $value, $matches) )
    {
      if ( ( $matches[1] < 1 ) || ( $matches[1] > 12 ) )
      {
        $error_code = "VV0033";
        $error      = "ERR_API_INVALID_ARGUMENTS: $name is not a valid date";
      }
      #elseif ( ( date('y') > $matches[2] ) || ( ( date('y') == $matches[2] ) && ( date('m') > $matches[1] ) ) )
      #{
      #  // expired
      #}
    }
    else
    {
      $error_code = "VV0070";
      $error      = "ERR_API_INVALID_ARGUMENTS: $name is not a date in the format mmyy";
    }
  }
  elseif ( $format == 'dd-mm-yyyy' )
  {
    if ( preg_match("/^(\d\d)\-(\d\d)\-(\d\d\d\d)$/", $value, $matches) )
    {
      if ( ( $matches[1] < 1 ) || ( $matches[1] > 31 ) || ( $matches[2] < 1 ) || ( $matches[2] > 12 ) || ( $matches[3] <= 0 ) )
      {
        $error_code = "VV0033";
        $error      = "ERR_API_INVALID_ARGUMENTS: $name is not a valid date";
      }
    }
    else
    {
      $error_code = "VV0034";
      $error      = "ERR_API_INVALID_ARGUMENTS: $name is not a date in the format dd-mm-yyyy";
    }
  }
  elseif ( $format == 'mm-dd-yyyy' )
  {
    if ( preg_match("/^(\d\d)\-(\d\d)\-(\d\d\d\d)$/", $value, $matches) )
    {
      if ( ( $matches[1] < 1 ) || ( $matches[1] > 12 ) || ( $matches[2] < 1 ) || ( $matches[2] > 31 ) || ( $matches[3] <= 0 ) )
      {
        $error_code = "VV0033";
        $error      = "ERR_API_INVALID_ARGUMENTS: $name is not a valid date";
      }
    }
    else
    {
      $error_code = "VV0035";
      $error      = "ERR_API_INVALID_ARGUMENTS: $name is not a date in the format mm-dd-yyyy";
    }
  }
  else
  {
    $error = "ERR_API_INVALID_ARGUMENTS: Unknown format $format for $name validation";
  }

  return array( $error , $error_code );
}

/**
 * validatorFormat
 *
 * { "format": "JSON" }
 *
 * @return array
 */
function validatorFormat($name, $value, $format)
{
  $error = '';
  $error_code = '';

  // TODO:

  return array( $error , $error_code );
}

/**
 * validatorEmail
 *
 * @return array
 */
function validatorEmail($name, $value, $field)
{
  $error = '';
  $error_code = '';

  if ( ! check_duplicate_email($value) )
  {
    $error_code = "VV0059";
    $error = "ERR_API_INVALID_ARGUMENTS: $name is in use by an existing customer";
  }

  return array( $error , $error_code );
}

/**
 * validatorStringformat
 *
 * { "stringformat": "email" }
 * { "stringformat": "numeric" }
 * { "stringformat": "alphanumeric" }
 * { "stringformat": "alphanumeric_and_spaces" }
 * { "stringformat": "letters_spaces_apostrophe" }
 * { "stringformat": "alphanumeric_spaces_punctuation" }
 * { "stringformat": "street_address" }
 * { "stringformat": "alphanumeric_punctuation" }
 * { "stringformat": "alphanumeric_spaces_punctuation_octothorpe" }
 * { "stringformat": "no_digits" }
 * { "stringformat": "no_symbols" }
 * { "stringformat": "profanity_exclude" }
 * 
 * @return array
 */
function validatorStringformat($name, $value, $field)
{
  $error = '';
  $error_code = '';

  $validate = new \Validate;

  $fieldArr = explode( '|', $field );
  foreach ( $fieldArr as $field )
  {
    if ( $field == 'email' )
    {
      if ( ! $validate->email($value) )
      {
        $error_code = "EM0001";
        $error = "ERR_API_INVALID_ARGUMENTS: $name is not a valid email address";
      }
    }
    elseif ( $field == 'numeric' )
    {
      if ( ! $validate->string($value, array('format' => VALIDATE_NUM)) )
      {
        $error_code = "ST0001";
        $error = "ERR_API_INVALID_ARGUMENTS: $name must be numeric";
      }
    }
    elseif ( $field == 'alphanumeric' )
    {
      if ( ! $validate->string($value, array('format' => VALIDATE_ALPHA . VALIDATE_NUM)) )
      {
        $error_code = "ST0002";
        $error = "ERR_API_INVALID_ARGUMENTS: $name is not alphanumeric";
      }
    }
    elseif ( $field == 'alphanumeric_and_spaces' )
    {
      if ( ! $validate->string($value, array('format' => VALIDATE_ALPHA . VALIDATE_NUM . ' ')) )
      {
        $error_code = "ST0003";
        $error = "ERR_API_INVALID_ARGUMENTS: $name contains invalid characters";
      }
    }
    elseif ( $field == 'letters_spaces_apostrophe' )
    {
      if ( ! $validate->string($value, array('format' => VALIDATE_ALPHA . "' ")) )
      {
        $error_code = "ST0004";
        $error = "ERR_API_INVALID_ARGUMENTS: $name contains invalid characters";
      }
    }
    elseif ( $field == 'alphanumeric_spaces_punctuation' )
    {
      if ( ! $validate->string($value, array('format' => VALIDATE_STREET . VALIDATE_PUNCTUATION)) )
      {
        $error_code = "ST0005";
        $error = "ERR_API_INVALID_ARGUMENTS: $name contains invalid characters";
      }
    }
    elseif ( $field == 'street_address' )
    {
      if ( ! $validate->string($value, array('format' => VALIDATE_STREET)) )
      {
        $error_code = "ST0006";
        $error = "ERR_API_INVALID_ARGUMENTS: $name contains invalid characters";
      }
    }
    elseif ( $field == 'alphanumeric_punctuation' )
    {
      if ( ! $validate->string($value, array('format' => VALIDATE_ALPHA . VALIDATE_NUM . VALIDATE_PUNCTUATION)) )
      {
        $error_code = "ST0007";
        $error = "ERR_API_INVALID_ARGUMENTS: $name must be alphanumeric with punctuation";
      }
    }
    elseif ( $field == 'alphanumeric_spaces_punctuation_octothorpe' )
    {
      if ( ! $validate->string($value, array('format' => VALIDATE_STREET . VALIDATE_PUNCTUATION . '#')) )
      {
        $error_code = "ST0008";
        $error = "ERR_API_INVALID_ARGUMENTS: $name contains invalid characters"; 
      }
    }
    elseif ( $field == 'not_all_numeric' )
    {
      if ( is_numeric( $value ) )
      {
        $error_code = "ST0009";
        $error = "ERR_API_INVALID_ARGUMENTS: $name must contain at least one alphabetic character";
      }
    }
    elseif ( $field == 'no_digits' )
    {
      if ( preg_match( '/[0-9]/', $value ) )
      {
        $error_code = 'ST0010';
        $error = "ERR_API_INVALID_ARGUMENTS: $name cannot contain digits";
      }
    }
    elseif ( $field == 'no_symbols' )
    {
      if ( preg_match( "/[^a-zA-Z0-9\s]/", $value ) )
      {
        $error_code = 'ST0011';
        $error = "ERR_API_INVALID_ARGUMENTS: $name cannot contain symbols";
      }
    }
    elseif ( $field == 'printable_ascii' )
    {
      if ( preg_match( '/[^\x20-\x7e]/', $value ) )
      {
        $error_code = 'ST0012';
        $error = "ERR_API_INVALID_ARGUMENTS: $name cannot contain non-printable ascii characters";
      }
    }
    elseif ( $field == 'alpha_with_unicode' )
    {
      if ( preg_match( '/[^a-zA-Z\p{L} ]/u', $value ) )
      {
        $error_code = 'ST0013';
        $error = "ERR_API_INVALID_ARGUMENTS: $name can only contain alphabetic characters";
      }
    }
    elseif ( $field == 'profanity_exclude' )
    {
      if ( !validateProfanity($value) )
      {
        $error_code = 'ST0014';
        $error = "ERR_API_INVALID_ARGUMENTS: $name cannot contain profanities";
      }
    }
    elseif ( $field == 'alphanumeric_with_underscores' )
    {
      if ( ! preg_match('/^[a-zA-Z0-9_]+$/', $value ))
      {
        $error_code = 'ST0015';
        $error = "ERR_API_INVALID_ARGUMENTS: $name can only contain underscores, and alphanumeric characters";
      }
    }
    elseif ( $field == 'alphanumeric_start_end' )
    {
      if ( ! preg_match( '/^[a-zA-Z0-9]+$/', substr( $value, 0, 1 ) ) || ! preg_match( '/^[a-zA-Z0-9]+$/', substr( $value, -1 ) ) )
      {
        $error_code = 'ST0016';
        $error = "ERR_API_INVALID_ARGUMENTS: $name must begin and end with an alphanumeric character";
      }
    }
    elseif ( $field == 'no_sequential_symbols' )
    {
      if ( preg_match('/([\\_\\-\\.][\\_\\-\\.])/', $value) )
      {
        $error_code = 'ST0017';
        $error = "ERR_API_INVALID_ARGUMENTS: $name must not contain more than one symbol in a row";
      }
    }
    else
    {
      \logWarn("Mode $field not defined");
    }
  }
  
  return array( $error , $error_code );
}

/**
 * validatorActcode
 *
 * { "actcode": "valid_actcode" }
 *
 * @return array
 */
function validatorActcode($name, $value, $option)
{
  $error = '';
  $error_code = '';

  if ($option === 'valid_actcode') // { "actcode": "valid_actcode" }
  {
    // ActCodes are 11 digits, with the 11th digit being a checkdigit - the
    // usual LUHN algorithm check.
    // ActCodes are always provided with all 11 digits.
    // Actcodes will ALWAYS be prepended by [2-9]
    if (! preg_match("/^[2-9]\d{10}/", $value))
    {
      $error_code = "VV0020";
      $error = "ERR_API_INVALID_ARGUMENTS: $name value fails Actcode format validation";
    }
    elseif (! validate_act_code($value))
    {
      $error_code = "VV0018";
      $error = "ERR_API_INVALID_ARGUMENTS: $name value fails LUHN checksum";
    }
  }
  else
  {
    $error_code = "VV0019";
    $error = "ERR_API_INVALID_ARGUMENTS: Unknown validation $option for $name";
  }

  return array( $error , $error_code );
}

/**
 * validatorAddress
 *
 * @return array
 */
function validatorAddress($name, $value)
{
  $error = '';
  $error_code = '';

  $validate = new \Validate;

  if ( ! $validate->string( $value, array( 'format' => VALIDATE_STREET . VALIDATE_PUNCTUATION . '#' ) ) )
  {
    $error_code = "VV0047";
    $error = 'ERR_API_INVALID_ARGUMENTS: invalid address';
  }

  return array( $error , $error_code );
}

/**
 * validatorCC
 *
 * Credit Card number validation (MasterCard, Visa, AMEX, Discover allowed)
 *
 * @return array
 */
function validatorCC($name, $value)
{
  $error = '';
  $error_code = '';

  $valid = FALSE;

  $cc_types = array('MasterCard', 'Visa', 'AMEX', 'Discover');

  $validate = new \Validate_Finance_CreditCard;

  foreach ( $cc_types as $cc_type )
    if ( $validate->number( $value , $cc_type ) )
      $valid = TRUE;

  if ( ! $valid )
  {
    $error_code = "VV0063";
    $error = 'ERR_API_INVALID_ARGUMENTS: invalid credit card number';
  }

  return array( $error , $error_code );
}

/**
 * validatorCCDuplicate
 *
 * @return array
 */
function validatorCCDuplicate($name, $cc_number, $customer_id=NULL, $cvv, $cc_exp, $last_name)
{
  $error = '';
  $error_code = '';

  // some environments allow infinite CC count
  if ( \Ultra\UltraConfig\allowInfiniteCCCount() )
    return array( $error , $error_code );

  // CC duplicate check override
  if ( overridden_cc_dupe_check($cvv, $cc_number, $cc_exp, $last_name) )
    return array( $error , $error_code );

  $maxDuplicateCCCount = \Ultra\UltraConfig\maxDuplicateCCCount();

  $count = \ultra_cc_holders_count( $cc_number , $customer_id );

  if ( $count >= $maxDuplicateCCCount )
  {
    dlog("","This credit card is already in use by other $count users in our system");

    $error_code = 'CC0003';
    $error = 'ERR_API_INVALID_ARGUMENTS: This credit card is already in use by other users in our system';
  }

  return array( $error , $error_code );
}

/**
 * validatorCity
 *
 * @return array
 */
function validatorCity($name, $value)
{
  $error = '';
  $error_code = '';

  $validate = new \Validate;

  if ( ! $validate->string( $value, array( 'format' => VALIDATE_ALPHA . VALIDATE_NUM . VALIDATE_PUNCTUATION ) ) )
  {
    $error_code = "VV0049";
    $error = 'ERR_API_INVALID_ARGUMENTS: invalid city';
  }

  return array( $error , $error_code );
}

/**
 * validatorCountry
 *
 * @return array
 */
function validatorCountry($name, $value)
{
  $error = '';
  $error_code = '';

  if ( $value != 'US' )
  {
    $error_code = "VV0058";
    $error = 'ERR_API_INVALID_ARGUMENTS: invalid country';
  }

  return array( $error , $error_code );
}

/**
 * validatorCVV
 *
 * @return array
 */
function validatorCVV($name, $value)
{
  $error = '';
  $error_code = '';

  if ( !is_numeric($value) || (strlen($value) < 3) || (strlen($value) > 4) )
  {
    $error_code = 'VV0062';
    $error = 'ERR_API_INVALID_ARGUMENTS: the given cvv is invalid';
  }

  return array( $error , $error_code );
}

/**
 * validatorZipcode
 *
 * { "zipcode": "US" } ; { "zipcode": "in_coverage" }
 * Important: do not use { "zipcode": "in_coverage" } in JSON API definition due to performance considerations
 *
 * @return array
 */
function validatorZipcode($name, $value, $option)
{
  $error = '';
  $error_code = '';

  if ($option === 'US')
  {
    if (! preg_match("/^[0-9]{5}$/", $value))
    {
      $error_code = "VV0023";
      $error = "ERR_API_INVALID_ZIP: $name value is not a US zipcode";
    }
  }
  elseif ($option === 'in_coverage')
  {
    if ( preg_match("/^[0-9]{5}$/", $value ) )
    {
      if ( ! mssql_has_rows( htt_coverage_info_select_query( array( 'zip_code' => $value ) ) ) )
      {
        $error_code = "VV0021";
        $error = "ERR_API_INVALID_ZIP: ZIP Code $value is not in coverage. Choose a valid Zip.";
      }
    }
    else
    {
      $error_code = "VV0023";
      $error = "ERR_API_INVALID_ZIP: $name value is not a US zipcode";
    }
  }
  else
  {
    $error_code = "VV0019";
    $error = "ERR_API_INVALID_ARGUMENTS: validation type $option is unknown for field $name";
  }

  return array( $error , $error_code );
}

/**
 * validatorPIn
 *
 * INCOM-7: validate PIN
 *
 * @param string name
 * @param string value
 * @param int or array of ints: possible lengths
 * @return: array(message, code)
 */
function validatorPin($name, $value, $length)
{
  // defaults
  $error = NULL;
  $code = NULL;

  // check if string contains only digits
  if (! ctype_digit($value))
  {
    $code = 'PI0001';
    $error = 'ERR_API_INVALID_ARGUMENTS: invalid PIN (1)';
  }

  // check against multiple possible lengths
  elseif (is_array($length))
  {
    if (! in_array(strlen($value), $length))
    {
      $code = 'PI0001';
      $error = 'ERR_API_INVALID_ARGUMENTS: invalid PIN (2)';
    }
  }

  else // check against a single length
  {
    if ($length != strlen($value))
    {
      $code = 'PI0001';
      $error = 'ERR_API_INVALID_ARGUMENTS: invalid PIN (3)';
    }
  }

  return array($error, $code);
}

/**
 * validatorName
 *
 * @return string
 */
function validatorName($name, $value)
{
  $error = '';
  $error_code = '';

  $validate = new \Validate;

  if ( ! $validate->string( $value, array( 'format' => VALIDATE_ALPHA . "' " ) ) )
  {
    $error_code = "VV0038";
    $error = 'ERR_API_INVALID_ARGUMENTS: invalid name';
  }

  return array( $error , $error_code );
}

/**
 * validatorNumeric
 *
 * { "numeric": "integer" } -- digits [0..9] and any length
 * { "numeric": "float" } -- floating point numbers
 * { "numeric": "currency_US" } -- US Currency values
 *
 * @return string
 */
function validatorNumeric($name, $value, $option)
{
  $error = '';
  $error_code = '';

  // Not an inteter in the classical sense, but a string of digits
  if ($option === 'integer')
  {
    if (! preg_match('/^\d+$/', $value))
    {
      $error_code = 'VV0256';
      $error = "ERR_API_INVALID_ARGUMENTS: $name value fails integer format validation, must be a string containing only digits";
    }
  }
  elseif ($option === 'float')
  { // Checks flaoting point and scientific notation

    if ($value != NULL && (! is_float($value))) // Floating point and Scientific notation allowed
    {
      $error_code = 'VV0257';
      $error = "ERR_API_INVALID_ARGUMENTS: $name value fails float format validation";
    }
  }
  elseif ($option === 'currency_US') // Checks for valid currency values,the "$" is optional
  {
    if ($value != NULL && (! preg_match('/^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$/', $value)))
    {
      $error_code = 'VV0258';
      $error = "ERR_API_INVALID_ARGUMENTS: $name value fails USA Currency format validation";
    }
  }
  else
  {
    $error_code = 'VV0099';
    $error = "ERR_API_INVALID_ARGUMENTS: Unknown validation $option for $name";
  }

  return array( $error , $error_code );
}

/**
 * validatorCountry2chars
 *
 * { "country2chars": "ISO_3166-1" }
 *
 * @return string
 */
function validatorCountry2chars($name, $value, $option)
{
  $error = '';
  $error_code = '';

  if ($option === 'ISO_3166-1')
  {
    if ( ! empty($value) && ! validatorCountryCode( $value ) )
    {
      $error_code = "VV0058";
      $error = "ERR_API_INVALID_ARGUMENTS: $name value fails ISO_3166-1 validation";
    }
  }
  else
    $error = "ERR_API_INVALID_ARGUMENTS: Unknown validation $option for $name";

  return array( $error , $error_code );
}

/**
 * validatorCountryCode
 *
 * Given a country code, will determine if it is valid or not.
 * @param string $country_code
 * @return boolean
 * @author bwalters@ultra.me
 */
function validatorCountryCode($country_code = false)
{
  // Was country_code supplied?
  if (!$country_code) return false;

  // Is it a valid length?
  if (2 != strlen($country_code)) return false;

  // Is it in our list of country codes?
  $country_codes = getAlpha2CountryCodes();
  return !! (in_array($country_code, $country_codes));
}

/**
 * validatorCountryName
 *
 * Given a country name, will determine if it is valid or not.
 * @param string $country
 * @return boolean
 * @author bwalters@ultra.me
 */
function validatorCountryName($country = false)
{
  // Was country supplied?
  if (!$country) return false;

  // Is it a valid length? Found by checking strlen() of all countries
  if (strlen($country) < 4) return false;

  // Is it in our list of country codes?
  $countries = array_flip(getAlpha2CountryCodesMap());
  return !!(isset($countries[$country]));
}

/**
 * validatorPassword
 * Validates given password against rule provided
 * 
 * @param  string $name
 * @param  string $value
 * @param  string $rule
 * @return array $errors
 */
function validatorPassword($name, $value, $rule)
{
  $error = '';
  $error_code = '';

  if ($rule === 'blocklist')
  {
    if (!validateAgainstBlocklist($value))
    {
      $error = "ERR_API_INVALID_ARGUMENTS: password fails blocklist validation";
      $error_code = 'VV0245';
    }
  }
  else
  {
    $error = 'ERR_API_INTERNAL: No password validation rule provided';
    $error_code = 'VV0246';
  }

  return array( $error , $error_code );
}

/**
 * getPwBlockList
 * returns list of blocked passwords
 * list from : http://www.bmyers.com/public/1958.cfm
 * 
 * @return array
 */
function getPwBlockList()
{
  return array(
    '123456','porsche','firebird','prince','rosebud','password','guitar','butter','beach','jaguar',
    '12345678','chelsea','united','amateur','great','1234','black','turtle','7777777','cool',
    'pussy','diamond','steelers','muffin','cooper','12345','nascar','tiffany','redsox','1313',
    'dragon','jackson','zxcvbn','star','scorpio','qwerty','cameron','tomcat','testing','mountain',
    '696969','654321','golf','shannon','madison','mustang','computer','bond007','murphy','987654',
    'letmein','amanda','bear','frank','brazil','baseball','wizard','tiger','hannah','lauren',
    'master','xxxxxxxx','doctor','dave','japan','michael','money','gateway','eagle1','naked',
    'football','phoenix','gators','11111','squirt','shadow','mickey','angel','mother','stars',
    'monkey','bailey','junior','nathan','apple','abc123','knight','thx1138','raiders','alexis',
    'pass','iceman','porno','steve','aaaa','fuckme','tigers','badboy','forever','bonnie',
    '6969','purple','debbie','angela','peaches','jordan','andrea','spider','viper','jasmine',
    'harley','horny','melissa','ou812','kevin','ranger','dakota','booger','jake','matt',
    'iwantu','aaaaaa','1212','lovers','qwertyui','jennifer','player','flyers','suckit','danielle',
    'hunter','sunshine','fish','gregory','beaver','fuck','morgan','porn','buddy','4321',
    '2000','starwars','matrix','whatever','4128','test','boomer','teens','young','runner',
    'batman','cowboys','scooby','nicholas','swimming','trustno1','edward','jason','lucky','dolphin',
    'thomas','charles','walter','helpme','gordon','tigger','girls','cumshot','jackie','casper',
    'robert','booboo','boston','monica','stupid','access','coffee','braves','midnight','shit',
    'love','xxxxxx','yankee','college','saturn','buster','bulldog','lover','baby','gemini',
    '1234567','ncc1701','barney','cunt','apples','soccer','rabbit','victor','brian','august',
    'hockey','peanut','tucker','mark','3333','killer','john','princess','startrek','canada',
    'george','johnny','mercedes','sierra','blazer','sexy','gandalf','5150','leather','cumming',
    'andrew','spanky','doggie','232323','hunting','charlie','winter','zzzzzz','4444','kitty',
    'superman','brandy','gunner','beavis','rainbow','asshole','compaq','horney','bigcock','112233',
    'fuckyou','carlos','bubba','happy','arthur','dallas','tennis','2112','sophie','cream',
    'jessica','james','fred','ladies','calvin','panties','mike','johnson','naughty','shaved',
    'pepper','brandon','xxxxx','giants','surfer','1111','fender','tits','booty','samson',
    'austin','anthony','member','blonde','kelly','william','blowme','boobs','fucked','paul',
    'daniel','ferrari','donald','golden','mine','golfer','cookie','bigdaddy','0','king',
    'summer','chicken','bronco','fire','racing','heather','maverick','penis','sandra','5555',
    'hammer','chicago','voyager','pookie','eagle','yankees','joseph','rangers','packers','hentai',
    'joshua','diablo','birdie','einstein','newyork','maggie','sexsex','trouble','dolphins','little',
    'biteme','hardcore','white','0','redwings','enter','666666','topgun','chevy','smith',
    'ashley','willie','bigtits','winston','sticky','thunder','welcome','bitches','warrior','cocacola',
    'cowboy','chris','green','sammy','animal','silver','panther','super','slut','broncos',
    'richard','yamaha','qazwsx','8675309','private','fucker','justin','magic','zxcvbnm','skippy',
    'orange','banana','lakers','nipples','marvin','merlin','driver','rachel','power','blondes',
    'michelle','marine','slayer','victoria','enjoy','corvette','angels','scott','asdfgh','girl',
    'bigdog','fishing','2222','vagina','apollo','cheese','david','asdf','toyota','parker',
    'matthew','maddog','video','travis','qwert','121212','hooters','london','hotdog','time',
    'patrick','wilson','7777','paris','sydney','martin','butthead','marlboro','rock','women',
    'freedom','dennis','srinivas','xxxx','voodoo','ginger','fucking','internet','extreme','magnum',
    'blowjob','captain','action','redskins','juice','nicole','bigdick','carter','erotic','abgrtyu',
    'sparky','chester','jasper','dirty','777777','yellow','smokey','monster','ford','dreams',
    'camaro','xavier','teresa','freddy','maxwell','secret','steven','jeremy','arsenal','music',
    'dick','viking','11111111','access14','rush2112','falcon','snoopy','bill','wolf','russia',
    'taylor','blue','crystal','nipple','scorpion','111111','eagles','peter','iloveyou','rebecca',
    '131313','winner','pussies','alex','tester','123123','samantha','cock','florida','mistress',
    'bitch','house','beer','eric','phantom','hello','miller','rocket','legend','billy',
    'scooter','flower','theman','movie','6666','please','jack','oliver','success','albert'
  );
}

/**
 * validateAgainstBlocklist
 * validates $password against list of blocked strings
 *
 * @param  string $password
 * @return boolean
 */
function validateAgainstBlocklist($password)
{
  foreach (getPwBlockList() as $pwBlock)
  {
    if ($password === $pwBlock)
      return FALSE;
  }

  return TRUE;
}


/**
 * validateStreetAddress
 * validate a USA street address via smartystreets.com
 * validation is considered successfull when a single address is returned without any errors,
 * however empty result (no matches) cannot be fully trusted as this service fails without errors on incomplete addresses (e.g. missing Suite or Apartment)
 * @param Object or Array street address
 * @returns array(Array of address Objects that matched or NULL on failure, String NULL or error message on failure)
 */
function validateStreetAddress($address)
{
  logInfo('validating address: ' . jsonEncode($address));

  try
  {
    $matches = array();
    $error = NULL;

    // get credentials: smartystreets's lingo "secret key"
    $auth_id = find_credential('ultra/smarty/key');
    $auth_token = find_credential('ultra/smarty/token');
    if ( ! $auth_id || ! $auth_token)
      throw new \Exception('Environment configuration error');

    // check minimum required parameters
    $address = (object)$address;
    if (empty($address->street) || empty($address->city) || empty($address->state))
      throw new \Exception('Missing required parameters');

    // prepare HTTP query
    if ( ! $query = http_build_query(array(
      'auth-id'     => $auth_id,
      'auth-token'  => $auth_token,
      'street'      => $address->street,
      'secondary'   => empty($address->suite) ? NULL : $address->suite,
      'city'        => $address->city,
      'state'       => $address->state,
      'zipcode'     => empty($address->zipcode) ? NULL : $address->zipcode)))
      throw new \Exception('Failed to prepare address verification provider request');

    // execute request to Smarty Streets
    $response = file_get_contents("https://api.smartystreets.com/street-address?$query");
    $result = json_decode($response); // this may throw an Exception on invalid JSON when API fails
    logInfo('smartystreets retured ' . count($result) . " matches: $response");

    // check for invalid API response
    if ( ! is_array($result))
      throw new \Exception('Invalid address verification provider response');

    // check for no matches
    if ( ! count($result))
      throw new \Exception('Address does not exist');

    // collect matches into function response
    foreach ($result as $address)
    {
      $components = $address->components;
      $match = new \StdClass;
      $match->text = "{$address->delivery_line_1} {$address->last_line}";  // human readable entire address for UI display
      $match->street = 
        (empty($components->primary_number) ? NULL : "{$components->primary_number} ") .
        (empty($components->street_predirection) ? NULL : "{$components->street_predirection} ") .
        (empty($components->street_name) ? NULL : "{$components->street_name} ") .
        (empty($components->street_suffix) ? NULL : "{$components->street_suffix} ") .
        (empty($components->street_postdirection) ? NULL : $components->street_postdirection);
      $match->suite = 
        (empty($components->secondary_designator) ? NULL : "{$components->secondary_designator} ") .
        (empty($components->secondary_number) ? NULL : $components->secondary_number);
      $match->city = $components->city_name;
      $match->state = $components->state_abbreviation;
      $match->zipcode = $components->zipcode;
      $match->zip_plus = $components->plus4_code;
      $match->latitude = $address->metadata->latitude;
      $match->longitude = $address->metadata->longitude;
      $matches[] = $match;
    }
    if (count($matches) > 1)
      $error = 'Multiple matching addresses found';
  }
  catch(\Exception $e)
  {
    logError('EXCEPTION: ' . $e->getMessage());
    $error = $e->getMessage();
  }

  return array($matches, $error);
}


/**
 * validateProfanity
 *
 * @return boolean
 */
function validateProfanity( $string )
{
  // TODO:
  $wordsArr = array(
    'anal',
    'anus',
    'ass',
    'bastard',
    'bitch',
    'boob',
    'cock',
    'cum',
    'cunt',
    'dick',
    'dildo',
    'dyke',
    'fag',
    'faggot',
    'fuck',
    'fuk',
    'handjob',
    'homo',
    'jizz',
    'kike',
    'kunt',
    'muff',
    'nigger',
    'penis',
    'piss',
    'poop',
    'pussy',
    'queer',
    'rape',
    'semen',
    'sex',
    'shit',
    'slut',
    'titties',
    'twat',
    'vagina',
    'vulva',
    'wank'
  );

  return in_array( $string, $wordsArr );
}

/**
 * validateICCIDBrandId
 *
 * Checks if the given $iccid belongs to the given $brand_id
 *
 * @param  String  $iccid
 * @param  Integer $brand_id
 * @return boolean
 */
function validateICCIDBrandId( $iccid , $brand_id )
{
  if ( empty( $brand_id ) )
    return FALSE;

  // load ICCID info from DB
  $sim_brand_id = \get_brand_id_from_iccid( $iccid );

  return ! ! ( $sim_brand_id && ( $brand_id == $sim_brand_id ) );
}

/**
 * validateICCIDBrandName
 *
 * Checks if the given $iccid belongs to the given $brand_name
 *
 * @param  String  $iccid
 * @param  String  $brand_name (Mint, Univision, ...)
 * @return boolean
 */
function validateICCIDBrandName( $iccid , $brand_name )
{
  if ( empty( $brand_name ) )
    return FALSE;

  $brand_id = \Ultra\UltraConfig\getBrandIdFromBrandName( $brand_name );

  if ( empty( $brand_id ) )
    return FALSE;

  return validateICCIDBrandId( $iccid , $brand_id );
}

/**
 * validateICCIDBrand
 *
 * Checks if the given $iccid belongs to the given $brand
 *
 * @param  String  $iccid
 * @param  String  $brand (MINT, ULTRA, ...)
 * @return boolean
 */
function validateICCIDBrand( $iccid , $brand )
{
  if ( empty( $brand ) )
    return FALSE;

  $brand_id = \Ultra\UltraConfig\getBrandIdFromBrand( $brand );

  if ( empty( $brand_id ) )
    return FALSE;

  return validateICCIDBrandId( $iccid , $brand_id );
}

/**
 * validateMSISDNBrand
 *
 * Checks if the given $msisdn belongs to the given $brand
 *
 * @param  String  $msisdn
 * @param  String  $brand (MINT, ULTRA, ...)
 * @return boolean
 */
function validateMSISDNBrand( $msisdn , $brand )
{
  if ( empty( $brand ) )
    return FALSE;

  $brand_id = \Ultra\UltraConfig\getBrandIdFromBrand( $brand );

  if ( empty( $brand_id ) )
    return FALSE;

  return validateMSISDNBrandId( $msisdn , $brand_id );
}

/**
 * validateMSISDNBrandId
 *
 * Checks if the given $msisdn belongs to the given $brand_id
 *
 * @param  String  $msisdn
 * @param  Integer $brand_id
 * @return boolean
 */
function validateMSISDNBrandId( $msisdn , $brand_id )
{
  if ( empty( $brand_id ) )
    return FALSE;
  
  // load MSISDN info from DB
  $msisdn_brand_id = \get_brand_id_from_msisdn( $msisdn );
  
  return ! ! ( $msisdn_brand_id && ( $brand_id == $msisdn_brand_id ) );
}

/**
 * validateMintBrandId
 *
 * @return boolean
 */
function validateMintBrandId( $brand_id )
{
  $brand_mint = \Ultra\UltraConfig\getBrandIdFromBrand( 'MINT' );

  return ! ! ( $brand_mint == $brand_id );
}

/**
 * validateUltraBrandId
 *
 * @return boolean
 */
function validateUltraBrandId( $brand_id )
{
  $brand_ultra = \Ultra\UltraConfig\getBrandIdFromBrand( 'ULTRA' );

  return ! ! ( $brand_ultra == $brand_id );
}

/**
 * validateUnivisionBrandId
 *
 * @return boolean
 */
function validateUnivisionBrandId( $brand_id )
{
  $brand_univision = \Ultra\UltraConfig\getBrandIdFromBrand( 'UNIVISION' );

  return ! ! ( $brand_univision == $brand_id );
}


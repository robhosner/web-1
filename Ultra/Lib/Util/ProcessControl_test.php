<?php

/**
 * tests for Ultra/Lib/Util/ProcessControl class
 *
 * usage:
 * php Ultra/Lib/Util/ProcessControl_test.php getCpuLoad
 * php Ultra/Lib/Util/ProcessControl_test.php getMemoryUsage
 * php Ultra/Lib/Util/ProcessControl_test.php getFreeMemory
 * php Ultra/Lib/Util/ProcessControl_test.php getDuplicateProcesses
 * php Ultra/Lib/Util/ProcessControl_test.php getProcessAge [$PID]
 * php Ultra/Lib/Util/ProcessControl_test.php terminateProcess [$PID]
 *
 */

require_once 'db.php';
require_once 'Ultra/Lib/Util/ProcessControl.php';

if (empty($argv[1]))
  die("no test name given \n");

$test = 'test_' . $argv[1];
if ( ! is_callable($test))
  die("unknown test function {$argv[1]} \n");

echo "executing $test\n";
$test($argv);


function test_getDuplicateProcesses($argv)
{
  // no duplicates test: we are the only process
  $process = new \Ultra\Lib\Util\ProcessControl(TRUE);
  $dups = $process->getDuplicateProcesses();
  echo "found " . count($dups) . " processes, 0 expected \n";

  // now let's fork a duplicate process
  $pid = pcntl_fork();
  if ($pid == -1)
    die("failed to fork child process \n");
  elseif ($pid) // we are parent: look for duplicate child process
  {
    $dups = $process->getDuplicateProcesses();
    echo "found " . count($dups) . " processes, 1 expected, dupi \n";
    if (count($dups))
      echo "dup process ID " . $dups[0] . ", expected $pid \n";
  }
  else // we are child: sleep and let the parent do the work
    sleep(1);
}


function test_getProcessAge($argv)
{
  $process = new \Ultra\Lib\Util\ProcessControl(TRUE);
  $pid = empty($argv[2]) ? getmypid() : $argv[2];
  $age = $process->getProcessAge($pid);
  echo "process $pid is $age sec old \n";
}


function test_terminateProcess($argv)
{
  $process = new \Ultra\Lib\Util\ProcessControl(TRUE);
  if (empty($argv[2]))
  {
    // fork another process and put it to sleep so that we can terminate it
    $pid = pcntl_fork();
    if ($pid == -1)
      die("failed to fork child process \n");
    if ( ! $pid)
    {
      echo "i am a sad child process " . getmypid() . ", waiting to be killed by my parent\n";
      sleep(10);
    }
  }
  $age = $process->getProcessAge($pid);
  echo "i am parent: process $pid is $age sec old, attempting to terminate \n";
  $result = $process->terminateProcess($pid);
}


function test_getCpuLoad($argv)
{
  $process = new \Ultra\Lib\Util\ProcessControl(TRUE);
  $load = $process->getCpuLoad();
  echo "CPU load is $load% \n";
}


function test_getMemoryUsage($argv)
{
  $process = new \Ultra\Lib\Util\ProcessControl(TRUE);
  $usage = $process->getMemoryUsage();
  echo "RAM usage: $usage \n";
}


function test_getFreeMemory($argv)
{
  $process = new \Ultra\Lib\Util\ProcessControl(TRUE);
  $usage = $process->getFreeMemory();
  echo "Free RAM: $usage \n";
}


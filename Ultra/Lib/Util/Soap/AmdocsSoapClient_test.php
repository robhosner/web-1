<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Soap/AmdocsSoapClient.php';
require_once 'Ultra/Lib/Util/Soap/SendSMSPayload.php';
require_once 'Ultra/Lib/Util/Soap/SendSMSResponse.php';

$sms = new SendSMSPayload('9499032013', 'en', 'Testing');
$payload = $sms->getPayload();

$amdocs = new AmdocsSoapClient(FALSE, 'this-is-a-test');
$response = $amdocs->call('SendSMS', $payload);

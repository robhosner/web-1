#!/usr/bin/perl


use strict;
use warnings;


use Data::Dumper;
#use JSON::XS;
use Ultra::Lib::Util::Config;
use Ultra::Lib::Util::Redis;

my $token = $ARGV[0];

#my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

my $config = Ultra::Lib::Util::Config->new();

my $ultraRedis = Ultra::Lib::Util::Redis->new( CONFIG => $config );

my $info = $ultraRedis->monitorInfo();

#print STDOUT $json_coder->encode( $info );

if ( $token eq 'ALL' )
{
  my ($sec,$min,$hr,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);

  my $now = sprintf "%04d-%02d-%02d %02d:%02d:%02d", ($year + 1900, ++$mon, $mday, $hr, $min, $sec);

  for my $token( qw/blocked_clients connected_clients dbsize mem_fragmentation_ratio used_cpu_sys used_cpu_user_children used_memory used_memory_human used_memory_peak_human used_memory_rss/ )
  {
    print "$now $token ".$info->{ $token }."\n";
  }

  exit;
}

print $info->{ $token }."\n";

__END__

Examples:

{"last_save_time":"1385407640","bgsave_in_progress":"0","vm_enabled":"0","uptime_in_seconds":"12894005","total_connections_received":"18404035","used_memory_rss":"9650176","used_cpu_sys":"9169.79","redis_git_dirty":"0","loading":"0","latest_fork_usec":"711","connected_clients":"22","used_memory_peak_human":"2.22M","pubsub_patterns":"0","mem_allocator":"jemalloc-2.2.5","uptime_in_days":"149","keyspace_hits":"3919872","client_biggest_input_buf":"0","gcc_version":"4.4.6","changes_since_last_save":"42","arch_bits":"64","lru_clock":"128751","role":"master","db0":"keys=1256,expires=1227","multiplexing_api":"epoll","pubsub_channels":"0","redis_git_sha1":"00000000","used_cpu_user_children":"29.37","dbsize":"1256","redis_version":"2.4.10","process_id":"1646","used_memory_human":"1.31M","keyspace_misses":"243364347","used_cpu_user":"4962.30","total_commands_processed":"273143922","mem_fragmentation_ratio":"7.01","used_memory":"1375776","client_longest_output_list":"0","blocked_clients":"0","aof_enabled":"0","evicted_keys":"0","bgrewriteaof_in_progress":"0","expired_keys":"33855","used_memory_peak":"2332232","connected_slaves":"0","used_cpu_sys_children":"85.33"}

perl Ultra/Lib/Util/Redis_monitor.pl blocked_clients
perl Ultra/Lib/Util/Redis_monitor.pl changes_since_last_save
perl Ultra/Lib/Util/Redis_monitor.pl connected_clients
perl Ultra/Lib/Util/Redis_monitor.pl dbsize
perl Ultra/Lib/Util/Redis_monitor.pl mem_fragmentation_ratio
perl Ultra/Lib/Util/Redis_monitor.pl used_cpu_sys
perl Ultra/Lib/Util/Redis_monitor.pl used_cpu_user_children
perl Ultra/Lib/Util/Redis_monitor.pl used_memory
perl Ultra/Lib/Util/Redis_monitor.pl used_memory_human
perl Ultra/Lib/Util/Redis_monitor.pl used_memory_peak_human
perl Ultra/Lib/Util/Redis_monitor.pl used_memory_rss

sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli3_dev HTT_CONFIGROOT=/home/ht/config/rgalli3_dev DOCUMENT_ROOT=/home/ht/www/rgalli3_dev/web ./Ultra/Lib/Util/Redis_monitor.pl dbsize'


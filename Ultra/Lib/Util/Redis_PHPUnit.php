<?php

require_once 'lib/util-common.php';
require_once 'Ultra/Lib/Util/Redis.php';

/**
 * PHPUnit tests for Ultra/Lib/Util/Redis.php
 */
class RedisTest extends PHPUnit_Framework_TestCase
{
  private $redisObj;
  private $testField = 'test_field';
  private $testNonexistantField = 'test_nonexistant_field';

  public function setUp()
  {
    $this->redisObj = new \Ultra\Lib\Util\Redis( false );

    $this->redisObj->del( $this->testField );
    $this->redisObj->del( $this->testNonexistantField );
  }

  public function test__setAndGet()
  {
    // failures
    $nonexistantFields = array( 'a1'.time() , 'z1'.time() , 'hhhh1'.time() , 'iiii1'.time() , 'qqqq1'.time() );
    foreach( $nonexistantFields as $field )
    {
      $result = $this->redisObj->get( $field );
      $this->assertNull( $result );
    }

    // set/get test
    $num = rand();
    $result = $this->redisObj->set( $this->testField, $num );
    $this->assertTrue( $result );

    $result = $this->redisObj->get( $this->testField );
    $this->assertEquals( $result, $num );

    // test TTL
    $num = rand();
    $result = $this->redisObj->set( $this->testField, $num, 2 );
    $this->assertTrue( $result );

    $result = $this->redisObj->get( $this->testField );
    $this->assertEquals( $result, $num );

    sleep( 3 );

    $result = $this->redisObj->get( $this->testField );
    $this->assertNull( $result );
  }

  public function test__setnx()
  {
    // failure
    $result = $this->redisObj->get( $this->testNonexistantField );
    $this->assertNull( $result );

    // successful
    $num = rand();
    $result = $this->redisObj->setnx( $this->testNonexistantField, $num );
    $this->assertTrue( $result );

    $result = $this->redisObj->get( $this->testNonexistantField );
    $this->assertEquals( $result, $num );

    $this->redisObj->del( $this->testField );

    // test TTL
    $num = rand();
    $result = $this->redisObj->setnx( $this->testField, $num, 2 );
    $this->assertTrue( $result );

    $result = $this->redisObj->get( $this->testField );
    $this->assertEquals( $result, $num );

    sleep( 3 );

    $result = $this->redisObj->get( $this->testField );
    $this->assertNull( $result );

    // fail
    $result = $this->redisObj->setnx( $this->testNonexistantField, $num + 1 );
    $this->assertFalse( $result );

    // cleanup
    $result = $this->redisObj->del( $this->testNonexistantField );
    $this->assertEquals( $result, 1 );
  }

  public function test__expire()
  {
    // set it
    $num = rand();
    $result = $this->redisObj->set( $this->testField, $num );
    $this->assertTrue( $result );

    $result = $this->redisObj->get( $this->testField );
    $this->assertEquals( $result, $num );

    // expire it
    $result = $this->redisObj->expire( $this->testField, 2 );
    $this->assertTrue( $result );

    sleep( 3 );

    $result = $this->redisObj->get( $this->testField );
    $this->assertNull( $result );
  }

  public function test__incr_or_set()
  {
    // non-existant
    $result = $this->redisObj->incr_or_set( $this->testField );
    $this->assertTrue( $result );

    $result = $this->redisObj->get( $this->testField );
    $this->assertEquals( $result, 1 );

    // increment
    $result = $this->redisObj->incr_or_set( $this->testField );
    $this->assertEquals( $result, 2 );

    $result = $this->redisObj->get( $this->testField );
    $this->assertEquals( $result, 2 );

    $this->redisObj->del( $this->testField );

    // test TTL
    $result = $this->redisObj->incr_or_set( $this->testField, 2 );
    $this->assertTrue( $result );

    $result = $this->redisObj->get( $this->testField );
    $this->assertEquals( $result, 1 );

    sleep( 3 );

    $result = $this->redisObj->get( $this->testField );
    $this->assertNull( $result );
  }

  public function test__incr()
  {
    // failure (wrong type)
    $result = $this->redisObj->set( $this->testField, 'string' );
    $this->assertTrue( $result );

    $result = $this->redisObj->incr( $this->testField );
    $this->assertFalse( $result );

    $this->redisObj->del( $this->testField );

    // increment
    $result = $this->redisObj->incr( $this->testField );
    $this->assertEquals( $result, 1 );

    $result = $this->redisObj->get( $this->testField );
    $this->assertEquals( $result, 1 );

    $result = $this->redisObj->incr( $this->testField );
    $this->assertEquals( $result, 2 );

    $result = $this->redisObj->get( $this->testField );
    $this->assertEquals( $result, 2 );
  }

  public function test__saddAndSrem()
  {
    // failure
    $result = $this->redisObj->srem( $this->testField, 'string' );
    $this->assertEquals( $result, 0 );

    // success
    $result = $this->redisObj->sadd( $this->testField, 'string1' );
    $this->assertEquals( $result, 1 );

    $result = $this->redisObj->smembers( $this->testField );
    $expectedArr = array( 'string1' );
    $this->assertTrue( array_diff( $expectedArr, $result ) === array_diff( $result, $expectedArr ) );

    $result = $this->redisObj->sadd( $this->testField, 'string2' );
    $this->assertEquals( $result, 1 );

    $result = $this->redisObj->smembers( $this->testField );
    $expectedArr = array( 'string1', 'string2' );
    $this->assertTrue( array_diff( $expectedArr, $result ) === array_diff( $result, $expectedArr ) );

    $result = $this->redisObj->srem( $this->testField, 'string1' );
    $this->assertEquals( $result, 1 );

    $result = $this->redisObj->smembers( $this->testField );
    $expectedArr = array( 'string2' );
    $this->assertTrue( array_diff( $expectedArr, $result ) === array_diff( $result, $expectedArr ) );
  }

  public function test__hsetAndHget()
  {
    $this->redisObj->hdel( 'test_hash', 'key1' );
    $this->redisObj->hdel( 'test_hash', 'key2' );

    // failure
    $result = $this->redisObj->hget( 'test_hash', $this->testNonexistantField );
    $this->assertFalse( ! ! $result );

    // success
    $result = $this->redisObj->hset( 'test_hash', 'key1', 'val1' );
    $this->assertEquals( $result, 1 );
    $result = $this->redisObj->hget( 'test_hash', 'key1' );
    $this->assertEquals( $result, 'val1' );

    $result = $this->redisObj->hset( 'test_hash', 'key2', 'val2' );
    $this->assertEquals( $result, 1 );
    $result = $this->redisObj->hget( 'test_hash', 'key2' );
    $this->assertEquals( $result, 'val2' );

    $result = $this->redisObj->hset( 'test_hash', 'key1', 'new_val' );
    $this->assertEquals( $result, 0 );
    $result = $this->redisObj->hget( 'test_hash', 'key1' );
    $this->assertEquals( $result, 'new_val' );
  }

  public function test__hmsetAndHmget()
  {
    $this->redisObj->hdel( 'test_hash', 'key1' );
    $this->redisObj->hdel( 'test_hash', 'key2' );

    $result = $this->redisObj->hmset( 'test_hash', array( 'key1' => 'val1', 'key2' => 'val2' ) );
    $this->assertTrue( ! ! $result );
    
    $result = $this->redisObj->hmget( 'test_hash', array( 'key1', 'key2' ) );
    $this->assertEquals( $result, array( 'key1' => 'val1', 'key2' => 'val2' ) );
  }

  public function test_hgetall()
  {
    $this->redisObj->hdel( 'test_hash', 'key1' );
    $this->redisObj->hdel( 'test_hash', 'key2' );

    // empty
    $result = $this->redisObj->hgetall( 'test_hash' );
    $this->assertTrue( empty( $result ) );

    // success
    $result = $this->redisObj->hset( 'test_hash', 'key1', 'val1' );
    $this->assertEquals( $result, 1 );
    $result = $this->redisObj->hget( 'test_hash', 'key1' );
    $this->assertEquals( $result, 'val1' );

    $result = $this->redisObj->hset( 'test_hash', 'key2', 'val2' );
    $this->assertEquals( $result, 1 );
    $result = $this->redisObj->hget( 'test_hash', 'key2' );
    $this->assertEquals( $result, 'val2' );

    $result = $this->redisObj->hgetall( 'test_hash' );
    $expectedArr = array( 'key1' => 'val1', 'key2' => 'val2' );
    $this->assertEquals( $result, $expectedArr );

    // hlen
    $result = $this->redisObj->hlen( 'test_hash' );
    $this->assertEquals( $result, 2 );

    // hkeys
    $result = $this->redisObj->hkeys( 'test_hash' );
    $expectedArr = array( 'key1', 'key2' );
    $this->assertTrue( array_diff( $expectedArr, $result ) === array_diff( $result, $expectedArr ) );
  }

  public function test__zpop()
  {
    // create list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    // check list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zpop( $this->testField );
      $this->assertEquals( $result, $i );
    }
  }

  public function test__zhead_list()
  {
    // create list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    // check list (chunks of 10)
    for ( $i = 0; $i < 50; $i = $i + 10 )
    {
      $result = $this->redisObj->zhead_list( $this->testField, 10, $i );
      for ( $j = 0; $j < 10; $j++ )
      {
        $this->assertEquals( $result[$j], $i + $j );
      }
    }
  }

  public function test__zcard()
  {
    // create list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    $result = $this->redisObj->zcard( $this->testField );
    $this->assertEquals( $result, 50 );
  }

  public function test__zcount()
  {
    // create list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zcount( $this->testField, 0, $i );
      $this->assertEquals( $result, $i+1 );
    }
  }

  public function test__zrank()
  {
    // create list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zrank( $this->testField, $i );
      $this->assertEquals( $result, $i );
    }
  }

  public function test__zrem()
  {
    // create list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    // success
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zrem( $this->testField, $i );
      $this->assertEquals( $result, 1 );
    }

    // fail
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zrem( $this->testField, $i );
      $this->assertEquals( $result, 0 );
    }
  }

  public function test__zremrangebyrank()
  {
    // create list
    for ( $i = 1; $i <= 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    // success (chunks of 10)
    for ( $i = 0; $i < 5; $i++ )
    {
      $result = $this->redisObj->zremrangebyrank( $this->testField, 0, 9 );
      $this->assertEquals( $result, 10 );
    }
  }
  
  public function test__zremrangebyscore()
  {
    // create list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    // success (chunks of 10)
    for ( $i = 0; $i < 50; $i = $i + 10 )
    {
      $result = $this->redisObj->zremrangebyscore( $this->testField, $i, $i + 9 );
      $this->assertEquals( $result, 10 );
    }
  }

  public function test__zrevrange()
  {
    // create list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    // success (chunks of 10)
    for ( $i = 0; $i < 50; $i = $i + 10 )
    {
      $result = $this->redisObj->zrevrange( $this->testField, $i, $i + 9 );
      for ( $j = 0; $j < 10; $j++ )
      {
        $this->assertEquals( $result[$j], 49 - ($j + $i) );
      }
    }
  }

  public function test__zrevrangebyscore()
  {
    // create list
    for ( $i = 1; $i <= 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    // success (chunks of 10)
    for ( $i = 50; $i > 0; $i = $i - 10 )
    {
      $result = $this->redisObj->zrevrangebyscore( $this->testField, $i, $i - 9 );
      for ( $j = 0; $j < 10; $j++ )
      {
        $this->assertEquals( $result[$j], $i - $j );
      }
    }
  }

  public function test__zrevrank()
  {
    // create list
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zadd( $this->testField, $i, $i );
      $this->assertEquals( $result, 1 );
    }

    // success (chunks of 10)
    for ( $i = 0; $i < 50; $i++ )
    {
      $result = $this->redisObj->zrevrank( $this->testField, $i );
      $this->assertEquals( $result, 49 - $i );
    }
  }

  public function test__llen()
  {
    $nonexistantFields = array( 'a'.time() , 'z'.time() , 'hhhh'.time() , 'iiii'.time() , 'qqqq'.time() );

    // test non existant fields
    foreach( $nonexistantFields as $field )
    {
      $result = $this->redisObj->llen( $field );
      $this->assertEquals( $result, 0 );
    }

    // not a list
    $result = $this->redisObj->set( $this->testField, 'string' );
    $this->assertTrue( $result );
    $result = $this->redisObj->llen( $this->testField );
    $this->assertFalse( ! ! $result );

    $this->redisObj->del( $this->testField );

    // success
    for ( $i = 1; $i <= 20; $i++ )
    {
      $result = $this->redisObj->lpush( $this->testField, $i );
      $this->assertEquals( $result, $i );

      $result = $this->redisObj->llen( $this->testField );
      $this->assertEquals( $result, $i );
    }
  }

  public function test__lpopAndLpush()
  {
    // push
    for ( $i = 1; $i <= 50; $i++ )
    {
      $result = $this->redisObj->lpush( $this->testField, $i );
      $this->assertEquals( $result, $i );
    }

    // pop
    for ( $i = 50; $i > 0; $i-- )
    {
      $result = $this->redisObj->lpop( $this->testField );
      $this->assertEquals( $result, $i );
    }
  }

  public function test__rpopAndRpush()
  {
    // push
    for ( $i = 1; $i <= 50; $i++ )
    {
      $result = $this->redisObj->rpush( $this->testField, $i );
      $this->assertEquals( $result, $i );
    }

    // pop
    for ( $i = 50; $i > 0; $i-- )
    {
      $result = $this->redisObj->rpop( $this->testField );
      $this->assertEquals( $result, $i );
    }
  }

  public function test__lrange()
  {
    // push
    for ( $i = 1; $i <= 50; $i++ )
    {
      $result = $this->redisObj->rpush( $this->testField, $i );
      $this->assertEquals( $result, $i );
    }

    // chunks of 10
    for ( $i = 0; $i < 50; $i = $i +10 )
    {
      $result = $this->redisObj->lrange( $this->testField, $i, $i+9 );
      for ( $j = 0; $j < 10; $j++ )
      {
        $this->assertEquals( $result[$j], $i + $j + 1 );
      }
    }
  }
}

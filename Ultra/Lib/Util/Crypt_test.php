<?php

require_once 'Ultra/Lib/Util/Crypt.php';

$key    = 'UltraKey1';
$data   = '310260840773384';
$secret = 'GyN+Mike9iDrJvvDnMrX';


// test symmetric encryption
$encrypted = \Ultra\Lib\Util\Crypt\encodeRC4($key, $data);
$decrypted = \Ultra\Lib\Util\Crypt\decodeRC4($key, $encrypted);
echo "data: $data -> encrypted: $encrypted -> decrypted: $decrypted \n";

// test decryption data on sample TMO data
$decrypted = \Ultra\Lib\Util\Crypt\decodeRC4($key, $secret);
$encrypted = \Ultra\Lib\Util\Crypt\encodeRC4($key, $decrypted);
echo "encrypted: $secret -> data: $decrypted -> encrypted $encrypted \n";

$enc = sha1($data);
if (\Ultra\Lib\Util\Crypt\checkSha1($data, $enc))
  echo 'checkSha1: success';
else
  echo 'checkSha1: failure';
echo PHP_EOL;

if (\Ultra\Lib\Util\Crypt\checkSha1($data, 'fail'))
  echo 'checkSha1 controlled failure: failure';
else
  echo 'checkSha1 controlled failure: success';
echo PHP_EOL;

?>

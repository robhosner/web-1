<?php

require_once 'Ultra/Lib/Util/SQLite.php';

$db_name = 'SQLite_test.db';
$table_name = 'people';

try
{
  $db = new Ultra\Lib\Util\SQLite($db_name);
  if (file_exists($db_name)) test_drop_table($db, $table_name);
  test_create_table($db, $table_name);
  test_truncate_table($db, $table_name);
  test_insert_rows($db, $table_name);
  test_select_all_fields_all_rows($db, $table_name);
  test_update_table($db, $table_name, array(array('field'=>'first_name','value'=>'Chinchilla')),array(array('field'=>'first_name','operator'=>'=','comparison'=>'First 0')));
  test_select_row($db, $table_name, array(array('field'=>'first_name','operator'=>'=','comparison'=>'Chinchilla')));
  test_delete_row($db, $table_name);
  test_select_row($db, $table_name, array(array('field'=>'first_name','operator'=>'=','comparison'=>'Chinchilla')));
  cleanup($db_name);
}
catch (\Exception $e)
{
  echo "Unexpected error : " . $e->getMessage() . PHP_EOL;
  echo $e->getFile() . ": " . $e->getLine() . PHP_EOL;
  echo $e->getTraceAsString() . PHP_EOL;
  if (isset($db)) echo "SQLite error: code ({$db->getLastErrorCode()}), message ({$db->getLastErrorMessage()}), e: {$e->getMessage()}" . PHP_EOL;
  echo "Query: " . $db->getLastQuery() . PHP_EOL;
  cleanup($db_name);
}

function test_drop_table($db, $table_name)
{
  $result = $db->dropTable($table_name);
  echo "Query: " . $db->getLastQuery() . PHP_EOL;
  echo "Response: " . print_r($result, true) . PHP_EOL;
}

function test_create_table($db, $table_name)
{
  $definition = array(
    array(
      'name' => 'first_name',
      'type' => 'STRING'
    ),
    array(
      'name' => 'last_name',
      'type' => 'STRING'
    )
  );
  $result = $db->createTable($table_name, $definition);
  echo "Query: " . $db->getLastQuery() . PHP_EOL;
  echo "Response: " . print_r($result, true) . PHP_EOL;
}

function test_truncate_table($db, $table_name)
{
  $result = $db->truncateTable($table_name);
  echo "Query: " . $db->getLastQuery() . PHP_EOL;
  echo "Response: " . print_r($result, true) . PHP_EOL;
}

function test_insert_rows($db, $table_name)
{
  for ($i = 0; $i < 10; $i++)
  {
    $parameters = array(
      array(
        'name'  => ':first_name',
        'value' => "First {$i}",
        'type'  => \SQLITE3_TEXT
      ),
      array(
        'name'  => ':last_name',
        'value' => "Last {$i}",
        'type'  => \SQLITE3_TEXT
      )
    );
    $result = $db->insertRow($table_name, $parameters);
    echo "Query: " . $db->getLastQuery() . PHP_EOL;
    echo "Response: " . print_r($result, true) . PHP_EOL;
  }
}

function test_select_all_fields_all_rows($db, $table_name)
{
  $fields     = false;
  $parameters = false;
  $result = $db->select($table_name, $fields, $parameters);
  echo "Query: " . $db->getLastQuery() . PHP_EOL;
  echo "Results: " . $db->getResultCount() . PHP_EOL;
  echo "Response: ";
  echo print_r($result, true) . PHP_EOL;
  echo PHP_EOL;
}

function test_select_first_only_all_rows($db, $table_name)
{
  $fields     = array('first_name');
  $parameters = false;
  $result = $db->select($table_name, $fields, $parameters);
  echo "Query: " . $db->getLastQuery() . PHP_EOL;
  echo "Response: " . join(", ", $result) . PHP_EOL;
}

function test_update_table($db, $table_name, $fields, $where)
{
  $result = $db->update($table_name, $fields, $where);
  echo "Query: " . $db->getLastQuery() . PHP_EOL;
  echo "Results: " . $db->getResultCount() . PHP_EOL;
  echo "Response: ";
  echo print_r($result, true) . PHP_EOL;
  echo PHP_EOL;
}

function test_delete_row($db, $table_name)
{
  $where = array(
    array(
      'field'       => 'first_name',
      'operator'    => '=',
      'comparison'  => 'Chinchilla'
    )
  );
  $result = $db->delete($table_name, $where);
  echo "Query: " . $db->getLastQuery() . PHP_EOL;
  echo "Results: " . $db->getResultCount() . PHP_EOL;
  echo "Response: " .print_r($result, true) . PHP_EOL;
  echo PHP_EOL;
}

function test_select_row($db, $table_name, $parameters)
{
  $result = $db->select($table_name, false, $parameters);
  echo "Query: " . $db->getLastQuery() . PHP_EOL;
  echo "Results: " . $db->getResultCount() . PHP_EOL;
  echo "Response: " .print_r($result, true) . PHP_EOL;
  echo PHP_EOL;
}

// Cleanup
function cleanup($db_name)
{
  echo "Removing test database... ";
  if (!unlink($db_name)) {
    echo "FAILED!";
  } else {
    echo "SUCCESS!";
  }

  echo PHP_EOL;
  echo PHP_EOL;
  exit(1);
}
<?php

namespace Ultra\Lib\Util\Redis;

require_once 'db/ultra_session.php';
require_once 'Ultra/Lib/Util/Redis.php';

/**
 * Redis UltraSession class
 *
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/Sessions+Spec
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Dealer Portal
 */
class UltraSession
{
  const PREFIX = 'ultrasession/'; // Redis prefix for Ultra Sessions
  const TTL    = 1800; // 30 minutes before the Redis key expires for an Ultra Session UUID

  private $redis;

  /**
   * Class constructor
   */
  public function __construct ( $redis=NULL )
  {
    // initialize private variables

    $this->redis = ( is_null($redis) )
                   ?
                   new \Ultra\Lib\Util\Redis
                   :
                   $redis
                   ;
  }

  /**
   * addObject
   *
   * @return string
   */
  public function addObject( $params )
  {
    // supported properties and their default values
    $properties = array(
      'account'             => NULL,    // ACCOUNTS.ACCOUNT
      'business_code'       => NULL,    // MVNO/Master/Dealer code
      'business_name'       => NULL,    // MVNO/Master/Dealer name
      'customer_id'         => NULL,    // CUSTOMERS.CUSTOMER_ID
      'dealer'              => '',      // MVNO/Master/Dealer numeric ID
      'distributor'         => '',      // numeric ID
      'masteragent'         => '',      // numeric ID
      'reset_password'      => FALSE,   // temporary password used to login
      'role'                => NULL,    // BUSINESS_TYPE + ROLE_NAME
      'user_id'             => NULL,    // tblPHPUserLog.UserID
      'timestamp_creation'  => time(),
      'timestamp_lastcheck' => time()); // every $sessionExpirationSeconds seconds we should check if the session expired

    foreach ($properties as $property => $default)
      $objectArray[$property] = isset($params[$property]) ? $params[$property] : $default;

    $securityToken = $this->generateSecurityToken();

    $this->redis->set( $this::PREFIX . $securityToken , json_encode( $objectArray ) , $this::TTL );

    // associate 'customer_id' with $securityToken if this a customer session
    if ($objectArray['customer_id'])
      $this->redis->set( $this::PREFIX . 'customer_id/' . $objectArray['customer_id'] , $securityToken , $this::TTL );

    return $securityToken;
  }

  /**
   * refreshObject
   *
   * Refresh the Redis object with an updated 'timestamp_lastcheck'
   *
   * @return NULL
   */
  public function refreshObject( $securityToken , $objectArray )
  {
    $objectArray['timestamp_lastcheck'] = time();

    $this->redis->set( $this::PREFIX . $securityToken , json_encode( $objectArray ) , $this::TTL );
  }

  /**
   * shouldCheckExpiration
   *
   * @return boolean - true if the session object should be checked
   */
  public function shouldCheckExpiration( $objectArray )
  {
    $sessionExpirationSeconds = \Ultra\UltraConfig\celluphoneUltrasessionExpiration();

    if ( ! $sessionExpirationSeconds )
      $sessionExpirationSeconds = 240; // 4 minutes

    return ! ! ( $objectArray['timestamp_lastcheck'] + $sessionExpirationSeconds < time() );
  }


  /**
   * getCustomerObject
   *
   * Retrieve a customer Ultra Session Object from Redis
   *
   * @return array
   */
  public function getCustomerObject( $customer_id )
  {
    $securityToken = $this->redis->get( $this::PREFIX . 'customer_id/' . $customer_id );

    return $this->getObject( $securityToken );
  }

  /**
   * getObject
   *
   * Retrieve Ultra Session Object from Redis
   *
   * @return array
   */
  public function getObject( $securityToken=NULL )
  {
    $objectArray = NULL;

    if ( ! $securityToken )
      return $objectArray;

    $objectJSON = $this->redis->get( $this::PREFIX . $securityToken );

    if ( $objectJSON )
      $objectArray = json_decode( $objectJSON );

    return $objectArray;
  }

  /**
   * generateSecurityToken
   *
   * Generate an Ultra Session UUID
   *
   * @return string
   */
  private function generateSecurityToken( $namespace='UltraSession' )
  {
    return getNewUUID('ST', $namespace ,FALSE);
  }
}

?>

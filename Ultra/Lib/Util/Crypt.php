<?php

namespace Ultra\Lib\Util\Crypt;

require_once 'Crypt/RC4.php';

/**
 * encodeRC4
 * TMO specific function to encode data using RC4 encryption with hashed key
 * @uses pear phpseclib/Crypt_RC4
 * @see AMDOCS-70
 * @param String encryption key
 * @param String data
 * @return String encrypted stream
 */
function encodeRC4($key, $data)
{
  if ( ! $key || ! $data)
  {
    dlog('', 'either key or data is NULL');
    return NULL;
  }

  $crypter = new \Crypt_RC4();
  $crypter->setKey(pack('H*', md5($key)));
  return base64_encode($crypter->encrypt($data));
}


/**
 * decodeRC4
 * TMO specific function to decode data using RC4 encryption with a hashed key
 * @uses pear phpseclib/Crypt_RC4
 * @see AMDOCS-70
 * @param String encryption key
 * @param String data
 * @return String decrypted stream
 */
function decodeRC4($key, $data)
{
  if ( ! $key || ! $data)
  {
    dlog('', 'either key or data is NULL');
    return NULL;
  }

  $crypter = new \Crypt_RC4();
  $crypter->setKey(pack('H*', md5($key)));
  return $crypter->decrypt(base64_decode($data));
}


function checkSha1($p, $h)
{
  return ($h == sha1($p));
}

?>

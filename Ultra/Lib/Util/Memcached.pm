package Ultra::Lib::Util::Memcached;


use strict;
use warnings;


use Cache::Memcached;
use Ultra::Lib::Util::Config;


use base qw(Ultra::Lib::Util::Logger Ultra::Lib::Util::Error::Setter);


=head1 NAME

Ultra::Lib::Util::Memcached

Connection to Memcached

=head1 SYNOPSIS

  my $memcached = Ultra::Lib::Util::Memcached->new();

  print $memcached->mcGet( $token );
  print $memcached->mcSet( $token , $value , $ttl );
  print $memcached->mcIncr( $token , $value , $ttl );

=cut


sub _initialize
{
  my ($this, $data) = @_;

  $data->{ CONFIG } ||= Ultra::Lib::Util::Config->new();

  my $mcServer = $data->{ CONFIG }->envConfigValue('mc/server') . ':' . $data->{ CONFIG }->envConfigValue('mc/port');

  print STDERR "server = $mcServer\n";

  my $mcParams =
  {
    servers => [ $mcServer ],
    compress_threshold => 10000000,
    debug => 0,
  };

  $data->{ MEMCACHED } = new Cache::Memcached $mcParams;

  $this->SUPER::_initialize($data);
}


sub mcGet
{
  my $this = shift;

  return $this->{ MEMCACHED }->get( $this->{ CONFIG }->envConfigValue('mc/prefix') . shift);
}


sub mcSet
{
  my $this = shift;

  return $this->{ MEMCACHED }->set( $this->{ CONFIG }->envConfigValue('mc/prefix') . shift, shift);
}


sub mcIncr
{
  my $this = shift;

  return $this->{ MEMCACHED }->incr( $this->{ CONFIG }->envConfigValue('mc/prefix') . shift, shift);
}


1;


__END__



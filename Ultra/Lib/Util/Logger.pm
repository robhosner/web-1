package Ultra::Lib::Util::Logger;


use strict;
use warnings;


use Log::Report mode => 'VERBOSE';


# this should be decided by the &main code
#dispatcher 'FILE', 'log', mode => 'DEBUG', to => '/tmp/something.log';


sub log
{
  my ($this,@text) = @_;

  if ( $this->{ LOG_ENABLED } )
  {
    for my $t(@text)
    {
      info logging_prefix().$t;
    }
  }
}

sub logging_prefix
{
  return '';
  # return sprintf('[%s] [%s] ', scalar localtime, $$);
  # return sprintf('[%s] ', $$);
}

1;


__END__



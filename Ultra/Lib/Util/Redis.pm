package Ultra::Lib::Util::Redis;


use strict;
use warnings;


use Redis;
use Ultra::Lib::Util::Config;


use base qw(Ultra::Lib::Util::Error::Setter);


=head1 NAME

Ultra::Lib::Util::Redis

Ultra Redis utilities

=head1 SYNOPSIS

  my $config = Ultra::Lib::Util::Config->new();

  my $object = Ultra::Lib::Util::Redis->new( CONFIG => $config );

=cut


sub _initialize
{
  my ($this, $data) = @_;

  $data->{ LOG_ENABLED } = 1;

  $this->SUPER::_initialize($data);

  $this->_initializeRedis();
}


sub _initializeRedis
{
  my ($this) = @_;

  if ( ! $this->{ REDIS_OBJECT } )
  {
    my $server   = $this->{ CONFIG }->find_credential('redis/host');
    my $password = $this->{ CONFIG }->find_credential('redis/password');

    if ( $this->{ CONFIG }->find_credential('redis/cluster/hosts') )
    {
      # cluster connection

      $this->{ CONFIG }->find_credential('redis/cluster/hosts') =~ /(\d+\.\d+\.\d+\.\d+)\s/;
      $server   = $1.':6379';
      $password = 0;
      #$password = $this->{ CONFIG }->find_credential('redis/cluster/password');
    }

    my $redis_params =
    {
      server    => $server,
      reconnect => 0,
      name      => 'Ultra::Lib::Util::Redis-'.$$,
    };

    $this->{ REDIS_OBJECT } = Redis->new( %$redis_params );

    # Redis authentication
    if ( $password )
    {
      $this->{ REDIS_OBJECT }->auth( $password );
    }
  }
}

sub monitorInfo
{
  my ($this) = @_;

  my $monitorInfo = $this->{ REDIS_OBJECT }->info;

  $monitorInfo->{ dbsize } = $this->{ REDIS_OBJECT }->dbsize;

  return $monitorInfo;
}

1;

__END__


<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Hash.php';

$login_name    = 'test';
$access_number = '1111111111';

list ( $salt , $uid , $token ) = \Ultra\Lib\Util\getTokenForRefresh( $login_name , $access_number );
echo $salt          . PHP_EOL;
echo $uid           . PHP_EOL;
echo $token         . PHP_EOL;
echo strlen($token) . PHP_EOL;
echo PHP_EOL;

$login_name    = 'dummy';
$access_number = '9999999999';

list ( $salt , $uid , $token ) = \Ultra\Lib\Util\getTokenForRefresh( $login_name , $access_number );
echo $salt          . PHP_EOL;
echo $uid           . PHP_EOL;
echo $token         . PHP_EOL;
echo strlen($token) . PHP_EOL;
echo PHP_EOL;

$login_name    = 'seinfeld';

list ( $salt , $uid , $token ) = \Ultra\Lib\Util\getTokenForRefresh( $login_name , $access_number );
echo $salt          . PHP_EOL;
echo $uid           . PHP_EOL;
echo $token         . PHP_EOL;
echo strlen($token) . PHP_EOL;
echo PHP_EOL;

sleep(1);

// expecting hash mismatch
$error_code = \Ultra\Lib\Util\verifyTokenForRefresh( 'wrongloginname' , $access_number , $salt , $uid , $token );
echo 'E:'.$error_code . PHP_EOL;
echo PHP_EOL;

// expecting access_number mismatch
$error_code = \Ultra\Lib\Util\verifyTokenForRefresh( $login_name , '1' , $salt , $uid , $token );
echo 'E:'.$error_code . PHP_EOL;
echo PHP_EOL;

// expecting hash mismatch
$error_code = \Ultra\Lib\Util\verifyTokenForRefresh( $login_name , $access_number , '1' , $uid , $token );
echo 'E:'.$error_code . PHP_EOL;
echo PHP_EOL;

// expecting uid mismatch
$error_code = \Ultra\Lib\Util\verifyTokenForRefresh( $login_name , $access_number , $salt , '1' , $token );
echo 'E:'.$error_code . PHP_EOL;
echo PHP_EOL;

// expecting token is invalid
$error_code = \Ultra\Lib\Util\verifyTokenForRefresh( $login_name , $access_number , $salt , $uid , '1' );
echo 'E:'.$error_code . PHP_EOL;
echo PHP_EOL;

// expecting success
$error_code = \Ultra\Lib\Util\verifyTokenForRefresh( $login_name , $access_number , $salt , $uid , $token );
echo 'E:'.$error_code . PHP_EOL;
echo PHP_EOL;

// expecting token is expired
$token = preg_replace( '/^\d+/' , ( time() - 86401 ) , $token );
$error_code = \Ultra\Lib\Util\verifyTokenForRefresh( $login_name , $access_number , $salt , $uid , $token );
echo 'E:'.$error_code . PHP_EOL;
echo PHP_EOL;

// expecting missing parameters
$error_code = \Ultra\Lib\Util\verifyTokenForRefresh( NULL , NULL , NULL , NULL , NULL );
echo 'E:'.$error_code . PHP_EOL;
echo PHP_EOL;

exit;

$encryptedPassword = '15e58107a9c516445b1e871694b37c7afc8ec991d7d8ccf114126f534a896906vxkeOc3hEeb4A/ATLToWpyqljpshWGtLi4YiaY5gPwg=';

$givenPassword = '1422566638';
$authenticated    = \Ultra\Lib\Util\authenticatePasswordHS( $encryptedPassword , $givenPassword );

  echo "authenticated: " . (($authenticated) ? 'true' : 'false') . PHP_EOL;

$givenPassword = '142256663q';
$authenticated    = \Ultra\Lib\Util\authenticatePasswordHS( $encryptedPassword , $givenPassword );

  echo "authenticated: " . (($authenticated) ? 'true' : 'false') . PHP_EOL;

exit;

echo Ultra\Lib\Util\encryptPasswordHS( 'asndmhfskdfhsldfh' );
  echo PHP_EOL;
echo Ultra\Lib\Util\encryptPasswordHS( 'asndmhfskdfhsldfh1' );
  echo PHP_EOL;
echo Ultra\Lib\Util\encryptPasswordHS( 'asndmhfskdfhsldfh' );
  echo PHP_EOL;

exit;

function test_password_gen($password,$fail=FALSE)
{
  /******************************************
   * Generating password hashes (Create user)
   *****************************************/
  $hasher           = new Ultra\Lib\Util\Hash();
  $salt             = $hasher->generate_salt();
  $hashed_password  = $hasher->generate_password($password, $salt);
  $iterations       = $hasher->get_iterations();
  // Store info in db ($iterations, $hashed_password, $salt);

  echo "password (" . strlen($password) . "): {$password}\n";
  echo "salt (" . strlen($salt) . "): {$salt}\n";
  echo "hashed (" . strlen($hashed_password) . ") : {$hashed_password}\n";
  echo "DB value (" . strlen($salt.$hashed_password) . ") : ".$salt.$hashed_password."\n";

  /******************************************
   * Checking password hashes (Login)
   *****************************************/
  if ( $fail )
    $password.='bogus';
  $hasher           = new Ultra\Lib\Util\Hash();
  $authenticated    = $hasher->check_password($password, base64_decode($hashed_password), $salt);
  echo "authenticated: " . (($authenticated) ? 'true' : 'false') . PHP_EOL;
  echo PHP_EOL;
  echo PHP_EOL;
}

$passwords = array(
  'S0m3P4sSw013d',
  'a',
  'SuperCalifragalisticExpealidocious',
  '!@#N$/912n3(R123r'
);

foreach ($passwords as $password)
{
  test_password_gen($password);
}

test_password_gen($password,TRUE);


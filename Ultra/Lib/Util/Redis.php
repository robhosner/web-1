<?php

namespace Ultra\Lib\Util;

if (!getenv("UNIT_TESTING"))
{
  include_once('Ultra/UltraConfig.php');
}

// maintaing a global for redis connections due to performance considerations
global $redis_connection;

class Redis
{
  protected $redis;
  private $verbose;

  public function __construct( $verbose=TRUE )
  {
    global $redis_connection;

    # setting verbosity
    $this->verbose = $verbose;

    if ( empty( $redis_connection ) )
    {
      try
      {
        $redisClusterInfo = \Ultra\UltraConfig\redisClusterInfo();

        if ( empty($redisClusterInfo['name']) || ! is_array( $redisClusterInfo['hosts'] ) || ! count( $redisClusterInfo['hosts'] ) )
        {
          #\logDebug('creating new redis connection');

          # Redis PHP class - object instantiation
          $this->redis = new \Redis();

          # Redis configuration for the current environment
          list($host,$port) = explode(':',\Ultra\UltraConfig\redisHost());

          # Redis connection
          if ( ! $this->redis->connect( $host , $port ) )
            throw new \Exception("redis connection attempt failed ( host = $host , port = $port )");

          # Redis authentication
          if ( ! $this->redis->auth( \Ultra\UltraConfig\redisPassword() ) )
            throw new \Exception("redis authentication attempt failed");
        }
        else
        {
          \logDebug('creating new redis cluster connection');

          # Import predis module
          require_once '/opt/php/vendor/predis/autoload.php';

          $hosts = array();

          foreach( $redisClusterInfo['hosts'] as $host )
            $hosts[] = $host . ':6379/?timeout=60' ;

          \logDebug('hosts = '.json_encode($hosts));

          # Predis Client PHP class - object instantiation
          $this->redis = new \Predis\Client(
            $hosts,
            array( 'cluster' => $redisClusterInfo['name'] )
          );
        }
      }
      catch(\Exception $e)
      {
        dlog('', $e->getMessage());
        $this->redis = NULL;
      }

      $redis_connection = $this->redis;
    }
    else
    {
      $this->redis = $redis_connection;

      #\logDebug('using existing redis connection');
    }
  }

  public function __destruct()
  {
    #if ( ! is_null( $this->redis ) )
    #  $this->redis->close();
  }

  /**
   * hasConnection
   *
   * Returns true if redis connection exists
   *
   * @return boolean
   */
  public function hasConnection()
  {
    return !is_null( $this->redis );
  }

  /**
   * moveClusterConnection
   *
   * Move the cluster connection to another node
   *
   * @return boolean
   */
  protected function moveClusterConnection( $message )
  {
    $redisClusterInfo = \Ultra\UltraConfig\redisClusterInfo();

    preg_match( '/MOVED (\d+) ([\d\.]+)\:(\d+)/' , (string)$message , $matches , PREG_OFFSET_CAPTURE);

    if ( ! empty( $matches[2][0] ) )
    {
      $hosts   = array('tcp://' . $matches[2][0] . '/?password=' . $redisClusterInfo['password'] . '&timeout=60' );
      #$options = array("cluster",$redisClusterInfo['name']);
      $options = array("cluster" => $redisClusterInfo['name']);

      \logDebug('hosts   = '.json_encode( $hosts ));
      \logDebug('options = '.json_encode( $options ));

      $this->redis = new \Predis\Client(
        $hosts,
        $options
      );

      return ! ! is_object( $this->redis );
    }

    return FALSE;
  }

  /**
   * verbosity
   *
   * dlog verbosity
   */
  public function verbosity()
  {
    return ( $this->verbose ? '' : 'silent' );
  }

  /**
   * is_cluster
   *
   * returns TRUE if we are connected to a Redis cluster and we are using the Predis Client PHP class
   */
  public function is_cluster()
  {
    $redisClusterInfo = \Ultra\UltraConfig\redisClusterInfo();

    return ! empty($redisClusterInfo['name']);
  }

  /**
   * http://redis.io/commands/ttl
   */
  public function ttl($field)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = FALSE;
    $retry  = FALSE;

    try
    {
      $result = $this->redis->ttl($field);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->ttl($field);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/set
   */
  public function set($field,$value,$ttl=NULL)
  {
    dlog( $this->verbosity() , "field = $field ; ttl = $ttl");

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    if ( is_null($ttl) || !$ttl )
      $ttl = 60*60*24*7; // default TTL is 1 week

    $result = FALSE;
    $retry  = FALSE;

    try
    {
      $result = $this->redis->set($field,$value);
      $result = $this->redis->expire($field,$ttl);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->set($field,$value);
        $result = $this->redis->expire($field,$ttl);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/setnx
   */
  public function setnx($field,$value,$ttl=NULL)
  {
    dlog( $this->verbosity() , "field = $field ; ttl = $ttl");

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    if ( is_null($ttl) || !$ttl )
      $ttl = 60*60*24*7; // default TTL is 1 week

    $result = FALSE;
    $retry  = FALSE;

    try
    {
      $result = $this->redis->setnx($field,$value);

      if ( $result )
        $this->redis->expire($field,$ttl);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->setnx($field,$value);

        if ( $result )
          $this->redis->expire($field,$ttl);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/get
   */
  public function get($field, $skiplog=FALSE)
  {
    if ( ! $skiplog )
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $value = NULL;
    $retry  = FALSE;

    try
    {
      $value = $this->redis->get($field);

      if ( $value === FALSE )
        $value = NULL;
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $value = $this->redis->get($field);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $value;
  }

  /**
   * http://redis.io/commands/del
   */
  public function del($field)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = FALSE;
    $retry  = FALSE;

    try
    {
      $result = $this->redis->del($field);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->del($field);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/expire
   */
  public function expire($field,$ttl)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = FALSE;
    $retry  = FALSE;

    try
    {
      $result = $this->redis->expire($field,$ttl);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->expire($field,$ttl);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/exists
   */
  public function exists($field)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = FALSE;
    $retry  = FALSE;

    try
    {
      $result = $this->redis->exists($field);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->exists($field);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * incr_or_set
   */
  public function incr_or_set($key,$ttl = NULL)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ($this->get($key))
      return $this->incr($key);
    else
    {
      if ( is_null($ttl) || !$ttl )
        $ttl = 60*60*24*7; // default TTL is 1 week

      $this->set($key, 1);

      return $this->expire($key,$ttl);
    }
  }

  /**
   * http://redis.io/commands/incr
   */
  public function incr($field)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = FALSE;
    $retry  = FALSE;

    try
    {
      $result = $this->redis->incr($field);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->incr($field);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/srem
   */
  public function srem($set,$element)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->srem($set,$element);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->srem($set,$element);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/sadd
   */
  public function sadd($set,$element)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->sadd($set,$element);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->sadd($set,$element);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/sismember
   */
  public function sismember($set,$element)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->sismember($set,$element);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->sismember($set,$element);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/smembers
   */
  public function smembers($set)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->smembers($set);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->smembers($set);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/hdel
   */
  public function hdel($hash,$field)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->hdel($hash,$field);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->hdel($hash,$field);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/hexists
   */
  public function hexists($hash,$field)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->hexists($hash,$field);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->hexists($hash,$field);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/hget
   */
  public function hget($hash,$field)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->hget($hash,$field);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->hget($hash,$field);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/hgetall
   */
  public function hgetall($hash)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->hgetall($hash);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->hgetall($hash);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/hkeys
   */
  public function hkeys($hash)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->hkeys($hash);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->hkeys($hash);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/hlen
   */
  public function hlen($hash)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->hlen($hash);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->hlen($hash);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/hmget
   */
  public function hmget($hash,$fields)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->hmget($hash,$fields);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->hmget($hash,$fields);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    if ( ! empty( $result ) && $this->is_cluster() )
    {
      // predis behaves differently

      $adjusted_result = array();

      for( $i=0 ; $i < count($fields) ; $i++ )
        $adjusted_result[ $fields[ $i ] ] = $result[ $i ];

      return $adjusted_result;
    }

    return $result;
  }

  /**
   * http://redis.io/commands/hmset
   */
  public function hmset($hash,$fields)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = FALSE;
    $retry  = FALSE;

    try
    {
      $result = $this->redis->hmset($hash,$fields);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e)
    {
      // $hash may not be a hash
      dlog('', $e->getMessage());
      $result = FALSE;
    }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->hmset($hash,$fields);
      }
      catch(\Exception $e)
      {
        // $hash may not be a hash
        dlog('', $e->getMessage());
        $result = FALSE;
      }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/hset
   */
  public function hset($hash,$field,$value)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->hset($hash,$field,$value);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->hset($hash,$field,$value);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * zpop
   */
  public function zpop($key,$min=-1,$max=9999999999)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $member = $this->zhead($key,$min,$max);

    if ( is_null( $member ) )
    {
      return NULL;
    }
    else
    {
      $this->zrem($key,$member);

      return $member;
    }
  }

  /**
   * zhead_list
   *
   * Not a native Redis operator: given the whole sorted set, returns the ones with lower score
   *
   * @return array
   */
  public function zhead_list($key,$list_size=5,$min=-1,$max=9999999999)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( !$list_size || ( $list_size < 1 ) )
      $list_size = 1;

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $rangebyscore = NULL;
    $retry  = FALSE;

    try
    {
      $rangebyscore = $this->redis->zrangebyscore($key,$min,$max);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $rangebyscore = $this->redis->zrangebyscore($key,$min,$max);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return ( $rangebyscore && is_array( $rangebyscore ) && count( $rangebyscore ) ) ? array_slice( $rangebyscore , 0 , $list_size ) : NULL ;
  }

  /**
   * zhead
   *
   * Not a native Redis operator: given the whole sorted set, returns the one with lower score
   *
   * @return string or NULL
   */
  public function zhead($key,$min=-1,$max=9999999999)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $rangebyscore = NULL;
    $retry  = FALSE;

    try
    {
      $rangebyscore = $this->redis->zrangebyscore($key,$min,$max);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $rangebyscore = $this->redis->zrangebyscore($key,$min,$max);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return ( $rangebyscore && is_array( $rangebyscore ) && count( $rangebyscore ) ) ? $rangebyscore[0] : NULL ;
  }

  /**
   * http://redis.io/commands/zadd
   */
  public function zadd($key,$score,$member)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zadd($key,$score,$member);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zadd($key,$score,$member);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zcard
   */
  public function zcard($key)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zcard($key);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zcard($key);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zcount
   */
  public function zcount($key,$min,$max)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zcount($key,$min,$max);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zcount($key,$min,$max);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zrank
   */
  public function zrank($key,$member)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zrank($key,$member);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zrank($key,$member);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zrangebyscore
   */
  public function zrangebyscore($key,$min,$max)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zrangebyscore($key,$min,$max);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zrangebyscore($key,$min,$max);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zrangebyscore
   */
  public function zhead_limit($key, $offset=0)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }
    
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zrangebyscore($key,'-inf','+inf', array('limit' => array($offset, 1)));
      if ( is_array( $result ) && count( $result ) )
        return $result[0];
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zrangebyscore($key,'-inf','+inf', array('limit' => array($offset, 1)));
        if ( is_array( $result ) && count( $result ) )
          return $result[0];
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return null;
  }

  /**
   * http://redis.io/commands/zrange
   */
  public function zrange($key,$start,$stop)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zrange($key,$start,$stop);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zrange($key,$start,$stop);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zrem
   */
  public function zrem($key,$member)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zrem($key,$member);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zrem($key,$member);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zremrangebyrank
   */
  public function zremrangebyrank($key,$start,$stop)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zremrangebyrank($key,$start,$stop);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zremrangebyrank($key,$start,$stop);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zremrangebyscore
   */
  public function zremrangebyscore($key,$min,$max)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zremrangebyscore($key,$min,$max);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zremrangebyscore($key,$min,$max);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zrevrange
   */
  public function zrevrange($key,$start,$stop)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zrevrange($key,$start,$stop);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zrevrange($key,$start,$stop);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zrevrangebyscore
   */
  public function zrevrangebyscore($key,$max,$min, $skiplog=FALSE)
  {
    if ( ! $skiplog )
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zrevrangebyscore($key,$max,$min);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zrevrangebyscore($key,$max,$min);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zrevrank
   */
  public function zrevrank($key,$member)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zrevrank($key,$member);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zrevrank($key,$member);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/zscore
   */
  public function zscore($key,$member)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->zscore($key,$member);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->zscore($key,$member);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/LLEN
   */
  public function llen($key)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->llen($key);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->llen($key);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/lpop
   */
  public function lpop($key)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->lpop($key);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->lpop($key);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/lpush
   */
  public function lpush($key,$value)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->lpush($key,$value);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->lpush($key,$value);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/lrange
   */
  public function lrange($key,$start,$stop)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->lrange($key,$start,$stop);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->lrange($key,$start,$stop);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/lrem
   */
  public function lrem($key,$value,$count=1)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      if ( $this->is_cluster() )
        // predis behaves differently
        $result = $this->redis->lrem($key,$count,$value);
      else
        $result = $this->redis->lrem($key,$value,$count);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        if ( $this->is_cluster() )
          // predis behaves differently
          $result = $this->redis->lrem($key,$count,$value);
        else
          $result = $this->redis->lrem($key,$value,$count);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/lset
   */
  public function lset($key,$index,$value)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->lset($key,$index,$value);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->lset($key,$index,$value);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/rpop
   */
  public function rpop($key)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->rpop($key);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->rpop($key);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

  /**
   * http://redis.io/commands/rpush
   */
  public function rpush($key,$value)
  {
    dlog( $this->verbosity() , '(%s)', func_get_args());

    if ( is_null( $this->redis ) ) { dlog('',"no Redis object available"); return FALSE; }

    $result = '';
    $retry  = FALSE;

    try
    {
      $result = $this->redis->rpush($key,$value);
    }
    catch(\Predis\Response\ServerException $p)
    {
      $retry  = ! ! $this->moveClusterConnection( $p->getMessage() );
    }
    catch(\Exception $e) { dlog('', $e->getMessage()); }

    if ( $retry )
    {
      try
      {
        $result = $this->redis->rpush($key,$value);
      }
      catch(\Exception $e) { dlog('', $e->getMessage()); }
    }

    return $result;
  }

}

/**
 * redis_read
 *
 * Shortcut to read a single token
 */
function redis_read( $key )
{
  $redis = new Redis;

  return $redis->get( $key );
}

/**
 * redis_write
 *
 * Shortcut to write a single token
 */
function redis_write( $key , $value , $ttl=NULL )
{
  $redis = new Redis;

  return $redis->set( $key , $value , $ttl );
}

/**
 * redis_delete
 *
 * Shortcut to delete a single token
 */
function redis_delete( $key )
{
  $redis = new Redis;

  return $redis->del( $key );
}


<?php

/**
 * Utility class for Linux process management
 * works with PHP command line processes only, used by runners to ensure system stability
 * @author VYT, Feb 2015
 */

namespace Ultra\Lib\Util;

class ProcessControl
{
  // constants
  const PROC_DIR = '/proc'; // Linux proc directory has all process info we need
  const INTERPRETER = 'php'; // script interpreter name

  // variables
  private $valid = FALSE; // has class initialized propery?
  private $pid = NULL; // this process ID
  private $verbose = FALSE;


  /**
   * constructor
   * initalialize our class to ensure all required functions are available
   * @param Boolean verbose logging
   * @throws Exception on failure
   */
  public function __construct($verbose = FALSE)
  {
    $functions = array(
      'dlog',
      'sys_getloadavg',
      'posix_kill');
    foreach ($functions as $function)
      if ( ! is_callable($function))
        throw new \Exception(__CLASS__ . " cannot access function $function");

    if ( ! $this->pid = getmypid())
      throw new \Exception(__CLASS__ . ' failed to get its process ID');

    if ($verbose)
    {
      dlog('', 'enabling verbose output');
      $this->verbose = $verbose;
    }

    $this->valid = TRUE;
  }


  /**
   * getDuplicateProcesses
   * return all process numbers with the same PHP script name
   * @return Array or process IDs, NULL on failure
   */
  public function getDuplicateProcesses()
  {
    $result = array();

    try
    {
      if ( ! $this->valid)
        throw new \Exception('class initialization failed');

      // get our command line string
      $path = self::PROC_DIR . '/' . $this->pid . '/cmdline';
      $command = file_get_contents($path);
      if ($command === FALSE || empty($command))
        throw new \Exception("failed to read $path");
      if ($this->verbose)
        dlog('', 'this process ID: %d, full command line: %s', $this->pid, $command);

      // extract executable and script names (discard parameters)
      list($executable, $script) = explode("\0", $command);

      // remove full path if present
      if (strrpos($executable, '/') !== FALSE)
        $executable = substr($executable, strrpos($executable, '/') + 1);
      if ($this->verbose)
        dlog('', 'executable: %s, script: %s', $executable, $script);

      // verify that we are a proper PHP command line process
      if ($executable != self::INTERPRETER)
        throw new \Exception('we are not a PHP command line process');

      // scan all processes to identify duplicates by script name
      $procs = scandir(self::PROC_DIR);
      if (empty($procs) || ! count($procs))
        throw new \Exception('failed to scan ' . self::PROC_DIR . ' directory');
      foreach ($procs as $pid)
      {
        $path = self::PROC_DIR . '/' . $pid;
        if (is_numeric($pid) && $pid != $this->pid && is_dir($path))
        {
          $content = file_get_contents($path . '/cmdline');
          if ($content === FALSE || empty($content)) // we may not be able to read process info that belongs to other users
            continue;

          // extract executable name and remove full path if present
          list($exec) = explode("\0", $content);
          if (strrpos($exec, '/') !== FALSE)
            $exec = substr($exec, strrpos($exec, '/') + 1);

          // skip process if not PHP command line
          if ($exec != self::INTERPRETER)
            continue;

          // PHP process found: extract and compare script name (not parameters)
          list( , $name) = explode("\0", $content);
          if ($name == $script)
          {
            if ($this->verbose)
              dlog('', 'found duplicate process ID: %d, command: %s', $pid, $content);
            $result[] = $pid;
          }
        }
      }

      if ($this->verbose)
        dlog('', 'found %d duplicate processes: %s', count($result), $result);
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: %s', $e->getMessage());
      $result = NULL;
    }

    return $result;
  }


  /**
   * getProcessAge
   * return the age of a given process in seconds (how long ago a process was created)
   * @param Integer process ID
   * @return Interger age in seconds or NULL on failure
   */
  public function getProcessAge($pid)
  {
    $age = NULL;
    
    try
    {
      if ( ! $this->valid)
        throw new \Exception('class initialization failed');

      if ( ! $pid)
        throw new \Exception('invalid parameter');

      // read process dir ctime
      $path = self::PROC_DIR . "/$pid";
      if ( ! $ctime = filectime($path))
        throw new \Exception("cannot read $path ctime");
      $age = time() - $ctime;
      if ($this->verbose)
        dlog('', 'process %d is %d sec old', $pid, $age);
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: %s', $e->getMessage());
    }

    return $age;
  }


  /**
   * terminateProcess
   * attempt to terminate a process by process ID
   * for use with getDuplicateProcesses method
   * WARNING: this only works when the given process executes under the same UID as the process calling this method or root
   *   e.g. a cron job that attemps to kill another cron job launched with the same username will succeed, while attemping to terminate another user's process will fail
   *   however if the current process is executing as root then this method will AWLAYS succeed
   * @param Integer process ID
   * @return Boolean TRUE on success or FALSE on failure
   */
  public function terminateProcess($pid)
  {
    $result = FALSE;

    try
    {
      if ( ! $this->valid)
        throw new \Exception('class initialization failed');

      if ( ! $pid)
        throw new \Exception('invalid parameter');

      // check that process exists
      $path = self::PROC_DIR . '/$pid';
      if  ( ! file_exists(self::PROC_DIR . "/$pid"))
        throw new \Exception("process $pid is not running");

      // ask nicely to terminate and allow to clean up
      if ($this->verbose)
        dlog('', 'attempting to terminate process %d', $pid);
      if ( ! posix_kill($pid, SIGTERM))
      {
        $err = posix_get_last_error();
        $msg = posix_strerror($err);
        throw new \Exception("POSIX error $err: $msg");
      }
      sleep(1);

      // kill if still running
      if  (file_exists($path))
      {
        if ($this->verbose)
          dlog('', 'process %d is still running, attempting to kill', $pid);
        if ( ! posix_kill($pid, SIGKILL))
        {
          $err = posix_get_last_error();
          $msg = posix_strerror($err);
          throw new \Exception("POSIX error $err: $msg");
        }
      }

      // check result
      if  ( ! file_exists($path))
        $result = TRUE;
      if ($this->verbose)
        dlog('', 'process %d terminated: %s', $pid, $result ? 'yes' : 'no');
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: %s', $e->getMessage());
    }

    return $result;
  }


  /**
   * getCpuLoad
   * return average system CPU load within the last minute
   * @return Integer 1-100 (%) or NULL on failure
   */
  public function getCpuLoad()
  {
    $result = NULL;

    try
    {
      if ( ! $this->valid)
        throw new \Exception('class initialization failed');
      list($result) = sys_getloadavg();
      if ($this->verbose)
        dlog('', 'CPU load %.2f%%', $result);
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: %s', $e->getMessage());
    }

    return $result;
  }


  /**
   * getMemoryUsage
   * return percent of current system memory usage (not the application)
   * @return Float RAM usage percentage or NULL on failure
   */
  public function getMemoryUsage()
  {
    $total = 0;
    $free = 0;
    $used = NULL;

    try
    {
      if ( ! $this->valid)
        throw new \Exception('class initialization failed');

      $path = self::PROC_DIR . '/meminfo';
      if ($info = fopen($path, 'r'))
      {
        while ($line = fgets($info))
        {
          $match = NULL;
          if (strpos($line, 'MemTotal:') === 0)
          {
            preg_match('!\d+!', $line, $match);
            if (! empty($match[0]))
              $total = $match[0];
          }
          elseif (strpos($line, 'MemFree:') === 0)
          {
            preg_match('!\d+!', $line, $match);
            if (! empty($match[0]))
              $free = $match[0];
          }
        }
        fclose($info);
      }
      else
        throw new \Exception("failed to open $path");

      if ($total && $free)
        $used = (float)sprintf('%.2f', ($total - $free) / $total * 100);
      if ($this->verbose)
        dlog('', 'RAM usage: %s%%, total %d kB, free %d kB', $used, $total, $free);
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: %s', $e->getMessage());
    }

    return $used;
  }


  /**
   * getFreeMemory
   * returns amount of free memory available on the system
   * @return Integer free memory in bytes or NULL on failure
   */
  public function getFreeMemory()
  {
    $free = NULL; // free memory
    $base = NULL; // kB (no others known quantifiers)

    try
    {
      if ( ! $this->valid)
        throw new \Exception('class initialization failed');

      $path = self::PROC_DIR . '/meminfo';
      if ($info = fopen($path, 'r'))
      {
        while ($line = fgets($info))
        {
          $match = array();
          if (strpos($line, 'MemFree:') === 0)
          {
            preg_match('!(\d+) +([k])!', $line, $match);
            if ( ! empty($match[1]) && ! empty($match[2]))
            {
              list( , $free, $base) = $match;
              break;
            }
          }
        }
        fclose($info);

        if ( ! $free && ! $base)
          throw new \Exception('failed to locate free memory entry');

        // convert base to bytes
        if ($base == 'k')
          $free *= 1024;

      }
      else
        throw new \Exception("failed to open $path");

      if ($this->verbose)
        dlog('', 'Free RAM: %s bytes', number_format($free));
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: %s', $e->getMessage());
    }

    return $free;
  }

}

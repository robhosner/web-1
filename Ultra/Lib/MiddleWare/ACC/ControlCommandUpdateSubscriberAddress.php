<?php

namespace Ultra\Lib\MiddleWare\ACC;

/**
 * ControlCommandUpdateSubscriberAddress control command implementation
 */
class ControlCommandUpdateSubscriberAddress extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'UpdateSubscriberAddress';

    $customer_id = $parameters['customer_id'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $command , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}

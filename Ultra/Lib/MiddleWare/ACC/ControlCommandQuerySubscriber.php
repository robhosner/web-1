<?php

namespace Ultra\Lib\MiddleWare\ACC;

/**
 * QuerySubscriber control command implementation
 */
class ControlCommandQuerySubscriber extends ControlCommandBase implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'QuerySubscriber';

    $outboundControlMessage = $this->buildOutboundControlMessage( $command , $parameters );

    return $this->interactWithChannel( $command , $outboundControlMessage );
  }
}

?>

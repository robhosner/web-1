<?php

namespace Ultra\Lib\MiddleWare\ACC;

require_once 'classes/PortInQueue.php';

/**
 * PortIn control command implementation
 */
class ControlCommandPortIn extends ControlCommandBiphasic implements ControlCommand
{
  public function processCommand($parameters)
  {
    dlog('',"%s",$parameters);

    $command = 'PortIn';
    $acc_api = 'ActivateSubscriber';

    if ( empty( $parameters['customer_id'] ) )
      return make_error_result('customer_id missing');

    if ( empty( $parameters['MSISDN'] ) )
      return make_error_result('MSISDN missing');

    $customer_id          = $parameters['customer_id'];
    $msisdn               = $parameters['MSISDN'];
    $zipCode              = NULL;
    $donorAccountNumber   = NULL;
    $donorAccountPassword = NULL;

    if ( isset($parameters['zipCode']) )
      $zipCode              = $parameters['zipCode'];

    if ( isset($parameters['donorAccountNumber']) ) 
      $donorAccountNumber   = $parameters['donorAccountNumber'];

    if ( isset($parameters['donorAccountPassword']) )
      $donorAccountPassword = $parameters['donorAccountPassword'];

    $parameters = $this->addCorrelationParameter($parameters,$command);

    // There are no pending ACC asynchronous API call, initiate a new Command Invocation
    $result = $this->initiate( $command , $acc_api , $parameters );

    if ( $result->is_failure() )
      return $this->initiateErrorResult( $command , $customer_id , $result );

    $parameters = $this->addPortInParameters($parameters);

    unset( $parameters['ICCID'] );
    unset( $parameters['MSISDN'] );
    unset( $parameters['wholesalePlan'] );
    unset( $parameters['ultra_plan_name'] );
    unset( $parameters['customer_id'] );

    $outboundControlMessage = $this->buildOutboundControlMessage( $acc_api , $parameters );

    $result = $this->interactWithChannel( $acc_api , $outboundControlMessage , FALSE , 180 );

    dlog('',"interactWithChannel result = %s",$result);
    dlog('',"interactWithChannel result errors = %s",$result->get_errors());

    if ( ! $result->has_errors() )
      $this->initiatePortIn($parameters,$customer_id,$acc_api,$msisdn,$zipCode,$donorAccountNumber,$donorAccountPassword,$result->data_array);

    return $result;
  }

  /**
   * initiatePortIn
   *
   * A0 - A Port Request is made has the Sync Return Successfully
   *
   * @return NULL
   */
  private function initiatePortIn($parameters,$customer_id,$acc_api,$msisdn,$zipCode,$donorAccountNumber,$donorAccountPassword,$responseData)
  {
    dlog('', "(%s)", func_get_args());

    $portInQueue = new \PortInQueue();

    $old_service_provider = NULL;

    if ( isset($responseData['message']) )
    {
      $data = $this->controlChannel->extractFromMessage( $responseData['message'] );

      dlog('',"message body = %s",$data->body);

      $dataBody = $this->extractDataBodyFromMessage( $responseData['message'] );

      dlog('',"dataBody = %s",$dataBody);

      if ( is_array($dataBody) )
      {
        if ( ! empty($dataBody['OldServiceProvider']) )
          $old_service_provider = $dataBody['OldServiceProvider'];

        if ( isset($dataBody['portRequestId']) )
          $responseData['portRequestId'] = $dataBody['portRequestId'];

        if ( isset($dataBody['portDueTime']) )
          $responseData['portDueTime'] = $dataBody['portDueTime'];

        if ( isset($dataBody['portDueDate']) )
          $responseData['portDueDate'] = $dataBody['portDueDate'];
      }
    }

    if (!isset($responseData['portRequestId']))
    {
      dlog('','ERROR ESCALATION ALERT DAILY - ActivateSubscriber (PortIn) did not return a portRequestId');
      $responseData['portRequestId'] = 'NONE_GIVEN';
    }

    $due_date_time = NULL;
    if ( isset($responseData['portDueDate']) )
      $due_date_time = date_to_datetime($responseData['portDueDate'], FALSE, TRUE);
    else
      dlog('','ERROR ESCALATION ALERT DAILY - ActivateSubscriber (PortIn) did not return a portDueDate');

    $result = $portInQueue->initiate(
      array(
        'customer_id'      => $customer_id,
        'msisdn'           => $msisdn,
        'last_acc_api'     => $acc_api,
        'account_number'   => $donorAccountNumber,
        'account_password' => $donorAccountPassword,
        'account_zipcode'  => $zipCode,
        //      <portRequestId> tag from ActivateSubscriberResponse
        'port_request_id'  => $responseData['portRequestId'],
        //        <portDueTime> tag from QueryStatusResponse or <portDueDate> ActivateSubscriberResponse
        'due_date_time'    => $due_date_time,
        // <OldServiceProvider> tag from ActivateSubscriberResponse
        'old_service_provider' => $old_service_provider
      )
    );

    dlog('',"PortInQueue::initiate returned %s",$result);

    // ignore $result

    return NULL;
  }

  private function addPortInParameters($parameters)
  {
    $parameters['numberGroup'] = '';
    $parameters['ReturnURL']   = '';
    $zipCode = empty($parameters['zipCode']) ? '30319' : $parameters['zipCode'];

    $parameters['ResourceData']['Subscriber_ResourceData'][] = array(
      'productCategory' => 'MSISDN',
      'serialNumber'    => $parameters['MSISDN']
    );

    $parameters = $this->addOriginatorDataParameters($parameters);
    $parameters = $this->addPoolAccInd($parameters);
    $parameters = $this->addResourceData($parameters);
    $parameters = $this->addAutoRenew($parameters);
    $parameters = $this->addPlanDataOnActivation($parameters);
    $parameters = $this->addContactData($parameters);

    $parameters['PortInData']['specificationValueName'] = 'PORTTYPE';
    $parameters['PortInData']['specificationValue']     = 'PI'; # regular, i.e. no porting. Otherwise 'PI'

    $parameters['PortInData']['firstName'] = 'Prepaid';
    $parameters['PortInData']['lastName']  = 'Customer';
    $parameters['PortInData']['fullName']  = '';
    $parameters['PortInData']['PortInAddressList']['PortInAddressInfo'][] = array(
      'addressLine1' => '123 Main Street',
      'addressLine2' => '',
      'State'        => 'GA',
      'Zip'          => $zipCode,
      'City'         => 'Atlanta',
      'Country'      => 'USA'
    );

    if ( !isset($parameters['donorAccountNumber']) || ( $parameters['donorAccountNumber'] == '' ) )
      $parameters['PortInData']['donorAccountNumber']   = '12345';
    else
    {
      $parameters['PortInData']['donorAccountNumber'] = $parameters['donorAccountNumber'];
      unset( $parameters['donorAccountNumber'] );
    }

    if ( !isset($parameters['donorAccountPassword']) || ( $parameters['donorAccountPassword'] == '' ) )
      $parameters['PortInData']['donorAccountPassword'] = '1234';
    else
    {
      $parameters['PortInData']['donorAccountPassword'] = $parameters['donorAccountPassword'];
      unset( $parameters['donorAccountPassword'] );
    }

    return $parameters;
  }
}


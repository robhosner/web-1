<?php


namespace Ultra\Lib\MiddleWare\Adapter;

if (!getenv("UNIT_TESTING"))
{
  require_once 'db/ultra_imei_history.php';
  require_once 'lib/inventory/functions.php';
  require_once 'lib/transitions.php';
  require_once 'Ultra/Lib/MQ/ControlChannel.php';
  require_once 'Ultra/Lib/MVNE/MakeItSo.php';
}


/**
 * Main Adapter class for MVNE2 Middleware
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project MVNE2
 */
class Control
{
  private $controlChannel;

  function __destruct()
  {
  }

  public function __construct()
  {
    // ControlChannel object
    $this->controlChannel = new \Ultra\Lib\MQ\ControlChannel;
  }

  /**
   * validateMWCall
   *
   * Adapter validation layer
   *
   * @returns a Result object
   */
  private function validateMWCall($params)
  {
    dlog('',"params = %s",$params);

    $result = new \Result();

    // validate required params
    $requiredParamsByCommand = $this->requiredParamsByCommand();

    $errors = array();

    if ( isset( $requiredParamsByCommand[ $params['command'] ] ) )
      foreach( $requiredParamsByCommand[ $params['command'] ] as $requiredParam )
        if ( ! isset( $params[ $requiredParam ] ) || ( $params[ $requiredParam ] == '' ) )
          $errors[] = "Missing ".$requiredParam;

    $errors = $this->validateUltraPlan($errors,$params);
    $errors = $this->validateZipCode($errors,$params);
    $errors = $this->validateLanguage($errors,$params);
    $errors = $this->validateWholesalePlan($errors,$params);
    $errors = $this->validateICCID($errors,$params);
    $errors = $this->validateMSISDN($errors,$params);
    $errors = $this->validatePortAccount($errors,$params);
    $errors = $this->validateAddOnFeatureListRaw($errors,$params);

    if ( count($errors) ) {
      dlog('', "validateMWCall errors = %s", $errors);
      $result->add_errors( $errors );
    } else {
      $result->succeed();
    }

    return $result;
  }

  /**
   * validateAddOnFeatureListRaw
   *
   * returns @array
   */
  private function validateAddOnFeatureListRaw($errors,$params)
  {
    if ( isset($params['addOnFeatureListRaw']) && $params['addOnFeatureListRaw'] && !is_array($params['addOnFeatureListRaw']) )
    {
      $errors[] = "Invalid addOnFeatureListRaw";
    }

    return $errors;
  }

  /**
   * validatePortAccount
   *
   * returns @array
   */
  private function validatePortAccount($errors,$params)
  {
    if (
         isset($params['port_account'])
      && $params['port_account']
      && !is_numeric($params['port_account'])
    )
      $errors[] = "Invalid port_account";

/*
    if ( 
         isset($params['port_password'])
      && !is_numeric($params['port_password'])
    )
      $errors[] = "Invalid port_password";
*/

    return $errors;
  }

  /**
   * validateICCID
   *
   * returns @array
   */
  private function validateICCID($errors,$params)
  {
    if (
         isset($params['iccid'])
      && ( $params['iccid'] != '' )
      && !preg_match("/^[0-9]{19}$/", $params['iccid'] )
    )
      $errors[] = "Invalid iccid";

    return $errors;
  }

  /**
   * validateMSISDN
   *
   * returns @array
   */
  private function validateMSISDN($errors,$params)
  {
    if (
         isset($params['msisdn'])
      && ( $params['msisdn'] != '' )
      && !preg_match("/^[0-9]{10}$/", $params['msisdn'] )
    )
      $errors[] = "Invalid msisdn";

    return $errors;
  }

  /**
   * validateWholesalePlan
   *
   * Validates 'wholesale_plan'
   *
   * returns @array
   */
  private function validateWholesalePlan($errors,$params)
  {
    if (
         isset($params['wholesale_plan'])
      && ( $params['wholesale_plan'] != '' )
      && ( $params['wholesale_plan'] != PRIMARY_WHOLESALE_PLAN    )
      && ( $params['wholesale_plan'] != SECONDARY_WHOLESALE_PLAN  )
      && ( $params['wholesale_plan'] != UV_PRIMARY_WHOLESALE_PLAN )
      && ( $params['wholesale_plan'] != MINT_WHOLESALE_PLAN       )
      && ( $params['wholesale_plan'] != MRC_WHOLESALE_PLAN        )
    )
      $errors[] = "Invalid wholesale_plan";

    return $errors;
  }

  /**
   * validateLanguage
   *
   * Validates 'preferred_language'
   *
   * returns @array
   */
  private function validateLanguage($errors,$params)
  {
    if (
         isset($params['preferred_language'])
      && ( $params['preferred_language'] != '' )
      && ! in_array( $params['preferred_language'] , array('EN','ES','ZH','en','es','zh') )
    )
      $errors[] = "Invalid preferred_language";

    return $errors;
  }

  /**
   * validateZipCode
   *
   * Validates 'zipcode'
   *
   * returns @array
   */
  private function validateZipCode($errors,$params)
  {
    if (
         isset($params['zipcode'])
      && ( $params['zipcode'] != '' )
      && !preg_match("/^[0-9]{5}$/", $params['zipcode'] )
    )
      $errors[] = "Invalid zipcode";

    return $errors;
  }

  /**
   * validateUltraPlan
   *
   * Validates 'ultra_plan'
   *
   * returns @array
   */
  private function validateUltraPlan($errors, $params)
  {
    // get standard Ultra L plan names and add non-standard types
    $plans = get_ultra_plans_short();
    $plans[] = '';
    $plans[] = 'STANDBY';

    if (isset($params['ultra_plan']) && ! in_array($params['ultra_plan'], $plans))
      $errors[] = "Invalid ultra_plan '{$params['ultra_plan']}'";

    return $errors;
  }

  /**
   * isQuery
   *
   * Returns TRUE if the given command is a query (read only operation)
   *
   * returns @boolean
   */
  public function isQuery( $command )
  {
    return in_array(
      $command,
      array('CheckBalance','GetMVNEDetails','GetNetworkDetails','GetNGPList','QueryStatus','QuerySubscriber')
    );
  }

  /**
   * requiredParamsByCommand
   *
   * Lists required parameters for each MW command
   *
   * returns @array
   */
  private function requiredParamsByCommand()
  {
    // TODO: check if the parameter 'customer_id' is required for the command 'DeactivateSubscriber'
    return array(
      'ActivateSubscriber'      => array( 'iccid' , 'preferred_language' , 'zipcode' , 'ultra_plan' , 'wholesale_plan' , 'customer_id' ),
      'CanActivate'             => array( 'iccid' ),
      'CancelDeviceLocation'    => array(),
      'CancelPortIn'            => array( 'msisdn' ),
      'ChangeIMEI'              => array(),
      'ChangeMSISDN'            => array( 'customer_id' , 'msisdn' , 'iccid' , 'zipcode' ),
      'ChangeSIM'               => array( 'customer_id' , 'msisdn' , 'old_iccid' , 'new_iccid' ),
      'CheckBalance'            => array( 'msisdn' ),
      'DeactivateSubscriber'    => array( 'customer_id' , 'iccid' , 'msisdn' ),
      'GetMVNEDetails'          => array(),
      'GetNetworkDetails'       => array( 'iccid' ),
      'GetNGPList'              => array( 'zipcode' ),
      'MakeitsoIntraPort'       => array( 'customer_id', 'msisdn', 'old_iccid' , 'new_iccid', 'ultra_plan',  'preferred_language' ),
      'MakeitsoReactivate'      => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' ),
      'MakeitsoDeactivate'      => array( 'iccid' , 'msisdn' , 'customer_id' ),
      'MakeitsoRenewPlan'       => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' ),
      'MakeitsoSuspend'         => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' ),
      'MakeitsoUpgradePlan'     => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' , 'option' ),
      'PortIn'                  => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' ),
      'PortInEligibility'       => array( 'msisdn' ),
      'PortInMvne1To2'          => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' ),
      'QueryStatus'             => array(),
      'QuerySubscriber'         => array(),
      'ReactivateSubscriber'    => array( 'iccid' , 'msisdn' , 'customer_id' , 'preferred_language' , 'ultra_plan' ),
      'RenewPlan'               => array( 'msisdn' , 'ultra_plan' , 'wholesale_plan' , 'customer_id' ),
      'RenewPlanRaw'            => array( 'msisdn' , 'ultra_plan' , 'wholesale_plan' , 'customer_id' ),
      'RestoreSubscriber'       => array( 'iccid' , 'msisdn' , 'customer_id' ),
      'SendSMS'                 => array( 'msisdn' , 'preferred_language' , 'sms_text' ),
      'SuspendSubscriber'       => array( 'iccid' , 'msisdn' , 'customer_id' ),
      'UpdatePlanAndFeatures'   => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' , 'option' ),
      'UpdatePlanAndFeaturesRaw'=> array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' ),
      'UpdateSubscriberAddress' => array( 'iccid' , 'msisdn', 'E911Address' ),
      'UpdateSubscriberFeature' => array( 'iccid' , 'msisdn' , 'feature' ),
      'UpdateWholesalePlan'     => array( 'msisdn' , 'wholesale_plan', 'customer_id' ),
      'MakeitsoAddMintData'     => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' , 'data_add_on' ),
      'MakeitsoSuspendMintPlan' => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' ),
      'MakeitsoRenewEndMintPlan'=> array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' ),
      'MakeitsoRenewMidMintPlan'=> array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' ),
      'MakeitsoRenewMintPlan'   => array( 'iccid' , 'msisdn' , 'customer_id' ),
      'UpdateThrottleSpeed'     => array( 'iccid' , 'msisdn' , 'customer_id' , 'ultra_plan' , 'wholesale_plan' , 'preferred_language' , 'throttle_speed', 'lte_select_method', 'balance_values' ),
    );
  }

  /**
   * mwCall
   *
   * returns a Result object
   */
  private function mwCall($params,$communication='synchronous',$requestReplyTimeoutSeconds=NULL)
  {
    dlog('',"params = %s",$params);

    if ( ! $this->isQuery( $params['command'] ) && \Ultra\UltraConfig\amdocs_query_only() )
    {
      $result = make_error_Result( 'This operation is currently not available (MW Query Only)' );

      $result->data_array['fatal']       = '0';
      $result->data_array['unavailable'] = '1';

      return $result;
    }

    if ( \Ultra\UltraConfig\amdocs_autofail() )
    {
      $result = make_error_Result( 'This operation is currently not available (MW Disabled)' );

      $result->data_array['fatal']       = '0';
      $result->data_array['unavailable'] = '1';

      return $result;
    }

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $message = $this->buildControlMessage($params);
    dlog('',"message = $message");

    if ( ! $requestReplyTimeoutSeconds )
      $requestReplyTimeoutSeconds = \Ultra\UltraConfig\channelsDefaultReplyTimeoutSeconds();

    $this->controlChannel->setRequestReplyTimeoutSeconds( $requestReplyTimeoutSeconds );

    $result = ( $communication == 'synchronous' )
            ?
            $this->mwSynchCall($message)
            :
            $this->mwAsynchCall($message)
            ;

    return $result;
  }

  /**
   * mwAsynchCall
   *
   * enqueue command into ULTRA/MW Control Queue ; don't wait for reply
   */
  private function mwAsynchCall($message)
  {
    // create an Asynchronous ULTRA/MW Outbound Control Channel

    $outboundUltraMWControlChannel = $this->controlChannel->createNewUltraMWControlAsynchChannel();

    $extractedData = $this->controlChannel->extractFromMessage( $message );

    // the asynch callback will need to retrieve this message
    $this->controlChannel->storeAsynchCallByAction( $extractedData->_actionUUID , $extractedData->header , $message );

    return $this->controlChannel->fireAndForgetToControlChannel($outboundUltraMWControlChannel,$message);
  }

  /**
   * mwSynchCall
   *
   * enqueue command into ULTRA/MW Control Queue and wait for reply
   *
   * returns a Result object
   */
  private function mwSynchCall($message)
  {
    $result = new \Result();

    # I am a Requestor, I want to send an *outbound* message through $outboundUltraMWControlChannel,
    # then I will synchronously wait for a reply the Replier will send through $inboundUltraMWControlChannel

    // create Request-Reply Ultra MW Control Channels
    list($outboundUltraMWControlChannel,$inboundUltraMWControlChannel) = $this->controlChannel->createNewUltraMWControlSynchChannels();
    dlog('',"Ultra MW Control Channels : outbound = $outboundUltraMWControlChannel ; inbound = $inboundUltraMWControlChannel");

    // send message to outbound ULTRA/MW Control Channel
    $controlUUID = $this->controlChannel->sendToControlChannel( $outboundUltraMWControlChannel , $message );
    #dlog('',"controlUUID = $controlUUID");

    $responseResult = $this->controlChannel->waitForUltraMWControlMessage( $inboundUltraMWControlChannel );

    if ( $responseResult->is_failure() )
    {
      \logError('waitForUltraMWControlMessage failed');
      $result = $responseResult;
    }
    else
    {
      dlog('',"Success: message = %s",$responseResult->data_array['message']);

      $result->succeed();

      $result->add_data_array( $this->extractFromControlResponse( $responseResult->data_array['message'] ) );

      if ( is_array( $result->data_array )
        && ! empty( $result->data_array['body'] )
        && is_object( $result->data_array['body'] )
        && ! empty( $result->data_array['body']->ResultCode )
        && ! empty( $result->data_array['body']->ResultMsg )
      )
      {
        // propagate default result code and message
        $result->data_array['ResultCode'] = $result->data_array['body']->ResultCode;
        $result->data_array['ResultMsg']  = $result->data_array['body']->ResultMsg;
      }
    }

    return $result;
  }

  /**
   * mwCallback
   *
   * returns a Result object
   */
  public function mwCallback($message)
  {
    $result = new \Result();

    try
    {
      $extractedData = $this->commandChannel->extractFromMessage( $message );

      dlog('',"extractedData = %s",$extractedData);

      if ( ! $extractedData )
        throw new Exception("ERR_API_INTERNAL: Could not extract data from message");

      if ( ! $extractedData->header )
        throw new Exception("ERR_API_INTERNAL: Could not extract header from message");

      if ( ( ! property_exists( $extractedData , 'body' ) ) || ( ! $extractedData->body ) )
        throw new Exception("ERR_API_INTERNAL: Could not extract body from message");

      dlog('',"command       = %s",$extractedData->header);
      dlog('',"parameters    = %s",(array) $extractedData->body);
      dlog('',"uuid          = %s",$extractedData->_uuid);
      dlog('',"actionUUID    = %s",$extractedData->_actionUUID);

# TODO:




    }
    catch(Exception $e)
    {
      dlog('', $e->getMessage());
      $errors[] = $e->getMessage();
    }

    return $result;
  }

  /**
   * extractFromControlResponse
   *
   * extracts data from the response
   *
   * @returns array
   */
  private function extractFromControlResponse($response)
  {
    $extractedData = '';

    if ( ! $response )
    {
      dlog('',"Response is empty");
    }
    else
    {
      $extractedData = $this->controlChannel->extractFromMessage( $response );

      $extractedData = (array) $extractedData->body;
    }

    return $extractedData;
  }

  /**
   * buildControlMessage
   *
   * Builds a message to be sent to the Ultra MW Control Channel
   */
  private function buildControlMessage($params)
  {
    $header = $params['command'];
    unset($params['command']);

    if ( empty( $params['actionUUID'] ) )
      $actionUUID = 'NO_actionUUID';
    else
      $actionUUID = $params['actionUUID'];

    unset($params['actionUUID']);

    return $this->controlChannel->buildMessage(
      array(
        'header'     => $header,
        'body'       => $params,
        'actionUUID' => $actionUUID
      )
    );
  }

  // ActivateSubscriber
  public function mwActivateSubscriber($params)
  {
    // mocking success for testing purposes in DEV environments
    if ( override_test_iccid( $params['iccid'] ) )
    {
      dlog('',"(*) ICCID activation overridden (*)");

      \Ultra\Lib\DB\Customer\setWholesalePlan($params['customer_id'], $params['wholesale_plan']);

      return make_ok_Result(
        array(
          'ResultCode'           => '100',
          'ResultMsg'            => 'Success',
          'body' => array(
            'MSISDN'               => '1001001000',
            'ResultCode'           => '100',
            'ResultMsg'            => 'Success',
            'portRequestId'        => '1',
            'portDueDate'          => '2014-12-09T15:10:39-08:00',
            'serviceTransactionId' => 'API-ULTRA-213723385226'
          )
        )
      );
    }

    $params['command'] = 'ActivateSubscriber';
    $result = $this->mwCall($params,'synchronous',120);

    if ( $result->is_success() )
    {
      $redis = new \Ultra\Lib\Util\Redis;

      $redis->del( 'iccid/good_to_activate/'     . luhnenize( $params['iccid'] ) );
      $redis->del( 'iccid/not_good_to_activate/' . luhnenize( $params['iccid'] ) );

      // DATAQ-164: record wholesale plan
      if ( ! empty($result->data_array['ResultCode']) &&  $result->data_array['ResultCode'] == 100)
        \Ultra\Lib\DB\Customer\setWholesalePlan($params['customer_id'], $params['wholesale_plan']);
    }

    return $result;
  }

  // CanActivate (not a native Amdocs API)
  public function mwCanActivate($params)
  {
    $params['command'] = 'CanActivate';

    $result = new \Result();

    $redis = new \Ultra\Lib\Util\Redis;

    if ( $redis->get( 'iccid/good_to_activate/' . luhnenize( $params['iccid'] ) ) )
    {
      $result->data_array['available'] = TRUE;
      $result->data_array['cached']    = TRUE;
      $result->data_array['success']   = TRUE;
      $result->succeed();
    }
    elseif ( $redis->get( 'iccid/not_good_to_activate/' . luhnenize( $params['iccid'] ) ) )
    {
      $result->data_array['available'] = FALSE;
      $result->data_array['cached']    = TRUE;
      $result->data_array['success']   = TRUE;
      $result->succeed();
    }
    else
    {
      $result = $this->mwCall($params);

      $result->data_array['cached'] = FALSE;

      // the MW call succeeded
      if ( ! $result->is_failure() )
      {
        // the iccid is available
        if ( $result->data_array['available'] )
        {
          $result->data_array['success'] = TRUE;

          $redis->set( 'iccid/good_to_activate/' . luhnenize( $params['iccid'] ) , 1 , 60 * 20 );
        }
        // the iccid is not available
        else
        {
          $result->data_array['success'] = FALSE;

#TODO: other stuff according to what we get in $result

          $redis->set( 'iccid/not_good_to_activate/' . luhnenize( $params['iccid'] ) , 1 , 60 * 20 );
        }
      }
    }

    return $result;
  }

  // CanActivateAsynch (not a native Amdocs API)
  public function mwCanActivateAsynch($params)
  {
    $params['command'] = 'CanActivateAsynch';

    $result = new \Result();

    $redis = new \Ultra\Lib\Util\Redis;

    if ( $redis->get( 'iccid/good_to_activate/' . luhnenize( $params['iccid'] ) ) )
    {
      // nothing to do
      $result->succeed();
    }
    else
    {
      $result = $this->mwCall($params,'asynchronous');

      if ( $result->is_success() )
        $result->set_pending();
    }

    return $result;
  }

  // CancelDeviceLocation
  public function mwCancelDeviceLocation($params)
  {
    $params['command'] = 'CancelDeviceLocation';

    return $this->mwCall($params);
  }

  // CancelPortIn
  public function mwCancelPortIn($params)
  {
    $params['command'] = 'CancelPortIn';

    if ( isset($params['msisdn']) )
      $params['msisdn'] = normalize_msisdn_10( $params['msisdn'] );

    $mwResult = $this->mwCall($params);

    $customer_id = NULL;

    if ( $mwResult->is_success() && $mwResult->data_array['success'] )
    {
      // get customer_id and ICCID
      teldata_change_db();
      $customer_data = mssql_fetch_all_rows(
        logged_mssql_query(
          \Ultra\Lib\DB\makeSelectQuery(
            'HTT_CUSTOMERS_OVERLAY_ULTRA',
            NULL,
            array( 'CURRENT_ICCID_FULL' , 'CUSTOMER_ID' ),
            array( 'current_mobile_number' => $params['msisdn'] )
          )
        )
      );

      if ( $customer_data && is_array($customer_data) && count($customer_data) )
      {
        // free the ICCID
        unreserve_iccid( $customer_data[0][0] );
        $customer_id = $customer_data[0][1];
      }
      else
      {
        dlog('',"MSISDN ".$params['msisdn']." not found in HTT_CUSTOMERS_OVERLAY_ULTRA, searching in HTT_CANCELLATION_REASONS");

        $customer_data = mssql_fetch_all_rows(
          logged_mssql_query(
            \Ultra\Lib\DB\makeSelectQuery(
              'HTT_CANCELLATION_REASONS',
              NULL,
              array( 'ICCID' , 'CUSTOMER_ID' ),
              array( 'MSISDN' => $params['msisdn'] )
            )
          )
        );

        if ( $customer_data && is_array($customer_data) && count($customer_data) )
        {
          // free the ICCID
          unreserve_iccid( $customer_data[0][0] );
          $customer_id = $customer_data[0][1];
        }
      }
    }

    if ( $customer_id )
    {
      // release COMMAND_INVOCATIONS for this port attempt

      $commandInvocationObject = new \CommandInvocation();

      $result = $commandInvocationObject->loadOpenByCustomerId( $customer_id );

      if ( $result->is_failure() )
        return $result;
      elseif ( $result->is_pending() )
      {
        $result = $commandInvocationObject->endAsPortCancelled();
        dlog('',"CommandInvocation::endAsPortCancelled result = %s", $result);
        if ($result->is_failure())
          return $result;
      }
      else
        dlog('',"MSISDN ".$params['msisdn']." is not associated with an OPEN Command Invocation");

      // PROD-1705: reconnect DB
      teldata_change_db();
    }

    return $mwResult;
  }

  // ChangeIMEI
  public function mwChangeIMEI($params)
  {
    $params['command'] = 'ChangeIMEI';

    return $this->mwCall($params);
  }

  // ChangeMSISDN
  public function mwChangeMSISDN($params)
  {
    $params['command'] = 'ChangeMSISDN';

    if ( isset($params['msisdn']) )
      $params['msisdn'] = normalize_msisdn_10( $params['msisdn'] );

    // the MSISDN returned is the new one
    $result = $this->mwCall($params);

    // the MW call succeeded and we got a MSISDN
    if ( $result->is_success()
      && isset( $result->data_array['body'] )
      && property_exists( $result->data_array['body'] , 'MSISDN' )
    )
      $result->data_array['msisdn'] = $result->data_array['body']->MSISDN;

    return $result;
  }

  // mwChangePlanImmediate is a wrapper for UpdatePlanAndFeatures
  public function mwChangePlanImmediate($params)
  {
    $params['command'] = 'ChangePlanImmediate';

    return $this->mwCall($params);
  }

  // ChangeSIM
  public function mwChangeSIM($params)
  {
    $params['command'] = 'ChangeSIM';

    if ( isset($params['msisdn']) )
      $params['msisdn'] = normalize_msisdn_10( $params['msisdn'] );

    return $this->mwCall($params);
  }

  // CheckBalance
  public function mwCheckBalance($params)
  {
    $params['command'] = 'CheckBalance';

    return $this->mwCall($params);
  }

  // DeactivateSubscriber
  public function mwDeactivateSubscriber($params)
  {
    $params['command'] = 'DeactivateSubscriber';

    return $this->mwCall($params);
  }

  /**
   * mwGetMVNEDetails (not a native Amdocs API)
   */
  public function mwGetMVNEDetails($params)
  {
    $params['command'] = 'GetMVNEDetails';

    // validation

    $validation_errors = array();

    // msisdn or iccid needed
    if ( !( isset($params['msisdn']) && $params['msisdn'] )
      && !( isset($params['iccid'])  && $params['iccid']  ) )
    {
      $validation_errors[] = "Please provide either msisdn or iccid, or both";
    }
    else
    {
      if ( $params['msisdn'] && ! preg_match('/^\d{10}$/', $params['msisdn']) )
        $validation_errors[] = "msisdn is not valid";

      if ( $params['iccid']  && ! preg_match('/^\d{19}$/', $params['iccid']) )
        $validation_errors[] = "iccid is not valid";
    }

    if ( count( $validation_errors ) )
      return make_error_Result( $validation_errors );

    $result = $this->mwCall($params);

    // add to ULTRA.IMEI_HISTORY
    if (isset($result->data_array['GetNetworkDetails'])
      && property_exists($result->data_array['GetNetworkDetails'], 'MSISDN')
      && $result->data_array['GetNetworkDetails']->MSISDN
      && property_exists($result->data_array['GetNetworkDetails'], 'SIM')
      && $result->data_array['GetNetworkDetails']->SIM
      && property_exists($result->data_array['GetNetworkDetails'], 'IMEI')
      && $result->data_array['GetNetworkDetails']->IMEI)
    {
      teldata_change_db();
      \add_to_imei_history( array(
        'msisdn' => $result->data_array['GetNetworkDetails']->MSISDN,
        'iccid'  => $result->data_array['GetNetworkDetails']->SIM,
        'imei'   => $result->data_array['GetNetworkDetails']->IMEI));
    }

    return $result;
  }

  // GetNGPList
  public function mwGetNGPList($params)
  {
    $params['command'] = 'GetNGPList';

    return $this->mwCall($params);
  }

  // GetNetworkDetails
  public function mwGetNetworkDetails($params)
  {
    $params['command'] = 'GetNetworkDetails';

    $return = $this->mwCall($params);

    if ( isset($return->data_array['body'])
      && property_exists( $return->data_array['body'] , 'IMEI' )
      && property_exists( $return->data_array['body'] , 'MSISDN' )
      && $return->data_array['body']->IMEI
      && $return->data_array['body']->MSISDN
    )
    {
      teldata_change_db();

      \add_to_imei_history( array(
        'msisdn' => $return->data_array['body']->MSISDN,
        'iccid'  => $params['iccid'],
        'imei'   => $return->data_array['body']->IMEI
      ) );
    }

    return $return;
  }

  // MakeitsoAddMintData($params)
  public function mwMakeitsoAddMintData($params)
  {
    if (! $connection = \Ultra\Lib\DB\ultra_acc_connect())
      return make_error_Result('Failed to connect to database');

    $open_miso = get_makeitso(array('CUSTOMER_ID' => $params['customer_id'], 'STATUS' => 'PENDING'), 1, 'MAKEITSO_QUEUE_ID');

    if (count($open_miso))
      return make_error_result("Subscriber {$params['customer_id']} has pending MISO {$open_miso[0]->MAKEITSO_QUEUE_ID}");

    teldata_change_db();

    $params['command'] = 'MakeitsoAddMintData';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueAddMintData(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['data_add_on']
    );
  }

  // MakeitsoDeactivate (not a native Amdocs API); will eventually make sure that the given customer is deactivated
  public function mwMakeitsoDeactivate($params)
  {
    $params['command'] = 'MakeitsoDeactivate';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueDeactivate(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid']
    );
  }

  // MakeitsoReactivate (not a native Amdocs API); will eventually make sure that the given customer is reactivated
  public function mwMakeitsoReactivate($params)
  {
    $params['command'] = 'MakeitsoReactivate';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueReactivate(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language']
    );
  }

  // MakeitsoRenewPlan (not a native Amdocs API); will eventually make sure that the given customer's plan is renewed
  public function mwMakeitsoRenewPlan($params)
  {
    $params['command'] = 'MakeitsoRenewPlan';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueRenewPlan(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['plan_tracker_id'],
      $params['subplan'],
      \Ultra\Lib\RoamCreditPromo::promoAmountFromParams($params)
    );
  }

  // MakeitsoSuspend (not a native Amdocs API); will eventually make sure that the given customer is suspended
  public function mwMakeitsoSuspend($params)
  {
    $params['command'] = 'MakeitsoSuspend';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueSuspend(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['plan_tracker_id']
    );
  }

  // MakeitsoUpgradePlan (not a native Amdocs API); will eventually make sure that the given customer's plan is upgraded
  public function mwMakeitsoUpgradePlan($params)
  {
    // fork logic for Mint Data Add On
    if ( ! empty($params['option'])
      && \Ultra\MvneConfig\isMintDataSOC( $params['option'] )
    )
    {
      $params['data_add_on'] = $params['option'];

      // Mint Data Add On requires its own MISO task
      return $this->mwMakeitsoAddMintData($params);
    }

    $params['command'] = 'MakeitsoUpgradePlan';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueUpgradePlan(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['option'],
      !empty($params['extra_options']) && is_array($params['extra_options']) ? $params['extra_options'] : []
    );
  }

  // PortIn (not a native Amdocs API)
  public function mwPortIn($params)
  {
    $params['command'] = 'PortIn';

    // without those values we get <ResultMsg>Request Structure Invalid</ResultMsg>
    if ( !isset($params['port_account'])  || ( $params['port_account'] == '' ) )
      $params['port_account']  = '12345';
    if ( !isset($params['port_password']) || ( $params['port_password'] == '' ) )
      $params['port_password'] = '1234';

    $result = $this->mwCall($params,'synchronous',120);

    if ( $result->is_success() )
    {
      $redis = new \Ultra\Lib\Util\Redis;

      $redis->del( 'iccid/good_to_activate/'     . luhnenize( $params['iccid'] ) );
      $redis->del( 'iccid/not_good_to_activate/' . luhnenize( $params['iccid'] ) );
      $redis->del( 'msisdn/good_to_port/'     . $params['msisdn'] );
      $redis->del( 'msisdn/not_good_to_port/' . $params['msisdn'] );
      
      // DATAQ-164: record wholesale plan in ULTRA.WHOLESALE_PLAN_TRACKER
      if ( ! empty($result->data_array['ResultCode']) &&  $result->data_array['ResultCode'] == 100)
        \Ultra\Lib\DB\Customer\setWholesalePlan($params['customer_id'], $params['wholesale_plan']);
    }

    return $result;
  }

  // PortInEligibility
  public function mwPortInEligibility($params, $ignoreCache = FALSE)
  {
    $params['command'] = 'PortInEligibility';

    if ( isset($params['msisdn']) )
      $params['msisdn'] = normalize_msisdn_10( $params['msisdn'] );

    $result = new \Result();

    // mocking success for testing purposes in DEV environments
    if ( override_test_msisdn($params['msisdn']) )
    {
      $result->data_array['eligible'] = TRUE;
      $result->data_array['success']  = TRUE;

      return $result;
    }

    $result->data_array['failure_reason'] = '';

    $redis = new \Ultra\Lib\Util\Redis;

    // cached success (if allowed)
    if ( ! $ignoreCache && $redis->get( 'msisdn/good_to_port/' . $params['msisdn'] ) )
    {
      $result->data_array['eligible']  = TRUE;
      $result->data_array['cached']    = TRUE;
      $result->data_array['success']   = TRUE;
      $result->succeed();
    }
    // cached failure (if allowed)
    elseif ( ! $ignoreCache && $redis->get( 'msisdn/not_good_to_port/' . $params['msisdn'] ) )
    {
      $result->data_array['eligible']  = FALSE;
      $result->data_array['cached']    = TRUE;
      $result->data_array['success']   = TRUE;
      $result->succeed();
    }
    else
    {
      $result = $this->mwCall($params);

      $result->data_array['cached'] = FALSE;

      // the MW call succeeded
      if ( ! $result->is_failure() )
      {
        // the MW response has the data we expect
        if ( ! empty($result->data_array['body']) && is_object($result->data_array['body']) && property_exists( $result->data_array['body'] , 'eligibilityStatus' ) )
        {
          // the msisdn is eligible
          if ( $result->data_array['body']->eligibilityStatus == 'true' )
          {
            $result->data_array['eligible'] = TRUE;
            $result->data_array['success']  = TRUE;

            $redis->set( 'msisdn/good_to_port/' . $params['msisdn'] , 1 , 60 * 20 );
          }
          // the msisdn is not eligible and we know why
          elseif( property_exists( $result->data_array['body'] , 'failureReason' ) && $result->data_array['body']->failureReason )
          {
            $result->data_array['eligible']       = FALSE;
            $result->data_array['failure_reason'] = $result->data_array['body']->failureReason;

            $redis->set( 'msisdn/not_good_to_port/' . $params['msisdn'] , 1 , 60 * 20 );
          }
          // the msisdn is not eligible but we don't know why
          else
          {
            $result->data_array['eligible']       = FALSE;
            $result->data_array['failure_reason'] = '-unknown-';
          }
        }
        else
          $result->add_error( 'Incomplete MW response' );
      }
    }

    return $result;
  }

  // QueryStatus
  public function mwQueryStatus($params)
  {
    $params['command'] = 'QueryStatus';

    return $this->mwCall($params);
  }

  // QuerySubscriber
  public function mwQuerySubscriber($params)
  {
    $params['command'] = 'QuerySubscriber';

    if ( ( !isset($params['msisdn']) || ( $params['msisdn'] == '' ) )
      && ( !isset($params['iccid'] ) || ( $params['iccid']  == '' ) )
    )
      return \make_error_Result( "This function requires msisdn or iccid" );

    if ( isset($params['msisdn']) && $params['msisdn'] )
      $params['msisdn'] = normalize_msisdn_10( $params['msisdn'] );

    if ( isset($params['iccid']) && $params['iccid'] )
      $params['iccid'] = luhnenize($params['iccid']);

    return $this->mwCall($params);
  }

  // ReactivateSubscriber
  public function mwReactivateSubscriber($params)
  {
    $params['command'] = 'ReactivateSubscriber';

    return $this->mwCall($params);
  }

  // RenewPlan
  public function mwRenewPlan($params)
  {
    $params['command'] = 'RenewPlan';

    return $this->mwCall($params);
  }

  // RenewPlanRaw -- Does the exact same thing as RenewPlan, but the AddOnFeatureList node is custom ( $params['addOnFeatureListRaw'] )
  public function mwRenewPlanRaw($params)
  {
    $params['command'] = 'RenewPlanRaw';

    return $this->mwCall($params);
  }

  // ResendOTA
  public function mwResendOTA($params)
  {
    $params['command'] = 'ResendOTA';

    return $this->mwCall($params);
  }

  // ResetVoiceMailPassword
  public function mwResetVoiceMailPassword($params)
  {
    $params['command'] = 'ResetVoiceMailPassword';

    return $this->mwCall($params);
  }

  // RestoreSubscriber
  public function mwRestoreSubscriber($params)
  {
    $params['command'] = 'RestoreSubscriber';

    return $this->mwCall($params);
  }

  // SendSMS
  public function mwSendSMS($params)
  {
    $params['command'] = 'SendSMS';

    if ( isset($params['msisdn']) )
      $params['msisdn'] = normalize_msisdn_10( $params['msisdn'] );

    // 801 - This is used for Immediate delivery
    // 802 - Called "Courtesy" and queues the message to be delivered between 8am and 8pm
    if ( ! isset($params['qualityOfService']) )
      $params['qualityOfService'] = getSMSQualityOfService( $param['sms_text'] );

    return $this->mwCall($params);
  }

  // SendSMSAsynch (not a native Amdocs API)
  public function mwSendSMSAsynch($params)
  {
    $params['command'] = 'SendSMS';

    if ( isset($params['msisdn']) )
      $params['msisdn'] = normalize_msisdn_10( $params['msisdn'] );

    // 801 - This is used for Immediate delivery
    // 802 - Called "Courtesy" and queues the message to be delivered between 8am and 8pm
    if ( ! isset($params['qualityOfService']) )
      $params['qualityOfService'] = getSMSQualityOfService( $param['sms_text'] );

    $result = $this->mwCall($params,'asynchronous');

    if ( $result->is_success() )
      $result->set_pending();

    return $result;
  }

  // SuspendSubscriber
  public function mwSuspendSubscriber($params)
  {
    $params['command'] = 'SuspendSubscriber';

    return $this->mwCall($params);
  }

  // UpdateBalance
  public function mwUpdateBalance($params)
  {
    $params['command'] = 'UpdateBalance';

    return $this->mwCall($params);
  }

  // UpdateSubscriberAddress
  public function mwUpdateSubscriberAddress($params)
  {
    if (! $connection = \Ultra\Lib\DB\ultra_acc_connect())
      return make_error_Result('Failed to connect to database');

    $open_miso = get_makeitso(array('CUSTOMER_ID' => $params['customer_id'], 'STATUS' => 'PENDING'), 1, 'MAKEITSO_QUEUE_ID');

    if (count($open_miso))
      return make_error_result("Subscriber {$params['customer_id']} has pending MISO {$open_miso[0]->MAKEITSO_QUEUE_ID}");

    teldata_change_db();

    $params['command'] = 'UpdateSubscriberAddress';

    $result = $this->mwCall($params);

    if ($result->is_success() && ! empty($result->data_array['ResultCode']) && $result->data_array['ResultCode'] == 100)
    {
      $sqlResult = \set_acc_address_e911($params['customer_id'], implode($params['E911Address'], "\n"));
      if (!$sqlResult)
        \logWarn('Error inserting address into database for customer_id : ' . $params['customer_id']);
    }

    return $result;
  }

  // UpdatePlanAndFeatures
  public function mwUpdatePlanAndFeatures($params)
  {
    $params['command'] = 'UpdatePlanAndFeatures';

    return $this->mwCall($params);
  }

  /**
   * mwUpdatePlanAndFeaturesRaw
   *
   * Does the exact same thing as UpdatePlanAndFeatures, but B_VOICE , B_SMS , VOICEMAIL values are provided as input
   * 'raw_b_voice'
   * 'raw_b_sms'
   * 'raw_voicemail'
   */
  public function mwUpdatePlanAndFeaturesRaw($params)
  {
    $params['command'] = 'UpdatePlanAndFeaturesRaw';

    return $this->mwCall($params);
  }

  // UpdatePortIn
  public function mwUpdatePortIn($params)
  {
    $params['command'] = 'UpdatePortIn';

    return $this->mwCall($params);
  }

  // UpdateSubscriberFeature (not a native Amdocs API)
  public function mwUpdateSubscriberFeature($params)
  {
    $params['command'] = 'UpdateSubscriberFeature';

    return $this->mwCall($params);
  }

  // UpdateWholesalePlan
  public function mwUpdateWholesalePlan($params)
  {
    $params['command'] = 'UpdateWholesalePlan';

    $result = $this->mwCall($params);

    // DATAQ-167: update wholesale plan tracker
    if ($result->is_success() && ! empty($result->data_array['ResultCode']) &&  $result->data_array['ResultCode'] == 100)
      \Ultra\Lib\DB\Customer\setWholesalePlan($params['customer_id'], $params['wholesale_plan']);

    return $result;
  }


  /**
   * mwMakeitsoIntraPort
   *
   * validate and enqueue Makeitso IntraPort
   * @param Array parameters
   * return Object Result
   */
  public function mwMakeitsoIntraPort($params)
  {
    $params['command'] = substr(__FUNCTION__, 2);
    $result = $this->validateMWCall($params);
    if ($result->is_failure())
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo;
    return $makeItSo->enqueue(array(
      'type'              => 'IntraPort',
      'action_uuid'       => $params['action_uuid'],
      'customer_id'       => $params['customer_id'],
      'options'           => json_encode(array(
        'msisdn'              => $params['msisdn'],
        'old_iccid'           => $params['old_iccid'],
        'new_iccid'           => $params['new_iccid'],
        'ultra_plan'          => $params['ultra_plan'],
        'preferred_language'  => $params['preferred_language'],
        'wholesale_plan'      => $params['wholesale_plan'],
        'state'               => $params['state']))
      )
    );
  }

// mwMakeitsoRenewEndMintPlan (not a native Amdocs API); will eventually make sure that the given customer's plan is renewed
  public function mwMakeitsoRenewEndMintPlan($params)
  {
    $params['command'] = 'MakeitsoRenewEndMintPlan';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueRenewEndMintPlan(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['plan_tracker_id'],
      $params['subplan']
    );
  }

  // mwMakeitsoRenewMidMintPlan (not a native Amdocs API); will eventually make sure that the given customer's plan is renewed
  public function mwMakeitsoRenewMidMintPlan($params)
  {
    $params['command'] = 'MakeitsoRenewMidMintPlan';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueRenewMidMintPlan(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['plan_tracker_id'],
      $params['subplan']
    );
  }

  // MakeitsoSuspendMintPlan (not a native Amdocs API); will eventually make sure that the given customer is suspended
  public function mwMakeitsoSuspendMintPlan($params)
  {
    $params['command'] = 'MakeitsoSuspendMintPlan';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueSuspendMintPlan(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['plan_tracker_id']
    );
  }

  // MakeitsoRenewMintPlan (not a native Amdocs API); will eventually make sure that the given customer's Mint SIM is restored from Suspended
  public function mwMakeitsoRenewMintPlan($params)
  {
    $params['command'] = 'MakeitsoRenewMintPlan';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueRenewMintPlan(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['plan_tracker_id']
    );
  }

  //
  public function mwMakeitsoUpdateThrottleSpeed($params)
  {
    if (! $connection = \Ultra\Lib\DB\ultra_acc_connect())
      return make_error_Result('Failed to connect to database');

    // $open_miso = get_makeitso(array('CUSTOMER_ID' => $params['customer_id'], 'STATUS' => 'PENDING'), 1, 'MAKEITSO_QUEUE_ID');

    // if (count($open_miso))
    //   return make_error_result("Subscriber {$params['customer_id']} has pending MISO {$open_miso[0]->MAKEITSO_QUEUE_ID}");

    teldata_change_db();

    $params['command'] = 'UpdateThrottleSpeed';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueUpdateThrottleSpeed(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['throttle_speed'],
      $params['lte_select_method'],
      $params['balance_values'],
      $params['plan_tracker_id'],
      $params['migratingFromMrc']
    );
  }

  public function mwMakeitsoRemoveFromBan($params)
  {
    if (! $connection = \Ultra\Lib\DB\ultra_acc_connect())
      return make_error_Result('Failed to connect to database');

    teldata_change_db();

    $params['command'] = 'UpdatePlanAndFeatures';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueRemoveFromBan(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['preferred_language'],
      $params['ban']
    );
  }

  public function mwMakeitsoAddToBan($params)
  {
    if (! $connection = \Ultra\Lib\DB\ultra_acc_connect())
      return make_error_Result('Failed to connect to database');

    teldata_change_db();

    $params['command'] = 'UpdatePlanAndFeatures';

    $result = $this->validateMWCall($params);

    if ( $result->is_failure() )
      return $result;

    $makeItSo = new \Ultra\Lib\MVNE\MakeItSo(
      array(
        'instantly' => ! ! ( isset($params['instantly']) && $params['instantly'] )
      )
    );

    return $makeItSo->enqueueAddToBan(
      $params['actionUUID'],
      $params['customer_id'],
      $params['msisdn'],
      $params['iccid'],
      $params['ultra_plan'],
      $params['wholesale_plan'],
      $params['option'],
      $params['preferred_language']
    );
  }
}

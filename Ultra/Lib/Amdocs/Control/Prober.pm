package Ultra::Lib::Amdocs::Control::Prober;


use strict;
use warnings;


use Data::Dumper;
use JSON::XS;
use Ultra::Lib::MQ::ControlChannel;


use base qw(Ultra::Lib::Amdocs::Base);


=head1 NAME

Ultra::Lib::Amdocs::Control::Prober

=head1 Amdocs Controls Handler

=head1 SYNOPSIS

  use Ultra::Lib::Amdocs::Control::Prober;

  my $controlProber = Ultra::Lib::Amdocs::Control::Prober->new();

  print STDOUT $controlProber->probeControlCommand( $command );

=cut


=head4 probeControlCommand

  Direct SOAP $command call to Amdocs.

=cut
sub probeControlCommand
{
  my ($this,$command,$probeParams) = @_;

  $this->log("probeControlCommand invoked with command = $command , probeParams = ".$this->{ JSON_CODER }->encode( $probeParams ) );

  my $ssl_cert_file = '/etc/httpd/certs/acc/ULTRA.pem';
  my $end_point     = 'https://acc-prod.proxy.ultra.me:8084/acc/inbound.aspx';

  $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = "0";
  $ENV{HTTPS_DEBUG}                  = 1;
  $ENV{HTTPS_CERT_FILE}              = $ssl_cert_file;
  $ENV{HTTPS_KEY_FILE}               = $ssl_cert_file;

  my $ssl_opts = {
    SSL_cert_file => $ssl_cert_file,
    SSL_key_file  => $ssl_cert_file,
    SSL_passwd_cb => sub { return "ultra1"; },
    SSL_version   => 'SSLv3'
  };

  print STDERR "ssl_cert_file = $ssl_cert_file\n";
  print STDERR "end_point     = $end_point\n";

  my $return =
  {
    'outcome'      => '',
    'soapRequest'  => '',
    'soapResponse' => '',
  };

  my $wsdl = XML::Compile::WSDL11->new( $this->{ WSDL_FILE } );

  my $ua = LWP::UserAgent->new;

  $ua->default_headers->header( 'SOAPAction' => 'http://www.sigvalue.com/acc/'.$command );

  $ua->ssl_opts( %$ssl_opts );

  my $trans = XML::Compile::Transport::SOAPHTTP->new( user_agent => $ua , address => $end_point );

  my $op = $wsdl->operation( $command );

  $wsdl->compileCall( $op , transport => $trans );

  # Do the actual call.
  # $trace will let us log the request and the response.
  my ($answer,$trace,$start_time) = (undef,undef,scalar(time));

  eval
  {
    ($answer,$trace) = $wsdl->call( $command => { parameters => $probeParams } );
  };

  print STDERR 'ACC Sync '.$command.' call duration = '.substr((scalar(time)-$start_time),0,5)."\n";

  if ( $@ ) # error originating from the last eval
  {
    $return->{ 'outcome' } = $@;
  }
  elsif ( ! $trace )
  {
    $return->{ 'outcome' } = "No trace available after $command SOAP call";
  }
  else
  {
    $this->log("request  content = ".$trace->request()->content());
    $this->log("response content = ".$trace->response()->content());

    $this->log("response status  = ".$trace->response()->status_line());

    # returns SOAP request
    $return->{ 'soapRequest' } = $this->prettyPrintXML( $trace->request()->content() );

    if ( $trace->response()->status_line() =~ /Connection timed out/ )
    {
      $return->{ 'soapResponse' } = $trace->response()->status_line();
    }
    else
    {
      # returns SOAP response
      $return->{ 'soapResponse' } = $this->prettyPrintXML( $trace->response()->content() );
    }

    $return->{ 'outcome' } = $trace->response()->status_line();
  }

  $this->log("probeControlCommand returning ".$this->{ JSON_CODER }->encode( $return ) );

  return $return;
}


=head4 probeACCMWControlCommand

  Main method to test an outbound Control Command.
  1) build message as a serialized Control Command
  2) enqueue message in outbound ACC MW Control Channel
  3) synchronously wait for a reply
  4) extract data from reply
  5) return the data

=cut
sub probeACCMWControlCommand
{
  my ($this,$command,$params) = @_;

  my $return;

  my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

  # instantiate Ultra::Lib::MQ::ControlChannel
  my $mqControlChannel = Ultra::Lib::MQ::ControlChannel->new( JSON_CODER => $json_coder );

  my $data =
  {
    'header'     => $command,
    'body'       => $params,
    'actionUUID' => 'actionUUID-Prober-'.time,
  };

  my $outboundMessage = $mqControlChannel->buildMessage( $data );

  # create Request-Reply ACC MW Control Channels
  my ( $outboundControlSynchChannel , $inboundControlSynchChannel ) = $mqControlChannel->createNewACCMWControlChannels();

  $mqControlChannel->log("outbound Control Channel = $outboundControlSynchChannel");
  $mqControlChannel->log("inbound  Control Channel = $inboundControlSynchChannel");

  my $controlUUID = $mqControlChannel->sendToControlChannel( $outboundControlSynchChannel , $outboundMessage );

  $mqControlChannel->log("controlUUID     = $controlUUID");
  $mqControlChannel->log("outboundMessage = $outboundMessage");

  my $inboundMessage = $mqControlChannel->waitForControlMessage( $inboundControlSynchChannel , $mqControlChannel->inboundACCMWControlChannel() );

# TODO: fix this
$return = $inboundMessage;

  return $return;
}


1;


__END__



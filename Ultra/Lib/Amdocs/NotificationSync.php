<?php

/**
 * Runner to update pending COMMAND_INVOCATIONS when the corresponding notification is received in production SOAP_LOG
 * @todo: check for notification result and update command invocation appropriately
 * @author: VYT @ 2015-04-29
 */

require_once 'db.php';

/**
 * main execution
 */

// get next open command invocation
\Ultra\Lib\DB\ultra_acc_connect();
if ( ! $command = getOpenCommandInvocation())
  exit(0);

// check if notification has arrived (into prod)
if ( ! $notification = getInvocationNotification($command))
  exit(0);

// success: close command invocation
if ( ! closeCommandInvocation($command))
{
  dlog('', "ERROR: failed to close command invocation {$command->COMMAND_INVOCATIONS_ID}");
  exit(0);
}

/**
 * get latest open command invocation
 */
function getOpenCommandInvocation()
{
  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery(
    'COMMAND_INVOCATIONS',
    1,
    NULL,
    array('STATUS' => 'OPEN'),
    NULL,
    'COMMAND_INVOCATIONS_ID DESC',
    TRUE))
  {
    dlog('', 'ERROR: failed to generate SELECT SQL');
    return NULL;
  }

  if ($commands = mssql_fetch_all_objects(logged_mssql_query($sql)))
  {
    dlog('', 'found open command invocation %s', $commands[0]->COMMAND_INVOCATIONS_ID);
    return $commands[0];
  }

  dlog('', 'no open command invocations found');
  return NULL;
}

/**
 * search for command invocation notification
 */
function getInvocationNotification($command)
{
  if (empty($command->CORRELATION_ID))
  {
    dlog('', 'ERROR: CORRELATION_ID is missing in commnand invocation %d', $command->COMMAND_INVOCATIONS_ID);
    return NULL;
  }

  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery(
    'SOAP_LOG',
    1,
    'SOAP_LOG_ID',
    array(
      "SOAP_DATE > '{$command->START_DATE_TIME}'",
      'TYPE_ID' => 3,
      'TAG' => $command->CORRELATION_ID),
    NULL,
    'SOAP_LOG_ID DESC',
    TRUE))
  {
    dlog('', 'ERROR: failed to generate SELECT query');
    return NULL;
  }

  if ($notifications = mssql_fetch_all_objects(logged_mssql_query($sql)))
  {
    $notification = $notifications[0];
    dlog('', 'found notification %s', $notification->SOAP_LOG_ID);
    return $notification;
  }

  return NULL;
}

/**
 * close open command invocation
 */
function closeCommandInvocation($command)
{
  if ( ! $sql = \Ultra\Lib\DB\makeUpdateQuery('COMMAND_INVOCATIONS', array('STATUS' => 'COMPLETED'), array('COMMAND_INVOCATIONS_ID' => $command->COMMAND_INVOCATIONS_ID)))
  {
    dlog('', 'ERROR: failed to generate UPDATE query');
    return FALSE;
  }

  if ( ! is_mssql_successful(logged_mssql_query($sql)))
  {
    dlog('', 'ERROR: failed to update command invocation %s', $command->COMMAND_INVOCATIONS_ID);
    return FALSE;
  }

  return TRUE;
}


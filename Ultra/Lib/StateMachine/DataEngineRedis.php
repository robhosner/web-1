<?php

namespace Ultra\Lib\StateMachine;

require_once 'Ultra/Lib/StateMachine/DataEngineInterface.php';

class DataEngineRedis implements DataEngineInterface
{
  public $currentTransitionData;
  private $currentActionData;

  /**
   * storeTransitionAndActions
   *
   * Store Transition and Actions in Redis
   *
   * @return \Outcome Object
   */
  public function storeTransitionAndActions()
  {
// TODO

    return NULL;
  }

  /**
   * loadTransitionAndActionsByUUID
   *
   * Load Transition and Actions from Redis using the unique identifier
   *
   * @return array
   */
  public function loadTransitionAndActionsByUUID( $transition_uuid )
  {
// TODO

    return NULL;
  }

  /**
   * loadNextTransitionAndActions
   *
   * Load data for the next transition which should be resolved
   *
   * @return array
   */
  public function loadNextTransitionAndActions()
  {
// TODO

    return NULL;
  }

  /**
   * updatePlanState
   *
   * Update plan_state value
   * 
   * @return string
   */
  public function updatePlanState( $plan_state , $customer_id )
  {
    $error = '';

//TODO:

    return $error;
  }

  public function abortTransitionAndActions()
  {
// TODO
  
    return '';
  }

  public function recordActionOutcome( $outcome )
  {
// TODO

    return NULL;
  }

  /**
   * recordActionPending
   *
   * Take note of timestamp at the beginning of the current action
   *
   * @return NULL
   */
  public function recordActionPending()
  {
//TODO:

    return NULL;
  }

  public function closeAction( $action_uuid )
  {
// TODO

    return NULL;
  }

  public function closeTransition()
  {
// TODO

    return ;;;
  }

  public function getNextActionData()
  {
// TODO

    return NULL;
  }

  /**
   * resetCurrentActionData
   *
   * Clears the current action data
   *
   * @return null
   */
  public function resetCurrentActionData()
  {
    $this->currentActionData = array();
  }
}


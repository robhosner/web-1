<?php

/*

Action functions

All functions must take at least 3 parameters
( $customerData , $currentTransitionData , $action_uuid )
the 4th parameter is optional, but if given it must be an array of action parameters.
All functions must return an \Outcome Object

*/

namespace Ultra\Lib\StateMachine\Action;


require_once 'classes/Flex.php';
require_once 'classes/LineCredits.php';
require_once 'Ultra/Lib/MVNE/MakeItSo.php';
require_once 'Ultra/Lib/Services/FamilyAPI.php';
require_once 'Ultra/Lib/Services/OnlineSalesAPI.php';
require_once 'Ultra/Lib/DB/Customer.php';

/**
 * Does nothing, used for testing
 *
 * @return \Outcome Object
 */
function noop( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  dlog('',"params = %s",$params);

  $outcome = new \Outcome;

  $outcome->succeed();

  return $outcome;
}

/**
 * Does nothing, used for testing
 *
 * @return \Outcome Object
 */
function test( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  return noop( $customerData , $currentTransitionData , $action_uuid , $params );
}

/**
 * Sets the packaged_balance1 field for a customer to 0
 *
 * @return \Outcome Object
 */
function emptyMinutes( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE ACCOUNTS SET packaged_balance1 = 0 WHERE CUSTOMER_ID = %d", $customerData['customer_id']);

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to zero packaged_balance1';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}


/**
 * Sets the expires_date field of an htt_inventory_sim record to the current date, given an ICCID number
 *
 * @return \Outcome Object
 */
function expireSimCard( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['current_iccid_full']))
    $outcome->add_errors_and_code('Missing current_iccid_full', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE HTT_INVENTORY_SIM SET EXPIRES_DATE = getutcdate() WHERE ICCID_NUMBER = '%s'",
    mssql_escape_with_zeroes(substr($customerData['current_iccid_full'], 0, -1)));

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to set htt_inventory_sim expires date';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Sets the expires_date field of an htt_ultra_msisdn record to the current date, given a msisdn
 *
 * @return \Outcome Object
 */
function expireMSISDN( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['current_mobile_number']))
    $outcome->add_errors_and_code('Missing current_mobile_number', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE HTT_ULTRA_MSISDN SET EXPIRES_DATE = getutcdate() WHERE MSISDN = '%s'",
    mssql_escape_with_zeroes($customerData['current_mobile_number']));

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to set htt_ultra_msisdn expires date';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Disables an account by customer_id
 *
 * @return \Outcome Object
 */
function disableAccount( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf('UPDATE ACCOUNTS SET %s WHERE customer_id = %d',
    account_enabled_assignment(FALSE),
    $customerData['customer_id']
  );

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to set disable account';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Disables a MSISDN
 *
 * @return \Outcome Object
 */
function deactivateUltraMSISDN( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['current_mobile_number']))
    $outcome->add_errors_and_code('Missing current_mobile_number', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE HTT_ULTRA_MSISDN SET MSISDN_ACTIVATED = 0 , LAST_TRANSITION_UUID = '%s' WHERE MSISDN = '%s'",
    mssql_escape_with_zeroes($currentTransitionData['transition_uuid']),
    mssql_escape_with_zeroes($customerData['current_mobile_number'])
  );

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to disable ultra MSISDN';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Deletes account_aliases record for a MSISDN, given current_mobile_number
 *
 * @return \Outcome Object
 */
function deleteAccountAliasesByMSISDN( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['current_mobile_number']))
    $outcome->add_errors_and_code('Missing current_mobile_number', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("DELETE FROM ACCOUNT_ALIASES WHERE ALIAS = '%s'",
    mssql_escape_with_zeroes($customerData['current_mobile_number'])
  );

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to remove account alias';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Deletes account_aliases record for a MSISDN by their account_id, given current_mobile_number
 *
 * @return \Outcome Object
 */
function deleteAccountAliasesByAccountId( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['current_mobile_number']))
    $outcome->add_errors_and_code('Missing current_mobile_number', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("IF EXISTS (SELECT a.ACCOUNT_ID FROM ACCOUNT_ALIASES a where a.ALIAS = '%s')
                  BEGIN DELETE FROM ACCOUNT_ALIASES WHERE ALIAS = '%s' END",
    mssql_escape_with_zeroes($customerData['current_mobile_number']),
    mssql_escape_with_zeroes($customerData['current_mobile_number']));

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to remove account alias';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Runs the [ULTRA].[DeactivateAccount] stored procedure.
 *
 * @return \Outcome Object
 */
function runDeactivateAccountStoredProcedure(array $customerData, $currentTransitionData, $action_uuid, $params=NULL)
{
  $outcome = new \Outcome;

  if (empty($customerData['current_mobile_number']))
    $outcome->add_errors_and_code('Missing current_mobile_number', 'MP0001');

  if (empty($customerData['current_iccid_full']))
    $outcome->add_errors_and_code('Missing current_iccid_full', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $params = [
    [
      'param_name' => '@ALIAS',
      'variable'   => $customerData['current_mobile_number'],
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 10,
    ],
    [
      'param_name' => '@ICCID_NUMBER',
      'variable'   => substr($customerData['current_iccid_full'], 0, -1),
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 19,
    ],
    [
      'param_name' => '@LAST_TRANSITION_UUID',
      'variable'   => $currentTransitionData['transition_uuid'],
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 40,
    ],
    [
      'param_name' => '@DELETED',
      'variable'   => 1,
      'type'       => SQLBIT,
      'is_output'  => true,
      'is_null'    => false,
      'max_length' => 1,
    ],
  ];

  if (\Ultra\Lib\DB\run_stored_procedure('[ULTRA].[DeactivateAccount]', $params) === false)
  {
    $error = '[ULTRA].[DeactivateAccount] failed to process';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Sets the activation_date_time field for an accounts record, given customer_id
 *
 * @return \Outcome Object
 */
function setActivationDateTimeIfUnset( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE ACCOUNTS SET ACTIVATION_DATE_TIME = GETUTCDATE() 
                  WHERE customer_id = %d AND ACTIVATION_DATE_TIME IS NULL", $customerData['customer_id']);

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to update accounts activation_date_time';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Sets the plan_started field for an htt_customers_overlay_ultra record, given customer_id and plan_started
 *
 * @return \Outcome Object
 */
function setPlanStarted( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $planStarted = 'GETUTCDATE()';
  // change to customer plan expires for these transitions
  foreach (['Monthly Renewal', 'Monthly Renewal and Change Plan'] as $label)
  {
    if (strpos($label, $currentTransitionData['transition_label']) === 0)
      $planStarted = "'{$customerData['plan_expires']}'";
  }

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET plan_started = %s WHERE customer_id = %d",
    $planStarted,
    $customerData['customer_id']);

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to update htt_customers_overlay_ultra.plan_started';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Sets the gross_add_date field of a htt_customers_overlay_ultra record to current date, given a customer_id
 *
 * @return \Outcome Object
 */
function setGrossAddDate( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET GROSS_ADD_DATE = GETUTCDATE() WHERE customer_id = %d",
    $customerData['customer_id']);

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to update htt_customers_overlay_ultra.gross_add_date';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Logs the cancellation transition in the activation history table
 *
 * @return \Outcome Object
 */
function logCancellationTransitionInActivationHistory( array $customerData , $currentTransitionData , $action_uuid , $params=NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = log_cancellation_transition_in_activation_history($currentTransitionData['transition_uuid'], $customerData['customer_id']);

  if ( ! $result['success'])
  {
    $error = 'Failed to log cancellation transition';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Sets the MSISDN and ICCID values in htt_customers_overlay_ultra record to null, given a customer_id
 *
 * @return \Outcome Object
 */
function nullifySimAndMsisdn( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA 
                  SET CURRENT_MOBILE_NUMBER = NULL , CURRENT_ICCID_FULL = NULL , CURRENT_ICCID = NULL
                  WHERE CUSTOMER_ID = %d",
                  $customerData['customer_id']);

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to nullify sim and MSISDN';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Removes any leftovers from immediate bolt ons
 *
 * @return \Outcome Object
 */
function removeImmediateBoltOns( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome(null, true);

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  // UpGlobe bolt on
  // reset packaged_balance3
  $sql = accounts_update_query([
    'customer_id'       => $customerData['customer_id'],
    'packaged_balance3' => 0
  ]);
  if ( !run_sql_and_check($sql) )
  {
    $error = 'Failed to reset packaged_balance3';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }

  // remove ANI_FORMAT records
  if ( !empty($customerData['current_mobile_number']) && strlen($customerData['current_mobile_number']) == 10 )
  {
    if ( !ani_format_remove_upglobe($customerData['current_mobile_number']) )
    {
      $error = 'Failed to remove ANI_FORMAT records';
      \logError($error);
      $outcome->add_errors_and_code($error, 'DB0009');
    }
  }

  return $outcome;
}

/**
 * Triggers Recurring Bolt Ons effects for a customer
 * Note, by the time we reach this step, we have already swept the STORED_VALUE into the wallet (BALANCE).
 *
 * @return \Outcome Object
 */
function processRecurringBoltOns( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = process_recurring_bolt_ons([
    'customer_id' => $customerData['customer_id'],
    'plan'        => get_plan_from_cos_id($currentTransitionData['to_cos_id'])
  ]);

  if ( ! $result['success'])
  {
    $error = 'Failed to process recurring bolt ons';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Logs the transition in the activation history table
 *
 * @return \Outcome Object
 */
function logTransitionInActivationHistory( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = log_transition_in_activation_history($currentTransitionData['transition_uuid'], $customerData['customer_id']);

  if (!$result['success'])
  {
    $error = 'Failed to log transition in activation history';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Logs the new activation transition in the activation history table
 *
 * @return \Outcome Object
 */
function logNewActivationInActivationHistory( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = log_new_activation_in_activation_history($currentTransitionData['transition_uuid'], $customerData['customer_id']);

  if (!$result['success'])
  {
    $error = 'Failed to log new activation in activation history';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * saveGrossAddDate
 *
 * Sets the gross_add_date of an htt_customers_overlay_ultra record to the current date, given a customer_id
 *
 * @return \Outcome Object
 */
/*
function saveGrossAddDate( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome(null, true);

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');
  if (!$outcome->is_success())
    return $outcome;

  $sql = sprintf("UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET GROSS_ADD_DATE = GETUTCDATE() WHERE customer_id = %d",
    $customerData['customer_id']);

  if (!run_sql_and_check($sql))
  {
    $error = 'Failed to update htt_customers_overlay_ultra.gross_add_date';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }

  return $outcome;
}
*/

/**
 * Logs plan started in the activation history table
 *
 * @return \Outcome Object
 */
function logPlanStartedInActivationHistory( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = log_plan_started_in_activation_history($customerData['customer_id']);

  if ( ! $result['success'])
  {
    $error = 'Failed to log plan started in activation history';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * voice/sms/data socs are not added
 * Invokes mwActivateSubscriber
 * $plan can be [ STANDBY , NINETEEN , TWENTY_NINE , THIRTY_NINE , FORTY_NINE , FIFTY_NINE ]
 *
 * @return \Outcome Object
 */
function mvneProvisionSIM( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['current_iccid_full']))
    $outcome->add_errors_and_code('Missing current_iccid_full', 'MP0001');

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($customerData['postal_code']))
    $outcome->add_errors_and_code('Missing postal_code', 'MP0001');

  if (empty($customerData['preferred_language']))
    $outcome->add_errors_and_code('Missing preferred_language', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = \mvneProvisionSIM(
    $customerData['current_iccid_full'],
    $customerData['postal_code'],
    $customerData['preferred_language'],
    get_plan_name_from_short_name(get_plan_from_cos_id($currentTransitionData['to_cos_id'])),
    $customerData['customer_id'],
    $currentTransitionData['transition_uuid'],
    $action_uuid
  );

  if ($result->is_success())
    $outcome->succeed();
  else
  {
    $error = 'MVNE provision failed';
    \logError($error);
    $outcome->add_errors_and_code($error, 'PR0001');
  }

  return $outcome;
}

/**
 * Invokes mwActivateSubscriber
 * $plan can be [ STANDBY , NINETEEN , TWENTY_NINE , THIRTY_NINE , FORTY_NINE , FIFTY_NINE ]
 *
 * @return \Outcome Object
 */
function mvneActivateSIM( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['current_iccid_full']))
    $outcome->add_errors_and_code('Missing current_iccid_full', 'MP0001');

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($customerData['postal_code']))
    $outcome->add_errors_and_code('Missing postal_code', 'MP0001');

  if (empty($customerData['preferred_language']))
    $outcome->add_errors_and_code('Missing preferred_language', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = \mvneActivateSIM(
    $customerData['current_iccid_full'],
    $customerData['postal_code'],
    $customerData['preferred_language'],
    get_plan_name_from_short_name(get_plan_from_cos_id($currentTransitionData['to_cos_id'])),
    $customerData['customer_id'],
    $currentTransitionData['transition_uuid'],
    $action_uuid
  );

  if ($result->is_success())
    $outcome->succeed();
  else
  {
    $error = 'MVNE activation failed';
    \logError($error);
    $outcome->add_errors_and_code($error, 'PR0001');
  }

  return $outcome;
}

/**
 * Sets the activation_date_time field for an accounts_record, given a customer_id
 *
 * @return \Outcome Object
 */
function activeMaybeSetActivationDateTime( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE ACCOUNTS SET ACTIVATION_DATE_TIME = GETUTCDATE() 
                  WHERE customer_id = %d AND ACTIVATION_DATE_TIME IS NULL",
                  $customerData['customer_id']);

  if ( ! run_sql_and_check($sql))
  {
    $error = 'Failed to set accounts.activation_date_time';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * activePlanStartedX
 *
 * Sets the plan_started field of an htt_customers_overlay_ultra record, given a customer_id
 *
 * @return \Outcome Object
 */
/*
function activePlanStartedX( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome(null, true);

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');
  if (!$outcome->is_success())
    return $outcome;

  $plan_started = 'GETUTCDATE()';
  switch ($params['plan_started'])
  {
    case 'plan_expires':
      $plan_started = $customerData['plan_expires'];
      break;
    case 'now':
      $plan_started = 'GETUTCDATE()';
      break;
  }

  $sql = sprintf("UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET plan_started = %s WHERE customer_id = %d",
    mssql_escape_with_zeroes($plan_started),
    $customerData['customer_id']);
  
  if (!run_sql_and_check($sql))
  {
    $error = 'Failed to set htt_customers_overlay_ultra.plan_started';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }

  return $outcome;
}
*/


/**
 * Sets the plan_expires field of an htt_customers_overlay_ultra record to 30 days after existing value, given a customer_id and plan_started
 *
 * @return \Outcome Object
 */
function initialActivePlanExpires( array $customerData, $currentTransitionData, $action_uuid, $params = null )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $sql = sprintf("UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA 
                  SET plan_expires = dbo.PT_TO_UTC(convert(date, dateadd(s, 36000, dateadd(d, %d, dbo.UTC_TO_PT(%s)))))
                  WHERE customer_id = %d",
                  30,
                  'GETUTCDATE()',
                  $customerData['customer_id']);

  if ( ! run_sql_and_check($sql)) 
  {
    $error = 'Failed to set htt_customers_overlay_ultra.plan_expires';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * updates HTT_CUSTOMERS_OVERLAY_ULTRA.plan_expires 30 days for customer
 *
 * @return \Outcome Object
 */
/**
 * updates HTT_CUSTOMERS_OVERLAY_ULTRA.plan_expires 30 days for customer
 *
 * @return \Outcome Object
 */
function renewalActivePlanExpires( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $customer = get_customer_from_customer_id($customerData['customer_id'], ['plan_started']);

  $daysOffset = 30;
  // if mint
  if (\Ultra\UltraConfig\planMonthsByCosId($currentTransitionData['to_cos_id']) == 12)
  {
     // TODO: get months remaining
    $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customerData['customer_id']);
    $daysOffset = \Ultra\UltraConfig\mintTwelveMonthRenewalOffsets($multiMonthOverlay->UTILIZED_MONTHS);
  }

  \dlog('', "Customer Plan Started: %s", $customer->plan_started);
  \dlog('', "Customer Days Offset: %d", $daysOffset);

  //$date = new \DateTime(date('Y-m-d', strtotime($customer->plan_started . " +$daysOffset days")));
  $date = new \DateTime(date('Y-m-d', strtotime($customer->plan_started . " +$daysOffset days")), new \DateTimeZone('America/Los_Angeles'));
  $hour = $date->format('I') ? 7 : 8;

  \dlog('',  "Hour: %d", $hour);
  \dlog('', 'DST: %d', date('I'));

  $sql = sprintf("UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA
                  SET    plan_expires = DATEADD(HH, $hour, CONVERT(DATETIME,CONVERT(DATE, DATEADD(DAY, %d, '%s'))))
                  WHERE  customer_id = %d",
                  $daysOffset,
                  $customer->plan_started,
                  $customerData['customer_id']);

  if ( ! run_sql_and_check($sql)) 
  {
    $error = 'Failed to set htt_customers_overlay_ultra.plan_expires';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0009');
  }
  else
    $outcome->succeed();

  return $outcome;
}


/**
 * Execute appropriate middleware Ensure Suspend depending in customer's MVNE
 *
 * @return \Outcome Object
 */
function mvneEnsureSuspend( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome(null, true);

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($customerData['current_mobile_number']))
    $outcome->add_errors_and_code('Missing current_mobile_number', 'MP0001');

  if (empty($customerData['current_iccid_full']))
    $outcome->add_errors_and_code('Missing current_iccid_full', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = \mvneEnsureSuspend(
    $action_uuid,
    $customerData['customer_id'],
    $customerData['current_mobile_number'],
    $customerData['current_iccid_full']
  );

  if ( ! $result->is_success())
  {
    $error = 'Failed to ensure suspend';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Execute appropriate middleware Ensure Cancel depending in customer's MVNE
 * Invokes mwMakeitsoDeactivate
 * NOOP if agent is 'Port Out Import Tool' in HTT_CANCELLATION_REASONS
 *
 * @return \Outcome Object
 */
function mvneEnsureCancel( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome(null, true);

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($customerData['current_mobile_number']))
    $outcome->add_errors_and_code('Missing current_mobile_number', 'MP0001');

  if (empty($customerData['current_iccid_full']))
    $outcome->add_errors_and_code('Missing current_iccid_full', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = \mvneEnsureCancel(
    $customerData['current_mobile_number'],
    $customerData['current_iccid_full'],
    $currentTransitionData['transition_uuid'],
    $action_uuid,
    $customerData['customer_id']
  );

  if ( ! $result->is_success())
  {
    $error = 'Failed to ensure cancel';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * sleep for X seconds
 *
 * @return \Outcome Object
 */
function sleep( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  $seconds = ( ! empty($params['seconds'])) ? $params['seconds'] : 5;
  \sleep($seconds);

  $outcome->succeed();

  return $outcome;
}

/**
 * Invokes mwPortIn
 *
 * @return \Outcome Object
 */
function mvneRequestPortIn( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if (empty($currentTransitionData['context']['port_in_msisdn']))
    $outcome->add_errors_and_code('Missing port_in_msisdn', 'MP0001');

  if (empty($currentTransitionData['context']['port_in_iccid']))
    $outcome->add_errors_and_code('Missing port_in_iccid', 'MP0001');

  if (empty($currentTransitionData['context']['port_in_account_number']))
    $outcome->add_errors_and_code('Missing port_in_account_number', 'MP0001');

  if (empty($currentTransitionData['context']['port_in_account_password']))
    $outcome->add_errors_and_code('Missing port_in_account_password', 'MP0001');

  if (empty($currentTransitionData['context']['port_in_zipcode']))
    $outcome->add_errors_and_code('Missing port_in_zipcode', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = \mvneRequestPortIn(
    $customerData['customer_id'],
    $currentTransitionData['context']['port_in_msisdn'],
    $currentTransitionData['context']['port_in_iccid'],
    $currentTransitionData['context']['port_in_account_number'],
    $currentTransitionData['context']['port_in_account_password'],
    $currentTransitionData['context']['port_in_zipcode'],
    $currentTransitionData['to_cos_id']
  );

  if ( ! $result->is_success() )
  {
    $error = 'Failed to request port in';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Invokes mwMakeitsoRenewPlan
 *
 * @return \Outcome Object
 */
function mvneMakeitsoRenewPlan( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = \mvneMakeitsoRenewPlan(
    $customerData['customer_id'],
    get_plan_from_cos_id($currentTransitionData['to_cos_id'])
  );

  if ( ! $return['success'])
  {
    $error = 'Failed running MakeitsoRenewPlan';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Invokes mwMakeitsoRenewMintPlan
 *
 * @return \Outcome Object
 */
function mvneMakeitsoRenewMintPlan( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = \mvneMakeitsoRenewMintPlan(
    $customerData['customer_id'],
    get_plan_from_cos_id($currentTransitionData['to_cos_id'])
  );

  if ( ! $return['success'])
  {
    $error = 'Failed running MakeitsoRenewMintPlan';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Invokes mwMakeitsoRenewEndMintPlan
 *
 * @return \Outcome Object
 */
function mvneMakeitsoRenewEndMintPlan( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = \mvneMakeitsoRenewEndMintPlan(
    $customerData['customer_id'],
    get_plan_from_cos_id($currentTransitionData['to_cos_id'])
  );

  if ( ! $return['success'])
  {
    $error = 'Failed running MakeitsoRenewEndMintPlan';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Invokes mwMakeitsoRenewMidMintPlan
 *
 * @return \Outcome Object
 */
function mvneMakeitsoRenewMidMintPlan( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = \mvneMakeitsoRenewMidMintPlan(
    $customerData['customer_id'],
    get_plan_from_cos_id($currentTransitionData['to_cos_id'])
  );

  if ( ! $return['success'])
  {
    $error = 'Failed running MakeitsoRenewMidMintPlan';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Adds a new transtition which contain the action 'internal_func_delayed_ensure_suspend'
 *
 * @return \Outcome Object
 */
function delayedProvisioningSuspend( array $customerData, $currentTransitionData, $action_uuid, $params = null )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = internal_func_delayed_provisioning_suspend($customerData['customer_id']);

  if ( ! $return['success'])
  {
    $error = 'Failed delayed provisioning suspend';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * invoke record_ICCID_activation
 * update ACTIVATION_ICCID
 * reserve iccid
 *
 * @return \Outcome Object
 */
function simActivationHandler( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = sim_activation_handler(
    $customerData['customer_id'],
    $currentTransitionData['transition_uuid'],
    $currentTransitionData['to_cos_id']
  );

  if ( ! $return['success'])
  {
    $error = 'SIM activation failed';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * insert into ACCOUNT_ALIASES
 * insert into DESTINATION_ORIGIN_MAP
 *
 * @return \Outcome Object
 */
function msisdnActivationHandler( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = msisdn_activation_handler( $customerData['customer_id'] );

  if ( ! $return['success'])
  {
    $error = 'Failed to activate MSISDN';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Logs the port transition in the activation history table
 *
 * @return \Outcome Object
 */
function logPortTransitionInActivationHistory( array $customerData, $currentTransitionData, $action_uuid, $params = null )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = log_port_transition_in_activation_history(
    $currentTransitionData['transition_uuid'],
    $customerData['customer_id']
  );

  if ( ! $return['success'])
  {
    $error = 'Failed to log port transition';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Logs the intraport transition in the activation history table
 *
 * @return \Outcome Object
 */
function logIntraportTransitionInActivationHistory( array $customerData, $currentTransitionData, $action_uuid, $params = null )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = log_intraport_transition_in_activation_history(
    $currentTransitionData['transition_uuid'],
    $customerData['customer_id']
  );

  if ( ! $return['success'])
  {
    $error = 'Failed to log port transition';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Logs the port iccid in the activation history table
 *
 * @return \Outcome Object
 */
function logPortIccidInActivationHistory( array $customerData, $currentTransitionData, $action_uuid, $params = null )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($customerData['current_iccid']))
    $outcome->add_errors_and_code('Missing iccid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = log_port_iccid_in_activation_history(
    $customerData['customer_id'],
    $customerData['current_iccid']
  );

  if ( ! $return['success'])
  {
    $error = 'Failed to log ICCID port';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Sweep stored value
 *
 * @return \Outcome Object
 */
function sweepStoredValueExplicit( array $customerData, $currentTransitionData, $action_uuid, $params = null )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $possibleLabels = array(
    [
      'labels' => [
        'Orange Sim Activation',
        'Activate Provisioned',
        'Activate Promo',
        'Activate Intra Port',
        'Activate Dealer Demo',
        'Mint End Monthly Renewal',
        'Mint Mid Monthly Renewal',
        'Monthly Renewal and Change Plan',
        'Monthly Renewal',
        'Port Activated',
        'Activate' // keep at bottom
      ],
      'params' => [
        'source'    => 'SPEND',
        'reference' => 'Monthly Fee'
      ]
    ],

    [
      'labels' => ['Reactivate from Suspend'],
      'params' => [
        'source'    => 'SPEND',
        'reference' => 'REACTIVATION_PAYMENT'
      ]
    ],

    [
      'labels' => ['Change Plan Mid-Cycle'],
      'params' => [
        'source'    => 'SPEND',
        'reference' => 'PLAN UPGRADE'
      ]
    ]
  );

  foreach ($possibleLabels as $labelBlock)
  {
    foreach ($labelBlock['labels'] as $possibleLabel)
    {
      if (strpos($currentTransitionData['transition_label'], $possibleLabel) === 0)
        $params = array_merge($params, $labelBlock['params']);
    }
  }

  $return = func_sweep_stored_value_explicit(
    $customerData['customer_id'],
    $params['source'],
    $params['reference']
  );

  if ( ! $return['success'])
  {
    $error = 'Failed to sweep stored value';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Spend from balance
 *
 * @return \Outcome Object
 */
function spendFromBalanceExplicit( array $customerData, $currentTransitionData, $action_uuid, $params = null )
{
  $outcome = new \Outcome;

  // customer is using line credits
  if (
    isset($currentTransitionData['context']['use_line_credit'])
    && $currentTransitionData['context']['use_line_credit']
  ) {
    $outcome->succeed();
    return $outcome;
  }
  // end customer is using line credits

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $possibleLabels = array(
    [
      'labels' => [
        'Reactivate from Suspend',
        'Activate Promo'
      ],
      'params' => [
        'detail'    => 'Reactivation',
        'reason'    => 'Reactivation',
        'reference' => 'REACTIVATION_PAYMENT',
        'source'    => 'SPEND'
      ]
    ],

    [
      'labels' => [ 'Activate Provisioned' ],
      'params' => [
        'detail'    => 'Activation',
        'reason'    => 'Provisioned Activation',
        'reference' => 'REACTIVATION_PAYMENT',
        'source'    => 'SPEND'
      ]
    ],

    [
      'labels' => [
        'Orange Sim Activation',
        'Activate Intra Port',
        'Activate Dealer Demo',
        'Activate',
        'Port Activated'
      ],
      'params' => [
        'detail'    => 'First Monthly Fee',
        'reason'    => 'Provisioned Activation',
        'reference' => 'ACTIVATION_PAYMENT',
        'source'    => 'SPEND'
      ]
    ],

    [
      'labels' => [ 'Monthly Renewal and Change Plan' ],
      'params' => [
        'detail'    => 'Plan Change Renewal',
        'reason'    => 'Monthly Renewal into Plan ' . get_plan_from_cos_id($currentTransitionData['to_cos_id']),
        'reference' => 'SPEND',
        'source'    => 'RENEWAL_PAYMENT'
      ]
    ],

    [
      'labels' => [
        'Monthly Renewal',
        'Mint Mid Monthly Renewal',
        'Mint End Monthly Renewal',
        'End Monthly Renewal'
      ],
      'params' => [
        'detail'    => 'Monthly Renewal',
        'reason'    => 'Monthly Renewal',
        'reference' => 'RENEWAL_PAYMENT',
        'source'    => 'SPEND'
      ]
    ],

    [
      'labels' => [ 'Change Plan Mid-Cycle' ],
      'params' => [
        'detail'    => 'Change Plan to ' . get_plan_from_cos_id($currentTransitionData['to_cos_id']),
        'reason'    => 'Change Plan to ' . get_plan_from_cos_id($currentTransitionData['to_cos_id']),
        'reference' => 'DELTA_PAYMENT',
        'source'    => 'SPEND'
      ]
    ]
  );

  foreach ($possibleLabels as $labelBlock)
  {
    foreach ($labelBlock['labels'] as $possibleLabel)
    {
      if (strpos($currentTransitionData['transition_label'], $possibleLabel) === 0)
        $params = array_merge($params, $labelBlock['params']);
    }
  }

  $newPlanCost = get_plan_cost_from_cos_id($currentTransitionData['to_cos_id']);
  // if a mid-cycle change, calculate delta
  if ($params['reference'] == 'DELTA_PAYMENT')
  {
    $oldPlanCost = get_plan_cost_from_cos_id($currentTransitionData['from_cos_id']);
    $newPlanCost -= $oldPlanCost;

    if (\Ultra\UltraConfig\isMintPlan($currentTransitionData['from_cos_id']))
    {
      $deltaCostResult = \calculateMintPlanDeltaCost(
        $customerData['customer_id'],
        get_plan_from_cos_id($currentTransitionData['from_cos_id']),
        get_plan_from_cos_id($currentTransitionData['to_cos_id'])
      );

      $newPlanCost = $deltaCostResult->get_data_key('amount_owed');
    }
  }
  $cost = $newPlanCost / 100;

  $return = func_spend_from_balance_explicit(
    $customerData['customer_id'],
    $cost,
    $params['detail'],
    $params['reason'],
    $params['source'],
    $params['reference'],
    $currentTransitionData['transition_uuid']
  );

  if ( ! $return['success'])
  {
    $error = 'Failed to spend from balance';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Records first activation in the htt_plan_tracker table
 *
 * @return \Outcome Object
 */
function httPlanTrackerFirstActivation( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = func_htt_plan_tracker_first_activation($customerData['customer_id'], $currentTransitionData['to_cos_id']);

  if ( ! $return['success'])
  {
    $error = 'Failed track first activation';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Sets the packaged_balance1 and packaged_balance2 fields of the accounts table for a user, based on their current cos_id
 *
 * @return \Outcome Object
 */
function setMinutes( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $planName = get_plan_name_from_short_name(get_plan_from_cos_id($currentTransitionData['to_cos_id']));
  $planConfig = \Ultra\UltraConfig\getUltraPlanConfiguration($planName);

  $return = func_set_minutes(
    $customerData['customer_id'],
    $planConfig['intl_minutes'],
    $planConfig['unlimited_intl_minutes']
  );

  if ( ! $return['success'])
  {
    $error = 'Failed set balance fields';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * send port in success sms
 *
 * @return \Outcome Object
 */
function sendSMSWifiCallingActivation( array $customerData, $currentTransitionData, $action_uuid, $params )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  // function always returns success
  $return = \sendSMSWifiCallingActivation($customerData['customer_id']);

  $outcome->succeed();

  return $outcome;
}


/**
 * send port in success sms
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSPortSuccess( array $customerData, $currentTransitionData, $action_uuid, $params )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  // function always returns success
  $return = funcSendExemptCustomerSMSPortSuccess(array('customer_id' => $customerData['customer_id']), $currentTransitionData['to_cos_id']);

  $outcome->succeed();

  return $outcome;
}

/**
 * Sends an SMS to the user for new phone activation
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSActivationNewPhone( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  $return = funcSendExemptCustomerSMSActivationNewPhone(array('customer_id' => $customerData['customer_id']));

  if ( ! $return['success'])
  {
    $error = 'Failed to send SMS activation new phone';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * sendExemptCustomerSMSActivateHelp
 *
 * Sends an SMS to the user for activation help
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSActivateHelp( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  $return = funcSendExemptCustomerSMSActivateHelp(array('customer_id' => $customerData['customer_id']));

  if ( ! $return['success'])
  {
    $error = 'Failed to send SMS activate help';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * sendExemptCustomerSMSActivatePlanInfo
 *
 * Sends an SMS to the user for activation plan information
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSActivatePlanInfo( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  $return = funcSendExemptCustomerSMSActivatePlanInfo(array(
    'customer_id' => $customerData['customer_id'],
    'cos_id'      => $currentTransitionData['to_cos_id']
  ));

  if ( ! $return['success'])
  {
    $error = 'Failed to send SMS activate plan info';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * sendExemptCustomerSMSActivateDataHelp
 *
 * Sends an SMS to the user for activation data help
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSActivateDataHelp( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  $return = funcSendExemptCustomerSMSActivateDataHelp(array('customer_id' => $customerData['customer_id']));

  if ( ! $return['success'])
  {
    $error = 'Failed to send SMS activate data help';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * portAddToHttUltraMsisdn
 *
 * Add a new row into HTT_ULTRA_MSISDN and ULTRA.HTT_ACTIVATION_HISTORY
 *
 * @return \Outcome Object
 */
function portAddToHttUltraMsisdn( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $success = port_add_to_htt_ultra_msisdn($customerData['customer_id'], $currentTransitionData['transition_uuid']);

  if ( ! $success)
  {
    $error = 'Failed to add msisd records';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * resetZeroMinutes
 *
 * Resets a user's zero minutes given a customer_id and cos_id
 *
 * @return \Outcome Object
 */
function resetZeroMinutes( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = func_reset_zero_minutes($customerData['customer_id'], $currentTransitionData['to_cos_id']);

  if ( ! $return['success'])
  {
    $error = 'Failed to reset zero minutes';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * sendExemptCustomerSMSProvisionedReminder
 *
 * Sends an SMS to a customer for provisioned reminder
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSProvisionedReminder( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = funcSendExemptCustomerSMSProvisionedReminder(array(
    'customer_id' => $customerData['customer_id'],
    'cos_id'      => $currentTransitionData['to_cos_id']
  ));

  if ( ! $return['success'])
  {
    $error = 'Failed to send SMS';
    \logError($error);
    $outcome->add_errors_and_code($error, 'IN0002');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * sendActivationEmail
 *
 * Sends an activation email
 *
 * @return \Outcome Object
 */
function sendActivationEmail( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');
  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if (\Ultra\Lib\Flex::isFlexPlan($currentTransitionData['to_cos_id'])) {
    send_flex_activation_email($customerData['customer_id']);
  } else {
    send_activation_email($customerData['customer_id'], null, $currentTransitionData['transition_label']);
  }

  // if ( ! $success)
  // {
  //   $error = 'Failed sending activation email';
  //   \logError($error);
  //   $outcome->add_error($error);
  // }
  // else
  
  $outcome->succeed();

  return $outcome;
}

/**
 * validateOrangeSim
 *
 * Validates an orange sim
 *
 * @return \Outcome Object
 */
function validateOrangeSim( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  $result = func_validate_orange_sim($customerData['customer_id']);

  if ( ! $result->is_success())
  {
    if (count($errors = $result->get_errors()))
    {
      \logError($errors[0]);
      $outcome->add_error($errors[[0]]);
    }
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * multiMonthEnrollment
 *
 * Enroll in MULTI_MONTH for customer_id and current_iccid
 *
 * @return \Outcome Object
 */
function multiMonthEnrollment( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  // if (empty($customerData['current_iccid']))
  if (empty($customerData['current_iccid']))
    $outcome->add_errors_and_code('Missing iccid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = funcMultiMonthEnrollment($customerData['customer_id'], $customerData['current_iccid']);

  if ( ! $result->is_success())
  {
    if (count($errors = $result->get_errors()))
    {
      \logError($errors[0]);
      $outcome->add_error($errors[[0]]);
    }
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * inventory_individual_ship_sim
 * create a replacement SIM shipment order for a customer via default fullfillment provider
 * validate subscriber shipping address, ship product type based on subscriber's brand
 *
 * @return \Outcome Object
 */
function inventoryIndividualShipSIM( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = inventory_individual_ship_sim($customerData['customer_id'], $currentTransitionData['transition_uuid']);

  if ( ! $return['success'])
  {
    $error = 'Failed to create SIM shipment order';
    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/*
 * update demo line DB records upon successfull ICCID activation
 *
 * @return \Outcome Object
 */
function finalizeDemoLineActivation( array $customerData, $currentTransitionData, $action_uuid, $params = null)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['context']['demo_line']))
    $outcome->add_errors_and_code('Missing demo_line', 'MP0001');

  if (empty($currentTransitionData['context']['email']))
    $outcome->add_errors_and_code('Missing email', 'MP0001');

  if (empty($currentTransitionData['context']['activations']))
    $outcome->add_errors_and_code('Missing activations', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = \Ultra\Lib\DemoLine\finalizeActivation(
    $customerData['customer_id'],
    $currentTransitionData['context']['demo_line'],
    $currentTransitionData['context']['email'],
    $currentTransitionData['context']['activations']
  );

  if ( ! $return['success'])
  {
    $error = 'Error updating demo line records to finalize activation';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * queue up MakeItSo MakeitsoIntraPort
 *
 * @return \Outcome Object
 */
function mvneIntraPort( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($customerData['current_mobile_number']))
    $outcome->add_errors_and_code('Missing current_mobile_number', 'MP0001');

  if (empty($currentTransitionData['context']['old_iccid']))
    $outcome->add_errors_and_code('Missing old_iccid', 'MP0001');

  if (empty($currentTransitionData['context']['port_in_iccid']))
    $outcome->add_errors_and_code('Missing port_in_iccid', 'MP0001');

  if (empty($currentTransitionData['context']['port_in_account_number']))
    $outcome->add_errors_and_code('Missing port_in_account_number', 'MP0001');

  if (empty($currentTransitionData['context']['old_brand_id']))
    $outcome->add_errors_and_code('Missing old_brand_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = \mvneIntraPort(
    $customerData['customer_id'],
    $customerData['current_mobile_number'],
    $currentTransitionData['context']['old_iccid'],
    $currentTransitionData['context']['port_in_iccid'],
    get_plan_from_cos_id($currentTransitionData['to_cos_id']),
    $customerData['preferred_language'],
    $currentTransitionData['to_plan_state'],
    $currentTransitionData['context']['port_in_account_number'],
    $currentTransitionData['context']['old_brand_id']
  );

  if ( ! $return['success'])
  {
    $error = 'Error running mvneIntraPort';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Execute mwCancelPortIn only if there is a port in progress for the given current_mobile_number
 *
 * @return \Outcome Object
 */
function mvneCancelPortIn( array $customerData, $currentTransitionData, $action_uuid, $params = NULL)
{
  $outcome = new \Outcome;

  if (empty($customerData['current_mobile_number']))
    $outcome->add_errors_and_code('Missing current_mobile_number', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = \mvneCancelPortIn(
    $customerData['current_mobile_number'],
    $currentTransitionData['transition_uuid'],
    $action_uuid
  );

  if (count($errors = $result->get_errors()))
  {
    $outcome->add_error($errors[0]);
    \logError($errors[0]);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * disable entries in ULTRA.CC_HOLDERS for customer
 *
 * @return \Outcome Object
 */
function disableCCHoldersEntries( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = disable_cc_holders_entries(['customer_id' => $customerData['customer_id']]);

  if (count($errors = $result->get_errors()))
  {
    $outcome->add_error($errors[0]);
    \logError($errors[0]);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * ULTRA.PROMOTIONAL_PLANS.INCLUDED_PLAN_CYCLES : If > 1, add cycles-1 months value to stored_value
 * ULTRA.PROMOTIONAL_PLANS.ADDITIONAL_MESSAGES  : send all additional messages
 *
 * @return \Outcome Object
 */
function ultraPromotionalPlansPostActivation( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = ultra_promotional_plans_post_activation($customerData['customer_id']);

  if ( ! $return['success'])
  {
    $error = 'Error updating ULTRA.PROMOTIONAL_PLANS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * send disabling/suspended SMS to customer
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSDisabling( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = funcSendExemptCustomerSMSDisabling([ 'customer_id' => $customerData['customer_id']]);

  if ( ! $return['success'])
  {
    $error = 'Error sending disabling SMS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Mint send disabling/suspended SMS to customer
 *
 * @return \Outcome Object
 */
function sendMintExemptCustomerSMSDisabling( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = funcSendMintExemptCustomerSMSDisabling(['customer_id' => $customerData['customer_id']]);

  if ( ! $return['success'])
  {
    $error = 'Error sending disabling SMS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * updates HTT_PLAN_TRACKER for renewal
 *
 * @return \Outcome Object
 */
function httPlanTrackerPlanRenewal( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = func_htt_plan_tracker_plan_renewal($customerData['customer_id'], $currentTransitionData['to_cos_id']);
  
  if ( ! $return['success'])
  {
    $error = 'Error updating htt plan tracker';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * set MONTHLY_RENEWAL_TARGET to NULL for customer
 *
 * @return \Outcome Object
 */
function resetMonthlyRenewalTarget( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = func_reset_monthly_renewal_target($customerData['customer_id']);

  if ( ! $return['success'])
  {
    $error = 'Error resetting monthly renewal target for customer';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * send monthly plan renewal SMS to customer
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSMonthlyPlanRenewal( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = funcSendExemptCustomerSMSMonthlyPlanRenewal([ 'customer_id' => $customerData['customer_id']]);

  if ( ! $return['success'])
  {
    $error = 'Error sending monthly plan renewal SMS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
* send monthly plan renewal SMS to a MINT customer
*
* @return \Outcome Object
*/
function sendExemptMintCustomerSMSMonthlyPlanRenewal( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = sendSMSMonthlyPlanRenewalMint([
    'customer_id' => $customerData['customer_id'],
    'plan' => get_plan_from_cos_id($currentTransitionData['to_cos_id'])
    ]);

  if ( ! $return['success'])
  {
    $error = 'Error sending monthly plan renewal SMS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

function sendExemptMintCustomerEmailMonthlyPlanRenewal(array $customerData, $currentTransitionData, $action_uuid, $params = NULL)
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = sendMintMonthlyPlanRenewalEmail([
    'customer_id' => $customerData['customer_id'],
    'plan' => get_plan_from_cos_id($currentTransitionData['to_cos_id'])
  ]);

  if (!$return['success'])
  {
    $error = 'Error sending monthly plan renewal Email';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    // $outcome->add_error($error);
  }

  // dont fail transition if failed sending email
  $outcome->succeed();

  return $outcome;
}

/**
 * updates destination origin map for cancellation
 *
 * @return \Outcome Object
 */
function destinationOriginMapCancellationByCustomerId( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = destination_origin_map_cancellation_by_customer_id($customerData['customer_id']);

  if ( ! $return['success'])
  {
    $error = 'Error updating destination origin map';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * send monthly plan change SMS to customer
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSMonthlyPlanChange( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = funcSendExemptCustomerSMSMonthlyPlanChange([
    'customer_id' => $customerData['customer_id'],
    'plan_to'     => get_plan_from_cos_id($currentTransitionData['to_cos_id'])
  ]);

  if ( ! $return['success'])
  {
    $error = 'Error sending monthly plan change SMS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * MINT send monthly plan change SMS to customer
 *
 * @return \Outcome Object
 */
function sendMintExemptCustomerSMSMonthlyPlanChange( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = funcMintSendExemptCustomerSMSMonthlyPlanChange([
    'customer_id' => $customerData['customer_id'],
    'plan_from' => $currentTransitionData['from_cos_id'],
    'plan_to' => $currentTransitionData['to_cos_id']
  ]);

  if ( ! $return['success'])
  {
    $error = 'Error sending monthly plan change SMS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Change Plan Mid-Cycle L49 to L59 (change plan not during monthly renewal)
 * If packaged_balance1 >300000 ($5) then the value needs to be set to 300000
 *
 * @return \Outcome Object
 */
function midcyclePackagedBalanceCheck( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = internal_func_midcycle_packaged_balance_check($customerData['customer_id']);

  if ( ! $return['success'])
  {
    $error = 'Error on midcycle packaged balance check';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Set the value of package_balance1
 *
 * @return \Outcome Object
 */
function setDeltaMinutes( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $planName = get_plan_name_from_short_name(get_plan_from_cos_id($currentTransitionData['to_cos_id']));
  $planConfig = \Ultra\UltraConfig\getUltraPlanConfiguration($planName);

  $return = set_delta_minutes(
    $customerData['customer_id'],
    $planConfig['talk_minutes'],
    $planConfig['intl_minutes']
  );

  if ( ! $return['success'])
  {
    $error = 'Error setting delta minutes';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}


/**
 * records COS_ID in HTT_PLAN_TRACKER
 *
 * @return \Outcome Object
 */
function httPlanTrackerPlanChange( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = func_htt_plan_tracker_plan_change(
    $customerData['customer_id'],
    $currentTransitionData['to_cos_id']
  );

  if ( ! $return['success'])
  {
    $error = 'Error updating htt plan tracker';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Sends an immediate plan change SMS
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSImmediatePlanChange( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = funcSendExemptCustomerSMSImmediatePlanChange([
    'customer_id' => $customerData['customer_id'],
    'plan_from'   => get_plan_from_cos_id($currentTransitionData['from_cos_id']),
    'plan_to'     => get_plan_from_cos_id($currentTransitionData['to_cos_id'])
  ]);

  if ( ! $return['success'])
  {
    $error = 'ERROR sending immediate plan change SMS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();


  return $outcome;
}

/**
 * Invokes enqueueUpgradePlan for immediate plan switches
 *
 * @return \Outcome Object
 */
function mvneChangePlanImmediate( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if (empty($currentTransitionData['transition_uuid']))
    $outcome->add_errors_and_code('Missing transition_uuid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = \mvneChangePlanImmediate(
    $customerData['customer_id'],
    get_plan_from_cos_id($currentTransitionData['to_cos_id']),
    $currentTransitionData['transition_uuid'],
    $action_uuid
  );

  if ( ! $return['success'])
  {
    $error = 'ERROR running mvneChangePlanImmediate';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * supports claiming of credit card and auto enrollment if ONLINE_ORDERS row exists
 *
 * @return \Outcome Object
 */
function onlineAutoEnroll( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($customerData['current_iccid']))
    $outcome->add_errors_and_code('Missing iccid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = online_auto_enroll($customerData['customer_id'], $customerData['current_iccid']);
  if (count($errors = $result->get_errors()))
  {
    $outcome->add_error($errors[0]);
    \logError($errors[0]);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Send port in success email to customer
 *
 * @return \Outcome Object
 */
function sendPortInSuccessEmail( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');
  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  if (\Ultra\Lib\Flex::isFlexPlan($currentTransitionData['to_cos_id'])) {
    send_flex_activation_email($customerData['customer_id'], true, $currentTransitionData['from_plan_state']);
    $outcome->succeed();
  } else {
    // always returns success == true
    $success = send_portin_success_email($customerData['customer_id']);
    if ($success)
      $outcome->succeed();
  }

  return $outcome;
}

/**
 * Send port in failure email to customer
 *
 * @return \Outcome Object
 */
function sendPortInFailureEmail( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  // always returns success == true
  $success = send_portin_failure_email($customerData['customer_id']);
  if ($success)
    $outcome->succeed();

  return $outcome;
}

/**
 * Updates
 * - ULTRA.HTT_ACTIVATION_HISTORY
 * - HTT_CUSTOMERS_OVERLAY_ULTRA.MVNE
 * - HTT_ULTRA_MSISDN.MVNE
 *
 * @return \Outcome Object
 */
function logNewIccidInActivationHistory( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($customerData['current_iccid']))
    $outcome->add_errors_and_code('Missing iccid', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = log_new_iccid_in_activation_history($customerData['customer_id'], $customerData['current_iccid']);
  
  if ( ! $return['success'])
  {
    $error = 'DB error logging new iccid in activation history';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Enroll in BOGO if ICCID is within a specific range
 *
 * @return \Outcome Object
 */
function bogoEnrollment( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($customerData['current_iccid']))
    $outcome->add_errors_and_code('Missing iccid', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing cos_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = funcBogoEnrollment($customerData['customer_id'], $customerData['current_iccid'], $currentTransitionData['to_cos_id']);

  if (count($errors = $result->get_errors()))
  {
    $outcome->add_error($errors[0]);
    \logError($errors[0]);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * validate that given customer is acquired via Web POS and refund plan cost back to ACCOUNTS.BALANCE
 *
 * @return \Outcome Object
 */
function refundWebPosCustomer( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = \refundWebPosCustomer($customerData['customer_id']);
  if (count($errors = $result->get_errors()))
  {
    $outcome->add_error($errors[0]);
    \logError($errors[0]);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * checks multi-month customer
 * downgrades to monthly COS_ID if 0 months left
 *
 * @return \Outcome Object
 */
function checkFinalMultiMonth( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $result = funcCheckFinalMultiMonth($customerData['customer_id']);
  if (count($errors = $result->get_errors()))
  {
    $outcome->add_error($errors[0]);
    \logError($errors[0]);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * sends a plan downgraded SMS to customer
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSPlanSuspendedUpgraded( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = funcSendExemptCustomerSMSPlanSuspendedUpgraded([
    'customer_id' => $customerData['customer_id'],
    'plan_cost'   => get_plan_cost_from_cos_id($currentTransitionData['to_cos_id'])
  ]);

  if ( ! $return['success'])
  {
    $error = 'Failed to send suspended upgraded SMS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * sends an un-suspend SMS to customer
 *
 * @return \Outcome Object
 */
function sendExemptCustomerSMSUnsuspend( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome;

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if ($outcome->has_errors())
    return $outcome;

  $return = funcSendExemptCustomerSMSUnsuspend(['customer_id' => $customerData['customer_id']]);
  if ( ! $return['success'])
  {
    $error = 'Failed to send unsuspended SMS';
    if (isset($return['errors']) && count($errors = $return['errors']))
      $error = $errors[0];

    \logError($error);
    $outcome->add_error($error);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * sends an un-suspend SMS to customer
 *
 * @return \Outcome Object
 */
function multiMonthOverlayEnrollment( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome();

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  $result = \multiMonthOverlayEnrollment($customerData['customer_id'], $currentTransitionData['to_cos_id']);
  if (count($errors = $result->get_errors()))
  {
    $outcome->add_error($errors[0]);
    \logError($errors[0]);
  }
  else
    $outcome->succeed();


  return $outcome;
}

/**
 * sends an un-suspend SMS to customer
 *
 * @return \Outcome Object
 */
function checkMonthOverlay( array $customerData, $currentTransitionData, $action_uuid, $params = NULL )
{
  $outcome = new \Outcome();

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  $result = \checkMonthOverlay($customerData['customer_id'], $currentTransitionData['to_cos_id']);
  if (count($errors = $result->get_errors()))
  {
    $outcome->add_error($errors[0]);
    \logError($errors[0]);
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * Set the customers default throttle speed.
 *
 * @param array $customerData
 * @param $currentTransitionData
 * @param $action_uuid
 * @param null $params
 * @return \Outcome Object
 */
function setDefaultThrottleSpeed(array $customerData, $currentTransitionData, $action_uuid, $params = NULL)
{
  $outcome = new \Outcome();

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  if (empty($currentTransitionData['to_cos_id']))
    $outcome->add_errors_and_code('Missing to_cos_id', 'MP0001');

  $sql = sprintf("
    INSERT INTO ULTRA.CUSTOMER_OPTIONS (CUSTOMER_ID, OPTION_ATTRIBUTE, OPTION_VALUE) 
    VALUES(%d, 'LTE_SELECT.CARRIER_SELECTED_DEFAULT', 'Full Speed')",
    $customerData['customer_id']
  );

  if (!run_sql_and_check($sql))
  {
    $error = 'Unable to record default throttle speed in ULTRA.CUSTOMER_OPTIONS.';
    \logError($error);
    $outcome->add_errors_and_code($error, 'DB0007');
  }
  else
    $outcome->succeed();

  return $outcome;
}

/**
 * migrate customers to default throttle speed.
 *
 * @param array $customerData
 * @param $currentTransitionData
 * @param $action_uuid
 * @param null $params
 * @return \Outcome Object
 */
function migrateDefaultThrottleSpeed(array $customerData, $currentTransitionData, $action_uuid, $params = NULL)
{
  $outcome = new \Outcome();

  if (empty($customerData['customer_id']))
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');

  $overlay = (array) get_ultra_customer_from_customer_id($customerData['customer_id'], ['GROSS_ADD_DATE']);
  $option = customer_options_get_throttle_speed($customerData['customer_id']);
  $fullSpeed = 'Full Speed';

  // Dont migrate throttle speed for customers who have manually changed it already after August 16, 2017 12:00:00 AM
  if (strtotime($overlay['GROSS_ADD_DATE']) > 1502866800 && (!empty($option) && $option->OPTION_VALUE != $fullSpeed)) {
    $outcome->succeed();
    return $outcome;
  } elseif (!empty($option) && $option->OPTION_VALUE == $fullSpeed) {
    $outcome->succeed();
    return $outcome;
  }

  mvneMakeitsoUpdateThrottleSpeed(
    $customerData['customer_id'],
    get_plan_from_cos_id($currentTransitionData['to_cos_id']),
    'full',
    'LTE_SELECT.CARRIER_MIGRATED_RT_19',
    null,
    null,
    true
  );

  $outcome->succeed();
  return $outcome;
}

/**
 * setup flex plans
 *
 * @param array $customerData
 * @param $currentTransitionData
 * @param $action_uuid
 * @param null $params
 * @return \Outcome Object
 */
function setupFlexPlans(array $customerData, $currentTransitionData, $action_uuid, $params = NULL)
{
  $outcome = new \Outcome();

  // succeed if not flex plan
  if ( ! \Ultra\Lib\Flex::isFlexPlan($currentTransitionData['to_cos_id']))
  {
    \logDebug(__function__ . ' : not a flex plan');
    $outcome->succeed();
    return $outcome;
  }

  if (empty($customerData['customer_id']))
  {
    $outcome->add_errors_and_code('Missing customer_id', 'MP0001');
    return $outcome;
  }

  try
  {
    // if there is an error, escalate but let transition continue
    $result = \Ultra\Lib\Flex::activateFamilyMembers($customerData['customer_id'], true);
    if ( ! $result->is_success())
    {
      $errors = $result->get_errors();
      $error  = count($errors)
        ? "ESCALATION FLEX {$errors[0]}"
        : "ESCALATION FLEX error activating family member(s)";
      \logError($error);
    }

    // be sure connect to ultra data
    teldata_change_db();

    $outcome->succeed();
  }
  catch (\Exception $e)
  {
    $outcome->add_errors_and_code($e->getMessage(), 'IN0002');
  }

  return $outcome;
}

function makeitsoSetupFlex(array $customerData, $currentTransitionData, $action_uuid, $params = NULL)
{
  $outcome = new \Outcome();

  // succeed if not flex plan
  if ( ! \Ultra\Lib\Flex::isFlexPlan($currentTransitionData['to_cos_id']))
  {
    \logDebug(__function__ . ' : not a flex plan');
    $outcome->succeed();
    return $outcome;
  }

  try
  {
    $actionUUID = getNewActionUUID(__FUNCTION__ . ' ' . time());

    $m = new \Ultra\Lib\MVNE\MakeItSo();
    $result = $m->enqueueFlexSetup($actionUUID, $customerData['customer_id']);
    if ( ! $result->is_success())
    {
      $errors = $result->get_errors();
      $error  = (count($errors)) ? $errors[0] : 'Unknown error';
      throw new Exception($error);
    }

    $outcome->succeed();
  }
  catch (\Exception $e)
  {
    $outcome->add_errors_and_code($e->getMessage(), 'IN0002');
  }

  teldata_change_db();

  return $outcome;
}

function syncToBAN(array $customerData, $currentTransitionData, $action_uuid, $params = NULL)
{
  $outcome = new \Outcome();

  // succeed if not flex plan
  if ( ! \Ultra\Lib\Flex::isFlexPlan($currentTransitionData['to_cos_id']))
  {
    \logDebug(__function__ . ' : not a flex plan');
    $outcome->succeed();
    return $outcome;
  }

  try
  {
    $result = \Ultra\Lib\Flex::syncFamilyMemberToBAN($customerData['customer_id']);
    if (!$result->is_success()) {
      \logError('Failed to sync member to BAN');
    }

    $outcome->succeed();
  }
  catch (\Exception $e)
  {
    $outcome->add_errors_and_code($e->getMessage(), 'IN0002');
  }

  return $outcome;
}

function disableAutoRecharge(array $customerData, $currentTransitionData, $action_uuid, $params = NULL)
{
  $outcome = new \Outcome();

  try
  {
    \Ultra\Lib\DB\Customer\updateAutoRecharge($customerData['customer_id'], 0);

    $outcome->succeed();
  }
  catch (\Exception $e)
  {
    $outcome->add_errors_and_code($e->getMessage(), 'IN0002');
  }

  teldata_change_db();

  return $outcome;
}

function cancelFlexOrders(array $customerData, $currentTransitionData, $action_uuid, $params = NULL)
{
  $outcome = new \Outcome();

  // skip if not flex plan
  if ( ! \Ultra\Lib\Flex::isFlexPlan($currentTransitionData['to_cos_id']))
  {
    $outcome->succeed();
    return $outcome;
  }

  try
  {
    $onlineSales = new \Ultra\Lib\Services\OnlineSalesAPI();
    $getOrderResult = $onlineSales->getRetailerOrders($customerData['customer_id'], 'unpaid');

    \dlog('', 'OnlineSales API Result: %s', $getOrderResult);

    if ($getOrderResult->is_failure())
      \dlog('', 'Error retrieving order');
    else
    {
      $orderId = $getOrderResult->data_array['items'][0]['orderId'];

      $cancelOrderResult = $onlineSales->cancelOrder($orderId);

      if ($getOrderResult->is_failure())
        \dlog('', 'Error cancelling order');
      else
        $outcome->succeed();
    }
  }
  catch (\Exception $e)
  {
    $outcome->add_errors_and_code($e->getMessage(), 'IN0002');
  }

  return $outcome;
}

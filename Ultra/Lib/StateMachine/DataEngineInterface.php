<?php

namespace Ultra\Lib\StateMachine;

interface DataEngineInterface
{
  public function storeTransitionAndActions();
  public function loadTransitionAndActionsByUUID( $transition_uuid );
  public function abortTransitionAndActions();
  public function closeAction( $action_uuid );
  public function closeTransition();
  public function loadNextTransitionAndActions();
  public function getNextActionData();
  public function recordActionOutcome( $outcome );
  public function recordActionPending();
  public function updatePlanState( $plan_state , $customer_id );
  public function resetCurrentActionData();
}


<?php

require_once("db.php");
require_once("Ultra/Lib/MQ/ControlChannel.php");

$controlChannel = new \Ultra\Lib\MQ\ControlChannel;

# create Request-Reply ACC MW Control Channels
list($c_outbound,$c_inbound) = $controlChannel->createNewACCMWControlChannels();

echo "($c_outbound,$c_inbound)\n";

# create Request-Reply ULTRA MW Control Channels
list($c_outbound,$c_inbound) = $controlChannel->createNewUltraMWControlSynchChannels();

echo "($c_outbound,$c_inbound)\n";

# I am a Requestor, I want to send an *outbound* message through $c_outbound,
# then I will synchronously wait for a reply the Replier will send through $c_inbound

dlog('',"c_outbound = $c_outbound");
dlog('',"c_inbound  = $c_inbound");

$anOutboundRequestMessage = '[' . time() . '] qwerty ####';

$controlUUID = $controlChannel->sendToControlChannel( $c_outbound , $anOutboundRequestMessage );

dlog('',"controlUUID = $controlUUID");
dlog('',"message     = $anOutboundRequestMessage");

#$anInboundReplyMessage = $controlChannel->waitForControlMessage( $c_inbound , $controlChannel->inboundACCMWControlChannel() );
$anInboundReplyMessage = $controlChannel->waitForControlMessage( $c_inbound , $controlChannel->inboundUltraMWControlChannel() );

if ( $anInboundReplyMessage )
{
  dlog('',"Success: message = $anInboundReplyMessage");
}
else
{
  dlog('',"Failure");
}

?>

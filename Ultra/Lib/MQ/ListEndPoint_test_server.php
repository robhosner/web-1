<?php

require_once("db.php");
require_once("Ultra/Lib/MQ/ListEndPoint.php");

$listEndPoint = new \Ultra\Lib\MQ\ListEndPoint( \Ultra\Lib\MQ\ListEndPoint::SMS_OUTBOUND_SYNCH_LIST );

list( $messageKey , $message ) = $listEndPoint->popMessage();

echo "] messageKey = $messageKey\n";
echo "] message    = $message\n";

if ( $messageKey )
{
  $reply = '{reply}';

  $status = $listEndPoint->replyMessage( $messageKey , $reply );
}


use strict;
use warnings;


use Test::More;


BEGIN { use_ok('Ultra::Lib::MQ::ControlChannel'); }


# instantiate Ultra::Lib::MQ::ControlChannel
my $mq_cc = Ultra::Lib::MQ::ControlChannel->new();

ok( ref $mq_cc eq 'Ultra::Lib::MQ::ControlChannel' , 'Ultra::Lib::MQ::ControlChannel new' );

# create Request-Reply ACC MW Control Channels
my ($c_outbound,$c_inbound) = $mq_cc->createNewACCMWControlChannels();

ok ( $c_outbound && $c_inbound , 'createNewControlSynchChannels' );

ok( 'created' eq $mq_cc->peekControlChannelStatus( $c_outbound ) , 'peekControlChannelStatus created' );

my $anOutboundRequestMessage = 'hello from ControlChannel_test.pl';

# send outbound Request
my $controlUUID = $mq_cc->sendToControlChannel( $c_outbound , $anOutboundRequestMessage );

ok( $controlUUID , 'sendToControlChannel' );

my $peekControlChannelResult = $mq_cc->peekControlChannel( $c_outbound );

ok( $peekControlChannelResult eq $anOutboundRequestMessage , 'peekControlChannel' );

ok( 'filled' eq $mq_cc->peekControlChannelStatus( $c_outbound ) , 'peekControlChannelStatus filled' );

# extract ACC MW outbound message
my $popControlChannelResult = $mq_cc->popControlChannel( $c_outbound , $mq_cc->outboundControlSynchChannel( $mq_cc->controlChannelTypeACCMW() ) );

ok( $popControlChannelResult eq $anOutboundRequestMessage , 'popControlChannel' );

my $anInboundReplyMessage = 'how do you do ControlChannel_test.pl';

# send inbound Request
$controlUUID = $mq_cc->sendToControlChannel( $c_inbound , $anInboundReplyMessage );

ok( $controlUUID , 'sendToControlChannel' );

my $peekControlChannelResult = $mq_cc->peekControlChannel( $c_inbound );

ok( $peekControlChannelResult eq $anInboundReplyMessage , 'peekControlChannel' );

ok( 'filled' eq $mq_cc->peekControlChannelStatus( $c_inbound ) , 'peekControlChannelStatus filled' );

# extract ACC MW inbound message
$popControlChannelResult = $mq_cc->popControlChannel( $c_inbound , $mq_cc->inboundControlSynchChannel( $mq_cc->controlChannelTypeACCMW() ) );

ok( $popControlChannelResult eq $anInboundReplyMessage , 'popControlChannel' );


done_testing();


__END__


Test script for Ultra::Lib::MQ::ControlChannel



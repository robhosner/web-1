<?php

namespace Ultra\Lib\MQ;

include_once('Ultra/Lib/MQ/EndPoint.php');

class ControlChannel extends EndPoint
{
  private function controlChannelTypeACCMW()   { return 'ACC_MW';   }
  private function controlChannelTypeUltraMW() { return 'ULTRA_MW'; }
  private function controlChannelTypeSMSMW()   { return 'SMS_MW';   } // obsolete

  public function outboundControlAsynchChannel($type)
  {
    return $type.'/OUTBOUND/ASYNCH';
  }

  public function inboundControlAsynchChannel($type)
  {
    return $type.'/INBOUND/ASYNCH';
  }

  public function outboundControlSynchChannel($type)
  {
    return $type.'/OUTBOUND/SYNCH';
  }

  public function inboundControlSynchChannel($type)
  {
    return $type.'/INBOUND/SYNCH';
  }

  // obsolete
  public function inboundSMSMWControlChannel()
  {
    return $this->inboundControlSynchChannel( $this->controlChannelTypeSMSMW() );
  }

  // obsolete
  public function outboundSMSMWControlChannel()
  {
    return $this->outboundControlSynchChannel( $this->controlChannelTypeSMSMW() );
  }

  public function inboundACCMWControlChannel()
  {
    return $this->inboundControlSynchChannel( $this->controlChannelTypeACCMW() );
  }

  public function outboundACCMWControlChannel()
  {
    return $this->outboundControlSynchChannel( $this->controlChannelTypeACCMW() );
  }

  public function inboundUltraMWControlChannel()
  {
    return $this->inboundControlSynchChannel( $this->controlChannelTypeUltraMW() );
  }

  public function outboundUltraMWControlChannel()
  {
    return $this->outboundControlSynchChannel( $this->controlChannelTypeUltraMW() );
  }

  // obsolete
  public function createNewSMSMWControlChannels()
  {
    return $this->createNewControlSynchChannels( $this->controlChannelTypeSMSMW() );
  }

  public function createNewACCMWControlChannels()
  {
    return $this->createNewControlSynchChannels( $this->controlChannelTypeACCMW() );
  }

  public function createNewUltraMWControlSynchChannels()
  {
    return $this->createNewControlSynchChannels( $this->controlChannelTypeUltraMW() );
  }

  // create an outbound asynch channel (from UltraMW layer)
  public function createNewUltraMWControlAsynchChannel()
  {
    $newChannelName = $this->getNewChannelName();

    $requestChannelName = 'out_'.$newChannelName;

    $requestChannelName = $this->createControlChannel( $requestChannelName , $this->outboundControlAsynchChannel( $this->controlChannelTypeUltraMW() ) );

    return $requestChannelName;
  }

  // create an outbound asynch channel (from ACCMW layer)
  public function createNewACCMWControlAsynchChannel()
  {
    $newChannelName = $this->getNewChannelName();

    $requestChannelName = 'out_'.$newChannelName;

    $requestChannelName = $this->createControlChannel( $requestChannelName , $this->outboundControlAsynchChannel( $this->controlChannelTypeACCMW() ) );

    return $requestChannelName;
  }

  public function createNewControlSynchChannels( $messageQueue )
  {
    $newChannelName = $this->getNewChannelName();

    $requestChannelName = 'out_'.$newChannelName;
    $replyChannelName   = 'in_'.$newChannelName;

    $requestChannelName = $this->createControlChannel( $requestChannelName , $this->outboundControlSynchChannel( $messageQueue ) );
    $replyChannelName   = $this->createControlChannel( $replyChannelName   , $this->inboundControlSynchChannel( $messageQueue ) );

    return array( $requestChannelName , $replyChannelName );
  }

  public function existsControlChannel($channelName)
  {
    return $this->existsEndPointChannel($channelName,$this->controlChannelPrefix());
  }

  public function getChannelTypeMembers($messageQueue)
  {
    return $this->redis->smembers( $messageQueue );
  }

  public function createControlChannel($channelName,$messageQueue)
  {
    #dlog('',"$channelName , $messageQueue");

    return $this->createEndPointChannel($channelName,$messageQueue,$this->controlChannelPrefix());
  }

  public function peekControlChannel($channelName)
  {
    $return = '';

    if ( $this->redis->exists( $this->controlChannelPrefix() . '_' . $channelName )
      && $this->redis->exists( 'CS_' . $channelName )
      && $this->redis->exists( 'CM_' . $channelName )
      && ( $this->redis->get( 'CS_' . $channelName ) == 'filled' )
    )
    {
      # read the CONTROL_UUID contained in the channel
      $controlUUID = $this->redis->get( 'CM_' . $channelName );

      if ( $controlUUID )
      {
        # get the message pointed by the CONTROL_UUID
        # we assume that a message is a ``true value``
        $return = $this->redis->get( $controlUUID );
      }
    }

    return $return;
  }

  public function peekControlChannelStatus($channelName)
  {
    return $this->peekChannelStatus( $channelName , $this->controlChannelPrefix() );
  }

  public function sendToSMSControlChannel($channelName,$message)
  {
    return $this->sendToControlChannel($channelName,$message);
  }

  public function sendToControlChannel($channelName,$message)
  {
    return $this->sendToEndPointChannel($channelName,$message,$this->controlChannelPrefix());
  }

  public function isControlChannelFilled($controlChannel)
  {
    return ( $this->peekControlChannelStatus( $controlChannel ) == 'filled' );
  }

  public function getNextOutboundSMSMessage()
  {
    $return = array();

    $messageQueue = $this->outboundSMSMWControlChannel();

    dlog('',"messageQueue = $messageQueue");

    $outboundControlSynchChannels = $this->redis->smembers( $messageQueue );

    $exitLoop = FALSE;

    # check all filled channels
    while( ! $exitLoop )
    {
      $nextOutboundControlChannel = '';

      if ( $outboundControlSynchChannels && is_array($outboundControlSynchChannels) && count($outboundControlSynchChannels) )
      {
        $nextOutboundControlChannel = array_shift($outboundControlSynchChannels);
      }

      if ( $nextOutboundControlChannel )
      {
        # there is a message in this outbound ACC MW Control Channel
        if ( $this->isControlChannelFilled( $nextOutboundControlChannel ) )
        {
          # we extract the message
          $message = $this->popControlChannel( $nextOutboundControlChannel , $messageQueue );

          if ( $message )
          {
            # success: we will return the channel name and the message
            $return = array( $nextOutboundControlChannel , $message );

            $exitLoop = TRUE;
          }
        }
      }
      else
      {
        # there are no more outbound ACC MW Control Channels, we exit
        $exitLoop = TRUE;
      }
    }

    return $return;
  }

  /**
   * getNextOutboundMessage
   *
   * Extracts a message and the channel name from a filled outbound Ultra MW Control Channel.
   * Returns an empty array in case of failure.
   *
   * @return array
   */ 
  public function getNextOutboundMessage( $asynchronous=FALSE , $offset=0 )
  {
    $return = array();

    // inner layer (ULTRAMW)
    $messageQueue = ( $asynchronous )
                    ?
                    $this->outboundControlAsynchChannel( $this->controlChannelTypeUltraMW() )
                    :
                    $this->outboundUltraMWControlChannel()
                    ;

    dlog('',"messageQueue = $messageQueue");

    $outboundControlSynchChannels = $this->redis->smembers( $messageQueue );

    $exitLoop = FALSE;

    # check all filled channels
    while( ! $exitLoop )
    {
      $nextOutboundControlChannel = '';

      if ( $outboundControlSynchChannels && is_array($outboundControlSynchChannels) && count($outboundControlSynchChannels) )
      {
// TODO: MVNO-2614 - use $offset and an alternative to array_shift

        $nextOutboundControlChannel = array_shift($outboundControlSynchChannels);
      }

      if ( $nextOutboundControlChannel )
      {
        # there is a message in this outbound ACC MW Control Channel
        if ( $this->isControlChannelFilled( $nextOutboundControlChannel ) )
        {
          # we extract the message
          $message = $this->popControlChannel( $nextOutboundControlChannel , $messageQueue );

          if ( $message )
          {
            # success: we will return the channel name and the message
            $return = array( $nextOutboundControlChannel , $message );

            $exitLoop = TRUE;
          }
        }
      }
      else
      {
        # there are no more outbound ACC MW Control Channels, we exit
        $exitLoop = TRUE;
      }
    }

    return $return;
  }

  /**
   * waitForSMSMWControlMessage
   *
   * @return object of class \Result
   */
  public function waitForSMSMWControlMessage($channelName)
  {
    $messageQueue = $this->inboundSMSMWControlChannel();

    $delays = array( 2,.25,.25,.25,.25,.25,.25,.25,.25 );

    return $this->waitForControlMessage($channelName,$messageQueue,$delays);
  }

  /**
   * waitForUltraMWControlMessage
   *
   * @return object of class \Result
   */
  public function waitForUltraMWControlMessage($channelName,$delays=array())
  {
    $messageQueue = $this->inboundUltraMWControlChannel();

    return $this->waitForControlMessage($channelName,$messageQueue,$delays);
  }

  /**
   * waitForACCMWControlMessage
   *
   * @return object of class \Result
   */
  public function waitForACCMWControlMessage($channelName,$delays=array())
  {
    $messageQueue = $this->inboundACCMWControlChannel();

    return $this->waitForControlMessage($channelName,$messageQueue,$delays);
  }

  /**
   * waitForControlMessage
   *
   * wait synchronously for and inbound message in channel $channelName
   * $messageQueue denotes the Message Queue which the channel belongs to
   *
   * @return object of class \Result
   */
  public function waitForControlMessage($channelName,$messageQueue,$delays=array())
  {
    dlog('',"channelName = $channelName , messageQueue = $messageQueue , delays = %s",$delays);

    $result = new \Result();

    $exitLoop    = FALSE;
    $startTime   = time();
    $timeoutTime = time() + $this->getRequestReplyTimeoutSeconds();

    dlog('',"Start waiting on control channel $channelName...");

    $waitHistory = array();

    while( ! $exitLoop )
    {
      $exitLoop = $this->isControlChannelFilled( $channelName );

      if ( $exitLoop )
      {
        # we got a reply on the Reply Channel

        $result->data_array['message'] = $this->popControlChannel( $channelName , $messageQueue );

        $result->succeed();
      }
      else
      {
        # check for timeout
        if ( $timeoutTime < time() )
        {
          $exitLoop = TRUE;

          dlog('', __CLASS__ . " Timeout after ".$this->getRequestReplyTimeoutSeconds()." seconds on channel $channelName" );

          $result = make_timeout_Result( "waitForCommandMessage timed out after ".$this->getRequestReplyTimeoutSeconds()." seconds" );
        }
      }

      // decide how much time should we sleep
      if ( is_array( $delays ) && count( $delays ) )
        $channelPingFrequencySeconds = array_shift( $delays );
      else
        $channelPingFrequencySeconds = $this->getChannelPingFrequencySeconds();

      $waitHistory[] = $channelPingFrequencySeconds;

      if ( ! $exitLoop )
        // fragment of a second
        usleep( $channelPingFrequencySeconds*1000000 );
    }

    dlog('',"ended after %s seconds - seq : %s",array_sum( $waitHistory ),$waitHistory);
    dlog('',"result = %s",$result);

    return $result;
  }

  public function popControlChannel($channelName,$messageQueue)
  {
    return $this->popEndPointChannel($channelName,$messageQueue,$this->controlChannelPrefix());
  }

  /**
   * fireAndForgetToControlChannel
   *
   * sends $message to Control Channel ; does not wait for reply
   *
   * returns a Result object
   */
  public function fireAndForgetToControlChannel($channelName,$message)
  {
    $result = new \Result();

    // send fire and forget message
    $commandUUID = $this->sendToControlChannel( $channelName , $message );
    dlog('',"commandUUID = $commandUUID");

    if ( $commandUUID )
    {
      $result->succeed();
    }
    else
    {
      $result->fail();
      dlog('',"Failure");
    }

    return $result;
  }

  /**
   * controlChannelPrefix
   *
   * String prefix for control channel names
   *
   * @return string
   */
  public function controlChannelPrefix()
  {
    return 'CN';
  }

  /**
   * getSMSQueueLength
   *
   * @return integer
   */
  public function getSMSQueueLength()
  {
    $queue = $this->redis->smembers( $this->outboundSMSMWControlChannel() );

    return is_array($queue) ? count($queue) : 0 ;
  }
}


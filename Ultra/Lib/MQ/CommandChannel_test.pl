use strict;
use warnings;

use JSON::XS;
use Ultra::Lib::MQ::CommandChannel;

my $json_coder = JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

my $mqCommandChannelObject = Ultra::Lib::MQ::CommandChannel->new( JSON_CODER => $json_coder );

print $mqCommandChannelObject->outboundASPSOAPCommandSynchChannel()."\n";

__END__

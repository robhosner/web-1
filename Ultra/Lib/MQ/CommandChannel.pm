package Ultra::Lib::MQ::CommandChannel;


use strict;
use warnings;


use Time::HiRes qw( usleep );


use base qw(Ultra::Lib::MQ::EndPoint);


=head1 NAME

Ultra::Lib::MQ::CommandChannel

=head1 Command Channel

  Subclass of Ultra::Lib::MQ::EndPoint with specific methods related with Command Channel synchronous and asynchronous communication

=head1 SYNOPSIS

  use Ultra::Lib::MQ::CommandChannel;

  my $mqCommandChannelObject = Ultra::Lib::MQ::CommandChannel->new( JSON_CODER => $json_coder );

  # select an outbound request channel + message
  my $getNextOutboundMessageResult = $mqCommandChannelObject->getNextOutboundASPSOAPSynchMessage();

  my ( $nextOutboundCommandChannel , $message ) = @$getNextOutboundMessageResult;

  my $decodedData = $mqCommandChannelObject->extractFromMessage($message);

  my $nextInboundCommandChannel = $mqCommandChannelObject->toInbound( $nextOutboundCommandChannel );

  # response
  my $uuid = $mqCommandChannelObject->sendToCommandChannel( $nextInboundCommandChannel , $inboundResponseMessage );

=cut

=head1 METHODS

=head4

  ASP SOAP Command Channel Type

=cut
sub commandChannelTypeASPSOAP { return 'ASP_SOAP'; }


=head4

  Outbound Command Synch Channel

=cut
sub outboundCommandSynchChannel
{
  my ($this,$type) = @_;

  return $type.'/OUTBOUND/ASP/SYNCH';
}


=head4

  Inbound Command Synch Channel

=cut
sub inboundCommandSynchChannel
{
  my ($this,$type) = @_;

  return $type.'/INBOUND/ASP/SYNCH';
}


=head4

  Outbound Command Asynch Channel (to delete?)

=cut
sub outboundCommandAsynchChannel
{
  my ($this,$type) = @_;

  return $type.'/OUTBOUND/ASP/ASYNCH';
}


=head4

  Inbound Command Asynch Channel (to delete?)

=cut
sub inboundCommandAsynchChannel
{
  my ($this,$type) = @_;

  return $type.'/INBOUND/ASP/ASYNCH';
}


=head4

  ASP SOAP Inbound Command Synch Channel

=cut
sub inboundASPSOAPCommandSynchChannel
{
  my ($this) = @_;

  return $this->inboundCommandSynchChannel( $this->commandChannelTypeASPSOAP() );
}


=head4

  ASP SOAP Outbound Command Synch Channel

=cut
sub outboundASPSOAPCommandSynchChannel
{
  my ($this) = @_;

  return $this->outboundCommandSynchChannel( $this->commandChannelTypeASPSOAP() );
}


=head4

  ASP SOAP Inbound Command Asynch Channel (to delete?)

=cut
sub inboundASPSOAPCommandAsynchChannel
{
  my ($this) = @_;

  return $this->inboundCommandAsynchChannel( $this->commandChannelTypeASPSOAP() );
}


=head4

  ASP SOAP Outbound Command Asynch Channel (to delete?)

=cut
sub outboundASPSOAPCommandAsynchChannel
{
  my ($this) = @_;

  return $this->outboundCommandAsynchChannel( $this->commandChannelTypeASPSOAP() );
}


=head4

  Creates a new ASP SOAP inbound channel for asynchronous callbacks

=cut
sub createNewInboundASPSOAPCommandAsynchChannel
{
  my ($this) = @_;

  my $newChannelName = $this->_getNewChannelName();

  my $inboundChannelName = 'in_'.$newChannelName;

  $inboundChannelName = $this->createCommandChannel( $inboundChannelName , $this->inboundASPSOAPCommandAsynchChannel() );

  return $inboundChannelName;
}


=head4

  Creates new ASP SOAP synchronous channels (request and reply)

=cut
sub createNewASPSOAPCommandSynchChannels
{
  my ($this) = @_;

  my $newChannelName = $this->_getNewChannelName();

  my $requestChannelName = 'out_'.$newChannelName;
  my $replyChannelName   = 'in_'.$newChannelName;

  $requestChannelName = $this->createCommandChannel( $requestChannelName , $this->outboundASPSOAPCommandSynchChannel() );
  $replyChannelName   = $this->createCommandChannel( $replyChannelName   , $this->inboundASPSOAPCommandSynchChannel() );

  return ( $requestChannelName , $replyChannelName );
}


=head4

  Waits for an inbound ASP SOAP message in channel $channelName (TODO: only PHP?)

=cut
sub waitForInboundASPSOAPCommandMessage
{
  my ($this,$channelName) = @_;

  my $messageQueue = $this->inboundASPSOAPCommandSynchChannel();

  return $this->waitForCommandMessage($channelName,$messageQueue);
}


=head4

  Waits for a $messageQueue message in channel $channelName

=cut
sub waitForCommandMessage
{
  my ($this,$channelName,$messageQueue) = @_;

  $this->log("waitForCommandMessage - $channelName,$messageQueue");

  my $return = '';

  my $exitLoop = 0;

  my $timeoutTime = time() + $this->getRequestReplyTimeoutSeconds();

  my $channelPingFrequencySeconds = $this->getChannelPingFrequencySeconds();

  while( ! $exitLoop )
  {
    $exitLoop = $this->isCommandChannelFilled( $channelName );

    if ( $exitLoop )
    {
      # we got a reply on the Reply Channel
      $return = $this->popCommandChannel( $channelName , $messageQueue );
    }
    else
    {
      $this->log("Waiting on channel $channelName...");

      # check for timeout
      if ( $timeoutTime < time )
      {
        $exitLoop = 1;

        $this->log("Timeout after ".$this->getRequestReplyTimeoutSeconds()." seconds");

        #TODO:
        #$this->addError("waitForCommandMessage timed out after ".$this->getRequestReplyTimeoutSeconds()." seconds");
      }
    }

    if ( ! $exitLoop )
    {
      sleep $channelPingFrequencySeconds;
    }
  }

  return $return;
}


=head4

  Returns the next available message in $channel

=cut
sub getNextOutboundASPSOAPMessage
{
  my ($this,$channel) = @_;

  my $return = [];

  my $outboundCommandChannels = $this->_redis->smembers( $channel );

  my $exitLoop = 0;

  # check all filled channels
  while( ! $exitLoop )
  {
    my $nextOutboundCommandChannel = '';

    if ( $outboundCommandChannels )
    {
      $nextOutboundCommandChannel = shift @$outboundCommandChannels;
    }

    if ( $nextOutboundCommandChannel )
    {
      #$this->log("getNextOutboundASPSOAPMessage nextOutboundCommandChannel = $nextOutboundCommandChannel");

      # there is a message in this outbound ASPSOAP Channel
      if ( $this->isCommandChannelFilled( $nextOutboundCommandChannel ) )
      {
        # we extract the message
        my $message = $this->popCommandChannel( $nextOutboundCommandChannel , $channel );

        if ( $message )
        {
          # success: we will return the channel name and the message
          $return = [ $nextOutboundCommandChannel , $message ];

          $exitLoop = 1;
        }
      }
    }
    else
    {
      # there are no more outbound ACC MW Control Channels, we exit
      $exitLoop = 1;
    }
  }

  return $return;
}


=head4

  Returns the next available message in the Asynch ASP SOAP Channel

=cut
sub getNextOutboundASPSOAPAsynchMessage
{
  my ($this) = @_;

  # outer layer ( Asynch ASPSOAP )
  my $asynchChannel = $this->outboundASPSOAPCommandAsynchChannel();

  $this->log("getNextOutboundASPSOAPAsynchMessage asynchChannel = $asynchChannel");

  return $this->getNextOutboundASPSOAPMessage( $asynchChannel );
}


=head4

  Returns the next available message in the Synch ASP SOAP Channel

=cut
sub getNextOutboundASPSOAPSynchMessage
{
  my ($this) = @_;

  # outer layer ( Synch ASPSOAP )
  my $synchChannel = $this->outboundASPSOAPCommandSynchChannel();

  return $this->getNextOutboundASPSOAPMessage( $synchChannel );
}


=head4

  Pops a message from the $channelName Command channel in the $messageQueue message queue

=cut
sub popCommandChannel
{
  my ($this,$channelName,$messageQueue) = @_;

  return $this->popEndPointChannel( $channelName , $messageQueue , $this->commandChannelPrefix() );
}


=head4

  Verifies if $commandChannel is 'filled'

=cut
sub isCommandChannelFilled
{
  my ($this,$commandChannel) = @_;

  #$this->log("isCommandChannelFilled commandChannel is ".$this->peekCommandChannelStatus( $commandChannel ));

  return ( $this->peekCommandChannelStatus( $commandChannel ) eq 'filled' );
}


=head4

  Read the $commandChannel status

=cut
sub peekCommandChannelStatus
{
  my ($this,$commandChannel) = @_;

  return $this->peekChannelStatus( $commandChannel , $this->commandChannelPrefix() );
}


=head4

  Verifies if $commandChannel exists

=cut
sub existsCommandChannel
{
  my ($this,$channelName) = @_;

  return $this->existsEndPointChannel( $channelName , $this->commandChannelPrefix() );
}


=head4

  Creates a Command channel named $channelName in $messageQueue

=cut
sub createCommandChannel
{
  my ($this,$channelName,$messageQueue) = @_;

  $this->log("$channelName , $messageQueue");

  return $this->createEndPointChannel( $channelName , $messageQueue , $this->commandChannelPrefix() );
}


=head4

  Sends $message to the Command channel named $channelName

=cut
sub sendToCommandChannel
{
  my ($this,$channelName,$message) = @_;

  return $this->sendToEndPointChannel( $channelName , $message , $this->commandChannelPrefix() );
}


=head4



=cut
sub commandChannelPrefix
{
  return 'CC';
}


1;


__END__



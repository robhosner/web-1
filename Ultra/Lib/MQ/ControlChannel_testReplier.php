<?php

require_once("db.php");
require_once("Ultra/Lib/MQ/ControlChannel.php");

$controlChannel = new \Ultra\Lib\MQ\ControlChannel;

# select an outbound request channel + message
$getNextOutboundMessageResult = $controlChannel->getNextOutboundMessage();

if ( ! count($getNextOutboundMessageResult) ) { die("No outbound request channel found.\n"); }

list( $nextOutboundControlChannel , $message ) = $getNextOutboundMessageResult;

print_r( array( $nextOutboundControlChannel , $message ) );

dlog('',"outbound channel = $nextOutboundControlChannel");
dlog('',"outbound message = $message");

$anInboundRequestMessage = $controlChannel->buildMessage( array( 'header' => 'H' , 'body' => array('x'=>'y') , 'uuid' => 'test' , 'actionUUID' => 'test' ) );
$nextInboundControlChannel = $controlChannel->toInbound( $nextOutboundControlChannel );

dlog('',"inbound channel = $nextInboundControlChannel");
dlog('',"inbound message = $anInboundRequestMessage");

$controlUUID = $controlChannel->sendToControlChannel( $nextInboundControlChannel , $anInboundRequestMessage );

dlog('',"controlUUID = $controlUUID");

/*
my ( $nextOutboundControlChannel , $message ) = @$getNextOutboundMessageResult;

$controlChannel->log("outbound channel = $nextOutboundControlChannel");
$controlChannel->log("outbound message = $message");

my $anInboundRequestMessage = '[' . time . '] 456 def !!!';
my $nextInboundControlChannel = $controlChannel->toInbound( $nextOutboundControlChannel );

$controlChannel->log("inbound channel = $nextInboundControlChannel");
$controlChannel->log("inbound message = $anInboundRequestMessage");

my $controlUUID = $controlChannel->sendToControlChannel( $nextInboundControlChannel , $anInboundRequestMessage );

$controlChannel->log("controlUUID = $controlUUID");
*/

?>

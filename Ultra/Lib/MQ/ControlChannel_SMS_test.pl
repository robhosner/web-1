use strict;
use warnings;


use Ultra::Lib::MQ::ControlChannel;


# instantiate Ultra::Lib::MQ::ControlChannel
my $mq_cc = Ultra::Lib::MQ::ControlChannel->new();

# create Request-Reply ACC MW Control Channels
my ($c_outbound,$c_inbound) = $mq_cc->createNewSMSMWControlChannels();

print "($c_outbound,$c_inbound)\n";

print $mq_cc->peekControlChannelStatus( $c_outbound )."\n"; # 'created'

my $anOutboundRequestMessage = 'hello from ControlChannel_SMS_test.pl';

# send outbound Request
my $controlUUID = $mq_cc->sendToSMSControlChannel( $c_outbound , $anOutboundRequestMessage );

print "$controlUUID\n";

my $peekControlChannelResult = $mq_cc->peekControlChannel( $c_outbound );

print "$peekControlChannelResult\n"; # $anOutboundRequestMessage

print $mq_cc->peekControlChannelStatus( $c_outbound )."\n"; # 'filled'

# extract SMS MW outbound message
my $popControlChannelResult = $mq_cc->popControlChannel( $c_outbound , $mq_cc->outboundControlSynchChannel( $mq_cc->controlChannelTypeSMSMW() ) );

print "$popControlChannelResult\n"; # $anOutboundRequestMessage

my $anInboundReplyMessage = 'how do you do ControlChannel_SMS_test.pl';

# send inbound Request
$controlUUID = $mq_cc->sendToSMSControlChannel( $c_inbound , $anInboundReplyMessage );

print "$controlUUID\n";

$peekControlChannelResult = $mq_cc->peekControlChannel( $c_inbound );

print "$peekControlChannelResult\n"; # $anInboundReplyMessage

print $mq_cc->peekControlChannelStatus( $c_inbound )."\n"; # 'filled'

$popControlChannelResult = $mq_cc->popControlChannel( $c_inbound , $mq_cc->inboundControlSynchChannel( $mq_cc->controlChannelTypeSMSMW() ) );

print "$popControlChannelResult\n"; # $anInboundReplyMessage

__END__

Test script for Ultra::Lib::MQ::ControlChannel


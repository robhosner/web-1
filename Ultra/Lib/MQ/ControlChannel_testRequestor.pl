use strict;
use warnings;


use Ultra::Lib::MQ::ControlChannel;


# set up our logs
use Log::Report mode => 'VERBOSE';
dispatcher 'FILE', 'log', mode => 'DEBUG', to => '/tmp/ControlChannel_testRequestor.log';


# instantiate Ultra::Lib::MQ::ControlChannel
my $mq_cc = Ultra::Lib::MQ::ControlChannel->new();

# create Request-Reply ACC MW Control Channels
my ($c_outbound,$c_inbound) = $mq_cc->createNewACCMWControlChannels();


# I am a Requestor, I want to send an *outbound* message through $c_outbound,
# then I will synchronously wait for a reply the Replier will send through $c_inbound

$mq_cc->log("c_outbound = $c_outbound");
$mq_cc->log("c_inbound  = $c_inbound");

my $anOutboundRequestMessage = '[' . time . '] 123 abc |||';

my $controlUUID = $mq_cc->sendToControlChannel( $c_outbound , $anOutboundRequestMessage );

$mq_cc->log("controlUUID = $controlUUID");
$mq_cc->log("message     = $anOutboundRequestMessage");

my $anInboundReplyMessage = $mq_cc->waitForControlMessage( $c_inbound , $mq_cc->inboundACCMWControlChannel() );

if ( $anInboundReplyMessage )
{
  $mq_cc->log("Success: message = $anInboundReplyMessage");
}
else
{
  $mq_cc->log("Failure");
}


__END__



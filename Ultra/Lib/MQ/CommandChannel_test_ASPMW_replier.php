<?php

include_once 'Ultra/Lib/MQ/CommandChannel.php';

$commandChannel = new \Ultra\Lib\MQ\CommandChannel;

# select an outbound request Synch channel + message
$getNextOutboundMessageResult = $commandChannel->getNextOutboundASPMWSynchMessage();

if ( count($getNextOutboundMessageResult) )
{
  list( $nextOutboundCommandChannel , $message ) = $getNextOutboundMessageResult;

  dlog('',"nextOutboundCommandChannel = $nextOutboundCommandChannel");
  dlog('',"message = $message");

  $nextInboundCommandChannel = $commandChannel->toInbound( $nextOutboundCommandChannel );

  $inboundReply = $commandChannel->buildMessage(
    array( 'actionUUID' => 'CommandChannel_test_ASPMW_replier' , 'uuid' => 'CommandChannel_test_ASPMW_replier' , 'header' => 'H' , 'body' => array('x'=>'y') )
  );

  dlog('',"inbound channel = $nextInboundCommandChannel");
  dlog('',"inbound message = %s",$inboundReply);

  $controlUUID = $commandChannel->sendToCommandChannel( $nextInboundCommandChannel , $inboundReply );

  dlog('',"controlUUID = $controlUUID");
}
else
{
  dlog('',"No outbound request channel found.");
}


# select an outbound request channel + message
#$getNextOutboundMessageResult = $commandChannel->getNextOutboundASPMWAsynchMessage();

?>

<?php

namespace Ultra\Lib\MQ;

if (!getenv("UNIT_TESTING"))
{
  include_once('lib/util-common.php');
  include_once('Ultra/Lib/Util/Redis.php');
  include_once('Ultra/UltraConfig.php');
}

/**
 * Main Class which implements Endpoint functionalities.
 * Bridge code which enables applications to send/receive messages, and to create different types of channels.
 * 
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Ultra Middleware
 */
class EndPoint
{
  # Redis connection
  protected $redis;

  # Maximum allowed duration of a Request-Reply exchange.
  # Set as the environment default when the class is instantiated,
  # it can be overridden by setRequestReplyTimeoutSeconds
  protected $requestReplyTimeoutSeconds;

  /**
   * The EndPoint constructor instantiates a Redis object and connects to Redis
   */
  public function __construct()
  {
    try
    {
      $this->redis = new \Ultra\Lib\Util\Redis( FALSE );

/*
      $this->redis = new \Redis();

      if ( ! $this->redis )
        throw new \Exception("Cannot instantiate object of class Redis");

      list($host,$port) = explode(':',\Ultra\UltraConfig\redisHost());

      # Redis connection
      if ( ! $this->redis->connect( $host , $port ) )
        throw new \Exception("redis connection attempt failed ( host = $host , port = $port )");

      if ( ! $this->redis )
        throw new \Exception("Connection with Redis lost after 'connect'");

      # Redis authentication
      if ( ! $this->redis->auth( \Ultra\UltraConfig\redisPassword() ) )
        throw new \Exception("redis authentication attempt failed");
*/

      if ( ! $this->redis )
        throw new \Exception("Connection with Redis lost after 'auth'");

      $this->setRequestReplyTimeoutSeconds( \Ultra\UltraConfig\channelsDefaultReplyTimeoutSeconds() );
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());
      $this->redis = NULL;
    }
  }

  function __destruct()
  {
  }

  /**
   * toInbound
   *
   * Converts an outbound channel name to an inbound channel name
   *
   * @return string
   */
  public function toInbound($requestChannelName)
  {
    return preg_replace('/^out_/', 'in_', $requestChannelName);
  }

  /**
   * toOutbound
   *
   * Converts an inbound channel name to an outbound channel name
   *
   * @return string
   */
  public function toOutbound($replyChannelName)
  {
    return preg_replace('/^in_/', 'out_', $replyChannelName);
  }

  /**
   * notificationChannel
   *
   * Name of a $mq notification channel
   *
   * @return string
   */
  private function notificationChannel($mq)
  {
    return $mq.'/INBOUND/NOTIFICATION';
  }

  /**
   * notificationChannelACCMW
   *
   * ACC MW notification channel name
   *
   * @return string
   */
  public function notificationChannelACCMW()
  {
    return $this->notificationChannel( 'ACCMW' );
  }

  /**
   * notificationChannelUltraMW
   *
   * ULTRA MW notification channel name
   *
   * @return string
   */
  public function notificationChannelUltraMW()
  {
    return $this->notificationChannel( 'ULTRAMW' );
  }

  /**
   * enqueueNotificationChannel
   *
   * Add message to the ULTRA MW notification channel
   *
   * @return string - notificationUUID: the Redis key which points to $message
   */
  public function enqueueNotificationChannel($message)
  {
    $notificationChannel = $this->notificationChannelUltraMW();

    dlog('',"notificationChannel = $notificationChannel");

    # create a unique identifier for the notification message
    $notificationUUID = $this->getNewNotificationUUID();

    # associate the NOTIFICATION_UUID with $message
    $this->redis->set($notificationUUID,$message);
    $this->redis->expire($notificationUUID,$this->getMessageTTLSeconds());

    # enqueue the NOTIFICATION_UUID in the Notification Channel
    $this->redis->rpush($notificationChannel,$notificationUUID);

    dlog('',"returning $notificationUUID");

    return $notificationUUID;
  }

  /**
   * dequeueNotificationChannel
   *
   * Extract a message from a notification channel
   *
   * @return string - message or empty string in case of trouble
   */
  public function dequeueNotificationChannel($notificationChannel)
  {
    $return = '';

    dlog('',"dequeueNotificationChannel is about to extract a notificationUUID from channel $notificationChannel");

    # FIFO operation: we pop from left
    $notificationUUID = $this->redis->lpop( $notificationChannel );

    if ( $notificationUUID )
    {
       dlog('',"dequeueNotificationChannel is about to get the message associated with the notificationUUID $notificationUUID");

       # finally we get the message
       $return = $this->redis->get( $notificationUUID );
    }

    dlog('',"dequeueNotificationChannel returns $return");

    return $return;
  }

  /**
   * dequeueNotificationChannelACCMW
   *
   * Extract a message from the ACC MW notification channel
   *
   * U <- ACCMW <- SOAP
   *            ^^
   */
  public function dequeueNotificationChannelACCMW()
  {
    $notificationChannel = $this->notificationChannelACCMW();

    return $this->dequeueNotificationChannel($notificationChannel);
  }

  /**
   * dequeueNotificationChannelUltraMW
   *
   * Extract a message from the ULTRA MW notification channel
   *
   * U <- ACCMW <- SOAP
   *   ^^
   */
  public function dequeueNotificationChannelUltraMW()
  {
    $notificationChannel = $this->notificationChannelUltraMW();

    return $this->dequeueNotificationChannel($notificationChannel);
  }

  /**
   * peekNotificationChannelACCMW
   *
   * Check the first element in the ACC MW notification channel
   *
   * U <- ACCMW <- SOAP
   *            ^^
   */
  public function peekNotificationChannelACCMW()
  {
    $notificationChannel = $this->notificationChannelACCMW();

    return $this->peekNotificationChannel($notificationChannel);
  }

  /**
   * peekNotificationChannelUltraMW
   *
   * Check the first element in the ULTRA MW notification channel
   *
   * U <- ACCMW <- SOAP
   *   ^^
   */
  public function peekNotificationChannelUltraMW()
  {
    $notificationChannel = $this->notificationChannelUltraMW();

    return $this->peekNotificationChannel($notificationChannel);
  }

  /**
   * peekNotificationChannel
   *
   * Check the first element in $notificationChannel
   *
   * @return string - empty in case of issues
   */
  private function peekNotificationChannel($notificationChannel)
  {
    $return = '';

    #dlog('',"peekNotificationChannel is about to check a notificationUUID from channel $notificationChannel");

    # check the top (left) of the queue
    $notificationUUID = $this->redis->lrange( $notificationChannel , 0 , 0 );

    if ( $notificationUUID && is_array($notificationUUID) && count($notificationUUID) )
    {
      $return = $this->redis->get( $notificationUUID[0] );

      dlog('',"peekNotificationChannel notificationUUID value = %s",$return);

      if ( ! $return )
      {
        // we must remove $notificationUUID[0] from $notificationChannel

        dlog('',"abandoning notificationUUID ",$notificationUUID[0]);

        $return = $this->redis->lrem( $notificationChannel , $notificationUUID[0] );
      }
    }

    #dlog('',"peekNotificationChannel returns %s",$return);

    return $return;
  }

  /**
   * getNewChannelName
   *
   * generates a unique string (a UUID without {}) which will be part of a new channel name
   *
   * @return string
   */
  public function getNewChannelName()
  {
    return getNewUUID('CH','EndPoint',FALSE);
  }

  /**
   * getNewNotificationUUID
   *
   * generates a New Notification UUID
   *
   * @return string
   */
  public function getNewNotificationUUID()
  {
    return getNewUUID('NX','',TRUE);
  }

  /**
   * getNewControlUUID
   *
   * generates a New Control UUID
   *
   * @return string
   */
  public function getNewControlUUID()
  {
    return getNewUUID('CX','',TRUE);
  }

  /**
   * getNewSMSOutboundMessageKey
   *
   * generates a New SMS Outbound Message Key
   *
   * @return string
   */
  public function getNewOutboundMessageKey( $prefix='MK' )
  {
    return 'out_'.$this->_getMessageUUID( $prefix );
  }

  /**
   * _getMessageUUID
   *
   * generates a New Message UUID
   *
   * @return string
   */
  public function _getMessageUUID( $prefix='MM' )
  {
    return getNewUUID($prefix,'',TRUE);
  }

  /**
   * getRequestReplyTimeoutSeconds
   *
   * Returns timeout in seconds for synchronous waits
   *
   * @return integer
   */
  public function getRequestReplyTimeoutSeconds()
  {
    return $this->requestReplyTimeoutSeconds;
  }

  /**
   * setRequestReplyTimeoutSeconds
   *
   * Set timeout in seconds for synchronous waits
   */
  public function setRequestReplyTimeoutSeconds($seconds)
  {
    $this->requestReplyTimeoutSeconds = $seconds;
  }

  /**
   * getCreatedDateTime
   *
   * Message timestamp
   *
   * @return string - a formatted date
   */
  public function getCreatedDateTime()
  {
    return date("Y-m-d H:i:s");
  }

  /**
   * buildMessage
   *
   * Builds and returns a MW message
   *
   * @return string - json-encoded message (an associative array)
   */
  public function buildMessage($params)
  {
    if ( empty($params['paidEvent']) )
      $params['paidEvent'] = 0;

    $message = [
      'header'      => $params['header'],
      'body'        => $params['body'],
      'paidEvent'   => $params['paidEvent'],
      '_actionUUID' => $params['actionUUID'],
      '_uuid'       => $this->_getMessageUUID(),
      '_timestamp'  => time()
    ];

    // append makeitso queue id, if passed
    if ( ! empty($params['makeitsoQueueId']) )
      $message['makeitsoQueueId'] = $params['makeitsoQueueId'];

    // append soc name for Paid Events, if passed
    if ( ! empty($params['socName']) )
      $message['socName'] = $params['socName'];

    return json_encode($message);
  }

  /**
   * extractFromMessage
   *
   * Extract data from a message
   *
   * @return array
   */
  public function extractFromMessage($message)
  {
    return json_decode($message);
  }

  /**
   * getMessageTTLSeconds
   *
   * Redis messages TTL in seconds
   *
   * @return integer
   */
  public function getMessageTTLSeconds()
  {
    $default = 24 * 60 * 60 ;

    $messageTTLSeconds = \Ultra\UltraConfig\find_config('redis/messages/ttl_hours') * 60 * 60 ;

    return ( $messageTTLSeconds ) ? $messageTTLSeconds : $default ;
  }

  /**
   * getChannelPingFrequencySeconds
   *
   * How often we sould check for messages in a channel
   *
   * @return integer
   */
  public function getChannelPingFrequencySeconds()
  {
    $default = 5 ;

    $channelPingFrequencySeconds = \Ultra\UltraConfig\find_config('redis/channels/ping_frequency_seconds');

    return ( $channelPingFrequencySeconds ) ? $channelPingFrequencySeconds : $default ;
  }

  /**
   * createEndPointChannel
   *
   * Generic method to create channels
   * $prefix . '_' . $channelName is the channel name
   * The channel will be associated to $messageQueue
   *
   * Returns FALSE if the channel already exists or it cannot be created
   */
  public function createEndPointChannel($channelName,$messageQueue,$prefix)
  {
    $return = FALSE;

    #dlog('',"$channelName , $messageQueue , $prefix");

    if ( ! $this->existsEndPointChannel($channelName,$prefix) )
    {
      # allocates channel name
      $this->redis->set(    $prefix . '_' . $channelName , 'channel' );
      $this->redis->expire( $prefix . '_' . $channelName , $this->getMessageTTLSeconds() );

      if ( $this->reserveEndPointChannel( $channelName , $prefix ) )
      {
        #dlog('',"Process " . getmypid() . " reserved channel $channelName");

        # add this channel to the $messageQueue set (layer)
        $this->redis->sadd( $messageQueue , $channelName );
        #dlog('',"Adding $channelName to set $messageQueue");

        # release channel
        $this->releaseEndPointChannel( $channelName , 'created' );

        $return = $channelName;
      }
      else
      {
        dlog('',"Process " . getmypid() . " could not reserve channel $channelName");
      }
    }
    else
    {
      dlog('',"channel $channelName already exists");
    }

    return $return;
  }

  /**
   * releaseEndPointChannel
   *
   * Release a Channel we previously reserved using getmypid().
   */
  protected function releaseEndPointChannel( $channelName , $status )
  {
    $success = ! ! $this->assignChannelStatus( $channelName , $status );
  }

  /**
   * existsEndPointChannel
   *
   * Checks if a Channel exists
   *
   * @return boolean
   */
  public function existsEndPointChannel($channelName,$prefix)
  {
    return $this->redis->exists( $prefix . '_' . $channelName );
  }

  /**
   * reserveEndPointChannel
   *
   * Reserve Channel using getmypid().
   *
   * @return boolean - FALSE in case of issues
   */
  protected function reserveEndPointChannel( $channelName , $prefix )
  {
    $success = FALSE;

    $currentStatus = $this->peekChannelStatus( $channelName , $prefix );

    if ( !preg_match("/^reserved /", $currentStatus) && !preg_match("/^used/", $currentStatus) )
    {
      $success = ! ! $this->assignChannelStatus( $channelName , "reserved " . getmypid() );

      if ( $success )
      {
        $currentStatus = $this->peekChannelStatus( $channelName , $prefix );

        $success = ( $currentStatus == "reserved " . getmypid() );
      }
    }

    return $success;
  }

  /**
   * assignChannelStatus
   *
   * Set Channel Status
   *
   * @return boolean
   */
  protected function assignChannelStatus($channelName,$status)
  {
    if ( ! $this->redis->set( 'CS_' . $channelName , $status ) )
      return FALSE;

    $this->redis->expire( 'CS_' . $channelName , $this->getMessageTTLSeconds() );

    return TRUE;
  }

  /**
   * peekChannelStatus
   *
   * Check Channel Status
   *
   * @return string - empty in case Channel does not exist
   */
  public function peekChannelStatus( $channelName , $prefix )
  {
    $return = '';

    if ( $this->redis->exists( $prefix . '_' . $channelName )
      && $this->redis->exists( 'CS_' . $channelName )
    )
    {
      $return = $this->redis->get( 'CS_' . $channelName );
    }

    return $return;
  }

  /**
   * popEndPointChannel
   *
   * Generic method to pop a message from a channel
   *
   * @return string - a message (or an empty string if failure)
   */
  public function popEndPointChannel($channelName,$messageQueue,$prefix)
  {
    #dlog('',"$channelName,$messageQueue,$prefix");

    $return = '';

    if ( $this->redis->exists( $prefix . '_' . $channelName ) # channel name
      && $this->redis->exists( 'CS_' . $channelName ) # channel status
      && $this->redis->exists( 'CM_' . $channelName ) # channel message
      && ( $this->redis->get( 'CS_' . $channelName ) == 'filled' )
    )
    {
      # read the CONTROL_UUID contained in the channel
      $controlUUID = $this->redis->get( 'CM_' . $channelName );

      if ( $controlUUID )
      {
        if ( $this->reserveEndPointChannel( $channelName , $prefix ) )
        {
          #dlog('',"Process " . getmypid() . " reserved channel $channelName");

          # get the message pointed by the CONTROL_UUID
          # we assume that every message is a ``true value``
          $return = $this->redis->get( $controlUUID );

          # remove the message from the channel
          $this->redis->del( 'CM_' . $channelName );

          # remove channel from channel set
          $this->redis->srem( $messageQueue , $channelName );
          #dlog('',"Removing $channelName from $messageQueue");

          # release channel
          $this->releaseEndPointChannel( $channelName , 'used' );
        }
        else
        {
          dlog('',"Process " . getmypid() . " could not reserve channel $channelName");
        }
      }
    }

    if ( ! $return ) { dlog('',"failure"); }

    return $return;
  }

  /**
   * sendToEndPointChannel
   *
   * Generic method to send a message to a channel
   *
   * Returns FALSE if the message cannot be sent
   */
  protected function sendToEndPointChannel($channelName,$message,$prefix)
  {
    # we do not accept empty messages
    if ( is_null($message) || ( $message == '' ) )
    {
      dlog('',"sendToEndPointChannel : message cannot be empty");

      return FALSE;
    }

    $return = FALSE;

    // verify that the channel exists
    if ( $this->redis->exists( $prefix .  '_' . $channelName ) )
    {
      if ( $this->reserveEndPointChannel( $channelName , $prefix ) )
      {
        # create a unique identifier for the control message
        $controlUUID = $this->getNewControlUUID(); #TODO: EndPointUUID

        # associate the CONTROL_UUID with $message
        $this->redis->set($controlUUID,$message);
        $this->redis->expire($controlUUID,$this->getMessageTTLSeconds());

        # place the CONTROL_UUID in the appropriate container
        $this->redis->set( 'CM_' . $channelName , $controlUUID );
        $this->redis->expire( 'CM_' . $channelName , $this->getMessageTTLSeconds() );

        # release channel
        $this->releaseEndPointChannel( $channelName , 'filled' );

        $return = $controlUUID;
      }
      else
        dlog('',"Process " . getmypid() . " could not reserve channel $channelName");
    }
    else
      dlog('',"Channel $channelName does not exist");

    return $return;
  }

  /**
   * storeAsynchCallByAction
   *
   * Stores initial asynch call in Redis to be retrieved by the callback
   * Every action can trigger only a command of each type
   */
  public function storeAsynchCallByAction( $actionUUID , $command , $message )
  {
    dlog('',"$actionUUID , $command => $message");

    $this->redis->set( 'ASYNCH/ACTION/' . $actionUUID . '/' . $command , $message );
    $this->redis->expire( 'ASYNCH/ACTION/' . $actionUUID . '/' . $command , $this->getMessageTTLSeconds() );
  }

  /**
   * retrieveAsynchCallByAction
   *
   * Retrieve initial asynch call from Redis
   */
  public function retrieveAsynchCallByAction( $actionUUID , $command )
  {
    return $this->redis->get( 'ASYNCH/ACTION/' . $actionUUID . '/' . $command );
  }

}


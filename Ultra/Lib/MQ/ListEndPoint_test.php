<?php

require_once("db.php");
require_once("Ultra/Lib/MQ/ListEndPoint.php");

echo Ultra\Lib\MQ\ListEndPoint::SMS_OUTBOUND_SYNCH_LIST . PHP_EOL;
echo Ultra\Lib\MQ\ListEndPoint::SMS_OUTBOUND_ASYNCH_LIST . PHP_EOL;

exit;

$listEndPoint = new \Ultra\Lib\MQ\ListEndPoint('test');

$message = 'm'.time();

echo "] message = $message\n";

$messageKey = $listEndPoint->pushMessage( $message );

echo "] messageKey = $messageKey\n";

$message = $listEndPoint->popMessage();

echo "] message = $message\n";

$message = $listEndPoint->popMessage();

echo "] message = $message\n";

exit;

$message = 'm'.time();

echo "] message = $message\n";

$listEndPoint->pushMessage( $message );

$message = $listEndPoint->popMessage();

echo "] message = $message\n";

$message = $listEndPoint->popMessage();

echo "] message = $message\n";

exit;

$messageId = 'M'.time();

echo "] messageId = $messageId\n";

$listEndPoint->pushMessageId( $messageId );

$messageId = $listEndPoint->popMessageId();

echo "] messageId = $messageId\n";

$messageId = $listEndPoint->popMessageId();

echo "] messageId = $messageId\n";


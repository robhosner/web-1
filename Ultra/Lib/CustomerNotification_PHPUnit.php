<?php

require_once 'classes/PHPUnitBase.php';
require_once 'Ultra/Lib/CustomerNotification.php';

class CustomerNotificationTest extends PHPUnitBase
{
  public function setUp()
  {
    teldata_change_db();
  }


  public function test__CustomerNotification()
  {
/*
    // invalid MSISDN
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array("MSISDN" => "12345");
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, 'MP0003');
    unset($cn);

    // invalid message (foreign language)
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "4246462018",
      "smsText" => 'Жители запада-Крыма, снова остались без света?');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // invalid message (non-ASCII characters)
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "3476455407",
      "smsText" => '¿¡á,é-í+ó+ú_ü~ñ¿¡');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // blank message
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => '3476455407', #"4246462018",
      "smsText" => '');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test BALANCE
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "3476455407",
      "smsText" => 'Bal.');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test TMO->ULTRA command mapping
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "3476455407",
      "smsText" => '225'); // aka BALANCE
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test DATA
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "3476455407",
      "smsText" => 'data');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test STORE and DEALER without zipcode
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "3476455407",
      "smsText" => 'dealer');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test STORE and DEALER with zipcode
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "3476455407",
      "smsText" => 'store 11211');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test STORE and DEALER with invalid zipcode
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "3476455407",
      "smsText" => 'store 99999');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test language jumps: SPANISH
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "3476455407",
      "smsText" => 'eng');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test MENU
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN" => "3476455407",
      "smsText" => 'menu');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test GOGO with existing active MSISDN
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => '3476455407',
      "smsText" => 'GOGO 11211');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test GOGO with invalid fake MSISDN
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "12345678901234",
      "smsText" => 'GOGO 11211');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test GOGO with neutral sub
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "4246462308",
      "smsText" => 'GOGO 11211');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test RECHAREGE without PIN
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "3476455407",
      "smsText" => 'recharge');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test RECHAREGE with invalid PIN format
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "3476455407",
      "smsText" => 'recharge 12345');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // successfull PIN recharge
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "3476455407",
      "smsText" => 'recargar 9999889999');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // campaign test
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "3476455407",
      "smsText" => 'SATURN');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

    // test unknown command
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "3476455407",
      "smsText" => 'strawberries and creme');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);

*/
    // test too many workflow falures (invalid input)
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "3476455407",
      "smsText" => 'up something');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);
  }


  private function checkOutcomeByCode($outcome, $code)
  {
    $this->assertTrue(is_object($outcome));

    if ($code)
    {
      $this->assertTrue($outcome->is_failure());
      $this->assertNotEmpty($outcome->get_error_codes());
      $this->assertNotEmpty($outcome->get_errors());
      $this->assertContains($code, $outcome->get_error_codes());
    }
    else
      $this->assertTrue($outcome->is_success());
  }


  public function test__BoltOns()
  {
    // test misspelled bolton keyword
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "3476455407",
      "smsText" => 'buy');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);
  }


  public function test__terminate()
  {
    // test misspelled bolton keyword
    $cn = new \Ultra\Lib\CustomerNotification();
    $params = array(
      "MSISDN"  => "3476455407",
      "smsText" => 'cancelar');
    $outcome = $cn->execute($params);
    # print_r($outcome);
    $this->checkOutcomeByCode($outcome, NULL);
    unset($cn);
  }

  public function test__isWorkflowRestricted()
  {
    $cn = new \Ultra\Lib\CustomerNotification();

    $check = $cn->isWorkflowRestricted('LANGUAGE', 1);
    $this->assertFalse($check);

    $check = $cn->isWorkflowRestricted('RECHARGE', 1);
    $this->assertFalse($check);

    $check = $cn->isWorkflowRestricted('STORE', 1);
    $this->assertFalse($check);

    $check = $cn->isWorkflowRestricted('UPINTL', 1);
    $this->assertFalse($check);

    $check = $cn->isWorkflowRestricted('LANGUAGE', 3);
    $this->assertTrue($check);

    $check = $cn->isWorkflowRestricted('RECHARGE', 3);
    $this->assertTrue($check);

    $check = $cn->isWorkflowRestricted('STORE', 3);
    $this->assertTrue($check);

    $check = $cn->isWorkflowRestricted('UPINTL', 3);
    $this->assertTrue($check);

    unset($cn);
  }

  public function test__getInvalidCommandErrorCode()
  {
    $cn = new \Ultra\Lib\CustomerNotification();

    $errorCode = $cn->getInvalidCommandErrorCode(1);
    $this->assertEquals('IN0010', $errorCode);

    $errorCode = $cn->getInvalidCommandErrorCode(2);
    $this->assertEquals('IN0010', $errorCode);

    $errorCode = $cn->getInvalidCommandErrorCode(3);
    $this->assertEquals('IN0014', $errorCode);

    unset($cn);
  }
}

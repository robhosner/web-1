<?php

require_once 'db.php';
require_once 'Ultra/Lib/MeS.php';

abstract class AbstractTestStrategy
{
  abstract function test( $argv );
}

class Test_MeS_perform_call extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
      'profile_id'       => '94100011811900000074',
      'profile_key'      => 'blGMSGehNOZCHESLZCFtZKvqJPUzVhbE',
      'transaction_type' => 'T',
      'card_number'      => '4012301230123010',
      'card_exp_date'    => '1111'
    );

    $url = NULL;

    return \Ultra\Lib\MeS\perform_call($params,$url);
  }
}

class Test_MeS_store extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
#      'cc_number'    => '4012301230123010', # success
#      'cc_number'    => '1111222233334444', # failure
#      'expires_date' => '0116'
      'cc_number'    => $argv[2],
      'expires_date' => $argv[3]
    );

    return \Ultra\Lib\MeS\store($params);
  }
}

class Test_MeS_tokenize_and_sale extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
      'cc_number'    => $argv[2],
      'expires_date' => $argv[3]
    );

    $result = \Ultra\Lib\MeS\store($params);

    if ( $result->is_success() )
    {
      $params = array(
        'address'      => '123 Main',
        'zipcode'      => '55555',
        'amount'       => $argv[4],
        'token'        => $result->data_array['token']
      );

      return \Ultra\Lib\MeS\sale($params);
    }
    else
      echo "failure :(\n";
  }
}

class Test_MeS_tokenize_and_sale_recurring extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
      'cc_number'    => $argv[2],
      'expires_date' => $argv[3]
    );

    $result = \Ultra\Lib\MeS\store($params);

    if ( $result->is_success() )
    {
      $params = array(
        'address'      => '123 Main',
        'zipcode'      => '55555',
        'amount'       => $argv[4],
        'token'        => $result->data_array['token']
      );

      return \Ultra\Lib\MeS\sale_recurring($params);
    }
    else
      echo "failure :(\n";
  }
}

class Test_MeS_refund extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
      'transaction_id' => $argv[2]
    );

    return \Ultra\Lib\MeS\refund($params);
  }
}

class Test_MeS_verify extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
      'address'      => '123 Main',
      'zipcode'      => '55555',
      'amount'       => '0.00',
      'cc_number'    => $argv[2],
      'expires_date' => $argv[3],
      'cvv'          => $argv[4]
    );

    return \Ultra\Lib\MeS\verify($params);
  }
}

class Test_MeS_sale extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
      'address'      => '123 Main',
      'zipcode'      => '55555',
      'amount'       => $argv[2],
      'token'        => $argv[3]
    );

    return \Ultra\Lib\MeS\sale($params);
  }
}

class Test_MeS_sale_recurring extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
      'address'      => '123 Main',
      'zipcode'      => '55555',
      'amount'       => $argv[2],
      'token'        => $argv[3]
    );

    return \Ultra\Lib\MeS\sale_recurring($params);
  }
}

class Test_MeS_void extends AbstractTestStrategy
{
  function test( $argv )
  {
    $params = array(
      'transaction_id' => $argv[2]
    );

    return \Ultra\Lib\MeS\void($params);
  }
}


# perform test #


$testClass = 'Test_MeS_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$result = $testObject->test( $argv );

print_r( $result->get_errors() );
print_r( $result->to_string() );

echo "\n";

/*
php Ultra/Lib/MeS_test.php perform_call
php Ultra/Lib/MeS_test.php sale $AMOUNT $TOKEN
php Ultra/Lib/MeS_test.php sale_recurring $AMOUNT $TOKEN
php Ultra/Lib/MeS_test.php store  $CC_NUMBER $EXPIRES_DATE
php Ultra/Lib/MeS_test.php refund $TRANSACTION_ID
php Ultra/Lib/MeS_test.php tokenize_and_sale $CC_NUMBER $EXPIRES_DATE $AMOUNT
php Ultra/Lib/MeS_test.php tokenize_and_sale_recurring $CC_NUMBER $EXPIRES_DATE $AMOUNT
php Ultra/Lib/MeS_test.php verify $CC_NUMBER $EXPIRES_DATE $CVV
php Ultra/Lib/MeS_test.php void $TRANSACTION_ID
*/

?>

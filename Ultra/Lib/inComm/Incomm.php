<?php

namespace Ultra\Lib\Incomm;


/*
PHP module for the interaction with Incomm XML-based APIs

See http://wiki.hometowntelecom.com:8090/display/SPEC/InComm

InComm issued PINs are 10 digits, all numeric.
*/


require_once('classes/Result.php');

// this curl error is not defined in our enviro
if (! defined('CURLE_OPERATION_TIMEDOUT'))
  define('CURLE_OPERATION_TIMEDOUT', 28);

// connection parameters to improve code robustness
define('INCOMM_CONNECT_TIMEOUT' , 15); // give up if cannot connect, seconds
define('INCOMM_RESPONSE_TIMEOUT' , 15); // give up if cannot receive response, seconds

// inComm error codes
define('INCOMM_SUCCESS',        0);     // successfull transaction
define('INCOMM_CARD_ACTIVATED', 53);    // already reversed and can be used
define('INCOMM_CARD_ACTIVE',    4001);  // sold and can be used
define('INCOMM_CARD_DEACTIVE',  4002);  // has not been sold yet
define('INCOMM_CARD_REDEEMED',  4003);  // already redeemed

// special test PIN that succeeds on all operations
define('INCOMM_TEST_PIN', '9999889999');
define('INCOMM_PIN_LENGTH', 10);

/**
 * fabricateResult
 *
 * fabricate successfull inComm API response for a given request
 * used with a special testing PIN only
 *
 * @return object Result
 */
function fabricateResult($action)
{
  $result = new \Result();
  $result->succeed();
  $srcRefNum = generateSrcRefNum();

  // every action retuns different data set
  switch ($action)
  {
    case 'Redeem':
      $response = array(
        'RespRefNum'    => substr($srcRefNum, 0, 10),
        'SrcRefNum'     => $srcRefNum,
        'RespAction'    => $action,
        'RespCode'      => INCOMM_SUCCESS,
        'RespMsg'       => 'Success',
        'Date'          => date('Ymd'),
        'Time'          => date('His'),
        'FaceValue'     => '49.0',
        'Balance'       => '0.0',
        'UPC'           => '799999999999',
        'SerialNumber'  => '5555555555',
        'MerchantName'  => 'Business Development Testing',
        'StoreID'       => '99999999');
      break;

    case 'Reverse':
      $response = array(
        'RespRefNum'    => substr($srcRefNum, 0, 10),
        'SrcRefNum'     => $srcRefNum,
        'RespAction'    => $action,
        'RespCode'      => INCOMM_SUCCESS,
        'RespMsg'       => 'Success',
        'Date'          => date('Ymd'),
        'Time'          => date('His'),
        'FaceValue'     => '49.0',
        'Balance'       => '0.0',
        'UPC'           => '799999999999',
        'SerialNumber'  => '5555555555',
        'MerchantName'  => 'Business Development Testing',
        'StoreID'       => '99999999');
      break;

    case 'StatInq':
      $response = array(
        'RespRefNum'    => 0,
        'SrcRefNum'     => $srcRefNum,
        'RespAction'    => $action,
        'RespCode'      => INCOMM_CARD_ACTIVE,
        'RespMsg'       => 'Card is Active',
        'Date'          => date('Ymd'),
        'Time'          => date('His'),
        'FaceValue'     => '49.0',
        'Balance'       => '0.0',
        'UPC'           => '799999999999',
        'SerialNumber'  => '5555555555',
        'MerchantName'  => 'Business Development Testing',
        'StoreID'       => '99999999');
      break;

    default:
      $result->add_error("Unhandled action $action");
      $result->fail();
  }

  $response['fabricated'] = 'TRUE';
  $result->add_data_array($response);
  return $result;
}


/**
 * schemaDefinitionFile
 *
 * Incomm schema definition file
 *
 * @return string
 */
function schemaDefinitionFile()
{
  return 'Ultra/Lib/inComm/Request.xml';
}


/**
 * prepareRequestXML
 *
 * load empty XML schema from file, fill in given values and return it as a string
 *
 * @return string or NULL (on error)
 */
function prepareRequestXML($params)
{
  $schemaDefinitionFile = schemaDefinitionFile();

  // load schema definition
  $xml = new \DomDocument();
  if ( ! $xml->load( $schemaDefinitionFile ) )
  {
    dlog('','ERROR ESCALATION ALERT DAILY - Cannot load Incomm schema definition file '.$schemaDefinitionFile);
    return NULL;
  }

  // set values
  foreach ($params as $name => $value)
  {
    // locate the node with the given name
    $list = $xml->getElementsByTagName($name);
    if ($list->length)
      $list->item(0)->nodeValue = $value; // and set its value
  }

  // dlog('', 'request XML: %s', $xml->saveHTML());
  return $xml->saveHTML();
}


/**
 * singleValueNames
 *
 * return a static array of value names to be extracted from all requests
 * these values are part of inComm API response in <Name>Value</Name> format
 */
function singleValueNames()
{
  return array(
    'RespRefNum',    // transaction ID
    'SrcRefNum',     // reference ID to include in possible reversal request
    'RespAction',    // type of transaction
    'RespCode',      // error code
    'RespMsg',       // response message
    'Date',          // YYYYMMDD
    'Time',          // HHMMSS
    'FaceValue',     // PIN value (not transaction amount)
    'Balance',       // PIN remaining balance after transaction
    'UPC',           // PIN UPC
    'MerchantName',  // where PIN was sold
    'StoreID');      // merchant store ID
}


/**
 * pinRedemption
 *
 * redeemed given PIN
 *
 * @return Result object with success only if PIN has been redeemed
 */
function pinRedemption($pin, $customer)
{
  dlog('', "pin = $pin");

  if (! ctype_digit($pin) || strlen($pin) != INCOMM_PIN_LENGTH)
  {
    dlog('', "ERROR: invalid PIN $pin");
    return \make_error_result('Invalid PIN');
  }

  // catch testing PIN
  if ($pin == INCOMM_TEST_PIN && \Ultra\UltraConfig\isDevelopmentDB())
    return fabricateResult('Redeem');

  $values = array(
    'ReqCat'      => 'TransferredValue',
    'ReqAction'   => 'Redeem',
    'Date'        => date('Ymd'),
    'Time'        => date('His'),
    'PartnerName' => \Ultra\UltraConfig\incommPartnerName(),
    'AcctNum'     => encodeAccountNumber($customer->ACCOUNT, $customer->current_mobile_number),
    'PIN'         => $pin,
    'SrcRefNum'   => generateSrcRefNum()
  );
  $xml = prepareRequestXML($values);
  if (! $xml)
    return make_error_Result('Failed to initialize XML');

  // call API and check for errors
  $result = incommCall($xml);
  if ($result->is_success() && $result->get_data_key('RespCode') != INCOMM_SUCCESS)
  {
    $result->add_error('Failed to redeed PIN. ' . $result->get_data_key('RespMsg'));
    $result->fail();
  }

  return $result;
}


/**
 * pinReversal
 *
 * The reversal is not triggered by users, it's triggered by timeouts that occur so the pin isnt assumed used on incomm platform and never get redeemed on the ultra platfom
 *
 * @param array('pin', 'SrcRefNum')
 * @return Result object successfull only if PIN has been reversed
 */
function pinReversal( $params, $customer )
{
  dlog('', 'params = ' . json_encode($params));

  if (! is_numeric($params['pin']) || strlen($params['pin']) != INCOMM_PIN_LENGTH)
  {
    dlog('', "ERROR: invalid PIN {$params['pin']}");
    return \make_error_result('Invalid PIN');
  }

  if (empty($params['SrcRefNum']))
    return make_error_Result('Missing required SrcRefNum parameter');

  // catch testing PIN
  if ($params['pin'] == INCOMM_TEST_PIN && \Ultra\UltraConfig\isDevelopmentDB())
    return fabricateResult('Reverse');

  $values = array(
    'ReqCat'      => 'TransferredValue',
    'ReqAction'   => 'Reverse',
    'Date'        => date('Ymd'),
    'Time'        => date('His'),
    'PartnerName' => \Ultra\UltraConfig\incommPartnerName(),
    'AcctNum'     => encodeAccountNumber($customer->ACCOUNT, $customer->current_mobile_number),
    'PIN'         => $params['pin'],
    'SrcRefNum'   => $params['SrcRefNum']
  );
  $xml = prepareRequestXML($values);
  if (! $xml)
    return make_error_Result('Failed to initialize XML');

  // call API and check result
  $result = incommCall($xml);
  // INCOMM_CARD_ACTIVATED is returned if trying to reverse an already active card => not an error
  if ($result->is_success() && ! in_array($result->get_data_key('RespCode'), array(INCOMM_SUCCESS, INCOMM_CARD_ACTIVATED)))
  {
    $result->add_error('Failed to reverse PIN. ' . $result->get_data_key('RespMsg'));
    $result->fail();
  }
  
  return $result;
}


/**
 * statusInquiry
 *
 * Checks the status of a given PIN
 * 
 * @param PIN string or number
 * @return Result object, success only if PIN can be redeemed
 */
function statusInquiry($pin)
{
  dlog('', "pin = $pin");

  if (! is_numeric($pin) || strlen($pin) != INCOMM_PIN_LENGTH)
  {
    dlog('', "ERROR: invalid PIN $pin");
    return \make_error_result('Invalid PIN');
  }

  // catch testing PIN
  if ($pin == INCOMM_TEST_PIN && \Ultra\UltraConfig\isDevelopmentDB())
    return fabricateResult('StatInq');

  // prepare API parameters
  $values = array(
    'ReqCat'      => 'TransferredUse',
    'ReqAction'   => 'StatInq',
    'Date'        => date('Ymd'),
    'Time'        => date('His'),
    'PartnerName' => \Ultra\UltraConfig\incommPartnerName(),
    'PIN'         => $pin,
    'SrcRefNum'   => generateSrcRefNum()
  );
  $xml = prepareRequestXML($values);
  if (! $xml)
    return make_error_Result('Failed to initialize XML');

  // call API
  $result = incommCall($xml);

  // error handling
  if ($result->is_success() && $result->get_data_key('RespCode') != INCOMM_CARD_ACTIVE)
  {
    $result->add_error('PIN cannot be used. ' . $result->get_data_key('RespMsg'));
    $result->fail();
  }

  return $result;
}


/**
 * generateSrcRefNum
 *
 * Generates a *UNIQUE* number
 *
 * @return string
 */
function generateSrcRefNum()
{
  list($usec, $sec) = explode(" ", microtime());

  // 5 digits process ID
  $pid  = sprintf("%05d", getmypid());

  // 8 digits microseconds
  $usec = sprintf("%08d", ($usec*100000000));

  // 9 digits time (we remove the leading 1)
  $time = substr( time() , 1);

  return substr( $pid.$time.$usec , 0 , -4 );
}


/**
 * incommCall
 *
 * Perform a POST request to incomm URL
 *
 * @return Result object with data_array NAME => VALUE
 */
function incommCall($xml)
{
  $result = new \Result();
  $values = array();

  try
  {
    // prepare InComm URL
    $url = \Ultra\UltraConfig\incommURL();
    if (! $url)
      throw new \Exception('Invalid configuration URL');

    // init HTTP session
    $urlConn = curl_init($url);
    if (! $urlConn)
      throw new \Exception('Failed session initialization');

    // configure session options
    $options = array(
      CURLOPT_POST            => TRUE,
      CURLOPT_HTTPHEADER      => array('Content-type', 'application/x-www-form-urlencoded'),
      CURLOPT_POSTFIELDS      => $xml,
      CURLOPT_RETURNTRANSFER  => TRUE,
      CURLOPT_SSL_VERIFYPEER  => FALSE,
      CURLOPT_CONNECTTIMEOUT  => INCOMM_CONNECT_TIMEOUT,
      CURLOPT_TIMEOUT         => INCOMM_RESPONSE_TIMEOUT);
    if (! curl_setopt_array($urlConn, $options))
      throw new \Exception('Failed session configuration');

    // knock-knock!
    $response = curl_exec($urlConn);
    if (! $response)
      throw new \Exception('Failed inComm API call: ' . curl_error($urlConn), curl_errno($urlConn));

    // check API result
    $code = curl_getinfo($urlConn, CURLINFO_HTTP_CODE);
    if ($code != 200)
      throw new \Exception("Failed inComm API call with HTTP code $code");
    curl_close($urlConn);

    // parse response
    $parser = xml_parser_create();
    if (! xml_parse_into_struct($parser, $response, $vals, $index))
      throw new \Exception('Failed to parse inComm response');

    // free the parser
    xml_parser_free($parser);

    // get single value response data
    foreach(singleValueNames() as $name)
    {
      $upper = strtoupper($name);
      if (isset($vals[$index[$upper][0]]))
        $values[$name] = isset($vals[$index[$upper][0]]['value']) ? $vals[$index[$upper][0]]['value'] : NULL;
      else
      {
        $result->add_warning("Missing $name parameter");
        $values[$name] = NULL;
      }
    }

    // extract PIN SerialNumber which immediately follows the element named 'SerialNumber'
    foreach ($vals as $id => $element)
      if (! empty($element['value']) && $element['value'] == 'SerialNumber')
        $values['SerialNumber'] = empty($vals[$id + 1]['value']) ? NULL : $vals[$id + 1]['value'];

    // check that we received RespCode parameter (though we may not receive others in case of errors)
    if (isset($values['RespCode']))
      $result->succeed();
  }
  catch (\Exception $exc)
  {
    // handle transient network errors
    if (in_array($exc->getCode(), array(
      CURLE_COULDNT_RESOLVE_PROXY,
      CURLE_COULDNT_RESOLVE_HOST,
      CURLE_COULDNT_CONNECT,
      CURLE_OPERATION_TIMEDOUT)))
      $result->timeout();

    // log error message
    $error = $exc->getMessage();
    dlog('', "ERROR: $error");
    if ($response)
      dlog('', 'server response: ' . print_r($response, TRUE));
    $result->add_error($error);
  }

  // log transaction
  dlog('', 'result: ' . json_encode($values));

  $result->add_data_array($values);
  return $result;
}


/**
 * encodeAccountNumber
 * format a Ultra subscriber account into inComm AN20 (alphanumeric varchar 20)
 * @param String ACCOUNTS.ACCOUNT
 * @param String MSISDN
 * @return String formatted account number either 'A0000000000123456789' or plain MSISDN
 */
function encodeAccountNumber($account, $msisdn)
{
  $result = NULL;

  // RTRIN-35: enable or disable account handoff via cfengine flag
  if ( ! \Ultra\UltraConfig\isPaymentProviderHandoffEnabled('INCOMM'))
    return $result;

  if (empty($account) && empty($msisdn))
    dlog('', "ERROR: invalid parameters account: '$account', msisdn: '$msisdn'");

  // default format is 'A0000000000xxxxxxxxx'
  if (strlen($account) < 20 && ctype_digit($account))
    $result = sprintf('A%019d', $account);
  elseif ( ! empty($msisdn))
    $result = normalize_msisdn($msisdn, TRUE);
  else
    dlog('', "ERROR: cannot encode account number for account '$account' and MSISDN '$msisdn'");

  return $result;
}


/**
 * decodeAccountNumber
 * decode inComm account number (reverse of encodeAccountNumber) and retrieve subscriber record from DB
 * @param String account number
 * @return Object subscriber
 */
function decodeAccountNumber($account)
{
  $subscriber = NULL;

  if ($account[0] == 'A') // encoded ACCOUNTS.ACCOUNT
  {
    $sql = make_find_ultra_customers_from_details(array('account' => intval(substr($account, -19))));
    $result = mssql_fetch_all_objects(logged_mssql_query($sql));
    if (count($result) != 1)
      dlog('', 'ERROR: failed to get customer record');
    else
      $subscriber = $result[0];
  }
  elseif (ctype_digit($account) && in_array(strlen($account), array(10, 11))) // MSISDN
    $subscriber = get_customer_from_msisdn(normalize_msisdn($account, FALSE));
  else
    dlog('', "ERROR: invalid account '$account'");

  return $subscriber;
}

?>

<?php

/*
 * Dealer Demo Line functions used by demo_line.ph runner and dealerportal APIs
 * @see DEMO-1
 * @see http://wiki.hometowntelecom.com:8090/display/SPEC/Automation+of+Demo+Line+Program
 */

namespace Ultra\Lib\DemoLine;

// dealer types
define('DEMO_DEALER_EPP', 'EPP'); // EPP dealer codes start with string 'EPP'
define('DEMO_DEALER_1_1', '1:1'); // all other dealers are 1:1

// demo line status
define('DEMO_STATUS_ELIGIBLE',  'eligible');
define('DEMO_STATUS_ACTIVE',    'active');
define('DEMO_STATUS_INACTIVE',  'inactive');
define('DEMO_STATUS_WARNING',   'warning');
define('DEMO_STATUS_INVALID',   'invalid');
define('DEMO_STATUS_UNCLAIMED', 'unclaimed');

// SMS templates
define ('DEMO_SMS_WARNING', 'demo_warning');
define ('DEMO_SMS_REMINDER', 'demo_reminder');
define ('DEMO_SMS_CANCELLED', 'demo_cancelled');

// redis caching for ULTRA.DEMO_LINE data related to a single dealer
define('DEMO_LINE_CACHE_KEY', 'ultra/api/dealerportal/cache/demolines/');
define('DEMO_LINE_CACHE_TTL', 21600); //  12 hours since data is updated nightly

/**
 * createDemoLine
 * create new eligible demo line
 * @param Integer dealer ID
 * @param String dealer type
 * @param Integer number of required activations
 * @return Boolean TRUE on success of FALSE on failure
 */
function createDemoLine($dealer, $type, $activations)
{
  $result = FALSE;

  try
  {
    if (empty($dealer) || empty($type) || ! in_array($type, array(DEMO_DEALER_EPP, DEMO_DEALER_1_1)))
      throw new \Exception('invalid parameter: ' . json_encode(func_get_args()));

    flushRedisKeyByDealerId($dealer);

    // per Sean Enright: EPP dealer lines do not expire
    date_default_timezone_set('UTC');
    $expiration = ($type == DEMO_DEALER_EPP && $activations == 0) ? NULL : date('M 15 Y', strtotime('next month'));

    if ( ! $sql = \Ultra\Lib\DB\makeInsertQuery(
      'ULTRA.DEMO_LINE',
      array(
        'DEALER_ID'           => $dealer,
        'DEALER_TYPE'         => $type,
        'STATUS'              => DEMO_STATUS_ELIGIBLE,
        'ACTIVATIONS_NEEDED'  => $activations,
        'CLAIM_BY_DATE'       => $expiration)))
      throw new \Exception('failed to create SQL INSERT query');

    if ( ! $result = is_mssql_successful(logged_mssql_query($sql)))
      throw new \Exception("failed DB statement: $sql");
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}


/**
 * updateDemoLineStatus
 * update ULTRA.DEMO_LINE.STATUS
 * @param Integer ULTRA.DEMO_LINE.DEMO_LINE_ID
 * @param Array values to update
 * @param Integer dealer ID
 * @return Boolean TRUE on success or FALSE on failure
 */
function updateDemoLineStatus($line, $values, $dealer_id)
{
  $result = FALSE;

  try
  {
    if (empty($line) || empty($values) || empty($values['STATUS']) || empty($dealer_id))
      throw new \Exception('invalid parameter: ' . json_encode(func_get_args()));

    if ( ! in_array($values['STATUS'], array(DEMO_STATUS_ELIGIBLE, DEMO_STATUS_ACTIVE, DEMO_STATUS_WARNING, DEMO_STATUS_INVALID, DEMO_STATUS_UNCLAIMED)))
      throw new \Exception("invalid status: {$values['STATUS']}");

    flushRedisKeyByDealerId($dealer_id);

    if ( ! $sql = \Ultra\Lib\DB\makeUpdateQuery('ULTRA.DEMO_LINE', $values, array('DEMO_LINE_ID' => $line)))
      throw new \Exception('failed to create SQL INSERT query');

    if ( ! $result = is_mssql_successful(logged_mssql_query($sql)))
      throw new \Exception("failed DB statement: $sql");
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}


/**
 * sendDemoLineSms
 * send SMS to a demo line
 * @param Interger CUSTOMER_ID
 * @param String template
 * @param Integer activations
 * @return Boolean TRUE on success or FALSE on failure
 */
function sendDemoLineSms($line, $template)
{
  $result = FALSE;

  try
  {
    if (empty($line->CUSTOMER_ID))
      throw new \Exception("DEMO_LINE_ID {$line->DEMO_LINE_ID} is missing CUSTOMER_ID");

    // get MSISDN from CUSTOMER_ID
    if ( ! $msisdn = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $line->CUSTOMER_ID, 'CURRENT_MOBILE_NUMBER'))
      throw new \Exception("failed to get MSISDN for CUSTOMER_ID {$line->CUSTOMER_ID}");

    // check template
    if ( ! in_array($template, array(DEMO_SMS_WARNING, DEMO_SMS_REMINDER, DEMO_SMS_CANCELLED)))
      throw new \Exception("invalid SMS template $template");

    // send SMS
    $return = enqueue_daytime_sms($line->CUSTOMER_ID, $template, array('msisdn' => $msisdn, 'activations' => $line->ACTIVATIONS_NEEDED));
    if ($return->is_failure())
      throw new \Exception("failed to send SMS to CUSTOMER_ID {$line->CUSTOMER_ID} due to " . json_encode($result_object->get_errors()));

    $result = TRUE;
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}


/**
 * flushRedisKeyByDealerId
 * flush the redis key by dealer_id when a new record is inserted or one is updated
 * @param Integer $dealer_id 
 */
function flushRedisKeyByDealerId($dealer_id)
{
  $redis = new \Ultra\Lib\Util\Redis;
  $key = DEMO_LINE_CACHE_KEY . $dealer_id;
  $redis->del($key);
}


/**
 * findDealerById
 * search array of daelear for given id
 * @param Interger dealer ID
 * @param Array of Objects
 * @return Object found dealer or NULL if not found
 */
function findDealerById($id, $dealers)
{
  $result = NULL;
  
  try
  {
    if (empty($id) || empty($dealers) || ! is_array($dealers) || ! count($dealers))
      throw new \Exception('invalid parameter: ' . json_encode(func_get_args()));

    foreach ($dealers as $dealer)
      if ($dealer->id == $id)
      {
        $result = $dealer;
        break;
      }
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}


/**
 * getActiveLines
 * get all active or warning demo lines
 * @return Array of Objects
 */
function getActiveLines()
{
  $result = FALSE;

  try
  {
    // get all existing active demo lines
    $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.DEMO_LINE', NULL, NULL, array('STATUS' => array(DEMO_STATUS_ACTIVE, DEMO_STATUS_WARNING)), NULL, NULL, TRUE);
    $lines = mssql_fetch_all_objects(logged_mssql_query($sql));
    if ( ! count($lines))
      throw new \Exception('no active lines found, DB malfunction');
    dlog('', 'found %d active/warning demo lines', count($lines));
    $result = $lines;
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}


/**
 * getEligibleDealers
 * load eligible demo line dealers along with their codes and activation counts
 * @return Array of Objects
 */
function getEligibleDealers($mode = 'month')
{
  $dealers = NULL;

  try
  {
    // get eligible dealers with their activation counts for this month as of yesterday
    $participants = \Ultra\UltraConfig\isDevelopmentDB() ? array() : \Ultra\UltraConfig\dealer_portal_demo_line_dealers(); // get soft launch list if running in production
    $dealers = get_eligible_demo_line_dealers($participants, $mode);
    if ( ! is_array($dealers) || ! count($dealers))
      throw new \Exception('no eligible dealers found, DB malfunction');
    dlog('', 'found %d eligible dealers', count($dealers));

    // get dealer codes
    foreach ($dealers as &$dealer)
    {
      // determine dealer type and number of eligible lines
      $dealer->type = substr($dealer->code, 0, strlen(DEMO_DEALER_EPP)) == DEMO_DEALER_EPP ? DEMO_DEALER_EPP : DEMO_DEALER_1_1;

      if ($dealer->activations >= 20)
        $dealer->eligible = 2;
      else // either EPP or 10 activations
        $dealer->eligible = 1;

      if (empty($dealer->activations)) // correct NULL value for EPP dealer without activations
        $dealer->activations = 0;

      // get all existing demo lines for this dealer
      $dealer->lines = ultra_get_demo_lines_for_eligibility_check($dealer->id);
      dlog('', 'dealer id %d, code %s, type %s with %d activations is eligible for %d demo lines, has %d lines', $dealer->id, $dealer->code, $dealer->type, $dealer->activations, $dealer->eligible, count($dealer->lines));
    }
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $dealers;
}


/**
 * dailyEligibilityCheck
 * daily demo line eligibility check
 * @return Boolean TRUE on success FALSE on failure
 */
function dailyEligibilityCheck($dealers)
{
  $result = FALSE;

  try
  {
    if ( ! is_array($dealers))
      throw new \Exception('invalid parameter: ' . json_encode($dealers));

    foreach ($dealers as $dealer)
    {
      // process each active or eligible line
      $max = max($dealer->eligible, count($dealer->lines));
      for ($i = 0; $i < $max; $i++)
      {
        $sql = NULL;
        if ( ! empty($dealer->lines[$i])) // line exists
        {
          $line = $dealer->lines[$i]; // for convenience
          dlog('', 'checking line %d of %d eligible for dealer code %s, dealer id %d: line id %d, status %s', $i + 1, $max, $dealer->code, $dealer->id, $line->DEMO_LINE_ID, $line->STATUS);

          // if line exists with status 'warning' and is now eligible then update to 'active'
          if ($line->STATUS == DEMO_STATUS_WARNING && $line->ACTIVATIONS_NEEDED / 10 <= $dealer->eligible)
          {
            if ( ! updateDemoLineStatus($line->DEMO_LINE_ID, array('STATUS' => DEMO_STATUS_ACTIVE), $dealer->id))
              throw new \Exception("failed to update demo line id {$line->DEMO_LINE_ID} status from {$line->STATUS} to " . DEMO_STATUS_ACTIVE);
          }
        }

        // if eligible line does not exist then add it with status 'eligible'
        else
        {
          dlog('', 'checking line %d of %d eligible for dealer code %s, dealer id %d: line does not exist', $i + 1, $dealer->eligible, $dealer->code, $dealer->id);
          if ( ! createDemoLine($dealer->id, $dealer->type, $i ? 20 : ($dealer->type == DEMO_DEALER_EPP ? 0 : 10)))
            throw new \Exception("failed to create new eligible line for dealer ID {$dealer->id}");
        }
      }
    }

    $result = TRUE;
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}


/**
 * startOfMonthCheck
 * check all lines at the beginning of each month
 * @return Boolean TRUE on success FALSE on failure
 */
function startOfMonthCheck($dealers, $lines)
{
  $result = FALSE;

  try
  {
    if ( ! is_array($dealers) || ! is_array($lines))
      throw new \Exception('invalid parameter: ' . json_encode($dealers) . json_encode($lines));

    foreach ($lines as $line)
    {
      // determine if line is eligible
      $dealer = findDealerById($line->DEALER_ID, $dealers);
      if ( ! $dealer || $line->ACTIVATIONS_NEEDED > $dealer->activations) // not eligible
      {
        dlog('', 'line %d with status %s for dealer ID %d with %d activations is not eligible', $line->DEMO_LINE_ID, $line->STATUS, $line->DEALER_ID, empty($dealer->activations) ? 0 : $dealer->activations);

        if ($line->STATUS == DEMO_STATUS_ACTIVE)
        {
          if ( ! updateDemoLineStatus($line->DEMO_LINE_ID, array('STATUS' => DEMO_STATUS_WARNING), $line->DEALER_ID))
            throw new \Exception("failed to update demo line id {$line->DEMO_LINE_ID} status from {$line->STATUS} to " . DEMO_STATUS_WARNING);

          // send REMINDER SMS
          sendDemoLineSms($line, DEMO_SMS_REMINDER);
        }

        // if line status is already 'warning' then invalidate it
        elseif ($line->STATUS == DEMO_STATUS_WARNING)
        {
          if ( ! updateDemoLineStatus($line->DEMO_LINE_ID, array('STATUS' => DEMO_STATUS_INVALID, 'INVALID_DATE' => 'NOW'), $line->DEALER_ID))
            throw new \Exception("failed to update demo line id {$line->DEMO_LINE_ID} status from {$line->STATUS} to " . DEMO_STATUS_INVALID);

          // remove line from ULTRA.CUSTOMER_OPTIONS
          if ( ! remove_customer_options($line->CUSTOMER_ID, array(BILLING_OPTION_DEMO_LINE)))
            throw new \Exception("failed to remove demo line option for customer_id {$line->CUSTOMER_ID}");

          // send REMINDER SMS
          sendDemoLineSms($line, DEMO_SMS_CANCELLED);
        }
      }
    }

    $result = TRUE;
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}


/**
 * middleOfMonthCheck
 * check all demo lines in the middle of each month and issue a warning SMS
 * @return Boolean TRUE on success FALSE on failure
 */
function middleOfMonthCheck($dealers, $lines)
{
  $result = FALSE;

  try
  {
    if ( ! is_array($dealers) || ! is_array($lines))
      throw new \Exception('invalid parameter: ' . json_encode($dealers) . json_encode($lines));

    foreach ($lines as $line)
    {
      // determine if line is eligible
      $dealer = findDealerById($line->DEALER_ID, $dealers);
      if ( ! $dealer || $line->ACTIVATIONS_NEEDED > $dealer->activations) // not eligible
      {
        dlog('', 'line %d with status %s for dealer ID %d with %d activations is not eligible', $line->DEMO_LINE_ID, $line->STATUS, $line->DEALER_ID, empty($dealer->activations) ? 0 : $dealer->activations);

        // if line status is 'warning' then issue warning SMS
        if ($line->STATUS == DEMO_STATUS_WARNING)
          sendDemoLineSms($line, DEMO_SMS_WARNING);
      }
    }

    $result = TRUE;
  }

  catch(\Exception $x)
  {
    dlog('', 'EXCEPTION: %s', $x->getMessage());
  }

  return $result;
}

/**
 * checkForUnclaimedLinesAndUpdate
 * if a line has status 'eligible' and has expired then update status to 'unclaimed'
 * unless it is the first line of an EPP dealer
 * @return NULL
 */
function checkForUnclaimedLinesAndUpdate()
{
  $lines = ultra_get_eligible_demo_lines();
  foreach ($lines as $line)
  {
    if ($line->STATUS == DEMO_STATUS_ELIGIBLE && ! empty($line->CLAIM_BY_DATE) && date_to_epoch($line->CLAIM_BY_DATE) < time() && $line->ACTIVATIONS_NEEDED > 0)
    {
      dlog('', 'DEMO_LINE_ID %d status updated to unclaimed', $line->DEMO_LINE_ID);
      if ( ! updateDemoLineStatus($line->DEMO_LINE_ID, array('STATUS' => DEMO_STATUS_UNCLAIMED), $line->DEALER_ID))
        throw new \Exception("failed to update demo line id {$line->DEMO_LINE_ID} status from {$line->STATUS} to " . DEMO_STATUS_UNCLAIMED);
    }
  }
}


/**
 * transferDemoLines
 * move dealer demo lines from children to parent
 * @see PROD-1743
 * @param Object parent dealer
 * @returns Array (String error message or NULL on success, String warning message or NULL on success)
 */
function transferDemoLines($parent)
{
  // get all children and check result
  list($children, $error) = \Ultra\Lib\DB\DealerPortal\getBusinessChildren(array('business_type' => 'Dealer', 'parent_business_id' => $parent->DEALERSITEID));
  if ($error)
    return array($error, NULL);
  if ( ! count($children)) // no children: nothing to do
    return array(NULL, NULL);

  // update ULTRA.DEMO_LINE
  teldata_change_db();
  $type = substr($parent->DEALERCD, 0, strlen(DEMO_DEALER_EPP)) == DEMO_DEALER_EPP ? DEMO_DEALER_EPP : DEMO_DEALER_1_1;
  for ($i = 0, $dealers = array(); $i < count($children); $i++)
    $dealers[] = $children[$i]->DEALERSITEID;
  if ( ! $sql = \Ultra\Lib\DB\makeUpdateQuery('ULTRA.DEMO_LINE', array('DEALER_ID' => $parent->DEALERSITEID, 'DEALER_TYPE' => $type), array('DEALER_ID' => $dealers)))
    return array(__FUNCTION__ . ': failed to prepare UPDATE statement (1)', NULL);
  if ( ! run_sql_and_check($sql))
    return array(__FUNCTION__ . ': failed to update demo lines (1)', NULL);

  // update CUSTOMER_OPTIONS
  for ($i = 0, $dealers = array(); $i < count($children); $i++)
    $dealers[] = $children[$i]->DEALERCD;
  if ( ! $sql = \Ultra\Lib\DB\makeUpdateQuery('ULTRA.CUSTOMER_OPTIONS', array('OPTION_VALUE' => $parent->DEALERCD), array('OPTION_ATTRIBUTE' => BILLING_OPTION_DEMO_LINE, 'OPTION_VALUE' => $dealers)))
    return array(__FUNCTION__ . ': failed to prepare UPDATE query (1)', NULL);
  if ( ! $result = is_mssql_successful(logged_mssql_query($sql)))
    return array(__FUNCTION__ . ': failed to update demo line (2)', NULL);

  // get current number of the parent's demo lines and emit a warning if at or over maximum (but do not cancel them)
  if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.DEMO_LINE', NULL, 'DEMO_LINE_ID', array('DEALER_ID' => $parent->DEALERSITEID, 'STATUS' => array(DEMO_STATUS_ACTIVE, DEMO_STATUS_WARNING)), NULL, NULL, TRUE))
    return array(__FUNCTION__ . ': failed to repare SELECT statement', NULL);
  $lines = mssql_fetch_all_objects(logged_mssql_query($sql));
  $message = 'Dealer ' . $parent->DEALERCD . ' has ' . count($lines) . ' demo line(s)';
  \logInfo($message);
  return array(NULL, count($lines) > 1 ? $message : NULL);
}

/**
 * decommission
 * $params is an associative Array(demo_line_id)
 * @param  Array of params
 * @return Object result
 */
function decommission($params)
{
  if (empty($params['demo_line_id']))
    return make_error_result('One or more required parameters are missing');

  if ( ! ultra_demo_line_update_status($params['demo_line_id'], DEMO_STATUS_INACTIVE) )
    return make_error_Result('FAILED updating ULTRA.DEMO_LINE status for DEMO_LINE_ID ' . $params['demo_line_id']);

  if ( ! $check = ultra_demo_line_delete_customer_option($params['demo_line_id']))
    return make_error_Result('FAILED deleting CUSTOMER_OPTION for DEMO_LINE_ID ' . $params['demo_line_id']);

  return make_ok_Result();
}

/**
 * assignCustomer
 * $params is an associative Array(demo_line_id,customer_id)
 * @param  Array of params
 * @return Object result
 */
function assignCustomer($params)
{
  if (empty($params['demo_line_id']) || empty($params['customer_id']))
    return make_error_result('One or more required parameters are missing');

  return ultra_demo_line_assign_new_customer($params['demo_line_id'], $params['customer_id']);
}

/**
 * changeSettings
 * $params is an associative Array(demo_line_id,customer_id,status,activations_needed)
 * @param  Array of params
 * @return Object result
 */
function changeSettings($params)
{
  if (empty($params['demo_line_id']) || empty($params['customer_id']) || empty($params['status']) || empty($params['activations_needed']))
    return make_error_result('One or more required parameters are missing');

  $sql = \Ultra\Lib\DB\makeUpdateQuery('ULTRA.DEMO_LINE',
    array(
      'CUSTOMER_ID'        => $params['customer_id'],
      'STATUS'             => $params['status'],
      'ACTIVATIONS_NEEDED' => $params['activations_needed']
    ),
    array('DEMO_LINE_ID' => $params['demo_line_id']));

  if ( ! $result = is_mssql_successful(logged_mssql_query($sql)))
    return make_error_Result('FAILED updating ULTRA.DEMO_LINE with DEMO_LINE_ID ' . $params['demo_line_id']);

  return make_ok_Result();
}

/**
 * addFundingRecord
 * $params is an associative Array(demo_line_id)
 * @param  Array of params
 * @return Object result
 */
function addFundingRecord($params)
{
  if (empty($params['demo_line_id']))
    return make_error_result('One or more required parameters are missing');

  return ultra_demo_line_add_missing_customer_option($params['demo_line_id']);
}


/**
 * finalizeActivation
 * update demo line DB records upon successfull ICCID activation
 * @param Integer customer ID
 * @param Integer demo line ID
 * @param String dealer email address
 * @param Integer number of required activations
 * @returns Array (Boolean success of failure, Array of error messages)
 */
function finalizeActivation($customerId, $lineId, $email, $activations)
{
  $result = array('success' => FALSE, 'errors' => array());

  // set the ULTRA.DEMO_LINE  to active
  $sql = \Ultra\Lib\DB\makeUpdateQuery(
    'ULTRA.DEMO_LINE',
    array('STATUS' => DEMO_STATUS_ACTIVE, 'CUSTOMER_ID' => $customerId),
    array('DEMO_LINE_ID' => $lineId));
  if ( ! is_mssql_successful(logged_mssql_query($sql)))
  {
    $result['errors'][] = 'failed to update demo line';
    return $result;
  }

  // errors here are not fatal
  if ( ! empty($email))
  {
    if ( ! $customer = get_ultra_customer_from_customer_id($customerId, array('current_iccid', 'current_mobile_number')))
      \logError("unable to get customer record for customer ID $customerId");

    else if ( ! $success = funcSendExemptCustomerEmail_demo_line_confirmation(array(
      'email'  => $email,
      'msisdn' => $customer->current_mobile_number,
      'iccid'  => $customer->current_iccid,
      'activations_needed' => $activations)))
      \logError('failed to send email to dealer');

  }

  $result['success'] = TRUE;
  return $result;
}

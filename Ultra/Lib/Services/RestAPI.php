<?php

namespace Ultra\Lib\Services;

abstract class RestAPI
{
  const DEFAULT_CONNECT_TIMEOUT = 5;
  const DEFAULT_RESPONSE_TIMEOUT = 10;
  const CURL_RETRY_CODES = [28, 55, 56];

	private $host;
	private $basePath;
	private $curlConfig;
  private $numRetries;

	public function __construct($host, $basePath=null, array $curlConfig=[], $numRetries=0)
	{
		$this->host = $host;
		$this->basePath = rtrim($basePath, '/') . '/';

		// default curl config values
		$curlConfig[CURLOPT_CONNECTTIMEOUT] = !empty($curlConfig[CURLOPT_CONNECTTIMEOUT]) ? $curlConfig[CURLOPT_CONNECTTIMEOUT] : self::DEFAULT_CONNECT_TIMEOUT;
		$curlConfig[CURLOPT_TIMEOUT] = !empty($curlConfig[CURLOPT_TIMEOUT]) ? $curlConfig[CURLOPT_TIMEOUT] : self::DEFAULT_RESPONSE_TIMEOUT;

		$this->curlConfig = $curlConfig;
    $this->numRetries = $numRetries;
	}

	private function call($method, $path, array $params=[], array $headers=[], $contentType='application/json', $retries=0)
	{
    $url = $this->host . $this->basePath . ltrim($path, '/');
    $result = [
      'code'  => null,
      'body'  => null
    ];

    $options = [];
    foreach ($this->curlConfig as $key=>$val) {
    	$options[$key] = $val;
    }
    $options[CURLOPT_HTTPHEADER] = [ 'Content-Type: ' . $contentType ];
    $options[CURLOPT_RETURNTRANSFER] = true;

    // add any extra headers
    foreach ($headers as $header) {
      $options[CURLOPT_HTTPHEADER][] = $header;
    }

    $method = strtoupper( $method );
    if ( $method == 'POST' || $method == 'PUT' || $method == 'DELETE') {
      $options[CURLOPT_CUSTOMREQUEST] = $method;
      $options[CURLOPT_POST] = true;
      $options[CURLOPT_POSTFIELDS] = json_encode( $params );
    } elseif ( $method == 'GET' ) {
      if (!empty($params)) {
        if ( strstr( $url, '?' ) !== false ) {
          $url .= '&' . http_build_query( $params );
        } else {
          $url .= '?' . http_build_query( $params );
        }
      }
    } else {
      throw new \Exception( 'Unhandled method ' . $method );
    }

    \logDebug( "method = $method, url = $url" );
    \logDebug( 'parameters = ' . json_encode( $params ) );

    // init HTTP session
    $urlConn = curl_init( $url );
    if ( !$urlConn )
      throw new \Exception( 'Failed curl session initialization' );

    \logDebug( json_encode( $options ) );

    if ( !curl_setopt_array( $urlConn, $options ) )
      throw new \Exception( 'Failed curl session configuration' );

    $result['body'] = curl_exec( $urlConn );
    $result['code'] = curl_getinfo( $urlConn, CURLINFO_HTTP_CODE );
    $result['curl_errno'] = curl_errno( $urlConn );

    \logDebug('API response: ' . json_encode($result));

    curl_close( $urlConn );

    // check for retry
    if (!empty($result['curl_errno']) 
      && in_array($result['curl_errno'], self::CURL_RETRY_CODES)
      && $retries > 0
    ) {
      \logDebug('cURL error (' . $result['curl_errno'] . ') - Retrying API request');
      sleep(1);
      return $this->call($method, $path, $params, $headers, $contentType, $retries-1);
    }

    return $result;
	}

	public function get($path, array $parameters=[], array $headers=[])
	{
		return $this->call('GET', $path, $parameters, $headers, 'application/json', $this->numRetries);
	}

	public function post($path, array $parameters=[], array $headers=[])
	{
		return $this->call('POST', $path, $parameters, $headers, 'application/json', $this->numRetries);
	}

  public function put($path, array $parameters=[], array $headers=[])
  {
    return $this->call('PUT', $path, $parameters, $headers, 'application/json', $this->numRetries);
  }

  public function delete($path, array $parameters=[], array $headers=[])
  {
    return $this->call('DELETE', $path, $parameters, $headers, 'application/json', $this->numRetries);
  }
}

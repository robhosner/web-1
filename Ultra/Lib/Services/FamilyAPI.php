<?php

namespace Ultra\Lib\Services;

require_once 'Ultra/Lib/Services/RestAPI.php';
require_once 'Ultra/Configuration/Configuration.php';

use \Ultra\Configuration\Configuration;

class FamilyAPI extends RestAPI
{
  private $configObj;
  private $config;

  public function __construct(Configuration $configObj=null)
  {
    $this->configObj = !empty($configObj) ? $configObj : new Configuration();

    $this->config = $this->configObj->getFamilyAPIConfig();
    if (empty($this->config['host']) || empty($this->config['basepath'])) {
      throw new \Exception('Missing FamilyAPI configuration');
    }

    $curlConfig = [ CURLOPT_TIMEOUT => 30 ];

    parent::__construct($this->config['host'], $this->config['basepath'], $curlConfig, 3);
  }

  public function getOrCreateFamilyID($customerID)
  {
    // check existing family
    $result = $this->getCustomerFamilyID($customerID);
    if ($result->is_success()) {
      return $result;
    } else {
      // create family
      return $this->createCustomerFamily($customerID);
    }
  }

  public function createCustomerFamily($customerID)
  {
    $response = $this->post('family', [ 'customerId' => $customerID ]);

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      // add family_id to result data
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);
      
      if (is_array($responseArr) && !empty($responseArr['familyId'])) {
        $result->data_array['family_id'] = $responseArr['familyId'];
        $result->data_array['invite_code'] = $responseArr['inviteCode'];
        $result->succeed();
      } else {
        $result->add_error('Failed to parse Family API response');
      }
      
      return $result;
    }
  }

  public function getCustomerFamilyID($customerID)
  {
    $response = $this->get('family?customerId=' . $customerID);

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      // add family_id to result data
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);
      
      if (is_array($responseArr) && !empty($responseArr['familyId'])) {
        $result->data_array['family_id'] = $responseArr['familyId'];
        $result->succeed();
      } else {
        $result->add_error('Failed to parse Family API response');
      }
      
      return $result;
    }
  }

  public function getFamilyByCustomerID($customerID)
  {
    // find family ID
    $result = $this->getCustomerFamilyID($customerID);
    if (!$result->is_success()) {
      return $result;
    }

    // get family by family ID
    $familyID = $result->data_array['family_id'];
    $response = $this->get('family/' . $familyID);
    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    }

    // add family_id to result data
    $result = new \Result();
    $responseArr = json_decode($response['body'], true);
      
    if (is_array($responseArr)) {
      foreach ($responseArr as $key => $val) {
        $result->data_array[$key] = $responseArr[$key];
      }
      
      $result->succeed();
    } else {
      $result->add_error('Failed to parse Family API response');
    }
      
    return $result;
  }

  public function applyFamilyBoltOns($familyID, array $boltOns, $immediate)
  {
    $response = $this->post(
      'family/' . $familyID . '/bolt-on',
      [
        'boltOns'   => $boltOns,
        'immediate' => (bool)$immediate
      ]
    );

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      // add family ID to result data
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);
      
      if (is_array($responseArr)
        && !empty($responseArr['familyId'])
        && array_key_exists('createdSharedDataBucket', $responseArr)
      ) {
        $result->data_array['family_id'] = $responseArr['familyId'];
        $result->data_array['createdSharedDataBucket'] = $responseArr['createdSharedDataBucket'];
        $result->succeed();
      } else {
        $result->add_error('Failed to parse Family API response');
      }
      
      return $result;
    }
  }

  public function applyFamilyBoltOnsByCustomerID($customerID, array $boltOns, $immediate)
  {
    // get the family ID for customer
    $result = $this->getCustomerFamilyID($customerID);
    if ($result->is_success()) {
      if (!empty($result->data_array['family_id'])) {
        // apply bolt on to family
        $familyID = $result->data_array['family_id'];

        return $this->applyFamilyBoltOns($familyID, $boltOns, $immediate);
      } else {
        $result->add_error('Missing family_id in response');
        return $result;
      }
    } else {
      return $result;
    }
  }

  public function getFamilyIDByInviteCode($inviteCode)
  {
    $response = $this->get('family?inviteCode=' . $inviteCode);

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      // add family ID to result data
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);
      
      if (is_array($responseArr) && !empty($responseArr['familyId'])) {
        $result->data_array['family_id'] = $responseArr['familyId'];
        $result->succeed();
      } else {
        $result->add_error('Failed to parse Family API response');
      }
      
      return $result;
    }
  }

  public function joinFamily($customerID, $inviteCode)
  {
    $response = $this->post(
      'family/join',
      [
        'customerId'  => $customerID,
        'inviteCode'  => $inviteCode
      ]
    );

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      $result = new \Result();
      $result->succeed();

      return $result;
    }
  }

  public function activateFamilyMember($customerID)
  {
    $response = $this->post('family/activate', ['customerId' => $customerID]);

    $result = new \Result();
    if ($response['code'] == 200) {
      $result->succeed();
    } else if ($response['code'] == 409) {
      // treat already activated as success
      $result->succeed();
    } else {
      return $this->getErrorResult($response);
    }

    return $result;
  }

  public function removeFamilyMember($familyId, $customerID)
  {
    $response = $this->delete("family/$familyId/members/$customerID");
    $result = new \Result();

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    }

    $result->succeed();
    return $result;
  }

  public function getCustomers(array $customerIds)
  {
    $response = $this->get("customer?customerIds=" . implode(",", $customerIds));
    $result = new \Result();

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    }

    $responseArr = json_decode($response['body'], true);

    if (is_array($responseArr) && !empty($responseArr['customerIds'])) {
      $result->data_array['customerIds'] = $responseArr['customerIds'];
      $result->succeed();
    } else {
      $result->add_error('Failed to parse Family API response');
    }

    return $result;
  }

  private function getErrorResult($response)
  {
    $result = new \Result();

    // check for curl errors
    if (!empty($response['curl_errno'])) {
      $result->add_error('Family API curl failed - ' . $response['curl_errno']);
      return $result;
    }

    // add errors from API response
    if (!empty($response['body'])) {
      $responseArr = json_decode($response['body'], true);
      if (is_array($responseArr) && !empty($responseArr['errors'])) {
        foreach ($responseArr['errors'] as $error) {
          $result->add_error($error);
        }
      }
    }

    return $result;
  }
}

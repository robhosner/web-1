<?php
namespace Ultra\Lib\Services;

use Ultra\Configuration\Configuration;

class UserDeviceAPI extends RestAPI
{
  private $configObj;
  private $config;

  public function __construct(Configuration $configObj)
  {
    $this->configObj = $configObj;

    $this->config = $this->configObj->getUserDeviceAPIConfig();
    if (empty($this->config['host']) || empty($this->config['basepath'])) {
      throw new \Exception('Missing UserDevice configuration');
    }

    $curlConfig = [ CURLOPT_TIMEOUT => 30 ];

    parent::__construct($this->config['host'], $this->config['basepath'], $curlConfig, 3);
  }

  public function sendApnSettings($msisdn)
  {
    $response = $this->post('/apn', ['msisdn' => "$msisdn"]);

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      $result = new \Result(null, true);
      return $result;
    }
  }

  public function setDeviceInfo($msisdn, $iccid, $imei)
  {
    $response = $this->post(
      '/iccid/' . $iccid,
      [
        'MSISDN'  => $msisdn,
        'IMEI'    => $imei
      ]
    );

    if ($response['code'] != 200) {
      return $this->getErrorResult($response);
    } else {
      $result = new \Result();
      $responseArr = json_decode($response['body'], true);

      if (is_array($responseArr) && array_key_exists('newDevice', $responseArr)) {
        $result->data_array['new_device'] = $responseArr['newDevice'];
        $result->succeed();
      } else {
        $result->add_error('Failed to parse User Device API response');
      }

      return $result;
    }
  }

  private function getErrorResult($response)
  {
    $result = new \Result();

    // check for curl errors
    if (!empty($response['curl_errno'])) {
      $result->add_error('UserDevice API curl failed - ' . $response['curl_errno']);
      return $result;
    }

    // add errors from API response
    if (!empty($response['body'])) {
      $responseArr = json_decode($response['body'], true);
      if (is_array($responseArr) && !empty($responseArr['errors'])) {
        foreach ($responseArr['errors'] as $error) {
          $result->add_error($error);
        }
      }
    }

    return $result;
  }
}
<?php

require_once 'db.php';
require_once 'Ultra/Lib/StateMachine.php';
#require_once 'Primo/Lib/StateMachine/DataEngineMongoDB.php';
require_once 'Ultra/Lib/StateMachine/DataEngineMSSQL.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRAM.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRedis.php';

# Ultra specific actions and requirements
require_once 'Ultra/Lib/StateMachine/Action/functions.php';
require_once 'Ultra/Lib/StateMachine/Requirement/functions.php';

teldata_change_db();

$redis = new \Ultra\Lib\Util\Redis();

$dataEngineMSSQLObject = new \Ultra\Lib\StateMachine\DataEngineMSSQL;

$sm = new \Ultra\Lib\StateMachine( $dataEngineMSSQLObject , NULL , NULL , $redis );


print_r( $sm );

$state = 'Neutral';
$label = 'Provision STHIRTY_NINE';
$customerData = array(
  'customer_id' => 31,
  'cos_id'      => 123,
  'plan_state'  => 'Neutral'
);

/*
$sm->getForwardStar( $state );

print_r( $sm );

$label = 'Request Port FIFTY_NINE';

$sm->selectTransitionByLabel( $label , $state );

print_r( $sm->getCurrentTransition() );

$sm->generateTransitionData();

print_r( $sm );
*/

$sm->setCustomerData( $customerData );

print_r( $sm );

$sm->selectTransitionByLabel( $label , $state );

print_r( $sm->getCurrentTransition() );

$sm->generateTransitionData();

print_r( $sm );

if ( ! $sm->is_success() )
{
  die("generateTransitionData failed!\n");
}

$sm->runTransition();

#print_r( $sm );


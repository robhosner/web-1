<?php

require_once 'db.php';
require_once 'Ultra/Lib/DB/Merchants/Tokenizer.php';

teldata_change_db();

$tokenizer = new \Ultra\Lib\DB\Merchants\Tokenizer();

$params = array(
  'cc_number'    => '',
  'last_name'    => '',
  'expires_date' => ''
);

if ( $tokenizer->alwaysFailAccount( $params ) )
{ echo "always fail\n"; } else { echo "not always fail\n"; }

if ( $tokenizer->alwaysSucceedAccount( $params ) )
{ echo "always succeed\n"; } else { echo "not always succeed\n"; }

$params = array(
  'bin'          => '',
  'last_four'    => '',
  'last_name'    => '',
  'expires_date' => ''
);

if ( $tokenizer->alwaysFailAccount( $params ) )
{ echo "always fail\n"; } else { echo "not always fail\n"; }

if ( $tokenizer->alwaysSucceedAccount( $params ) )
{ echo "always succeed\n"; } else { echo "not always succeed\n"; }



/*

// invalid
$params = array(
  'address'      => '123 Main',
  'zipcode'      => '12345',
  'cc_number'    => '1234123412341234',
  'expires_date' => '1221',
  'cvv'          => '1234',
  'last_name'    => 'test'
);

// always fail
$params = array(
  'address'      => '123 Main',
  'zipcode'      => '12345',
  'cc_number'    => '4195473398654406',
  'expires_date' => '1214',
  'cvv'          => '990',
  'last_name'    => 'Ultrasmith'
);

*/

// always succeed
$params = array(
  'address'                 => '123 Main',
  'zipcode'                 => '12345',
  'cc_number'               => '4067129148890000',
  'expires_date'            => '1216',
  'cvv'                     => '110',
  'last_name'               => 'Jack Ultrasmith',
  'client_reference_number' => 'ULTRA'
);

/*
$params = array(
  'cc_number'    => '4111111111111111',
  'customer_id'  => '33',
  'last_name'    => '',
  'expires_date' => '0915',
  'gateway' => 'MeS',
  'token'   => 'dummytoken',
  'charge_amount' => 12.34
);
*/

$result = $tokenizer->runVerify( $params );
#$result = $tokenizer->runCharge( $params );

print_r( $result );

exit;

/*
$params = array();
$result = $tokenizer->runTokenization( $params );
print_r( $result );

$params['expires_date'] = '0116';
$result = $tokenizer->runTokenization( $params );
print_r( $result );

$params['cc_number'] = '4012301230123010';
$result = $tokenizer->runTokenization( $params );
print_r( $result );

$params['customer_id'] = 5;
$result = $tokenizer->runTokenization( $params );
print_r( $result );

exit;

*/

// test always succeed
$params = array(
  'expires_date' => '1214',
  'cc_number'    => '4067129148899676',
  'customer_id'  => '3184',
  'last_name'    => 'Ultrasmith'
);

/*

$params = array(
  'cc_number'    => '4111111111111111',
  'customer_id'  => '3',
  'last_name'    => '',
  'expires_date' => '0915'
);

*/

$result = $tokenizer->runTokenization( $params );
print_r( $result );

?>

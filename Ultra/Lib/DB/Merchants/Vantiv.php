<?php

namespace Ultra\Lib\DB\Merchants;

use \Ultra\Lib\Services\PaymentAPI;
use \Ultra\Configuration\Configuration;

require_once 'Ultra/Lib/DB/Merchants/MerchantInterface.php';
require_once 'Ultra/Configuration/Configuration.php';
require_once 'Ultra/Lib/Services/PaymentAPI.php';

class Vantiv implements MerchantInterface
{
  private $configObj;
  private $api;

  public function __construct(Configuration $configObj=null, PaymentAPI $paymentAPI=null)
  {
    $this->configObj = !empty($configObj) ? $configObj : new Configuration();
    $this->api = !empty($paymentAPI) ? $paymentAPI : new PaymentAPI($this->configObj);
  }

  /**
   * tokenize
   *
   * Creates token and returns it
   *
   * @returns a Result object
   */
  public function tokenize( $params )
  {
    // same as verify
    $result = $this->verify($params);
    $result->data_array = $result->data_array + $this->merchantInfo();

    return $result;
  }

  /**
   * merchantInfo
   *
   * Specific Merchant Data to be stored in ULTRA.CC_HOLDER_TOKENS
   *
   * @return array
   */
  public function merchantInfo()
  {
    return array(
      'gateway'          => 'vntv',
      'merchant_account' => '1',
      'cc_processor_id'  => $this->configObj->getCCProcessorIDByName('vantiv')
    );
  }

  /**
   * sale
   *
   * Invoke MeS API for a sale
   *
   * @returns a Result object
   */
  public function sale( $params, $recurring=false )
  {
    $required = [ 'token', 'cc_transactions_id', 'customer_id', 'expires_date', 'amount', 'brand_id' ];
    foreach ($required as $field) {
      if (!array_key_exists($field, $params)) {
        return \make_error_Result('Missing ' . $field . ' parameter');
      }
    }

    // call payment API
    $data = [
      'token'           => $params['token'],
      'orderID'         => (string)$params['cc_transactions_id'],
      'customerID'      => $params['customer_id'],
      'brandID'         => (int)$params['brand_id'],
      'expDate'         => $params['expires_date'],
      'amount'          => (int)round($params['amount'] * 100),
      'billingAddress1' => !empty($params['address']) ? $params['address'] : null,
      'billingZipCode'  => !empty($params['zipcode']) ? $params['zipcode'] : null,
      'recurring'       => $recurring
    ];

    return $this->api->charge($params['cc_processors_id'], $data);
  }

  /**
   * saleRecurring
   *
   * Invoke Vantiv API for a recurring sale
   *
   * @returns a Result object
   */
  public function saleRecurring( $params )
  {
    return $this->sale($params, true);
  }

  /**
   * void
   *
   * Invoke Vantiv API for voiding a transaction
   *
   * @returns a Result object
   */
  public function void( $params )
  {
    throw new \Exception('Unhandled');
  }

  /**
   * refund
   *
   * Invoke Vantiv API for refunding a transaction
   *
   * @returns a Result object
   */
  public function refund( $params )
  {
    throw new \Exception('Unhandled');
  }

  /**
   * verify
   *
   * Invoke Vantiv API for validating a credit card
   *
   * @returns a Result object
   */
  public function verify( $params )
  {
    $required = ['token', 'customer_id', 'brand_id'];
    foreach ($required as $field) {
      if (!array_key_exists($field, $params)) {
        return \make_error_Result('Missing required field ' . $field);
      }
    }

    $data = [
      'token'           => $params['token'],
      'billingAddress1' => !empty($params['address']) ? $params['address'] : null,
      'billingCity'     => !empty($params['cc_city']) ? $params['cc_city'] : null,
      'billingState'    => !empty($params['cc_state_region']) ? $params['cc_state_region'] : null,
      'billingZipCode'  => !empty($params['zipcode']) ? $params['zipcode'] : null,
      'expDate'         => !empty($params['expires_date']) ? $params['expires_date'] : null,
      'orderID'         => !empty($params['cc_transactions_id']) ? (string)$params['cc_transactions_id'] : (string)time(),
      'customerID'      => $params['customer_id'],
      'brandID'         => (int)$params['brand_id']
    ];

    $result = $this->api->verify($this->configObj->getCCProcessorIDByName('vantiv'), $data);
    if ($result->is_success()) {
      // token to use is in response... set overrides
      $result->data_array['overrides'] = [];
      $result->data_array['overrides']['token'] = $result->data_array['token'];
    }

    // set validation response codes
    $result->data_array['transaction_id'] = time();
    $result->data_array['cvv_validation'] = 'M';
    $result->data_array['avs_validation'] = 'Y';

    return $result;
  }
}

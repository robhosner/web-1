<?php

namespace Ultra\Lib\DB\Merchants;

require_once 'Ultra/Lib/DB/Merchants/MerchantInterface.php';

/**
 * Test Merchant class for tokenization
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Credit Card Processor
 */

class Test implements MerchantInterface
{
  /**
   * tokenize
   *
   * Creates token and returns it
   *
   * Input:
   *  - 'cc_number'
   *  - 'expires_date'
   */
  public function tokenize( $params )
  {
    dlog('', "(%s)", func_get_args());

    $result = \make_ok_Result(
      array(
        'token' => '_test_invalid_token_raf_'
      )
    );

    $result->data_array = array_merge( $result->data_array , $this->merchantInfo() );

    return $result;
  }

  /**
   * merchantInfo
   *
   * Specific Merchant Data to be stored in ULTRA.CC_HOLDER_TOKENS
   *
   * @return array
   */
  public function merchantInfo()
  {
    return array(
      'gateway'          => 'Test',
      'merchant_account' => '1'
    );
  }

  /**
   * sale
   *
   * @returns a Result object
   */
  public function sale( $params )
  {
    dlog('', "(%s)", func_get_args());

    return \make_ok_Result();
  }

  /**
   * void
   *
   * @returns a Result object
   */
  public function void( $params )
  {
    dlog('', "(%s)", func_get_args());

    return \make_ok_Result();
  }

  /**
   * void
   *
   * @returns a Result object
   */
  public function refund( $params )
  {
    dlog('', "(%s)", func_get_args());

    return \make_ok_Result();
  }

  public function verify($params)
  {
    dlog('', "(%s)", func_get_args());

    return \make_ok_Result();
  }
}

?>

#!/usr/bin/perl


use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::DB::MSSQL;
use Ultra::Lib::DB::SoapLog;
use Ultra::Lib::Util::Config;


my $config = Ultra::Lib::Util::Config->new();


# connect to DB
my $db = Ultra::Lib::DB::MSSQL->new( DB_NAME => 'ULTRA_ACC' , CONFIG => $config );

my $dbSoapLog = Ultra::Lib::DB::SoapLog->new( DB => $db );

my $query = " SELECT TOP 1 * FROM MISO_QUEUE ";

my $sth = $dbSoapLog->{ DB }->executeSelect( $query );

if ( $dbSoapLog->{ DB }->hasErrors() )
{
  die Dumper( $dbSoapLog->{ DB }->getErrors() );
}
else
{
  die Dumper( $sth->fetchrow_hashref() );
}

__END__

sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli3_dev HTT_CONFIGROOT=/home/ht/config/rgalli3_dev ./Ultra/Lib/DB/UltraGatewayQueries_test.pl'


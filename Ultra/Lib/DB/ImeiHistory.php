<?php

namespace Ultra\Lib\DB\ImeiHistory;

/**
 * insertImeiHistory
 *
 * Executes stored procedure [ultra].[Insert_Imei_History]
 *
 * @return array
 * @param array $params
 */
function insertImeiHistory($payload)
{  
  $params = array(
      array(
      'param_name' => '@IMEI',
      'variable'   => $payload['imei'],
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 16,
    ),
    array(
      'param_name' => '@MSISDN',
      'variable'   => $payload['msisdn'],
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 10,
    ),
    array(
      'param_name' => '@ICCID',
      'variable'   => $payload['iccid'],
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 19,
    ),
  );
  return \Ultra\Lib\DB\run_stored_procedure('[ultra].[Insert_Imei_History]', $params);
}
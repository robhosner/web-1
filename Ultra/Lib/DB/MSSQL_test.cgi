#!/usr/bin/perl


use strict;
use warnings;


use lib "$ENV{DOCUMENT_ROOT}";

use CGI;
use Data::Dumper;
use Ultra::Lib::DB::MSSQL;

my $q = CGI->new;

my $db = Ultra::Lib::DB::MSSQL->new();

my $dbh = $db->dbh();

my $sql = "SELECT GETUTCDATE();";

my @out1 = @{$dbh->selectall_arrayref($sql)};

my $out2 = Dumper($db->readall($sql));

my $insert_sql = "
INSERT INTO htt_auditlog (
  [EVENT_UUID],
  [EVENT_MODULE],
  [EVENT_NAME],
  [EVENT_RESULT],
  [SITE_ID],
  [CUSTOMER_ID],
  [SCOPE_NET],
  [SCOPE_SOURCE],
  [SCOPE_DEST],
  [SCOPE_CHANNEL],
  [SCOPE_DOLLAR_COST],
  [SCOPE_CHANNEL_TYPE]
) VALUES (
  1,1,1,1,1,1,1,1,1,1,1,1
)";

$db->executeWrite($insert_sql);

my $html = "\n$sql\n".Dumper(\@out1)."<br>$out2";

print
  $q->header().
  $q->start_html('Ultra::Lib::DB::MSSQL test').
  $q->h1('Ultra::Lib::DB::MSSQL test').
  $html.
  $q->end_html;

__END__


<?php

namespace Ultra\Lib\DB;

require_once 'Ultra/Lib/DB/MSSQL.php';

/**
 * Cache
 * database access cashing layer
 * change log
 *  v1.0: implements read only access by primary key
 *  v1.1: added database connection initialization: application can create multiple class instances for working with several databases concurrently
 *  v1.2: added method selectUserError; connect to DB only if needed
 *  v1.3: moved DB operations to PDO/libdb to avoid interference with mssql_x functions used by existing code base
 * @author VYT 2015
 */
class Cache
{
  // contants
  const DEFAULT_TTL = 3600; // default caching time is 1 hour

  // variables
  private $redis        = NULL;  // Object redis instance
  private $link         = NULL;  // Resource database connection
  private $host         = NULL;  // String database host name
  private $port         = NULL;  // database connection port
  private $database     = NULL;  // String database name
  private $error        = NULL;  // String last error message encountered


  /**
   * constructor
   * without specific host and DB the class will connect to default host and DB (aka ULTRA_DATA)
   * @param Object Redis instance
   * @param String database host name
   * @param String database name
   * @param Integer database connection port
   */
  public function __construct($redis = NULL, $host = NULL, $database = NULL, $port = NULL)
  {
    $this->redis = ($redis && is_object($redis)) ? $redis : new \Ultra\Lib\Util\Redis;
    if ( ! $host)
      list($host, $port) = explode(':',  host());
    $this->host = $host;
    $this->port = $port ? $port : defaultPort();
    $this->database = $database ? $database : \Ultra\UltraConfig\getDBName();
  }


  /**
   * destructor
   */
  public function __destruct()
  {
  }


  /**
   * getLastError
   * return and clear last error message
   * @return String error message or NULL of no error were encountered
   */
  public function getLastError()
  {
    $error = $this->error;
    $this->error = NULL;
    return $error;
  }


  /**
   * selectRow
   * fetch a single table row from database cache by primary key
   * example: $cache->select('ULTRA.USER_ERROR_MESSAGES', 'ERROR_CODE', 'FA0003');
   * @param String table name
   * @param String primary key name
   * @param Mixed primary key value
   * @param Integer max cache TTL
   * @param Boolean DB lock
   *   0:  get current DB value, but still cache it for future use with default TTL
   *   -1: do not cache
   * @return Object table row on success or NULL on failure
   */
  function selectRow($table, $primary, $value, $ttl = self::DEFAULT_TTL, $lock = FALSE)
  {
    if ( ! $schema = $this->validateInput($table, $primary, $value))
      return NULL;

    // get cached value from redis
    if ($ttl >= 0)
    {
      // get cached row and check if data is sufficently fresh: an imperect method since we cannot get the original TTL
      $key = $this->cacheKey($table, $primary, $value);
      $row = $this->redis->get($key);
      if ($row && $this->redis->ttl($key) <= $ttl)
        return json_decode($row);
    }

    // get current value from DB
    if ( ! $row = $this->prepExecPlan($table, $schema, $primary, $value, $lock))
      return NULL;

    // cache and return DB result
    if ($ttl >= 0)
      $this->redis->set($key, json_encode($row), $ttl ? $ttl : self::DEFAULT_TTL);
    return $row;
  }


  /**
   * selectUserError
   * return a row of ULTRA.USER_ERROR_CODES by error code
   * converience function because it always returns an error, even if DB error has occurred
   * @param String error code ULTRA.USER_ERROR_CODES.ERROR_CODE
   * @return Object DB row
   */
  public function selectUserError($code)
  {
    if ( ! $row = $this->selectRow('ULTRA.USER_ERROR_MESSAGES', 'ERROR_CODE', $code, SECONDS_IN_DAY, FALSE))
    {
      // return generic hard coded error with the last DB driver message in DETAILS
      if ( ! $message = $this->getLastError())
        $message = "unknown error code $code";
      logError("failed to retrieve DB row for error $code: $message");
      $row = (object)array(
        'ERROR_CODE'  => 'DB0001',
        'EN_MESSAGE'  => 'An unexpected database error has occurred',
        'ES_MESSAGE'  => 'Se ha producido un error inesperado',
        'ZH_MESSAGE'  => '\u7a81\u53d1\u6570\u636e\u5e93\u9519\u8bef',
        'DETAILS'     => $message);
    }

    return $row;
  }


  /**
   * purge
   * remove previously cached data from cache, so that next time it is requested a DB read is forced
   * @param String table name
   * @param String primary key name
   * @param Mixed primary key value
   */
  public function purge($table, $primary, $value)
  {
    if ( ! $schema = $this->validateInput($table, $primary, $value))
      return NULL;

    // remove from cache
    $key = $this->cacheKey($table, $primary, $value);
    $this->redis->del($key);
  }


  /****************   PRIVATE METHODS ONLY  BEYOND THIS LINE   ****************/

  /**
   * validateInput
   * validate schema and primary key value
   * @param String table name
   * @param String PK name
   * @param Mixed PK value
   * @return Array table schema on success or NULL on failure
   */
  private function validateInput($table, $primary, $value)
  {
    if ( ! $schema = getTableSchema($table))
      return $this->setError("unknown table $table");

    if (empty($schema[$primary]))
      return $this->setError("unknown primary key $primary");

    // this precludes using 0 (zero) as primary key value but in reality we never have such rows
    if (empty($value))
      return $this->setError("missing primary key $primary value '$value'");

    return $schema;
  }


  /**
   * cacheKey
   * compose cache key of format NAMESPACE/CLASS/[HOST]/[DATABASE]/TABLE/KEY
   * @param String table name
   * @param String primary key column name
   * @param String primary key value
   * @return String cache key
   */
  private function cacheKey($table, $primary, $value)
  {
    return strtolower(str_replace('\\', '/', __CLASS__) . "/{$this->database}/$table/$primary/") . $value;
  }


  /**
   * prepExecPlan
   * generate parameterized SQL query based on a primary key, return a single result table row
   * achieves the same performance as a stored procedure; requies MSSQL server 2008 or higher
   * as of 2016 PHP 5.6 this is the only way to create a cached and reusable query plan using dblib driver
   * @see https://msdn.microsoft.com/en-us/library/ms188001.aspx
   * @author: VYT, 2015-12-08
   * @param String table name
   * @param Array table schema
   * @param String primary key column name
   * @param Mixed primary key value
   * @param Boolean DB lock
   * @return Object table row on success or NULL on failure
   */
  private function prepExecPlan($table, $schema, $primary, $value, $lock)
  {
    $data = NULL;
    $statement = NULL;

    try
    {
      // connect to DB if needed
      if ( ! $this->link && ! $this->openConnection())
        return logError("failed to connect to database {$this->database} on host {$this->host}");

      // prepare primary key schema
      $strings = array('CHAR', 'NCHAR', 'VARCHAR', 'NVARCHAR', 'TEXT', 'NTEXT');
      $integers = array('BIT', 'INT', 'BIGINT', 'SMALLINT', 'TINYINT', 'NUMERIC', 'DECIMAL');
      list($type, $length, $nullable) = parseColumnDefinition($schema[$primary]);

      // certain SQL types require length (extended schema definition) to proceed
      if (in_array($type, $strings) && ! $length)
        return $this->setError("missing extended schema definition for table $table");

      // prepare parameterized query
      $lock = $lock ? NULL : 'WITH (NOLOCK)';
      $query = "SELECT * FROM $table $lock WHERE $primary = @p1";

      // prepare primary key definition
      $definition = "@p1 $type" . ($length ? "($length)" : NULL);

      // prepare primary key value: we support strings and integers only
      if (in_array($type, $strings))
        $param = sprintf('@p1 = %s', mssql_escape_with_zeroes($value));
      elseif (in_array($type, $integers))
        $param = sprintf('@p1 = %d', $value);
      else
        return $this->setError("unsupported primary key $primary type $type");

      // prepare and execute cached plan via stored procedure
      logInfo("query: $query, parameter: $definition, value: " . cleanse_credit_card_string($param));
      $sql = "exec sp_executesql N'$query', N'$definition', $param";
      $statement = $this->link->query($sql);
      $result = $statement->fetchAll(\PDO::FETCH_OBJ);

      // validate result
      $count = count($result);
      if ($count != 1)
        return $this->setError("$count rows found");

      // success
      $data = $result[0];
    }

    catch(\PDOException $e)
    {
      if ($statement)
        logError('statement error: ' . json_encode($statement->errorInfo()));
      if ($this->link)
        logError('connection error: ' . json_encode($this->link->errorInfo()));
      $this->setError($e->getMessage());
    }

    return $data;
  }


  /**
   * openConnection
   * connect to host and database with environment configured credentials
   * @return Resource connection or NULL on failure
   */
  private function openConnection()
  {
    try
    {
      // use only MSSQL supported attributes: https://msdn.microsoft.com/en-us/library/ff628164%28v=sql.105%29.aspx
      $attributes = array
      (
        \PDO::ATTR_CASE => \PDO::CASE_UPPER,
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
      );
      $username = user();
      $password = pwd();

      if (isMssqlExtensionMissing()) {
        $uri = "sqlsrv:Server={$this->host},{$this->port};Database={$this->database}";
      } else {
        // enable Unicode support
        $uri = "dblib:version=7.0;charset=UTF-8;host={$this->host};port={$this->port};dbname={$this->database}";
      }

      $this->link = new \PDO($uri, $username, $password, $attributes);
      logInfo("using database {$this->database} on host {$this->host} as user $username");
    }

    catch(\PDOException $e)
    {
      $this->setError($e->getMessage());
      unset($this->link);
      $this->link = NULL;
    }

    return $this->link;
  }


  /**
   * setError
   * log and save last error encountered
   * @param String error message
   */
  private function setError($message)
  {
    $this->error = $message;
    return logError($message);
  }

}

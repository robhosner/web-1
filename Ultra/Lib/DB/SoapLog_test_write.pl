#!/usr/bin/perl


use strict;
use warnings;


use Data::Dumper;
use Ultra::Lib::DB::MSSQL;
use Ultra::Lib::DB::SoapLog;
use Ultra::Lib::Util::Config;
use XML::LibXML;
use XML::LibXML::PrettyPrint;
use XML::Simple;


my $config = Ultra::Lib::Util::Config->new();

my $mw_db_host  = $config->find_credential('mw/db/host');

my $db = Ultra::Lib::DB::MSSQL->new(
  DB_NAME => 'ULTRA_ACC',
  DB_HOST => $mw_db_host,
  CONFIG  => $config
);

my $dbSoapLog = Ultra::Lib::DB::SoapLog->new( DB => $db , CONFIG => $config );

my $params = {
  data_xml   => '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://www.sigvalue.com/acc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><ns1:SendSMS><ns1:UserData><ns1:senderId>MVNEACC</ns1:senderId><ns1:channelId>ULTRA</ns1:channelId><ns1:timeStamp>2015-03-17T10:55:10</ns1:timeStamp></ns1:UserData><ns1:qualityOfService>801</ns1:qualityOfService><ns1:preferredLanguage>en</ns1:preferredLanguage><ns1:smsText>Test '.time.'</ns1:smsText><ns1:SubscriberSMSList><ns1:SubscriberSMS><ns1:MSISDN>8323985363</ns1:MSISDN><ns1:Language>en</ns1:Language><ns1:Text xsi:nil="true"/></ns1:SubscriberSMS></ns1:SubscriberSMSList></ns1:SendSMS></SOAP-ENV:Body></SOAP-ENV:Envelope>',
  command    => 'TEST',
  session_id => 'TEST',
  type_id    => 4,
};

my $success = $dbSoapLog->insertSoapLog($params);

if ( $success )
{
  print "Success\n";
}
else
{
  print "Failure\n";
}

__END__

Examples:

mw-dev-01:
%> sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli_acc_dev HTT_CONFIGROOT=/home/ht/config/rgalli_acc_dev Ultra/Lib/DB/SoapLog_test_write.pl'

web-dev-01:
%> sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli3_dev HTT_CONFIGROOT=/home/ht/config/rgalli3_dev Ultra/Lib/DB/SoapLog_test_write.pl'


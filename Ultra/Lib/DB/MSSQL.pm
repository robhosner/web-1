package Ultra::Lib::DB::MSSQL;


use strict;
use warnings;


use Data::Dumper;
use DBD::Sybase;
use DBI;
use JSON::XS;
use Ultra::Lib::Util::Config;


use base qw(Ultra::Lib::Util::Logger Ultra::Lib::Util::Error::Setter);


=head1 NAME

Ultra::Lib::DB::MSSQL

Ultra DB connection to MS SQL

=head1 SYNOPSIS

  my $db = Ultra::Lib::DB::MSSQL->new();

  # ... alternatively ...

  my $config      = Ultra::Lib::Util::Config->new();

  my $db = Ultra::Lib::DB::MSSQL->new(
    DB_NAME => 'ULTRA_ACC',
    DB_HOST => $host,
    CONFIG  => $config
  );

  # connect

  my $dbh = $db->dbh();

  # select

  my $sql = "SELECT GETUTCDATE();";

  my @out = @{$dbh->selectall_arrayref($sql)};


=cut


sub _initialize
{
  my ($this, $data) = @_;

  $data->{ LOG_ENABLED } = 1;
  $data->{ CONFIG      } ||= Ultra::Lib::Util::Config->new();
  $data->{ JSON_CODER  } ||= JSON::XS->new()->relaxed()->utf8()->allow_blessed->convert_blessed->allow_nonref();

  if ( ( exists $data->{ DB_TYPE } ) && $data->{ DB_TYPE } )
  {
    $data->{ DB_TYPE } .= '/';
  }
  else
  {
    $data->{ DB_TYPE } = '';
  }

  $this->SUPER::_initialize($data);
}

sub getDBName
{
  my ($this) = @_;

  if ( $this->{ DB_NAME } ) { return $this->{ DB_NAME }; }

  return $this->_config()->find_credential($this->{ DB_TYPE }.'db/name');
}

sub getDBUser
{
  my ($this) = @_;

  return $this->_config()->find_credential($this->{ DB_TYPE }.'db/user');
}

sub getDBPasswd
{
  my ($this) = @_;

  return $this->_config()->find_credential_from_file($this->{ DB_TYPE }.'db/pfile');
}

sub getDBHost
{
  my ($this) = @_;

  if ( $this->{ DB_HOST } ) { return $this->{ DB_HOST }; }

  return $this->_config()->find_credential($this->{ DB_TYPE }.'db/host');
}

sub _config
{
  my ($this) = @_;

  return $this->{ CONFIG };
}

sub dbh
{
  my ($this) = @_;

  if ( $this->{ DBH } ) { return $this->{ DBH }; }

  my $dbName   = $this->getDBName();
  my $dbUser   = $this->getDBUser();
  my $dbPasswd = $this->getDBPasswd();
  my $dbHost   = $this->getDBHost();

  my $connect_string = "DBI:Sybase:database=".$dbName.";server=".$dbHost;

  $this->log( "connecting to DB $dbName , host $dbHost , user $dbUser" );

  my $dbh = DBI->connect($connect_string,
                         $dbUser,
                         $dbPasswd,
                         {'RaiseError' => 1});

  if ( ! defined $dbh )
  {
    $this->log( __PACKAGE__ . "Couldn't connect to DB $dbName on host $dbHost with user $dbUser : $!" );

    $this->addError("Couldn't connect to DB $dbName on host $dbHost with user $dbUser : $!");

    return;
  }

  #$this->log( __PACKAGE__ . " connection established" );

  return $dbh;
}


sub executeWrite
{
  my ($this, $query, $bindingRef, $read) = @_;

  $read ||= 0;

  $this->log( __PACKAGE__ . ' executeWrite ' . $this->format_log_query($query) . $this->{ JSON_CODER }->encode( $bindingRef ) );

  my $success = 0;
  my $rows    = [];

  if ( defined $bindingRef )
  {
    my $sth;

    local $@;

    eval
    {
      $sth = $this->dbh()->prepare($query);
    };

    if ( $@ )
    {
      $this->log( __PACKAGE__. " Cannot perform query " . $this->format_log_query($query) . " - $@");
      $this->addErrors( __PACKAGE__. " Cannot perform query " . $this->format_log_query($query) . " - $@");
    }
    else
    {
      my $executeSuccess = 0;

      eval
      {
        $executeSuccess = $sth->execute( @$bindingRef );
      };

      if ( $@ )
      {
        $this->log( __PACKAGE__. " Cannot execute query " . $this->format_log_query($query) . " - $@");
        $this->addErrors( __PACKAGE__. " Cannot execute query " . $this->format_log_query($query) . " - $@");
      }

      if ( $executeSuccess )
      {
        if ( $read )
        {
          if ( $sth )
          {
            while( my $row = $sth->fetchrow_hashref() )
            {
              push(@$rows, $row);
            }
          }
          else
          {
            $this->log( __PACKAGE__. " Cannot read result after query " . $this->format_log_query($query) . " - " . $DBI::errstr . " [" . $this->dbh()->err() . "] $@");
            $this->addErrors( __PACKAGE__. " Cannot read result after query " . $this->format_log_query($query) . " - " . $DBI::errstr . " [" . $this->dbh()->err() . "] $@");
          }
        }

        $sth->finish();

        $success = 1;
      }
      else
      {
        $this->log( __PACKAGE__. " Cannot perform query " . $this->format_log_query($query) . " - " . $DBI::errstr . " [" . $this->dbh()->err() . "] $@");
        $this->addErrors( __PACKAGE__. " Cannot perform query " . $this->format_log_query($query) . " - " . $DBI::errstr . " [" . $this->dbh()->err() . "] $@");
      }
    }
  }
  else
  {
    local $@;

    $this->log( __PACKAGE__ . ' executeWrite invoking do' );

    eval
    {
      $success = $this->dbh()->do($query);
    };

    if ( ( ! $success ) || $@ )
    {
      $this->addErrors( __PACKAGE__. " Cannot perform query " . $this->format_log_query($query) . " - " . $DBI::errstr . " [" . $this->dbh()->err() . "] $@");
    }
  }

  return ($success, $rows);
}


sub executeSelect
{
  my ($this, $query, @params) = @_;

  my $sth;

  $this->log( __PACKAGE__ . " executeSelect : " . $this->format_log_query($query) . " [" . join(" , ",@params) . "]" );

  eval
  {
    $sth = $this->dbh()->prepare( $query );

    $sth->execute( @params );
  };

  if ( $@ )
  {
    $this->addErrors( __PACKAGE__. " Cannot perform query " . $this->format_log_query($query) . " - " . $DBI::errstr . " [" . $this->dbh()->err() . "] $@");
  }
  elsif ( ! $sth )
  {
    $this->addErrors( __PACKAGE__. " Cannot perform query " . $this->format_log_query($query) . " - " . $DBI::errstr . " [" . $this->dbh()->err() . "]");
  }

  return $sth;
}


sub readall
{
  my ($this, $query) = @_;

  my $rows = [];

  my $sth = $this->executeSelect( $query );

  if ( $sth )
  {
    while( my $row = $sth->fetchrow_hashref() )
    {
      push(@$rows, $row);
    }
  }
  else
  {
    $this->addErrors( __PACKAGE__. " Cannot perform query " . $this->format_log_query($query) . " - " . $DBI::errstr . " [" . $this->dbh()->err() . "] $@");
  }

  return $rows;
}


=head4 addErrors

  Add to errors array

=cut
sub addErrors
{
  my ($this, $errors) = @_;

  # TODO: use a logger
  print STDERR Dumper($errors);

  return $this->SUPER::addErrors($errors);
}


=head4 format_log_query

  Cleanup string to be logged

=cut
sub format_log_query
{
  my ($this, $query) = @_;

  $query =~ s/[\n\r]/ /g;

  return $query;
}


=head4 writeToRedoLog

  Append data to redo log

=cut
sub writeToRedoLog
{
  my ($this, $redoLogName, $sql, $values) = @_;

  my $fd;

  # open file where we want to append data
  if ( ! open( $fd , ">>".$redoLogName ) )
  {
    $this->log( 'Cannot open file <'.$redoLogName.'> for writing' );

    return 0;
  }

  # append to file
  print $fd
    time .
    "\n" .
    $this->format_log_query( $sql ) .
    "\n" .
    $this->{ JSON_CODER }->encode( $values ) .
    "\n" ;

  # close file
  if ( ! close( $fd ) )
  {
    $this->log( 'Cannot close file <'.$redoLogName.'>' );

    return 0;
  }

  return 1;
}


1;


__END__


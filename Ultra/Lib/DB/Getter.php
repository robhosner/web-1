<?php

/**
 * PHP module to access redis-cached database values
 */

namespace Ultra\Lib\DB\Getter;

require_once ('lib/util-common.php');
require_once ('Ultra/Lib/Util/Redis.php');
require_once ('Ultra/Lib/DB/Schema.php');
require_once ('Ultra/Lib/DB/Customer.php');

define ('TTL_ONE_HOUR', 3600);
define ('TTL_ONE_DAY',  86400);

/**
 * setScalarMVNE
 *
 * MVNE changed for a customer, we have to update the cache
 *
 * @return boolean
 */
function setScalarMVNE( $mvne , $msisdn=NULL , $iccid=NULL , $customer_id=NULL , $redis=NULL )
{
  if ( !$mvne || ( ( $mvne != '1' ) && ( $mvne != '2' ) ) )
    return FALSE;

  if ( !$redis )
    $redis = new \Ultra\Lib\Util\Redis;

  if ( $msisdn )
    $redis->set( 'ultra/getter/MSISDN/'.normalize_msisdn_10($msisdn).'/MVNE' , $mvne , TTL_ONE_HOUR );

  if ( $iccid )
    $redis->set( 'ultra/getter/ICCID/'.luhnenize($iccid).'/MVNE' , $mvne , TTL_ONE_HOUR );

  if ( $customer_id )
    $redis->set( 'ultra/getter/CUSTOMER_ID/'.$customer_id.'/MVNE' , $mvne , TTL_ONE_HOUR );

  return TRUE;
}

/**
 * getScalarConfig
 *
 * return static configuration for getScalar()
 *
 * @param string input: type of input (e.g. 'iccid', 'customer_id', 'msisdn')
 * @return array:
 *   method: array(function => array(table, table...)): functions which return table values
 *   normalize: optional function to normalize input value for redis
 *   length: expected input string length [after normalization]
 *   ttl: cache life in seconds
 * @author: VYT, 14-01-28
 */
function getScalarConfig($input)
{
  $config = array(
    'CUSTOMER_ID' => array(
      'method' => array(
        'get_ultra_customer_from_customer_id' => array('HTT_CUSTOMERS_OVERLAY_ULTRA'),
        'get_ultra_msisdn_from_customer_id'   => array('HTT_ULTRA_MSISDN'),
        'get_account_from_customer_id'        => array('ACCOUNTS')),
      'ttl'    => TTL_ONE_HOUR),

    'ICCID' => array(
      'method' => array('get_htt_inventory_sim_from_iccid' => array('HTT_INVENTORY_SIM')),
      'normalize' => 'luhnenize',
      'length' => 19,
      'ttl'    => TTL_ONE_HOUR),

    'MSISDN' => array(
      'method' => array(
        'get_ultra_msisdn_from_msisdn' => array('HTT_ULTRA_MSISDN'),
        'get_ultra_customer_from_msisdn' => array('HTT_CUSTOMERS_OVERLAY_ULTRA'),
        'get_portin_queue_by_msisdn' => array('PORTIN_QUEUE')),
      'normalize' => 'normalize_msisdn',
      'length' => 10,
      'ttl'    => TTL_ONE_HOUR),

    'ROLE_NAME' => array(
      'method' => array('get_role_by_role_name' => array('DP_RBAC.ROLE')),
      'ttl' => TTL_ONE_HOUR),

    'ULTRA.HTT_ACTIVATION_HISTORY.CUSTOMER_ID' => array(
      'method' => array('get_activation_history_by_customer_id' => array('ULTRA.HTT_ACTIVATION_HISTORY')),
      'ttl'    => TTL_ONE_HOUR),
    
    'DEALERCD' => array(
      'method'  => array('get_dealer_from_code' => array('TBLDEALERSITE')),
      'ttl'     => TTL_ONE_HOUR)

    #,'COUNTRY' => array(
    #  'method' => array('\Ultra\Lib\DB\Customer\getCountryByCustomerId' => array('CUSTOMERS')),
    #  'ttl'    => TTL_ONE_HOUR)
  );

  if (isset($config[$input]))
    return $config[$input];
  else
    return NULL;
}

/**
 * getScalar
 *
 * get single value from cache or DB
 * @param string inputType: DB key field name (e.g. 'ICCID', 'customer_id' as defined in getScalarConfig)
 * @param inputValue: key value (e.g. 1234567890, 2345)
 * @param string outputType: DB field of interest; beware it is case-sensitive (must match DB schema)
 * @param int ttl: optional redis cache life in seconds
 * @param string specific_table: get outputType from a specific given table only;
 *  useful when same outputType occurs in multiple tables (e.g. MVNE, CUSTOMER_ID)
 *   positive: get from cache if available, save to cache otherwise
 *   0 - always get value from DB
 *   negative - get value from DB and remove from cache
 */
function getScalar( $inputType , $inputValue , $outputType , $ttl=NULL , $specific_table=NULL , $redis=NULL )
{
  // check params
  if (empty($inputType) || ! isset($inputValue) || empty($outputType))
    return NULL;
  $inputType = strtoupper($inputType);
  $outputTypeLower = strtolower($outputType);
  $outputTypeUpper = strtoupper($outputType);
  $specific_table = strtoupper($specific_table);

  // get configuration for this type of data
  $config = getScalarConfig($inputType);
  if (! $config)
  {
    dlog('', "ERROR: getScalarConfig does not know input $inputType");
    return NULL;
  }

  // normalize input value if needed
  if (isset($config['normalize']) && is_callable($config['normalize']))
    $inputValue = $config['normalize']($inputValue);
  
  // check input length
  if (isset($config['length']) && strlen($inputValue) != $config['length'])
  {
    dlog('', "ERROR: invalid inputValue length for $inputType");
    return NULL;
  }

  // init redis
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  // if $specific_table is given then ensure we are using the right redis key (i.e. MVNE occurs in multiple tables)
  $redis_key = "ultra/getter/$inputType/$inputValue/$outputTypeUpper" . ($specific_table ? "/$specific_table" : NULL);
  $ttl = $ttl === NULL ? $config['ttl'] : $ttl;

  // try getting from cache
  if ($ttl > 0)
  {
    $result = $redis->get( $redis_key );
    if ($result)
      return $result;
  }

  // determine function name for requested type of output
  $function = NULL;
  foreach ($config['method'] as $method => $schema)
    foreach ($schema as $table)
    {
      // look for DB function only if no specific table is given or the table matches
      if (! $specific_table || $table == $specific_table)
      {
        // get all columns of this table and check if outputType is defined there
        $columns = \Ultra\Lib\DB\getTableSchema($table);
        if (! $columns)
        {
          dlog('', "ERROR: getTableSchema() does not know table $table");
          return NULL;
        }
        if (isset($columns[$outputTypeUpper]))
        {
          $function = $method;
          break 2;
        }
      }
    }

  // verify that function is valid
  if (! $function)
  {
    dlog('', "ERROR: undefined function for output $outputType");
    return NULL;
  }
  if (! is_callable($function))
  {
    dlog('', "ERROR: cannot call function $function");
    return NULL;
  }

  // get data from DB
  $db_result = $function("$inputValue");
  if (! $db_result) // nothing found, nothing to do
    return NULL;

  // evaluate DB result: first try given case then lower and UPPER
  if (is_object($db_result)) // most functions return an object
  {
    if (property_exists($db_result, $outputType)) // user case
      $result = $db_result->$outputType;
    elseif (property_exists($db_result, $outputTypeLower))
      $result = $db_result->$outputTypeLower;
    elseif (property_exists($db_result, $outputTypeUpper))
      $result = $db_result->$outputTypeUpper;
    else
      dlog('', "WARNING: cannot find class property $outputType");
  }
  elseif (is_array($db_result)) // some return arrays
  {
    if (array_key_exists($outputType, $db_result)) // user case
      $result = $db_result[$outputType];
    elseif (array_key_exists($outputTypeLower, $db_result))
      $result = $db_result[$outputTypeLower];
    elseif (array_key_exists($outputTypeUpper, $db_result))
      $result = $db_result[$outputTypeUpper];
    else
      dlog('', "WARNING: cannot find array key $outputType");
  }
  else
    dlog('', 'ERROR: cannot handle result of type ' . gettype($db_result));

  if ( ! $result)
    return NULL;

  // update cache if requested
  if ($ttl > 0)
    $redis->set($redis_key, $result, $ttl);
  elseif ($ttl < 0)
    $redis->del($redis_key);

  return $result;
}

?>

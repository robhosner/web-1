<?php

require_once 'db.php';

teldata_change_db();
$sql = \Ultra\Lib\DB\makeSelectQuery('HTT_CUSTOMERS_OVERLAY_ULTRA', 10, NULL, array('PLAN_STATE' => 'Active'));
$data = mssql_fetch_all_objects(logged_mssql_query($sql));
$good = \Ultra\Lib\DB\normalizeSchema($data);
print_r($good, TRUE);
exit();


// test makeDeleteQuery with bad table
$sql = \Ultra\Lib\DB\makeDeleteQuery('RANDOM_TABLE', array('field' => 1));
echo "SQL: $sql \n";

// test makeDeleteQuery with bad attributes
$sql = \Ultra\Lib\DB\makeDeleteQuery('ULTRA.DEMO_LINE', array('field' => 1));
echo "SQL: $sql \n";

// test makeDeleteQuery without attributes
$sql = \Ultra\Lib\DB\makeDeleteQuery('ULTRA.DEMO_LINE', array());
echo "SQL: $sql \n";

// successfull test makeDeleteQuery with single attribute
$sql = \Ultra\Lib\DB\makeDeleteQuery('ULTRA.DEMO_LINE', array('DEMO_LINE_ID' => 1));
echo "SQL: $sql \n";

// successfull test makeDeleteQuery with multiple attributes
$sql = \Ultra\Lib\DB\makeDeleteQuery('ULTRA.DEMO_LINE', array('DEMO_LINE_ID' => 1, 'DEALER_ID' => 10));
echo "SQL: $sql \n";


// testing NULL paramter $clauses
$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ULTRA.PROMO_BROADCAST_CAMPAIGN',
  NULL,
  array( '*' )
);

print_r(array('sql' => $sql));

$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ULTRA.PROMO_BROADCAST_CAMPAIGN',
  NULL,
  array( '*' ),
  array('EARLIEST_DELIVERY_TIME' => 'lteq_0000')
);

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ULTRA.PROMO_BROADCAST_CAMPAIGN',
  NULL,
  array( '*' ),
  array('EARLIEST_DELIVERY_TIME' => 'gteq_1111')
);

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ULTRA.PROMO_BROADCAST_CAMPAIGN',
  NULL,
  array( '*' ),
  array('EARLIEST_DELIVERY_TIME' => 'lt_2222')
);

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ULTRA.PROMO_BROADCAST_CAMPAIGN',
  NULL,
  array( '*' ),
  array('EARLIEST_DELIVERY_TIME' => 'gt_3333')
);

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ULTRA.PROMO_BROADCAST_CAMPAIGN',
  NULL,
  array( '*' ),
  array('EARLIEST_DELIVERY_TIME' => '1234')
);

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ULTRA.PROMO_BROADCAST_CAMPAIGN',
  NULL,
  array( '*' ),
  array('EARLIEST_DELIVERY_TIME' => array('9999','8888'))
);

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ACCOUNTS',
  NULL,
  array('CUSTOMER_ID'),
  array('CREATION_DATE_TIME' => 'past_7_days')
);

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ACCOUNTS',
  NULL,
  array('CUSTOMER_ID'),
  array('CREATION_DATE_TIME' => 'last_8_hours')
);

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeSelectQuery(
  'ACCOUNTS',
  NULL,
  array('CUSTOMER_ID'),
  array('CREATION_DATE_TIME' => 'last_9_minutes')
);

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeUpdateQuery(
            'ULTRA.PROMO_BROADCAST_LOG',
            array(
              'PROMO_STATUS'       => 'CLAIMED',
              'PROMO_CLAIMED_DATE' => timestamp_to_date( time() , MSSQL_DATE_FORMAT , 'UTC' )
            ),
            array(
              'PROMO_BROADCAST_CAMPAIGN_ID' => 1,
              'CUSTOMER_ID'                 => 31
            )
          );

echo "$sql\n";

$sql = \Ultra\Lib\DB\makeSelectQuery('HTT_CANCELLATION_REASONS', NULL, array( 'CUSTOMER_ID' ), array( 'MSISDN' => '1234512345' ) );

echo "$sql\n";

?>

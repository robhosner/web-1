<?php


// This replaces fields.php

namespace Ultra\Lib\DB\Setter\Customer;

require_once 'Ultra/Lib/DB/Merchants/Tokenizer.php';
require_once 'Ultra/Lib/MeS.php';
require_once 'Ultra/Lib/Util/Validator.php';
include_once("Validate.php");
include_once("Validate/Finance/CreditCard.php");


/**
 * disable_cc_info
 *
 * Disable the customer's credit card
 * Input:
 *  - 'customer_id'
 *
 * @return Result object
 */
function disable_cc_info( $params )
{
  dlog('', "(%s)", func_get_args());

  // disable ULTRA.CC_HOLDER_TOKENS rows
  $success = \disable_ultra_cc_holder_tokens( $params['customer_id'] );

  if ( ! $success )
    return \make_error_Result( 'DB error while updating ULTRA.CC_HOLDER_TOKENS' );

  // disable ULTRA.CC_HOLDERS rows
  $success = \disable_ultra_cc_holders( $params );

  if ( ! $success )
    return \make_error_Result( 'DB error while updating ULTRA.CC_HOLDERS' );

  // wipe out CUSTOMERS.CC_* fields
  $success = \reset_credit_card_info( $params['customer_id'] );

  if ( ! $success )
    return \make_error_Result( 'DB error while updating CUSTOMERS' );

  return \make_ok_Result();
}

/**
 * ultra_info
 *
 * Updates a customer's info in the DB table htt_customers_overlay_ultra
 * Input:
 *  - 'customer_id'
 *  - 'notes'
 *  - 'current_mobile_number'
 *  - 'current_iccid'
 *  - 'activation_iccid'
 *  - 'easypay_activated'
 *  - 'stored_value'
 *  - 'plan_state'
 *  - 'preferred_language'
 *  - 'monthly_cc_renewal'
 *  - 'plan_started' (not yet validated)
 *  - 'plan_expires' (not yet validated)
 *  - 'tos_accepted' (not yet validated)
 *  - 'customer_source' (validated, not stored yet)
 *
 * @return Result object
 */
function ultra_info( $params )
{
  dlog('', "(%s)", func_get_args());

  list( $errors , $error_codes ) = validate_ultra_info( $params );

  if ( count( $errors ) )
  {
    $result = \make_error_Result( $errors );

    $result->data_array['error_codes'] = $error_codes;

    return $result;
  }

  if ( isset($params['current_iccid']) )
    $params['iccid_current'] = $params['current_iccid'];

  if ( isset($params['activation_iccid']) )
    $params['iccid_activated'] = $params['activation_iccid'];

  $sql = htt_customers_overlay_ultra_update_query( $params );

  if ( ! is_mssql_successful(logged_mssql_query($sql)) )
    return \make_error_Result( 'DB error while updating customer info' );

  return \make_ok_Result();
}

/**
 * cc_info
 *
 * Updates a customer's CC info in the DB
 * Input:
 *  - 'customer_id'
 *  - 'cc_name'
 *  - 'cc_address1'
 *  - 'cc_address2'
 *  - 'cc_city'
 *  - 'cc_country'
 *  - 'cc_state_region'
 *  - 'cc_postal_code'
 *  - 'cc_number'
 *  - 'e_mail'
 *  - 'cvv'
 *  - 'expires_date'
 *  - 'state_region'
 *  - 'postal_code'
 *  - 'country'
 *  - 'city'
 *  - 'address1'
 *  - 'address2'
 *
 * @return Result object
 */
function cc_info( $params , $last_name='' )
{
  dlog('', "(%s)", func_get_args());

  if ( isset($params['last_name']) )
    $last_name = $params['last_name'];

  list( $errors , $error_codes , $cvv_validation , $avs_validation , $userError) = validate_cc_info( $params , $last_name );

  if ( count( $errors ) )
  {
    $result = \make_error_Result( $errors );

    $result->data_array['error_codes'] = $error_codes;
    $result->data_array['user_errors'] = $userError;

    return $result;
  }

  // tokenize and save 'cc_number'
  if ( isset($params['cc_number']) && ( $params['cc_number'] != '' ) && ! is_null($params['cc_number']) || !empty($params['token']))
  {
    if ( ! \Ultra\UltraConfig\cc_transactions_enabled() )
    {
      $result = \make_error_Result( 'Credit Card transactions have been disabled temporarily' );

      $result->data_array['error_codes'] = array( 'CC0004' );

      return $result;
    }

    $customer = get_ultra_customer_from_customer_id($params['customer_id'], array('BRAND_ID'));
    if ( ! $customer)
    {
      $result = \make_error_Result( 'The customer does not exist' );

      $result->data_array['error_codes'] = array( 'VV0031' );

      return $result;
    }

    $tokenizer = new \Ultra\Lib\DB\Merchants\Tokenizer();

    $last_name = ( ( isset($params['cc_name']) && $params['cc_name'] ) ? $params['cc_name'] : $last_name );

    $result = $tokenizer->runTokenization(
      array(
        'customer_id'             => $params['customer_id'],
        'brand_id'                => $customer->BRAND_ID,
        'expires_date'            => $params['expires_date'],
        'cc_number'               => $params['cc_number'],
        'cvv'                     => $params['cvv'],
        'last_name'               => $last_name,
        'cvv_validation'          => $cvv_validation,
        'avs_validation'          => $avs_validation,
        'client_reference_number' => \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID),
        'address'                 => !empty($params['cc_address1']) ? $params['cc_address1'] : null,
        'zipcode'                 => !empty($params['cc_postal_code']) ? $params['cc_postal_code'] : null,
        'cc_city'                 => !empty($params['cc_city']) ? $params['cc_city'] : null,
        'cc_state_region'         => !empty($params['cc_state_region']) ? $params['cc_state_region'] : null,
        'token'                   => !empty($params['token']) ? $params['token'] : null,
        'processor_name'          => !empty($params['processor']) ? $params['processor'] : null
      )
    );

    dlog('',"result = %s",$result);

    $result->data_array['error_codes'] = array();

    if ( $result->is_failure() )
    {
      $result->data_array['error_codes'] = array( 'DB0001' );

      return $result;
    }
  }

  $sql = \customers_update_query( $params );

  if ( ! is_mssql_successful(logged_mssql_query($sql)) )
    return \make_error_Result( 'DB error while updating customer info' );

  return \make_ok_Result();
}

/**
 * validate_ultra_info
 *
 * @return array
 */
function validate_ultra_info( $params )
{
  $errors      = array();
  $error_codes = array();

  if ( !isset($params['customer_id'])    || !$params['customer_id'] )
    list( $error , $error_code ) = array('customer_id is a required parameter','------');
  else
    list( $error , $error_code ) = \Ultra\Lib\Util\validateTypeInteger('customer_id',     $params['customer_id']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['notes'])                 && $params['notes'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorStringformat( 'notes', $params['notes'] , 'alphanumeric_spaces_punctuation_octothorpe' );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['current_mobile_number']) && $params['current_mobile_number'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorMsisdn( 'current_mobile_number', $params['current_mobile_number'] );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['current_iccid'])         && $params['current_iccid'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccidFormat( 'current_iccid', luhnenize( $params['current_iccid'] ) );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['easypay_activated'])     && $params['easypay_activated'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validateTypeBoolean( 'easypay_activated', $params['easypay_activated'] );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['stored_value'])          && $params['stored_value'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validateTypeReal( 'stored_value', $params['stored_value'] );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['plan_state'])            && $params['plan_state'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorMatchesPlanState( 'plan_state', $params['plan_state'] );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['preferred_language'])    && $params['preferred_language'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorMatchesLanguage( 'preferred_language', $params['preferred_language'] );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  #if ( isset($params['plan_started'])          && $params['plan_started'] )

  #if ( isset($params['plan_expires'])          && $params['plan_expires'] )

  #if ( isset($params['tos_accepted'])          && $params['tos_accepted'] )

  if ( isset($params['monthly_cc_renewal'])    && $params['monthly_cc_renewal'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validateTypeBoolean( 'monthly_cc_renewal', $params['monthly_cc_renewal'] );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['activation_iccid'])      && $params['activation_iccid'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorIccidFormat( 'activation_iccid', luhnenize( $params['activation_iccid'] ) );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['customer_source'])       && $params['customer_source'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorStringformat( 'customer_source', $params['customer_source'] , 'alphanumeric_spaces_punctuation_octothorpe' );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  dlog('',"errors = %s",$errors);

  return array( $errors , $error_codes );
}

/**
 * validate_cc_info
 *
 * @return array
 */
function validate_cc_info( $params , $last_name='' )
{
  dlog('',"last_name = $last_name");

  $errors      = array();
  $error_codes = array();

  $error = NULL;

  $cvv_validation = NULL;
  $avs_validation = NULL;

  $userError = null;

  if ( !isset($params['customer_id'])    || !$params['customer_id'] )
    list( $error , $error_code ) = array('customer_id is a required parameter','------');
  else
    list( $error , $error_code ) = \Ultra\Lib\Util\validateTypeInteger('customer_id',     $params['customer_id']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['local_phone'])     && $params['local_phone'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorRegexp(        'local_phone', $params['local_phone'] , '^[\s\d\.\-\(\)\+]+$' );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['e_mail'])          && $params['e_mail'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorStringformat(       'e_mail', $params['e_mail'] , 'email' );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['state_region'])    && $params['state_region'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorStringformat( 'state_region', $params['state_region'] , 'alphanumeric_punctuation' );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['postal_code'])     && $params['postal_code'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode(  'postal_code',      $params['postal_code'], 'US');

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['country'])         && $params['country'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorCountry(       'country',     $params['country']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['city'])            && $params['city'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorCity(             'city',     $params['city']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['address1'])        && $params['address1'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorAddress(      'address1',     $params['address1']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['address2'])        && $params['address2'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorAddress(      'address2',     $params['address2']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['cc_name'])         && $params['cc_name'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorName(          'cc_name',     $params['cc_name']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['cc_address1'])     && $params['cc_address1'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorAddress(   'cc_address1',     $params['cc_address1']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['cc_address2'])     && $params['cc_address2'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorAddress(   'cc_address2',     $params['cc_address2']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['cc_city'])         && $params['cc_city'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorCity(      'cc_city',         $params['cc_city']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['cc_country'])      && $params['cc_country'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorCountry(   'cc_country',      $params['cc_country']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['cc_state_region']) && $params['cc_state_region'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorStringformat( 'cc_state_region', $params['cc_state_region'] , 'alphanumeric_punctuation' );

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['cc_postal_code'])  && $params['cc_postal_code'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorZipcode(   'cc_postal_code',  $params['cc_postal_code'], 'US');

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['cc_number']) && $params['cc_number'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorCC(        'cc_number',       $params['cc_number']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['cvv'])             && $params['cvv'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorCVV(       'cvv',             $params['cvv']);

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( isset($params['expires_date'])    && $params['expires_date'] )
    list( $error , $error_code ) = \Ultra\Lib\Util\validatorDateformat('expires_date',    $params['expires_date'], 'mmyy');

  if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }

  if ( !count($errors) && isset($params['cc_number']) && $params['cc_number'] )
  {
    // some parameters should be provided together with 'cc_number'
    if ( !isset($params['cvv'])
      || !isset($params['expires_date'])
      || !isset($params['cc_postal_code'])
      || !$params['cvv']
      || !$params['expires_date']
      || !$params['cc_postal_code']
    )
      list( $error , $error_code ) = array('ERR_API_INVALID_ARGUMENTS: Missing credit card parameters','VV0037');
    else
    {
      if ( ! \Ultra\UltraConfig\cc_transactions_enabled() )
      {
        $error = 'Credit Card transactions have been disabled temporarily';
        $error_code = 'CC0004';
      }
      else
      {
        $cc_name = ( isset($params['cc_name']) && $params['cc_name'] ) ? $params['cc_name'] : $last_name ;

        dlog('',"cc_name = $cc_name");

      /* API-202 : previous cc validation logic, moved to runTokenization
        // check for CC duplicates only if there are no other errors
        list( $error , $error_code ) =
          \Ultra\Lib\Util\validatorCCDuplicate( 'cc_number' , $params['cc_number'] , $params['customer_id'] , $params['cvv'] , $params['expires_date'] , $cc_name );
      */

        if ( ! isset($params['cc_address1']) )
          $params['cc_address1'] = '';

        if ( ! $error )
          // perform a ``verify`` call to the payment gateway
          list( $error , $error_code , $cvv_validation , $avs_validation , $userError) = validate_cc_from_gateway( $params , $cc_name );
      }
    }

    if ( $error ) { $errors[] = $error; $error_codes[] = $error_code; $error = NULL; }
  }

  dlog('',"errors = %s",$errors);

  return array( $errors , $error_codes , $cvv_validation , $avs_validation , $userError);
}

/**
 * validate_cc_from_gateway
 *
 * Perform a ``verify`` call to the primary gateway.
 * Validation by itself won't affect DB rows.
 *
 * @return array
 */
function validate_cc_from_gateway( $params , $last_name='' )
{
  dlog('',"last_name = $last_name");

  $error = '';
  $error_code = '';

  $cvv_validation = NULL;
  $avs_validation = NULL;

  $userError = null;

  $address = $params['cc_address1'];
  if ( isset($params['cc_address2']) )
    $address .= ' ' . $params['cc_address2'];

  #$last_name = '';
  if ( isset($params['cc_name']) && $params['cc_name'] )
    $last_name = $params['cc_name'];

  // see API-380
  $customer = get_ultra_customer_from_customer_id($params['customer_id'], array('BRAND_ID'));
  if ( ! $customer)
  {
    $error = 'ERR_API_INTERNAL: The customer does not exist';
    return array($error, 'VV0031', 0, 0, $error);
  }

  $tokenizer = new \Ultra\Lib\DB\Merchants\Tokenizer();

  $result = $tokenizer->runVerify(
    array(
      'customer_id'             => $params['customer_id'],
      'address'                 => $address,
      'zipcode'                 => $params['cc_postal_code'],
      'cc_number'               => $params['cc_number'],
      'expires_date'            => $params['expires_date'],
      'cvv'                     => $params['cvv'],
      'last_name'               => $last_name,
      'client_reference_number' => \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID)
    )
  );

  if ( $result->is_failure() )
  {
    $errors = $result->get_errors();

    if (isset($errors['user_errors']))
    {
      $errors = $errors['user_errors'];
      $error = $errors['EN'];
    }
    else
    {
      $error = implode(',', $errors);
    }

    $error = 'ERR_API_INTERNAL: Credit Card Gateway Validation Failed - (' . $error . ')';
    $error_code = 'CC0001';
    $userError = $errors;

    dlog('', "error = %s", $error);
  }
  else
  {
    dlog('','Credit Card Gateway Validation Succeeded');

    $cvv_validation = $result->data_array['cvv_validation'];
    $avs_validation = $result->data_array['avs_validation'];
  }

  dlog('','%s',$result->data_array);

  return array( $error , $error_code , $cvv_validation , $avs_validation , $userError);
}

?>

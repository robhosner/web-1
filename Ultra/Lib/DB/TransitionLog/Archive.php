<?php

namespace Ultra\Lib\DB\TransitionLog\Archive;

/**
 * un_archive
 *
 * HTT_ACTION_PARAMETER_ARCHIVE --> HTT_ACTION_PARAMETER_LOG
 *           HTT_ACTION_ARCHIVE --> HTT_ACTION_LOG
 *       HTT_TRANSITION_ARCHIVE --> HTT_TRANSITION_LOG
 *
 * @return string
 */
function un_archive( $transition_uuid )
{
  dlog('',"transition_uuid = $transition_uuid");

  $query_result = mssql_fetch_all_rows(
    logged_mssql_query(
      \Ultra\Lib\DB\makeSelectQuery('HTT_TRANSITION_LOG', NULL, array( 'TRANSITION_UUID' ), array( 'TRANSITION_UUID' => $transition_uuid ))
    )
  );

  if ( $query_result && is_array($query_result) && count($query_result) )
    return "TRANSITION_UUID $transition_uuid already in HTT_TRANSITION_LOG";

  $error = log_before_un_archive($transition_uuid);

  if ( $error )
    return $error;

  return perform_un_archive($transition_uuid);
}

/**
 * log_before_un_archive
 *
 * We log all the content
 *
 * @return string
 */
function log_before_un_archive($transition_uuid)
{
  $query_result = mssql_fetch_all_rows(
    logged_mssql_query(
      \Ultra\Lib\DB\makeSelectQuery('HTT_TRANSITION_ARCHIVE', NULL, array( '*' ), array( 'TRANSITION_UUID' => $transition_uuid ))
    )
  );

  dlog('',"HTT_TRANSITION_ARCHIVE dump :\n%s",$query_result);

  $query_result = mssql_fetch_all_objects(
    logged_mssql_query(
      \Ultra\Lib\DB\makeSelectQuery('HTT_ACTION_ARCHIVE', NULL, array( '*' ), array( 'TRANSITION_UUID' => $transition_uuid ))
    )
  );

  if ( $query_result && is_array($query_result) && count($query_result) )
  {
    dlog('',"HTT_ACTION_ARCHIVE dump :\n%s",$query_result);

    foreach($query_result as $n => $action)
    {
      $query_result = mssql_fetch_all_objects(
        logged_mssql_query(
          \Ultra\Lib\DB\makeSelectQuery('HTT_ACTION_PARAMETER_ARCHIVE', NULL, array( '*' ), array( 'ACTION_UUID' => $action->ACTION_UUID ))
        )
      );

      dlog('',"HTT_ACTION_PARAMETER_ARCHIVE dump :\n%s",$query_result);
    }
  }
  else
    return "No actions found for TRANSITION_UUID $transition_uuid";

  return NULL;
}

/**
 * perform_un_archive
 *
 * Proceed with the migration
 *
 * @return string
 */
function perform_un_archive($transition_uuid)
{
  $query_result = mssql_fetch_all_rows(
    logged_mssql_query(
      \Ultra\Lib\DB\makeSelectQuery('HTT_ACTION_ARCHIVE', NULL, array( 'ACTION_UUID' ), array( 'TRANSITION_UUID' => $transition_uuid ))
    )
  );

  $sql_array = array();

  if ( $query_result && is_array($query_result) && count($query_result) )
  {
    $sql_array[] = copy_transition($transition_uuid);
    $sql_array[] = copy_actions($transition_uuid);

    foreach($query_result as $n => $action)
    {
      $sql_array[] = copy_archive_action_parameter($action[0]);
      $sql_array[] = rm_archive_action_parameter($action[0]);
    }

    $sql_array[] = rm_actions($transition_uuid);
    $sql_array[] = rm_transition($transition_uuid);

    $result = exec_queries_in_transaction( $sql_array );

    if ( ! $result['success'] )
      return $result['errors'][0];
  }
  else
    return "No actions found for TRANSITION_UUID $transition_uuid";

  return NULL;
}

/**
 * copy_transition
 *
 * Query
 *
 * @return string
 */
function copy_transition($transition_uuid)
{
  return sprintf(
    "INSERT INTO HTT_TRANSITION_LOG
     (CUSTOMER_ID, TRANSITION_UUID, STATUS, CREATED, CLOSED, FROM_COS_ID, FROM_PLAN_STATE, TO_COS_ID, TO_PLAN_STATE, CONTEXT, HTT_ENVIRONMENT, TRANSITION_LABEL)
     SELECT
     CUSTOMER_ID, TRANSITION_UUID, STATUS, CREATED, CLOSED, FROM_COS_ID, FROM_PLAN_STATE, TO_COS_ID, TO_PLAN_STATE, CONTEXT, HTT_ENVIRONMENT, TRANSITION_LABEL
     FROM HTT_TRANSITION_ARCHIVE
     WHERE TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );
}

/**
 * copy_actions
 *
 * Query
 *
 * @return string
 */
function copy_actions($transition_uuid)
{
  return sprintf(
    "INSERT INTO HTT_ACTION_LOG
     (ACTION_UUID, TRANSITION_UUID, STATUS, CREATED, PENDING_SINCE, CLOSED, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, ACTION_RESULT, ACTION_SQL)
     SELECT ACTION_UUID, TRANSITION_UUID, STATUS, CREATED, PENDING_SINCE, CLOSED, ACTION_SEQ, ACTION_TYPE, ACTION_NAME, ACTION_RESULT, ACTION_SQL
     FROM HTT_ACTION_ARCHIVE
     WHERE TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );
}

/**
 * rm_actions
 *
 * Query
 *
 * @return string
 */
function rm_actions($transition_uuid)
{
  return sprintf(
    "DELETE FROM HTT_ACTION_ARCHIVE WHERE TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );
}

/**
 * rm_transition
 *
 * Query
 *
 * @return string
 */
function rm_transition($transition_uuid)
{
  return sprintf(
    "DELETE FROM HTT_TRANSITION_ARCHIVE WHERE TRANSITION_UUID = %s",
    mssql_escape_with_zeroes($transition_uuid)
  );
}

/**
 * un_archive_action_parameter
 *
 * Query
 *
 * @return string
 */
function un_archive_action_parameter($action_uuid)
{
  return sprintf(
    "DELETE HTT_ACTION_PARAMETER_ARCHIVE
     OUTPUT
     DELETED.ACTION_UUID, DELETED.PARAM, DELETED.VAL
     INTO HTT_ACTION_PARAMETER_LOG
     WHERE ACTION_UUID = %s",
    mssql_escape_with_zeroes($action_uuid)
  );
}

/**
 * un_archive_action
 *
 * Query
 *
 * @return string
 */
function un_archive_action($action_uuid)
{
  return sprintf(
    "DELETE HTT_ACTION_ARCHIVE
     OUTPUT
     DELETED.ACTION_UUID, DELETED.TRANSITION_UUID, DELETED.STATUS, DELETED.CREATED, DELETED.PENDING_SINCE, DELETED.CLOSED, DELETED.ACTION_SEQ, DELETED.ACTION_TYPE, DELETED.ACTION_NAME, DELETED.ACTION_RESULT, DELETED.ACTION_SQL
     INTO HTT_ACTION_LOG
     WHERE ACTION_UUID = %s",
    mssql_escape_with_zeroes($action_uuid)
  );
}

/**
 * rm_archive_action_parameter
 *
 * Query
 *
 * @return string
 */
function rm_archive_action_parameter($action_uuid)
{
  return sprintf(
    "DELETE FROM HTT_ACTION_PARAMETER_ARCHIVE WHERE ACTION_UUID = %s",
    mssql_escape_with_zeroes($action_uuid)
  );
}

/**
 * copy_archive_action_parameter
 *
 * Query
 *
 * @return string
 */
function copy_archive_action_parameter($action_uuid)
{
  return sprintf(
    "INSERT INTO HTT_ACTION_PARAMETER_LOG
     (ACTION_UUID, PARAM, VAL)
     SELECT ACTION_UUID, PARAM, VAL
     FROM HTT_ACTION_PARAMETER_ARCHIVE
     WHERE ACTION_UUID = %s",
    mssql_escape_with_zeroes($action_uuid)
  );
}

?>

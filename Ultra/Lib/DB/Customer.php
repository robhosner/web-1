<?php

namespace Ultra\Lib\DB\Customer;

// customer data cache
define ('CUSTOMER_CACHE_KEY', 'ultra/customer/'); // this is a hash dataset, not a simple string

/**
 * updatePlanDates
 *
 * Updates PLAN_STARTED and PLAN_EXPIRED fields for a HTT_CUSTOMERS_OVERLAY_ULTRA row
 *
 * @return boolean|array
 */
function updatePlanDates( $customer_id, $plan_duration )
{
  if ( !ctype_digit( (string)$customer_id ) )
  {
    \logError( 'customer_id is not an integer in updatePlanDates call' );
    return false;
  }
  if ( !ctype_digit( (string)$plan_duration ) )
  {
    \logError( 'plan_duration is not an integer in updatePlanDates call' );
    return false;
  }

  $params = array(
    array(
      'param_name' => '@CUSTOMER_ID',
      'variable'   => $customer_id,
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 255,
    ),
    array(
      'param_name' => '@PLAN_DURATION',
      'variable'   => $plan_duration,
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 255,
    )
  );

  return \Ultra\Lib\DB\run_stored_procedure('[PRIMO].[UpdatePlanDate]', $params);
}

/**
 * getDealersForCustomer
 *
 * Executes stored procedure [ultra].[Get_Dealers_For_Customer]
 *
 * @return array
 */
function getDealersForCustomer($customer_id)
{  
  $params = array(
    array(
      'param_name' => '@CustomerId',
      'variable'   => $customer_id,
      'type'       => SQLINT4,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 255,
    ),
  );

  return \Ultra\Lib\DB\run_stored_procedure('[ultra].[Get_Dealers_For_Customer]', $params);
}

/**
 * getCountryByCustomerId
 *
 * @param integer $customer_id
 * @return string
 */
function getCountryByCustomerId( $customer_id )
{
  return \Ultra\Lib\DB\fetch_first_value(
    sprintf(
      "SELECT COUNTRY FROM CUSTOMERS WHERE CUSTOMER_ID = %d",
      $customer_id
    )
  );
}

/**
 * setWholesalePlan
 *
 * Record TMO wholesale plan in ULTRA.WHOLEALE_PLAN_TRACKER and subscriber redis cache hash
 * @param Integer customer ID
 * @param String wholesale plan name
 *  - PRIMARY_WHOLESALE_PLAN
 *  - SECONDARY_WHOLESALE_PLAN
 *  - UV_PRIMARY_WHOLESALE_PLAN
 *  - UV_SECONDARY_WHOLESALE_PLAN
 *  - MINT_WHOLESALE_PLAN
 * @author: VYT, 2015-0416
 * @return boolean
 */
function setWholesalePlan($customerId, $wholesalePlanName, $redis = NULL)
{
  // check parameters
  if ( empty($customerId)
    || empty($wholesalePlanName)
  )
    return \logError('ERROR: invalid parameters '.json_encode(func_get_args()));

  $wholesalePlanId = \Ultra\UltraConfig\getwholesalePlanIdFromName($wholesalePlanName);

  if ( empty($wholesalePlanId) )
    return \logError('ERROR: invalid wholesalePlanName : '.$wholesalePlanName);

  // prepare ULTRA.WHOLESALE_PLAN_TRACKER statement and execute it
  if ( ! $statement = \Ultra\Lib\DB\makeInsertQuery('ULTRA.WHOLESALE_PLAN_TRACKER', array('CUSTOMER_ID' => $customerId, 'WHOLESALE_PLAN_ID' => $wholesalePlanId, 'LAST_UPDATED_DATE' => 'NOW')))
    return \logError('ERROR: failed to generate SQL statement');

  if ( ! logged_mssql_query($statement) )
    return \logError('ERROR: failed statement $statement');

  // cache in hash for subsequent use
  if ( ! $redis && ! $redis = new \Ultra\Lib\Util\Redis )
    return \logError('ERROR: failed to connect to redis');

  $hashKey   = CUSTOMER_CACHE_KEY . $customerId;
  $hashField = 'wholesale_plan_name';
  $redis->hset($hashKey, $hashField, $wholesalePlanName);

  return TRUE;
}

/**
 * getWholesalePlan
 *
 * return subscriber's TMO wholesale plan from hash cache or ULTRA.WHOLESALE_PLAN_TRACKER or UltraConfig
 * will fail on Neutral customers with COS_ID = STANDBY, getUltraPlanConfiguration() shold be used when activating
 * @param Integer customer ID
 * @returns String wholesale plan or NULL on error
 * @author: VYT, 2015-0416
 */
function getWholesalePlan($customerId, $redis = NULL)
{
  // check parameters
  if (empty($customerId))
  {
    dlog('', 'ERROR: invalid parameters %s', func_get_args());
    return NULL;
  }

  // try to get from hash cache
  $wholesalePlanName = NULL;
  if ( ! $redis && ! $redis = new \Ultra\Lib\Util\Redis)
    dlog('', 'ERROR: failed to connect to redis');
  else
  {
    $hashKey = CUSTOMER_CACHE_KEY . $customerId;
    $hashField = 'wholesale_plan_name';
    $wholesalePlanName = $redis->hget(CUSTOMER_CACHE_KEY . $customerId, $hashField);
  }

  // if not found, try to get from DB
  if ( ! $wholesalePlanName)
  {
    // prepare ULTRA.WHOLESALE_PLAN_TRACKER statement and execute it
    if ( ! $statement = \Ultra\Lib\DB\makeSelectQuery('ULTRA.WHOLESALE_PLAN_TRACKER', 1, 'WHOLESALE_PLAN_ID', array('CUSTOMER_ID' => $customerId), NULL, 'LAST_UPDATED_DATE DESC', TRUE))
      dlog('', 'ERROR: failed to generate SQL statement');
    elseif ( ! $result = mssql_fetch_all_rows(logged_mssql_query($statement)))
      dlog('', "WARNING: no results");
    elseif ( ! empty($result[0][0]))
      $wholesalePlanName = \Ultra\UltraConfig\getWholesalePlanNameFromId( $result[0][0] );

    // cache for subsequent use
    if ($wholesalePlanName && $redis)
      $redis->hset($hashKey, $hashField, $wholesalePlanName);

    // if all failed, determine *expected* wholesale plan from COS_ID but do not cache it
    if ( ! $wholesalePlanName)
    {
      dlog('', 'WARNING: attempting to determine wholesale plan from COS_ID');
      if ( ! $cos_id = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $customerId, 'COS_ID', NULL, 'ACCOUNTS', $redis))
        dlog('', 'WARNING: missing COS_ID for customer %d', $customerId);
      else if ( ! $ultraPlanName = get_plan_name_from_short_name(get_plan_from_cos_id($cos_id)))
        dlog('', 'WARNING: failed to get plan name from COS_ID %d', $cos_id);
      else if ( ! $ultraPlanConfig = \Ultra\UltraConfig\getUltraPlanConfiguration($ultraPlanName))
        dlog('', 'WARNING: failed to get %s plan configuration', $ultraPlanName);
      elseif (empty($ultraPlanConfig['wholesale']))
        dlog('', 'WARNING: missing wholesale value for plan %s', $ultraPlanName);
      else
        $wholesalePlanName = $ultraPlanConfig['wholesale'];
    }
  }

  return $wholesalePlanName;
}

/**
 * updateAutoRecharge
 *
 * Update the monthly_cc_renewal column in htt_customers_overlay_ultra table.
 *
 * @param $customerId
 * @param $enroll_auto_recharge
 * @throws \Exception
 */
function updateAutoRecharge($customerId, $enroll_auto_recharge)
{
  $sql = htt_customers_overlay_ultra_update_query([
    'customer_id'        => $customerId,
    'monthly_cc_renewal' => $enroll_auto_recharge
  ]);

  if (!is_mssql_successful(logged_mssql_query($sql)))
    $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB0001');
}


#!/usr/bin/perl


use strict;
use warnings;


use Data::Dumper;
use Redis;
use Ultra::Lib::DB::MSSQL;
use Ultra::Lib::DB::ImeiHistory;

#my $config      = Ultra::Lib::Util::Config->new();
#print $config->find_credential('mw/db/host') . "\n";
#print $config->find_credential('mw/db/port') . "\n";
#print $config->find_credential('mw/db/name') . "\n";
#print $config->find_credential('mw/db/user') . "\n";
#print $config->find_credential('mw/db/pfile') . "\n";

my $db = Ultra::Lib::DB::MSSQL->new( DB_NAME => 'ULTRA_ACC' , DB_TYPE => 'mw' );
#my $db = Ultra::Lib::DB::MSSQL->new( DB_NAME => 'ULTRA_ACC' );

print Dumper( $db )."\n";

my $redis_params =
{
  server    => $db->{ CONFIG }->find_credential('redis/host'),
  reconnect => 0,
  name      => 'Ultra::Lib::DB::ImeiHistory_test-'.$$,
};

my $redis = Redis->new( %$redis_params );

$redis->auth( $db->{ CONFIG }->find_credential('redis/password') );

my $dbImeiHistory = Ultra::Lib::DB::ImeiHistory->new(
  DB           => $db,
  REDIS_OBJECT => $redis,
);

my $success = $dbImeiHistory->insertImeiHistory({
  imei   => '1234567890123458',
  msisdn => '1001001000',
  iccid  => '1234567890123456789',
});

#my $success = $dbImeiHistory->addImeiHistory({
#  imei   => '1234567890123459',
#  msisdn => '1001001000',
#  iccid  => '1234567890123456789',
#});

__END__

sudo su apache -s /bin/bash -c '/bin/env HTT_ENV=rgalli3_dev HTT_CONFIGROOT=/home/ht/config/rgalli3_dev ./Ultra/Lib/DB/ImeiHistory_test.pl'

select * from ULTRA_ACC..IMEI_HISTORY;


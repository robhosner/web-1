<?php
namespace Ultra\Lib\DB\ActionLog;

/**
 * selectActionLog
 *
 * Executes stored procedure [ULTRA].[ACTION_LOG_LOOKUP]
 *
 * @return array
 */
function selectActionLog($transition_uuid, $status)
{  
  $params = array(
    array(
      'param_name' => '@TRANSITION_UUID',
      'variable'   => $transition_uuid,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 128,
    ),
    array(
      'param_name' => '@STATUS',
      'variable'   => $status,
      'type'       => SQLVARCHAR,
      'is_output'  => false,
      'is_null'    => false,
      'max_length' => 128,
    ),
  );
  return \Ultra\Lib\DB\run_stored_procedure('[ULTRA].[ACTION_LOG_LOOKUP]', $params);
}
<?php

require_once 'db.php';
require_once 'Ultra/Lib/DB/MongoDB/Base.php';

$b = new \Ultra\Lib\DB\MongoDB\Base();

print_r( $b );

echo "db0\n";
$db0 = $b->getDB();
print_r( $db0 );

echo "db1\n";
$db1 = $b->getDB();
print_r( $db1 );

echo "db2\n";
$db2 = $b->getDB('test');
print_r( $db2 );

echo "db3\n";
$db3 = $b->getDB('Events');
print_r( $db3 );

$collection = $db3->Testing;

print_r( $collection );

$cursor = $collection->find( array() );

$c = 0;
// iterate cursor to display title of documents
foreach ($cursor as $document)
{
  $c++;
  print_r($document);
}
echo "$c documents found\n";


<?php

namespace Ultra\Lib\DB\MongoDB\Schema;

/**
 * schema
 *
 * MongoDB Events collection definition
 * http://wiki.hometowntelecom.com:8090/display/SPEC/Event+Tracker+Architecture
 *
 * @return array
 */
function Events()
{
  return array(
    'Events' => array(
      'customer_id'       => array( MDB_EVENT_CUSTOMER_ID,        'int'         ),
      'timestamp'         => array( MDB_EVENT_TIMESTAMP,          'timestamp'   ), // mandatory
      'category'          => array( MDB_EVENT_CATEGORY,           'varchar'     ),
      'type'              => array( MDB_EVENT_TYPE,               'int'         ),
      'object'            => array( MDB_EVENT_OBJECT,             'varchar'     ), // mandatory
      'class'             => array( MDB_EVENT_CLASS,              'varchar'     ),
      'status'            => array( MDB_EVENT_STATUS,             'int'         ),
      'source'            => array( MDB_EVENT_SOURCE,             'varchar'     ), // mandatory
      'description'       => array( MDB_EVENT_DESCRIPTION,        'varchar'     ),
      'application'       => array( MDB_EVENT_APPLICATION,        'varchar'     ), // mandatory
      'priority'          => array( MDB_EVENT_PRIORITY,           'int'         ),
      'ip_address'        => array( MDB_EVENT_IP,                 'varchar'     ), // mandatory
      'name'              => array( MDB_EVENT_NAME,               'varchar'     ),
      'msisdn'            => array( MDB_EVENT_MSISDN,             'varchar'     )
    )
  );
}


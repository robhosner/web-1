<?php
namespace Ultra\Configuration\Repositories\Cfengine;

use Ultra\Configuration\Interfaces\ConfigurationRepository as Configuration;

class ConfigurationRepository implements Configuration
{
  /**
   * Find the configuration by key.
   *
   * @param $key
   * @return mixed
   */
  public function findConfigByKey($key)
  {
    global $credentials;
    
    $value = '';
    $skey = $key;

    // if the credentials aren't there, return NULL
    if ( ! $credentials || ! is_array($credentials) )
      return NULL;

    foreach ($credentials as $cname => $cred)
    {
      if ( ! empty( $cred ) )
        foreach ($cred as $key => $val)
          if ($key === $skey)
            $value = $val;
    }
    
    if ( $value || ( $value != '' ) )
      return $value;

    global $e_config;

    if ( isset($e_config) && isset($e_config[ $key ]) )
      return $e_config[ $key ];

    return '';
  }
}

<?php
namespace Ultra;

use Ultra\Exceptions\InvalidObjectCreationException;

require_once 'Ultra/Exceptions/InvalidObjectCreationException.php';

/**
 * Class Hydrator
 * @package Ultra
 */
trait Hydrator
{
  /**
   * Hyrdate class properties from $data array.
   *
   * @param array $data
   * @param bool $changeArrayKeyCasing
   */
  private function hydrate(array $data, $changeArrayKeyCasing = false)
  {
    // Used to have a consistent property casing when hydrating the class model.
    if ($changeArrayKeyCasing)
    {
      $data = array_change_key_case($data, CASE_LOWER);
    }

    $dataKeys = array_keys($data);
    $classVars = array_keys(get_object_vars($this));

    foreach ($dataKeys as $key)
    {
      if (in_array($key, $classVars))
      {
        $this->$key = $data[$key];
      }
    }
  }

  /**
   * Hyrdate all class properties from $data array.
   *
   * @param array $data
   * @param bool $changeArrayKeyCasing
   * @throws InvalidObjectCreationException
   */
  private function hyrdateAllItems(array $data, $changeArrayKeyCasing = false)
  {
    // Used to have a consistent property casing when hydrating the class model.
    if ($changeArrayKeyCasing) {
      $data = array_change_key_case($data, CASE_LOWER);
    }

    $dataKeys = array_keys($data);
    $classVars = array_keys(get_object_vars($this));

    foreach ($dataKeys as $key) {
      if (in_array($key, $classVars)) {
        $this->$key = $data[$key];
      }
    }

    foreach ($classVars as $key) {
      if (empty($this->$key)) {
        throw new InvalidObjectCreationException("Missing required parameter $key in class " . get_class($this));
      }
    }
  }
}

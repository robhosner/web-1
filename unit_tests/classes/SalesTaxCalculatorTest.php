<?php

use Ultra\Accounts\Account;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Payments\TaxCalculator\SureTax;

require 'classes/SalesTaxCalculator.php';

class SalesTaxCalculatorTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var SalesTaxCalculator
   */
  public $calc;

  public $testTaxResult;

  public function setUp()
  {
    $this->calc = new SalesTaxCalculator();
    $this->testTaxResult = new Result();
    $this->testTaxResult->data_array = (array) json_decode('
    {  
      "Successful":"Y",
      "ResponseCode":"9999",
      "HeaderMessage":"Success",
      "ItemMessages":[  
      
      ],
      "ClientTracking":"",
      "TotalTax":"2.15080",
      "TransId":720019857,
      "GroupList":[ 
        {  
          "LineNumber":"1",
          "StateCode":"IL",
          "InvoiceNumber":"82672739123298869",
          "CustomerNumber":"0000000001",
          "TaxList":[  
            {  
               "TaxTypeCode":"101",
               "TaxTypeDesc":"STATE SALES TAX",
               "TaxAmount":"1.18750",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.062500000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            },
            {  
               "TaxTypeCode":"403",
               "TaxTypeDesc":"(RTA) REGIONAL TRANS. AUTH.",
               "TaxAmount":"0.14250",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.007500000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            },
            {  
               "TaxTypeCode":"304",
               "TaxTypeDesc":"CITY SALES TAX",
               "TaxAmount":"0.23750",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.012500000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            },
            {  
               "TaxTypeCode":"106",
               "TaxTypeDesc":"IL PREPAID WIRELESS 911 CHARGE",
               "TaxAmount":"0.57000",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.030000000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            },
            {  
               "TaxTypeCode":"109",
               "TaxTypeDesc":"MTS ASSESSMENT",
               "TaxAmount":"0.01330",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.000700000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            }
          ]
        }
      ]
    }');

    parent::setUp();
  }

  public function testSetBillingInfoByCustomerIdReturnsNull()
  {
    $this->assertNull($this->calc->setBillingInfoByCustomerId(0));
  }

  public function testSetBillingInfoByCustomerIdWithCCPostalCode()
  {
    $customerRepo = $this->getMock(CustomerRepository::class);
    $customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(1, ['BRAND_ID'])
      ->will($this->returnValue(new Customer(['customer_id' => 1, 'brand_id' => 1])));

    $customerRepo->expects($this->any())
      ->method('getCustomerFromCustomersTable')
      ->with(1, ['CC_POSTAL_CODE','POSTAL_CODE'])
      ->will($this->returnValue(new Customer(['customer_id' => 1, 'cc_postal_code' => 91234, 'postal_code' => 12345])));

    $accountRepo = $this->getMock(AccountsRepository::class);
    $accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(1, ['ACCOUNT'])
      ->will($this->returnValue(new Account(['account' => 1234])));

    $this->calc->setCustomerRepository($customerRepo);
    $this->calc->setAccountRepository($accountRepo);

    $result = $this->calc->setBillingInfoByCustomerId(1);

//    print_r($result);

    $this->assertEquals([
      'customer_id' => 1,
      'account' => 1234,
      'billingZipCode' => 91234
    ], $result);

    $this->assertEquals(12345, $this->calc->activationZipCode);
  }

  public function testSetBillingInfoByCustomerIdWithPostalCode()
  {
    $customerRepo = $this->getMock(CustomerRepository::class);
    $customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(1, ['BRAND_ID'])
      ->will($this->returnValue(new Customer(['customer_id' => 1, 'brand_id' => 1])));

    $customerRepo->expects($this->any())
      ->method('getCustomerFromCustomersTable')
      ->with(1, ['CC_POSTAL_CODE','POSTAL_CODE'])
      ->will($this->returnValue(new Customer(['customer_id' => 1, 'postal_code' => 12345])));

    $accountRepo = $this->getMock(AccountsRepository::class);
    $accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(1, ['ACCOUNT'])
      ->will($this->returnValue(new Account(['account' => 1234])));

    $this->calc->setCustomerRepository($customerRepo);
    $this->calc->setAccountRepository($accountRepo);

    $result = $this->calc->setBillingInfoByCustomerId(1);

//    print_r($result);

    $this->assertEquals([
      'customer_id' => 1,
      'account' => 1234,
      'billingZipCode' => 12345
    ], $result);

    $this->assertEquals(12345, $this->calc->activationZipCode);
  }

  public function testSetAccountEmpty()
  {
    $result = $this->calc->setAccount('');
    $this->assertEmpty($this->calc->account);
    $this->assertNull($result);
  }

  public function testSetAccount()
  {
    $result = $this->calc->setAccount('account');
    $this->assertEquals('account', $this->calc->account);
    $this->assertNull($result);
  }

  public function testSetAmountEmpty()
  {
    $result = $this->calc->setAmount('');
    $this->assertEmpty($this->calc->amount);
    $this->assertNull($result);
  }

  public function testSetAmount()
  {
    $result = $this->calc->setAmount(123);
    $this->assertEquals(123, $this->calc->amount);
    $this->assertNull($result);
  }

  public function testAddMount()
  {
    $this->calc->setAmount(123);
    $result = $this->calc->addAmount(123);
    $this->assertEquals(123 * 2, $this->calc->amount);
    $this->assertNull($result);
  }

  public function testSetBillingZipCodeEmpty()
  {
    $result = $this->calc->setBillingZipCode('');
    $this->assertEmpty($this->calc->billingZipCode);
    $this->assertNull($result);
  }

  public function testSetBillingZipCode()
  {
    $result = $this->calc->setBillingZipCode(92123);
    $this->assertEquals(92123, $this->calc->billingZipCode);
    $this->assertNull($result);
  }

  public function testIsCaliforniaZipIsFalse()
  {
    $this->assertFalse(SalesTaxCalculator::isCaliforniaZip(12345));
  }

  public function testIsCaliforniaZipIsTrue()
  {
    $this->assertTrue(SalesTaxCalculator::isCaliforniaZip(91234));
  }

  /*
  public function testCalculateTaxesFeesRechargeReturnsError()
  {
    $calc = $this->getMockBuilder(SalesTaxCalculator::class)
      ->setConstructorArgs([])
      ->setMethods(['getSalesTax'])
      ->getMock();

    $calc->setBillingZipCode(91234);
    $calc->setAmount(500);
    $calc->setAccount('account');

    $result = new Result();
    $result->add_error('Fail');

    $calc->expects($this->any())
      ->method('getSalesTax')
      ->with(500000)
      ->will($this->returnValue($result));

    $result = $calc->calculateTaxesFees('RECHARGE');

//      print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertEquals('SureTax API error', $result->data_array['sales_tax_rule']);
    $this->assertEquals('Fail', $result->get_warnings()[0]);

  }
  */

  /*
  public function testCalculateTaxesFeesRechargeReturns0Tax()
  {
    $calc = $this->getMockBuilder(SalesTaxCalculator::class)
      ->setConstructorArgs([])
      ->setMethods(['getSalesTax'])
      ->getMock();

    $calc->setBillingZipCode(91234);
    $calc->setAmount(500);
    $calc->setAccount('account');

    $result = new Result();
    $result->add_data_array(['TotalTax' => 0.00]);
    $calc->expects($this->any())
      ->method('getSalesTax')
      ->with(500000)
      ->will($this->returnValue($result));

    $result = $calc->calculateTaxesFees('RECHARGE');

//      print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertEquals('SureTax API error', $result->data_array['sales_tax_rule']);
    $this->assertEquals('SureTax API returned 0 taxes', $result->get_warnings()[0]);
  }
  */

  public function testCalculateTaxesFeesRechargeSuccess()
  {
    $calc = $this->getMockBuilder(SalesTaxCalculator::class)
      ->setConstructorArgs([])
      ->setMethods(['getSalesTax', 'getRecoveryFee'])
      ->getMock();

    $calc->setBillingZipCode(91234);
    $calc->setAmount(500);
    $calc->setAccount('account');

    $result = new stdClass();
    $result->cos_id = 123;

    $accountRepo = $this->getMock(AccountsRepository::class);
    $accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(null, ['COS_ID'])
      ->will($this->returnValue($result));

    $calc->setAccountRepository($accountRepo);

    $this->testTaxResult->add_data_array(['TotalTax' => 10.00, 'zip_code' => 91234]);
    $calc->expects($this->any())
      ->method('getSalesTax')
      ->with(500000)
      ->will($this->returnValue($this->testTaxResult));

    $calc->expects($this->any())
      ->method('getRecoveryFee')
      ->with(500, 123)
      ->will($this->returnValue([
        'recovery_fee'   => 0,
        'recovery_rule'  => 0,
        'recovery_basis' => 0
      ]));

    $customerRepo = $this->getMock(CustomerRepository::class);
    $customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue(true));

    $calc->setCustomerRepository($customerRepo);

    $result = $calc->calculateTaxesFees('RECHARGE');

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertEquals('SureTax Rule 500000', $result->data_array['sales_tax_rule']);
    $this->assertEquals(43, $result->data_array['sales_tax_percentage']);
    $this->assertEquals(215, $result->data_array['mts_tax']);
    $this->assertEquals(0, $result->data_array['sales_tax']);
  }

  public function testCalculateTaxesFeesNotRechargeSuccess()
  {
    $calc = $this->getMockBuilder(SalesTaxCalculator::class)
      ->setConstructorArgs([])
      ->setMethods(['getSalesTax', 'getRecoveryFee'])
      ->getMock();

    $calc->setBillingZipCode(91234);
    $calc->setAmount(500);
    $calc->setAccount('account');

    $result = new stdClass();
    $result->cos_id = 123;

    $accountRepo = $this->getMock(AccountsRepository::class);
    $accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(null, ['COS_ID'])
      ->will($this->returnValue($result));

    $calc->setAccountRepository($accountRepo);

    $this->testTaxResult->add_data_array(['TotalTax' => 10.00, 'zip_code' => 91234]);
    $calc->expects($this->any())
      ->method('getSalesTax')
      ->with('500000')
      ->will($this->returnValue($this->testTaxResult));

    $calc->expects($this->any())
      ->method('getRecoveryFee')
      ->with(500, 123)
      ->will($this->returnValue([
        'recovery_fee'   => 0,
        'recovery_rule'  => 0,
        'recovery_basis' => 0
      ]));

    $customerRepo = $this->getMock(CustomerRepository::class);
    $customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue(true));

    $calc->setCustomerRepository($customerRepo);

    $result = $calc->calculateTaxesFees('NOT A RECHARGE');

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertEquals('SureTax Rule 500000', $result->data_array['sales_tax_rule']);
    $this->assertEquals(43, $result->data_array['sales_tax_percentage']);
    $this->assertEquals(215, $result->data_array['mts_tax']);
    $this->assertEquals(0, $result->data_array['sales_tax']);
  }

  public function testParseTaxResultJan2017ForCalifornia()
  {
    $this->calc->setBillingZipCode(91234);
    $this->calc->setAmount(500);
    $result = $this->calc->parseTaxResultJan2017($this->testTaxResult);

//    print_r($result);

    $this->assertEquals(215, $result['mts_tax']);
    $this->assertEquals(0, $result['sales_tax']);
    $this->assertEquals(43, $result['sales_tax_percentage']);
  }

  public function testParseTaxResultJan2017NotForCalifornia()
  {
    $this->calc->setBillingZipCode(12345);
    $this->calc->setAmount(500);
    $result = $this->calc->parseTaxResultJan2017($this->testTaxResult);

//    print_r($result);

    $this->assertEquals(0, $result['mts_tax']);
    $this->assertEquals(215, $result['sales_tax']);
    $this->assertEquals(43, $result['sales_tax_percentage']);
  }

  public function testParseTaxResultDefault()
  {
    $this->calc->setBillingZipCode(12345);
    $this->calc->setAmount(500);
    $result = $this->calc->parseTaxResultDefault($this->testTaxResult);

//    print_r($result);

    $this->assertEquals(0, $result['mts_tax']);
    $this->assertEquals(156, $result['sales_tax']);
    $this->assertEquals(0.0825, $result['sales_tax_percentage']);
    $this->assertEquals('SureTax Rule v1', $result['sales_tax_rule']);
  }

  public function testParseTaxResultMTSUpdateForCalifornia()
  {
    $this->calc->setBillingZipCode(91234);
    $this->calc->setAmount(500);
    $result = $this->calc->parseTaxResultMTSUpdate($this->testTaxResult);

//    print_r($result);

    $this->assertEquals(1, $result['mts_tax']);
    $this->assertEquals(142, $result['sales_tax']);
    $this->assertEquals(28.6, $result['sales_tax_percentage']);
    $this->assertEquals('SureTax Rule 500000', $result['sales_tax_rule']);
  }

  public function testParseTaxResultMTSUpdateNotForCalifornia()
  {
    $this->calc->setBillingZipCode(12345);
    $this->calc->setAmount(500);
    $result = $this->calc->parseTaxResultMTSUpdate($this->testTaxResult);

//    print_r($result);

    $this->assertEquals(0, $result['mts_tax']);
    $this->assertEquals(142, $result['sales_tax']);
    $this->assertEquals(28.4, $result['sales_tax_percentage']);
    $this->assertEquals('SureTax Rule 500000', $result['sales_tax_rule']);
  }

  public function testGetRecoveryFeeWhereAmountIs0()
  {
    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(false));

    SalesTaxCalculator::setConfiguration($config);
    $result = $this->calc->getRecoveryFee(0, 123);

//    print_r($result);

    $this->assertEquals(0, $result['recovery_fee']);
    $this->assertEquals(0, $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeWhereAmountIsGreaterThan1000()
  {
    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(false));

    SalesTaxCalculator::setConfiguration($config);
    $result = $this->calc->getRecoveryFee(1001, 123);

//    print_r($result);

    $this->assertEquals(100, $result['recovery_fee']);
    $this->assertEquals('Charge > $10', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeWhereAmountIsLessThan1000()
  {
    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(false));

    SalesTaxCalculator::setConfiguration($config);
    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(50, $result['recovery_fee']);
    $this->assertEquals('Charge <= $10', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeWhereAmountIsGreaterThanEqualTo5700()
  {
    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(false));

    $config->expects($this->any())
      ->method('getBrandIDFromCOSID')
      ->will($this->returnValue(1));

    $config->expects($this->any())
      ->method('planMonthsByCosID')
      ->will($this->returnValue(3));

    SalesTaxCalculator::setConfiguration($config);
    $result = $this->calc->getRecoveryFee(5700, 123);

//    print_r($result);

    $this->assertEquals(300, $result['recovery_fee']);
    $this->assertEquals('Multi month plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeFor1MonthMint()
  {
    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(1));

    SalesTaxCalculator::setConfiguration($config);
    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(100, $result['recovery_fee']);
    $this->assertEquals('1 Month Plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeFor3MonthMint()
  {
    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(3));

    SalesTaxCalculator::setConfiguration($config);
    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(175, $result['recovery_fee']);
    $this->assertEquals('3 Month Plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeFor6MonthMint()
  {
    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(6));

    SalesTaxCalculator::setConfiguration($config);
    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(250, $result['recovery_fee']);
    $this->assertEquals('6 Month Plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeFor12MonthMint()
  {
    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(12));

    SalesTaxCalculator::setConfiguration($config);
    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(400, $result['recovery_fee']);
    $this->assertEquals('12 Month Plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeForMint()
  {
    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(22));

    SalesTaxCalculator::setConfiguration($config);
    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(0, $result['recovery_fee']);
    $this->assertEquals(0, $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetSalesTaxNoAmountProvided()
  {
    $this->calc->amount = null;
    $result = $this->calc->getSalesTax(12);

//    print_r($result);

    $this->assertContains('No amount provided', $result->get_errors()[0]);

    $result = $this->calc->getSalesTax(12);
    $this->calc->amount = 'no amount';
    $this->assertContains('No amount provided', $result->get_errors()[0]);
  }

  public function testGetSalesTaxNoZipProvided()
  {
    $this->calc->amount = 100;
    $this->calc->billingZipCode = 'zip';
    $result = $this->calc->getSalesTax(12);

//    print_r($result);

    $this->assertContains('No zip code provided', $result->get_errors()[0]);

    $result = $this->calc->getSalesTax(12);
    $this->calc->billingZipCode = 'no amount';
    $this->assertContains('No zip code provided', $result->get_errors()[0]);
  }

  public function testGetSalesTaxNoAccountProvided()
  {
    $this->calc->amount = 100;
    $this->calc->billingZipCode = 12345;
    $result = $this->calc->getSalesTax(12);

//    print_r($result);

    $this->assertContains('No account provided', $result->get_errors()[0]);
  }

  public function testGetSalesTaxSuccess()
  {
    $calc = $this->getMockBuilder(SalesTaxCalculator::class)
      ->setConstructorArgs([])
      ->setMethods(['callSureTax'])
      ->getMock();

    $calc->amount         = 100;
    $calc->billingZipCode = 12345;
    $calc->account        = 12345;

    $calc->expects($this->any())
      ->method('callSureTax')
      ->with(100, 12345, 12345, 12)
      ->will($this->returnValue(true));

    $result = $calc->getSalesTax(12);

//    print_r($result);

    $this->assertTrue($result);
  }

  public function testRecordSureTaxQuoteReturnsNull()
  {
    $this->assertNull($this->calc->recordSureTaxQuote('string'));
  }

  public function testCallSureTaxFail()
  {
    $result = new Result();
    $result->fail();

    $sureTax = $this->getMock(SureTax::class);
    $sureTax->expects($this->any())
      ->method('performCall')
      ->with([
        'amount'                => (100 / 100),
        'zip_code'              => trim(12345),
        'account'               => 1,
        'transaction_type_code' => 1,
        'return_file_code'      => null
      ])
      ->will($this->returnValue($result));

    $this->calc->setSureTax($sureTax);
    $result = $this->calc->callSureTax(100, 12345, 1, 1, null);

//      print_r($result);

    $this->assertEquals('SureTax API error : call failed', $result->get_errors()[0]);
  }

  public function testCallSureTaxReturnsNoTax()
  {
    $result = new Result();
    $result->succeed();

    $sureTax = $this->getMock(SureTax::class);
    $sureTax->expects($this->any())
      ->method('performCall')
      ->with([
        'amount'                => (100 / 100),
        'zip_code'              => trim(12345),
        'account'               => 1,
        'transaction_type_code' => 1,
        'return_file_code'      => null
      ])
      ->will($this->returnValue($result));

    $this->calc->setSureTax($sureTax);
    $result = $this->calc->callSureTax(100, 12345, 1, 1, null);

//      print_r($result);

    $this->assertEquals('SureTax API error : call did not return meaningful data', $result->get_errors()[0]);
  }

  public function testCallSureTaxReturnsRetryError()
  {
    $result = new Result();
    $result->succeed();

    $itemMessage = new stdClass();
    $itemMessage->Message = 'Error';
    $itemMessage->ResponseCode = 9152;

    $result->add_data_array(['ItemMessages' => [$itemMessage], 'TotalTax' => 123]);
    $this->calc->activationZipCode = 92345;

    $sureTax = $this->getMock(SureTax::class);
    $sureTax->expects($this->any(1))
      ->method('performCall')
      ->with([
        'amount'                => (100 / 100),
        'zip_code'              => trim(92345),
        'account'               => 1,
        'transaction_type_code' => 1,
        'return_file_code'      => null
      ])
      ->will($this->returnValue($result));

    $sureTax->expects($this->any(2))
      ->method('performCall')
      ->with([
        'amount'                => (100 / 100),
        'zip_code'              => trim(92345),
        'account'               => 1,
        'transaction_type_code' => 1,
        'return_file_code'      => null
      ])
      ->will($this->returnValue($result));

    $this->calc->setSureTax($sureTax);
    $result = $this->calc->callSureTax(100, 92345, 1, 1, null);

//      print_r($result);

    $this->assertEquals('SureTax API error : message = Error', $result->get_errors()[0]);
  }

  public function testCallSureTaxReturnsInvalidResponse()
  {
    $result = new Result();
    $result->succeed();

    $taxList = new stdClass();
    $taxList->TaxList = [];

    $result->add_data_array([
      'TotalTax' => 123,
      'GroupList' => [$taxList]
    ]);
    $this->calc->activationZipCode = 92345;

    $sureTax = $this->getMock(SureTax::class);
    $sureTax->expects($this->any(1))
      ->method('performCall')
      ->with([
        'amount'                => (100 / 100),
        'zip_code'              => trim(92345),
        'account'               => 1,
        'transaction_type_code' => 1,
        'return_file_code'      => null
      ])
      ->will($this->returnValue($result));

    $sureTax->expects($this->any(2))
      ->method('performCall')
      ->with([
        'amount'                => (100 / 100),
        'zip_code'              => trim(92345),
        'account'               => 1,
        'transaction_type_code' => 1,
        'return_file_code'      => null
      ])
      ->will($this->returnValue($result));

    $this->calc->setSureTax($sureTax);
    $result = $this->calc->callSureTax(100, 92345, 1, 1, null);

//      print_r($result);

    $this->assertEquals('SureTax API error : API response invalid', $result->get_errors()[0]);
  }

  public function testCallSureTaxReturnsSuccess()
  {
    $result = new Result();
    $result->succeed();

    $taxList = new stdClass();
    $taxList->TaxList = [1];

    $result->add_data_array([
      'TotalTax' => 123,
      'GroupList' => [$taxList]
    ]);
    $this->calc->activationZipCode = 92345;

    $sureTax = $this->getMock(SureTax::class);
    $sureTax->expects($this->any(1))
      ->method('performCall')
      ->with([
        'amount'                => (100 / 100),
        'zip_code'              => trim(92345),
        'account'               => 1,
        'transaction_type_code' => 1,
        'return_file_code'      => null
      ])
      ->will($this->returnValue($result));

    $sureTax->expects($this->any(2))
      ->method('performCall')
      ->with([
        'amount'                => (100 / 100),
        'zip_code'              => trim(92345),
        'account'               => 1,
        'transaction_type_code' => 1,
        'return_file_code'      => null
      ])
      ->will($this->returnValue($result));

    $this->calc->setSureTax($sureTax);
    $result = $this->calc->callSureTax(100, 92345, 1, 1, null);

//      print_r($result);

    $this->assertTrue($result->is_success());
  }

  public function testSetCosId()
  {
    $this->assertEquals(123, $this->calc->setCosId(123));
  }
}

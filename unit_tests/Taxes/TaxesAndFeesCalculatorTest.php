<?php

use Ultra\Configuration\Configuration;
use Ultra\Configuration\Repositories\Cfengine\ConfigurationRepository;
use Ultra\Payments\TaxCalculator\SureTax;
use Ultra\Taxes\TaxesAndFeesCalculator;
use Ultra\Taxes\TransactionTypeCodeConfig;

class TaxesAndFeesCalculatorTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var TaxesAndFeesCalculator
   */
  public $calc;

  public $testTaxResult;
  private $sureTax;
  private $transactionTypeCodeConfig;
  private $config;

  public function setUp()
  {
    $configRepo = $this->getMock(ConfigurationRepository::class);
    $configRepo->expects($this->any())
      ->method('findConfigByKey')
      ->will($this->returnValue(1));

    $this->transactionTypeCodeConfig = $this->getMockBuilder(TransactionTypeCodeConfig::class)
      ->setConstructorArgs([$configRepo])
      ->setMethods(['calculateTaxesAndFees'])
      ->getMock();

    $this->sureTax = $this->getMock(SureTax::class);
    $this->config = $this->getMock(Configuration::class);
    $this->calc = new TaxesAndFeesCalculator($this->sureTax, $this->transactionTypeCodeConfig, $this->config);

    $this->testTaxResult = new Result();
    $this->testTaxResult->data_array = (array) json_decode('
    {  
      "Successful":"Y",
      "ResponseCode":"9999",
      "HeaderMessage":"Success",
      "ItemMessages":[  
      
      ],
      "ClientTracking":"",
      "TotalTax":"2.15080",
      "TransId":720019857,
      "GroupList":[ 
        {  
          "LineNumber":"1",
          "StateCode":"IL",
          "InvoiceNumber":"82672739123298869",
          "CustomerNumber":"0000000001",
          "TaxList":[  
            {  
               "TaxTypeCode":"101",
               "TaxTypeDesc":"STATE SALES TAX",
               "TaxAmount":"1.18750",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.062500000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            },
            {  
               "TaxTypeCode":"403",
               "TaxTypeDesc":"(RTA) REGIONAL TRANS. AUTH.",
               "TaxAmount":"0.14250",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.007500000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            },
            {  
               "TaxTypeCode":"304",
               "TaxTypeDesc":"CITY SALES TAX",
               "TaxAmount":"0.23750",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.012500000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            },
            {  
               "TaxTypeCode":"106",
               "TaxTypeDesc":"IL PREPAID WIRELESS 911 CHARGE",
               "TaxAmount":"0.57000",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.030000000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            },
            {  
               "TaxTypeCode":"109",
               "TaxTypeDesc":"MTS ASSESSMENT",
               "TaxAmount":"0.01330",
               "Revenue":"19",
               "CountyName":"KANE",
               "CityName":"ELGIN",
               "TaxRate":0.000700000000,
               "PercentTaxable":1.000000,
               "FeeRate":0,
               "RevenueBase":"19.00000",
               "TaxOnTax":"0"
            }
          ]
        }
      ]
    }');
  }

  public function testCalculateTaxesAndFeesMissingParameters()
  {
    $result = $this->calc->calculateTaxesAndFees(1, 2, 3, 4, null);
    $this->assertFalse($result->is_success());
    $this->assertContains('missing one or more parameters $brand, $cosId, $product, $amount, $zipCode', $result->get_errors());
  }

  public function testCalculateTaxesAndFeesInvalidBrand()
  {
    $result = $this->calc->calculateTaxesAndFees(2, 2, 3, 4, 5);
    $this->assertFalse($result->is_success());
    $this->assertContains("Invalid brand 2", $result->get_errors());
  }

  public function testCalculateTaxesAndFeesMissingTransactionTypeCode()
  {
    $result = $this->calc->calculateTaxesAndFees(1, 2, 3, 4, 5);
    $this->assertFalse($result->is_success());
    $this->assertContains("No TransactionTypeCode found for brand: ULTRA, product: 3.", $result->get_errors());

    $result = $this->calc->calculateTaxesAndFees(3, 2, 3, 4, 5);
    $this->assertFalse($result->is_success());
    $this->assertContains("No TransactionTypeCode found for brand: MINT, product: 3.", $result->get_errors());
  }

  public function testCalculateTaxesAndFeesSureTaxFails()
  {
    $this->sureTax->expects($this->any())
      ->method('performCall')
      ->will($this->returnValue(new Result(null, false)));
    $result = $this->calc->calculateTaxesAndFees(1, 2, 'PLAN', 4, 5);

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertEquals(
    [
      'zip_code' => 5,
      'mts_tax' => 0,
      'sales_tax' => 0,
      'sales_tax_rule' => 'SureTax API error',
      'sales_tax_percentage' => 0,
      'recovery_fee' => 50,
      'recovery_fee_rule' => 'CC Recovery Rule v1',
      'recovery_fee_basis' => 'Charge <= $10',
    ], $result->data_array);
  }

  public function testCalculateTaxesAndFeesSureTaxSucceeds()
  {
    $this->testTaxResult->succeed();
    $this->testTaxResult->add_data_array(['TotalTax' => 10.00, 'zip_code' => 91234]);

    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->will($this->returnValue(true));

    $this->config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->will($this->returnValue(3));


    $this->sureTax->expects($this->any())
      ->method('performCall')
      ->will($this->returnValue($this->testTaxResult));
    $result = $this->calc->calculateTaxesAndFees(3, 2, 'PLAN', 4500, 5);

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertEquals(
      [
        'zip_code' => 91234,
        'mts_tax' => 0.0,
        'sales_tax' => 215.0,
        'sales_tax_rule' => 'SureTax Rule 1',
        'sales_tax_percentage' => 4.59893048128342218916486672242172062397003173828125,
        'recovery_fee' => 175,
        'recovery_fee_rule' => 'CC Recovery Rule v1',
        'recovery_fee_basis' => '3 Month Plan',
      ], $result->data_array);
  }

  public function testParseTaxResultJan2017ForCalifornia()
  {
    $class = new ReflectionClass(TaxesAndFeesCalculator::class);
    $amount = $class->getProperty("amount");
    $amount->setAccessible(true);
    $amount->setValue($this->calc, 500);

    $zip = $class->getProperty("zipCode");
    $zip->setAccessible(true);
    $zip->setValue($this->calc, 91234);

    $method = $class->getMethod('parseTaxResultJan2017');
    $method->setAccessible(true);

    $result = $method->invoke($this->calc, $this->testTaxResult);

//    print_r($result);

    $this->assertEquals(215, $result['mts_tax']);
    $this->assertEquals(0, $result['sales_tax']);
    $this->assertEquals(43, $result['sales_tax_percentage']);
  }

  public function testParseTaxResultJan2017NotForCalifornia()
  {
    $class = new ReflectionClass(TaxesAndFeesCalculator::class);
    $amount = $class->getProperty("amount");
    $amount->setAccessible(true);
    $amount->setValue($this->calc, 500);

    $zip = $class->getProperty("zipCode");
    $zip->setAccessible(true);
    $zip->setValue($this->calc, 12345);

    $method = $class->getMethod('parseTaxResultJan2017');
    $method->setAccessible(true);

    $result = $method->invoke($this->calc, $this->testTaxResult);

//    print_r($result);

    $this->assertEquals(0, $result['mts_tax']);
    $this->assertEquals(215, $result['sales_tax']);
    $this->assertEquals(43, $result['sales_tax_percentage']);
  }

  public function testGetRecoveryFeeWhereAmountIs0()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(false));

    $result = $this->calc->getRecoveryFee(0, 123);

//    print_r($result);

    $this->assertEquals(0, $result['recovery_fee']);
    $this->assertEquals(0, $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeWhereAmountIsGreaterThan1000()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(false));

    $result = $this->calc->getRecoveryFee(1001, 123);

//    print_r($result);

    $this->assertEquals(100, $result['recovery_fee']);
    $this->assertEquals('Charge > $10', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeWhereAmountIsLessThan1000()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(false));

    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(50, $result['recovery_fee']);
    $this->assertEquals('Charge <= $10', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeWhereAmountIsGreaterThanEqualTo5700()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(false));

    $this->config->expects($this->any())
      ->method('getBrandIDFromCOSID')
      ->will($this->returnValue(1));

    $this->config->expects($this->any())
      ->method('planMonthsByCosID')
      ->will($this->returnValue(3));

    $result = $this->calc->getRecoveryFee(5700, 123);

//    print_r($result);

    $this->assertEquals(300, $result['recovery_fee']);
    $this->assertEquals('Multi month plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeFor1MonthMint()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $this->config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(1));

    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(100, $result['recovery_fee']);
    $this->assertEquals('1 Month Plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeFor3MonthMint()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $this->config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(3));

    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(175, $result['recovery_fee']);
    $this->assertEquals('3 Month Plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeFor6MonthMint()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $this->config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(6));

    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(250, $result['recovery_fee']);
    $this->assertEquals('6 Month Plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeFor12MonthMint()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $this->config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(12));

    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(400, $result['recovery_fee']);
    $this->assertEquals('12 Month Plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeForMint()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $this->config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(22));

    $result = $this->calc->getRecoveryFee(500, 123);

//    print_r($result);

    $this->assertEquals(0, $result['recovery_fee']);
    $this->assertEquals(0, $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetRecoveryFeeForMintWithAddBalance()
  {
    $this->config->expects($this->any())
      ->method('isMintPlan')
      ->with(123)
      ->will($this->returnValue(true));

    $this->config->expects($this->any())
      ->method('mintMonthsByCosId')
      ->with(123)
      ->will($this->returnValue(12));

    $class = new ReflectionClass(TaxesAndFeesCalculator::class);
    $method = $class->getMethod('getRecoveryFee');
    $method->setAccessible(true);

    $product = $class->getProperty("product");
    $product->setAccessible(true);
    $product->setValue($this->calc, 'ADD_BALANCE');

    $result = $method->invoke($this->calc, 500, 123);

//    print_r($result);

    $this->assertEquals(0, $result['recovery_fee']);
    $this->assertEquals('12 Month Plan', $result['recovery_basis']);
    $this->assertEquals('CC Recovery Rule v1', $result['recovery_rule']);
  }

  public function testGetSalesTaxSuccess()
  {
    $this->testTaxResult->succeed();
    $this->testTaxResult->add_data_array(['TotalTax' => 12.00, 'zip_code' => 12345]);

    $this->sureTax->expects($this->any())
      ->method('performCall')
      ->will($this->returnValue($this->testTaxResult));

    $result = $this->calc->getSalesTax(12, 12345, 1);

//    print_r($result);

    $this->assertTrue($result->is_success());
  }

  public function testCallSureTaxReturnsNoTax()
  {
    $this->sureTax->expects($this->any())
      ->method('performCall')
      ->will($this->returnValue(new Result(null, true)));

    $class = new ReflectionClass(TaxesAndFeesCalculator::class);
    $method = $class->getMethod('callSureTax');
    $method->setAccessible(true);

    $result = $method->invoke($this->calc, 100, 12345, 1, 1, null);

//      print_r($result);

    $this->assertEquals('SureTax API error : call did not return meaningful data', $result->get_errors()[0]);
  }

  public function testCallSureTaxReturnsRetryError()
  {
    $result = new Result();
    $result->succeed();

    $itemMessage = new stdClass();
    $itemMessage->Message = 'Error';
    $itemMessage->ResponseCode = 9152;

    $result->add_data_array(['ItemMessages' => [$itemMessage], 'TotalTax' => 123]);

    $this->sureTax->expects($this->any())
      ->method('performCall')
      ->will($this->returnValue($result));

    $class = new ReflectionClass(TaxesAndFeesCalculator::class);
    $method = $class->getMethod('callSureTax');
    $method->setAccessible(true);

    $result = $method->invoke($this->calc,100, 92345, 1, 1, null);

//      print_r($result);

    $this->assertEquals('SureTax API error : message = Error', $result->get_errors()[0]);
  }

  public function testCallSureTaxReturnsInvalidResponse()
  {
    $taxList = new stdClass();
    $taxList->TaxList = [];

    $this->testTaxResult->succeed();
    $this->testTaxResult->add_data_array(['TotalTax' => 10.00, 'zip_code' => 92345, 'GroupList' => [$taxList]]);

    $this->sureTax->expects($this->any())
      ->method('performCall')
      ->will($this->returnValue($this->testTaxResult));
    $result = $this->calc->calculateTaxesAndFees(3, 2, 'PLAN', 4, 92345);

//    print_r($result);

    $this->assertEquals('SureTax API error : API response invalid', $result->get_warnings()[0]);
  }

  public function testCalculateTaxesFeesFlexRecoveryFee()
  {
    $this->testTaxResult->succeed();
    $this->testTaxResult->add_data_array(['TotalTax' => 10.00, 'zip_code' => 92345]);

    $this->sureTax->expects($this->any())
      ->method('performCall')
      ->will($this->returnValue($this->testTaxResult));

    $class = new ReflectionClass(TaxesAndFeesCalculator::class);
    $method = $class->getMethod('calculateTaxesFees');
    $method->setAccessible(true);

    $amount = $class->getProperty("amount");
    $amount->setAccessible(true);
    $amount->setValue($this->calc, 500);

    $result = $method->invoke($this->calc, 1, 'FLEX_RECHARGE_2');

//    print_r($result);

    $this->assertEquals([
      'zip_code' => 92345,
      'mts_tax' => 0.0,
      'sales_tax' => 215.0,
      'sales_tax_rule' => 'SureTax Rule 1',
      'sales_tax_percentage' => 35.83333333333333570180911920033395290374755859375,
      'recovery_fee' => 100,
      'recovery_fee_rule' => 'CC Recovery Rule v1',
      'recovery_fee_basis' => 'Charge <= $10',
    ], $result->data_array);
  }
}

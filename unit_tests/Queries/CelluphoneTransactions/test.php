<?php

//require_once 'db.php';
require_once 'db/htt_celluphone_transactions.php';

class QueriesCelluphoneTransactionsTest extends PHPUnit_Framework_TestCase
{
  public function setUp()
  {
  }

  public function test_celluphone_pq_query()
  {
    $sql = \celluphone_pq_query();

    $sampleSQL = "INSERT INTO [htt_celluphone_transactions]
    ([customer_id],
    transaction_date,
    plan_code,
    trans_type,
    charge_amount,
    payment_type,
    is_commissionable,
    bill_ref_id)
      SELECT h.customer_id,
        [transaction_date],
        CASE h.[cos_id]
WHEN '98280' THEN 'L19'
WHEN '98283' THEN 'L24'
WHEN '98274' THEN 'L29'
WHEN '98332' THEN 'S29'
WHEN '98288' THEN 'M29'
WHEN '98284' THEN 'L34'
WHEN '98275' THEN 'L39'
WHEN '98314' THEN 'D39'
WHEN '98285' THEN 'L44'
WHEN '98333' THEN 'S44'
WHEN '98276' THEN 'L49'
WHEN '98279' THEN 'L59'
WHEN '98293' THEN 'UV20'
WHEN '98289' THEN 'UV30'
WHEN '98290' THEN 'UV35'
WHEN '98291' THEN 'UV45'
WHEN '98294' THEN 'UV50'
WHEN '98292' THEN 'UV55'
WHEN '98320' THEN 'M01S'
WHEN '98321' THEN 'M03S'
WHEN '98322' THEN 'M06S'
WHEN '98323' THEN 'M12S'
WHEN '98324' THEN 'M01M'
WHEN '98325' THEN 'M03M'
WHEN '98326' THEN 'M06M'
WHEN '98327' THEN 'M12M'
WHEN '98328' THEN 'M01L'
WHEN '98329' THEN 'M03L'
WHEN '98330' THEN 'M06L'
WHEN '98331' THEN 'M12L'
        END                AS PLAN_CODE,
        'PQ'               AS TRANS_TYPE,
        0                  AS CHARGE_AMOUNT,
        'NA'               AS PAYMENT_TYPE,
        is_commissionable  AS IS_COMMISSIONABLE,
        h.[transaction_id] AS BILL_REF_ID
      FROM [htt_billing_history] h
      JOIN htt_customers_overlay_ultra o
      ON   o.customer_id = h.customer_id
      WHERE h.source IN ('EPAY', 'TCETRA', 'INCOMM', 'QPAY')
      AND o.BRAND_ID != 3
      AND h.description = 'Plan Recharge (PQ)'
      AND h.cos_id IN ('98280','98283','98274','98332','98288','98284','98275','98314','98285','98333','98276','98279','98293','98289','98290','98291','98294','98292','98320','98321','98322','98323','98324','98325','98326','98327','98328','98329','98330','98331')
      AND h.transaction_id NOT IN (SELECT bill_ref_id
                               FROM   htt_celluphone_transactions WITH (nolock)
                               WHERE  bill_ref_id IS NOT NULL
                               AND [trans_type] = 'PQ')
    ORDER  BY transaction_date";

    $this->assertEquals( $sampleSQL , $sql );
  }

  public function test_celluphone_transaction_newperiod_log_query()
  { 
    $sql = \celluphone_transaction_newperiod_log_query();

    $sampleSQL = "INSERT INTO [htt_celluphone_transactions]
            ([customer_id],
             transaction_date,
             plan_code,
             trans_type,
             charge_amount,
             payment_type,
             is_commissionable,
             bill_ref_id)
  SELECT h.customer_id,
       [transaction_date],
       CASE h.[cos_id]
WHEN '98280' THEN 'L19'
WHEN '98283' THEN 'L24'
WHEN '98274' THEN 'L29'
WHEN '98332' THEN 'S29'
WHEN '98288' THEN 'M29'
WHEN '98284' THEN 'L34'
WHEN '98275' THEN 'L39'
WHEN '98314' THEN 'D39'
WHEN '98285' THEN 'L44'
WHEN '98333' THEN 'S44'
WHEN '98276' THEN 'L49'
WHEN '98279' THEN 'L59'
WHEN '98293' THEN 'UV20'
WHEN '98289' THEN 'UV30'
WHEN '98290' THEN 'UV35'
WHEN '98291' THEN 'UV45'
WHEN '98294' THEN 'UV50'
WHEN '98292' THEN 'UV55'
WHEN '98320' THEN 'M01S'
WHEN '98321' THEN 'M03S'
WHEN '98322' THEN 'M06S'
WHEN '98323' THEN 'M12S'
WHEN '98324' THEN 'M01M'
WHEN '98325' THEN 'M03M'
WHEN '98326' THEN 'M06M'
WHEN '98327' THEN 'M12M'
WHEN '98328' THEN 'M01L'
WHEN '98329' THEN 'M03L'
WHEN '98330' THEN 'M06L'
WHEN '98331' THEN 'M12L'
        END                AS PLAN_CODE,
       'NP'               AS TRANS_TYPE,
       0                  AS CHARGE_AMOUNT,
       'NA'               AS PAYMENT_TYPE,
       1                  AS IS_COMMISSIONABLE,
       h.[transaction_id] AS BILL_REF_ID
  FROM   [htt_billing_history] h
  JOIN htt_customers_overlay_ultra o
  ON   o.customer_id = h.customer_id
  WHERE  transaction_date > '2014-07-01'
       AND o.BRAND_ID != 3
       AND source = 'SPEND'
       AND detail IN ( 'Monthly Renewal', 'Plan Change Renewal', 'Reactivation', 'First Monthly Fee', 'Activation' )
       AND h.cos_id IN ('98280','98283','98274','98332','98288','98284','98275','98314','98285','98333','98276','98279','98293','98289','98290','98291','98294','98292','98320','98321','98322','98323','98324','98325','98326','98327','98328','98329','98330','98331')
       AND h.[transaction_id] NOT IN (SELECT bill_ref_id
                                      FROM   htt_celluphone_transactions WITH (nolock)
                                      WHERE  bill_ref_id IS NOT NULL
                                             AND [trans_type] = 'NP')
  ORDER  BY transaction_date";

    $this->assertEquals( $sampleSQL , $sql );
  }

  public function test_celluphone_activation_log_query()
  { 
    $sql = \celluphone_activation_log_query();

    $sampleSQL = "insert into [HTT_CELLUPHONE_TRANSACTIONS] ([CUSTOMER_ID], TRANSACTION_DATE, PLAN_CODE, TRANS_TYPE, CHARGE_AMOUNT, PAYMENT_TYPE, IS_COMMISSIONABLE) 
  SELECT
    ACTIVATED_CUSTOMER_ID as CUSTOMER_ID,
    ACTIVATED_DATE as TRANSACTION_DATE,
    CASE h.INITIAL_COS_ID
WHEN '98280' THEN 'L19'
WHEN '98283' THEN 'L24'
WHEN '98274' THEN 'L29'
WHEN '98332' THEN 'S29'
WHEN '98288' THEN 'M29'
WHEN '98284' THEN 'L34'
WHEN '98275' THEN 'L39'
WHEN '98314' THEN 'D39'
WHEN '98285' THEN 'L44'
WHEN '98333' THEN 'S44'
WHEN '98276' THEN 'L49'
WHEN '98279' THEN 'L59'
WHEN '98293' THEN 'UV20'
WHEN '98289' THEN 'UV30'
WHEN '98290' THEN 'UV35'
WHEN '98291' THEN 'UV45'
WHEN '98294' THEN 'UV50'
WHEN '98292' THEN 'UV55'
WHEN '98320' THEN 'M01S'
WHEN '98321' THEN 'M03S'
WHEN '98322' THEN 'M06S'
WHEN '98323' THEN 'M12S'
WHEN '98324' THEN 'M01M'
WHEN '98325' THEN 'M03M'
WHEN '98326' THEN 'M06M'
WHEN '98327' THEN 'M12M'
WHEN '98328' THEN 'M01L'
WHEN '98329' THEN 'M03L'
WHEN '98330' THEN 'M06L'
WHEN '98331' THEN 'M12L'
END as PLAN_CODE,
    'AC' as TRANS_TYPE,
    0 as CHARGE_AMOUNT,
    'AC' as PAYMENT_TYPE,
    CASE
      WHEN s.[PRODUCT_TYPE] = 'ORANGE' THEN 0
      WHEN s.[ICCID_BATCH_ID] = 'UVIMPORT' THEN 0
      WHEN o.[NOTES] LIKE 'intra-brand port-in%' THEN 0
      ELSE 1
    END as IS_COMMISSIONABLE
  FROM [HTT_ACTIVATION_LOG] h
  JOIN [HTT_INVENTORY_SIM] s on h.[ICCID_FULL] = s.[ICCID_FULL]
  JOIN [HTT_CUSTOMERS_OVERLAY_ULTRA] o on o.[CUSTOMER_ID] = h.[ACTIVATED_CUSTOMER_ID]
  WHERE h.[ACTIVATED_CUSTOMER_ID] NOT IN (select customer_id from [HTT_CELLUPHONE_TRANSACTIONS] WITH (NOLOCK) where TRANS_TYPE='AC')
  AND o.BRAND_ID != 3
  AND h.INITIAL_COS_ID IN ('98280','98283','98274','98332','98288','98284','98275','98314','98285','98333','98276','98279','98293','98289','98290','98291','98294','98292','98320','98321','98322','98323','98324','98325','98326','98327','98328','98329','98330','98331') order by ACTIVATED_DATE";

    $this->assertEquals( $sampleSQL , $sql );
  }

  public function test_celluphone_transaction_log_query()
  {
    $sql = \celluphone_transaction_log_query();

    $sampleSQL = "insert into [HTT_CELLUPHONE_TRANSACTIONS] ([CUSTOMER_ID], TRANSACTION_DATE, PLAN_CODE, TRANS_TYPE, CHARGE_AMOUNT, PAYMENT_TYPE, IS_COMMISSIONABLE, BILL_REF_ID) 
select h.CUSTOMER_ID, [TRANSACTION_DATE], case h.[COS_ID]
WHEN '98280' THEN 'L19'
WHEN '98283' THEN 'L24'
WHEN '98274' THEN 'L29'
WHEN '98332' THEN 'S29'
WHEN '98288' THEN 'M29'
WHEN '98284' THEN 'L34'
WHEN '98275' THEN 'L39'
WHEN '98314' THEN 'D39'
WHEN '98285' THEN 'L44'
WHEN '98333' THEN 'S44'
WHEN '98276' THEN 'L49'
WHEN '98279' THEN 'L59'
WHEN '98293' THEN 'UV20'
WHEN '98289' THEN 'UV30'
WHEN '98290' THEN 'UV35'
WHEN '98291' THEN 'UV45'
WHEN '98294' THEN 'UV50'
WHEN '98292' THEN 'UV55'
WHEN '98320' THEN 'M01S'
WHEN '98321' THEN 'M03S'
WHEN '98322' THEN 'M06S'
WHEN '98323' THEN 'M12S'
WHEN '98324' THEN 'M01M'
WHEN '98325' THEN 'M03M'
WHEN '98326' THEN 'M06M'
WHEN '98327' THEN 'M12M'
WHEN '98328' THEN 'M01L'
WHEN '98329' THEN 'M03L'
WHEN '98330' THEN 'M06L'
WHEN '98331' THEN 'M12L'
END as PLAN_CODE, 'BL' as TRANS_TYPE,
case
when COMMISSIONABLE_CHARGE_AMOUNT IS NOT NULL THEN COMMISSIONABLE_CHARGE_AMOUNT
else CHARGE_AMOUNT
end as CHARGE_AMOUNT,
case SOURCE
  WHEN 'EPAY' THEN 'EP'
  WHEN 'WEBCC' THEN 'CC'
  WHEN 'WEBPIN' THEN 'PN'
  WHEN 'PHONEPIN' THEN 'PN'
  WHEN 'INCOMM' THEN 'IN'
  WHEN 'CSCOURTESY' THEN 'CS'
  WHEN 'ORANGE_SIM' THEN 'IO'
  WHEN 'QPAY' THEN 'QP'
  WHEN 'TCETRA' THEN 'TC'
END as PAYMENT_TYPE, 
IS_COMMISSIONABLE, h.[TRANSACTION_ID] as BILL_REF_ID
from [HTT_BILLING_HISTORY] h
JOIN htt_customers_overlay_ultra o
ON   o.customer_id = h.customer_id
where ([IS_COMMISSIONABLE] =1 or SOURCE='ORANGE_SIM') and ENTRY_TYPE='LOAD' and SOURCE IN ('EPAY','WEBCC','WEBPIN','PHONEPIN','CSPIN','CSCOURTESY','ORANGE_SIM','INCOMM','QPAY', 'TCETRA')
AND o.BRAND_ID != 3
and h.COS_ID in ('98280','98283','98274','98332','98288','98284','98275','98314','98285','98333','98276','98279','98293','98289','98290','98291','98294','98292','98320','98321','98322','98323','98324','98325','98326','98327','98328','98329','98330','98331') and h.[TRANSACTION_ID] not in  (select BILL_REF_ID from HTT_CELLUPHONE_TRANSACTIONS WITH (NOLOCK) where BILL_REF_ID is not null AND [trans_type] = 'BL')
order by TRANSACTION_DATE";

    $this->assertEquals( $sampleSQL , $sql );
  }

  public function test_celluphone_qualified_spend_query()
  {
    $sql = \celluphone_qualified_spend_query();

    $sampleSQL = "INSERT INTO HTT_CELLUPHONE_TRANSACTIONS
      (CUSTOMER_ID, TRANSACTION_DATE, PLAN_CODE, TRANS_TYPE, CHARGE_AMOUNT, PAYMENT_TYPE, IS_COMMISSIONABLE, BILL_REF_ID) 
    SELECT
      h.CUSTOMER_ID,
      h.TRANSACTION_DATE,
      CASE h.COS_ID
WHEN '98280' THEN 'L19'
WHEN '98283' THEN 'L24'
WHEN '98274' THEN 'L29'
WHEN '98332' THEN 'S29'
WHEN '98288' THEN 'M29'
WHEN '98284' THEN 'L34'
WHEN '98275' THEN 'L39'
WHEN '98314' THEN 'D39'
WHEN '98285' THEN 'L44'
WHEN '98333' THEN 'S44'
WHEN '98276' THEN 'L49'
WHEN '98279' THEN 'L59'
WHEN '98293' THEN 'UV20'
WHEN '98289' THEN 'UV30'
WHEN '98290' THEN 'UV35'
WHEN '98291' THEN 'UV45'
WHEN '98294' THEN 'UV50'
WHEN '98292' THEN 'UV55'
WHEN '98320' THEN 'M01S'
WHEN '98321' THEN 'M03S'
WHEN '98322' THEN 'M06S'
WHEN '98323' THEN 'M12S'
WHEN '98324' THEN 'M01M'
WHEN '98325' THEN 'M03M'
WHEN '98326' THEN 'M06M'
WHEN '98327' THEN 'M12M'
WHEN '98328' THEN 'M01L'
WHEN '98329' THEN 'M03L'
WHEN '98330' THEN 'M06L'
WHEN '98331' THEN 'M12L'
ELSE 'L00'
        END AS PLAN_CODE,
      'QS' AS TRANS_TYPE,
      CASE COMMISSIONABLE_CHARGE_AMOUNT WHEN 0 THEN CASE CHARGE_AMOUNT WHEN 0 THEN -BALANCE_CHANGE ELSE CHARGE_AMOUNT END ELSE COMMISSIONABLE_CHARGE_AMOUNT END AS CHARGE_AMOUNT,
      'NA' AS PAYMENT_TYPE, 
      1 AS IS_COMMISSIONABLE,
      h.TRANSACTION_ID AS BILL_REF_ID
    FROM HTT_BILLING_HISTORY h
    JOIN htt_customers_overlay_ultra o
    ON   o.customer_id = h.customer_id
    WHERE
      h.TRANSACTION_DATE >= '11/1/2014 8:00 AM'
      AND h.ENTRY_TYPE = 'SPEND'
      AND (h.DETAIL IN ('Monthly Renewal', 'Plan Change Renewal', 'Activation', 'First Monthly Fee', 'portal__ApplyBoltOn', 'dealerportal__ApplyBoltOn', 'customercare__ApplyBoltOn', 'Ultra\Lib\BoltOn\addBoltOnsRecurring', 'Reactivation') OR h.reference = 'DELTA_PAYMENT')
      AND h.TRANSACTION_ID NOT IN (SELECT BILL_REF_ID FROM HTT_CELLUPHONE_TRANSACTIONS WITH (NOLOCK) WHERE BILL_REF_ID IS NOT NULL AND TRANS_TYPE = 'QS')
      AND o.BRAND_ID != 3
    ORDER BY TRANSACTION_DATE";

    $this->assertEquals( $sampleSQL , $sql );
  }

  public function test_celluphone_transaction_log_customercare_query()
  {
    $sql = \celluphone_transaction_log_customercare_query();

    $sampleSQL = "with commissionable_transacts as (
select h.[TRANSACTION_ID], row_number() over (partition by h.[CUSTOMER_ID] order by [TRANSACTION_ID] asc) as rn
from HTT_BILLING_HISTORY h join [ACCOUNTS] a on  h.[CUSTOMER_ID]=a.customer_id
where [SOURCE]='CSCOURTESY'
and detail in ('customercare__AddCourtesyCash','customercare__AddCourtesyStoredValue')
and charge_amount > 18
and h.[TRANSACTION_DATE] between a.[CREATION_DATE_TIME] and dateadd(d,15,a.[CREATION_DATE_TIME]) 
and h.COS_ID in ('98280','98283','98274','98332','98288','98284','98275','98314','98285','98333','98276','98279','98293','98289','98290','98291','98294','98292','98320','98321','98322','98323','98324','98325','98326','98327','98328','98329','98330','98331')
)
insert into [HTT_CELLUPHONE_TRANSACTIONS] ([CUSTOMER_ID], TRANSACTION_DATE, PLAN_CODE, TRANS_TYPE, CHARGE_AMOUNT, PAYMENT_TYPE, IS_COMMISSIONABLE, BILL_REF_ID)
select  h.[CUSTOMER_ID], h.[TRANSACTION_DATE], case h.[COS_ID]
WHEN '98280' THEN 'L19'
WHEN '98283' THEN 'L24'
WHEN '98274' THEN 'L29'
WHEN '98332' THEN 'S29'
WHEN '98288' THEN 'M29'
WHEN '98284' THEN 'L34'
WHEN '98275' THEN 'L39'
WHEN '98314' THEN 'D39'
WHEN '98285' THEN 'L44'
WHEN '98333' THEN 'S44'
WHEN '98276' THEN 'L49'
WHEN '98279' THEN 'L59'
WHEN '98293' THEN 'UV20'
WHEN '98289' THEN 'UV30'
WHEN '98290' THEN 'UV35'
WHEN '98291' THEN 'UV45'
WHEN '98294' THEN 'UV50'
WHEN '98292' THEN 'UV55'
WHEN '98320' THEN 'M01S'
WHEN '98321' THEN 'M03S'
WHEN '98322' THEN 'M06S'
WHEN '98323' THEN 'M12S'
WHEN '98324' THEN 'M01M'
WHEN '98325' THEN 'M03M'
WHEN '98326' THEN 'M06M'
WHEN '98327' THEN 'M12M'
WHEN '98328' THEN 'M01L'
WHEN '98329' THEN 'M03L'
WHEN '98330' THEN 'M06L'
WHEN '98331' THEN 'M12L'
END as PLAN_CODE,
'BL' as TRANS_TYPE, 
case 
when h.charge_amount>59 then 59
when h.charge_amount<0 then 0
else h.charge_amount
end as CHARGE_AMOUNT,
'CS' as PAYMENT_TYPE, 1 as IS_COMMISSIONABLE, h.[TRANSACTION_ID] as BILL_REF_ID
 from commissionable_transacts t
join  HTT_BILLING_HISTORY h on t.transaction_id=h.[TRANSACTION_ID]
JOIN htt_customers_overlay_ultra o
ON   o.customer_id = h.customer_id
where t.rn=1
and h.[TRANSACTION_ID] not in  (select BILL_REF_ID from HTT_CELLUPHONE_TRANSACTIONS WITH (NOLOCK) where BILL_REF_ID is not null)
AND o.BRAND_ID != 3
order by TRANSACTION_DATE";

    $this->assertEquals( $sampleSQL , $sql );
  }

  public function test_celluphone_changeplan_log_query()
  {
    $sql = \celluphone_changeplan_log_query();

    $sampleSQL = "declare @start_dt datetime = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 1, 0)
declare @end_dt datetime = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0);

--CTE_HPT_GA (sorted asc to grab initial plan tracker record) includes all plan_tracker records for each sub as long as the plan started on/before the reporting end date
with cte_hpt_ga as
(select customer_id, cos_id, period, plan_started, plan_expires, row_number() over (partition by customer_id order by period, plan_updated, plan_expires, plan_started) as 'RN'
 from htt_plan_tracker where plan_started < @end_dt and cos_id in
('98280','98283','98274','98332','98288','98284','98275','98314','98285','98333','98276','98279','98293','98289','98290','98291','98294','98292','98320','98321','98322','98323','98324','98325','98326','98327','98328','98329','98330','98331')
  and datediff(dd, plan_started, plan_expires) >= 30)
 
--CTE_HPT (sorted descending to grab latest plan tracker record)
,cte_hpt as
(select customer_id, cos_id, period, plan_started, plan_expires, row_number() over (partition by customer_id order by period desc, plan_updated desc, plan_expires desc, plan_started desc) as 'RN'
 from htt_plan_tracker where plan_started >= @start_dt and plan_started < @end_dt and cos_id in ('98280','98280','98283','98274','98332','98288','98284','98275','98314','98285','98333','98276','98279','98293','98289','98290','98291','98294','98292','98320','98321','98322','98323','98324','98325','98326','98327','98328','98329','98330','98331') and datediff(dd, plan_started, plan_expires) >= 30 and period = 1)

insert into [HTT_CELLUPHONE_TRANSACTIONS] ([CUSTOMER_ID], TRANSACTION_DATE, PLAN_CODE, TRANS_TYPE, CHARGE_AMOUNT, PAYMENT_TYPE, IS_COMMISSIONABLE) 
select u.[customer_id] as CUSTOMER_ID,
              getutcdate() as TRANSACTION_DATE,
              case hpt.cos_id
WHEN '98280' THEN 'L19'
WHEN '98283' THEN 'L24'
WHEN '98274' THEN 'L29'
WHEN '98332' THEN 'S29'
WHEN '98288' THEN 'M29'
WHEN '98284' THEN 'L34'
WHEN '98275' THEN 'L39'
WHEN '98314' THEN 'D39'
WHEN '98285' THEN 'L44'
WHEN '98333' THEN 'S44'
WHEN '98276' THEN 'L49'
WHEN '98279' THEN 'L59'
WHEN '98293' THEN 'UV20'
WHEN '98289' THEN 'UV30'
WHEN '98290' THEN 'UV35'
WHEN '98291' THEN 'UV45'
WHEN '98294' THEN 'UV50'
WHEN '98292' THEN 'UV55'
WHEN '98320' THEN 'M01S'
WHEN '98321' THEN 'M03S'
WHEN '98322' THEN 'M06S'
WHEN '98323' THEN 'M12S'
WHEN '98324' THEN 'M01M'
WHEN '98325' THEN 'M03M'
WHEN '98326' THEN 'M06M'
WHEN '98327' THEN 'M12M'
WHEN '98328' THEN 'M01L'
WHEN '98329' THEN 'M03L'
WHEN '98330' THEN 'M06L'
WHEN '98331' THEN 'M12L'
END as PLAN_CODE,
              'CP' as TRANS_TYPE,
              0 as CHARGE_AMOUNT,
              'CP' as PAYMENT_TYPE,
              case when s.[PRODUCT_TYPE] = 'ORANGE' THEN 0 else 1 end as IS_COMMISSIONABLE
from [htt_customers_overlay_ultra] u join [ACCOUNTS] a on u.[customer_id]=a.[CUSTOMER_ID]
join [HTT_INVENTORY_SIM] s on u.[ACTIVATION_ICCID_FULL]=s.[ICCID_FULL]
inner join cte_hpt_ga ga on u.customer_id = ga.customer_id and ga.rn = 1 and ga.plan_started >= @start_dt
inner join cte_hpt hpt on ga.customer_id = hpt.customer_id and hpt.rn = 1 and ga.cos_id != hpt.cos_id
where u.BRAND_ID != 3
order by u.[customer_id]
-- and a.COS_ID 
-- order by ACTIVATED_DATE";

    $this->assertEquals( $sampleSQL , $sql );
  }
}


<?php
namespace Ultra\UltraConfig
{
  function redisClusterInfo(){ return []; }
  function redisHost(){ return []; }
  function channelsDefaultReplyTimeoutSeconds() { return 90; }
}

namespace
{
  define('STATE_ACTIVE', 'Active');
  define('STATE_CANCELLED', 'Cancelled');
  define('STATE_NEUTRAL', 'Neutral');
  define('STATE_PORT_IN_DENIED', 'Port-In Denied');
  define('STATE_PORT_IN_REQUESTED', 'Port-In Requested');
  define('STATE_PRE_FUNDED', 'Pre-Funded');
  define('STATE_PROMO_UNUSED', 'Promo Unused');
  define('STATE_PROVISIONED', 'Provisioned');
  define('STATE_SUSPENDED', 'Suspended');
  define('FINAL_STATE_COMPLETE', 'Completed'); // MVNO-2302

  define('COSID_ULTRA_NINETEEN', 98280);
  define('COSID_ULTRA_TWENTY_NINE', 98274);
  define('COSID_ULTRA_THIRTY_FOUR', 98284);
  define('COSID_ULTRA_THIRTY_NINE', 98275);
  define('COSID_ULTRA_FORTY_FOUR', 98285);
  define('COSID_ULTRA_FORTY_NINE', 98276);
  define('COSID_ULTRA_FIFTY_NINE', 98279);
  define('COSID_ULTRA_MULTI_TWENTY_NINE', 98288);
  define('COSID_ULTRA_STWENTY_NINE', 98332);
  define('COSID_ULTRA_SFORTY_FOUR', 98333);

  function teldata_change_db() { return ''; }

  function dlog($options, $message)
  {
    $args = func_get_args();

    $options = array_shift($args);
    $facility = '';

    // string to be appended to the log
    $output = FALSE;

    if (! is_array($options))
      $options = array( 'f' => $options );

    $facility = isset($options['f']) ? $options['f'] : '';
    $brief = isset($options['brief']) || in_array('brief', $options);

    // should we append $output to the log ?
    if (1)
    {
      $trace = debug_backtrace();
      $preface = "main::() ";
      if ($trace && count($trace) > 1)
      {
        $caller = $trace[1];
        $preface = (isset($caller['class']) ? "{$caller['class']}::" : '') . $caller['function'] . "(): ";
      }

      if (is_array($args[0]) || is_object($args[0]))
        $args[0] = json_encode($args[0]);

      if (is_null($args[0]))
        $args[0] = enclose_JSON($args[0]);

      global $request_id;
      $request_prefix = '';

      if (isset($request_id))
        $request_prefix = $request_id . " ";

      $args[0] = $request_prefix . "[" . getmypid() . "] " . $preface . $args[0];

      /* error_log("dlog() calling sprintf() with parameters " . json_encode($args)); */
      if (count($args) > 1)
      {
        // encode any arrays with json_encode
        $encoded_args = array(
          array_shift($args)
        );

        while (count($args) > 0)
        {
          $arg = array_shift($args);

          if (is_array($arg) || is_object($arg))
          {
            if ( 1 == 2 )
              $arg = \toKeyValString($arg);
            else
              $arg = json_encode($arg);
          }

          if (is_null($arg))
            $arg = enclose_JSON($arg);

          if ($brief && strlen($arg) > 50)
            $arg = substr($arg, 0, 22) . '...' . substr($arg, - 22);

          $encoded_args[] = $arg;
        }

        $output = call_user_func_array('sprintf', $encoded_args);
      }
      else
        $output = $args[0];
    }

    // PROD-1782: preserve application time zone and restore default
    $appTZ = date_default_timezone_get();
    if ($appTZ != 'America/Los_Angeles')
      date_default_timezone_set('America/Los_Angeles');

    try
    {
      if ( $output )
      {
        if ( 1 ) // Logger is enabled
        {
          if ( empty( $ultra_logger_object ) || ! $ultra_logger_object )
            $ultra_logger_object = new UltraLogger(getReferringIPAddress(), FALSE);

          $ultra_logger_object->logDebug($output);
        }
        else // $loggerFlag 0 or not set: Log to error_log()
          error_log($output);
      }
    }
    catch (Exception $e)
    {
      error_log("FATAL LOGGING ERROR " . $e->getMessage() . " Original Error message: " . PHP_EOL . $output);
    }

    // PROD-1782: restore application time zone if needed
    if ($appTZ != 'America/Los_Angeles')
      date_default_timezone_set($appTZ);
  }

  function getReferringIPAddress()
  {
    $IP = '';

    if (getenv('HTTP_CLIENT_IP'))
      $IP = getenv('HTTP_CLIENT_IP');
    elseif (getenv('HTTP_X_FORWARDED_FOR'))
      $IP = getenv('HTTP_X_FORWARDED_FOR');
    elseif (getenv('HTTP_X_FORWARDED'))
      $IP = getenv('HTTP_X_FORWARDED');
    elseif (getenv('HTTP_FORWARDED_FOR'))
      $IP = getenv('HTTP_FORWARDED_FOR');
    elseif (getenv('HTTP_FORWARDED'))
      $IP = getenv('HTTP_FORWARDED');
    else
      $IP = "";

    return $IP;
  }

  function getNewActionUUID($namespace = '')
  {
    return getNewUUID('AX', $namespace);
  }

  function getNewUUID($prefix, $namespace = '', $parentheses=TRUE)
  {
    $uid = uniqid("", true);
    $data = $namespace;
    $data .= $_SERVER['REQUEST_TIME'];

    if (isset($_SERVER['HTTP_USER_AGENT']))
    {
      $data .= $_SERVER['HTTP_USER_AGENT'];
    }
    if (isset($_SERVER['REMOTE_ADDR']))
    {
      $data .= $_SERVER['REMOTE_ADDR'];
    }
    if (isset($_SERVER['REMOTE_PORT']))
    {
      $data .= $_SERVER['REMOTE_PORT'];
    }

    $hash = strtoupper(hash('ripemd128', $uid . md5($data)));

    $uuid = substr($hash, 0, 16) . '-' . substr($hash, 16, 32);

    return ( $parentheses )
      ?
      "{" . $prefix . "-" . $uuid . "}"
      :
      $prefix . "-" . $uuid
      ;
  }

  function sendSMSE911AddressUpdated() {}

  function logit() {}

  function build_log_preface( $trace )
  {
    $preface = "main::() ";
    if ($trace && count($trace) > 1)
    {
      $caller = $trace[1];
      $preface = (isset($caller['class']) ? "{$caller['class']}::" : '') . $caller['function'] . "(): ";
    }

    return $preface;
  }

  function logDebug( $message )
  {
    dlog('DEBUG', build_log_preface( debug_backtrace() ) . $message);
    return NULL;
  }

  function logError( $message )
  {
    dlog('', build_log_preface( debug_backtrace() ) . $message);
    return NULL;
  }

  function timeUTC()
  {
    $oldTZ = date_default_timezone_get();

    date_default_timezone_set('UTC');
    $ts = time();

    date_default_timezone_set($oldTZ);

    return $ts;
  }
}
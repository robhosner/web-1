<?php
namespace unit_tests;

use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\PaymentAPI;

class PaymentAPITest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var PaymentAPI
   */
  private $class;
  private $config;
  private $paymentData = [
    'orderID' => 1,
    'customerID' => 1,
    'brandID' => 1,
    'token' => 1,
    'expDate' => 1299,
    'amount' => 1,
    'unneeded' => 12345,
  ];

  private $verifyData = [
    'orderID' => 1,
    'brandID' => 1,
    'token' => 1,
    'expDate' => 1299,
    'unneeded' => 12345,
  ];

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getPaymentAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test', 'jwtSecret' => 'SECRET']));

    $this->class = $this->getMockBuilder(PaymentAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['post'])
      ->getMock();
  }

  public function testChargeMissingRequiredParam()
  {
    $this->paymentData['amount'] = 0;
    $result = $this->class->charge(1, $this->paymentData);
    $this->assertFalse($result->is_success());
    $this->assertContains('Missing field amount', $result->get_errors());
  }

  public function testChargeFails()
  {
    $this->paymentData['amount'] = 1;

    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
        'curl_errno' => 123,
        'body' => '{"errors": ["errr]}',
      ]));

    $result = $this->class->charge(1, $this->paymentData);
    $this->assertFalse($result->is_success());
    $this->assertContains('(retry) Payment API curl failed - 123', $result->get_errors());
  }

  public function testChargeErrors()
  {
    $this->paymentData['amount'] = 1;

    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
        'body' => '{"errors": ["errr"]}',
      ]));

    $result = $this->class->charge(1, $this->paymentData);
    $this->assertFalse($result->is_success());
    $this->assertContains('errr', $result->get_errors());
  }

  public function testChargeUnknownError()
  {
    $this->paymentData['amount'] = 1;

    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
        'body' => '{"errors": [errr]}',
      ]));

    $result = $this->class->charge(1, $this->paymentData);
    $this->assertFalse($result->is_success());
    $this->assertContains('Unknown error', $result->get_errors());
  }

  public function testChargeMissingTransactionIDInResponse()
  {
    $this->paymentData['amount'] = 1;

    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{}',
      ]));

    $result = $this->class->charge(1, $this->paymentData);
    $this->assertFalse($result->is_success());
    $this->assertContains('Missing transactionID in response', $result->get_errors());
  }

  public function testChargeSucceeds()
  {
    $this->paymentData['amount'] = 1;

    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"transactionID": 123}',
      ]));

    $result = $this->class->charge(1, $this->paymentData);
    $this->assertTrue($result->is_success());
    $this->assertEquals(['transaction_id' => 123], $result->data_array);
  }

  public function testVerifyRequiredParam()
  {
    unset($this->verifyData['brandID']);
    $result = $this->class->verify(1, $this->verifyData);
    $this->assertFalse($result->is_success());
    $this->assertContains('Missing field brandID', $result->get_errors());
  }

  public function testVerifyErrors()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 400,
        'body' => '{"errors": ["errr"]}',
      ]));

    $result = $this->class->verify(1, $this->verifyData);
    $this->assertFalse($result->is_success());
    $this->assertContains('errr', $result->get_errors());
  }

  public function testVerifyMissingResponseBody()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '',
      ]));

    $result = $this->class->verify(1, $this->verifyData);
    $this->assertFalse($result->is_success());
    $this->assertContains('Missing response body', $result->get_errors());
  }

  public function testVerifyMissingResponseToken()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"stuff": 123}',
      ]));

    $result = $this->class->verify(1, $this->verifyData);
    $this->assertFalse($result->is_success());
    $this->assertContains('Missing response token', $result->get_errors());
  }

  public function testVerifySucceeds()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{"token": 123}',
      ]));

    $result = $this->class->verify(1, $this->verifyData);
    $this->assertTrue($result->is_success());
    $this->assertEquals(['token' => 123], $result->data_array);
  }
}

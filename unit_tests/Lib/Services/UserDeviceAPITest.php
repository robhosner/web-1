<?php
namespace unit_tests;

use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\UserDeviceAPI;

class UserDeviceAPITest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var UserDeviceAPI
   */
  private $class;
  private $config;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getUserDeviceAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->class = $this->getMockBuilder(UserDeviceAPI::class)
      ->setConstructorArgs([
        $this->config,
      ])
      ->setMethods(['get', 'post', 'put', 'delete'])
      ->getMock();
  }

  public function testMissingConfiguration()
  {
    $this->setExpectedException(\Exception::class);
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getUserDeviceAPIConfig')
      ->will($this->returnValue([]));
    new UserDeviceAPI($this->config);
  }

  public function testSendApnSettingsCurlError()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
        'curl_errno' => 1
      ]));

    $result = $this->class->sendApnSettings(1);
    $this->assertFalse($result->is_success());
    $this->assertContains('UserDevice API curl failed - 1', $result->get_errors());
  }

  public function testSendApnSettingsErrors()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 500,
        'body' => '{"errors": ["errrr"]}'
      ]));

    $result = $this->class->sendApnSettings(1);
    $this->assertFalse($result->is_success());
    $this->assertContains('errrr', $result->get_errors());
  }

  public function testSendApnSettingsSucceeds()
  {
    $this->class->expects($this->any())
      ->method('post')
      ->will($this->returnValue([
        'code' => 200,
        'body' => '{}'
      ]));

    $result = $this->class->sendApnSettings(1);
    $this->assertTrue($result->is_success());
  }
}

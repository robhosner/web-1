<?php

use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\CustomerCare\ValidateGBASim;
use Ultra\Lib\ApiErrors;
use Ultra\Sims\Repositories\Mssql\SimRepository;
use Ultra\Sims\Sim;

class ValidateGBASimTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var ValidateGBASim
   */
  public  $api;
  private $customerRepo;
  private $simRepo;

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->simRepo = $this->getMock(SimRepository::class);

    $this->api = new ValidateGBASim($this->customerRepo, $this->simRepo);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues(['customer_id' => 0]);
    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(0, ['current_iccid_full'])
      ->will($this->returnValue(false));

    $result = $this->api->customercare__ValidateGBASim();
    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testCustomerDoesNotHaveAValidGBASim()
  {
    $this->api->setInputValues(['customer_id' => 1]);

    $customer = new stdClass();
    $customer->customer_id = 1;
    $customer->current_iccid_full = 1234577;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(1, ['current_iccid_full'])
      ->will($this->returnValue($customer));

    $sim = new Sim([
      'iccid_number' => 1234,
      'brand_id' => 1,
      'tmoprofilesku' => 123456789,
      'iccid_full' => 1234,
    ]);

    $this->simRepo->expects($this->any())
      ->method('getSimInventoryAndBatchInfoByIccid')
      ->with($customer->current_iccid_full )
      ->will($this->returnValue($sim));

    $result = $this->api->customercare__ValidateGBASim();
    $this->assertFalse($result->data_array['valid']);
  }
}

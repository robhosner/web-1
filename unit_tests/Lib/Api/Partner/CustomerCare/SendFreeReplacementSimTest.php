<?php
namespace unit_tests;

use Result;
use Ultra\Configuration\Configuration;
use Ultra\Configuration\Repositories\Cfengine\ConfigurationRepository;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\CustomerCare\CalculateTaxesAndFees;
use Ultra\Lib\Api\Partner\CustomerCare\SendFreeReplacementSim;
use Ultra\Lib\ApiErrors;
use Ultra\Payments\TaxCalculator\SureTax;
use Ultra\Sims\Inventory;
use Ultra\Taxes\TaxesAndFeesCalculator;
use Ultra\Taxes\TransactionTypeCodeConfig;

class SendFreeReplacementSimTest extends \PHPUnit_Framework_TestCase
{
  public $inventory;
  public $params = [
    'customer_id' => 2345678896,
  ];

  /**
   * @var SendFreeReplacementSim
   */
  public $api;

  public function setUp()
  {
    $this->inventory = $this->getMock(Inventory::class);
    $this->api = new SendFreeReplacementSim($this->inventory);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues($this->params);
  }

  public function testShipSimFails()
  {
    $this->inventory->expects($this->any())
      ->method('shipSim')
      ->will($this->returnValue(['success' => false, 'error' => 'boo', 'code' => 123]));

    $result = $this->api->customercare__SendFreeReplacementSim();

//    print_r($result);

    $this->assertContains('123', $result->data_array['error_codes']);
    $this->assertContains('ERR_API_INTERNAL: boo', $result->get_errors());
  }

  public function testShipSimSucceeds()
  {
    $this->inventory->expects($this->any())
      ->method('shipSim')
      ->will($this->returnValue(['success' => true]));

    $result = $this->api->customercare__SendFreeReplacementSim();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

<?php

use Ultra\Configuration\Configuration;
use Ultra\Dealers\Repositories\Mssql\DealersRepository;
use Ultra\Lib\Api\Partner\Ultrainfo\GetLocations;
use Ultra\Lib\ApiErrors;

class GetLocationsTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var GetLocations
   */
  private $api;
  
  /**
   * @var DealersRepository
   */
  private $dealerRepo;
  
  private $defaultParams = [
    'latitude'      => 33.7472559, // 18.124278303232824,
    'longitude'     => -118.0447211, // -66.16442878515626,
    'radius'        => 100,
    'max_entries'   => 50,
    'location_type' => 'DEALER',
  ];
  
  private $apiParams = [];
  
  public function setUp()
  {
    // API setup
    $this->dealerRepo = $this->getMock(DealersRepository::class);
    $this->config = $this->getMock(Configuration::class);
    $this->api = $this->getMockBuilder(GetLocations::class)
      ->setConstructorArgs([$this->dealerRepo, $this->config])
      ->setMethods(['getDealerLocatorThresholds', 'getBrandFromShortName'])
      ->getMock();
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->apiParams = $this->defaultParams;
    $this->apiParams['brand'] = 'ULTRA';
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);

    $thresholds = [
      'LOCATOR_ACTIVATION_THRESHOLD' => 5,
      'LOCATOR_MILE_THRESHOLD' => 5,
      'LOCATOR_DAYS_THRESHOLD' => 30
    ];
    
    $this->config->expects($this->any())
      ->method('getDealerLocatorThresholds')
      ->will($this->returnValue($thresholds));

    $this->config->expects($this->any())
      ->method('getBrandFromShortName')
      ->will($this->returnValue(['id' => 1]));
  }

  public function testNoLocationsFound()
  {
    $this->defaultParams['brand_id'] = 1;

    $this->dealerRepo->expects($this->any())
      ->method('getLocations')
      ->with($this->defaultParams)
      ->will($this->returnValue([]));
    
    $this->api->setInputValues($this->apiParams);
    $result = $this->api->ultrainfo__GetLocations();
    
//    print_r($result);
    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testTopUpResults()
  {
    $location1 = new stdClass();
    $location1->LOCATION_ID = 6208;
    $location1->RETAILER_NAME = "Video Los Primitos";
    $location1->ADDRESS1 = "3256 Lincoln Ave";
    $location1->ADDRESS2 = "";
    $location1->CITY = "Anaheim";
    $location1->STATE = "CA";
    $location1->ZIPCODE = "92801";
    $location1->RETAILER_CONTACT = "STEFFANNY FABIAN";
    $location1->RETAILER_PHONE_NUMBER = "7148689530";
    $location1->RETAILER_FAX_NUMBER = "";
    $location1->RETAILER_EMAIL_ADDRESS = "";
    $location1->LOCATION_TYPE = "DEALER";
    $location1->ACTIVE = 1;
    $location1->EXPIRES_DATE = "";
    $location1->LOCATION_SOURCE = "PORTAL_DEALER";
    $location1->latitude = 33.83181;
    $location1->longitude = -118.00379;
    $location1->location_distance = 30986.39258053;
    $location1->LOCATION_DISTANCE_MILES = 19.254099556669;
    $location1->ACTIVATIONS_COUNT = 5;

    $location2 = new stdClass();
    $location2->LOCATION_ID = 6208;
    $location2->RETAILER_NAME = "Video Los Primitos";
    $location2->ADDRESS1 = "3256 Lincoln Ave";
    $location2->ADDRESS2 = "";
    $location2->CITY = "Anaheim";
    $location2->STATE = "CA";
    $location2->ZIPCODE = "92801";
    $location2->RETAILER_CONTACT = "STEFFANNY FABIAN";
    $location2->RETAILER_PHONE_NUMBER = "7148689530";
    $location2->RETAILER_FAX_NUMBER = "";
    $location2->RETAILER_EMAIL_ADDRESS = "";
    $location2->LOCATION_TYPE = "TOPUP";
    $location2->ACTIVE = 1;
    $location2->EXPIRES_DATE = "";
    $location2->LOCATION_SOURCE = "PORTAL_DEALER";
    $location2->latitude = 33.83181;
    $location2->longitude = -118.00379;
    $location2->location_distance = 30986.39258053;
    $location2->LOCATION_DISTANCE_MILES = 19.254099556669;
    $location2->ACTIVATIONS_COUNT = 0;

    $this->defaultParams['brand_id'] = 1;
    $this->defaultParams['location_type'] = 'TOPUP';
    $this->apiParams['location_type'] = 'TOPUP';

    $this->dealerRepo->expects($this->any())
      ->method('getLocations')
      ->with($this->defaultParams)
      ->will($this->returnValue([$location1, $location2]));

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->ultrainfo__GetLocations();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['location_list']);
  }

  public function testFoundLocations()
  {
    $location1 = new stdClass();
    $location1->LOCATION_ID = 6208;
    $location1->RETAILER_NAME = "Video Los Primitos";
    $location1->ADDRESS1 = "3256 Lincoln Ave";
    $location1->ADDRESS2 = "";
    $location1->CITY = "Anaheim";
    $location1->STATE = "CA";
    $location1->ZIPCODE = "92801";
    $location1->RETAILER_CONTACT = "STEFFANNY FABIAN";
    $location1->RETAILER_PHONE_NUMBER = "7148689530";
    $location1->RETAILER_FAX_NUMBER = "";
    $location1->RETAILER_EMAIL_ADDRESS = "";
    $location1->LOCATION_TYPE = "DEALER";
    $location1->ACTIVE = 1;
    $location1->EXPIRES_DATE = "";
    $location1->LOCATION_SOURCE = "PORTAL_DEALER";
    $location1->latitude = 33.83181;
    $location1->longitude = -118.00379;
    $location1->location_distance = 30986.39258053;
    $location1->LOCATION_DISTANCE_MILES = 1;
    $location1->ACTIVATIONS_COUNT = 5;

    $location2 = new stdClass();
    $location2->LOCATION_ID = 6208;
    $location2->RETAILER_NAME = "Video Los Primitos";
    $location2->ADDRESS1 = "3256 Lincoln Ave";
    $location2->ADDRESS2 = "";
    $location2->CITY = "Anaheim";
    $location2->STATE = "CA";
    $location2->ZIPCODE = "92801";
    $location2->RETAILER_CONTACT = "STEFFANNY FABIAN";
    $location2->RETAILER_PHONE_NUMBER = "7148689530";
    $location2->RETAILER_FAX_NUMBER = "";
    $location2->RETAILER_EMAIL_ADDRESS = "";
    $location2->LOCATION_TYPE = "DEALER";
    $location2->ACTIVE = 1;
    $location2->EXPIRES_DATE = "";
    $location2->LOCATION_SOURCE = "PORTAL_DEALER";
    $location2->latitude = 33.83181;
    $location2->longitude = -118.00379;
    $location2->location_distance = 30986.39258053;
    $location2->LOCATION_DISTANCE_MILES = 1;
    $location2->ACTIVATIONS_COUNT = 0;

    $location3 = new stdClass();
    $location3->LOCATION_ID = 6208;
    $location3->RETAILER_NAME = "Video Los Primitos";
    $location3->ADDRESS1 = "3256 Lincoln Ave";
    $location3->ADDRESS2 = "";
    $location3->CITY = "Anaheim";
    $location3->STATE = "CA";
    $location3->ZIPCODE = "92801";
    $location3->RETAILER_CONTACT = "STEFFANNY FABIAN";
    $location3->RETAILER_PHONE_NUMBER = "7148689530";
    $location3->RETAILER_FAX_NUMBER = "";
    $location3->RETAILER_EMAIL_ADDRESS = "";
    $location3->LOCATION_TYPE = "DEALER";
    $location3->ACTIVE = 1;
    $location3->EXPIRES_DATE = "";
    $location3->LOCATION_SOURCE = "PORTAL_DEALER";
    $location3->latitude = 33.83181;
    $location3->longitude = -118.00379;
    $location3->location_distance = 30986.39258053;
    $location3->LOCATION_DISTANCE_MILES = 19.254099556669;
    $location3->ACTIVATIONS_COUNT = 6;

    $location4 = new stdClass();
    $location4->LOCATION_ID = 6208;
    $location4->RETAILER_NAME = "Video Los Primitos";
    $location4->ADDRESS1 = "3256 Lincoln Ave";
    $location4->ADDRESS2 = "";
    $location4->CITY = "Anaheim";
    $location4->STATE = "CA";
    $location4->ZIPCODE = "92801";
    $location4->RETAILER_CONTACT = "STEFFANNY FABIAN";
    $location4->RETAILER_PHONE_NUMBER = "7148689530";
    $location4->RETAILER_FAX_NUMBER = "";
    $location4->RETAILER_EMAIL_ADDRESS = "";
    $location4->LOCATION_TYPE = "DEALER";
    $location4->ACTIVE = 1;
    $location4->EXPIRES_DATE = "";
    $location4->LOCATION_SOURCE = "PORTAL_DEALER";
    $location4->latitude = 33.83181;
    $location4->longitude = -118.00379;
    $location4->location_distance = 30986.39258053;
    $location4->LOCATION_DISTANCE_MILES = 19.254099556669;
    $location4->ACTIVATIONS_COUNT = 0;
    
    $this->defaultParams['brand_id'] = 1;
    
    $this->dealerRepo->expects($this->any())
      ->method('getLocations')
      ->with($this->defaultParams)
      ->will($this->returnValue([$location1, $location2, $location3, $location4]));
    
    $this->api->setInputValues($this->apiParams);
    $result = $this->api->ultrainfo__GetLocations();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['location_list']);
  }
}
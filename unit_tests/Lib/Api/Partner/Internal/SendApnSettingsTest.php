<?php
namespace unit_tests\Lib\Api\Partner\Interal;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Internal\SendApnSettings;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\Services\UserDeviceAPI;
use Ultra\Messaging\Messenger;

class SendApnSettingsTest extends \PHPUnit_Framework_TestCase
{
  private $configuration;
  private $messenger;
  private $userDeviceApi;
  private $customerRepo;
  /**
   * @var SendApnSettings
   */
  private $api;

  public function setUp()
  {
    $this->configuration = $this->getMock(Configuration::class);
    $this->configuration->expects($this->any())
      ->method('getUserDeviceAPIConfig')
      ->will($this->returnValue(['host' => 'host', 'basepath' => 'path']));

    $this->userDeviceApi = $this->getMockBuilder(userDeviceApi::class)
      ->setConstructorArgs([$this->configuration])
      ->getMock();

    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->messenger = $this->getMock(Messenger::class);

    $this->api = new SendApnSettings($this->userDeviceApi, $this->messenger, $this->customerRepo);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues(['msisdn' => '123']);
  }

  public function testCustomerNotFound()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue(false));

    $result = $this->api->internal__SendApnSettings();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testSendApnSettingsFails()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue((object) ['CUSTOMER_ID' => 1]));

    $this->userDeviceApi->expects($this->any())
      ->method('sendApnSettings')
      ->will($this->returnValue(new \Result()));

    $result = $this->api->internal__SendApnSettings();

//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
    $this->assertEquals("Unable to send apn settings to 123", $result->data_array['message']);
  }

  public function testSendApnSettingsSucceedsSmsSucceeds()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue((object) ['CUSTOMER_ID' => 1]));

    $this->userDeviceApi->expects($this->any())
      ->method('sendApnSettings')
      ->will($this->returnValue(new \Result(null, true)));

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue(new \Result(null, true)));

    $result = $this->api->internal__SendApnSettings();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

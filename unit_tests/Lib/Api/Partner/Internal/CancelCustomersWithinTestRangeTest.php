<?php

use Ultra\Configuration\Repositories\Cfengine\ConfigurationRepository;
use Ultra\Lib\Api\Partner\Internal\CancelCustomersWithinTestRange;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Utilities\Common;

class CancelCustomersWithinTestRangeTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var CancelCustomersWithinTestRange
   */
  private $api;
  /**
   * @var CustomerRepository
   */
  private $customerRepository;
  private $apiParams = [
    'customersList' => '',
  ];
  
  private $selectFields = ['customer_id', 'cos_id', 'current_iccid', 'plan_state', 'current_mobile_number'];

  /**
   * @var ConfigurationRepository
   */
  private $configRepository;

  /**
   * @var Control
   */
  private $control;

  /**
   * @var Common
   */
  private $common;

  public function setUp()
  {
    $this->customerRepository = $this->getMock(CustomerRepository::class);
    $this->configRepository = $this->getMock(ConfigurationRepository::class);
    $this->control = $this->getMock(Control::class);
    $this->common = $this->getMock(Common::class);

    $this->common->expects($this->any())
      ->method('luhnenize')
      ->with('890126096316366775')
      ->will($this->returnValue('890126096316366775'));
    
    $this->configRepository->expects($this->at(0))
      ->method('findConfigByKey')
      ->with('mint/sim/test/list')
      ->will($this->returnValue('890126096316366775,890126096316366974|890126096316367175,890126096316367374'));

    $this->configRepository->expects($this->at(1))
      ->method('findConfigByKey')
      ->with('mint/sim/test/whitelist')
      ->will($this->returnValue('890126096316366778,890126096316366779'));

    $this->api = new CancelCustomersWithinTestRange($this->customerRepository, $this->configRepository, $this->control, $this->common);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testErrorThrownWhenAllowedSimRangeHasNotBeenSet()
  {
    $this->setExpectedException('Exception', 'The range of permissible sims to cancel has not been set.');

    $configRepository = $this->getMock(ConfigurationRepository::class);
    $configRepository->expects($this->any())
      ->method('findConfigByKey')
      ->with('mint/sim/test/list')
      ->will($this->returnValue(''));

    new CancelCustomersWithinTestRange($this->customerRepository, $configRepository, $this->control, $this->common);
  }

  public function testMissingList()
  {
    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('VV0116', $result->data_array['error_codes']);
  }

  public function testMissingCancellationReason()
  {
    $this->configRepository = $this->getMock(ConfigurationRepository::class);

    $this->configRepository->expects($this->at(0))
      ->method('findConfigByKey')
      ->with('mint/sim/test/list')
      ->will($this->returnValue('890126096316366775,890126096316366974|890126096316367175,890126096316367374'));

    $this->configRepository->expects($this->at(1))
      ->method('findConfigByKey')
      ->with('mint/sim/test/whitelist')
      ->will($this->returnValue(''));

    $this->api = new CancelCustomersWithinTestRange($this->customerRepository, $this->configRepository, $this->control, $this->common);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);

    $this->apiParams['customersList'] = "890126096316366775";
    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('VV0116', $result->data_array['error_codes']);
  }

  public function testInvalidIccid()
  {
    $this->apiParams['customersList'] = "111111111111111111:Activation";
    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('VV0236', $result->data_array['error_codes']);
  }

  public function testInvalidMsisdn()
  {
    $result = new Result();
    $result->add_error('FAILED');
    $result->data_array['ResultMsg'] = 'Invalid Msisdn';

    $this->control->expects($this->any())
      ->method('mwGetMVNEDetails')
      ->with([
        'actionUUID' => '',
        'msisdn' => '1111111111',
        'iccid' => ''
      ])
      ->will($this->returnValue($result));

    $this->apiParams['customersList'] = "1111111111:Activation";
    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('MW0001', $result->data_array['error_codes']);
  }

  public function testIccidFromMsisdnIsOutOfRange()
  {
    $this->apiParams['customersList'] = "1111111111:Activation";

    $customer = new stdClass();
    $customer->current_iccid = 111111111111111111;

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromMsisdns')
      ->with([1111111111], $this->selectFields)
      ->will($this->returnValue([$customer]));

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('VV0236', $result->data_array['error_codes']);
  }

  public function testIccidFromMsisdnIsInRange()
  {
    $this->apiParams['customersList'] = "1111111111:Activation";

    $customer = new stdClass();
    $customer->current_iccid = 890126096316366775;
    $customer->cos_id = 92220;
    $customer->customer_id = 1234;
    $customer->plan_state = "Active";
    $customer->current_mobile_number = 1111111111;

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromMsisdns')
      ->with([1111111111], $this->selectFields)
      ->will($this->returnValue([$customer]));

    $this->customerRepository->expects($this->any())
      ->method('cancelCustomer')
      ->with($customer, $customer->customer_id)
      ->will($this->returnValue(['success' => true]));

    $this->customerRepository->expects($this->any())
      ->method('addCancellationReason')
      ->with([
        'customer_id' => $customer->customer_id,
        'reason' => "Activation",
        'msisdn' => $customer->current_mobile_number,
        'iccid' => $this->common->luhnenize($customer->current_iccid),
        'cos_id' => $customer->cos_id,
        'type' => 'TEST',
        'status' => $customer->plan_state,
      ])
      ->will($this->returnValue(['success' => true]));

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertEmpty($result->data_array['error_codes']);
  }

  public function testIccidIsInRange()
  {
    $this->apiParams['customersList'] = "890126096316366775:Activation";

    $customer = new stdClass();
    $customer->current_iccid = 890126096316366775;
    $customer->cos_id = 92220;
    $customer->customer_id = 1234;
    $customer->plan_state = "Active";
    $customer->current_mobile_number = 12345678901;

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromIccids')
      ->with([890126096316366775], $this->selectFields)
      ->will($this->returnValue([$customer]));

    $this->customerRepository->expects($this->any())
      ->method('cancelCustomer')
      ->with($customer, $customer->customer_id)
      ->will($this->returnValue(['success' => true]));

    $this->customerRepository->expects($this->any())
      ->method('addCancellationReason')
      ->with([
        'customer_id' => $customer->customer_id,
        'reason' => "Activation",
        'msisdn' => $customer->current_mobile_number,
        'iccid' => $this->common->luhnenize($customer->current_iccid),
        'cos_id' => $customer->cos_id,
        'type' => 'TEST',
        'status' => $customer->plan_state,
      ])
      ->will($this->returnValue(['success' => true]));

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['cancelled_test_sims']);
  }

  public function testFailedToCancelCustomer()
  {
    $this->apiParams['customersList'] = "890126096316366775:Activation";

    $customer = new stdClass();
    $customer->current_iccid = 890126096316366775;
    $customer->cos_id = 92220;
    $customer->customer_id = 1234;
    $customer->plan_state = "Active";
    $customer->current_mobile_number = 12345678901;

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromIccids')
      ->with([890126096316366775], $this->selectFields)
      ->will($this->returnValue([$customer]));

    $this->customerRepository->expects($this->any())
      ->method('cancelCustomer')
      ->with($customer, $customer->customer_id)
      ->will($this->returnValue(['success' => false]));

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['failed_cancelled_test_sims']);
  }

  public function testFailedToAddCancellationReasonForCanceledCustomer()
  {
    $this->apiParams['customersList'] = "890126096316366775:Activation";

    $customer = new stdClass();
    $customer->current_iccid = 890126096316366775;
    $customer->cos_id = 92220;
    $customer->customer_id = 1234;
    $customer->plan_state = "Active";
    $customer->current_mobile_number = 12345678901;

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromIccids')
      ->with([890126096316366775], $this->selectFields)
      ->will($this->returnValue([$customer]));

    $this->customerRepository->expects($this->any())
      ->method('cancelCustomer')
      ->with($customer, $customer->customer_id)
      ->will($this->returnValue(['success' => true]));

    $this->customerRepository->expects($this->any())
      ->method('addCancellationReason')
      ->with([
        'customer_id' => $customer->customer_id,
        'reason' => "Activation",
        'msisdn' => $customer->current_mobile_number,
        'iccid' => $this->common->luhnenize($customer->current_iccid),
        'cos_id' => $customer->cos_id,
        'type' => 'TEST',
        'status' => $customer->plan_state,
      ])
      ->will($this->returnValue(false));

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertNotEmpty($result->data_array['cancelled_test_sims']);
  }

  public function testNoCustomerFoundForIccidInRange()
  {
    $result = new Result();
    $result->add_error('FAILED');
    $result->data_array['ResultMsg'] = 'Invalid Iccid';

    $this->control->expects($this->any())
      ->method('mwGetMVNEDetails')
      ->with([
        'actionUUID' => '',
        'msisdn' => '',
        'iccid' => $this->common->luhnenize('890126096316366775')
      ])
      ->will($this->returnValue($result));

    $this->apiParams['customersList'] = "890126096316366775:Activation";

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromIccids')
      ->with([890126096316366775], $this->selectFields)
      ->will($this->returnValue([]));

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('MW0001', $result->data_array['error_codes']);
  }

  public function testNotAMsisdnOrIccid()
  {
    $this->apiParams['customersList'] = "89012:Activation";

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('VV0116', $result->data_array['error_codes']);
  }

  public function testWhiteListedNumberCanNotBeCancelled()
  {
    $this->apiParams['customersList'] = "890126096316366778:Activation";

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('VV0236', $result->data_array['error_codes']);
  }

  public function testEnsureToCancelCustomerSuccess()
  {
    $result = new Result();
    $result->succeed();
    $result->data_array['QuerySubscriber'] = (object) ['MSISDN' => '123456789', 'SIM' => '890126096316366775'];
    $result->data_array['success'] = true;

    $this->control->expects($this->any())
      ->method('mwGetMVNEDetails')
      ->with([
        'actionUUID' => '',
        'msisdn' => '',
        'iccid' => $this->common->luhnenize('890126096316366775')
      ])
      ->will($this->returnValue($result));

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromIccids')
      ->with([890126096316366775], $this->selectFields)
      ->will($this->returnValue([]));

    $result = new Result();
    $result->data_array['QuerySubscriber'] = (object) ['MSISDN' => '123456789', 'SIM' => '890126096316366775'];
    $result->succeed();

    $this->customerRepository->expects($this->any())
      ->method('ensureCancelCustomer')
      ->with($result->data_array['QuerySubscriber']->MSISDN, $result->data_array['QuerySubscriber']->SIM)
      ->will($this->returnValue($result));

    $this->apiParams['customersList'] = "890126096316366775:Activation";

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('890126096316366775', $result->data_array['cancelled_test_sims']);
  }

  public function testEnsureToCancelCustomerFailed()
  {
    $result = new Result();
    $result->succeed();
    $result->data_array['QuerySubscriber'] = (object) ['MSISDN' => '123456789', 'SIM' => '890126096316366775'];
    $result->data_array['success'] = true;

    $this->control->expects($this->any())
      ->method('mwGetMVNEDetails')
      ->with([
        'actionUUID' => '',
        'msisdn' => '',
        'iccid' => $this->common->luhnenize('890126096316366775')
      ])
      ->will($this->returnValue($result));

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromIccids')
      ->with([890126096316366775], $this->selectFields)
      ->will($this->returnValue([]));

    $result = new Result();
    $result->data_array['QuerySubscriber'] = (object) ['MSISDN' => '123456789', 'SIM' => '890126096316366775'];

    $this->customerRepository->expects($this->any())
      ->method('ensureCancelCustomer')
      ->with($result->data_array['QuerySubscriber']->MSISDN, $result->data_array['QuerySubscriber']->SIM)
      ->will($this->returnValue($result));

    $this->apiParams['customersList'] = "890126096316366775:Activation";

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('890126096316366775', $result->data_array['failed_cancelled_test_sims']);
  }

  public function testMiddlewareReturnsMissingMsisdnFromIccid()
  {
    $result = new Result();
    $result->succeed();
    $result->data_array['QuerySubscriber'] = (object) ['MSISDN' => '', 'SIM' => '890126096316366775'];
    $result->data_array['success'] = true;
    
    $this->control->expects($this->any())
      ->method('mwGetMVNEDetails')
      ->with([
        'actionUUID' => '',
        'msisdn' => '',
        'iccid' => $this->common->luhnenize('890126096316366775')
      ])
      ->will($this->returnValue($result));

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromIccids')
      ->with([890126096316366775], $this->selectFields)
      ->will($this->returnValue([]));
    
    $this->apiParams['customersList'] = "890126096316366775:Activation";

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('MW0001', $result->data_array['error_codes']);
  }

  public function testMiddlewareReturnsIccidThatIsOutOfRange()
  {
    $result = new Result();
    $result->succeed();
    $result->data_array['QuerySubscriber'] = (object) ['MSISDN' => '1234567890', 'SIM' => '890126096316360000'];
    $result->data_array['success'] = true;

    $this->control->expects($this->any())
      ->method('mwGetMVNEDetails')
      ->with([
        'actionUUID' => '',
        'msisdn' => '1234567890',
        'iccid' => ''
      ])
      ->will($this->returnValue($result));

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromIccids')
      ->with([1234567890], $this->selectFields)
      ->will($this->returnValue([]));

    $this->apiParams['customersList'] = "1234567890:Activation";

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('VV0236', $result->data_array['error_codes']);
  }

  public function testMiddlewareAndDatabaseReturnNoUsers()
  {
    $result = new Result();
    $result->succeed();
    $result->data_array['QuerySubscriber'] = (object) ['MSISDN' => '1234567890', 'SIM' => '890126096316360000'];
    $result->data_array['success'] = true;

    $this->control->expects($this->any())
      ->method('mwGetMVNEDetails')
      ->with([
        'actionUUID' => '',
        'msisdn' => '1234567890',
        'iccid' => ''
      ])
      ->will($this->returnValue($result));

    $this->customerRepository->expects($this->any())
      ->method('getCustomersFromIccids')
      ->with([1234567890], $this->selectFields)
      ->will($this->returnValue([]));

    $this->apiParams['customersList'] = "1234567890:Activation";

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__CancelCustomersWithinTestRange();

//    print_r($result);

    $this->assertContains('VV0236', $result->data_array['error_codes']);
  }
}

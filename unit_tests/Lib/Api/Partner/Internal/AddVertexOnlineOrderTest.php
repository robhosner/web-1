<?php

use Ultra\Lib\Api\Partner\Internal\AddVertexOnlineOrder;
use Ultra\Lib\ApiErrors;
use Ultra\Orders\Repositories\Mssql\OrderRepository;

class AddVertexOnlineOrderTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var AddVertexOnlineOrder
   */
  private $api;
  private $orderRepository;
  private $apiParams = [
    'ordernum' => '',
    'iccid' => '',
    'imei' => '',
    'SKU' => '',
    'first_name' => '',
    'last_name' => '',
    'email' => '',
    'address1' => '',
    'address2' => '',
    'city' => '',
    'state' => '',
    'postal_code' => '',
    'token' => '',
    'bin' => '',
    'last_four' => '',
    'expires_date' => '',
    'gateway' => '',
    'merchant_account' => '',
    'avs_validation' => '',
    'cvv_validation' => '',
    'customer_ip' => '',
    'preferred_language' => '',
    'auto_enroll' => '',
    'track_trace' => '',
  ];

  public function setUp()
  {
    $this->orderRepository = $this->getMock(OrderRepository::class);
    $this->api = new AddVertexOnlineOrder($this->orderRepository);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->apiParams['expires_date'] = date('my');
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidExpirationDate()
  {
    $this->apiParams['expires_date'] = '1316';
    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__AddVertexOnlineOrder();

//    print_r($result);

    $this->assertContains('VV0007', $result->data_array['error_codes']);
  }
  
  public function testFailedInsertingOnlineOrderIntoDatabase()
  {
    $this->orderRepository->expects($this->any())
      ->method('addOrder')
      ->with($this->equalTo($this->apiParams))
      ->will($this->returnValue(false));

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__AddVertexOnlineOrder();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testSuccessfullyAddingOnlineOrder()
  {
    $this->orderRepository->expects($this->any())
      ->method('addOrder')
      ->with($this->equalTo($this->apiParams))
      ->will($this->returnValue(true));

    $this->api->setInputValues($this->apiParams);
    $result = $this->api->internal__AddVertexOnlineOrder();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

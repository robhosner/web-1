<?php

namespace unit_tests\Lib\Api\Partner\Interal;

use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Lib\Api\Partner\Internal\SetAccountValues;
use Ultra\Lib\ApiErrors;

class SetAccountValuesTest extends \PHPUnit_Framework_TestCase
{
  private $customerRepo;
  private $accountsRepo;

  private $api;

  public function setUp()
  {
    $this->config = $this->getMock(Configuration::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->accountsRepo = $this->getMock(AccountsRepository::class);

    $this->api = new SetAccountValues($this->config, $this->customerRepo, $this->accountsRepo);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testPlanStateFailure()
  {
    $this->api->setInputValues(['customer_id' => 123, 'packaged_balance3' => 100]);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['plan_state', 'brand_id'])
      ->will($this->returnValue((object)['plan_state' => 'Cancelled', 'brand_id' => 1]));

    $result = $this->api->internal__SetAccountValues();
    $this->assertContains('IN0001', $result->data_array['error_codes']);
  }

  public function testBrandFailure()
  {
    $this->api->setInputValues(['customer_id' => 123, 'packaged_balance3' => 100]);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['plan_state', 'brand_id'])
      ->will($this->returnValue((object)['plan_state' => 'Active', 'brand_id' => 1]));

    $this->config->expects($this->any())
      ->method('getBrandIdFromBrand')
      ->with('ULTRA')
      ->will($this->returnValue(2));

    $result = $this->api->internal__SetAccountValues();
    $this->assertContains('FA0004', $result->data_array['error_codes']);
  }

  public function testSuccess()
  {
    $this->api->setInputValues(['customer_id' => 123, 'packaged_balance3' => 100]);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['plan_state', 'brand_id'])
      ->will($this->returnValue((object)['plan_state' => 'Active', 'brand_id' => 1]));

    $this->config->expects($this->any())
      ->method('getBrandIdFromBrand')
      ->with('ULTRA')
      ->will($this->returnValue(1));

    $this->accountsRepo->expects($this->any())
      ->method('updateAccount')
      ->with(123, ['packaged_balance3' => 100])
      ->will($this->returnValue(true));

    $result = $this->api->internal__SetAccountValues();
    $this->assertTrue($result->is_success());
  }

}

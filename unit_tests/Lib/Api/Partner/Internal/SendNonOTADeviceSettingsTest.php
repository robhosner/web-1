<?php
namespace unit_tests\Lib\Api\Partner\Interal;

use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Internal\SendNonOTADeviceSettings;
use Ultra\Lib\ApiErrors;
use Ultra\Messaging\Messenger;

class SendNonOTADeviceSettingsTest extends \PHPUnit_Framework_TestCase
{
  private $customerRepo;
  private $messenger;

  /**
   * @var SendNonOTADeviceSettings
   */
  private $api;

  public $message = <<<'EOT'
1.Select "Settings" > "Cellular" > "Cellular Data" > "Cellular Data Option" > "APN"\n
2.Enter "alpha" > MMS \n
3.Select "APN"\n
4. Enter "wholesale"\n
5.Select "MMSC" \n
6.Enter "http://wholesale.mmsmvno.com/mms/wapenc"\n
7.Select "MMS Max Message Size"\n
8.Enter "1048576"\n
9.Select "MMS UA Prof URL" \n
10.Enter "http://www.apple.com/mms/uaprof.rdf"\n
11.Restart your iPhone
EOT;

  public function setUp()
  {
    $this->messenger = $this->getMock(Messenger::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);

    $this->api = new SendNonOTADeviceSettings($this->messenger, $this->customerRepo);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues(['msisdn' => '123']);
  }

  public function testMessageIsTheSame()
  {
    $this->assertEquals($this->message, $this->api->message);
  }

  public function testCustomerNotFound()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue(false));

    $result = $this->api->internal__SendNonOTADeviceSettings();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testSendSmsFails()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue((object) ['CUSTOMER_ID' => 1, 'BRAND_ID' => 1, 'preferred_language' => 'EN']));

    $this->messenger->expects($this->any())
      ->method('smsByLanguage')
      ->will($this->returnValue('message'));

    $this->messenger->expects($this->any())
      ->method('mvneBypassListSynchronousSendSMS')
      ->will($this->returnValue(new \Result()));

    $result = $this->api->internal__SendNonOTADeviceSettings();

//    print_r($result);

    $this->assertContains('SM0002', $result->data_array['error_codes']);
  }

  public function testSecondSendSmsFails()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue((object) ['CUSTOMER_ID' => 1, 'BRAND_ID' => 1, 'preferred_language' => 'EN']));

    $this->messenger->expects($this->any())
      ->method('smsByLanguage')
      ->will($this->returnValue('message'));

    $this->messenger->expects($this->any())
      ->method('mvneBypassListSynchronousSendSMS')
      ->will($this->returnValue(new \Result(null, true)));

    $this->messenger->expects($this->any())
      ->method('mvneBypassListAsynchronousSendSMS')
      ->will($this->returnValue(new \Result()));

    $result = $this->api->internal__SendNonOTADeviceSettings();

//    print_r($result);

    $this->assertContains('SM0002', $result->data_array['error_codes']);
  }

  public function testSendSmsSucceeds()
  {
    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->will($this->returnValue((object) ['CUSTOMER_ID' => 1, 'BRAND_ID' => 1, 'preferred_language' => 'EN']));

    $this->messenger->expects($this->any())
      ->method('smsByLanguage')
      ->will($this->returnValue('message'));

    $this->messenger->expects($this->any())
      ->method('mvneBypassListSynchronousSendSMS')
      ->will($this->returnValue(new \Result(null, true)));

    $this->messenger->expects($this->any())
      ->method('mvneBypassListAsynchronousSendSMS')
      ->will($this->returnValue(new \Result(null, true)));

    $result = $this->api->internal__SendNonOTADeviceSettings();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

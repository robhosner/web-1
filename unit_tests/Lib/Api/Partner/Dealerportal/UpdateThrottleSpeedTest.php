<?php
namespace unit_tests\Lib\Api\Partner\DealerPortal;

use Ultra\Accounts\Account;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Dealerportal\UpdateThrottleSpeed;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Mvne\Adapter;

class UpdateThrottleSpeedTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var CustomerRepository
   */
  private $customerRepo;
  /**
   * @var Session
   */
  public $session;
  /**
   * @var UpdateThrottleSpeed
   */
  private $api;
  private $adapter;
  private $accountsRepository;
  private $configuration;

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->configuration = $this->getMock(Configuration::class);
    $this->adapter = $this->getMockBuilder(Adapter::class)
      ->setConstructorArgs([
        $this->getMock(Control::class),
        $this->customerRepo,
        $this->configuration
      ])
      ->getMock();
    $this->accountsRepository = $this->getMock(AccountsRepository::class);

    $this->api = $this->getMockBuilder(UpdateThrottleSpeed::class)
      ->setConstructorArgs([$this->customerRepo, $this->adapter, $this->accountsRepository, $this->configuration])
      ->setMethods(['getValidUltraSessionData', 'checkUltraSessionAllowedAPICustomer'])
      ->getMock();

    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->api->setInputValues(['security_token' => '', 'customer_id' => '123', 'targetSpeed' => '']);
    $this->api->expects($this->any())
      ->method('getValidUltraSessionData')
      ->with('', 'dealerportal__UpdateThrottleSpeed')
      ->will($this->returnValue(['', 'SE0002', '']));

    $result = $this->api->dealerportal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('SE0002', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues(['security_token' => '', 'customer_id' => '123', 'targetSpeed' => '']);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue(false));

    $result = $this->api->dealerportal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testCustomerAccountNotFound()
  {
    $targetSpeed = 'full';
    $this->api->setInputValues(['security_token' => '', 'customer_id' => '123', 'targetSpeed' => $targetSpeed]);

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['canUpdateToThrottleSpeed'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('canUpdateToThrottleSpeed')
      ->with($targetSpeed)
      ->will($this->returnValue(false));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue($this->customer));

    $this->accountsRepository->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(false));

    $result = $this->api->dealerportal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testUnableToUpdateThrottleSpeed()
  {
    $targetSpeed = 'full';
    $this->api->setInputValues(['security_token' => '', 'customer_id' => '123', 'targetSpeed' => $targetSpeed]);

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['canUpdateToThrottleSpeed'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('canUpdateToThrottleSpeed')
      ->with('full')
      ->will($this->returnValue(false));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue($this->customer));

    $this->accountsRepository->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(new Account(['COS_ID' => 12345])));

    $this->configuration->expects($this->any())
      ->method('getThrottleSpeedFromKey')
      ->with('full')
      ->will($this->returnValue('full'));

    $result = $this->api->dealerportal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
  }

  public function testMvneMakeitsoUpdateThrottleSpeedFails()
  {
    $targetSpeed = 'full';
    $this->api->setInputValues(['security_token' => '', 'customer_id' => '123', 'targetSpeed' => $targetSpeed]);

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['canUpdateToThrottleSpeed'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('canUpdateToThrottleSpeed')
      ->with('full')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue($this->customer));

    $this->accountsRepository->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(new Account(['COS_ID' => 12345])));

    $this->configuration->expects($this->any())
      ->method('getThrottleSpeedFromKey')
      ->with('full')
      ->will($this->returnValue('full'));

    $this->configuration->expects($this->any())
      ->method('getPlanFromCosId')
      ->with(12345)
      ->will($this->returnValue(123));

    $result = $this->api->dealerportal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
  }

  public function testSuccesfulThrottleUpdate()
  {
    $targetSpeed = 'full';
    $this->api->setInputValues(['security_token' => '', 'customer_id' => '123', 'targetSpeed' => $targetSpeed]);

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['canUpdateToThrottleSpeed'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('canUpdateToThrottleSpeed')
      ->with('full')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue($this->customer));

    $this->accountsRepository->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(new Account(['COS_ID' => 12345])));

    $this->configuration->expects($this->any())
      ->method('getThrottleSpeedFromKey')
      ->with('full')
      ->will($this->returnValue('full'));

    $this->configuration->expects($this->any())
      ->method('getPlanFromCosId')
      ->with(12345)
      ->will($this->returnValue(123));

    $this->adapter->expects($this->any())
      ->method('mvneMakeitsoUpdateThrottleSpeed')
      ->with(123, 123, 'full', 'LTE_SELECT.DEALER_PORTAL')
      ->will($this->returnValue(['success' => 1]));

    $result = $this->api->dealerportal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

<?php
namespace unit_tests\Portal;

use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\Portal\ValidatePIN;
use Ultra\Lib\ApiErrors;
use Ultra\Payments\Payment;
use Ultra\Utilities\SessionUtilities;

class ValidatePINTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var ValidatePIN
   */
  public  $api;
  private $params = ['pin' => 12345];
  private $customerRepo;
  private $sessionUtils;
  private $payment;

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->sessionUtils = $this->getMock(SessionUtilities::class);
    $this->payment = $this->getMock(Payment::class);
    $this->api = new ValidatePIN($this->customerRepo, $this->sessionUtils, $this->payment);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testMissingZsession()
  {
    $this->api->setInputValues($this->params);

    $result = $this->api->portal__ValidatePIN();

//    print_r($result);

    $this->assertContains('SE0003', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues($this->params);

    $_COOKIE['zsession'] = 1;

    $customer = new \stdClass();
    $customer->customer_id = 1;
    $customer->current_mobile_number = 123456789;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromSession')
      ->with(1)
      ->will($this->throwException(new MissingRequiredParametersException('message')));

    $result = $this->api->portal__ValidatePIN();

//    print_r($result);

    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  public function testSessionIsExpired()
  {
    $this->api->setInputValues($this->params);

    $_COOKIE['zsession'] = 1;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromSession')
      ->with(1)
      ->will($this->returnValue(false));

    $result = $this->api->portal__ValidatePIN();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerIsBlockedFromApi()
  {
    $this->api->setInputValues($this->params);

    $_COOKIE['zsession'] = 1;

    $customer = new \stdClass();
    $customer->customer_id = 1;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromSession')
      ->with(1)
      ->will($this->returnValue($customer));

    $this->sessionUtils->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->with('portal__ValidatePIN', 1, 10)
      ->will($this->returnValue(true));

    $result = $this->api->portal__ValidatePIN();

//    print_r($result);

    $this->assertContains('AP0001', $result->data_array['error_codes']);
  }

  public function testPinHasAtLeastOneNotFound()
  {
    $this->api->setInputValues($this->params);

    $_COOKIE['zsession'] = 1;

    $customer = new \stdClass();
    $customer->customer_id = 1;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromSession')
      ->with(1)
      ->will($this->returnValue($customer));

    $this->sessionUtils->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->with('portal__ValidatePIN', 1, 10)
      ->will($this->returnValue(false));

    $this->payment->expects($this->any())
      ->method('funcValidatePinCards')
      ->with(['pin_list' => [$this->params['pin']]])
      ->will($this->returnValue(['at_least_one_not_found' => true]));

    $result = $this->api->portal__ValidatePIN();

//    print_r($result);

    $this->assertContains('PI0001', $result->data_array['error_codes']);
  }

  public function testPinHasAtLeastOneAtFoundry()
  {
    $this->api->setInputValues($this->params);

    $_COOKIE['zsession'] = 1;

    $customer = new \stdClass();
    $customer->customer_id = 1;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromSession')
      ->with(1)
      ->will($this->returnValue($customer));

    $this->sessionUtils->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->with('portal__ValidatePIN', 1, 10)
      ->will($this->returnValue(false));

    $this->payment->expects($this->any())
      ->method('funcValidatePinCards')
      ->with(['pin_list' => [$this->params['pin']]])
      ->will($this->returnValue(['at_least_one_not_found' => false, 'at_least_one_at_foundry' => true]));

    $result = $this->api->portal__ValidatePIN();

//    print_r($result);

    $this->assertContains('PI0002', $result->data_array['error_codes']);
  }

  public function testPinValidationHasErrors()
  {
    $this->api->setInputValues($this->params);

    $_COOKIE['zsession'] = 1;

    $customer = new \stdClass();
    $customer->customer_id = 1;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromSession')
      ->with(1)
      ->will($this->returnValue($customer));

    $this->sessionUtils->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->with('portal__ValidatePIN', 1, 10)
      ->will($this->returnValue(false));

    $this->payment->expects($this->any())
      ->method('funcValidatePinCards')
      ->with(['pin_list' => [$this->params['pin']]])
      ->will($this->returnValue(['at_least_one_not_found' => false, 'at_least_one_at_foundry' => false, 'errors' => [1]]));

    $result = $this->api->portal__ValidatePIN();

//    print_r($result);

    $this->assertContains('PI0003', $result->data_array['error_codes']);
  }

  public function testPinHasBeenUsed()
  {
    $this->api->setInputValues($this->params);

    $_COOKIE['zsession'] = 1;

    $customer = new \stdClass();
    $customer->customer_id = 1;
    $customer->current_mobile_number = 123456789;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromSession')
      ->with(1)
      ->will($this->returnValue($customer));

    $this->sessionUtils->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->with('portal__ValidatePIN', 1, 10)
      ->will($this->returnValue(false));

    $this->payment->expects($this->any())
      ->method('funcValidatePinCards')
      ->with(['pin_list' => [$this->params['pin']]])
      ->will($this->returnValue([
        'at_least_one_not_found' => false,
        'at_least_one_at_foundry' => false,
        'errors' => [],
        'at_least_one_customer_used' => true,
        'result' => [$this->params['pin'] => ['customer_used' => 123, 'last_changed_date' => "Oct 2 2012 08:39:38:233PM"]]
      ]));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['current_mobile_number'])
      ->will($this->returnValue($customer));

    $result = $this->api->portal__ValidatePIN();

//    print_r($result);

    $this->assertContains('PI0004', $result->data_array['error_codes']);
  }

  public function testPinIsSuccessful()
  {
    $this->api->setInputValues($this->params);

    $_COOKIE['zsession'] = 1;

    $customer = new \stdClass();
    $customer->customer_id = 1;
    $customer->current_mobile_number = 123456789;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromSession')
      ->with(1)
      ->will($this->returnValue($customer));

    $this->sessionUtils->expects($this->any())
      ->method('checkApiAbuseByIdentifier')
      ->with('portal__ValidatePIN', 1, 10)
      ->will($this->returnValue(false));

    $this->payment->expects($this->any())
      ->method('funcValidatePinCards')
      ->with(['pin_list' => [$this->params['pin']]])
      ->will($this->returnValue([
        'at_least_one_not_found' => false,
        'at_least_one_at_foundry' => false,
        'errors' => [],
        'at_least_one_customer_used' => false,
        'result' => [$this->params['pin'] => ['customer_used' => 123, 'last_changed_date' => "Oct 2 2012 08:39:38:233PM", 'pin_value' => 1]]
      ]));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['current_mobile_number'])
      ->will($this->returnValue($customer));

    $result = $this->api->portal__ValidatePIN();

//    print_r($result);

    $this->assertEquals(100, $result->data_array['pin_value']);
    $this->assertTrue($result->is_success());
  }
}

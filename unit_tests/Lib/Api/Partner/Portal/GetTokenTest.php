<?php
namespace unit_tests\Portal;

use Ultra\Lib\Api\Partner\Portal\GetToken;
use Ultra\Lib\ApiErrors;
use Ultra\Utilities\SessionUtilities;

class GetTokenTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var GetToken
   */
  public  $api;
  private $params = [];
  private $sessionUtils;

  public function setUp()
  {
    $this->sessionUtils = $this->getMock(SessionUtilities::class);
    $this->api = new GetToken($this->sessionUtils);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testFailedToGetToken()
  {
    $this->api->setInputValues($this->params);

    $session = new \stdClass();
    $session->token = null;

    $this->sessionUtils->expects($this->any())
      ->method('getSession')
      ->will($this->returnValue($session));

    $result = $this->api->portal__GetToken();

//    print_r($result);

    $this->assertContains('SE0001', $result->data_array['error_codes']);
    $this->assertNull($result->data_array['token']);
  }

  public function testSuccessfullyGetToken()
  {
    $this->api->setInputValues($this->params);

    $session = new \stdClass();
    $session->token = 'CoolTokenBro';

    $this->sessionUtils->expects($this->any())
      ->method('getSession')
      ->will($this->returnValue($session));

    $result = $this->api->portal__GetToken();

    $this->assertTrue($result->is_success());
    $this->assertEquals('CoolTokenBro', $result->data_array['token']);
  }
}

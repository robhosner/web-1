<?php

use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal\SetCustomerInfo;
use Ultra\Lib\ApiErrors;
use Ultra\Utilities\Common;

class SetCustomerInfoTest extends PHPUnit_Framework_TestCase
{
  public $utils;
  public $session;
  public $customerRepo;

  /**
   * @var Customer
   */
  public $customer;

  /**
   * @var SetCustomerInfo
   */
  public $api;

  public $apiParams = [
    'first_name' => '',
    'last_name' => '',
    'e_mail' => '',
    'login_password' => 'test123',
  ];

  public function setUp()
  {
    $this->session = $this->getMock(Session::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->utils = $this->getMock(Common::class);
    $this->api = new SetCustomerInfo($this->session, $this->customerRepo, $this->utils);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);

    $this->session->customer_id = 123;
  }

  public function testInvalidSession()
  {
    $this->api->setInputValues($this->apiParams);

    $this->session->customer_id = false;
    $result = $this->api->portal__SetCustomerInfo();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues($this->apiParams);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['plan_state'], true)
      ->will($this->returnValue(false));

    $result = $this->api->portal__SetCustomerInfo();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testCustomerIsInCancelledState()
  {
    $this->api->setInputValues($this->apiParams);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['plan_state'], true)
      ->will($this->returnValue(new Customer(['customer_id' => 123, 'plan_state' => 'Cancelled'])));

    $result = $this->api->portal__SetCustomerInfo();

//    print_r($result);

    $this->assertContains('IN0001', $result->data_array['error_codes']);
  }

  public function testMissingUpdateParams()
  {
    $this->apiParams['login_password'] = '';
    $this->api->setInputValues($this->apiParams);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['plan_state'], true)
      ->will($this->returnValue(new Customer(['customer_id' => 123, 'plan_state' => 'Active'])));

    $result = $this->api->portal__SetCustomerInfo();

//    print_r($result);

    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  public function testUpdateCustomerFails()
  {
    $this->api->setInputValues($this->apiParams);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['plan_state'], true)
      ->will($this->returnValue(new Customer(['customer_id' => 123, 'plan_state' => 'Active'])));

    $this->utils->expects($this->any())
      ->method('encryptPasswordHS')
      ->with('test123')
      ->will($this->returnValue('test123'));

    $this->customerRepo->expects($this->any())
      ->method('updateCustomerByCustomerId')
      ->with(123, ['login_password' => 'test123'])
      ->will($this->returnValue(false));

    $result = $this->api->portal__SetCustomerInfo();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testSuccess()
  {
    $this->api->setInputValues($this->apiParams);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['plan_state'], true)
      ->will($this->returnValue(new Customer(['customer_id' => 123, 'plan_state' => 'Active'])));

    $this->utils->expects($this->any())
      ->method('encryptPasswordHS')
      ->with('test123')
      ->will($this->returnValue('test123'));

    $this->customerRepo->expects($this->any())
      ->method('updateCustomerByCustomerId')
      ->with(123, ['login_password' => 'test123'])
      ->will($this->returnValue(true));

    $result = $this->api->portal__SetCustomerInfo();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

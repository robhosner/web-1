<?php
namespace unit_tests\Portal;

use Ultra\Accounts\Account;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Api\Partner\Portal\GetBoltOns;
use Ultra\Lib\ApiErrors;

class GetBoltOnsTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var GetBoltOns
   */
  public  $api;
  private $params = ['customer_id' => '1234'];
  private $accountRepo;
  private $config;

  public function setUp()
  {
    $this->accountRepo = $this->getMock(AccountsRepository::class);
    $this->config = $this->getMock(Configuration::class);
    $this->api = new GetBoltOns($this->accountRepo, $this->config);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testGetBoltOnInfoByPlan()
  {
    $this->api->setInputValues($this->params);

    $account = new Account(['cos_id' => 22]);

    $this->accountRepo->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with($this->params['customer_id'], ['COS_ID'])
      ->will($this->returnValue($account));

    $this->config->expects($this->any())
      ->method('getPlanFromCosId')
      ->with($account->cos_id)
      ->will($this->returnValue('plan123'));

    $this->config->expects($this->any())
      ->method('getPlanNameFromShortName')
      ->with('plan123')
      ->will($this->returnValue('PLAN'));

    $this->config->expects($this->any())
      ->method('getBoltOnInfoByPlan')
      ->with('PLAN')
      ->will($this->returnValue(['BOLTONS' => 'BOLT']));

    $result = $this->api->portal__GetBoltOns();

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertNotEmpty($result->data_array['bolt_ons']);
  }

  public function testNoBoltOnInformation()
  {
    $this->params['customer_id'] = 0;
    $this->api->setInputValues($this->params);

    $this->config->expects($this->any())
      ->method('getBoltOnsMappingByType')
      ->will($this->returnValue([]));

    $result = $this->api->portal__GetBoltOns();

//    print_r($result);

    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }
}

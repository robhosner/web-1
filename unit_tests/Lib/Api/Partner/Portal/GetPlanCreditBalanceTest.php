<?php

use Ultra\Lib\Api\Partner\Portal\GetPlanCreditBalance;
use Ultra\Lib\ApiErrors;

class GetPlanCreditBalanceTest extends PHPUnit_Framework_TestCase
{
  private $session;
  private $lineCredits;

  public function setUp()
  {
    $this->session = $this->getMock(Session::class);
    $this->lineCredits = $this->getMockBuilder(LineCredits::class)
      ->setMethods(['getBalance', 'dbConnect'])
      ->getMock();

    $this->lineCredits->expects($this->any())
      ->method('dbConnect')
      ->will($this->returnValue(''));

    $this->api = new GetPlanCreditBalance($this->session, $this->lineCredits);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function test__invalidSession()
  {
    $this->session->customer_id = false;
    $result = $this->api->portal__GetPlanCreditBalance();

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function test__getBalanceThrows()
  {
    $this->session->customer_id = 1;

    // mock getBalance method
    $this->lineCredits->expects($this->once())
      ->method('getBalance')
      ->will($this->throwException(new \Exception));

    $result = $this->api->portal__GetPlanCreditBalance();
    $this->assertFalse($result->is_success());
  }

  public function test__success()
  {
    $this->session->customer_id = 1;

    // mock getBalance method
    $this->lineCredits->expects($this->once())
      ->method('getBalance')
      ->will($this->returnValue(1));

    $result = $this->api->portal__GetPlanCreditBalance();
    $this->assertEquals(1, $result->data_array['credits']);
  }
}

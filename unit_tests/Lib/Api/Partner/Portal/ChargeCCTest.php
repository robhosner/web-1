<?php

use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Customers\StateActions;
use Ultra\Exceptions\CustomErrorCodeException;
use Ultra\FeatureFlags\FeatureFlagsClientWrapper;
use Ultra\Lib\Api\Partner\Portal\ChargeCC;
use Ultra\Lib\ApiErrors;
use Ultra\Payments\CreditCardCharger;
use Ultra\Payments\Payment;
use Ultra\Utilities\SessionUtilities;
use Ultra\Lib\Util\Redis;
use Ultra\Messaging\Messenger;

class ChargeCCTest extends PHPUnit_Framework_TestCase
{
  public $creditCardCharger;
  public $session;
  public $params = [
    'charge_amount' => 1900,
    'destination' => 12345,
    'product_type' => 'ADD_BALANCE'
  ];

  /**
   * @var ChargeCC
   */
  public $api;

  public function setUp()
  {
    $this->creditCardCharger = $this->getMockBuilder(CreditCardCharger::class)
      ->setConstructorArgs([
        $this->getMock(FeatureFlagsClientWrapper::class),
        $this->getMock(CustomerRepository::class),
        $this->getMock(SessionUtilities::class),
        $this->getMock(Redis::class),
        $this->getMock(StateActions::class),
        $this->getMock(Configuration::class),
        $this->getMock(Payment::class),
        $this->getMock(Messenger::class)
      ])
      ->getMock();
    $this->session = $this->getMock(Session::class);
    $this->session->customer_id = 123;

    $this->api = new ChargeCC($this->creditCardCharger, $this->session);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues($this->params);
  }

  public function testInvalidSession()
  {
    $this->session->customer_id = false;
    $result = $this->api->portal__ChargeCC();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testChargeCreditCardFails()
  {
    $this->creditCardCharger->expects($this->any())
      ->method('chargeCreditCard')
      ->will($this->throwException(new CustomErrorCodeException('BOO', 123)));
    $this->session->customer_id = 123;
    $result = $this->api->portal__ChargeCC();

//    print_r($result);

    $this->assertContains('123', $result->data_array['error_codes']);
    $this->assertContains('BOO', $result->get_errors());
  }

  public function testChargeCreditCardSucceeds()
  {
    $this->session->customer_id = 123;
    $result = $this->api->portal__ChargeCC();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

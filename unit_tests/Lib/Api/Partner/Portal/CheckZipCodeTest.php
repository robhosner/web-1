<?php
namespace unit_tests\Portal;

use Exception;
use Ultra\Lib\Api\Partner\Portal\CheckZipCode;
use Ultra\Lib\ApiErrors;
use Ultra\Utilities\Validator;

class CheckZipCodeTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var CheckZipCode
   */
  public  $api;
  private $params = ['zipcode' => 12345];
  private $validator;

  public function setUp()
  {
    $this->validator = $this->getMock(Validator::class);
    $this->api = new CheckZipCode($this->validator);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues($this->params);
  }

  public function testZipCodeIsOutOfCoverage()
  {
    $this->validator->expects($this->any())
      ->method('validateZipCodeIsInCoverage')
      ->with($this->params['zipcode'])
      ->will($this->returnValue(false));

    $result = $this->api->portal__CheckZipCode();

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertFalse($result->data_array['zipcode_valid']);
  }

  public function testZipCodeIsInCoverage()
  {
    $this->validator->expects($this->any())
      ->method('validateZipCodeIsInCoverage')
      ->with($this->params['zipcode'])
      ->will($this->returnValue(true));

    $result = $this->api->portal__CheckZipCode();

//    print_r($result);

    $this->assertTrue($result->is_success());
    $this->assertTrue($result->data_array['zipcode_valid']);
  }

  public function testZipCodeCheckThrowsException()
  {
    $this->validator->expects($this->any())
      ->method('validateZipCodeIsInCoverage')
      ->with($this->params['zipcode'])
      ->will($this->throwException(new Exception('boo')));

    $result = $this->api->portal__CheckZipCode();

//    print_r($result);

    $this->assertFalse($result->is_success());
    $this->assertFalse($result->data_array['zipcode_valid']);
  }
}

<?php

use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Customers\StateActions;
use Ultra\Lib\Api\Partner\Portal\ApplyBalanceToStoredValue;
use Ultra\Lib\ApiErrors;
use Ultra\Payments\Payment;
use Ultra\Utilities\Common;
use Ultra\Messaging\Messenger;

class ApplyBalanceToStoredValueTest extends PHPUnit_Framework_TestCase
{
  public $utils;
  public $session;
  public $customerRepo;
  public $messenger;

  /**
   * @var ApplyBalanceToStoredValue
   */
  public $api;

  public function setUp()
  {
    $this->utils = $this->getMock(Common::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->session = $this->getMock(Session::class);
    $this->payment = $this->getMock(Payment::class);
    $this->stateActions = $this->getMock(StateActions::class);
    $this->messenger = $this->getMock(Messenger::class);

    $this->api = new ApplyBalanceToStoredValue($this->utils, $this->session, $this->customerRepo, $this->payment, $this->stateActions, $this->messenger);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);

    $this->api->setInputValues(['amount' => 35]);
  }

  public function testInvalidSession()
  {
    $this->api->setInputValues([
      'amount' => 35
    ]);

    $this->session->customer_id = false;
    $result = $this->api->portal__ApplyBalanceToStoredValue();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->session->customer_id = 123;
    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(123)
      ->will($this->returnValue(false));

    $result = $this->api->portal__ApplyBalanceToStoredValue();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testInvalidState()
  {
    $this->session->customer_id = 123;

    $customer = new stdClass();
    $customer->preferred_language = 'EN';

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(123)
      ->will($this->returnValue($customer));


    $this->stateActions->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(false));

    $result = $this->api->portal__ApplyBalanceToStoredValue();

//    print_r($result);

    $this->assertContains('UN0001', $result->data_array['error_codes']);
  }

  public function testInvalidStateActive()
  {
    $this->session->customer_id = 123;

    $customer = new stdClass();
    $customer->preferred_language = 'EN';

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(123)
      ->will($this->returnValue($customer));


    $this->stateActions->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(['state' => 'Inactive']));

    $result = $this->api->portal__ApplyBalanceToStoredValue();

//    print_r($result);

    $this->assertContains('IN0001', $result->data_array['error_codes']);
  }

  public function testFailedSweepBalanceToStoredValue()
  {
    $this->session->customer_id = 123;

    $customer = new stdClass();
    $customer->preferred_language = 'EN';

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(123)
      ->will($this->returnValue($customer));

    $this->stateActions->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(['state' => 'Active']));

    $result = [];
    $result['success'] = false;
    $result['errors'] = ['Big Error', 'Another Error'];

    $this->payment->expects($this->any())
      ->method('funcSweepBalanceToStoredValue')
      ->with()
      ->will($this->returnValue($result));

    $result = $this->api->portal__ApplyBalanceToStoredValue();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testSuccess()
  {
    $this->session->customer_id = 123;

    $customer = new stdClass();
    $customer->preferred_language = 'EN';
    $customer->plan_expires = '2017-10-20 16:35:06.000';

    $this->customerRepo->expects($this->any())
      ->method('getCombinedCustomerByCustomerId')
      ->with(123)
      ->will($this->returnValue($customer));

    $this->stateActions->expects($this->any())
      ->method('getStateFromCustomerId')
      ->with(123)
      ->will($this->returnValue(['state' => 'Active']));

    $result = [];
    $result['success'] = true;
    $result['errors'] = [];

    $this->payment->expects($this->any())
      ->method('funcSweepBalanceToStoredValue')
      ->with()
      ->will($this->returnValue($result));

    $this->messenger->expects($this->any())
      ->method('enqueueImmediateSms')
      ->will($this->returnValue(new \Result(null, true)));

    $result = $this->api->portal__ApplyBalanceToStoredValue();

    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

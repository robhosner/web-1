<?php

use Ultra\Accounts\Account;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal\UpdateThrottleSpeed;
use Ultra\Lib\ApiErrors;
use Ultra\Lib\MiddleWare\Adapter\Control;
use Ultra\Mvne\Adapter;

class UpdateThrottleSpeedTest extends PHPUnit_Framework_TestCase
{

  /**
   * @var CustomerRepository
   */
  private $customerRepo;
  /**
   * @var Session
   */
  public $session;
  /**
   * @var UpdateThrottleSpeed
   */
  private $api;

  private $adapter;
  private $accountsRepository;
  private $configuration;
  private $customer;

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->session = $this->getMock(Session::class);
    $this->configuration = $this->getMock(Configuration::class);
    $this->adapter = $this->getMockBuilder(Adapter::class)
      ->setConstructorArgs([
        $this->getMock(Control::class),
        $this->customerRepo,
        $this->configuration
      ])
      ->getMock();
    $this->accountsRepository = $this->getMock(AccountsRepository::class);

    $this->api = new UpdateThrottleSpeed($this->session, $this->customerRepo, $this->adapter, $this->accountsRepository, $this->configuration);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->api->setInputValues(['targetSpeed' => '']);
    $this->session->customer_id = false;
    $result = $this->api->portal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerNotFound()
  {
    $this->api->setInputValues(['targetSpeed' => '']);

    $this->session->customer_id = 123;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['BRAND_ID'], true)
      ->will($this->returnValue(false));

    $result = $this->api->portal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testCustomerAccountNotFound()
  {
    $targetSpeed = 'full';
    $this->api->setInputValues(['targetSpeed' => '']);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123, 'brand_id' => 1]])
      ->setMethods(['canUpdateToThrottleSpeed'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('canUpdateToThrottleSpeed')
      ->with($targetSpeed)
      ->will($this->returnValue(false));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['BRAND_ID'], true)
      ->will($this->returnValue($this->customer));

    $this->accountsRepository->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(false));

    $result = $this->api->portal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testUnableToUpdateThrottleSpeed()
  {
    $targetSpeed = 'full';
    $this->api->setInputValues(['targetSpeed' => $targetSpeed]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123, 'brand_id' => 1]])
      ->setMethods(['canUpdateToThrottleSpeed'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('canUpdateToThrottleSpeed')
      ->with('full')
      ->will($this->returnValue(false));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['BRAND_ID'], true)
      ->will($this->returnValue($this->customer));

    $this->accountsRepository->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(new Account(['COS_ID' => 12345])));

    $this->configuration->expects($this->any())
      ->method('getThrottleSpeedFromKey')
      ->with(2.0)
      ->will($this->returnValue('full'));

    $result = $this->api->portal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
  }

  public function testMvneMakeitsoUpdateThrottleSpeedFails()
  {
    $targetSpeed = 'full';
    $this->api->setInputValues(['targetSpeed' => $targetSpeed]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123, 'brand_id' => 1]])
      ->setMethods(['canUpdateToThrottleSpeed'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('canUpdateToThrottleSpeed')
      ->with('full')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['BRAND_ID'], true)
      ->will($this->returnValue($this->customer));

    $this->accountsRepository->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(new Account(['COS_ID' => 12345])));

    $this->configuration->expects($this->any())
      ->method('getThrottleSpeedFromKey')
      ->with(2.0)
      ->will($this->returnValue('full'));

    $this->configuration->expects($this->any())
      ->method('getPlanFromCosId')
      ->with(12345)
      ->will($this->returnValue(123));

    $result = $this->api->portal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertContains('IN0002', $result->data_array['error_codes']);
  }

  public function testSuccesfulThrottleUpdate()
  {
    $targetSpeed = 'full';
    $this->api->setInputValues(['targetSpeed' => $targetSpeed]);

    $this->session->customer_id = 123;

    $this->customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123, 'brand_id' => 1]])
      ->setMethods(['canUpdateToThrottleSpeed'])
      ->getMock();

    $this->customer->expects($this->any())
      ->method('canUpdateToThrottleSpeed')
      ->with('full')
      ->will($this->returnValue(true));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, ['BRAND_ID'], true)
      ->will($this->returnValue($this->customer));

    $this->accountsRepository->expects($this->any())
      ->method('getAccountFromCustomerId')
      ->with(123, ['COS_ID'])
      ->will($this->returnValue(new Account(['COS_ID' => 12345])));

    $this->configuration->expects($this->any())
      ->method('getThrottleSpeedFromKey')
      ->with(2.0)
      ->will($this->returnValue('full'));

    $this->configuration->expects($this->any())
      ->method('getPlanFromCosId')
      ->with(12345)
      ->will($this->returnValue(123));

    $this->adapter->expects($this->any())
      ->method('mvneMakeitsoUpdateThrottleSpeed')
      ->with(123, 123, 'full')
      ->will($this->returnValue(['success' => 1]));

    $result = $this->api->portal__UpdateThrottleSpeed();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

<?php
namespace unit_tests\Portal;

use Session;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Api\Partner\Portal\ClearCreditCard;
use Ultra\Lib\ApiErrors;

class ClearCreditCardTest extends \PHPUnit_Framework_TestCase
{
  /**
   * @var ClearCreditCard
   */
  public  $api;
  private $session;
  private $customerRepo;

  public function setUp()
  {
    $this->session = $this->getMock(Session::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->api = new ClearCreditCard($this->session, $this->customerRepo);
    $this->api->result = new \Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
    $this->api->setInputValues([]);
  }

  public function testCustomerIsNotLoggedIn()
  {
    $this->session->customer_id = 0;
    $result = $this->api->portal__ClearCreditCard();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerIsNotFound()
  {
    $this->session->customer_id = 1;
    $result = $this->api->portal__ClearCreditCard();

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($this->session->customer_id, ['PREFERRED_LANGUAGE'], true)
      ->will($this->returnValue(false));

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }

  public function testVoidingCreditCardInformationFails()
  {
    $this->session->customer_id = 1;

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => $this->session->customer_id]])
      ->setMethods(['voidCreditCardInformation'])
      ->getMock();

    $customer->expects($this->any())
      ->method('voidCreditCardInformation')
      ->will($this->returnValue(false));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($this->session->customer_id, ['PREFERRED_LANGUAGE'], true)
      ->will($this->returnValue($customer));

    $result = $this->api->portal__ClearCreditCard();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testDisableCreditCardFails()
  {
    $this->session->customer_id = 1;

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => $this->session->customer_id]])
      ->setMethods(['voidCreditCardInformation', 'disableCreditCard'])
      ->getMock();

    $customer->expects($this->any())
      ->method('voidCreditCardInformation')
      ->will($this->returnValue(true));

    $result = new \Result();
    $result->fail();

    $customer->expects($this->any())
      ->method('disableCreditCard')
      ->will($this->returnValue($result));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($this->session->customer_id, ['PREFERRED_LANGUAGE'], true)
      ->will($this->returnValue($customer));

    $result = $this->api->portal__ClearCreditCard();

//    print_r($result);

    $this->assertContains('DB0001', $result->data_array['error_codes']);
  }

  public function testSuccessfullyClearingCreditCard()
  {
    $this->session->customer_id = 1;

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => $this->session->customer_id]])
      ->setMethods(['voidCreditCardInformation', 'disableCreditCard'])
      ->getMock();

    $customer->expects($this->any())
      ->method('voidCreditCardInformation')
      ->will($this->returnValue(true));

    $result = new \Result();
    $result->succeed();

    $customer->expects($this->any())
      ->method('disableCreditCard')
      ->will($this->returnValue($result));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with($this->session->customer_id, ['PREFERRED_LANGUAGE'], true)
      ->will($this->returnValue($customer));

    $result = $this->api->portal__ClearCreditCard();

//    print_r($result);

    $this->assertTrue($result->is_success());
  }
}

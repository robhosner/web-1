<?php

use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Lib\Api\Partner\Portal\GetE911SubscriberAddress;
use Ultra\Lib\ApiErrors;
use Ultra\Utilities\Common;

class GetE911SubscriberAddressTest extends PHPUnit_Framework_TestCase
{
  public $utils;
  public $session;
  public $customerRepo;

  /**
   * @var GetE911SubscriberAddress
   */
  public $api;

  public function setUp()
  {
    $this->utils = $this->getMock(Common::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->session = $this->getMock(Session::class);

    $this->api = new GetE911SubscriberAddress($this->utils, $this->session, $this->customerRepo);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  public function testInvalidSession()
  {
    $this->session->customer_id = false;
    $result = $this->api->portal__GetE911SubscriberAddress();

//    print_r($result);

    $this->assertContains('SE0007', $result->data_array['error_codes']);
  }

  public function testCustomerHasNoE911Address()
  {
    $this->session->customer_id = 123;

    $customer = $this->getMockBuilder(Customer::class)
      ->setConstructorArgs([['customer_id' => 123]])
      ->setMethods(['getAccAddressE911'])
      ->getMock();

    $customer->expects($this->any())
      ->method('getAccAddressE911')
      ->with()
      ->will($this->returnValue(null));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->returnValue($customer));

    $result = $this->api->portal__GetE911SubscriberAddress();
    $this->assertEmpty($result->data_array['address']);
  }

  public function testCustomerHasE911CantFindCustomer()
  {
    $this->session->customer_id = 123;

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->with(123, [], true)
      ->will($this->throwException(new MissingRequiredParametersException()));

    $result = $this->api->portal__GetE911SubscriberAddress();

//    print_r($result);

    $this->assertContains('VV0031', $result->data_array['error_codes']);
  }
}

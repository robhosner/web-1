<?php

use Ultra\Lib\Api\Partner\Portal\LookupOnlineOrderByActCode;
use Ultra\Lib\ApiErrors;
use Ultra\Orders\Repositories\Mssql\OrderRepository;
use Ultra\Sims\Repositories\Mssql\SimRepository;

class LookupOnlineOrderByActCodeTest extends PHPUnit_Framework_TestCase
{
  /**
   * @var LookupOnlineOrderByActCode
   */
  private $api;
  
  /**
   * @var SimRepository
   */
  private $simRepo;

  /**
   * @var OrderRepository
   */
  private $orderRepo;

  public function setUp()
  {
    // API setup
    $this->simRepo = $this->getMock(SimRepository::class);
    $this->orderRepo = $this->getMock(OrderRepository::class);

    $this->api = new LookupOnlineOrderByActCode($this->simRepo, $this->orderRepo);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }
  
  public function testMissingParameters()
  {
    $this->api->setInputValues(['actcode' => '']);
    $result = $this->api->portal__LookupOnlineOrderByActCode();

//    print_r($result);

    $this->assertContains('MP0001', $result->data_array['error_codes']);
  }

  public function testInvalidActCode()
  {
    $actcode = 'adsfasdfasd';

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue(false));

    $this->api->setInputValues(['actcode' => $actcode]);
    $result = $this->api->portal__LookupOnlineOrderByActCode();

//    print_r($result);

    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testNoOrdersFoundForIccid()
  {
    $actcode = '1234';
    $sim = new stdClass();
    $sim->ICCID_FULL = 1234567890;

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue($sim));

    $this->orderRepo->expects($this->any())
      ->method('getOrderByIccid')
      ->with($sim->ICCID_FULL,
        [
          'FIRST_NAME',
          'LAST_NAME',
          'EMAIL',
          'BIN',
          'LAST_FOUR',
          'AUTO_ENROLL'
        ]
      )
      ->will($this->returnValue(false));
    
    $this->api->setInputValues(['actcode' => $actcode]);
    $result = $this->api->portal__LookupOnlineOrderByActCode();

//    print_r($result);

    $this->assertContains('ND0001', $result->data_array['error_codes']);
  }

  public function testOrderFoundForIccid()
  {
    $actcode = '1234';
    $sim = new stdClass();
    $sim->ICCID_FULL = 1234567890;

    $this->simRepo->expects($this->any())
      ->method('getSimFromActCode')
      ->with($actcode)
      ->will($this->returnValue($sim));

    $order = new stdClass();
    $order->FIRST_NAME = 'Cameron';
    $order->LAST_NAME = 'Berlino';
    $order->EMAIL = 'cameron@ultra.me';
    $order->BIN = '123456';
    $order->LAST_FOUR = '9876';
    $order->AUTO_ENROLL = 1;

    $this->orderRepo->expects($this->any())
      ->method('getOrderByIccid')
      ->with($sim->ICCID_FULL,
        [
          'FIRST_NAME',
          'LAST_NAME',
          'EMAIL',
          'BIN',
          'LAST_FOUR',
          'AUTO_ENROLL'
        ]
      )
      ->will($this->returnValue($order));
    
    $this->api->setInputValues(['actcode' => $actcode]);
    $result = $this->api->portal__LookupOnlineOrderByActCode();

//    print_r($result);

    $this->assertEmpty($result->data_array['error_codes']);
    $this->assertContains($order->FIRST_NAME, $result->data_array['first_name']);
  }
}
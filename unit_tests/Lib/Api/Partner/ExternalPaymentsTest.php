<?php

use Ultra\Activations\Repositories\Mssql\ActivationHistoryRepository;
use Ultra\Lib\Api\Partner;

use Ultra\Lib\Api\Partner\ExternalPayments;

use Ultra\Dealers\Repositories\Mssql\DealersRepository;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\ApiErrors;
use Ultra\Payments\Payment;
use Ultra\Sims\Repositories\Mssql\SimRepository;
use Ultra\Configuration\Configuration;

define('MAX_ACCOUNT_BALANCE', 20000);


class ExternalPaymentsTest extends PHPUnit_Framework_TestCase
{
  private $api;
  protected $activationHistoryRepo;
  protected $dealersRepo;
  protected $customerRepo;
  protected $simRepo;
  protected $payment;
  protected $configuration;

  public function setUp()
  {
    $this->activationHistoryRepo = $this->getMock(ActivationHistoryRepository::class);
    $this->dealersRepo  = $this->getMock(DealersRepository::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->simRepo      = $this->getMock(SimRepository::class);
    $this->payment = $this->getMock(Payment::class);
    $this->configuration = $this->getMock(Configuration::class);

    $this->api = new ExternalPayments($this->activationHistoryRepo, $this->customerRepo, $this->dealersRepo, $this->simRepo, $this->payment, $this->configuration);
    $this->api->result = new Result();
    $this->api->defaultValues = [];
    $this->api->apiErrorHandler = $this->getMock(ApiErrors::class);
  }

  /**
   * Time difference with system time too large
   * @test
   * @expectedException \Exception
   */
  public function checkTimeSyncFail()
  {
    $method = new ReflectionMethod(ExternalPayments::class, 'checkTimeSync');
    $method->setAccessible(TRUE);
    $method->invoke($this->api, time() - 500);
  }

  /**
   * Time difference with system time too small
   * @test
   */
  public function checkTimeSyncPass()
  {
    try
    {
      $method = new ReflectionMethod(ExternalPayments::class, 'checkTimeSync');
      $method->setAccessible(TRUE);
      $method->invoke($this->api, time() - 50);

      $this->assertTrue(TRUE);
    }
    catch (\Exception $e)
    {
//      echo $e->getMessage();
      $this->fail();
    }
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function verifyDealerFailNoDealerFound()
  {
    $method = new ReflectionMethod(ExternalPayments::class, 'verifyDealer');
    $method->setAccessible(TRUE);

    $dealer_code = 'test';

    // no dealer found
    $this->dealersRepo->expects($this->any())
      ->method('getDealerFromCode')
      ->with($dealer_code)
      ->will($this->returnValue(FALSE));

    $method->invoke($this->api, $dealer_code);
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function verifyDealerFailNotActive()
  {
    $method = new ReflectionMethod(ExternalPayments::class, 'verifyDealer');
    $method->setAccessible(TRUE);

    $dealer_code = 'test';

    // dealer is not active
    $this->dealersRepo->expects($this->any())
      ->method('getDealerFromCode')
      ->with($dealer_code)
      ->will($this->returnValue((object)[
        'DealerSiteID' => 123,
        'ActiveFlag'   => 0
      ]));

      $method->invoke($this->api, $dealer_code, $checkActive = 1);
  }

  /**
   * @test
   */
  public function verifyDealerPass()
  {
    $method = new ReflectionMethod(ExternalPayments::class, 'verifyDealer');
    $method->setAccessible(TRUE);

    $dealer_code = 'test';

    try
    {
      // dealer is not active
      $this->dealersRepo->expects($this->any())
        ->method('getDealerFromCode')
        ->with($dealer_code)
        ->will($this->returnValue((object)[
          'DealerSiteID' => 123,
          'ActiveFlag'   => 1
        ]));

      $dealer = $method->invoke($this->api, $dealer_code, $checkActive = 1);

//      print_r($dealer);
    }
    catch (\Exception $e)
    {
//      echo $e->getMessage();
      $this->fail();
    }
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function verifySubscriberFailNotFound()
  {
    $phone_number = '7144959606';

    $method = new ReflectionMethod(ExternalPayments::class, 'verifySubscriber');
    $method->setAccessible(TRUE);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->with($phone_number)
      ->will($this->returnValue(FALSE));

    $subscriber = $method->invoke($this->api, $phone_number);
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function verifySubscriberFailInvalidPlanState()
  {
    $phone_number = '7144959606';

    $method = new ReflectionMethod(ExternalPayments::class, 'verifySubscriber');
    $method->setAccessible(TRUE);

    $this->customerRepo->expects($this->any())
      ->method('getCustomerFromMsisdn')
      ->with($phone_number)
      ->will($this->returnValue((object)[
        'plan_state' => 'INVALID'
      ]));

    $subscriber = $method->invoke($this->api, $phone_number);
  }

  /**
   * @test
   */
  public function verifySubscriberPass()
  {
    $phone_number = '7144959606';

    $method = new ReflectionMethod(ExternalPayments::class, 'verifySubscriber');
    $method->setAccessible(TRUE);

    try
    {
      $this->customerRepo->expects($this->any())
        ->method('getCustomerFromMsisdn')
        ->with($phone_number)
        ->will($this->returnValue((object)[
          'plan_state' => STATE_ACTIVE
        ]));

      $subscriber = $method->invoke($this->api, $phone_number);

//      print_r($subscriber);   
    }
    catch (\Exception $e)
    {
//      echo $e->getMessage();
      $this->fail();
    }
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function checkProductFailExcessBalance()
  {
    $method = new ReflectionMethod(ExternalPayments::class, 'checkProduct');
    $method->setAccessible(TRUE);

    $subscriber = (object)[
      'BALANCE'      => MAX_ACCOUNT_BALANCE,
      'stored_value' => 100
    ];
    $product    = 'ULTRA_PLAN_RECHARGE';
    $subproduct = 'L19';
    $amount     = '1900';

    $this->payment->expects($this->any())
      ->method('checkExcessBalance')
      ->with($subscriber, $amount)
      ->will($this->returnValue(true));

    $subscriber = $method->invoke($this->api, $subscriber, $product, $subproduct, $amount);
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function checkProductFailEmptySubproduct()
  {
    $method = new ReflectionMethod(ExternalPayments::class, 'checkProduct');
    $method->setAccessible(TRUE);

    $subscriber = (object)[
      'BALANCE'      => MAX_ACCOUNT_BALANCE,
      'stored_value' => 100
    ];
    $product    = 'ULTRA_PLAN_RECHARGE';
    $subproduct = '';
    $amount     = '1900';

    $subscriber = $method->invoke($this->api, $subscriber, $product, $subproduct, $amount);
  }

  /**
   * @test
   * @expectedException \Exception
   */
  public function checkProductFailSubproductNotEqualAmount()
  {
    $method = new ReflectionMethod(ExternalPayments::class, 'checkProduct');
    $method->setAccessible(TRUE);

    $subscriber = (object)[
      'BALANCE'      => 0,
      'stored_value' => 100
    ];
    $product    = 'ULTRA_PLAN_RECHARGE';
    $subproduct = 'L29';
    $amount     = '1900';

    $subscriber = $method->invoke($this->api, $subscriber, $product, $subproduct, $amount);
  }

  /**
   * @test
   */
  public function checkProductPass()
  {
    $method = new ReflectionMethod(ExternalPayments::class, 'checkProduct');
    $method->setAccessible(TRUE);

    $subscriber = (object)[
      'BALANCE'                => 0,
      'stored_value'           => 0,
      'MONTHLY_RENEWAL_TARGET' => 'L49',
      'COS_ID'                 => '98280',
      'plan_state'             => STATE_ACTIVE
    ];

    $product    = 'ULTRA_PLAN_RECHARGE';
    $subproduct = 'L19';
    $amount     = '1900';

    $subscriber = $method->invoke($this->api, $subscriber, $product, $subproduct, $amount);
  }
}
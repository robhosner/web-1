<?php

namespace unit_tests;

use Ultra\Lib\Api\Traits\FlexHandler;
use Ultra\Configuration\Configuration;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Lib\Services\SharedData;

class FlexHandlerTest extends \PHPUnit_Framework_TestCase
{
  private $flexHandler;
  private $config;
  private $familyAPI;
  private $sharedData;

  public function setUp()
  {
    $this->mock = $this->getMockForTrait(FlexHandler::class);

    // mock dependencies
    $this->config = $this->getMock(Configuration::class);
    $this->config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));
    $this->config->expects($this->any())
      ->method('getSharedDataConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));

    $this->familyAPI = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([
        $this->config
      ])
      ->getMock();
    $this->sharedData = $this->getMockBuilder(SharedData::class)
      ->setConstructorArgs([
        $this->config
      ])
      ->getMock();

    $this->flexHandler = $this->getMockForTrait(FlexHandler::class);
    $this->flexHandler->familyAPI = $this->familyAPI;

    // mock errException method
    $this->flexHandler->expects($this->any())
      ->method('errException')
      ->will($this->throwException(new \Exception));
  }

  public function test_validateInviteCode_InvalidErrors()
  {
    $familyResult = new \Result();
    $familyResult->add_error('whoops');

    $this->familyAPI->expects($this->once())
      ->method('getFamilyIDByInviteCode')
      ->will($this->returnValue($familyResult));

    $this->setExpectedException(\Exception::class);
    $this->flexHandler->validateInviteCode('invitecode');
  }

  public function test_validateInviteCode_InvalidNoErrors()
  {
    $familyResult = new \Result();

    $this->familyAPI->expects($this->once())
      ->method('getFamilyIDByInviteCode')
      ->will($this->returnValue($familyResult));

    $this->setExpectedException(\Exception::class);
    $this->flexHandler->validateInviteCode('invitecode');
  }

  public function test_validateInviteCode_MissingFamilyID()
  {
    $familyResult = new \Result(null, true);
    $familyResult->data_array['family_id'] = null;

    $this->familyAPI->expects($this->once())
      ->method('getFamilyIDByInviteCode')
      ->will($this->returnValue($familyResult));

    $this->setExpectedException(\Exception::class);
    $this->flexHandler->validateInviteCode('invitecode');
  }

  public function test_validateInviteCode_Success()
  {
    $familyResult = new \Result(null, true);
    $familyResult->data_array['family_id'] = 1;

    $this->familyAPI->expects($this->once())
      ->method('getFamilyIDByInviteCode')
      ->will($this->returnValue($familyResult));

    try {
      $this->flexHandler->validateInviteCode('invitecode');
    } catch (\Exception $e) {
      $this->fail('Unexpected exception');
    }
  }

  public function test_joinFamily_InvalidErrors()
  {
    $familyResult = new \Result();
    $familyResult->add_error('whoops');

    $this->familyAPI->expects($this->once())
      ->method('joinFamily')
      ->will($this->returnValue($familyResult));

    $this->setExpectedException(\Exception::class);
    $this->flexHandler->joinFamily(1, 'invitecode');
  }

  public function test_joinFamily_InvalidNoErrors()
  {
    $familyResult = new \Result();

    $this->familyAPI->expects($this->once())
      ->method('joinFamily')
      ->will($this->returnValue($familyResult));

    $this->setExpectedException(\Exception::class);
    $this->flexHandler->joinFamily(1, 'invitecode');
  }

  public function test_joinFamily_Success()
  {
    $familyResult = new \Result(null, true);

    $this->familyAPI->expects($this->once())
      ->method('joinFamily')
      ->will($this->returnValue($familyResult));

    try {
      $this->flexHandler->joinFamily(1, 'invitecode');
    } catch (\Exception $e) {
      $this->fail('Unexpected exception');
    }
  }

  public function test_createFamily_InvalidErrors()
  {
    $familyResult = new \Result();
    $familyResult->add_error('whoops');

    $this->familyAPI->expects($this->once())
      ->method('createCustomerFamily')
      ->will($this->returnValue($familyResult));

    $this->setExpectedException(\Exception::class);
    $this->flexHandler->createFamily(1);
  }

  public function test_createFamily_InvalidNoErrors()
  {
    $familyResult = new \Result();

    $this->familyAPI->expects($this->once())
      ->method('createCustomerFamily')
      ->will($this->returnValue($familyResult));

    $this->setExpectedException(\Exception::class);
    $this->flexHandler->createFamily(1);
  }

  public function test_createFamily_Success()
  {
    $familyResult = new \Result(null, true);
    $familyResult->data_array['invite_code'] = 'invitecode';

    $this->familyAPI->expects($this->once())
      ->method('createCustomerFamily')
      ->will($this->returnValue($familyResult));

    $this->flexHandler->expects($this->once())
      ->method('addToOutput')
      ->with('invite_code', 'invitecode');

    try {
      $this->flexHandler->createFamily(1);
    } catch (\Exception $e) {
      $this->fail('Unexpected exception');
    }
  }
}

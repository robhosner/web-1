<?php
namespace unit_tests;

use Ultra\Lib\Api\Traits\CCHandler;
use Ultra\Configuration\Configuration;
use Ultra\Customers\Interfaces\CustomerRepository;
use Ultra\CreditCards\Interfaces\CreditCardRepository;
use Ultra\Exceptions\MissingRequiredParametersException;
use Ultra\Exceptions\UnhandledCCProcessorException;
use Ultra\Exceptions\SaveMethodFailedException;

class CCHandlerTraitTest extends \PHPUnit_Framework_TestCase
{
  private $mock;
  private $configuration;
  private $customerRepo;
  private $creditCardRepo;

  public function setUp()
  {
    $this->mock = $this->getMockForTrait(CCHandler::class);

    // mock dependencies
    $this->configuration = $this->getMock(Configuration::class);
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->creditCardRepo = $this->getMock(CreditCardRepository::class);
  }

  public function testMissingParamExceptions()
  {
    $required = [
      'customer_id', 'account_cc_exp',
      'processor', 'bin', 'last_four', 'token'
    ];

    $fullParams = [
      'customer_id'     => 123,
      'account_cc_exp'  => '1220',
      'processor'       => 'mes',
      'bin'             => 123123,
      'last_four'       => 4321,
      'token'           => 'somekindatoken'
    ];

    $numExceptions = 0;
    for ($i = 0; $i < count($required); $i++) {
      $missing = $fullParams;
      unset($missing[array_keys($fullParams)[$i]]);

      try {
        $this->mock->validateAndMapParameters($this->configuration, $missing);
      } catch (MissingRequiredParametersException $e) {
        $numExceptions++;
      }
    }

    $this->assertEquals($i, $numExceptions);
  }

  public function testSuccessParamValidation()
  {
    $fullParams = [
      'customer_id'     => 123,
      'account_cc_exp'  => '1220',
      'cc_postal_code'  => 90210,
      'processor'       => 'mes',
      'bin'             => 123123,
      'last_four'       => 4321,
      'token'           => 'somekindatoken',
      'account_cc_cvv'  => 123
    ];

    $result = $this->mock->validateAndMapParameters($this->configuration, $fullParams);

    // check optional params added
    $expected = [
      'customer_id'     => 123,
      'cc_postal_code'  => 90210,
      'processor'       => 'mes',
      'bin'             => 123123,
      'last_four'       => 4321,
      'token'           => 'somekindatoken',
      'cvv'             => null,
      'cc_name'         => null,
      'cc_address1'     => null,
      'cc_address2'     => null,
      'cc_city'         => null,
      'cc_country'      => null,
      'cc_state_region' => null,
      'expires_date'    => '1220',
      'cvv'             => 123
    ];

    $this->assertEquals($result, $expected);
  }

  public function testUnhandledCCProcessorException()
  {
    $this->setExpectedException(UnhandledCCProcessorException::class);

    $fullParams = [
      'customer_id'     => 123,
      'account_cc_exp'  => '1220',
      'cc_postal_code'  => 90210,
      'processor'       => 'mes',
      'bin'             => 123123,
      'last_four'       => 4321,
      'token'           => 'somekindatoken',
      'account_cc_cvv'  => 123
    ];

    // mock getCCProcessorByName processor config not found
    $this->configuration->expects($this->once())
      ->method('getCCProcessorByName')
      ->will($this->returnValue([]));

    $this->customerRepo->expects($this->once())
      ->method('getCustomerById')
      ->will($this->returnValue(
      (object)[
        'customer_id' => 123,
        'preferred_language'  => 'EN',
        'brand_id' => 1
      ]
    ));
    $this->mock->validateAndSaveToken(
      $this->configuration,
      $this->customerRepo,
      $this->creditCardRepo,
      $fullParams
    );
  }
}

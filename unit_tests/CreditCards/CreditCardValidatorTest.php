<?php

use Ultra\Configuration\Configuration;
use Ultra\CreditCards\CreditCardValidator;
use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Lib\DB\Merchants\MeS;
use Ultra\Lib\DB\Merchants\Tokenizer;
use Ultra\Utilities\Validator;
use Ultra\Lib\Services\PaymentAPI;

class CreditCardValidatorTest extends \PHPUnit_Framework_TestCase
{
  private $validator;
  private $paymentAPI;
  private $mes;
  private $config;
  private $tokenizer;

  /**
   * @var CreditCardValidator
   */
  private $creditCardValidator;

  public function setUp()
  {
    $this->validator = $this->getMock(Validator::class);
    $this->config = $this->getMock(Configuration::class);

    $paymentAPIConfig = [
      'host'      => 'web-qa-08:3040',
      'basepath'  => '/v1',
      'jwtSecret' => 'shhhhhhared-secret'
    ];
    $this->config->expects($this->any())
      ->method('getPaymentAPIConfig')
      ->will($this->returnValue($paymentAPIConfig));

    $this->mes = $this->getMock(MeS::class, [], [$this->config]);
    $this->tokenizer = $this->getMock(Tokenizer::class);

    $this->creditCardValidator = new CreditCardValidator(
      $this->mes,
      $this->validator,
      $this->config,
      $this->tokenizer
    );

    parent::setUp();
  }

  public function testCreditCardThrowsExceptionForMissingParameter()
  {
    $this->validator->expects($this->any())
      ->method('validatorDateformat')
      ->with('expires_date', '1')
      ->will($this->returnValue(['ERR_API_INVALID_ARGUMENTS: the given cvv is invalid', 'VV0062']));

    $this->setExpectedException(InvalidObjectCreationException::class, 'ERR_API_INVALID_ARGUMENTS: the given cvv is invalid');

    $this->creditCardValidator->validate(
      [
        'customer_id' => 123456,
        'account_cc_cvv' => '',
        'cc_address1' => '1550 Scenic Ave',
        'cc_address2' => '100',
        'cc_city' => 'Costa Mesa',
        'cc_state_region' => 'CA',
        'cc_postal_code' => '92626',
        'cc_name' => 'Sharayu Ultrasmith',
        'cc_country' => 'US',
        'expires_date' => '1',
        'token' => '6dd058adde483774b8424ed3554e6da7',
        'bin' => '123454',
        'last_four' => '1234',
      ]
    );
  }

  public function testMissingRequiredParameters()
  {
    $this->setExpectedException(InvalidObjectCreationException::class, 'Missing required parameters.');

    $this->creditCardValidator->validate(
      [
        'account_cc_cvv' => '',
        'cc_address1' => '1550 Scenic Ave',
        'cc_address2' => '100',
        'cc_city' => 'Costa Mesa',
        'cc_state_region' => 'CA',
        'cc_postal_code' => '92626',
        'cc_name' => 'Sharayu Ultrasmith',
        'cc_country' => 'US',
        'expires_date' => '1216',
        'token' => '6dd058adde483774b8424ed3554e6da7',
        'bin' => '123454',
        'last_four' => '1234',
      ]
    );
  }

  public function testCreditCardTransactionsDisabled()
  {
    $this->config->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(false));

    $this->setExpectedException(InvalidObjectCreationException::class, 'Credit Card transactions have been disabled temporarily');

    $this->creditCardValidator->validate(
      [
        'customer_id' => 123456,
        'account_cc_cvv' => '123',
        'cc_address1' => '1550 Scenic Ave',
        'cc_address2' => '100',
        'cc_city' => 'Costa Mesa',
        'cc_state_region' => 'CA',
        'cc_postal_code' => '92626',
        'cc_name' => 'Sharayu Ultrasmith',
        'cc_country' => 'US',
        'expires_date' => '1216',
        'token' => '6dd058adde483774b8424ed3554e6da7',
        'bin' => '123454',
        'last_four' => '1234',
      ]
    );
  }

  public function testCreditCardAlwaysFails()
  {
    $this->config->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(true));

    $this->tokenizer->expects($this->any())
      ->method('alwaysFailAccount')
      ->will($this->returnValue(true));

    $this->setExpectedException(InvalidObjectCreationException::class, 'Always Fail Account match');

    $this->creditCardValidator->validate(
      [
        'customer_id' => 123456,
        'account_cc_cvv' => '123',
        'cc_address1' => '1550 Scenic Ave',
        'cc_address2' => '100',
        'cc_city' => 'Costa Mesa',
        'cc_state_region' => 'CA',
        'cc_postal_code' => '92626',
        'cc_name' => 'Sharayu Ultrasmith',
        'cc_country' => 'US',
        'expires_date' => '1216',
        'token' => '6dd058adde483774b8424ed3554e6da7',
        'bin' => '123454',
        'last_four' => '1234',
      ]
    );
  }

  public function testCreditCardAlwaysSucceeds()
  {
    $this->config->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(true));

    $this->tokenizer->expects($this->any())
      ->method('alwaysFailAccount')
      ->will($this->returnValue(false));

    $this->tokenizer->expects($this->any())
      ->method('alwaysSucceedAccount')
      ->will($this->returnValue(true));

    $this->creditCardValidator->validate(
      [
        'customer_id' => 123456,
        'account_cc_cvv' => '123',
        'cc_address1' => '1550 Scenic Ave',
        'cc_address2' => '100',
        'cc_city' => 'Costa Mesa',
        'cc_state_region' => 'CA',
        'cc_postal_code' => '92626',
        'cc_name' => 'Sharayu Ultrasmith',
        'cc_country' => 'US',
        'expires_date' => '1216',
        'token' => '6dd058adde483774b8424ed3554e6da7',
        'bin' => '123454',
        'last_four' => '1234',
      ]
    );
  }

  public function testCreditCardFailsAndHasUserErrors()
  {
    $this->config->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(true));

    $this->tokenizer->expects($this->any())
      ->method('alwaysFailAccount')
      ->will($this->returnValue(false));

    $this->tokenizer->expects($this->any())
      ->method('alwaysSucceedAccount')
      ->will($this->returnValue(false));

    $result = new \Result;
    $result->add_errors([
      'user_errors' => [
        'EN' => 'We were unable to process your Ultra Mobile transaction due to a system error. Please retry - visit http://m.ultra.me/#/update - calling 222 or go to the retailer',
      ]
    ]);

    $this->mes->expects($this->any())
      ->method('verify')
      ->will($this->returnValue($result));

    $this->setExpectedException(InvalidObjectCreationException::class, 'ERR_API_INTERNAL: Credit Card Gateway Validation Failed - (We were unable to process your Ultra Mobile transaction due to a system error. Please retry - visit http://m.ultra.me/#/update - calling 222 or go to the retailer)');

    $this->creditCardValidator->validate(
      [
        'customer_id' => 123456,
        'account_cc_cvv' => '123',
        'cc_address1' => '1550 Scenic Ave',
        'cc_address2' => '100',
        'cc_city' => 'Costa Mesa',
        'cc_state_region' => 'CA',
        'cc_postal_code' => '92626',
        'cc_name' => 'Sharayu Ultrasmith',
        'cc_country' => 'US',
        'expires_date' => '1216',
        'token' => '6dd058adde483774b8424ed3554e6da7',
        'bin' => '123454',
        'last_four' => '1234',
      ]
    );
  }

  public function testCreditCardFailsAndHasNoUserErrors()
  {
    $this->config->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(true));

    $this->tokenizer->expects($this->any())
      ->method('alwaysFailAccount')
      ->will($this->returnValue(false));

    $this->tokenizer->expects($this->any())
      ->method('alwaysSucceedAccount')
      ->will($this->returnValue(false));

    $result = new \Result;
    $result->add_error('We were unable to process your Ultra Mobile transaction due to a system error. Please retry - visit http://m.ultra.me/#/update - calling 222 or go to the retailer');

    $this->mes->expects($this->any())
      ->method('verify')
      ->will($this->returnValue($result));

    $this->setExpectedException(InvalidObjectCreationException::class, 'ERR_API_INTERNAL: Credit Card Gateway Validation Failed - (We were unable to process your Ultra Mobile transaction due to a system error. Please retry - visit http://m.ultra.me/#/update - calling 222 or go to the retailer)');

    $this->creditCardValidator->validate(
      [
        'customer_id' => 123456,
        'account_cc_cvv' => '123',
        'cc_address1' => '1550 Scenic Ave',
        'cc_address2' => '100',
        'cc_city' => 'Costa Mesa',
        'cc_state_region' => 'CA',
        'cc_postal_code' => '92626',
        'cc_name' => 'Sharayu Ultrasmith',
        'cc_country' => 'US',
        'expires_date' => '1216',
        'token' => '6dd058adde483774b8424ed3554e6da7',
        'bin' => '123454',
        'last_four' => '1234',
      ]
    );
  }

  public function testCreditCardPasses()
  {
    $this->config->expects($this->any())
      ->method('ccTransactionsEnabled')
      ->will($this->returnValue(true));

    $this->tokenizer->expects($this->any())
      ->method('alwaysFailAccount')
      ->will($this->returnValue(false));

    $this->tokenizer->expects($this->any())
      ->method('alwaysSucceedAccount')
      ->will($this->returnValue(false));

    $result = new \Result;
    $result->add_data_array([
      'cvv_validation' => 'Y',
      'avs_validation' => 'N'
    ]);

    $result->succeed();

    $this->mes->expects($this->any())
      ->method('verify')
      ->will($this->returnValue($result));

    $creditCard = $this->creditCardValidator->validate(
      [
        'customer_id' => 123456,
        'account_cc_cvv' => '123',
        'cc_address1' => '1550 Scenic Ave',
        'cc_address2' => '100',
        'cc_city' => 'Costa Mesa',
        'cc_state_region' => 'CA',
        'cc_postal_code' => '92626',
        'cc_name' => 'Sharayu Ultrasmith',
        'cc_country' => 'US',
        'expires_date' => '1216',
        'token' => '6dd058adde483774b8424ed3554e6da7',
        'bin' => '123454',
        'last_four' => '1234',
      ]
    );

    $this->assertEquals('Y', $creditCard->cvv_validation);
    $this->assertEquals('N', $creditCard->avs_validation);
  }
}

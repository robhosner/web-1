<?php

use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Customers\SendFamilyReplenishmentSms;
use Ultra\Lib\Services\FamilyAPI;
use Ultra\Messaging\Messenger;

class SendFamilyReplenishmentSmsTest extends PHPUnit_Framework_TestCase
{
  private $customerRepo;
  private $familyApi;
  private $messenger;

  /**
   * @var SendFamilyReplenishmentSms
   */
  private $class;

  public function setUp()
  {
    $this->customerRepo = $this->getMock(CustomerRepository::class);
    $this->messenger = $this->getMock(Messenger::class);

    $config = $this->getMock(Configuration::class);
    $config->expects($this->any())
      ->method('getFamilyAPIConfig')
      ->will($this->returnValue(['host' => 'test', 'basepath' => 'test']));
    $this->familyApi = $this->getMockBuilder(FamilyAPI::class)
      ->setConstructorArgs([$config,])
      ->setMethods(['get', 'getFamilyByCustomerID'])
      ->getMock();

    $this->class = new SendFamilyReplenishmentSms($this->familyApi, $this->customerRepo, $this->messenger);
  }

  public function testSendSmsFamilyNotFound()
  {
    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue(new Result()));

    $result = $this->class->sendSms(1);

    $this->assertFalse($result);
  }

  public function testSendSmsParentNotFound()
  {
    $family = new Result();
    $family->succeed();
    $family->add_to_data_array('parentCustomerId', 1);
    $family->add_to_data_array('payForFamily', true);
    $family->add_to_data_array('members', []);


    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($family));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue(false));

    $result = $this->class->sendSms(1);

    $this->assertFalse($result);
  }

  public function testSendSmsPayForFamilyAutoRechargeOn()
  {
    $family = new Result();
    $family->succeed();
    $family->add_to_data_array('parentCustomerId', 1);
    $family->add_to_data_array('payForFamily', true);
    $family->add_to_data_array('members', []);

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($family));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue((object) ['monthly_cc_renewal' => 1]));

    $this->messenger->expects($this->exactly(1))
      ->method('enqueueImmediateSms')
      ->with(1, 'flex_pay_for_family_auto_recharge_on', []);

    $result = $this->class->sendSms(1);

    $this->assertTrue($result);
  }

  public function testSendSmsParentAutoRechargeOffPayForFamilyOn()
  {
    $family = new Result();
    $family->succeed();
    $family->add_to_data_array('parentCustomerId', 1);
    $family->add_to_data_array('payForFamily', true);
    $family->add_to_data_array('members', []);

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($family));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue((object) ['monthly_cc_renewal' => 0]));

    $this->messenger->expects($this->exactly(1))
      ->method('enqueueImmediateSms')
      ->with(1, 'flex_parent_auto_recharge_off_pay_for_family_on', []);

    $result = $this->class->sendSms(1);

    $this->assertTrue($result);
  }

  public function testSendSmsParentAutoRechargeOffPayForFamilyOff()
  {
    $family = new Result();
    $family->succeed();
    $family->add_to_data_array('parentCustomerId', 1);
    $family->add_to_data_array('payForFamily', false);
    $family->add_to_data_array('members', []);

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($family));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue((object) ['monthly_cc_renewal' => 0]));

    $this->messenger->expects($this->exactly(1))
      ->method('enqueueImmediateSms')
      ->with(1, 'flex_parent_auto_recharge_off_pay_for_family_off', []);

    $result = $this->class->sendSms(1);

    $this->assertTrue($result);
  }

  public function testSendSmsChildAutoRechargeOff()
  {
    $family = new Result();
    $family->succeed();
    $family->add_to_data_array('parentCustomerId', 1);
    $family->add_to_data_array('payForFamily', false);
    $family->add_to_data_array('members', [['customerId' => 2]]);

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($family));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue((object) ['monthly_cc_renewal' => 1]));

    $this->messenger->expects($this->exactly(1))
      ->method('enqueueImmediateSms')
      ->with(2, 'flex_child_auto_recharge_off', []);

    $result = $this->class->sendSms(2);

    $this->assertTrue($result);
  }

  public function testSendSmsNothingToSend()
  {
    $family = new Result();
    $family->succeed();
    $family->add_to_data_array('parentCustomerId', 1);
    $family->add_to_data_array('payForFamily', false);
    $family->add_to_data_array('members', [['customerId' => 2]]);

    $this->familyApi->expects($this->any())
      ->method('getFamilyByCustomerID')
      ->will($this->returnValue($family));

    $this->customerRepo->expects($this->any())
      ->method('getCustomerById')
      ->will($this->returnValue((object) ['monthly_cc_renewal' => 1]));

    $result = $this->class->sendSms(3);

    $this->assertFalse($result);
  }
}

<?php

use Ultra\Exceptions\InvalidObjectCreationException;
use Ultra\Mvne\Commands\RemoveWIFISocCommand;

class RemoveWIFISocCommandTest extends PHPUnit_Framework_TestCase
{
  public function testFailsToCreateRemoveWIFISocCommand()
  {
    $commandInfo = [
      'actionUUID'         => 1234,
      'msisdn'             => 12345,
      'iccid'              => 123456,
      'customer_id'        => 123,
      'wholesale_plan'     => 1,
      'ultra_plan'         => 22,
    ];

    $this->setExpectedException(InvalidObjectCreationException::class);

    new RemoveWIFISocCommand($commandInfo);
  }

  public function testCreateRemoveWIFISocCommand()
  {
    $commandInfo = [
      'actionUUID'         => 1234,
      'msisdn'             => 12345,
      'iccid'              => 123456,
      'customer_id'        => 123,
      'wholesale_plan'     => 1,
      'ultra_plan'         => 22,
      'preferred_language' => 1,
    ];

    $command = new RemoveWIFISocCommand($commandInfo);
    $this->assertEquals('N-WFC|REMOVE', $command->option);
  }
}

<?php

require_once 'classes/PlanConfig.php';
include_once('Ultra/Constants.php');
include_once('db.php');

# "IndiaLD USA Monthly"

define("COSID_INDIALD_USA_MONTHLY_1000",find_credential('plans/ILD/INDIALD_USA_MONTHLY_1000/cos_id'));
define("COSID_INDIALD_USA_MONTHLY_1500",find_credential('plans/ILD/INDIALD_USA_MONTHLY_1500/cos_id'));
define("COSID_INDIALD_USA_MONTHLY_2000",find_credential('plans/ILD/INDIALD_USA_MONTHLY_2000/cos_id'));
define("COSID_INDIALD_USA_MONTHLY_500", find_credential('plans/ILD/INDIALD_USA_MONTHLY_500/cos_id'));
define("COSID_INDIALD_USA_MONTHLY_250", find_credential('plans/ILD/INDIALD_USA_MONTHLY_250/cos_id'));

# "IndiaLD Monthly"

define("COSID_INDIALD_MONTHLY_1000",find_credential('plans/ILD/INDIALD_MONTHLY_1000/cos_id'));
define("COSID_INDIALD_MONTHLY_2000",find_credential('plans/ILD/INDIALD_MONTHLY_2000/cos_id'));
define("COSID_INDIALD_MONTHLY_3000",find_credential('plans/ILD/INDIALD_MONTHLY_3000/cos_id'));
define("COSID_INDIALD_MONTHLY_500", find_credential('plans/ILD/INDIALD_MONTHLY_500/cos_id'));
define("COSID_INDIALD_MONTHLY_1500",find_credential('plans/ILD/INDIALD_MONTHLY_1500/cos_id'));
define("COSID_INDIALD_MONTHLY_250", find_credential('plans/ILD/INDIALD_MONTHLY_250/cos_id'));
define("COSID_INDIALD_MONTHLY_5000",find_credential('plans/ILD/INDIALD_MONTHLY_5000/cos_id'));

# "India LD Monthly"

define("COSID_INDIA_LD_MONTHLY_250", find_credential('plans/ILD/INDIA_LD_MONTHLY_250/cos_id'));
define("COSID_INDIA_LD_MONTHLY_500", find_credential('plans/ILD/INDIA_LD_MONTHLY_500/cos_id'));
define("COSID_INDIA_LD_MONTHLY_1000",find_credential('plans/ILD/INDIA_LD_MONTHLY_1000/cos_id'));
define("COSID_INDIA_LD_MONTHLY_1500",find_credential('plans/ILD/INDIA_LD_MONTHLY_1500/cos_id'));
define("COSID_INDIA_LD_MONTHLY_2000",find_credential('plans/ILD/INDIA_LD_MONTHLY_2000/cos_id'));
define("COSID_INDIA_LD_MONTHLY_3000",find_credential('plans/ILD/INDIA_LD_MONTHLY_3000/cos_id'));
define("COSID_INDIA_LD_MONTHLY_5000",find_credential('plans/ILD/INDIA_LD_MONTHLY_5000/cos_id'));

# not monthly

define("COSID_1_5_CALLING_CARD",  find_credential('plans/ILD/1_5_CALLING_CARD/cos_id'));
define("COSID_1_5_CALLING_CARD_T",find_credential('plans/ILD/1_5_CALLING_CARD_T/cos_id'));
define("COSID_QUARTER_CALLS",  find_credential('plans/ILD/QUARTER_CALLS/cos_id'));
define("COSID_QUARTER_CALLS_T",find_credential('plans/ILD/QUARTER_CALLS_T/cos_id'));
define("COSID_PAYG_MINUTES",  find_credential('plans/ILD/PAYG_MINUTES/cos_id'));
define("COSID_PAYG_MINUTES_T",find_credential('plans/ILD/PAYG_MINUTES_T/cos_id'));
define("COSID_UV_CARD",find_credential('plans/ILD/UV_CARD/cos_id'));
define("COSID_NO_PLAN",find_credential('plans/ILD/NO_PLAN/cos_id'));

$planConfig = \PlanConfig::Instance();
$planConfig->defineConstants();

/**
 * get_cos_id_from_plan
 *
 * Given a plan name, return the associated COS_ID
 *
 * @return integer
 */
function get_cos_id_from_plan($plan)
{
  // Primo default COS_ID
  if ( $plan === 'INTL00' )
    return 2;

  $map = cos_id_plan_map();

  if ( !isset($map[ $plan ]) )
  {
    dlog('',"WARNING: no COS_ID found for plan < $plan >");

    return '';
  }
  else
    return $map[ $plan ];
}

/**
 * cos_id_plan_description
 *
 * Association between COS_IDs and plan descriptions
 *
 * @return string
 */
function cos_id_plan_description($cos_id)
{
  // [ COS_ID => DESCRIPTION, .. ]
  $planConfig = PlanConfig::Instance();
  $cos_id_plan_description = $planConfig->cosIdPlanDescription();

  if ( isset($cos_id_plan_description[$cos_id]) )
    return $cos_id_plan_description[$cos_id];
  else
  {
    dlog('',"ERROR: cos_id_plan_description invoked with invalid cos_id : $cos_id");
    return 0;
  }
}

function cos_id_plan_map()
{
  // [ LONG_NAME => COS_ID, SHORT_NAME => COS_ID, .. ]
  $planConfig = PlanConfig::Instance();
  return $planConfig->cosIdPlanMap();
}

function get_brand_id_from_cos_id($cos_id)
{
  $planConfig = PlanConfig::Instance();
  return $planConfig->getPlanConfigItem($cos_id, 'brand_id');
}

// what the MM plan actually is
function monthly_plan_map()
{
  // TODO: move to PlanConfig
  return array(
    COSID_ULTRA_MULTI_TWENTY_NINE => 'L29'
  );
}

/**
 * is_multi_month_plan
 * @param Integer plan COS_ID
 * @return Bolean TRUE if multi month plan FALSE otherwise
 */
function is_multi_month_plan($given_cos_id)
{
  $map = monthly_plan_map();

  foreach ($map as $cos_id => $plan_id)
  {
    if ($given_cos_id == $cos_id)
      return TRUE;
  }

  return FALSE;
}

function plan_strings_map()
{
  // [ SHORT_NAME => LONG_NAME ]
  $planConfig = PlanConfig::Instance();
  return $planConfig->planStringsMap();
}

function get_short_name_from_plan_name($plan_name)
{
  $map = plan_strings_map();

  foreach ($map as $short_name => $plan)
    if ($plan == $plan_name)
      return $short_name;

  return null;
}

function get_plan_name_from_short_name($plan_short_name)
{
  $map = plan_strings_map();

  if ( isset($map[$plan_short_name]) )
  { return $map[$plan_short_name]; }
  else
  { return NULL;                   }
}

function get_plan_from_cos_id($given_cos_id)
{
  $map  = cos_id_plan_map();

  $plan = '';

  foreach( $map as $plan_id => $cos_id )
  {
    if ( $cos_id == $given_cos_id ) { $plan = $plan_id; }
  }

  return $plan;
}

function get_monthly_plan_from_cos_id($given_cos_id)
{
  $map = monthly_plan_map();

  $plan = '';

  foreach( $map as $cos_id => $plan_id )
  {
    if ( $cos_id == $given_cos_id )
      $plan = $plan_id;
  }

  return ( empty($plan) ) ? get_plan_from_cos_id($given_cos_id) : $plan;
}

function data_product_list()
{
  return array(

    # 'Data SOC' =>                     MB,  $

    'PayGo_200_250' =>
    [
      'Block at 200 MB (PayGo)',       200,  2.5
    ],
    'PayGo_500_500' =>
    [
      'Block at 500 MB (PayGo)',       500,  5
    ],
    'PayGo_1024_1024' =>
    [
      'Block at 1 GB (PayGo)',        1024,  10
    ],
    'PayGo_2048_2048' =>
    [
      'Block at 2 GB (PayGo)',        2048,  20
    ],
    'PayGo_3072_3072' =>
    [
      'Block at 3 GB (PayGo)',        3072,  20
    ],
    'Bucketed_1000_1000' =>
    [
      'Throttle at 1 GB (Bucketed)',  1000,  10
    ]
  );
}

function voice_product_list()
{
  return array(

    # 'Voice SOC' =>   Effective Voice Minutes, Cash Value (cents)
    '50_100'   =>  array(  50,  100  ),
    '125_250'  =>  array( 125,  250  ),
    '250_500'  =>  array( 250,  500  ),
    '500_500'  =>  array( 500,  500  ),
    '1000_1000' => array( 1000, 1000 ),
    '2000_2000' => array( 2000, 2000 )
  );
}

function data_socs_list()
{
  $data_product_list = data_product_list();

  $underscoreObject = new __ ;

  return $underscoreObject->map($data_product_list, function($e) { return $e[0]; });
}


/**
 * reserved for future use
 * as of 2016-01 all plans have orange SIMs but in the future some may not
 */
function is_orange_plan($plan)
{
  return TRUE;
  #return ( ( $plan === 'NINETEEN' ) || ( $plan === 'TWENTY_NINE' ) );
}

function get_ultra_plans_short()
{
  // [ SHORT_NAME, .. ]
  $planConfig = PlanConfig::Instance();
  return $planConfig->getUltraPlansShort();
}

function get_ultra_plans_long_definitions()
{
  // [ LONG_NAME, .. ]
  $planConfig = PlanConfig::Instance();
  return $planConfig->getUltraPlansLongDefinitions();
}


/**
 * is_restricted_plan
 * check if plan is restriced: offered for new activations only, not available to existing subscribers
 * @param String plan name
 * @return Boolean
 */
function is_restricted_plan($plan)
{
  // TODO: move to PlanConfig
  $restricted = array(
    PLAN_L24, // see project DOP
    PLAN_L29,
    PLAN_L44,
    PLAN_M29  // Ultra multi-month plan
  );

  return in_array($plan, $restricted);
}


/**
 * is_grandfathered_plan
 * check if plan is grandfathered: formerly offered and still supported for existing subscribers but no longer offered for new activation
 * @param String plan name
 * @return Boolean
 */
function is_grandfathered_plan($plan)
{
  // TODO: validate use, move to PlanConfig
  $grandfathered = array(
    PLAN_L39, 'L39' // see API-366
  );

  return in_array($plan, $grandfathered);
}


/**
 * get_orange_plan_map
 * VYT@16-01-06: this function appears unused and obsolete
 * KV array starts at 1
 * arrays consist (COS_ID, Months in offer)
 * @return Array KV array
 */
function get_orange_plan_map()
{
  $planConfig = \PlanConfig::Instance();
  return $planConfig->getOrangePlanMap();
}

/**
 * get_plan_name_map
 *
 * @return KV array COS_ID > NAME
 */
function get_plan_name_map()
{
  // [ COS_ID => DESCRIPTION, .. ]
  $planConfig = PlanConfig::Instance();
  return $planConfig->getPlanNameMap();
}

/**
 * get_plan_cost_map
 *
 * @return KV array COS_ID > COST
 */
function get_plan_cost_map()
{
  // [ COS_ID => COST, .. ]
  $planConfig = PlanConfig::Instance();
  return $planConfig->getPlanCostMap();
}

function plan_months_map()
{
  // [ COS_ID => MONTHS, .. ]
  $planConfig = PlanConfig::Instance();
  return $planConfig->planMonthsMap();
}

/**
 * This function return the MINT soc_name for cos_id
 * This is useful for recording paid events where soc_id is critical
 */
function get_mint_soc_name_from_cos_id($cos_id)
{
  // TODO: move to PlanConfig
  $map = [
    COSID_MINT_ONE_MONTH_S     => 'r_mint_s_01',
    COSID_MINT_ONE_MONTH_M     => 'r_mint_m_01',
    COSID_MINT_ONE_MONTH_L     => 'r_mint_l_01',
    COSID_MINT_THREE_MONTHS_S  => 'r_mint_s_03',
    COSID_MINT_THREE_MONTHS_M  => 'r_mint_m_03',
    COSID_MINT_THREE_MONTHS_L  => 'r_mint_l_03',
    COSID_MINT_SIX_MONTHS_S    => 'r_mint_s_06',
    COSID_MINT_SIX_MONTHS_M    => 'r_mint_m_06',
    COSID_MINT_SIX_MONTHS_L    => 'r_mint_l_06',
    COSID_MINT_TWELVE_MONTHS_S => 'r_mint_s_12',
    COSID_MINT_TWELVE_MONTHS_M => 'r_mint_m_12',
    COSID_MINT_TWELVE_MONTHS_L => 'r_mint_l_12'
  ];

  return (isset($map[$cos_id])) ? $map[$cos_id] : null;
}

/**
 * get_plan_cost_from_cos_id
 *
 * @param  Integer $cos_id
 * @return Integer cost of $cos_id || NULL if not found
 */
function get_plan_cost_from_cos_id($cos_id)
{
  $map = get_plan_cost_map();
  return (isset($map[$cos_id])) ? $map[$cos_id] : NULL;
}

/**
 * get_base_plan_name
 *
 * map multi month plan name into single month (base plan) for middleware purposes
 * see https://issues.hometowntelecom.com:8443/browse/MRP
 * @param String multi month plan name: either full (MULTI_TWENTY_NINE) or short (M29)
 * @return String base plan name: either full (TWENTY_NINE) or short (L29)
 */
function get_base_plan_name($plan)
{
  // TODO: move to PlanConfig
  // full plan name map: 'MULTI_TWENTY_NINE'
  $map = array(PLAN_M29  => PLAN_L29);
  if ( ! empty($map[$plan]))
    return $map[$plan];

  // short plan name map
  $map = array('M29' => 'L29');
  if ( ! empty($map[$plan]))
    return $map[$plan];

  // not found: return as is
  return $plan;
}


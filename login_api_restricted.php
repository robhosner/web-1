<?php


include_once('db.php');
include_once('lib/internal/functions.php');


teldata_change_db(); // connect to DB

$customer_id = '';

$success = FALSE;

if ( isset($_REQUEST['customer_id']) && isset($_REQUEST['password']) )
{
  $customer_id    = $_REQUEST['customer_id'];
  $password_input = $_REQUEST['password'];

  $password = func_read_temp_password($customer_id);

  dlog('',"func_read_temp_password got $password");

  if ( ( $password == $password_input ) && $password )
  {
    func_unset_temp_password($customer_id);

    $customer = find_customer(make_find_customer_query_anycosid($customer_id));

    dlog('',$customer);

    $_REQUEST['zsession'] = session_save($customer);

    $verified = verify_session($customer->CUSTOMER);

    dlog('',$verified);

    if ($verified)
    {
      // value and expiration of the cookie come from the session
      setcookielive('zsession',  $verified[2],        $verified[1], '/', $_SERVER["SERVER_NAME"]);
      setcookielive('zsessionC', $customer->CUSTOMER, $verified[1], '/', $_SERVER["SERVER_NAME"]);
      setcookielive('bypass_claim_account',  1,       $verified[1], '/', $_SERVER["SERVER_NAME"]);

      $zsessionC = $customer->CUSTOMER;
      $zsession  = $verified[2];

      # TODO: audit log

      $success = TRUE;
    }
  }
}

if ( $success )
{
  dlog('','login_api_restricted.php success');

  $redirect_url = sprintf('https://%s/', find_credential("ultra/rlogin/site"));
  header( 'Location: '.$redirect_url );
}
else
{
  dlog('','login_api_restricted.php failure');
  
  $msisdn = '';
  if ( isset($_REQUEST['customer_id']) )
  {
      $customer_id    = $_REQUEST['customer_id'];
      $customer = find_customer(make_find_ultra_customer_query_from_customer_id($customer_id));
      if ($customer) $msisdn = $customer->current_mobile_number;
  }
  $url_base = 'https://%s/login_as_customer.php?c=%s&msisdn=%s';
  if (isset($_REQUEST['password']) && $_REQUEST['password']!='')
  {
      //Password was sent, but failed
      $url_base = 'https://%s/login_as_customer.php?c=%s&invalid=1&msisdn=%s';
  }
  $redirect_url = sprintf($url_base, find_credential("ultra/rlogin/site"), $customer_id, $msisdn);
  
  header( 'Location: '.$redirect_url );
}

?>

{
  "meta": { "version": "1", "TODO": "" },

  "security_meta": { "RBAC": "internal group" },

  "commands":
  {
      "internal__CancelFraudCustomer":
      {
          "desc": "Cancels a customer due to Fraud suspicion.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              },
              {
                  "name": "msisdn",
                  "type": "string",
                  "required": true,
                  "validation": { "min_strlen": 10, "max_strlen": 10 },
                  "desc": "Ultra Mobile Phone Number (10 digits)"
              },
              {
                  "name": "reason",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 100 },
                  "desc": "Reason"
              },
              {
                  "name": "type",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 100 },
                  "desc": "Type"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },


      "internal__RecoverFailedTransition":
      {
          "desc": "Attempts to recover a Failed Transition.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "transition_uuid",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 38, "min_strlen": 36 },
                  "desc": "htt_transition_log.transition_uuid"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "notes",  "type": "string[]",  "desc": "Notes." }
          ]
      },


      "internal__GetCustomerData":
      {
          "desc": "Get customer data for our UI tools.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "customer_balance",         "type": "string",   "desc": "Balance held for paying plan renewals and for international calling and data purchases. Stored as cents." },
              { "name": "packaged_balance1",        "type": "string",   "desc": "accounts.packaged_balance1"                                },
              { "name": "customer_stored_value",    "type": "string",   "desc": "Balance stored for paying plan renewals. Stored as cents." },
              { "name": "customer_minutes",         "type": "integer",  "desc": "Balance held in minutes."                                  },
              { "name": "customer_plan_start",      "type": "epoch",    "desc": "UNIX Epoch timestamp of the start of the customers plan."  },
              { "name": "customer_plan_expires",    "type": "epoch",    "desc": "UNIX Epoch timestamp of the end of the customers plan."    },
              { "name": "preferred_language",       "type": "string",   "desc": "Preferred spoken language."                                },
              { "name": "customer_status",          "type": "string",   "desc": "Current Customer Status."                                  },
              { "name": "customer_loginname",       "type": "string",   "desc": "Login name."                                               },
              { "name": "customer_password",        "type": "string",   "desc": "Password."                                                 },
              { "name": "current_mobile_number",    "type": "string",   "desc": "Current Customer Mobile Number."                           },
              { "name": "recharge_status",          "type": "boolean",  "desc": "Recharge status"                                           },
              { "name": "current_iccid",            "type": "string",   "desc": "Current ICCID."                                            },
              { "name": "activation_iccid",         "type": "string",   "desc": "Activation ICCID."                                         },
              { "name": "first_name",               "type": "string",   "desc": "First Name."                                               },
              { "name": "last_name",                "type": "string",   "desc": "Last Name."                                                },
              { "name": "email",                    "type": "string",   "desc": "Email Address."                                            },
              { "name": "address",                  "type": "string",   "desc": "Address."                                                  },
              { "name": "city",                     "type": "string",   "desc": "City."                                                     },
              { "name": "state",                    "type": "string",   "desc": "State."                                                    },
              { "name": "country",                  "type": "string",   "desc": "Country."                                                  },
              { "name": "activation_start_date",    "type": "string",   "desc": "Activation Start Date."                                    },
              { "name": "customer_has_credit_card", "type": "boolean",  "desc": "True if the customer has a credit card."                   },
              { "name": "brand",                    "type": "string",   "desc": "brand to which the customer ICCID belongs"                 }
          ]
      },

      "internal__SubmitChargeAmount":
      {
          "desc": "Allows internal payment processors to add the given amount to the given customer account",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              },
              {
                  "name": "amount",
                  "type": "integer",
                  "required": true,
                  "validation": { "min_value": 1 },
                  "desc": "Amount to be added to an account. Unit: dollar cents"
              },
              {
                  "name": "reason",
                  "type": "string",
                  "required": true,
                  "validation": { "matches": [ "ULTRA_PLAN_RECHARGE", "ULTRA_ADD_BALANCE" ] },
                  "desc": "Reason of the payment"
              },
              {
                  "name": "reference",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Reference"
              },
              {
                  "name": "subproduct_id",
                  "desc": "Plan name",
                  "required": false,
                  "type": "string",
                  "validation": { }
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "request",               "type": "string",   "desc": "Original request encoded as JSON"                                   },
              { "name": "phone_number",          "type": "string",   "desc": "Ultra Mobile Phone Number (if success)"                             },
              { "name": "customer_balance",      "type": "integer",  "desc": "Account value (in cents) (if success)"                              },
              { "name": "customer_active",       "type": "boolean",  "desc": "Is the customer active?"                                            },
              { "name": "plan_name",             "type": "string",   "desc": "Ultra Mobile Plan Name - NINETEEN, TWENTY_NINE, THIRTY_NINE, FORTY_NINE, FIFTY_NINE" },
              { "name": "service_expires_epoch", "type": "epoch",    "desc": "UNIX Epoch timestamp of service expiry (implicit UTC) (if success)" }
          ]
      },

      "internal__Crone":
      {
          "desc": "cron runner, or needful errand",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "mode",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "crone mode"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "internal__States":
      {
          "desc": "State Machine",
          "parameters":
          [
              "common-parameters.json"
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "internal__ChangeState":
      {
          "desc": "Change customer state",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              },
              {
                  "name": "resolve_now",
                  "type": "boolean",
                  "required": false,
                  "default": true,
                  "validation": { },
                  "desc": "If true, try to resolve the transition now"
              },
              {
                  "name": "plan",
                  "desc": "Plan name", 
                  "required": true,
                  "type": "string", 
                  "validation": { "active_plan": 2 } 
              },
              {
                  "name": "state_name",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Desired state"
              },
              {
                  "name": "dry_run",
                  "type": "boolean",
                  "required": false,
                  "default": false,
                  "validation": { },
                  "desc": "Whether the state change will really be attempted"
              },
              {
                  "name": "max_path_depth",
                  "type": "integer",
                  "required": false,
                  "default": 1,
                  "validation": { },
                  "desc": "How deep a state change path can be"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "internal__TakeTransition":
      {
          "desc": "Change customer state",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              },
              {
                  "name": "resolve_now",
                  "type": "boolean",
                  "required": false,
                  "default": true,
                  "validation": { },
                  "desc": "If true, try to resolve the transition now"
              },
              {
                  "name": "transition_name",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Desired transition"
              },
              {
                  "name": "dry_run",
                  "type": "boolean",
                  "required": false,
                  "default": false,
                  "validation": { },
                  "desc": "Whether the state change will really be attempted"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "internal__GetState":
      {
          "desc": "Get customer state",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "plan_name", "type": "string", "desc": "Plan name" },
              { "name": "plan_desc_name", "type": "string", "desc": "Descriptive plan name" },
              { "name": "state_desc_name", "type": "string", "desc": "Descriptive name for the whole state" },
              { "name": "state_name", "type": "string", "desc": "Plan substate" },
              { "name": "state_valid", "type": "boolean", "desc": "Is the customer in a valid plan and substate?" }
          ]
      },

      "internal__GetActionsByTransition":
      {
          "desc": "Gets all actions belonging to a transition",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "transition_uuid",
                  "desc": "Transition ID",
                  "required": true,
                  "type": "string",
                  "validation": { }
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "actions", "type": "string[]", "desc": "List of actions" }
          ]
      },

      "internal__CheckAction":
      {
          "desc": "Check an action",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "action_id",
                  "desc": "Action ID",
                  "required": true,
                  "type": "string",
                  "validation": { }
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "internal__CheckTransition":
      {
          "desc": "Check a transition",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "transition_id",
                  "desc": "Transition UUID", 
                  "required": true,
                  "type": "string", 
                  "validation": { "transition_exists": "TRANSITION_UUID" }
              },
              {
                  "name": "explain",
                  "desc": "Just explain", 
                  "required": false,
                  "type": "boolean", 
                  "default": 0,
                  "validation": { }
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "customer_id", "type": "integer", "desc": "The customer for this transition" },
              { "name": "status", "type": "string", "desc": "Transition status" },
              { "name": "unknown_actions", "type": "string[]", "desc": "Actions with unknown status under this transition" },
              { "name": "open_actions", "type": "string[]", "desc": "Open actions under this transition" },
              { "name": "pending_actions", "type": "string[]", "desc": "Pending actions under this transition" },
              { "name": "done_actions", "type": "string[]", "desc": "Done actions under this transition" },
              { "name": "aborted_actions", "type": "string[]", "desc": "Aborted actions under this transition" },
              { "name": "created", "type": "epoch", "desc": "Date created (epoch)" },
              { "name": "closed", "type": "epoch", "desc": "Date closed (epoch)" },
              { "name": "is_closed_or_aborted", "type": "boolean", "desc": "Whether the transition is closed or aborted" }
          ]
      },

      "internal__GetAllAbortedTransitions":
      {
          "desc": "Get all aborted transition for a given date",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "transition_date",
                  "type": "string",
                  "required": false,
                  "validation": { "date_format": "mm-dd-yyyy" },
                  "desc": "Transition date (default = today)"
              },
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": false,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "transitions_active_to_suspended",    "type": "string[]", "desc": "Transition data (Active to Suspended)"     },
              { "name": "transitions_active_to_active",       "type": "string[]", "desc": "Transition data (Active to Active)"        },
              { "name": "transitions_suspended_to_active",    "type": "string[]", "desc": "Transition data (Suspended to Active)"     },
              { "name": "transitions_neutral_to_promounused", "type": "string[]", "desc": "Transition data (Neutral to Promo Unused)" },
              { "name": "transitions_promounusedto_active",   "type": "string[]", "desc": "Transition data (Promo Unused to Active)"  },
              { "name": "transitions_neutral_to_provisioned", "type": "string[]", "desc": "Transition data (Neutral to Provisioned)"  },
              { "name": "transitions_neutral_to_active",      "type": "string[]", "desc": "Transition data (Neutral to Active)"       }
          ]
      },

      "internal__GetAllPendingTransitions":
      {
          "desc": "Get all pending (is_closed_or_aborted is false) transition IDs",
          "parameters":
          [
              "common-parameters.json"
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "transitions", "type": "string[]", "desc": "Transition IDs" }
          ]
      },

      "internal__GetAllCustomerStateTransitions":
      {
          "desc": "Get all state transitions for a given customer",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "state_transitions", "type": "string[]", "desc": "Transition data" },
              { "name": "destination_origin_map_account", "type": "string",   "desc": "destination_origin_map.ACCOUNT" },
              { "name": "destination_origin_map_origin",  "type": "string",   "desc": "destination_origin_map.ORIGIN" },
              { "name": "account_alias",     "type": "string",   "desc": "account_aliases.ALIAS" },
              { "name": "msisdn",            "type": "string",   "desc": "The MSISDN" },
              { "name": "iccid",             "type": "string",   "desc": "iccid" },
              { "name": "balance",           "type": "string",   "desc": "accounts.balance" },
              { "name": "packaged_balance1", "type": "string",   "desc": "accounts.packaged_balance1" },
              { "name": "packaged_balance2", "type": "string",   "desc": "accounts.packaged_balance2" },
              { "name": "customer_stored_balance",      "type": "string",   "desc": "htt_customers_overlay_ultra.stored_value" }
          ]
      },

      "internal__GetPendingTransitions":
      {
          "desc": "Get all pending (is_closed_or_aborted is false) transition IDs",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "transitions", "type": "string[]", "desc": "Transition IDs" }
          ]
      },

      "internal__ResolvePendingTransitions":
      {
          "desc": "Resolve all pending (is_closed_or_aborted is false) transition IDs",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": false,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              },
              {
                  "name": "environments",
                  "type": "string[]",
                  "required": false,
                  "validation": { },
                  "desc": "List of environments in which transitions should be resolved"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "open_transitions",    "type": "string[]", "desc": "Transition IDs" },
              { "name": "closed_transitions",  "type": "string[]", "desc": "Transition IDs" },
              { "name": "aborted_transitions", "type": "string[]", "desc": "Transition IDs" }
          ]
      },

      "internal__IsEligiblePortIn":
      {
          "desc": "Is a MSISDN eligible for PortIn?  Validates on input.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "string",
                  "required": true,
                  "validation": { },
                  "desc": "Unique customer DB identifier or -1"
              },
              {
                  "name": "msisdn",
                  "type": "string",
                  "required": true,
                  "validation": { "max_strlen": 10, "min_strlen": 10 },
                  "desc": "The MSISDN"
              }
          ],

          "returns":
          [
              "common-returns.json",
              { "name": "eligible", "type": "boolean", "desc": "Is the MSISDN eligible?" },
              { "name": "msisdn", "type": "string", "desc": "The MSISDN" }
          ]
      },

      "internal__QueryPortIn":
      {
          "desc": "Invokes QueryPortIn on the network.",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": false,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              },
              {
                  "name": "msisdn",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 10 },
                  "desc": "MSISDN of Customer."
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "status",  "type": "string",  "desc": "status"  },
              { "name": "r_code",  "type": "string",  "desc": "R-code"  },
              { "name": "message", "type": "string",  "desc": "message" },
              { "name": "query_status",          "type": "string", "desc": "PORTIN_QUEUE.QUERYSTATUS_PORTSTATUS" },
              { "name": "port_query_status",     "type": "string", "desc": "Port Query Status"                   },
              { "name": "carrier_name",          "type": "string", "desc": "Carrier Name"                        },
              { "name": "provision_status_date", "type": "epoch",  "desc": "Latest ProvisioningStatus date"      },
              { "name": "query_message",         "type": "string", "desc": "PORTIN_QUEUE.QUERYSTATUS_ERRORMSG"   }
          ]
      },

      "internal__QueryMSISDN":
      {
          "desc": "Invokes QueryMSISDN on the network",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": false,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              },
              {
                  "name": "msisdn",
                  "type": "string",
                  "required": false,
                  "validation": { },
                  "desc": "MSISDN of Customer, leading 1 stripped."
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "msisdn",            "type": "integer", "desc": "msisdn" },
              { "name": "customer_id",       "type": "integer", "desc": "customer_id" },
              { "name": "iccid",             "type": "string",  "desc": "iccid" },
              { "name": "product",           "type": "string",  "desc": "product" },
              { "name": "porting_state",     "type": "string",  "desc": "porting_state" },
              { "name": "msisdn_state",      "type": "string",  "desc": "msisdn_state" },
              { "name": "active_services",   "type": "string[]",  "desc": "active_services" },
              { "name": "inactive_services", "type": "string[]",  "desc": "inactive_services" }
          ]
      },

      "internal__MonthlyServiceChargeStats":
      {
          "desc": "Monthly Service Charge statistics",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "stats_date",
                  "type": "string",
                  "required": true,
                  "validation": { "date_format": "mm-dd-yyyy" },
                  "desc": "Date for statistics"
              }
          ],

          "returns":
          [
              "common-returns.json"
          ]
      },

      "internal__Test":
      {
          "desc": "Test",
          "parameters":
          [
              "common-parameters.json",
              {
                  "name": "customer_id",
                  "type": "integer",
                  "required": false,
                  "validation": { },
                  "desc": "Unique customer DB identifier"
              },
              {
                  "name": "test_string",
                  "type": "string",
                  "required": false,
                  "validation": { "min_strlen": 2, "max_strlen": 10 },
                  "desc": "Test string length"
              },
              {
                  "name": "test_matches",
                  "type": "string",
                  "required": false,
                  "validation": { "matches": ["A","B"] },
                  "desc": "Test input matches"
              },
              {
                  "name": "test_integer",
                  "type": "integer",
                  "required": false,
                  "validation": { "min_value": 2, "max_value": 10 },
                  "desc": "Test integer value"
              },
              {
                  "name": "test_format",
                  "type": "string",
                  "required": false,
                  "validation": { "format": "JSON" },
                  "desc": "Test format validation"
              },
              {
                  "name": "test_date_format",
                  "type": "string",
                  "required": false,
                  "validation": { "date_format": "mm-dd-yyyy" },
                  "desc": "Test date format"
              },
              {
                  "name": "test_transition_exists",
                  "type": "string",
                  "required": false,
                  "validation": { "transition_exists": "TRANSITION_UUID" },
                  "desc": "Test transition exists"
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "output",    "type": "string",   "desc": "Test output." }
          ]
      },

      "internal__TestVersion2":
      {
          "desc": "Test Api Version 2",
          "parameters":
          [
              "common-parameters.json",

              {
                  "name": "input1",
                  "type": "string",
                  "required": true,
                  "validation": { "matches": [ "A" , "B" ] },
                  "desc": "Test input string."
              },
              {
                  "name": "input2",
                  "type": "integer",
                  "required": true,
                  "validation": { },
                  "desc": "Test input integer."
              }
          ],

          "returns":
          [
              "common-returns.json",

              { "name": "output1",    "type": "string",    "desc": "Test output string." },
              { "name": "output2",    "type": "integer",   "desc": "Test output integer." },
              { "name": "output3",    "type": "boolean",   "desc": "Test output boolean." },
              { "name": "output4",    "type": "string[]",  "desc": "Test output string array." },
              { "name": "output5",    "type": "integer[]", "desc": "Test output integer array." },
              { "name": "output6",    "type": "boolean[]", "desc": "Test output boolean array." }
          ]
      }
  }
}

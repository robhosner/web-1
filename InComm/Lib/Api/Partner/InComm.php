<?php

namespace InComm\Lib\Api\Partner;

require_once 'Ultra/Lib/Api/PartnerBase.php';
require_once 'Ultra/Lib/inComm/Incomm.php';

/**
 * inComm partner class
 * APIs for inComm phase 2
 * @author VYT, 2015-01
 * @see https://issues.hometowntelecom.com:8443/browse/RTRIN-1
 */
class InComm extends \Ultra\Lib\Api\PartnerBase
{

  /**
   * return X12 formatted date, time and in Eastern time zone
   * @return Array ('CCYYMMDD', 'HHMMSS', 'TMZ')
   */
  public function getDateTimeZone()
  {
    $now = new \DateTime(null, new \DateTimeZone('America/New_York'));
    return array($now->format('Ymd'), $now->format('His'), $now->format('T'));
  }
  
  /**
   * check static login credentials
   */
  protected function validateCredentials($username, $password)
  {
    if ( ! $credentials = \Ultra\UltraConfig\getPaymentProviderCredentials('incomm'))
    {
      dlog('', 'ERROR: missing inComm credentials');
      return FALSE;
    }
    
    if ($username != $credentials[0] || $password != $credentials[1])
    {
      dlog('', 'ERROR: invalid login credentials: %s:%s vs %s:%s', $credentials[0], $credentials[1], $username, $password);
      return FALSE;
    }
    
    return TRUE;
  }

}

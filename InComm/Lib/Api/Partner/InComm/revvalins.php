<?php

namespace InComm\Lib\Api\Partner\InComm;

class revvalins extends \InComm\Lib\Api\Partner\inComm
{
  /**
   * revvalins
   * inComm reverse value insertion
   * @return object Result
   */
  public function incomm__revvalins()
  {
    // initialize
    list($Version, $Date, $Time, $TimeZone, $IncommRefNum, $UPC, $MIN, $Amount, $UserID, $Password) = $this->getInputValues();
    $ServiceProviderRefNum = NULL;

    try
    {
      // check API login
      if ( ! $this->validateCredentials($UserID, $Password))
      {
        $RespCode = 10040;
        $RespMsg = 'Invalid UserID or Password';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid login information', 'SE0007');
      }

      // get subscriber
      teldata_change_db();
      if ( ! $subscriber = \Ultra\Lib\Incomm\decodeAccountNumber($MIN))
      {
        $RespCode = 10022;
        $RespMsg = 'Invalid MIN';
        $this->errException('ERR_API_INVALID_ARGUMENTS: The customer does not exist', 'VV0031');
      }

      // void pending transaction
      $query = \Ultra\Lib\DB\makeUpdateQuery(
        'HTT_BILLING_ACTIONS',
        array('STATUS' => 'VOID'),
        array(
          'STATUS'          => 'OPEN',
          'CUSTOMER_ID'     => $subscriber->CUSTOMER_ID,
          'TRANSACTION_ID'  => $IncommRefNum,
          'AMOUNT'          => $Amount * 100));
      if ( ! is_mssql_successful(logged_mssql_query($query)))
      {
        $RespCode = 10029;
        $RespMsg = 'System Error';
        $this->errException('ERR_API_INTERNAL: An unexpected database error has occurred', 'DB00001');
      }

      // check if transaction exists and was updated
      $query = \Ultra\Lib\DB\makeSelectQuery(
        'HTT_BILLING_ACTIONS',
        1,
        array('STATUS', 'UUID'),
        array(
          'CUSTOMER_ID'     => $subscriber->CUSTOMER_ID,
          'TRANSACTION_ID'  => $IncommRefNum,
          'AMOUNT'          => $Amount * 100));
      $transaction = mssql_fetch_all_objects(logged_mssql_query($query));
      if ( ! is_array($transaction) || ! count($transaction))
      {
        $RespCode = 10067;
        $RespMsg = 'Not Reversible';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Transaction does not exist', 'VV0003');
      }
      if ($transaction[0]->STATUS != 'VOID')
      {
        $RespCode = 10067;
        $RespMsg = 'Not Reversible';
        $this->errException('ERR_API_INTERNAL: Cannot void transaction', 'DB0002');
      }

      // add return values
      $ServiceProviderRefNum = $transaction[0]->UUID;
      $Amount = $subscriber->stored_value + $subscriber->BALANCE;

      // success
      list($Date, $Time, $TimeZone) = $this->getDateTimeZone();
      $RespCode = 0;
      $RespMsg = 'Success';
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    // fill in return values
    foreach (array('Version', 'Date', 'Time', 'TimeZone', 'ServiceProviderRefNum', 'Amount', 'RespCode', 'RespMsg') as $name)
      $this->addToOutput($name, $$name);
    return $this->result;
  }
}

?>

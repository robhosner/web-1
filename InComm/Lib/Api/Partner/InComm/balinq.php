<?php

namespace InComm\Lib\Api\Partner\InComm;

class balinq extends \InComm\Lib\Api\Partner\inComm
{
  /**
   * incomm__balinq
   * subsriber balance inquiry
   * @return object Result
   */
  public function incomm__balinq()
  {
    // initialize
    list($Version, $Date, $Time, $TimeZone, $IncommRefNum, $MIN, $UserID, $Password) = $this->getInputValues();
    $ServiceProviderRefNum = \Ultra\Lib\Incomm\generateSrcRefNum();
    $Amount = 0;

    try
    {
      // check API login
      if ( ! $this->validateCredentials($UserID, $Password))
      {
        $RespCode = 10040;
        $RespMsg = 'Invalid UserID or Password';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid login information', 'SE0007');
      }

      // get subscriber
      teldata_change_db();
      if ( ! $subscriber = \Ultra\Lib\Incomm\decodeAccountNumber($MIN))
      {
        $RespCode = 10022;
        $RespMsg = 'Invalid MIN';
        $this->errException('ERR_API_INVALID_ARGUMENTS: The customer does not exist', 'VV0031');
      }

      // check state
      if (in_array($subscriber->plan_state, array(STATE_CANCELLED, STATE_PORT_IN_DENIED)))
      {
        $RespCode = 10059;
        $RespMsg = 'Invalid Account';
        $this->errException('ERR_API_INVALID_ARGUMENTS: Invalid customer state for this command', 'IN0001');
      }

      // add subscriber balance
      $Amount = $subscriber->stored_value + $subscriber->BALANCE;

      // success
      list($Date, $Time, $TimeZone) = $this->getDateTimeZone();
      $RespCode = 0;
      $RespMsg = 'Success';
      $this->succeed();
    }
    catch(\Exception $e)
    {
      dlog('', 'EXCEPTION: ' . $e->getMessage());
    }

    // fill in return values
    foreach (array('Version', 'Date', 'Time', 'TimeZone', 'ServiceProviderRefNum', 'Amount', 'RespCode', 'RespMsg') as $name)
      $this->addToOutput($name, $$name);
    return $this->result;
  }
}

?>

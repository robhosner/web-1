{
  "meta": { "version": "4" },
  "security_meta": { "RBAC": "incomm group" },

  "commands":
  {
    "incomm__balinq":
    {
      "desc": "inComm subscriber balance inquiry",
      "parameters":
      [
        {
          "name": "Version",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 5 },
          "desc": "XML specifications version"
        },
        {
          "name": "Date",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 8, "max_strlen": 8 },
          "desc": "transaction date CCYYMMDD"
        },
        {
          "name": "Time",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4, "max_strlen": 6 },
          "desc": "transaction time HHMMSS"
        },
        {
          "name": "TimeZone",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 3, "max_strlen": 4 },
          "desc": "time zone code"
        },
        {
          "name": "IncommRefNum",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 12 },
          "desc": "unique transaction number"
        },
        {
          "name": "MIN",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 28 },
          "desc": "either encoded Ultra subscriber account or MSISDN"
        },
        {
          "name": "UserID",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user name"
        },
        {
          "name": "Password",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user password"
        }
      ],

      "template": "balinq.xml",
      "returns":
      [
        { "name": "Version", "type": "string", "desc": "XML specifications version" },
        { "name": "Date", "type": "string", "desc": "transaction date CCYYMMDD" },
        { "name": "Time", "type": "string", "desc": "transaction time HHMMSS" },
        { "name": "TimeZone", "type": "string", "desc": "time zone code" },
        { "name": "RespCode", "type": "integer", "desc": "error code" },
        { "name": "RespMsg", "type": "string", "desc": "error message" },
        { "name": "ServiceProviderRefNum", "type": "integer", "desc": "Ultra transaction number" },
        { "name": "Amount", "type": "string", "desc": "subscriber balance in dollars" }
      ]
    },


    "incomm__preauthrecharge":
    {
      "desc": "inComm value insertion pre-authorization",
      "parameters":
      [
        {
          "name": "Version",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 5 },
          "desc": "XML specifications version"
        },
        {
          "name": "Date",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 8, "max_strlen": 8 },
          "desc": "transaction date CCYYMMDD"
        },
        {
          "name": "Time",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4, "max_strlen": 6 },
          "desc": "transaction time HHMMSS"
        },
        {
          "name": "TimeZone",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 3, "max_strlen": 4 },
          "desc": "time zone code"
        },
        {
          "name": "IncommRefNum",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 12 },
          "desc": "unique transaction number"
        },
        {
          "name": "UPC",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 12, "max_strlen": 12 },
          "desc": "inComm Universal Product Code"
        },
        {
          "name": "MIN",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 28 },
          "desc": "either encoded Ultra subscriber account or MSISDN"
        },
        {
          "name": "Amount",
          "type": "real",
          "required": true,
          "validation": { },
          "desc": "amount to authorize"
        },
        {
          "name": "UserID",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user name"
        },
        {
          "name": "Password",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user password"
        }
      ],

      "template": "preauthrecharge.xml",
      "returns":
      [
        { "name": "Version", "type": "string", "desc": "XML specifications version" },
        { "name": "Date", "type": "string", "desc": "transaction date CCYYMMDD" },
        { "name": "Time", "type": "string", "desc": "transaction time HHMMSS" },
        { "name": "TimeZone", "type": "string", "desc": "time zone code" },
        { "name": "RespCode", "type": "integer", "desc": "error code" },
        { "name": "RespMsg", "type": "string", "desc": "error message" },
        { "name": "ServiceProviderRefNum", "type": "integer", "desc": "Ultra transaction number" },
        { "name": "Amount", "type": "string", "desc": "subscriber balance in dollars" }
      ]
    },


    "incomm__prevalins":
    {
      "desc": "inComm real time recharge value insertion pre-authorization",
      "parameters":
      [
        {
          "name": "Version",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 5 },
          "desc": "XML specifications version"
        },
        {
          "name": "Date",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 8, "max_strlen": 8 },
          "desc": "transaction date CCYYMMDD"
        },
        {
          "name": "Time",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4, "max_strlen": 6 },
          "desc": "transaction time HHMMSS"
        },
        {
          "name": "TimeZone",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 3, "max_strlen": 4 },
          "desc": "time zone code"
        },
        {
          "name": "IncommRefNum",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 12 },
          "desc": "unique transaction number"
        },
        {
          "name": "UPC",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 12, "max_strlen": 12 },
          "desc": "inComm Universal Product Code"
        },
        {
          "name": "MIN",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 28 },
          "desc": "either encoded Ultra subscriber account or MSISDN"
        },
        {
          "name": "Amount",
          "type": "real",
          "required": true,
          "validation": { },
          "desc": "amount to authorize"
        },
        {
          "name": "UserID",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user name"
        },
        {
          "name": "Password",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user password"
        }
      ],

      "template": "prevalins.xml",
      "returns":
      [
        { "name": "Version", "type": "string", "desc": "XML specifications version" },
        { "name": "Date", "type": "string", "desc": "transaction date CCYYMMDD" },
        { "name": "Time", "type": "string", "desc": "transaction time HHMMSS" },
        { "name": "TimeZone", "type": "string", "desc": "time zone code" },
        { "name": "RespCode", "type": "integer", "desc": "error code" },
        { "name": "RespMsg", "type": "string", "desc": "error message" },
        { "name": "ServiceProviderRefNum", "type": "integer", "desc": "Ultra transaction number" },
        { "name": "Amount", "type": "string", "desc": "subscriber balance in dollars" }
      ]
    },


    "incomm__valins":
    {
      "desc": "inComm value insertion",
      "parameters":
      [
        {
          "name": "Version",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 5 },
          "desc": "XML specifications version"
        },
        {
          "name": "Date",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 8, "max_strlen": 8 },
          "desc": "transaction date CCYYMMDD"
        },
        {
          "name": "Time",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4, "max_strlen": 6 },
          "desc": "transaction time HHMMSS"
        },
        {
          "name": "TimeZone",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 3, "max_strlen": 4 },
          "desc": "time zone code"
        },
        {
          "name": "IncommRefNum",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 12 },
          "desc": "unique transaction number"
        },
        {
          "name": "UPC",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 12, "max_strlen": 12 },
          "desc": "inComm Universal Product Code"
        },
        {
          "name": "MIN",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 28 },
          "desc": "either encoded Ultra subscriber account or MSISDN"
        },
        {
          "name": "Amount",
          "type": "real",
          "required": true,
          "validation": { },
          "desc": "amount to authorize"
        },
        {
          "name": "UserID",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user name"
        },
        {
          "name": "Password",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user password"
        },
        {
          "name": "MerchName",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 50 },
          "desc": "store name"
        },
        {
          "name": "StoreID",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 15 },
          "desc": "store number"
        },
        {
          "name": "TermID",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 15 },
          "desc": "terminal number"
        },
        {
          "name": "Zip",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 5 },
          "desc": "store zip code"
        },
        {
          "name": "SourceInfo",
          "type": "string",
          "required": false,
          "validation": { "max_strlen": 20 },
          "desc": "transaction source"
        }
      ],

      "template": "valins.xml",
      "returns":
      [
        { "name": "Version", "type": "string", "desc": "XML specifications version" },
        { "name": "Date", "type": "string", "desc": "transaction date CCYYMMDD" },
        { "name": "Time", "type": "string", "desc": "transaction time HHMMSS" },
        { "name": "TimeZone", "type": "string", "desc": "time zone code" },
        { "name": "RespCode", "type": "integer", "desc": "error code" },
        { "name": "RespMsg", "type": "string", "desc": "error message" },
        { "name": "ServiceProviderRefNum", "type": "integer", "desc": "Ultra transaction number" },
        { "name": "Amount", "type": "string", "desc": "subscriber balance in dollars after transaction" }
      ]
    },


    "incomm__revvalins":
    {
      "desc": "inComm value insertion reversal",
      "parameters":
      [
        {
          "name": "Version",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 5 },
          "desc": "XML specifications version"
        },
        {
          "name": "Date",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 8, "max_strlen": 8 },
          "desc": "transaction date CCYYMMDD"
        },
        {
          "name": "Time",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 4, "max_strlen": 6 },
          "desc": "transaction time HHMMSS"
        },
        {
          "name": "TimeZone",
          "type": "string",
          "required": true,
          "validation": { "min_strlen": 3, "max_strlen": 4 },
          "desc": "time zone code"
        },
        {
          "name": "IncommRefNum",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 12 },
          "desc": "unique transaction number"
        },
        {
          "name": "UPC",
          "type": "string",
          "required": false,
          "validation": { "min_strlen": 12, "max_strlen": 12 },
          "desc": "inComm Universal Product Code"
        },
        {
          "name": "MIN",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 28 },
          "desc": "either encoded Ultra subscriber account or MSISDN"
        },
        {
          "name": "Amount",
          "type": "real",
          "required": true,
          "validation": { },
          "desc": "amount to authorize"
        },
        {
          "name": "UserID",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user name"
        },
        {
          "name": "Password",
          "type": "string",
          "required": true,
          "validation": { "max_strlen": 20 },
          "desc": "static authorized user password"
        }
      ],

      "template": "revvalins.xml",
      "returns":
      [
        { "name": "Version", "type": "string", "desc": "XML specifications version" },
        { "name": "Date", "type": "string", "desc": "transaction date CCYYMMDD" },
        { "name": "Time", "type": "string", "desc": "transaction time HHMMSS" },
        { "name": "TimeZone", "type": "string", "desc": "time zone code" },
        { "name": "RespCode", "type": "integer", "desc": "error code" },
        { "name": "RespMsg", "type": "string", "desc": "error message" },
        { "name": "ServiceProviderRefNum", "type": "integer", "desc": "Ultra transaction number" },
        { "name": "Amount", "type": "string", "desc": "subscriber balance in dollars after transaction" }
      ]
    }
  }
}

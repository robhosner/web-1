function getPromos() {
    var params = {
        bath: 			'rest',
        callback: 		'createPromosTable',
        version: 		'1'
    }
    $.ajax('/partner.php?format=jsonp&partner=promotional&command=promotional__ListPromotionsForPreparation',{
        dataType:'jsonp',
        data: params
    });
}

function createPromosTable(data) {
    if (data.success) {
        var promoRows = '';
        for (var i in data.promotions) {
            var promo = data.promotions[i];
            promoRows += '<tr class="promo_' + promo.promo_status + '">';
            promoRows += '<td>' + promo.id + '</td>';
            promoRows += '<td>' + promo.promo_status + '</td>';
            promoRows += '<td>' + promo.project_name + '</td>';
            promoRows += '<td>' + promo.channel + '</td>';
            promoRows += '<td>' + promo.subchannel + '</td>';
            promoRows += '<td>' + promo.microchannel + '</td>';
            promoRows += '<td>' + promo.iccid_start + '</td>';
            promoRows += '<td>' + promo.iccid_end + '</td>';
            promoRows += '<td>' + promo.total_sims + '</td>';
            promoRows += '<td>' + promo.ready_customers + '</td>';
            promoRows += '<td>' + promo.ready_sims + '</td>';
			promoRows += '<td>' + promo.active_customers + '</td>';
            promoRows += '</tr>';
        }
        $('#promos-table tbody').html(promoRows);
        $('h1 img').hide();
    } else {
        $('table').hide().after('<h5>' + data.errors[0] + '</h5>');
        $('h1 img').hide();
    }
}


function preparePromoCustomers() {
	$('.alert').slideUp(200);
	$('h1 img').show();
	$('#preparePromoCustomers').attr('disabled','disabled');
    var params = {
        bath: 			'rest',
        callback: 		'prepareResponse',
        version: 		'1',
		promo_id: 		$('#form_id').val(),
		prepare_count: 	$('#form_count').val()
		
    }
    $.ajax('/partner.php?format=jsonp&partner=promotional&command=promotional__PreparePromoCustomers',{
        dataType:'jsonp',
        data: params
    });
}

function prepareResponse(data) {
	$('h1 img').hide();
	$('#preparePromoCustomers').removeAttr('disabled');
	if (data.success) {
		$('.alert').removeClass('error').addClass('success').html('Success').slideDown();
	} else {
		$('.alert').removeClass('success').addClass('error').html(data.errors[0]).slideDown();
	}
}

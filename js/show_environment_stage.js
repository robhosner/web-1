// include this script in tool to show env
// envStage.hide(); hides

  function addLoadEvent(func)
  {
    var oldnload = window.onload;
    if (typeof window.onload != 'function')
      window.onload = func;
    else
    {
      window.onload = function()
      {
        if (oldonload)
          oldonload();
        func();
      }
    }
  }

  var envStage;
  addLoadEvent(function() { envStage = new EnvStage; })

var EnvStage = function ()
{
  this.loadEnvironmentStage();
}
EnvStage.prototype = {
  baseUrl: '/ultra_api.php?version=2&bath=rest&format=json&partner=internal',
  command: 'command=internal__GetEnvironmentStage',
  stage:   'UNKNOWN',

  getEnvironmentStageData: function(callback)
  {
    var xmlhttp;

    if (window.XMLHttpRequest)
      xmlhttp = new XMLHttpRequest();
    else
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

    xmlhttp.onreadystatechange = function()
    {
      if (xmlhttp.readyState == 4)
      {
        if (xmlhttp.status == 200)
        {
          data = JSON.parse(xmlhttp.responseText);
          callback(data)
        }
      }
    }

    xmlhttp.open("GET", this.baseUrl + '&' + this.command, true);
    xmlhttp.send();
  },

  loadEnvironmentStage: function()
  {
    this.getEnvironmentStageData(function(data)
    {
      stage = data.stage;

      var div   = document.createElement("DIV");
      var style = 'background-color: red; color: white; display: none; font-family: sans-serif; font-weight: bold; margin: 0px; padding: 2px;';

      div.innerHTML = '<div id="env_stage" style="' + style + '">STAGE: ' + data.stage + '</div>';

      document.body.insertBefore(div, document.body.children[0]);

      document.getElementById('env_stage').style.display = 'block';
    });
  },

  hide: function()
  {
    document.getElementById('env_stage').style.display = 'none';
  },

  canRun: function(stages)
  {
    var canRun = (stages.indexOf(data.stage) != -1);
    if (!canRun)
      throw new Error('ERROR: tool cannot run in this environment');
    return canRun;
  }
}

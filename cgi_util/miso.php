<?php
/**
  * miso.php: web tool to show open MakeItSo
  * @see AMDOCS-451
  * @author VYT 14-08-12
  * @todo
      freeze table header position
      show last error
      show invocation
      action menu based on status
  */

?>

<!DOCTYPE html>
<html lang="en">
<head>

<link rel="stylesheet" type="text/css" href="/css/miso.css" />
<script type="text/javascript" src="/js/show_environment_stage.js"></script>
<script src="../js/vendor/moment.js"></script>
<script language="javascript">
var Miso = {
  records: [],
  clearRecords: function() {
    this.records = [];
  },
  addRecords: function(records) {
    this.clearRecords();
    this.records = records;
  },
  findRecordId: function(queue_id) {
    for (var i = 0; i < this.records.length; i++)
    {
      if (this.records[i].MAKEITSO_QUEUE_ID == queue_id)
        return i;
    }

    return false;
  },
  findNextRecordId: function(queue_id) {
    for (var i = 0; i < this.records.length; i++)
    {
      if (this.records[i].MAKEITSO_QUEUE_ID == queue_id && i < this.records.length - 1)
        return i + 1;
    }

    return false;
  },
  getRecord: function(id) {
    return this.records[id];
  }
}

var MisoUtil = {
  lastQueueId: null,
  nextQueueId: null,
  lastViewed: null,

  scrollTop: 0,
  storeScroll: function() {
    this.scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
  },
  restoreScroll: function() {
    window.scrollTo(0, this.scrollTop);
  },

  reset: function() {
    this.lastQueueId = null;
    this.nextQueueId = null;
    this.lastViewed = null;
  },
  setLastViewed: function(queue_id) {
    this.lastQueueId = null;
    this.nextQueueId = null;

    this.lastViewed = queue_id;
  },
  setLastQueueId: function(queue_id) {
    this.lastViewed = null;

    this.lastQueueId = queue_id;
    this.setNextQueueId();
  },
  setNextQueueId: function() {
    this.nextQueueId = Miso.getRecord(Miso.findNextRecordId(this.lastQueueId)).MAKEITSO_QUEUE_ID;
  },

  highlightLastViewed: function() {
    if (this.nextQueueId == null && this.lastViewed != null)
      this.hightlightRow(Miso.findRecordId(this.lastViewed), '#FF8B3D');
  },
  highlightNextQueueId: function() {
    if (this.lastViewed == null && this.nextQueueId != null)
      this.hightlightRow(Miso.findRecordId(this.nextQueueId), '#FF38CB');
  },
  hightlightRow: function(row, color) {
    var table = document.getElementById('data');
    for (var i = 0; i < Miso.records.length; i++)
    {
      if (Miso.records[i].STATUS != 'PENDING')
        table.rows[i + 1].style.background = (i == row) ? color : 'none';
    }
  },

  toggleSpinner: function(setTo) {
    document.getElementById('spinner').style.display = (setTo) ? null : 'none';
  },

  buildConsoleMessage: function(queue_id) {
    var table = document.getElementById('data');
    for (var i = 0; i < table.rows.length; i++)
    {
      if (table.rows[i].cells[1].innerHTML == queue_id)
        return table.rows[i].cells[5].innerHTML + ' - customer_id: ' + table.rows[i].cells[3].innerHTML;
    }
  },

  // return delta in seconds date - from
  getDeltaFrom: function(from, date)
  {
    if (from == null || date == null)
      return null;
  
    // add space between time and ante or post meridiem
    from = this.convertDateToEpoch(from);
    date = this.convertDateToEpoch(date);

    var delta = date - from;

    var totalSec = delta / 1000;
    var hours = parseInt( totalSec / 3600 ) % 24;
    var minutes = parseInt( totalSec / 60 ) % 60;
    var seconds = totalSec % 60;

    var result = (hours < 10 ? "0" + hours : hours) 
    + ":" + (minutes < 10 ? "0" + minutes : minutes) 
    + ":" + (seconds  < 10 ? "0" + seconds : seconds);

    return result;
  },

  /** convertDateToEpoch
  *  parse date string and return epoch value
  */
  convertDateToEpoch: function(date)
  {
    var date_split = date.split(' ');
    var mdy_split = date_split[0].split('/');
    mdy_split[2] = 20 + '' + mdy_split[2];

    date = mdy_split[0] + '/' + mdy_split[1] + '/' + mdy_split[2] + ' ' + date_split[1];
    date = date.slice(0, -2) + " " + date.slice(-2);

    return Date.parse(date);
  },

  extractLastErrorMsg: function(from)
  {
    // extract last error message
    var last = from.substr(from.lastIndexOf(';') + 1); // last MISO event
    last = last.split('|')[0]; // remove date
  
    if (last.indexOf('(') != -1)// remove STATUS
      last = last.substr(last.indexOf('('));  

    last = last.replace(/\(|\)/gi, ''); // remove parenthesis

    if (last.indexOf(':') != -1) // remove error code if present
      last = last.substr(last.indexOf(':') + 1);

    if (last == 'Timeout')
      last = 'Invocation Timed Out';
    return last;
  }
}

var BatchCommands = {
  active: false,
  queue: [],
  toggle: function(setTo) {
    this.queue = [];
    if (this.active) this.removeCheckboxes();

    if (setTo != undefined)
      this.active = !setTo;
    
    var display = (this.active) ? 'none' : 'inline-block';
    this.active = !this.active;

    document.getElementById('batch_commands').style.display = display;

    if (this.active) this.addCheckboxes();
  },

  addCheckboxes: function() {
    var table = document.getElementById('data');

    for (var i = 0; i < table.rows.length; i++)
    {
      var html = (table.rows[i].cells[1].innerHTML == 'FAILED') ? '<input type="checkbox" />' : '&nbsp;';
      var col = table.rows[i].insertCell(0);
      col.innerHTML += html;

      col.addEventListener('click',function (e) {
        e.stopPropagation();
      });
    }
  },

  removeCheckboxes: function() {
    var table = document.getElementById('data');
    for (var i = 0; i < table.rows.length; i++)
      table.rows[i].cells[0].remove();
  },

  runBatch: function(type) {
    MisoUtil.reset();

    this.queue = [];

    var table = document.getElementById('data');
    for (var i = 0; i < table.rows.length; i++)
    {
      var checkbox = table.rows[i].cells[0].getElementsByTagName('input')[0];
      if (checkbox && checkbox.checked)
      {
        this.queue.push(table.rows[i].cells[1].innerHTML);
      }
    }

    if (this.queue.length == 0)
    {
      alert('No MISO items selected.');
      return;
    }
    
    clearConsole();
    showConsole();

    var queue_id = BatchCommands.nextQueueItem();
    if (queue_id)
    {
      switch (type)
      {
        case 'retry': makeItSoCommand('retry', queue_id); break;
        case 'redo': makeItSoCommand('redo', queue_id); break;
      }
    }
  },

  nextQueueItem: function() {
    var queueItem = this.queue.shift();
    return (queueItem) ? queueItem : false;
  }
}

// globals
var sortOrder =  'asc'; // current sort order
var sortProperty = 'CREATED_DATE_TIME'; // current sort property
var sortIndex = 0; // column number of the current sort
var misoStatus = ['FAILED', 'PENDING'];
var misoType = ['Deactivate', 'RenewPlan', 'Suspend', 'UpgradePlan'];
var misoAge = null;

/**
 * startUp
 * main execution function called when page is loaded
 */
function startUp()
{
  var table = document.getElementById('data');
  if (table.rows.length > 1)
  {
    for (var j = table.rows.length - 1; j > 0; j--)
      table.deleteRow(j);
  }

  MisoUtil.toggleSpinner(true);

  // prepare AJAX request
  var url = '/ultra_api.php';
  var params = 'bath=rest&partner=mvnecare&version=2&command=mvnecare__ListMakeitso'
  if (misoAge !== null)
    params += '&created_days_ago=' + misoAge;

  // encode arrays as 'x-www-form-urlencoded'
  for (var i = 0; i < misoStatus.length; i++)
    params += '&status[]=' + misoStatus[i];
  for (var i = 0; i < misoType.length; i++)
    params += '&type[]=' + misoType[i];

  // get MISO via POST
  ajax = new XMLHttpRequest();
  ajax.open('POST', url, true);
  ajax.onreadystatechange = function()
  {
    if (ajax.readyState == 4)
      processApiResponse(ajax);
  }
  ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajax.setRequestHeader("Content-length", params.length);
  ajax.send(params);
}


/**
 * changeStatus
 * add or remove members from misoStatus array as user clicks appropriate checkboxes
 */
function changeArray(array, value, checked)
{
  if (checked)
    array.push(value);
  else
  {
    var index = array.indexOf(value);
    array.splice(index, 1);
  }
}


/**
 * processApiResponse
 * AJAX callback to interpret API output and save it into global
 */
function processApiResponse(ajax)
{
  try
  {
    // check HTTP response
    var message;
    if (ajax.status != 200)
      throw (message = 'HTTP Server Error ' + ajax.status);

    // JSON may fail to parse: over-write default cryptic message before catching the exception
    message = 'invalid server response';
    var response = JSON.parse(ajax.responseText);

    // check for API errors
    if (response.errors.length)
      throw (message = 'API error ' + response.errors[0]);

    Miso.addRecords(response.records);

    // check for empty dataset
    if (Miso.records.length == 0)
      throw (message = 'Nothing found.');

    // check for too much data
    if (Miso.records.length > 1000)
      alert('More than 1,000 records available, showing most recent only.');

    // create blank data table rows and cells
    insertTableRows(response.record_count)

    // insert values into empty table
    fillInTableRows();

    // remove spinner screen
    MisoUtil.toggleSpinner(false);
    MisoUtil.restoreScroll();
    MisoUtil.highlightNextQueueId();
  }
  catch (error)
  {
    return alert(message);
  }
}


/**
 * insertTableRow
 * add count of blank rows to data table
 */
function insertTableRows(count)
{
  // add count table rows
  var table = document.getElementById('data');

  for (i = 0; i < count; i++)
  {
    var row = table.insertRow(-1);
    row.className = 'miso';
    for (var j = 0 ; j < table.rows[0].cells.length; j++) // same number of cells as first row
      row.insertCell(-1);
  }
}

/**
 * fillInTableRows
 * enter miso data into a table rows
 */
function fillInTableRows()
{
  var table = document.getElementById('data');
  var options;

  for (var i = 0; i < Miso.records.length; i++)
  {
    var index = i + 1; // index 0 is heading, data starts at 1
    var miso = Miso.records[i];

    var row = table.rows[index];
    row.style.background = (miso.STATUS == 'PENDING' && miso.LAST_ATTEMPT_DATE_TIME) ? getPendingColor(miso.LAST_ATTEMPT_DATE_TIME) : 'none';

    // row onclick event
    row.setAttribute('onclick', 'showDetails(' + miso.MAKEITSO_QUEUE_ID + ')');

    // miso
    row.cells[0].innerHTML = miso.MAKEITSO_QUEUE_ID;

    // status
    row.cells[1].innerHTML = miso.STATUS;
    row.cells[1].style.color = getStatusColor(miso.STATUS);

    // subscriber
    row.cells[2].innerHTML = miso.CUSTOMER_ID;

    // MSISDN
    if (typeof miso.MSISDN === 'undefined') // first time: try to get from options
    {
      try // to parse JSON
      {
        options = JSON.parse(miso.MAKEITSO_OPTIONS);
        Miso.records[i].MSISDN = options.msisdn;
      }
      catch (e) // failed to parse
      {
        Miso.records[i].MSISDN = null;
      }
      miso.MSISDN = Miso.records[i].MSISDN;
    }
    row.cells[3].innerHTML = miso.MSISDN;

    // ICCID
    if (typeof miso.ICCID === 'undefined')
    {
      if (typeof options.iccid === 'undefined')
        Miso.records[i].ICCID = null;
      else
        Miso.records[i].ICCID = options.iccid;
      miso.ICCID = Miso.records[i].ICCID;
    }
    row.cells[3].innerHTML += '<br/>' + miso.ICCID;

    // MISO type
    row.cells[4].innerHTML = miso.MAKEITSO_TYPE;

    // MISO sybtype
    row.cells[5].innerHTML = miso.MAKEITSO_SUBTYPE;

    // attempts
    row.cells[6].innerHTML = miso.ATTEMPT_COUNT;

    // started
    row.cells[7].innerHTML = miso.CREATED_DATE_TIME;

    // last attempt
    row.cells[8].innerHTML = miso.LAST_ATTEMPT_DATE_TIME;

    // next attempt
    row.cells[9].innerHTML = miso.NEXT_ATTEMPT_DATE_TIME;

    row.cells[10].innerHTML = MisoUtil.getDeltaFrom(miso.CREATED_DATE_TIME, miso.LAST_ATTEMPT_DATE_TIME);

    // last error code
    row.cells[11].innerHTML = miso.LAST_ERROR_CODE;

    // last error msg
    row.cells[12].innerHTML = MisoUtil.extractLastErrorMsg(miso.ATTEMPT_HISTORY);
  }
}

function getStatusColor(status) {
  var color = 'black';

  switch (status)
  {
    case 'DONE': color = 'green';
    case 'FAILED': color = 'red';
  }

  return color;
}

function getPendingColor(lastAttemptDate)
{
  var four_hours = 60*60*4*1000;
  var time_now = (new Date).getTime();

  var time_diff = time_now - MisoUtil.convertDateToEpoch(lastAttemptDate);
  return (time_diff < four_hours) ? 'lightgreen' : 'lightyellow';
}


/** showConfig functions
 *  hide data table and display settings
 */
function showConfig()
{
  window.scrollTo(0, 0); // prevent partially obstructed dialog
  document.getElementById('data').style.display = 'none';
  document.getElementById('gear').style.display = 'none';
  document.getElementById('config').style.display = 'block';
}


/**
 * hideConfig
 * hide settings and restore data table
 */
function hideConfig()
{
  document.getElementById('data').style.display = null;
  document.getElementById('gear').style.display = null;
  document.getElementById('config').style.display = 'none';

  // clear old table data
  var table = document.getElementById('data');
  for (var i = table.rows.length - 1; i > 0; i--)
  {
    for (var j = table.rows[i].cells.length - 1; j >= 0; j--)
      table.rows[i].deleteCell(j);
    table.deleteRow(i);
  }

  // start over
  startUp();
}


/**
 * sortMiso
 * (re)-sort miso data by property
 */
function sortMiso(property, index)
{
  if (BatchCommands.active) index--;
  BatchCommands.toggle(false);

  // remove existing sort marker
  var table = document.getElementById('data');
  for (var i = 0; i < table.rows[0].cells.length; i++)
  {
    table.rows[0].cells[i].innerHTML = table.rows[0].cells[i].innerHTML.split(' ↑')[0];
    table.rows[0].cells[i].innerHTML = table.rows[0].cells[i].innerHTML.split(' ↓')[0];
  }
  
  // reverse order if sort property is the same
  if (property == sortProperty)
  {
    if (sortOrder == 'asc')
      sortOrder = 'desc';
    else
      sortOrder = 'asc';
  }
  else
    sortIndex = index;

  // set sort marker
  if (sortOrder == 'desc') table.rows[0].cells[index].innerHTML +=  ' &#8595;';
  else table.rows[0].cells[index].innerHTML += ' &#8593;';

  sortProperty = property;

  // sort array of objects
  Miso.records = Miso.records.sort(
    function(object1, object2)
    {
      var result;

      if (property == 'LAST_DELTA')
        result = sortLastDelta(object1, object2);
      else
      {
        if (object2[property] == null)
        {
          return -1;
        }
        else if (object1[property] == null)
          return 1;

        if (property == 'ATTEMPT_HISTORY')
          result = sortAttemptHistory(object1, object2);
        else
        {
          if (object1[property] > object2[property])
            result = 1;
          else if (object1[property] < object2[property])
            result = -1;
          else
            result = 0;
        }
      }
      
      // reverse if desc
      if (sortOrder == 'desc')
      {
        if (result == -1) result = 1;
        else if (result == 1) result = -1;
      }

      return result;
    });

  fillInTableRows();
}

function sortLastDelta(object1, object2)
{
  if (object2['LAST_ATTEMPT_DATE_TIME'] == null)
    return -1;
  else if (object1['LAST_ATTEMPT_DATE_TIME'] == null)
    return 1;

  var result = 0;

  if (MisoUtil.getDeltaFrom(object1['CREATED_DATE_TIME'], object1['LAST_ATTEMPT_DATE_TIME']) > MisoUtil.getDeltaFrom(object2['CREATED_DATE_TIME'], object2['LAST_ATTEMPT_DATE_TIME']))
    result = 1;
  else if (MisoUtil.getDeltaFrom(object1['CREATED_DATE_TIME'], object1['LAST_ATTEMPT_DATE_TIME']) < MisoUtil.getDeltaFrom(object2['CREATED_DATE_TIME'], object2['LAST_ATTEMPT_DATE_TIME']))
    result = -1;

  return result;
}

function sortAttemptHistory(object1, object2)
{
  var result = 0;

  if (MisoUtil.extractLastErrorMsg(object1['ATTEMPT_HISTORY']).trim() > MisoUtil.extractLastErrorMsg(object2['ATTEMPT_HISTORY']).trim())
    return 1;
  else if (MisoUtil.extractLastErrorMsg(object1['ATTEMPT_HISTORY']).trim() < MisoUtil.extractLastErrorMsg(object2['ATTEMPT_HISTORY']).trim())
    return -1;

  return result;
}

function addToConsole(msg)
{
  document.getElementById('batch_console_table').innerHTML += '<tr><td>' + msg + '</td></tr>';
}

function clearConsole()
{
  document.getElementById('batch_console_table').innerHTML = '';
}

function showConsole() {
  MisoUtil.storeScroll();

  document.getElementById('data').style.display = 'none';
  document.getElementById('gear').style.display = 'none';
  document.getElementById('batch_console').style.display = 'block';
}

function hideConsole() {
  BatchCommands.toggle(false);

  document.getElementById('data').style.display = 'block';
  document.getElementById('gear').style.display = 'block';
  document.getElementById('batch_console').style.display = 'none';

  startUp();
}

function makeItSoCommand(cmd, queue_id)
{
  if (cmd == 'hide')
  {
    var r = confirm('Are you certain you wish to hide makeitso item ' + queue_id + '?');
    if (r == false)
      return false;
  }

  MisoUtil.setLastQueueId(queue_id);
  MisoUtil.toggleSpinner(true);

  var url = '/ultra_api.php';

  var formData = new FormData();
  formData.append('bath', 'rest');
  formData.append('partner', 'mvneinternal');
  formData.append('version', 2);
  formData.append('makeitso_queue_id', queue_id);
  formData.append('reason', 'miso');

  var api = '';
  switch (cmd)
  {
    case 'redo': api = 'mvneinternal__RedoMakeItSo'; break;
    case 'retry': api = 'mvneinternal__RetryMakeItSo'; break;
    case 'hide': api = 'mvneinternal__HideMakeitso'; break;
  }
  formData.append('command', api);

  // get MISO via POST
  ajax = new XMLHttpRequest();
  ajax.makeItSoCommand = cmd;
  ajax.open('POST', url, true);
  ajax.onreadystatechange = function()
  {
    if (ajax.readyState == 4)
    {
      var response = JSON.parse(ajax.responseText);
      // check for API errors
      if (response.errors.length)
      {
        addToConsole(response.errors[0]);
        if (!BatchCommands.active)
        {
          alert('API error ' + response.errors[0]);
          MisoUtil.toggleSpinner(false);
          return;
        }
      }
      else
      {
        addToConsole(ajax.makeItSoCommand + ' initiated - ' + MisoUtil.buildConsoleMessage(MisoUtil.lastQueueId));
        document.getElementById('retry_redo').innerHTML = (ajax.makeItSoCommand == 'hide') ? 'Hide MISO Complete' : ajax.makeItSoCommand + ' initiated.';
      }

      var queue_id = BatchCommands.nextQueueItem();
      if (queue_id)
      {
        switch (ajax.makeItSoCommand)
        {
          case 'redo': makeItSoCommand('redo', queue_id); break;
          case 'retry': makeItSoCommand('retry', queue_id); break;
        }
      }
      else
      {
        if (!BatchCommands.active) startUp();
        else
        {
          MisoUtil.reset();
          MisoUtil.toggleSpinner(false);
        }
      }
    }
  }
  ajax.send(formData);
}

function showRecentMakeItSo(customer_id)
{
  document.getElementById('makeItSoDetail').innerHTML = '';

  var now = moment().unix();

  var date_from = moment.unix(now - 29*24*60*60).zone('+0000').format('DD-MM-YYYY');
  var date_to = moment.unix(now + 3*24*60*60).zone('+0000').format('DD-MM-YYYY');

  // prepare AJAX request
  var url = '/ultra_api.php';
  var params = 'bath=rest&partner=mvnecare&version=2&command=mvnecare__ListCustomerMakeitso'
  params += '&customer_id=' + customer_id;
  params += '&date_from=' + date_from;
  params += '&date_to=' + date_to;

  // get MISO via POST
  ajax = new XMLHttpRequest();
  ajax.open('POST', url, true);
  ajax.onreadystatechange = function()
  {
    if (ajax.readyState == 4)
    {
      var response = JSON.parse(ajax.responseText);
      if (response.records.length == 0)
        return;

      var html = '<tr>'
        + '<th>ID</th>'
        + '<th>Status</th>'
        + '<th>Started (PST)</th>'
        + '<th>Command</th>'
        + '<th>Attempts</th>'
        + '<th>Last Attempt Date (PST)</th>'
        + '</tr>';

      for (var i = 0; i < response.records.length; i++)
      {
        var record = response.records[i];
        html += '<tr>'
        + '<td>' + record.MAKEITSO_QUEUE_ID + '</td>'
        + '<td>' + record.STATUS + '</td>'
        + '<td>' + record.CREATED_DATE_TIME + '</td>'
        + '<td>' + record.MAKEITSO_TYPE + '</td>'
        + '<td>' + record.ATTEMPT_COUNT + '</td>'
        + '<td>' + record.LAST_ATTEMPT_DATE_TIME + '</td>'
        + '</tr>';
      }

      document.getElementById('makeItSoDetail').innerHTML = html;
    }
  }
  ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  ajax.setRequestHeader("Content-length", params.length);
  ajax.send(params);
}

/**
 * showDetails
 * show Miso Details dialog for the given MAKEITSO_QUEUE_ID
 */
function showDetails(id)
{
  MisoUtil.setLastViewed(id);

  BatchCommands.toggle(false);

  MisoUtil.storeScroll();
  window.scrollTo(0, 0); // prevent partially obstructed dialog
  document.getElementById('data').style.display = 'none';
  document.getElementById('gear').style.display = 'none';
  document.getElementById('details').style.display = 'block';

  // fill in detail values: first column
  var miso = Miso.records.filter(function(obj){ return obj.MAKEITSO_QUEUE_ID == id; })[0]; // sleek!
  document.getElementById('detailId').innerHTML = id;
  document.getElementById('detailStatus').innerHTML = miso.STATUS;
  document.getElementById('detailCustomerId').innerHTML = miso.CUSTOMER_ID;
  document.getElementById('detailAction').innerHTML = miso.ACTION_UUID;
  document.getElementById('detailType').innerHTML = miso.MAKEITSO_TYPE;
  document.getElementById('detailSubtype').innerHTML = miso.MAKEITSO_SUBTYPE;
  document.getElementById('detailCreated').innerHTML = miso.CREATED_DATE_TIME;
  document.getElementById('detailCompleted').innerHTML = miso.COMPLETED_DATE_TIME;

  // second column
  document.getElementById('detailApi').innerHTML = miso.LAST_ACC_API;

  // process options
  var options = miso.MAKEITSO_OPTIONS.replace('/{|}|"/gi', '');
  options = options.replace(/,/gi, '<br>');
  document.getElementById('detailOptions').innerHTML = options;

  document.getElementById('detailAttempts').innerHTML = miso.ATTEMPT_COUNT;
  document.getElementById('detailLast').innerHTML = miso.LAST_ATTEMPT_DATE_TIME;
  document.getElementById('detailNext').innerHTML = miso.NEXT_ATTEMPT_DATE_TIME;
  document.getElementById('detailError').innerHTML = miso.LAST_ERROR_CODE;
  
  // extract last error message
  document.getElementById('detailMessage').innerHTML = MisoUtil.extractLastErrorMsg(miso.ATTEMPT_HISTORY);

  if (miso.STATUS == 'FAILED')
  {
    document.getElementById('retry_redo').innerHTML = ''
      + '<button onclick=\'makeItSoCommand("retry", ' + id + ');\'>Retry</button>'
      + '<button onclick=\'makeItSoCommand("redo", ' + id + ');\'>Redo</button>'
      + '<button onclick=\'makeItSoCommand("hide", ' + id + ');\'>Hide</button>';
  }

  showRecentMakeItSo(miso.CUSTOMER_ID);
}


/**
 * hideDetails
 * hide MISO details and restore data table
 */
function hideDetails()
{
  document.getElementById('data').style.display = null;
  document.getElementById('gear').style.display = null;
  document.getElementById('details').style.display = 'none';

  MisoUtil.highlightLastViewed();
  MisoUtil.restoreScroll();
}

</script>

<title>MISO</title>
</head>

<body onload="startUp()">

  <!-- loading spinner -->
  <img id="spinner" class="centered" src='miso.gif' style='display: none;'/>
  
  <!-- config gear -->
  <div id="toolbar">
    <div class="toolbar_icon" id="gear"><img onclick="showConfig()" title='Configuration Settings' src="miso.png"/></div>
    <div id="batch_commands" style="display: none">
      <button onclick="BatchCommands.runBatch('redo');">Redo</button>
      <button onclick="BatchCommands.runBatch('retry');">Retry</button>
    </div>
    <div class="toolbar_icon" id="list_icon"><img onclick="BatchCommands.toggle();" title='Batch Commands' src="miso_list.png"/></div>
  </div>

  <!-- data table -->
  <table id="data" class="data">
    <!-- table header-->
    <tr id="header" class="header" title='click to sort by this column'>
      <td onclick="sortMiso('MAKEITSO_QUEUE_ID', this.cellIndex)">MISO</td>
      <td onclick="sortMiso('STATUS', this.cellIndex)">STATUS</td>
      <td onclick="sortMiso('CUSTOMER_ID', this.cellIndex)">SUBSCRIBER</td>
      <td onclick="sortMiso('MSISDN', this.cellIndex)">MSISDN/ICCID</td>
      <td onclick="sortMiso('MAKEITSO_TYPE', this.cellIndex)">TYPE</td>
      <td onclick="sortMiso('MAKEITSO_SUBTYPE', this.cellIndex)">SUBTYPE</td>
      <td onclick="sortMiso('ATTEMPT_COUNT', this.cellIndex)">ATTEMPTS</td>
      <td onclick="sortMiso('CREATED_DATE_TIME', this.cellIndex)">STARTED (PST)</td>
      <td onclick="sortMiso('LAST_ATTEMPT_DATE_TIME', this.cellIndex)">LAST (PST)</td>
      <td onclick="sortMiso('NEXT_ATTEMPT_DATE_TIME', this.cellIndex)">NEXT (PST)</td>
      <td onclick="sortMiso('LAST_DELTA', this.cellIndex)">LAST</td>
      <td onclick="sortMiso('LAST_ERROR_CODE', this.cellIndex)">LAST ERROR CODE</td>
      <td onclick="sortMiso('ATTEMPT_HISTORY', this.cellIndex)">LAST ERROR MSG</td>
    </tr>
    <!-- data rows are inserted here-->
  </table>

  <!-- config dialog -->  
  <div id="config" class="config">
    <span class="configHeader">Configuration Settings</span>
    <table class="configTable">
      <tr>
        <td class="configTitle">Status</td>
        <td></td>
        <td class="configTitle">Type</td>
        <td></td>
        <td class="configTitle">Created</td>
      </tr>
      <tr>
        <td class=configValue>
          <input type="checkbox" name="STATUS" value="DONE" onclick="changeArray(misoStatus, this.value, this.checked)"> Done</input><br>
          <input type="checkbox" name="STATUS" value="FAILED" checked=true onclick="changeArray(misoStatus, this.value, this.checked)"> Failed</input><br>
          <input type="checkbox" name="STATUS" value="PENDING" checked=true onclick="changeArray(misoStatus, this.value, this.checked)"> Pending</input><br>
          <input type="checkbox" name="STATUS" value="WITHDRAWN" onclick="changeArray(misoStatus, this.value, this.checked)"> Withdrawn</input>
        </td>
        <td></td>
        <td class=configValue>
          <input type="checkbox" name="TYPE" value="Deactivate" checked=checked onclick="changeArray(misoType, this.value, this.checked)"> Deactivate</input><br>
          <input type="checkbox" name="TYPE" value="RenewPlan" checked=checked onclick="changeArray(misoType, this.value, this.checked)"> RenewPlan</input><br>
          <input type="checkbox" name="TYPE" value="Suspend" checked=checked onclick="changeArray(misoType, this.value, this.checked)"> Suspend</input><br>
          <input type="checkbox" name="TYPE" value="UpgradePlan" checked=checked onclick="changeArray(misoType, this.value, this.checked)"> UpgradePlan</input>
        </td>
        <td></td>
        <td class=configValue>
          <input type="radio" name="OLD" value="1" onclick="misoAge = 1;"> less than a day ago<br>
          <input type="radio" name="OLD" value="2" onclick="misoAge = 2;"> less than 2 days ago<br>
          <input type="radio" name="OLD" value="3" onclick="misoAge = 4;"> less than 4 days ago<br>
          <input type="radio" name="OLD" value="7" onclick="misoAge = 7;"> less than a week ago<br>
          <input type="radio" name="OLD" value="" checked=true onclick="misoAge = null;"> any time<br>
        </td>
      <tr>
    </table>
    <button class="configButton" type="button" onclick="hideConfig()">&nbsp;Ok&nbsp;</button>
  </div>

  <div id="batch_console" class="details">
    <span class="configHeader">Batch Console <span id="batch_type"></span></span>
    <table id="batch_console_table">
    </table>

    <button class="detailsButton" type="button" onclick="hideConsole()">&nbsp;Ok&nbsp;</button>
  </div>

  <!-- MISO details dialog -->  
  <div id="details" class="details">
    <span class="configHeader">MISO Details</span>
    <table class="detailsTable">
      <tr>
        <td>ID:</td>
        <td id="detailId"></td>
        <td>ACC API:</td>
        <td id="detailApi"></td>
      </tr>
      <tr>
        <td>Status:</td>
        <td id="detailStatus"></td>
        <td>Attempts:</td>
        <td id="detailAttempts"></td>
      </tr>
      <tr>
        <td>Customer_id:</td>
        <td id="detailCustomerId"></td>
        <td>Last:</td>
        <td id="detailLast"></td>
      </tr>
      <tr>
        <td>Action UUID:</td>
        <td id="detailAction"></td>
        <td>Next:</td>
        <td id="detailNext"></td>
      </tr>
      <tr>
        <td>Type:</td>
        <td id="detailType"></td>
        <td>Completed:</td>
        <td id="detailCompleted"></td>
      </tr>
      <tr>
        <td>Subtype:</td>
        <td id="detailSubtype"></td>
        <td>ACC Error:</td>
        <td id="detailError"></td>
      </tr>
      <tr>
        <td>Created:</td>
        <td id="detailCreated"></td>
      </tr>
      <tr>
        <td>ACC API Parameters:</td>
        <td id="detailOptions"></td>
        <td>Message:</td>
        <td id="detailMessage"></td>
      </tr>
    </table>

    <div id="retry_redo">
    </div>

    <table id="makeItSoDetail">
    </table>

    <button class="detailsButton" type="button" onclick="hideDetails()">&nbsp;Ok&nbsp;</button>
  </div>
</body>
</html>

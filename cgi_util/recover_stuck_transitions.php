<?php

/*
 */

?>
<html>
  <head>
    <title>RECOVER ABORTED TRANSITION</title>
    <link rel="stylesheet" href="/css/jquery.datetimepicker.css">
    <script type="text/javascript" src="/js/show_environment_stage.js"></script>
    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/jquery.datetimepicker.js"></script>
    <script type="text/javascript">
      $(function() {
        $('#newExpirationDate').datetimepicker({
          format:'Y-m-d H:i:s'
        });
      });

      var readyText   = 'EXECUTE';
      var workingText = 'WORKING';

      var readyColor   = 'red';
      var workingColor = '#888';

      var baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

      function internal__GetAllStuckTransitions()
      {
        var c = confirm("Are you sure you wish to execute?");
        if (!c) return;

        var transition_uuid = $('#input_transition_uuid').val();

        $('#theButton').css('background-color', workingColor);
        $('#theButton').text(workingText);

        $.ajax({
          method: 'GET',
          url: baseURL + '&command=internal__GetAllStuckTransitions',
          data: 'transition_uuid=' + transition_uuid,
          success: function(data, status, settings) {
            var response = JSON.parse(data);

            console.log(response);
            $('#input_transition_uuids').val('');

            if ((response.errors && response.errors.length > 0) || (response.success !== true))
            {
              $('#output').hide();
              $('#message').html('<div style="color:red">' + response.errors[0] + '</div>').show();

              setTimeout(function() {
                $('#message').hide()
              }, 5000);

            }
            else
            {
              var envs = ["DEV","QA","PROD"];

              var tbl = '<table>';
              tbl += '<tr>';
              tbl += '<th></th>';
              tbl += '<th>TRANSITION_UUID</th>';
              tbl += '<th>CUSTOMER_ID</th>';
              tbl += '<th>CREATED</th>';
              tbl += '<th>FROM_STATE</th>';
              tbl += '<th>TO_STATE</th>';
              tbl += '<th>ENV</th>';
              tbl += '</tr>';

              var j = 0;
              for (var i = 0; i < response.stuck_transitions.length; i++)
              {
                var transition = response.stuck_transitions[i];

                if (envs.indexOf(transition[5]) == -1)
                  continue;

                var txt = $('#input_transition_uuids').val();
                if (j) txt = txt + ',';
                txt = txt + response.stuck_transitions[i][0];
                $('#input_transition_uuids').val(txt);

                tbl += '<tr>';
                tbl += '<td>' + j + '</td>';
                tbl += '<td>' + transition[0] + '</td>';
                tbl += '<td>' + transition[1] + '</td>';
                tbl += '<td>' + transition[2] + '</td>';
                tbl += '<td>' + transition[3] + '</td>';
                tbl += '<td>' + transition[4] + '</td>';
                tbl += '<td>' + transition[5] + '</td>';
                tbl += '</tr>';

                j++;
              }
              tbl += '</table>';

              $('#output').show();
              $('#output').html(tbl);
            }

            $('#theButton').css('background-color', readyColor);
            $('#theButton').text(readyText);
          }
        });
      }

      function internal__RecoverStuckTransition()
      {
        var c = confirm("Are you sure you wish to execute?");
        if (!c) return;

        var transition_uuid = $('#input_transition_uuids').val();

        $('#theButton').css('background-color', workingColor);
        $('#theButton').text(workingText);

        $.ajax({
          method: 'GET',
          url: baseURL + '&command=internal__RecoverStuckTransition',
          data: 'transition_uuids=' + transition_uuid,
          success: function(data, status, settings) {
            var response = JSON.parse(data);

            console.log(response);
            $('#input_transition_uuids').val('');

            if ((response.errors && response.errors.length > 0) || (response.success !== true))
            {
              $('#output').hide();
              $('#message').html('<div style="color:red">' + response.errors[0] + '</div>').show();

              setTimeout(function() {
                $('#message').hide()
              }, 5000);

            }
            else
            {
              var tbl = '<table>';

              if (response.aborted.length)
              {
                tbl += '<tr><strong>ABORTED</strong></tr>';
                for (var i = 0; i < response.aborted.length; i++)
                  tbl += '<tr><td>' + response.aborted[i] + '</td></tr>';
              }

              if (response.closed.length)
              {
                tbl += '<tr><strong>CLOSED</strong></tr>';
                for (var i = 0; i < response.closed.length; i++)
                  tbl += '<tr><td>' + response.closed[i] + '</td></tr>';
              }

              if (response.failed.length)
              {
                tbl += '<tr><strong>FAILED</strong></tr>';
                for (var i = 0; i < response.failed.length; i++)
                  tbl += '<tr><td>' + response.failed[i] + '</td></tr>';
              }

              tbl += '</table>';

              $('#output').show();
              $('#output').html(tbl);
            }

            $('#theButton').css('background-color', readyColor);
            $('#theButton').text(readyText);
          }
        });
      }
    </script>
    <style>
      div { margin: 10px; 0px}
      #container {
        font-family: sans-serif;
        width : 768px;
        margin: 0px auto;
        text-align: center;
      }

      label { font-weight: bold; }
      #theButton {
        background-color: red;
        border: none;
        cursor: pointer;
      }
      #output { margin-top: 32px; }

      table {
        border-collapse: collapse;
        width: 100%;
      }

      th, td {
        text-align: left;
        padding: 8px;
      }

      tr:nth-child(even){background-color: #f2f2f2}
    </style>
  </head>
  <body>
    <div id="container">
      <h1>RECOVER STUCK TRANSITIONS</h1>

      <div>
        <label>GET STUCK TRANSITIONS:</label>
        <button id="GetStuckTransition" onclick="internal__GetAllStuckTransitions();">Get Stuck Transitions</button>
      </div>

      <div>
        <label>TRANSITION UUIDS <small>comma delimited</small></label>
        <br />
        <textarea id="input_transition_uuids" type="text" name="transition_uuids" cols="64" rows="16"></textarea>
        <br />
        <button id="theButton" onclick="internal__RecoverStuckTransition();">EXECUTE</button>
      </div>
      <!--
      <div>
        <label>Expiration Date:</label>
        <input type="text" id="newExpirationDate">
      </div>
    -->
      <div id="message"></div>
    </div>
    <div id="output" style="display: none;"></div>
  </body>
</html>
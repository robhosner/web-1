<?php
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <title>Aborted Transitions Recovery Tool</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <link href="../styles/bootstrap2/css/bootstrap.min.css" rel="stylesheet">
  <link href="../styles/bootstrap2/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link href="../styles/modals.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="../styles/datepicker.css" rel="stylesheet" type="text/css" />

  <script type="text/javascript" src="/js/show_environment_stage.js"></script>
  <script language="javascript" src="../js/modernizr_custom.min.js"></script>
  <script language="javascript">

  // identifies ``Recover`` anchors
  var transition_ord = 0;

  Modernizr.load([
                   {
                   test: Modernizr.mq('only all'),
                   nope: '../styles/oldIE.css'
                   },
                   {
                   load: '//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js',
                   complete: function () { if ( !window.jQuery ) { Modernizr.load('js/jquery-1.7.2.min.js'); }}
                   },
                   {
                   load: [
                     "../js/jquery-ui-1.8.23.custom.min.js",
                     "../styles/bootstrap2/js/bootstrap.min.js",
                     "../styles/bootstrap2/js/bootstrap-datepicker.js",
                       "../js/sitewide.js"
                     ],

                   complete: function () {
                       $(document).ready(function() {

var l_xToday = new Date();
var l_sToday = zeroFill(l_xToday.getMonth()+1,2) + '-' + zeroFill(l_xToday.getDate(),2) + '-' + l_xToday.getFullYear();

$('#date_input').val(l_sToday).datepicker({
  format: 'mm-dd-yyyy'
});

$("#div_button_loading").hide();
$("#div_un_archive_loading").hide();

$("#button_un_archive").click(function() {

  if ( $("#un_archive_uuid").val() == '' )
    return false;

  $("#div_un_archive").hide();
  $("#div_un_archive_loading").show();
  un_archive_transition();
  return false;
});

$("#button_search").click(function() {
  $("#div_button1").hide();
  $("#div_button2").hide();
  $("#div_button_loading").show();
  $('#ajax_target_1, #ajax_transitions').html('');
  transitions_search();
  return false;
});

$("#button_search_stuck").click(function() {
  $("#div_button1").hide();
  $("#div_button2").hide();
  $("#div_button_loading").show();
  $('#ajax_target_1, #ajax_transitions').html('');
  stuck_transitions_search();
  return false;
});

$('body').on('click', 'a.recover_all_stuck_transitions_anchor', function() {
  recover_all_stuck_transitions();
  return false;
});

// associate recover transition
$('body').on('click', 'a.recover_transition', function() {
    recover_transition( $(this).attr("value") , $(this).attr("id") );
    return false;
});



                       });
                     }
                   },

                   {
                   test : window.JSON,
                   nope : '../js/json2.js'
                   }
                   ]);

function restore_buttons()
{
  $("#div_button_loading").hide();
  $("#div_button1").show();
  $("#div_button2").show();
}

function un_archive_transition()
{
  $.ajax({
    type:     'POST',
    url:      "/pr/internal/2/ultra/api/internal__UnarchiveTransition",
    dataType: 'json',
    data:     { transition_uuid : $("#un_archive_uuid").val() },
    error:    function errorUnarchiveTransition(data) {

                $("#div_un_archive_result").html( "API error" );

                $("#div_un_archive").show();
                $("#div_un_archive_loading").hide();

              },
    success:  function callbackUnarchiveTransition(data) {

                var html = '';

                if ( data == null ) {
                  html = 'internal__UnarchiveTransition returned no data :_(<br>';
                } else if ( data.success ) {
                  html = 'success';
                } else {
                  html = 'internal__UnarchiveTransition failed';
                }

                $("#div_un_archive_result").html( html );

                $("#div_un_archive").show();
                $("#div_un_archive_loading").hide();

              }
  });
}

function recover_all_stuck_transitions()
{
  $("#div_button1").hide();
  $("#div_button2").hide();
  $("#recover_all_stuck_transitions_div").hide();
  $("#div_button_loading").show();

  $.ajax({
    type:     'POST',
    url:      "/pr/internal/2/ultra/api/internal__RecoverAllStuckTransitions",
    dataType: 'json',
    data:     {},
    error:    function errorRecoverAllStuckTransitions(data) {

                $("#ajax_transitions").append( "<br>RecoverAllStuckTransitions error" );

                restore_buttons();

              },
    success:  function callbackRecoverAllStuckTransitions(data) {

                var html = '';

                if ( data == null ) {
                  html = 'internal__RecoverAllStuckTransitions returned no data :_(<br>';
                } else if ( data.success ) {
                  html = 'success';
                } else {
                  html = 'internal__RecoverAllStuckTransitions failed';
                }

                $("#ajax_transitions").html( html );

                restore_buttons();

              }
  });
}

function stuck_transitions_search()
{
  $.ajax({
    type:     'POST',
    url:      "/pr/internal/2/ultra/api/internal__GetAllStuckTransitions",
    dataType: 'json',
    data:     {},
    error:    function errorGetAllStuckTransitions(data) {

                $("#ajax_transitions").append( "<br>GetAllStuckTransitions error" );

                restore_buttons();

              },
    success:  function callbackGetAllStuckTransitions(data) {

                var html = '';

                if ( data == null ) {
                  html = 'internal__GetAllStuckTransitions returned no data :_(<br>';
                } else if ( data.success ) {
                  html = showStuckTransitions( data.stuck_transitions );
                } else {
                  html = 'internal__GetAllStuckTransitions failed';
                }

                $("#ajax_transitions").html( html );

                restore_buttons();

              }
  });
}

function transitions_search()
{
  $.ajax({
    type:     'POST',
    url:      "/pr/internal/1/ultra/api/internal__GetAllAbortedTransitions",
    dataType: 'json',
    data:     { transition_date : $("#date_input").val() },
    error:    function errorGetAllAbortedTransitions(data) {

                $("#ajax_transitions").append( "<br>GetAllAbortedTransitions error" );

                restore_buttons();

              },
    success:  function callbackGetAllAbortedTransitions(data) {

                var html = '';

                if ( data == null ) {
                  html = 'internal__GetAllAbortedTransitions returned no data :_(<br>';
                } else if ( data.success ) {
                  html = showAbortedTransitions( data );
                } else {
                  html = 'internal__GetAllAbortedTransitions failed';
                }

                $("#ajax_transitions").html( html );

                restore_buttons();

              }
  });
}

function showStuckTransitions( data )
{
  var html = '';

  if ( data.length == 0 )
  {
    html = '<h3>No Stuck Transitions Found</h3>';
  }
  else
  {
    html = '<h3>' + data.length + ' Stuck Transitions Found</h3>' +
           '<div id="recover_all_stuck_transitions_div"><b><a class="recover_all_stuck_transitions_anchor" href="#">Recover all</a></b></div>' +
           '<table border="1">' +
           '<tr><th>UUID</th><th>Created</th><th>From</th><th>To</th><th>Environment</th></tr>';

    for ( var i=0 ; i<data.length ; i++ )
    {
      html += '<tr><td>' + data[i][0] + '</td><td>' + data[i][1] + '</td><td>' + data[i][2] + '</td><td>' + data[i][3] + '</td><td>' + data[i][4] + '</td></tr>';
    }

    html += '</table>';
  }

  return html;
}

function showAbortedTransitions( data )
{
  var html =
    '<table border="1"><tr>  <td><b>From</b></td>  <td><b>To</b></td>  <td nowrap><b>Transition Id</b></td>  <td nowrap><b>Transition Label</b></td>  <td><b>Date</b></td>  <td nowrap><b>Customer Id</b></td>  <td><b>Errors</b></td>  </tr>';

  html += showAbortedTransitionsByType( data.transitions_active_to_suspended    , 'Active'       , 'Suspended'    );
  html += showAbortedTransitionsByType( data.transitions_active_to_active       , 'Active'       , 'Active'       );
  html += showAbortedTransitionsByType( data.transitions_suspended_to_active    , 'Suspended'    , 'Active'       );
  html += showAbortedTransitionsByType( data.transitions_neutral_to_promounused , 'Neutral'      , 'Promo Unused' );
  html += showAbortedTransitionsByType( data.transitions_promounusedto_active   , 'Promo Unused' , 'Active'       );
  html += showAbortedTransitionsByType( data.transitions_neutral_to_provisioned , 'Neutral'      , 'Provisioned'  );
  html += showAbortedTransitionsByType( data.transitions_neutral_to_active      , 'Neutral'      , 'Active'       );

  html += '</table>';

  return html;
}

function recover_transition( transition_uuid , recoverlink_id )
{
  alert('Attempting '+transition_uuid+' recovery');

  $("#feedback"+recoverlink_id).html('... attempting recovery ...');

  // remove link so that we cannot attempt recovery again
  $("#"+recoverlink_id).remove();

  $.ajax({
    type:     'POST',
    url:      "/pr/internal/1/ultra/api/internal__RecoverFailedTransition",
    dataType: 'json',
    data:     { transition_uuid : transition_uuid },
    error:    function error_recover_transition(data) {
                $("#feedback"+recoverlink_id).html( 'call failed' );
              },
    success:  function callback_recover_transition(data) {

                html = '';

                if ( data == null ) {
                  html = 'call failed';
                } else if ( data.success ) {
                  html = 'recovery succeeded';
                } else {
                  html = 'recovery failed';
                }

                $("#feedback"+recoverlink_id).html( html );
              }
  });

  return false;
}

function showAbortedTransitionsByType( transitions , from , to )
{
  var html = '';

  if ( ( typeof(transitions) != undefined ) && ( transitions != '' ) && ( toString.call(transitions) == '[object Array]' ) )
  {
    for ( var i=0 ; i<transitions.length ; i+=5 )
    {
      transition_ord++;

      html +=
        '<tr>  <td><b>' + from + '</b></td>  <td><b>' + to + '</b></td>' +
              '<td nowrap>' + transitions[i] +
                ' &nbsp; <a href="#" id="recoverlink_' + transition_ord + '" class="recover_transition" value=' + transitions[ i ] + '>Recover</a> <p id="feedbackrecoverlink_'+transition_ord+'"</p></td>  ' +
              '<td nowrap>' + transitions[i+1] + '</td>  ' +
              '<td nowrap>' + transitions[i+2] + '</td>  ' +
              '<td nowrap>' + transitions[i+3] + '</td>  ' +
              '<td nowrap>' + transitions[i+4] + '</td>  </tr>';
    }
  }

  return html;
}

</script>
  <!-- html5.js for IE less than 9 -->
  <!--[if lt IE 9]>
  <style type='text/css'>
  </style>
  <![endif]-->
</head>

<body>
  <div class="container-fluid">
    <div class="row-fluid" id='main_id' name='main_id'>
      <div class='span12 wellx' style='padding-top:20px;'>
        <form name="mainform" id="mainform" action="/" method="POST">
          <table>
            <tr>
              <td>
                Transitions aborted from date :
                <input type="text" id="date_input" />
              </td>
              <td>
              <div id="div_button1">
                <input type="button" value="Search Aborted Transitions" id="button_search">
              </div>
              </td>
              <td>
              <div id="div_button2">
                <input type="button" value="Search Stuck Transitions" id="button_search_stuck">
              </div>
              </td>
              <td>
              <div id="div_button_loading"> <img src='../images/load_hero.gif' alt='' /> </div>
              </td>
            </tr>
            <tr>
              <td nowrap>
                Un-Archive a transition : <input type="text" id="un_archive_uuid" />
              </td>
              <td>
                <div id="div_un_archive">
                  <input type="button" value="Unarchive Transition" id="button_un_archive">
                </div>
                <div id="div_un_archive_loading"> <img src='../images/load_hero.gif' alt='' /> </div>
              </td>
              <td>
                <div id="div_un_archive_result"></div>
              </td>
            </tr>
          </table>
        </form>
      </div>
      <div>
        <table>
          <tr>
            <td>
              <div id="ajax_transitions" class='span12 wellx' style='padding-top:20px;'> </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div><!--/.fluid-container-->
</body>
</html>

<html>
<head>
<style>
label { float: left; width: 196px; }
input[type="text"] { width: 256px; }
a:visited { color: blue; }
</style>

<script type="text/javascript" src="../js/show_environment_stage.js"></script>
<script type="text/javascript" src="../js/jquery-2.0.3.js"></script>

<script type="text/javascript">
$(function() {

});

function runCommand()
{
  var c = confirm("Are you sure you wish to execute?");
  if (!c) return;

  var inputParameters = $("#api :input[value!='']").serialize();

  var url = '/ultra_api.php?version=2&bath=rest&format=json&partner=customercare';
  url = url + '&command=customercare__ForceJoinFamily';

  $.ajax({
    url: url,
    type: 'POST',
    data: inputParameters,
    dataType: 'json',
    success: function(res) {
      console.log(res);
      $('#console').html(JSON.stringify(res, null, 2));
    },
    error: function(res) {
      console.log(res);
      $('#console').html(JSON.stringify(res, null, 2));
    }
  });
}
</script>
</head>
<body>

<h3>FORCE JOIN FAMILY</h3>

<div id="console"></div>

<div id="api">

<p>
<label>Parent Customer ID:</label>
<input type="text" name="parent_customer_id" />
</p>

<p>
<label>Child Customer ID:</label>
<input type="text" name="child_customer_id" />
</p>

</div>

<p><button onclick="runCommand();">Execute</button></p>

</body>
</html>
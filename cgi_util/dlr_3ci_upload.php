#!/usr/bin/env php
<?php


/*
input file name is given as first argument
input file name must be an unzipped csv file with the following format:

MessageDirection,MessageType,MessageId,ClientId,ClientName,CarrierId,CarrierName,ShortCode,CountryCode,PhoneNumber,CampaignTypeId,CampaignTypeName,TriggerId,CampaignId,CampaignName,BillingTypeId,BillingTypeName,PricePoint,ClientTag,MessageText,MessageSubject,StatusId,StatusDescription,DetailedStatusId,DetailedStatusDescription,SubmittedDate
"MT","","13593809245928783212","1736","Ultra Mobile","28","T-Mobile","7770","US","+19493946933","","","125009","0","","","","0","client_tag","Yet another test SMS from Ultra - does 3ci work now? It's 2013-02-22 21:00:01","","1","Message delivered","1","Message delivered","2013-02-23 00:00:04"

Example :

%> php cgi_util/dlr_3ci_upload.php $FILENAME


Stash:

  https://source.hometowntelecom.com:8843/projects/CMD/repos/reports/browse

*/


include_once('db.php');
include_once('db/htt_3ci_notifications.php');


if ( ( ! isset($_SERVER['argv'][1]) ) || ( $_SERVER['argv'][1] == '' ) )
{
  dlog('',"dlr_3ci_upload.php invoked with no file_name");
  exit;
}

$file_name = $_SERVER['argv'][1];

dlog('',"dlr_3ci_upload.php invoked with file_name = $file_name");

dlr_3ci_upload_main($file_name);

// main body of the script
function dlr_3ci_upload_main($file_name) {

  try {

    if ( ! file_exists($file_name) ) {
      throw new Exception("file_name $file_name does not exist");
    }

    if ( ! is_readable($file_name) ) {
      throw new Exception("file_name $file_name is not readable");
    }

    // loop through DBs $current_db
    $databases = array('ULTRA_DATA','ULTRA_DEVELOP_TEL');

    $db_link = NULL;

    foreach( $databases as $current_db )
    {
      dlog('',"connecting to DB ]] $current_db [[");

      $db_link = switch_db( $current_db , $db_link );

      dlog('',"connected to DB ]] $current_db [[ ($db_link)");

      dlog('',"opening file $file_name");

      $file_pointer = fopen($file_name, 'r');

      if ( ! $file_pointer ) {
        throw new Exception("file_name $file_name cannot be opened");
      }

      dlr_3ci_upload_file($file_pointer,$db_link,$current_db);

      fclose($file_pointer);
    }

  }
  catch(Exception $e) {
    dlog('', 'ERROR: '.$e->getMessage());
  }

}

function validate_3ci_file_header($header) {

  // TODO

  return explode(",", $header);
}

function process_3ci_line($header_list,$line,$db_link,$current_db) {

  $params = array();

  $dlr_3ci_fields_mapping = dlr_3ci_fields_mapping();

  # remove " at the beginning and at the end of the line
  $line = preg_replace('/^\"/', '', $line);
  $line = preg_replace('/\"$/', '', $line);

  # from string to list
  $line = preg_replace('/[\n\r]/', '', $line);
  $data = explode('","', $line);

  # loop through header fields
  for ( $i = 0; $i < count($header_list); $i++) {
    # if we care about this field
    if ( ( isset($dlr_3ci_fields_mapping[ $header_list[$i] ]) ) && ( $dlr_3ci_fields_mapping[ $header_list[$i] ] != '' ) ) { 
      $params[ $dlr_3ci_fields_mapping[ $header_list[$i] ] ] = $data[$i];
    }
  }

  # adjustments
  $params["PhoneNumber"]    = substr($params["PhoneNumber"], -10);
  $params["DATE_SUBMITTED"] = '"'.$params["DATE_SUBMITTED"].'"';

  # to which messaging queue (DB) the message belongs
  $params["messaging_DB"]   = dlr_3ci_default_DB();

  # try to match HTT_MESSAGING_QUEUE_ID
  if ( ( ! isset( $params["HTT_MESSAGING_QUEUE_ID"] ) ) || ( ! $params["HTT_MESSAGING_QUEUE_ID"] ) ) {

    dlog('',"Found row with missing client_tag / HTT_MESSAGING_QUEUE_ID : [[ $line ]]");
    $params["HTT_MESSAGING_QUEUE_ID"] = 0;

  } elseif ( preg_match("/threeci_test/i", $params["HTT_MESSAGING_QUEUE_ID"])) {

    dlog('',"threeci_test : skipped (".$params["HTT_MESSAGING_QUEUE_ID"].")");
    $params["HTT_MESSAGING_QUEUE_ID"] = 0;

  } elseif ( $params["MessageDirection"] != "MT" ) {

    dlog('',"MessageDirection not MT : skipped");
    $params["HTT_MESSAGING_QUEUE_ID"] = 0;

  } else {

    if ( ! is_numeric( $params["HTT_MESSAGING_QUEUE_ID"] ) ) {
      # HTT_MESSAGING_QUEUE_ID contains DB name
      if ( preg_match("/^\s*(.+)\|(\d+)\s*$/i", $params["HTT_MESSAGING_QUEUE_ID"], $matches) ) {
        #HTT_MESSAGING_QUEUE_ID is in the form $DB . '|' . htt_messaging_queue.HTT_MESSAGING_QUEUE_ID
        $params["messaging_DB"]           = $matches[1];
        $params["HTT_MESSAGING_QUEUE_ID"] = $matches[2];
      } else {
        dlog('',"Found row with invalid client_tag / HTT_MESSAGING_QUEUE_ID : [[ $line ]]");
        $params["HTT_MESSAGING_QUEUE_ID"] = 0;
      }
    }
      
    if ( $params["HTT_MESSAGING_QUEUE_ID"] ) {

      # process lines only for the current DB
      if ( $current_db == $params["messaging_DB"] ) {

        # query HTT_MESSAGING_QUEUE
        $sql = htt_messaging_queue_select_query(
          array(
            'htt_messaging_queue_id'         => $params["HTT_MESSAGING_QUEUE_ID"],
            'customers_overlay_ultra_fields' => array('o.current_mobile_number')
          )
        );

        $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

        if ( $query_result && is_array($query_result) && count($query_result) ) {

          if ( $query_result[0]->current_mobile_number != $params["PhoneNumber"] ) {

            # phone number mismatch
            # this may be legitimate, since our customer could have changed MSISDN (?)
            dlog('',"MSISDN mismatch : ".$params["PhoneNumber"]." != ".$query_result[0]->current_mobile_number);
            $params["HTT_MESSAGING_QUEUE_ID"] = 0;

          }

        } else {

          dlog('',"Could not find a match for HTT_MESSAGING_QUEUE_ID = ".$params["HTT_MESSAGING_QUEUE_ID"]." in DB table ");
          $params["HTT_MESSAGING_QUEUE_ID"] = 0;

        }

      } else {

        # this is not the DB to which we are currently connected
        $params["HTT_MESSAGING_QUEUE_ID"] = NULL;

      }
    }
  }

  return $params;

}

function dlr_3ci_upload_file($file_pointer,$db_link,$current_db) {

  $line_n = 0;

  $batch_insert_size = 15;

  # uses ULTRA_DATA and ULTRA_DEVELOP_TEL
  $batch_data = array();

  // file header
  $header = fgets($file_pointer);
  $header = preg_replace('/[\n\r]/', '', $header);

  $header_list = validate_3ci_file_header($header);

  dlog('', "header : %s", $header_list);

  // rest of the file
  while ($line = fgets($file_pointer)) {

    $line_n++;

    // parse line
    $data_3ci = process_3ci_line($header_list,$line,$db_link,$current_db);

    if ( $data_3ci["HTT_MESSAGING_QUEUE_ID"] ) {
      // HTT_MESSAGING_QUEUE_ID matched, we will store this row in HTT_3CI_NOTIFICATIONS
      $batch_data[] = $data_3ci;
    } else {
      // HTT_MESSAGING_QUEUE_ID did not match, we discard this row
      $line_n--;
    }

    if ( ( ( $line_n % $batch_insert_size ) == 0 ) && ( count($batch_data) ) ) {
      # we assume we are already connected to the right DB
      batch_insert_htt_3ci_notifications($batch_data);
      $batch_data = array();
    }
  }

  if ( count($batch_data) ) {
    batch_insert_htt_3ci_notifications($batch_data);
  }

  dlog('', "Done inserting $line_n lines");

}

function dlr_3ci_fields_mapping() {

  return array(
    'MessageDirection'          => 'MessageDirection',
    'MessageType'               => '',
    'MessageId'                 => 'MESSAGE_ID',
    'ClientId'                  => '',
    'ClientName'                => '',
    'CarrierId'                 => '',
    'CarrierName'               => '',
    'ShortCode'                 => '',
    'CountryCode'               => '',
    'PhoneNumber'               => 'PhoneNumber',
    'CampaignTypeId'            => '',
    'CampaignTypeName'          => '',
    'TriggerId'                 => '',
    'CampaignId'                => '',
    'CampaignName'              => '',
    'BillingTypeId'             => '',
    'BillingTypeName'           => '',
    'PricePoint'                => '',
    'ClientTag'                 => 'HTT_MESSAGING_QUEUE_ID',
    'MessageText'               => '',
    'MessageSubject'            => '',
    'StatusId'                  => 'STATUS_ID',
    'StatusDescription'         => '',
    'DetailedStatusId'          => 'STATUS_ID_DETAILED',
    'DetailedStatusDescription' => '',
    'SubmittedDate'             => 'DATE_SUBMITTED'
  );

}

function dlr_3ci_default_DB() {
  return 'ULTRA_DATA';
}

?>

<?php

set_include_path('../..');
require_once('lib/transitions.php');

?>
<html>
<head>
  <title>Transition Graph</title>
  <script type="text/javascript" src="./js/raphael-min.js"></script>
  <script type="text/javascript" src="./js/dracula_graffle.js"></script>
  <script type="text/javascript" src="../../js/jquery-2.0.3.js"></script>
  <script type="text/javascript" src="./js/dracula_graph.js"></script>

  <style>
  * { font-family: sans-serif; }

  h3 {
    border-bottom: 1px solid #eee;
  }

  #inspector {
    background-color: #111;
    color: #eee;
    width: 45%;
    height: 100%;
    padding: 8px;
    position: absolute;
      top: 0;
      right: 0;

    overflow-y: scroll;
  }

  ul { background: none; }

  #actions_list div {
    background-color: #333;
    border: 1px solid #eee;
    margin-bottom: 4px;
    padding: 4px;
  }
  #transition_list li:hover {
    cursor: pointer;
  }
  </style>
</head>
<body>

<div id="toolbar">
  <select id="states" name="" onchange=""></select>
  <select id="direction" name="" onchange="">
    <option value="source">source</option>
    <option value="dest">dest</option>
  </select>
  <button onclick="transition_graph.load();">Load</button>
</div>

<script type="text/javascript">
var transitions = <?php echo json_encode(ultra_transitions()); ?>;

var redraw, g, renderer;

var transition_graph = {
  currentState: null,
  currentDirection: null,

  sourceColor: ['green', 'darkgreen'],
  destColor: ['blue','darkblue'],

  anchorNode: null,
  nodes: [],

  buildStateOptions: function() {
    var t = [];

    for (var i = 0; i < transitions.length; i++)
    {
      var source = transitions[i]['source']['print'];
      var dest = transitions[i]['dest']['print'];

      if (t.indexOf(source) == -1) t.push(source);
      if (t.indexOf(dest) == -1) t.push(dest);
    }

    for (var stateName in t)
    {
        document.getElementById('states').innerHTML += '<option value="' + t[stateName] + '">' + t[stateName] + '</option>';
    }
  },

  load: function() {
    this.currentState = document.getElementById('states').value;
    this.currentDirection = document.getElementById('direction').value;

    var width = $(document).width() / 2;
    var height = $(document).height() - 100;

    document.getElementById('canvas').innerHTML = '';
    g = new Graph();
    layouter = new Graph.Layout.Spring(g);
    renderer = new Graph.Renderer.Raphael('canvas', g, width, height);

    this.nodes = {};

    for (var i = 0; i < transitions.length; i++)
    {
      var source = transitions[i]['source']['print'];
      var dest = transitions[i]['dest']['print'];

      var subject = null;

      if (this.currentDirection == 'source') {
        if (source != this.currentState)
          continue;
      }

      if (this.currentDirection == 'dest') {
        if (dest != this.currentState)
          continue;
      }

      if (!this.nodes[source])
      {
        g.addNode(source, { label: source });
        if (this.currentDirection == 'dest') {
          this.nodes[source] = [];
        }
      }

      if (!this.nodes[dest])
      {
        g.addNode(dest, { label: dest });
        if (this.currentDirection == 'source') {
          this.nodes[dest] = [];
        }
      }

      if (this.currentDirection == 'dest') this.nodes[source].push(transitions[i]);
      if (this.currentDirection == 'source') this.nodes[dest].push(transitions[i]);

      g.addEdge(source, dest, { directed: true, fill : "#333"});
    }

    layouter.layout();
    renderer.draw();

    this.relink();

    if (this.currentDirection == 'source')
      g.nodes[this.currentState]['shape'][0].attr({ fill: this.sourceColor[0], stroke: this.sourceColor[1] });
    else
      g.nodes[this.currentState]['shape'][0].attr({ fill: this.destColor[0], stroke: this.destColor[1] });
  },

  relink: function() {
    for (var node in this.nodes)
    {
      g.nodes[node]['shape'][0].attr({ fill: '#ccc', stroke: '#888' })

      g.nodes[node]['shape'][0].id = node;
      g.nodes[node]['shape'][0].mousedown(function() { transition_graph.clickNode(this) });
    }
  },

  clickNode: function(nodeShape) {
    console.log('click');

    for (var node in transition_graph.nodes) {
      if (g.nodes[node].label !== transition_graph.currentState)
        g.nodes[node]['shape'][0].attr({ fill: '#ccc', stroke: '#888' });
    }

    if (nodeShape.id !== transition_graph.currentState)
      nodeShape.attr({ fill: 'red', stroke: 'darkred' });

    var title = '';
    if (document.getElementById('direction').value == 'source')
    {
      title = transition_graph.currentState + '&nbsp>>>&nbsp' + nodeShape.id;
    }
    else
      title = nodeShape.id + '&nbsp>>>&nbsp' + transition_graph.currentState;

    document.getElementById('transition').innerHTML = title;

    document.getElementById('actions_list').innerHTML = '';
    document.getElementById('transition_list').innerHTML = '';

    for (var j = 0; j < transition_graph.nodes[nodeShape.id].length; j++)
    {
      document.getElementById('transition_list').innerHTML += ('<li onclick="transition_graph.getActions(\''+nodeShape.id+'\','+j+')"><u>' + transition_graph.nodes[nodeShape.id][j]['label'] + '</u></li>');
    }
  },

  getActions: function(id, n) {
    var html = '<h3>' + this.nodes[id][n]['label'] + '</h3>';
    for (var i = 0; i < this.nodes[id][n]['actions'].length; i++)
    {
      html += '<div>[' + (i + 1) + ']<br />';
      for (var key in this.nodes[id][n]['actions'][i])
      {
        html += key + ': ';
        if (typeof(this.nodes[id][n]['actions'][i][key]) == 'object')
        {
          html += '<ul>';
          for (var j in this.nodes[id][n]['actions'][i][key])
          {
            html += '<li>' + j + ' : ' + this.nodes[id][n]['actions'][i][key][j] + '</li>';
          }
          html += '</ul>';
        }
        else
          html += this.nodes[id][n]['actions'][i][key];

        html += '<br />';
      }
      html += '</div>'

      document.getElementById('actions_list').innerHTML = html;
    }
  }
}

window.onload = function() {
  transition_graph.buildStateOptions();
}
</script>

<div id="canvas"></div>

<div id="inspector">
  <h3 id="transition"></h3>
  <h4>
    <span id="source_state"></span>
    <span id="dest_state"></span>
  </h4>

  <ul id="transition_list">
  </ul>

  <ul id="actions_list">
  </ul>
</div>

</body>
</html>

<html>
<head>
<style>
label { float: left; width: 196px; }
input[type="text"] { width: 256px; }
a:visited { color: blue; }
</style>

<script type="text/javascript" src="../js/show_environment_stage.js"></script>
<script type="text/javascript" src="../js/jquery-2.0.3.js"></script>

<script type="text/javascript">
$(function() {

});

function runCommand()
{
  var c = confirm("Are you sure you wish to execute?");
  if (!c) return;

  var inputParameters = $("#api :input[value!='']").serialize();

  var url = '/ultra_api.php?version=2&bath=rest&format=json&partner=customercare';
  url = url + '&command=customercare__PromoEnroll';

  $.ajax({
    url: url,
    type: 'POST',
    data: inputParameters,
    dataType: 'json',
    success: function(res) {
      console.log(res);
      $('#console').html(JSON.stringify(res, null, 2));
    },
    error: function(res) {
      console.log(res);
      $('#console').html(JSON.stringify(res, null, 2));
    }
  });
}
</script>
</head>
<body>

<h3>ENROLL CUSTOMER IN PROMOTIONAL PLAN</h3>

<div id="console"></div>

<div id="api">
<p>
<label>Customer ID:</label>
<input type="text" name="customer_id" />
</p>

<p>
<label>Plan:</label>
<select name="plan">
  <option value="L19">L19</option>
</select>
</p>

<p>
<label>PROMO Reference ID:</label>
<select name="reference_id">
  <option value="3FOR30">3 FOR 30</option>
</select>
</p>

<p>
<label>Months:</label>
<select name="months">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
<!--
  <?php for ($i = 1; $i < 13; $i++): ?>
  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
  <?php endfor; ?>
-->
</select>
</p>

</div>

<p><button onclick="runCommand();">Execute</button></p>

</body>
</html>
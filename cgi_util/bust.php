<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>

<script type="text/javascript" src="/js/show_environment_stage.js"></script>
<script language="javascript">

// globals
var startTime; // start of execution time
var inputValues; // input values array, this tools supports MSISDNs only
var nextInput; // next index input to process
var timerID; // clock timer
var apiName; // API name we are working with
var runControl = true; // app is running


/**
 *  Execute
 *  main execution function: called when user clicks Query button
 */
function Execute()
{
  // initalize
  startTime = new Date();

  // parse input values into global array
  inputValues = ParseInput(document.getElementById('inputValues').value);
  if (! inputValues.length)
    return alert('ERROR: no input values entered.');
  if (inputValues.length > 10000)
    return alert('ERROR: cannot process more than 10,000 values, ' + inputValues.length + ' given.');
  
  // verify input are MSISDNs
  for (var i = 0; i < inputValues.length; i++)
    if (inputValues[i].length != 10)
      return alert('Invalid input ' + inputValues[i]);

  document.getElementById('runTotal').innerHTML = inputValues.length;

  // determine API type
  var selector = document.getElementById('inputTypeSelect');
  apiName = selector.options[selector.selectedIndex].value;
  apiData = selector.options[selector.selectedIndex].getAttribute('data-plan');



  document.getElementById('runQuery').innerHTML = selector.options[selector.selectedIndex].text;

  // determine queue size (aka speed)
  var speed = document.getElementById('inputSpeedSelect');
  var size = speed.options[speed.selectedIndex].value;
  if (size == 3)
  {
    var resp = confirm("WARNING: substantial processing impact!\nProceed?");
    if (resp !== true)
      return;
  }
  else if (size == 10)
  {
    var resp = confirm("WARNING: high processing impact!\nHave you received an explicit permission from Rizwan?");
    if (resp !== true)
      return;
  }

  // show general warning
  var resp = confirm("Have you received permission from Rizwan to modify these subscribers?");
  if (resp !== true)
    return;

  // hide input, show output
  var inputIds = ['inputArea','inputType','inputSpeed', 'inputControl'];
  var outputIds = ['runStats', 'tableHeader'];
  SwitchInterface(inputIds, outputIds);

  // create one row for each entry
  CreateRows(inputValues, 'dataTable');

  // start counter and commence execution
  timerID = setInterval(function() { RunTime(startTime, 'runTime') }, 1000);
  nextInput = 0;
  for (var i = 0; i < size; i++)
    if (inputValues[i])
      setTimeout(function() { QueueUp() }, 1000 * i); // spread out AJAX calls by 1 sec
}


/**
 *  create data rows: one for API results and another for XML
 */
function CreateRows(data, tableID)
{
  var table = document.getElementById(tableID);
  for (var i = 0; i < data.length; i++)
  {
    // API results row
    var row = table.insertRow(-1);
    row.className = 'data';

    var sub = row.insertCell(0);
    sub.setAttribute('id', 'sub' + i);
    sub.className = 'center';

    var iccid = row.insertCell(1);
    iccid.setAttribute('id', 'iccid' + i);

    var msisdn = row.insertCell(2);
    msisdn.setAttribute('id', 'msisdn' + i);
    msisdn.className = 'center';

    if (data[i].length < 10)
      sub.innerHTML = data[i];
    else if (data[i].length < 18)
      msisdn.innerHTML = data[i];
    else
      iccid.innerHTML = data[i];

    var info = row.insertCell(3);
    info.setAttribute('id', 'info' + i);
    info.innerHTML = 'waiting';
  }
}


/**
 *  GenerateExport
 *  prepare data for CVS export and initialize UI export element
 *  @see: http://adilapapaya.wordpress.com/2013/11/15/exporting-data-from-a-web-browser-to-a-csv-file-using-javascript/
 */
function GenerateExport()
{
  // create CVS data
  var rows = ['subscriber,ICCID,MSISDN,info'];
  for (var i = 0, z = inputValues.length; i < z; i++)
  {
    var sub = document.getElementById('sub' + i).innerHTML;
    var iccid = document.getElementById('iccid' + i).innerHTML;
    var msisdn = document.getElementById('msisdn' + i).innerHTML;
    var info = document.getElementById('info' + i).innerHTML;

    rows.push(sub + ',' + iccid + ',' + msisdn + ',' + info);
  }
  var csvData = rows.join("%0A");

  // create A tag
  var tag = document.createElement('a');
  tag.href = 'data:attachment/csv,' + csvData;
  tag.target = '_blank';
  tag.download = 'BARK.csv';
  tag.innerHTML = 'Export to CVS';

  // init UI element
  document.getElementById('cvsExport').appendChild(tag);
}


/**
 *  parse and return user input
 */
function ParseInput(input)
{
  // split input into individual strings delimited by almost anything
  var dirty = input.split(/,| |;|\n|\r/); // '|' is OR

  // remove empty values resulting in sequencial delimiters
  for (var clean = [], i = 0; i < dirty.length; i++)
    if (dirty[i])
      clean.push(dirty[i]);
    
  return clean;
}


/**
 *  display our current running time in runTime element
 */
function RunTime(start, id)
{
    var now = new Date();
    var seconds = Math.round((now - start) / 1000);
    var hours = ('0' + Math.floor(seconds / 3600)).substr(-2);
    seconds %= 3600;
    var minutes = ('0' + Math.floor(seconds / 60)).substr(-2);
    seconds = ('0' + seconds % 60).substr(-2);
    document.getElementById(id).innerHTML = hours + ':' + minutes + ':' + seconds;
}


/**
 * callback functions to combine each corresponding API output into a summary string
 * these only execute on API response without errors
 */
function internal__ChangeWholesaleService(response)
{
  var summary = 'result: ' + response.success;
  return summary;
}

function internal__NetworkSuspend(response)
{
  var summary = 'success: ' + response.success;
  return summary;
}

function internal__NetworkRestore(response)
{
  var summary = 'success: ' + response.success;
  return summary;
}

function internal__UpdatePlanAndFeatures(response)
{
  var summary = 'success: ' + response.success;
  return summary;
}

function internal__MvneRenewPlan(response)
{
  var summary = 'success: ' + response.success;
  return summary;
}

/**
 *  load input value into AJAX queue
 */
function QueueUp()
{
  // get pending index and data
  var index = nextInput++;
  if (index >= inputValues.length || runControl !== true) // end of array or execution: stop recursion
  {
    clearInterval(timerID);
    return;
  }
  else
    var data = inputValues[index];

  // change element info
  var info = document.getElementById('info' + index);
  info.innerHTML = 'processing';

  // prepare POST
  var url = '/ultra_api.php?bath=rest&partner=internal&version=2&command=' + apiName;
  var param;
  if (apiName == 'internal__ChangeWholesaleService')
    param = '&MSISDN=' + data + '&price_plan=' + apiData;
  else if (apiName == 'internal__NetworkSuspend')
    param = '&MSISDN=' + data;
  else if (apiName == 'internal__NetworkRestore')
    param = '&MSISDN=' + data;
  else if (apiName == 'internal__UpdatePlanAndFeatures')
    param = '&msisdn=' + data;
  else if (apiName == 'internal__MvneRenewPlan')
    param = '&msisdn=' + data;

  // send AJAX request
  ajax = new XMLHttpRequest();
  ajax.onreadystatechange = MakeCallback(index);
  ajax.open('GET', url + param, true);
  ajax.setRequestHeader('Content-Type', 'application/json;charset=UTF-8'); 
  ajax.send();
}

function MakeCallback(i)
{
  return function()
  {
    if (this.readyState == 4 && this.status == 200)
      QueueDown(i, this.responseText);
  }
}


/**
 * pause/resume execution
 */
function PauseResume()
{
  if (runControl)
    runControl = false;
  else
    runControl = true;
  return false;
}


/**
 * process AJAX response
 */
function QueueDown(index, response)
{
  var sub = document.getElementById('sub' + index);
  var iccid = document.getElementById('iccid' + index);
  var msisdn = document.getElementById('msisdn' + index);
  var info = document.getElementById('info' + index);
  var errors = document.getElementById('runError');

  // parse response
  try
  {
    var data = JSON.parse(response);

    // update subscriber ID
    if ( ! sub.innerHTML && data.customer_id)
      sub.innerHTML = data.customer_id;

    // update ICCID
    if ( ! iccid.innerHTML && data.ICCID)
      iccid.innerHTML = data.ICCID;

    // update MSISDN
    if ( ! msisdn.innerHTML && data.MSISDN)
      msisdn.innerHTML = data.MSISDN;

    // update info
    if (data.errors.length)
    {
      info.innerHTML = data.errors[0];
      if (data.user_errors !== 'undefined' && data.user_errors.length)
        info.innerHTML = info.innerHTML + ' (' + data.user_errors[0] + ')';
      info.style.color = '#D00';
      errors.innerHTML++;
    }
    else if (data.warnings.length && data.warnings[0] == 'MVNE plan is already set')
    {
      info.innerHTML = "The given Wholesale Plan is the same as the subscriber's current Wholesale Plan.";
      info.style.color = '#D00';
    }
    else
    {
      info.innerHTML = window[apiName](data);
      info.style.color = '#0B0';
    }
  }
  catch (e)
  {
    info.innerHTML = typeof data === 'undefined' || typeof data.errors === 'undefined' ? 'ERROR: invalid API response' : data.errors;
    info.style.color = '#D00';
    errors.innerHTML++;
  }

  // check if this is the last element
  if ((inputValues.length - 1) == index)
    GenerateExport();

  // update statistics
  document.getElementById('runDone').innerHTML++;

  QueueUp();

}


/**
 * hide static input and show static output UI
 */
function SwitchInterface(input, output)
{
  for (var i = 0; i < input.length; i++ )
    document.getElementById(input[i]).style.display = 'none';

  for (i = 0; i < output.length; i++ )
    document.getElementById(output[i]).style.display = '';
}


/**
 * show or hide GUI element
 */
function ToggleElement(id)
{
  var element = document.getElementById(id);
  element.style.display = element.style.display == 'none' ? '' : 'none';
}

</script>


<style>
  table { border-collapse: collapse; font-family: Verdana, Arial, serif; font-size: 12px; }
  tr.header { text-align: center; background-color: #CCF; border: 1px solid gray; }
  tr.data:hover { cursor: pointer; background-color: #BFF; }
  td { border-bottom: 1px solid gray; }
  td.center { text-align: center; }
  td.xml { background-color: #EEE }
</style>


</head>

<body style="background-color:#FDD;">

<table id=dataTable width="100%">

  <tr id=inputArea>
    <td colspan=2>1. Enter MSISDNs:</td>
    <td colspan=2><textarea rows="20" cols="100" id="inputValues" style="background-color:#FEE;" title="seperate values with space, carriage return, comma or semicolumn"></textarea></td>
  </tr>

  <tr id=inputType>
    <td colspan=2>2. Select update type:</td>
    <td colspan=2>
      <select id=inputTypeSelect>
        <option value="internal__UpdatePlanAndFeatures">Update L34 & L44 SOCs</option>
        <option value="internal__MvneRenewPlan">Renew Plan on MVNE</option>
        <option value="internal__NetworkRestore">Restore Subscribers</option>
        <option value="internal__NetworkSuspend">Suspend Subscribers</option>
        <option value="internal__ChangeWholesaleService" data-plan="1" selected>Switch to Wholesale Plan 1</option>
        <option value="internal__ChangeWholesaleService" data-plan="2">Switch to Wholesale Plan 2</option>
        <option value="internal__ChangeWholesaleService" data-plan="3">Switch to Wholesale Plan 3 ( W-UV-PRIMARY )</option>
        <option value="internal__ChangeWholesaleService" data-plan="6">Switch to Wholesale Plan 6 ( W-MRC )</option>
      </select>
    </td>
  </tr>

  <tr id=inputSpeed>
    <td colspan=2>3. Select execution speed:</td>
    <td colspan=2>
      <select id=inputSpeedSelect>
        <option value=1 title="1 query at a time" selected>Slowest</option>
        <option value=3 title="3 queries at a time">Low</option>
        <option value=6 title="6 queries at a time">Medium</option>
        <option value=10 title="10 queries at a time">High</option>
        <option value=12 title="12 queries at a time">Highest</option>
      </select>
    </td>
  </tr>

  <tr id=inputControl>
    <td colspan=2>4. Begin update:</td>
    <td colspan=2><button type="button" onclick="Execute()">Execute</button></td>
  </tr>

  <tr id=runStats style="display:none">
    <td colspan=4 style='background-color: #EEE;'>
      update type: <span id=runQuery></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      execution time: <span id=runTime></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      record count: <span id=runTotal></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      finished records: <span id=runDone>0</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      errors: <span id=runError>0</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      execution control: <a href='#' id=runControl onclick='PauseResume()'>Stop</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <span id=cvsExport></span>
    </td>
  </tr>

  <tr id=tableHeader class=header style='display: none'>
    <th width='180px'>SUBSCRIBER</td>
    <th width='180px'>ICCID</td>
    <th width='180px'>MSISDN</td>
    <th>INFO</td>
  </tr>  

</table>
</body>

</html>



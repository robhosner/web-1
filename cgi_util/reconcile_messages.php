<?php

/*
 */

?>
<html>
<head>

<title>RECONCILE MESSAGES</title>
<script type="text/javascript" src="/js/show_environment_stage.js"></script>
<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
$(function() {

});

var readyText   = 'EXECUTE';
var workingText = 'WORKING';

var readyColor   = 'red';
var workingColor = '#888';

var baseURL = '/ultra_api.php?version=2&bath=rest&format=json&partner=internal';

function internal__ReconcileMessages()
{
  var c = confirm("Are you sure you wish to execute reconcile messages?");
  if (!c) return;

  $('#redButton').css('background-color', workingColor);
  $('#redButton').text(workingText);

  $.ajax({
    method: 'GET',
    url: baseURL + '&command=internal__ReconcileMessages',
    success: function(data, status, settings) {
      var response = JSON.parse(data);

      if ((response.errors && response.errors.length > 0) || (response.success !== true))
      {
        alert(response.errors[0]);
      }
      else
      {
        alert('Success');
      }

      $('#redButton').css('background-color', readyColor);
      $('#redButton').text(readyText);
    }
  });
}
</script>

<style>
#container {
  font-family: sans-serif;
  width : 768px;
  margin: 0px auto;
  text-align: center;
}

#redButton {
  width: 256px;
  height: 256px;
  background-color: red;
  border: 10px solid #555;
  border-radius: 128px;
  color: white;
  cursor: pointer;
  font-size: 2.5em;
}
</style>

</head>
<body>
<div id="container">

<h1>Reconcile Messages</h1>
<button id="redButton" onclick="internal__ReconcileMessages();">EXECUTE</button>

</div>
</body>
</html>
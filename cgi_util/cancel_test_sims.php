<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <style>
      div {
        margin: 10px 0px;
      }
      .success {
        background: #5cb85c;
        color: #ffffff;
        padding: 0 10px;
      }
      .failed {
        background: #d9534f;
        color: #ffffff;
        padding: 0 10px;
      }
      p {
        padding: 10px 0;
      }
      #sims {
        padding: 0 10px;
      }
      #bulk-cancel-sim-container {
        display: none;
      }

      .spinner {
        display: inline-block;
        opacity: 0;
        max-width: 0;

        -webkit-transition: opacity 0.25s, max-width 0.45s;
        -moz-transition: opacity 0.25s, max-width 0.45s;
        -o-transition: opacity 0.25s, max-width 0.45s;
        transition: opacity 0.25s, max-width 0.45s; /* Duration fixed since we animate additional hidden width */
      }

      .has-spinner.active {
        cursor:progress;
      }

      .has-spinner.active .spinner {
        opacity: 1;
        margin-right: 5px;
        max-width: 50px; /* More than it will ever come, notice that this affects on animation duration */
      }
    </style>
    <script>
      $(document).ready(function() {
        $('#cancel-sim').on('click', function() {
          var simList;
          var exclamationSign = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>';
          var checkSign = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
          var reason;
          
          if ($(this).text() == 'Cancel Sim') {
            if ($('#iccid-or-msisdn').val() == '') {
              $('#results').removeClass('success').addClass('failed');
              $('#results').html("<p>" + exclamationSign + " Missing ICCID or MSISDN</p>");
              return false;
            }

            if ($('#custom-reason').val() != '') {
              reason = $('#custom-reason').val();
            } else {
              reason = $('#cancellation-reason option:selected').text();
            }

            simList = $('#iccid-or-msisdn').val() + ":" + reason;
          } else {
            var list = $('#bulk-sims').val().trim();

            if (list == '') {
              $('#results').removeClass('success').addClass('failed');
              $('#results').html("<p>" + exclamationSign + " Missing ICCID or MSISDN list</p>");
              return false;
            }

            simList = list.split("\n");
          }

          $(this).addClass('active');

          $.ajax({
            url: "/ultra_api.php?format=json&partner=internal&command=internal__CancelCustomersWithinTestRange&bath=rest&version=2&customers=" + simList,
            type: 'POST',
            success: function(result) {
              result = JSON.parse(result);

              $('#cancel-sim').removeClass('active');

              if (result.success) {
                $('#results').html('');
                $('#sims').html('');
                $('#results').removeClass('failed').removeClass('success');

                if (result.cancelled_test_sims.length) {
                  $('#results').html("<p class='success' style='padding: 10px;'>" + checkSign + " Successfully cancelled the following test sims:</p><div style='margin-left: 18px;'>" + result.cancelled_test_sims.join('<br>') + "</div>");
                }

                if (result.failed_cancelled_test_sims.length) {
                  $('#results').append("<p class='failed' style='padding: 10px;'>" + exclamationSign + " Failed to cancel the following test sims:</p><div style='margin-left: 18px;'>" + result.failed_cancelled_test_sims.join('<br>') + "</div>");
                }
              }

              if (!result.success) {
                $('#results').html('');
                $('#sims').html('');
                $('#results').removeClass('success').addClass('failed');
                $('#results').html("<p>" + exclamationSign + " " + result.user_errors + "</p>");
              }

              if (result.invalid_msisdn_or_iccids) {
                $('#results').html('');
                $('#sims').html('');
                $('#results').removeClass('success').addClass('failed');
                $('#results').html("<p>" + exclamationSign + " " + result.user_errors + "</p>");
                $('#sims').html("<div>" + result.invalid_msisdn_or_iccids.join('</br>') + "</div>");
              }

              if (result.not_test_sims) {
                $('#results').html('');
                $('#sims').html('');
                $('#results').removeClass('success').addClass('failed');
                $('#results').html("<p>" + exclamationSign + " " + result.user_errors + "</p>");
                $('#sims').html("<div>" + result.not_test_sims.join('</br>') + "</div>");
              }

              if (result.invalid_cancellation_reasons) {
                $('#results').html('');
                $('#sims').html('');
                $('#results').removeClass('success').addClass('failed');
                $('#results').html("<p>" + exclamationSign + " Invalid Cancellation Reason</p>");
                $('#sims').html("<div>" + result.invalid_cancellation_reasons.join('</br>') + "</div>");
              }
            }
          });
        });

        $('#bulk-cancel').on('click', function() {
          $('#cancel-sim-container').toggle();
          $('#bulk-cancel-sim-container').toggle();
          $('#results').html('');

          $('#cancel-sim').text() == 'Cancel Sim' ? $('#cancel-sim').html('<span class="spinner"><i class="fa fa-refresh fa-spin"></i></span>Cancel Sims') : $('#cancel-sim').html('<span class="spinner"><i class="fa fa-refresh fa-spin"></i></span>Cancel Sim');
          $(this).text() == 'Bulk Sim Cancellation' ? $(this).text('Back') : $(this).text('Bulk Sim Cancellation');
        });

        $('#custom-reason').on('input', function() {
          if ($(this).val() != '') {
            $('#cancellation-reason').attr('disabled', 'disabled');
          } else {
            $('#cancellation-reason').removeAttr('disabled');
          }
        });
      });
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <h1>Cancel Test Sims</h1>
      <div id="cancel-sim-container">
        <input type="text" class="form-control" id="iccid-or-msisdn" placeholder="ICCID or MSISDN">
        <label for="cancellation-reason">Reason for cancellation?</label>
        <select class="form-control" id="cancellation-reason" name="cancellation-reason">
          <option value="Activation">Activation</option>
          <option value="Port-In">Port-In</option>
          <option value="Intra-port">Intra-port</option>
          <option value="SIM Swap">SIM Swap</option>
          <option value="Bolt-On Purchase">Bolt-On Purchase</option>
          <option value="Plan Change">Plan Change</option>
          <option value="Monthly Renewal">Monthly Renewal</option>
          <option value="International Calling">International Calling</option>
          <option value="Data Use">Data Use</option>
          <option value="MSISDN Change">MSISDN Change</option>
          <option value="Port-Out">Port-Out</option>
        </select>
        <input style="margin-top:10px" type="text" class="form-control" id="custom-reason" placeholder="Custom Reason">
      </div>
      <div id="bulk-cancel-sim-container" class="form-group">
        <label for="bulk-sims">Format - MSISDN_OR_ICCID:CANCELLATION_REASON e.g., 111111111111111111:Activation, followed by a new line</label>
        <textarea name="bulk-sims" id="bulk-sims" class="form-control" style="height: 200px;" cols="70" rows="50"></textarea>
      </div>
      <div>
        <button class="btn btn-primary has-spinner" href="#" id="cancel-sim" role="button"><span class="spinner"><i class="fa fa-refresh fa-spin"></i></span>Cancel Sim</button>
        <button class="btn btn-info has-spinner" href="#" id="bulk-cancel" role="button"><span class="spinner"><i class="fa fa-refresh fa-spin"></i></span>Bulk Sim Cancellation</button>
      </div>
      <div id="results">
      </div>
      <div id="sims"></div>
    </div> 
  </body>
</html>
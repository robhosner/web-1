<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>

<script type="text/javascript" src="/js/show_environment_stage.js"></script>
<script language="javascript">

// globals
var startTime; // start of execution time
var inputValues; // input values array
var nextInput; // next index input to process
var timerID; // clock timer
var apiName; // API name we are working with
var runControl = true; // app is running


/**
 *  Execute
 *  main execution function: called when user clicks Query button
 */
function Execute()
{
  // initalize
  startTime = new Date();

  // parse input values into global array
  inputValues = ParseInput(document.getElementById('inputValues').value);
  if (! inputValues.length)
    return alert('ERROR: no input values entered.');
  if (inputValues.length > 3000)
    return alert('ERROR: cannot process more than 3,000 values, ' + inputValues.length + ' given.');
  document.getElementById('runTotal').innerHTML = inputValues.length;

  // determine API type
  var selector = document.getElementById('inputTypeSelect');
  apiName = selector.options[selector.selectedIndex].value;
  document.getElementById('runQuery').innerHTML = selector.options[selector.selectedIndex].text;

  // determine queue size (aka speed)
  var speed = document.getElementById('inputSpeedSelect');
  var size = speed.options[speed.selectedIndex].value;
  if (size == 3)
  {
    var resp = confirm("WARNING: any substantial fixes must be implemented prompty!\nProceed?");
    if (resp !== true)
      return;
    document.body.style.backgroundColor = 'orange';
  }
  else if (size == 10)
  {
    var resp = confirm("WARNING: high processing speed will impact production!\nHave you received an explicit permision from Rizwan?");
    if (resp !== true)
      return;
    document.body.style.backgroundColor = 'red';
  }

  // hide input, show output
  var inputIds = ['inputArea','inputType','inputSpeed', 'inputControl'];
  var outputIds = ['runStats', 'tableHeader'];
  SwitchInterface(inputIds, outputIds);

  // create one row for each entry
  CreateRows(inputValues, 'dataTable');

  // start counter and commence execution
  timerID = setInterval(function() { RunTime(startTime, 'runTime') }, 1000);
  nextInput = 0;
  for (var i = 0; i < size; i++)
    if (inputValues[i])
      setTimeout(function() { QueueUp() }, 1000 * i); // spread out AJAX calls by 1 sec
}


/**
 *  create data rows: one for API results and another for XML
 */
function CreateRows(data, tableID)
{
  var table = document.getElementById(tableID);
  for (var i = 0; i < data.length; i++)
  {
    // API results row
    var row = table.insertRow(-1);
    row.onclick = MakeOnClick(i);

    row.className = 'data';
    row.title = 'click to view XML';

    var iccid = row.insertCell(0);
    iccid.setAttribute('id', 'iccid' + i);

    var msisdn = row.insertCell(1);
    msisdn.setAttribute('id', 'msisdn' + i);
    msisdn.className = 'center';

    if (data[i].length >= 11)
      iccid.innerHTML = data[i];
    else
      msisdn.innerHTML = data[i];

    var sub = row.insertCell(2);
    sub.setAttribute('id', 'sub' + i);
    sub.className = 'center';

    var info = row.insertCell(3);
    info.setAttribute('id', 'info' + i);
    info.innerHTML = 'waiting';

    // XML row
    var xml = table.insertRow(-1);
    var cell = xml.insertCell(0);
    cell.setAttribute('colspan', '4');
    cell.setAttribute('id', 'xml' + i);
    cell.style.display = 'none';
    cell.innerHTML = '&nbsp;';
    cell.className = 'xml';
  }
}


/**
 *  GenerateExport
 *  prepare data for CVS export and initialize UI export element
 *  @see: http://adilapapaya.wordpress.com/2013/11/15/exporting-data-from-a-web-browser-to-a-csv-file-using-javascript/
 */
function GenerateExport()
{
  // create CVS data
  var rows = ['subscriber,ICCID,MSISDN,info'];
  for (var i = 0, z = inputValues.length; i < z; i++)
  {
    var sub = document.getElementById('sub' + i).innerHTML;
    var iccid = document.getElementById('iccid' + i).innerHTML;
    var msisdn = document.getElementById('msisdn' + i).innerHTML;
    var info = document.getElementById('info' + i).innerHTML;

    rows.push(sub + ',' + iccid + ',' + msisdn + ',' + info);
  }
  var csvData = rows.join("%0A");

  // create A tag
  var tag = document.createElement('a');
  tag.href = 'data:attachment/csv,' + csvData;
  tag.target = '_blank';
  tag.download = 'BARK.csv';
  tag.innerHTML = 'Export to CVS';

  // init UI element
  document.getElementById('cvsExport').appendChild(tag);
}


/**
 *  cleaner JS closer function
 */
function MakeOnClick(index)
{ return function() { ToggleElement('xml' + index); }; }


/**
 *  parse and return user input
 */
function ParseInput(input)
{
  // split input into individual strings delimited by almost anything
  var dirty = input.split(/,| |;|\n|\r/); // '|' is OR

  // remove empty values resulting in sequencial delimiters
  for (var clean = [], i = 0; i < dirty.length; i++)
    if (dirty[i])
      clean.push(dirty[i]);

  return clean;
}


/**
 *  display our current running time in runTime element
 */
function RunTime(start, id)
{
    var now = new Date();
    var seconds = Math.round((now - start) / 1000);
    var hours = ('0' + Math.floor(seconds / 3600)).substr(-2);
    seconds %= 3600;
    var minutes = ('0' + Math.floor(seconds / 60)).substr(-2);
    seconds = ('0' + seconds % 60).substr(-2);
    document.getElementById(id).innerHTML = hours + ':' + minutes + ':' + seconds;
}


/**
 *  internal__X
 *  callback functions to combine each corresponding API output into a summary string
 */
function internal__CheckBalance(data)
{
  var summary = '';
  for (var i = 0; i < data.BalanceValueList.BalanceValue.length; i++)
  {
    var element = data.BalanceValueList.BalanceValue[i];
    summary += (element.UltraName ? element.UltraName : 'Type ' + element.Type) +
      ': ' + element.Value +
      (element.Unit == 'Unit' ? '; ' : element.Unit + '; ');
  }
  return summary;
}

function internal__QuerySubscriber(data)
{
  // extract some but not all values from API response
  var summary = '';
  var names = ['IMSI', 'IMEI', 'WholesalePlan', 'PlanEffectiveDate', 'PlanId', 'SubscriberStatus'];
  for (var i = 0; i < names.length; i++)
    if (typeof data[names[i]] != 'undefined')
      summary += names[i] + ': ' + data[names[i]] + '; ';
  return summary;
}

function internal__GetNetworkDetails(data)
{
  // extract some but not all values from API response
  var summary = '';
  var names = ['MSStatus', 'NAPStatus', 'SIMStatus', 'INStatus', 'IMEI', 'IMSI'];
  for (var i = 0; i < names.length; i++)
    summary += names[i] + ': ' + data[names[i]] + '; ';
  return summary;
}

function internal__QueryStatus(data)
{
  // extract some but not all values from API response
  var summary = '';
  var names = ['MSISDN', 'portRequestedTime', 'portCompletedTime', 'serviceTransactionId', 'portDueTime', 'portSubmittedTime', 'portStatus','portStatusReason'];
  for (var i = 0; i < names.length; i++)
    summary += names[i] + ': ' + data[names[i]] + '; ';
  return summary;
}


/**
 *  load input value into AJAX queue
 */
function QueueUp()
{
  // get pending index and data
  var index = nextInput++;
  if (index >= inputValues.length || runControl !== true) // end of array or execution: stop recursion
  {
    clearInterval(timerID);
    return;
  }
  else
    var data = inputValues[index];

  // change element info
  var info = document.getElementById('info' + index);
  info.innerHTML = 'processing';

  // prepare POST
  var url = '/ultra_api.php?bath=rest&partner=internal&version=2&command=' + apiName;
  // TODO: why cannot I POST JSON?
  if (data.length <= 10)
    request = '&MSISDN=' + data;
  else
    request = '&ICCID=' + data;
  // request = JSON.stringify(request);

  // send AJAX request
  ajax = new XMLHttpRequest();
  ajax.onreadystatechange = MakeCallback(index);
  ajax.open('GET', url + request, true);
  ajax.setRequestHeader('Content-Type', 'application/json;charset=UTF-8'); 
  ajax.send();
}

function MakeCallback(i)
{
  return function()
  {
    if (this.readyState == 4 && this.status == 200)
      QueueDown(i, this.responseText);
  }
}


/**
 * pause/resume execution
 */
function PauseResume()
{
  if (runControl)
    runControl = false;
  else
    runControl = true;
  return false;
}


/**
 * process AJAX response
 */
function QueueDown(index, response)
{
  var info = document.getElementById('info' + index);
  var errors = document.getElementById('runError');

  // parse response
  try
  {
    var data = JSON.parse(response);

    // check for errors
    if (data.errors.length)
      throw new Exception(data.errors);

    // call function that handles API output
    info.innerHTML = window[apiName](data);
    info.style.color = '#0B0';

    // update ICCID
    iccid = document.getElementById('iccid' + index);
    if (! iccid.innerHTML && data.ICCID)
      iccid.innerHTML = data.ICCID;

    // update MSISDN
    msisdn = document.getElementById('msisdn' + index);  
    if (! msisdn.innerHTML && data.MSISDN)
      msisdn.innerHTML = data.MSISDN;

    // update subscriber ID
    if (data.customer_id)
    {
      sub = document.getElementById('sub' + index);
      sub.innerHTML = data.customer_id;
    }

    // update XML
    xml = document.getElementById('xml' + index);
    if (data.XML)
    {
      var text = document.createTextNode(data.XML);
      xml.appendChild(text);
    }

    // check if this is the last element
    if ((inputValues.length - 1) == index)
      GenerateExport();
  }
  catch (e)
  {
    info.innerHTML = typeof data === 'undefined' || typeof data.errors === 'undefined' ? 'ERROR: invalid API response' : data.errors;
    info.style.color = '#D00';
    errors.innerHTML++;
  }

  // update statistics
  document.getElementById('runDone').innerHTML++;

  QueueUp();

}


/**
 * hide static input and show static output UI
 */
function SwitchInterface(input, output)
{
  for (var i = 0; i < input.length; i++ )
    document.getElementById(input[i]).style.display = 'none';

  for (i = 0; i < output.length; i++ )
    document.getElementById(output[i]).style.display = '';
}


/**
 * show or hide all XML rows
 */
function ToggleXML()
{
  for (var i = 0; i < inputValues.length; i++)
    ToggleElement('xml' + i);
  return false;
}


/**
 * show or hide GUI element
 */
function ToggleElement(id)
{
  var element = document.getElementById(id);
  element.style.display = element.style.display == 'none' ? '' : 'none';
}

</script>


<style>
  table { border-collapse: collapse; font-family: Verdana, Arial, serif; font-size: 12px; }
  tr.header { text-align: center; background-color: #CCF; border: 1px solid gray; }
  tr.data:hover { cursor: pointer; background-color: #BFF; }
  td { border-bottom: 1px solid gray; }
  td.center { text-align: center; }
  td.xml { background-color: #EEE }
</style>


</head>

<body>

<table id=dataTable width="100%">

  <tr id=inputArea>
    <td colspan=2>1. Enter ICCIDs or MSISDNs:</td>
    <td colspan=2><textarea rows="20" cols="100" id="inputValues" title="seperate values with space, carriage return, comma or semicolumn"></textarea></td>
  </tr>

  <tr id=inputType>
    <td colspan=2>2. Select query type:</td>
    <td colspan=2>
      <select id=inputTypeSelect>
        <option value="internal__CheckBalance" selected>Check Balance</option>
        <option value="internal__QuerySubscriber">Subscriber Inquiry</option>
        <option value="internal__GetNetworkDetails">Network Details</option>
        <option value="internal__QueryStatus">Port Status Inquiry</option>
      </select>
    </td>
  </tr>

  <tr id=inputSpeed>
    <td colspan=2>3. Select execution speed:</td>
    <td colspan=2>
      <select id=inputSpeedSelect>
          <option value=1 title="1 query at a time" selected>Slowest</option>
          <option value=3 title="3 queries at a time">Low</option>
          <option value=6 title="6 queries at a time">Medium</option>
          <option value=10 title="10 queries at a time">High</option>
          <option value=12 title="12 queries at a time">Highest</option>
        </select>
    </td>
  </tr>

  <tr id=inputControl>
    <td colspan=2>4. Click to execute:</td>
    <td colspan=2><button type="button" onclick="Execute()">Execute</button></td>
  </tr>

  <tr id=runStats style="display:none">
    <td colspan=4 style='background-color: #EEE;'>
      query type: <span id=runQuery></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      execution time: <span id=runTime></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      record count: <span id=runTotal></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      finished records: <span id=runDone>0</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      errors: <span id=runError>0</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      execution control: <a href='#' id=runControl onclick='PauseResume()'>Stop</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      XML display: <a href='#' id=xmlControl onclick='ToggleXML()'>Toggle</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <span id=cvsExport></span>
    </td>
  </tr>

  <tr id=tableHeader class=header style='display: none'>
    <th width='180px'>ICCID</td>
    <th width='180px'>MSISDN</td>
    <th width='180px'>SUB ID</td>
    <th>INFO</td>
  </tr>  

</table>
</body>

</html>



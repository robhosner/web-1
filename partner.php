<?php

putenv("UNIT_TESTING=0");

$site = $_SERVER["SERVER_NAME"];
include_once("core.php");
include_once("session.php");
include_once("web.php");
include_once("lib/partner-validate.php");
include_once('lib/util-common.php');
include_once('lib/underscore/underscore.php');
include_once('classes/postageapp.inc.php');
include_once('Ultra/Lib/Billing/functions.php');
require_once 'Ultra/Lib/DB/DealerPortal.php';
include_once('Ultra/Lib/DB/Setter/Customer.php');
include_once('Ultra/Lib/MVNE/MakeItSo.php');
require_once('Ultra/Lib/Util/Redis.php');
require_once 'Ultra/Lib/Util/Redis/Billing.php';
require_once('Ultra/Lib/Util/Redis/Port.php');
include_once('db/htt_transition_log.php');

$start = microtime(TRUE);

$request_id = getNewUUID('UA','Partner PHPAPI');

$out = array(
             'errors' => array(),
             'success' => FALSE
);

$partner = NULL;
if (!array_key_exists("partner", $_REQUEST))
{
  $out['errors'][] = 'No partner specified';
}
else
{
  $partner = $_REQUEST['partner'];
}

$wsdl_file = "partner-meta/$partner.wsdl";

// default format is 'json'
$format      = (array_key_exists("format", $_REQUEST) && $_REQUEST['format'] !== '' ) ? $_REQUEST['format'] : 'json';
$wsdl        = $format === 'wsdl';
$html        = $format === 'html';
$wiki        = $format === 'wiki';
$php         = $format === 'php';
$qa          = $format === 'qa';
$json        = ($format === 'json' || $format === 'jsonp');
$xml         = $format === 'xml';

/* support JSONP */
if ($format === 'jsonp' && array_key_exists("callback", $_REQUEST))
{
  $jsonp = filter_var($_REQUEST['callback'], FILTER_SANITIZE_STRING);
}

// default bath is 'soap'
$bath = array_key_exists("bath", $_REQUEST) ? $_REQUEST['bath'] : 'soap';
$soap = $bath === 'soap';
$rest = $bath === 'rest';

$test        = array_key_exists("test",        $_REQUEST) ? $_REQUEST['test'] :        NULL;
$version     = array_key_exists("version",     $_REQUEST) ? $_REQUEST['version'] :     NULL;
$command     = array_key_exists("command",     $_REQUEST) ? $_REQUEST['command'] :     NULL;
$meta        = array_key_exists("meta",        $_REQUEST) ? $_REQUEST['meta'] :        NULL;
$mock        = array_key_exists("mock",        $_REQUEST) ? $_REQUEST['mock'] :        NULL;
$debug       = array_key_exists("debug",       $_REQUEST) ? $_REQUEST['debug'] :       NULL;
$always = array_key_exists("always", $_REQUEST) ? $_REQUEST['always'] : '';

$always_fail = $always === 'fail';
$always_succeed = $always === 'succeed';

$saved_command = $command;

if ($always_fail)
{
  $out['errors'][] = "ERR_API_INTERNAL: always fail";
}

if ($partner && ! in_array($partner, \Ultra\UltraConfig\getAllowedPartners()))
{
  $out['errors'][] = "Unknown partner $partner specified";
}

if (count($out['errors']) == 0)
{
  // directory which contains commands JSON definitions
  $dir = $e_config['git/deploy']."/partner-meta";

  // JSON definition file for $partner
  $f = "$dir/$partner.json";

  // read the JSON definition file
  $file_contents = file_get_contents($f);

  // decode JSON definition
  $p = json_decode_nice($file_contents, true);

  // validate JSON definition
  if (NULL == $p || !is_array($p) || !isset($p['commands']) || !is_array($p['commands']))
  {
    $out['errors'][] = "Could not load partner definition: " . debug_json_decode();
    $p = array('commands' => array());
  }

  // JSON actually contains commands definitions
  if (isset($p['commands']))
  {
    // we have to import all include files specified in $f
    foreach ($p['commands'] as $maybe_include => $def)
    {
      // it's an include file, not a command definition
      if (is_string($def))
      {
        // include file
        $if = "$dir/$maybe_include";

        // read the include file
        $include_file_content = file_get_contents($if);

        // decode JSON include file
        $ip = json_decode_nice($include_file_content, true);

        if (NULL == $ip || !is_array($ip) || !isset($ip['commands']) || !is_array($ip['commands']))
        {
          $out['errors'][] = "Could not load partner include definition from $if: " . debug_json_decode();
          $ip = array('commands' => array());
        }

        // loop through commands definitions
        foreach ($ip['commands'] as $icommand => $idef)
        {
          if ($def === 'include' || preg_match($def, $icommand))
          {
            if (isset($p['commands'][$icommand]))
            {
              $out['errors'][] = "Included command $icommand is a dupe";
            }
            else
            {
              $p['commands'][$icommand] = $idef;
            }
          }
        }
        unset($p['commands'][$maybe_include]);
      }
    }

    // loop through commands definitions
    foreach ($p['commands'] as $icommand => $def)
    {
      foreach (array("parameters", "returns") as $type)
      {
        if ($def === 'include')
          continue;

        $given = $def[$type];
        $back = array();
        foreach ($given as $pdef)
        {
          if (is_string($pdef))
          {
            $if = "$dir/$pdef";
            $ifc = file_get_contents($if);
            $ip = json_decode_nice($ifc, true);

            foreach ($ip as $parameter)
            {
              /* dlog('', "including $pdef parameter = %s", json_encode($parameter)); */
              /* this is much easier than keeping offsets */
              $back[] = $parameter;
            }
          }
          else
          {
            $back[] = $pdef;
          }
        }
        $p['commands'][$icommand][$type] = $back;
      }
    }
  }

  if (isset($p['meta']))
  {
    $out['partner_meta'] = $p['meta'];
  }
  else
  {
    $out['errors'][] = "Partner metadata was not specified in 'meta'";
  }

  if ($version && $version != 1)
  {
    $out['errors'][] = "Version prefix does not match";
  }

  if ( ! validateBrand($partner, $p))
    $out['errors'][] = 'ERR_API_INTERNAL: The API is not available for this brand';

  if (isset($p['commands']))
  {
    if (NULL == $command && (count($out['errors']) < 1))
    {
      if ($wsdl)
      {
        header("content-type: text/xml");
        echo make_wsdl($site, $partner, $e_config, $p);
        exit;
      }
      else if ($php)
      {
        header("content-type: text/plain");
        echo make_php($site, $partner, $e_config, $p);
        exit;
      }
      else if ($qa)
      {
        header("content-type: text/json");
        echo make_qa($p);
        exit;
      }
      else if ($html)
      {
        header("content-type: text/html");
        echo make_html($site, $partner, $e_config, $p);
        exit;
      }
      else if ($wiki)
      {
        header("content-type: text/plain");
        echo make_wiki($site, $partner, $e_config, $p);
        exit;
      }
      else
      {
        $out['commands'] = array_keys($p['commands']);
      }
    }
  }
  else
  {
    $out['errors'][] = "Commands were not specified in 'commands'";
  }
}

if (count($out['errors']) > 0)
{
  log_out();
}

if (!$meta && !$php && !$wsdl && count($out['errors']) == 0)
{
  if ($soap)
  {
    ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache

    $server = new SoapServer($wsdl_file);

    include_once("partner-face/$partner.php");

    foreach ($p['commands'] as $command => $def)
    {
      $server->addFunction($command);

      if (isset($HTTP_RAW_POST_DATA) &&
          preg_match("/tns:$command/", $HTTP_RAW_POST_DATA))
      {
        $saved_command = $command;
        $out['command'] = $command;
      }
    }

    dlog('', "handling SOAP call: site = $site");
    dlog('', "handling SOAP call: partner = $partner");
    dlog('', "handling SOAP call: request = %s", json_encode($_REQUEST));
    if ( isset($HTTP_RAW_POST_DATA) ) { dlog('', cleanse_credit_card_string($HTTP_RAW_POST_DATA)); }
    $xml = TRUE;

    dlog('', "invoking server handle");
    $server->handle();
    dlog('', "invoked server handle");
    exit;
  }
  else if ($rest)
  {
    if (isset($p['commands'][$command]))
    {
      $params = array();
      foreach ($p['commands'][$command]['parameters'] as $pdef)
      {
        $pname = $pdef['name'];

        if (isset($_REQUEST[$pname]))
        {
          $params[] = $_REQUEST[$pname];
        }
        else if ($pdef['required'] && !$always_succeed)
        {
          error_out("API_ERR_PARAMETER: Missing $command REST parameter $pname!");
        }
        else // parameter not required and not provided
        {
          if (isset($pdef['default']))
          {
            //$this->apiParameters[$pname] = $paramDefinition['default'];
            $params[] = $pdef['default'];
            $params_byname[$pname] = $pdef['default'];
          }
          else
          {
            //$this->apiParameters[] = '';
            $params[] = NULL;
            $params_byname[$pname] = NULL;
          }
        }
      }

      include_once("partner-face/$partner.php");

      // because Kate
      include_once("partner-face/ePay.php");
      include_once("partner-face/customercare/ValidateMSISDN.php");

      $params_json_encoded = json_encode($params);
      // remove 16 digits param values: they may be credit cards
      $params_json_encoded = preg_replace("/[\'\"]\d{16}[\'\"]/",'xxxxxxxxxxxxxxxx',$params_json_encoded);
      if (isset($_SERVER['PHP_AUTH_USER'])) {
        dlog('', "handling REST call: %s $site $partner $command, arguments %s", $_SERVER['PHP_AUTH_USER'], $params_json_encoded);
      } else {
        dlog('', "handling REST call: $site $partner $command, arguments %s", $params_json_encoded);
      }

      $out['command'] = $command;

      // Ensure the function actually exists before calling it
      if (is_array ($command)) {
        if (!method_exists ($command[0], $command[1])) {
          error_out (sprintf ('API_ERR_IMPLEMENTATION: Non-implemented function %s in class %s', $command[1], get_class($command[0])));
        }
      } else if (!function_exists ($command)) {
        error_out (sprintf ('API_ERR_IMPLEMENTATION: Non-implemented function %s', $command));
      }

      // Attempt to run the API call
      try {
        $api_return = call_user_func_array($command, $params);
      } catch (Exception $APIException) {
        error_out ($APIException->getMessage());
      }

      // Ensure the API return was valid
      if ($api_return === FALSE || $api_return === NULL) {
        error_out (sprintf ('API_ERR_IMPLEMENTATION: API call %s could not run or did not return a value.', $out['command']));
      } else if (trim ($api_return) == '') {
        error_out (sprintf ('API_ERR_IMPLEMENTATION: API call %s returned an empty string.', $out['command']));
      }

      // Send the API output to the browser
      exit ($api_return);
    }
    else
    {
      error_out("API_ERR_PARAMETER: unknown command $command, for partner $partner");
    }
    exit;
  }
}

/**
 * Terminates on Fatal errors
 *
 * @param unknown $err_string
 */
function error_out($err_string)
{
  echo flexi_encode(array("success" => false, "errors" => array($err_string)));

  // Terminate on fatal errors
  exit;
}

/**
 * Prints to dlog()
 *
 * @param string $response
 * @return string
 */
function log_out($response=NULL)
{
  global $request_id;
  global $out;
  global $saved_command;
  global $p;
  global $start;

  $command = isset($saved_command) ? $saved_command : json_encode($_REQUEST);

  $sep = '----';

  // to be used for API calls which end with a NOOP
  $log_skip = FALSE;
  if ( isset($p['log_skip']) && $p['log_skip'] )
    $log_skip = TRUE;

  $time = sprintf(", runtime: %.2f (sec)", microtime(TRUE) - $start);

  if ( ! $log_skip )
  {
    if ($response)
    {
      dlog("", $request_id . ' ' . "$sep$command:RESPONSE " . cleanse_credit_card_string($response) . $time);
    }
    else
    {
      dlog("", $request_id . ' ' . "$sep$command:REQUEST " . json_encode($_REQUEST) . $time);
    }
  }

  unset($out['utmz']);
  unset($out['homeboy']);
  unset($out['transition']);

  return $response;
}

/**
 *
 * @param array or xml $data
 * @return string json encoded
 */
function flexi_encode($data)
{
  global $xml;
  global $jsonp;

  log_out();

  if ($xml)
  {
    header("content-type: text/xml");
    if (is_array($data))
    {
      return log_out(to_xml($data, 'data', TRUE));
    }

    $xml_top = new SimpleXMLElement(sprintf("<?xml version=\"1.0\"?><data>%s</data>", htmlentities($data)));
    return log_out($xml_top->asXML());
  }
  else if ($jsonp)
  {
    return $jsonp . '('. log_out(json_encode($data)) . ');';
  }
  else
  {
    return log_out(json_encode($data));
  }
}

/**
 * Makes a PHP block of code.
 *
 * @param string $site
 * @param string $partner
 * @param string $e_config
 * @param string $p
 * @return string
 */
function make_php($site, $partner, $e_config, $p)
{
  dlog('', "making $site $partner PHP from config %s", json_encode($e_config));
  dlog('', "making $site $partner PHP from parsed JSON %s", json_encode($p));

  $code = '<?php

';

  foreach ($p['commands'] as $command => $def)
  {
    $code = $code . sprintf('
/**
 * %s
 *
 %s */
function %s(%s)
{
  global $p;
  global $mock;
  global $always_succeed;

  if ($always_succeed)
  {
    return flexi_encode(fill_return($p,
                                    "%s",
                                    func_get_args(),
                                    array("success"  => TRUE,
                                          "warnings" => array("ERR_API_INTERNAL: always_succeed"))));
  }

  $errors = validate_params($p, "%s", func_get_args(), $mock);

  if ($errors && count($errors) > 0)
  {
    // we use errors[] and warnings[] for more nuance instead of SoapFaults
    //throw new SoapFault("Server","Invalid parameters.");
    return flexi_encode(fill_return($p,
                                  "%s",
                                    func_get_args(),
                                    array("success" => FALSE,
                                          "errors"  => $errors)));
  }

  $success = ! count($errors);

  return flexi_encode(fill_return($p,
                                  "%s",
                                  func_get_args(),
                                  array("success" => $success,
                                        "errors"  => $errors)));
}

',
                            $def['desc'],
                            implode(' ', array_map(function($val)
                            {
                                return "* @param $val[1] \$$val[0]\n" ;
                            },
                                array_map(function($val)
                                    {
                                    return array($val['name'], $val['type']);
                            }, $def['parameters']))),
                            $command,
                            implode(', ', array_map(function($val)
                                                    {
                                                      return "\$$val";
                                                    },
                                                    array_map(function($val)
                                                              {
                                                                return $val['name'];
                                                              }, $def['parameters']))),
                            $command,
                            $command,
                            $command,
                            $command);
  }

  $code = $code . '
?>';

  return $code;
}

/**
 * make_wiki
 *
 * Makes a wiki entry for Confluence.
 *
 * @param string $site
 * @param string $partner
 * @param string $e_config
 * @param string $p
 * @return string
 */
function make_wiki($site, $partner, $e_config, $p)
{
  dlog('', "making $site $partner wiki markup from config %s", json_encode($e_config));
  dlog('', "making $site $partner wiki markup from parsed JSON %s", json_encode($p));

  $code="";
  $codep = "||Parameter Name||Type||Required||Detail||Validation Rules||\n";
  $coder = "||Parameter Name||Type||Detail||\n";

  foreach ($p['commands'] as $command => $def)
  {
    // Parameters
    $code .= "$partner - $command \n===\nInputs\n";
    $code .= $codep;
    foreach ($def['parameters'] as $pdef)
    {
      $code = $code . sprintf("|%s|%s|%s|%s|%s|\n",
                              $pdef['name'],
                              $pdef['type'],
                              $pdef['required'] ? 'required' : 'no',
                              $pdef['desc'],
                              $pdef['validation'] ? str_replace(array('{','}'),'',json_encode($pdef['validation'])) : 'none');
    }

    // Returns
    $code .= "\n\nReturns\n";
    $code .= $coder;
    foreach ($def['returns'] as $pdef)
    {
      $code = $code . sprintf("|%s|%s|%s|\n",
                              $pdef['name'],
                              $pdef['type'],
                              $pdef['desc']);
    }
    $code .= "\n\n===\n";
  }

  return $code;
}

function make_qa($p)
{
  return json_encode($p['commands']);
}

/**
 * Makes an html block of code.
 *
 * @param string $site
 * @param string $partner
 * @param string $e_config
 * @param string $p
 * @return string
 */
function make_html($site, $partner, $e_config, $p)
{
  dlog('', "making $site $partner HTML from config %s", json_encode($e_config));
  dlog('', "making $site $partner HTML from parsed JSON %s", json_encode($p));
  $code='';
  $codep = '<h3>Parameters</h3><table class="confluenceTable">
  <tbody>
  <tr>
  <th class="confluenceTh">Parameter Name</th>
  <th class="confluenceTh">Type</th>
  <th class="confluenceTh">Required</th>
  <th class="confluenceTh">Detail</th>
  <th class="confluenceTh">Validation Rules</th>
  </tr>';
  $coder = '<h3>Responses</h3><table class="confluenceTable">
  <tbody>
  <tr>
  <th class="confluenceTh">Parameter Name</th>
  <th class="confluenceTh">Type</th>
  <th class="confluenceTh">Detail</th>
  </tr>';

  foreach ($p['commands'] as $command => $def)
  {
    $code.= "<h2>$command</h2>";

    $code.= $codep;
    foreach ($def['parameters'] as $pdef)
    {
      $code = $code . sprintf('
        <tr>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        </tr>',
        $pdef['name'],
        $pdef['type'],
        $pdef['required'] ? 'required' : 'no',
        $pdef['desc'],
        $pdef['validation'] ? str_replace(array('{','}'),'',json_encode($pdef['validation'])) : 'none');
    }
    $code .= "\n</tbody></table>";
    $code.= $coder;
    foreach ($def['parameters'] as $pdef)
    {
      $code = $code . sprintf('
        <tr>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        <td class="confluenceTd">%s</td>
        </tr>',
        $pdef['name'],
        $pdef['type'],
        $pdef['desc']);
    }
    $code .= "\n</tbody></table>";
  }

  $code .= "<p>";

  return $code;
}

/**
 * Creates a WSDL from the input.
 *
 * @param string $site
 * @param string $partner
 * @param string $e_config
 * @param string $p
 * @return string
 */
function make_wsdl($site, $partner, $e_config, $p)
{
  dlog('', "making $site $partner wsdl from config %s", json_encode($e_config));
  dlog('', "making $site $partner wsdl from parsed JSON %s", json_encode($p));

  $namespace = "urn:httapi$partner";

  $type_map = array(
    'int[]'    => 'ArrayOf_xsd_int',
    'real[]'    => 'ArrayOf_xsd_float',
    'string[]' => 'ArrayOf_soapenc_string',
    'string'   => 'soapenc:string',
    'epoch'    => 'xsd:int',
    'integer'  => 'xsd:int',
    'real'     => 'xsd:float',
    'boolean'  => 'xsd:boolean',
    );

  $types = sprintf('
<wsdl:types>
  <schema targetNamespace="%s" xmlns="http://www.w3.org/2001/XMLSchema">
   <import namespace="http://schemas.xmlsoap.org/soap/encoding/"/>
   <complexType name="ArrayOf_soapenc_string">
    <complexContent>
     <restriction base="soapenc:Array">
      <attribute ref="soapenc:arrayType" wsdl:arrayType="soapenc:string[]"/>
     </restriction>
    </complexContent>
   </complexType>
   <complexType name="ArrayOf_xsd_int">
    <complexContent>
     <restriction base="soapenc:Array">
      <attribute ref="soapenc:arrayType" wsdl:arrayType="xsd:int[]"/>
     </restriction>
    </complexContent>
   </complexType>
   <complexType name="ArrayOf_xsd_float">
    <complexContent>
     <restriction base="soapenc:Array">
      <attribute ref="soapenc:arrayType" wsdl:arrayType="xsd:float[]"/>
     </restriction>
    </complexContent>
   </complexType>
  </schema>
 </wsdl:types>
', $namespace);

  $messages = '';
  $bindings_ops = '';
  $port_ops = '';

  foreach ($p['commands'] as $command => $def)
  {
    $parameters = $def['parameters'];

    $porder = '';
    $pdetail = '';

    foreach ($parameters as $pdef)
    {
      $pname = $pdef['name'];

      $porder = $porder . " $pname";

      if (isset($type_map[$pdef['type']]))
      {
        $pdetail = $pdetail . sprintf('
<wsdl:part name="%s" type="%s"/>
', $pname, $type_map[$pdef['type']]);
      }
      else
      {
        return "Unknown type for parameter $pname: $pdef[type]";
      }

    }

    $messages = $messages . sprintf('
   <wsdl:message name="%s_Response">
      <wsdl:part name="%s_Return" type="soapenc:string"/>
   </wsdl:message>

   <wsdl:message name="%s_Request">
%s
   </wsdl:message>
', $command, $command, $command, $pdetail);

    $port_ops = $port_ops . sprintf('
      <wsdl:operation name="%s" parameterOrder="%s">
         <wsdl:input message="impl:%s_Request" name="%s_Request"/>
         <wsdl:output message="impl:%s_Response" name="%s_Response"/>
      </wsdl:operation>
', $command, $porder, $command, $command, $command, $command);

    $bindings_ops = $bindings_ops . sprintf('
      <wsdl:operation name="%s">
         <wsdlsoap:operation soapAction=""/>
         <wsdl:input name="%s_Request">
            <wsdlsoap:body encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="%s" use="encoded"/>
         </wsdl:input>
         <wsdl:output name="%s_Response">
            <wsdlsoap:body encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="%s" use="encoded"/>
         </wsdl:output>
      </wsdl:operation>
', $command, $command, $namespace, $command, $namespace);
  }

  $bindings = sprintf('
   <wsdl:binding name="apiSoapBinding" type="impl:%s">
      <wsdlsoap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
%s
   </wsdl:binding>
', $partner, $bindings_ops);

  $ports = sprintf('
   <wsdl:portType name="%s">
%s
   </wsdl:portType>', $partner, $port_ops);

  return sprintf(
'<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions targetNamespace="urn:httapi%s" xmlns:apachesoap="http://xml.apache.org/xml-soap" xmlns:impl="urn:httapi%s" xmlns:intf="urn:httapi%s" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:wsdlsoap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:xsd="http://www.w3.org/2001/XMLSchema">

%s

%s

%s

%s

   <wsdl:service name="%sService">
      <wsdl:port binding="impl:apiSoapBinding" name="api">
         <wsdlsoap:address location="https://%s/ps/%s/%s/ultra/api"/>
      </wsdl:port>
   </wsdl:service>
</wsdl:definitions>', $partner, $partner, $partner, $types, $messages, $ports, $bindings, $partner, $site, $partner, $p['meta']['version']);

}


/**
 * validateBrand
 * validate API partner brand against allowed environment brands
 * @param String API partner
 * @param Array API definition
 * @return Boolean TRUE on successfull validation, FALSE otherwise
 */
function validateBrand($partner, $definition)
{
  // allow if JSON brand definition is missing
  if (empty($definition['brands']))
    return TRUE;

  // check against the allowed environemnt brands
  foreach ($definition['brands'] as $brand)
    if (\Ultra\UltraConfig\isBrandNameAllowed($brand))
      return TRUE;

  // API partner brand is not allowed
  return FALSE;
}

header("content-type: text/javascript");
echo json_encode($out);


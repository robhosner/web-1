<?php

use Ultra\Customers\Repositories\Mssql\CustomerRepository;

include_once('db.php');
include_once('db/accounts.php');
include_once('db/customers.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('db/htt_activation_log.php');
include_once('db/htt_inventory_sim.php');
include_once('db/htt_preferences_marketing.php');
include_once('db/ultra_activation_history.php');
include_once('db/ultra_customers_dealers.php');
include_once('Ultra/Lib/Util/Redis.php');
require_once 'Ultra/Lib/Util/Hash.php';

/**
 * create_ultra_customer_db_transaction
 *
 * Creates an Ultra customer using a MSSQL transaction
 *
 * @return array
 */
function create_ultra_customer_db_transaction($params)
{
  dlog('', '(%s)', func_get_args());

  # wraps create_ultra_customer in a DB transaction

  $result = array( 'errors' => array() , 'customer' => NULL );

  $params['mvne'] = '2';

  if ( ! start_mssql_transaction() )
  { $result['errors'][] = array('ERR_API_INTERNAL: DB error (2)'); }

  if ( count($result['errors']) < 1 )
  { $result = create_ultra_customer($params); }

  if ( $result['customer_id'] )
  {
    if ( ! commit_mssql_transaction() )
    { $result['errors'][] = array('ERR_API_INTERNAL: DB error (3)'); }
  }
  else
  {
    if ( ! rollback_mssql_transaction() )
    { $result['errors'][] = array('ERR_API_INTERNAL: DB error (4)'); }
  }

  if ( $result['customer_id'] )
  {
    $result['customer'] = get_customer_from_customer_id( $result['customer_id'] );

    $params['customer_id'] = $result['customer_id'];

    assign_ultra_customer_dealer($params);
  }
  else
  {
    $errors = $result['errors'];
    $result['customer'] = NULL;
  }

  return $result;
}

/**
 * assign_ultra_customer_dealer
 *
 * Invoked when a customer is created
 * Inserts a row into ULTRA.CUSTOMERS_DEALERS
 * Inserts a row into ULTRA.HTT_ACTIVATION_HISTORY
 *
 * @return NULL
 */
function assign_ultra_customer_dealer($params)
{
  if ( !isset($params['dealer']) )
  {
    $params['dealer'] = 0;

    dlog('',"dealer not provided to assign_ultra_customer_dealer, using 0");
  }

  $association_type = 'ACTIVATION';

  if ( isset($params['association_type']) )
    $association_type = $params['association_type'];

  // insert into ULTRA.CUSTOMERS_DEALERS
  $success = add_to_ultra_customers_dealers(
    array(
      'customer_id'      => $params['customer_id'],
      'dealer'           => $params['dealer'],
      'association_type' => $association_type
    )
  );

  if ( ! $success )
    dlog('',"add_to_ultra_customers_dealers failed");

  $ultra_activation_history_data = array(
    'customer_id'      => $params['customer_id'],
    'dealer'           => $params['dealer'],
    'final_state'      => STATE_NEUTRAL
  );

  if ( isset($params['final_state']) )
    $ultra_activation_history_data['final_state']               = $params['final_state'];
  if ( isset($params['masteragent']) )
    $ultra_activation_history_data['masteragent']               = $params['masteragent'];
  if ( isset($params['current_iccid_full']) )
    $ultra_activation_history_data['iccid_full']                = $params['current_iccid_full'];
  if ( isset($params['current_mobile_number']) )
    $ultra_activation_history_data['msisdn']                    = $params['current_mobile_number'];
  if ( isset($params['cos_id']) )
    $ultra_activation_history_data['cos_id']                    = $params['cos_id'];
  if ( isset($params['activation_cos_id']) )
    $ultra_activation_history_data['cos_id']                    = $params['activation_cos_id'];
  if ( isset($params['distributor']) )
    $ultra_activation_history_data['distributor']               = $params['distributor'];
  if ( isset($params['userid']) )
    $ultra_activation_history_data['userid']                    = $params['userid'];
  if ( isset($params['activation_type']) )
    $ultra_activation_history_data['activation_type']           = $params['activation_type'];
  if ( isset($params['funding_source']) )
    $ultra_activation_history_data['funding_source']            = $params['funding_source'];
  if ( isset($params['funding_amount']) )
    $ultra_activation_history_data['funding_amount']            = $params['funding_amount'];
  if ( isset($params['last_attempted_transition']) )
    $ultra_activation_history_data['last_attempted_transition'] = $params['last_attempted_transition'];
  if ( isset($params['promised_amount']) )
    $ultra_activation_history_data['promised_amount']           = $params['promised_amount'];

  // insert into ULTRA.HTT_ACTIVATION_HISTORY
  $success = add_to_ultra_activation_history( $ultra_activation_history_data );

  if ( ! $success )
    dlog('',"add_to_ultra_activation_history failed");

  return NULL;
}

/**
 * clone_customer
 * 
 * creates a ACCOUNTS,CUSTOMERS,HTT_CUSTOMERS_OVERLAY_ULTRA rows
 * UPDATES each row with information attained from provided CUSTOMER_ID
 * COS_ID and PLAN_STATE are updated to STANDBY/NEUTRAL
 * 
 * @param  Integer $prev_customer_id CUSTOMER_ID to clone
 * @param Object HTT_SIM_INVENTORY row
 * @return Object $customer || NULL on error
 */
function clone_customer($prev_customer_id, $sim)
{
  $customer_n = get_unique_customer_number();

  \logDebug("Cloning $prev_customer_id to new CUSTOMER NUMBER $customer_n");
  
  // create initial CUSTOMERS and ACCOUNTS rows
  $signup_query = make_signup_query($customer_n);
  mssql_fetch_all_objects(logged_mssql_query($signup_query));

  $customer = find_customer(make_find_new_signup_status_customer_query($customer_n));
  if ($customer && isset($customer->CUSTOMER_ID))
  {
    // create initial HTT_CUSTOMERS_OVERLAY_ULTRA row with PLAN_STATE = Neutral
    if (is_mssql_successful(logged_mssql_query(htt_customers_overlay_ultra_insert_query(
      array(
        'customer_id'   => $customer->CUSTOMER_ID,
        'current_iccid' => $sim->ICCID_NUMBER,
        'current_iccid_full' => $sim->ICCID_FULL,
        'plan_started'  => 'NULL',
        'plan_expires'  => 'NULL',
        'plan_state'    => STATE_NEUTRAL,
        'mvne'          => $sim->MVNE,
        'brand_id'      => $sim->BRAND_ID,
      )
    ))))
    {
      // clone ACCOUNTS row data to new CUSTOMER_ID
      $result = accounts_clone_by_customer_id($prev_customer_id, $customer->CUSTOMER_ID, array(
        'PIN',
        'PARENT_ACCOUNT_ID',
        'BATCH_ID',
        'SEQUENCE_NUMBER',
        'ACCOUNT_GROUP_ID',
        'ACCOUNT_TYPE',
        'CALLBACK_NUMBER',
        'BILLING_TYPE',
        'CREATION_DATE_TIME',
        'ACTIVATION_DATE_TIME',
        'STARTING_BALANCE',
        'CREDIT_LIMIT',
        'STARTING_PACKAGED_BALANCE1',
        'PACKAGED_BALANCE1',
        'SERVICE_CHARGE_DATE',
        'COS_ID',
        'WRITE_CDR',
        'SERVICE_CHARGE_STATUS',
        'CALLS_TO_DATE',
        'LAST_CALL_DATE_TIME',
        'MINUTES_TO_DATE_BILLED',
        'MINUTES_TO_DATE_ACTUAL',
        'LAST_CALLER',
        'LAST_CALLED_PARTY',
        'LAST_CREDIT_DATE_TIME',
        'LAST_DEBIT_DATE_TIME',
        'LAST_RECHARGE_DATE_TIME',
        'LAST_TRANSFER_DATE_TIME',
        'LAST_STATUS_UPDATE_DATE_TIME',
        'LAST_AUTHENTICATED_DATE_TIME',
        'PACKAGED_BALANCE2',
        'PACKAGED_BALANCE3',
        'PACKAGED_BALANCE4',
        'PACKAGED_BALANCE5',
        'STARTING_PACKAGED_BALANCE2',
        'STARTING_PACKAGED_BALANCE4',
        'STARTING_PACKAGED_BALANCE5',
        'FIRST_AUTHENTICATED_DATE_TIME',
        'PERIOD_CALLS_TO_DATE',
        'PERIOD_MINUTES_TO_DATE_BILLED',
        'PERIOD_MINUTES_TO_DATE_ACTUAL',
        'PERIOD_LAST_RESET_DATE_TIME',
        'ACCOUNT_STATUS_TYPE'
      ));
      if ($result->is_failure())
      {
        $errors = $result->get_errors();
        return \logError($errors[0]);
      }

      // clone CUSTOMERS row data to new CUSTOMER_ID
      $result = customers_clone_by_customer_id($prev_customer_id, $customer->CUSTOMER_ID, array(
        'FIRST_NAME',
        'LAST_NAME',
        'COMPANY',
        'ADDRESS1',
        'ADDRESS2',
        'CITY',
        'STATE_REGION',
        'POSTAL_CODE',
        'COUNTRY',
        'LOCAL_PHONE',
        'FAX',
        'E_MAIL',
        'DATE_OF_BIRTH',
        'SIGNUP_DATE',
        // 'LOGIN_NAME',
        // 'LOGIN_PASSWORD',
        'CC_NUMBER',
        'CC_EXP_DATE',
        'CC_NAME',
        'CC_ADDRESS1',
        'CC_ADDRESS2',
        'CC_CITY',
        'CC_COUNTRY',
        'CC_STATE_REGION',
        'CC_POSTAL_CODE',
        'NOTES',
        'USER_1',
        'USER_2',
        'USER_3',
        'USER_4',
        'USER_5',
        'USER_6',
        'USER_7',
        'USER_8',
        'USER_9',
        'USER_10',
        'CCV',
        'PUBLIC_CUSTOMER'
      ));
      if ($result->is_failure())
      {
        $errors = $result->get_errors();
        return \logError($errors[0]);
      }

      // clone HTT_CUSTOMERS_OVERLAY_ULTRA data to new CUSTOMER_ID
      $result = htt_customers_overlay_ultra_clone_by_customer_id($prev_customer_id, $customer->CUSTOMER_ID, array(
        'notes',
        'current_mobile_number',
        'applied_data_soc',
        'applied_data_soc_date',
        'easypay_activated',
        'preferred_language',
        'plan_started',
        'plan_expires',
        'monthly_cc_renewal',
        'tos_accepted',
        'CANCEL_REQUESTED',
        'CANCELLED',
        'DATA_RECHARGES_LEFT',
        'CUSTOMER_SOURCE',
        'GROSS_ADD_DATE'
      ));
      if ($result->is_failure())
      {
        $errors = $result->get_errors();
        return \logError($errors[0]);
      }

      // UPDATE COS_ID to STANDY for cloned customer
      if (!  is_mssql_successful(logged_mssql_query(accounts_update_query(
        array(
          'cos_id'      => get_cos_id_from_plan('STANDBY'),
          'customer_id' => $customer->CUSTOMER_ID
        )
      ))))
      {
        return \logError('ERROR updating ACCOUNTS for CUSTOMER_ID ' . $customer->CUSTOMER_ID);
      }
    }
  }
  else
  {
    return \logError('ERROR creating new customer');
  }

  $customer = get_customer_from_customer_id($customer->CUSTOMER_ID);

  return $customer;
}

/**
 * create_ultra_customer
 *
 * @return array
 */
function create_ultra_customer($params)
{
  $cos_id = $params['cos_id'];

  $return = array(
    'errors'      => array(),
    'customer_id' => FALSE
  );

  # step 1 : create rows in ACCOUNTS and CUSTOMERS

  // get BRAND_ID from activation ICCID
  $params['brand_id'] = get_brand_id_from_iccid($params['current_iccid_full']);
  // brand_id cannot be NULL
  if (empty($params['brand_id']))
  {
    $return['errors'][] = 'Cannot create customer : BRAND_ID of ICCID cannot be NULL';
    return $return;
  }

  // set default preferred language based on brand_id IF NOT SET
  if ( ! isset($params['preferred_language']))
  {
    switch ($params['brand_id'])
    {
      case 1: // ULTRA
        $params['preferred_language'] = 'EN';
        break;
      case 2: // UNIVISION
        $params['preferred_language'] = 'ES';
        break;
    }
  }

  $customer_n = get_unique_customer_number();

  $signup_query = make_signup_query($customer_n);

  # TODO:error handling?
  mssql_fetch_all_objects(logged_mssql_query($signup_query));

  $new_signup_status_customer_query = make_find_new_signup_status_customer_query($customer_n);

  $customer = find_customer($new_signup_status_customer_query);

  if ($customer)
  {

    # step 2 : update COS_ID to a valid ULTRA COS_ID

    $accounts_update_query = accounts_update_query(
      array(
        'cos_id'      => $cos_id,
        'customer_id' => $customer->CUSTOMER_ID
      )
    );

    if ( is_mssql_successful(logged_mssql_query($accounts_update_query)) )
    {

      # step 3 : update CUSTOMERS

      $params['customer_id'] = $customer->CUSTOMER_ID;

      if ( ( ! isset( $params['login_name'] ) ) && ( ! isset( $params['login_password'] ) ) )
      {
        $params['SET_NULL_LOGIN'] = 1;
      }
      else
        $params['login_password'] = \Ultra\Lib\Util\encryptPasswordHS($params['login_password']);

      $customers_update_query = customers_update_query( $params );

      if ( is_mssql_successful(logged_mssql_query($customers_update_query)) )
      {

        # step 4 : add row in HTT_CUSTOMERS_OVERLAY_ULTRA

        $params['customer_id'] = $customer->CUSTOMER_ID;

        $htt_customers_overlay_ultra_insert_query = htt_customers_overlay_ultra_insert_query( $params );

        if ( is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_insert_query)) )
        {

          # step 5 : add row in HTT_PREFERENCES_MARKETING

          $htt_preferences_marketing_insert_query = htt_preferences_marketing_insert_query( $params );

          if ( is_mssql_successful(logged_mssql_query($htt_preferences_marketing_insert_query)) ) {
            $return['customer_id'] = $customer->CUSTOMER_ID;

            if (!empty($params['contact_phone'])) {
              if (!(new CustomerRepository())->addCustomerContactPhone($customer->CUSTOMER_ID, $params['contact_phone'])) {
                $return['errors'][] = "Cannot add customer contact_phone";
              }
            }

            if ($params['brand_id'] === 1) {
              $errors = \set_marketing_settings([
                'marketing_email_option' => 0,
                'marketing_sms_option' => 0,
                'customer_id' => $customer->CUSTOMER_ID
              ]);

              if (count($errors)) {
                dlog('', '%s', $errors);
                $return['errors'][] = "Cannot create customer (6)";
              }

              if (!\ultra_customer_options_insert('PREFERENCES.OPT_OUT.VOICE', 1, $customer->CUSTOMER_ID)) {
                $return['errors'][] = "Cannot create customer (6)";
              }

            } else if ($params['brand_id'] === 3) {
              $errors = \set_marketing_settings([
                'marketing_email_option' => 1,
                'marketing_sms_option' => 1,
                'customer_id' => $customer->CUSTOMER_ID
              ]);

              if (count($errors)) {
                dlog('', '%s', $errors);
                $return['errors'][] = "Cannot create customer (6)";
              }

              if (!\ultra_customer_options_insert('PREFERENCES.OPT_OUT.VOICE', 0, $customer->CUSTOMER_ID)) {
                $return['errors'][] = "Cannot create customer (6)";
              }
            }

          } else {
            $return['errors'][] = "Cannot create customer (5)";
          }
        }
        else
          $return['errors'][] = "Cannot create customer (4)";
      }
      else
        $return['errors'][] = "Cannot create customer (3)";
    }
    else
      $return['errors'][] = "Cannot create customer (2)";
  }
  else
    $return['errors'][] = "Cannot create customer (1)";

  return $return;
}

function record_ICCID_activation_funcall(
  $iccid,
  $customer_id,
  $masteragent,
  $user_id,
  $transition_uuid,
  $agent,
  $distributor=NULL,
  $store=NULL
)
{
  return record_ICCID_activation(
    array(
      'iccid'           => $iccid,
      'customer_id'     => $customer_id,
      'masteragent'     => $masteragent,
      'user_id'         => $user_id,
      'transition_uuid' => $transition_uuid,
      'distributor'     => $distributor,
      'store'           => $store,
      'agent'           => $agent
    )
  );
}

# Input:
#   iccid ( 19 chars will be stripped to 18 )
#   customer_id
#   masteragent
#   distributor (not required)
#   store       (not required)
#   user_id
#   agent
#   transition_uuid
#   promised_amount (see BOLT-38)
function record_ICCID_activation($params)
{
  # Captures the Activation Attribution information in HTT_ACTIVATION_LOG and updates to ICCID info in HTT_INVENTORY_SIM

  dlog("","record_ICCID_activation params = ".json_encode($params));

  $result = array( 'errors' => array() , 'success' => FALSE );

  if ( ! isset( $params['iccid'] ) )
  {
    dlog("","ERROR: cannot be called without 'iccid'");
    return array( 'errors' => array('ERR_API_INTERNAL: Software error (10)') , 'success' => FALSE );
  }

  if ( ! start_mssql_transaction() )
  {
    return array( 'errors' => array('ERR_API_INTERNAL: DB error (10)') , 'success' => FALSE );
  }

  # patch: $params['iccid'] should be 18 characters
  if ( strlen($params['iccid']) > 18 )
  {
    preg_match("/^(.{18})/",$params['iccid'],$matches);

    $params['iccid'] = $matches[0];
  }

  if ( count($result['errors']) < 1 )
  {
    # insert into HTT_ACTIVATION_LOG

    if ( ( ! isset($params['distributor']) ) || is_null($params['distributor']) ) { $params['distributor'] = 'NULL'; }
    if ( ( ! isset($params['store']) )       || is_null($params['store'])       ) { $params['store']       = 'NULL'; }

    $htt_activation_log_insert_query = htt_activation_log_insert_query(
      array(
        'iccid'           => $params['iccid'], # htt_activation_log.ICCID_NUMBER lenght is 18
        'customer_id'     => $params['customer_id'],
        'masteragent'     => $params['masteragent'],
        'distributor'     => $params['distributor'],
        'dealer'          => $params['store'],
        'user_id'         => $params['user_id'],
        'date'            => 'getutcdate()',
        'activated_by'    => $params['agent'],
        'promised_amount' => $params['promised_amount'],
        'initial_cos_id'  => $params['initial_cos_id'],
        'initial_bolt_ons' => $params['initial_bolt_ons'])
    );

    if ( ! is_mssql_successful(logged_mssql_query($htt_activation_log_insert_query)) )
    { $return['errors'][] = "ERR_API_INTERNAL: DB error (11)"; }
  }

  if ( count($result['errors']) < 1 )
  {
    # update HTT_INVENTORY_SIM

    $htt_inventory_sim_update_query = htt_inventory_sim_update_query(
      array(
        'batch_sim_set'        => array($params['iccid']), # HTT_INVENTORY_SIM.ICCID_NUMBER lenght is 18
        'sim_activated'        => 1,
        'customer_id'          => $params['customer_id'],
        'last_changed_date'    => 'getutcdate()',
        'last_changed_by'      => ( isset( $params['user_id'] ) ? $params['user_id'] : '0' ),
        'last_transition_uuid' => $params['transition_uuid']
      )
    );

    if ( ! is_mssql_successful(logged_mssql_query($htt_inventory_sim_update_query)) )
    { $return['errors'][] = "ERR_API_INTERNAL: DB error (12)"; }
  }

  if ( count($result['errors']) < 1 )
  {
    if ( commit_mssql_transaction() )
    {
      $result['success'] = TRUE;
    }
    else
    { $result['errors'][] = array('ERR_API_INTERNAL: DB error (13)'); }
  }
  else
  {
    if ( ! rollback_mssql_transaction() )
    { $result['errors'][] = array('ERR_API_INTERNAL: DB error (14)'); }
  }

  return $result;
}

/**
 * save_port_account
 *
 * Store for 2 weeks port account credentials for 2 weeks in Redis
 *
 * @return boolean
 */
function save_port_account( $msisdn , $port_account_number , $port_account_password )
{
  if ( !$msisdn || !is_numeric( $msisdn ) )
    return FALSE;

  if ( (strlen($msisdn) == 11) )
    $msisdn = substr($msisdn, 1);

  $redis = new \Ultra\Lib\Util\Redis;

  if ( $port_account_number )
    $redis->set( 'ultra/port/'.$msisdn.'/account_number'   , $port_account_number   , 14 * 24 * 60 * 60 );

  if ( $port_account_password )
    $redis->set( 'ultra/port/'.$msisdn.'/account_password' , $port_account_password , 14 * 24 * 60 * 60 );

  return TRUE;
}

/**
 * retrieve_port_account
 *
 * Get port account credentials stored with a previous save_port_account
 */
function retrieve_port_account( $msisdn )
{
  if ( (strlen($msisdn) == 11) )
    $msisdn = substr($msisdn, 1);

  $redis = new \Ultra\Lib\Util\Redis;

  return array(
    'port_account_number'   => ( ( $msisdn && is_numeric( $msisdn ) ) ? $redis->get( 'ultra/port/'.$msisdn.'/account_number'   ) : '' ),
    'port_account_password' => ( ( $msisdn && is_numeric( $msisdn ) ) ? $redis->get( 'ultra/port/'.$msisdn.'/account_password' ) : '' )
  );
}

/**
 * cleanse_port_account
 *
 * Clean up port account credentials stored with a previous save_port_account
 */
function cleanse_port_account( $msisdn )
{
  if ( !$msisdn || !is_numeric( $msisdn ) )
    return FALSE;

  if ( (strlen($msisdn) == 11) )
    $msisdn = substr($msisdn, 1);

  $redis = new \Ultra\Lib\Util\Redis;

  $redis->del( 'ultra/port/'.$msisdn.'/account_number'   );
  $redis->del( 'ultra/port/'.$msisdn.'/account_password' );

  return TRUE;
}

/**
 * clear_redis_provisioning_values
 *
 * @return NULL
 */
function clear_redis_provisioning_values($customer, $redis=NULL, $transition=NULL)
{
  if (empty($customer))
  {
    dlog('', 'ERROR: missing customer ID');
    return NULL;
  }

  // prepare redis, keys and TTL
  if (! $redis)
    $redis = new \Ultra\Lib\Util\Redis;
  $customer_key = "provisioning/$customer";
  $transition_key = empty($transition) ? NULL : "provisioning/$transition";

  // clear keys
  foreach (array('masteragent', 'agent', 'distributor', 'store', 'userid') as $key)
  {
    $redis->del("$customer_key/activation_$key");

    if ($transition)
      $redis->del("$transition_key/activation_$key");
  }

  return NULL;
}

/**
 * set_redis_provisioning_values
 *
 * save redis provisioning values; called inside APIs after creation of a customer activation transition
 *
 * @return NULL
 */
function set_redis_provisioning_values($customer, $masteragent, $agent, $distributor, $store, $userid, $redis = NULL, $transition = NULL)
{
  if (empty($customer))
  {
    dlog('', 'ERROR: missing customer ID');
    return NULL;
  }

  // prepare redis, keys and TTL
  if (! $redis)
    $redis = new \Ultra\Lib\Util\Redis;
  $customer_key = "provisioning/$customer";
  $transition_key = empty($transition) ? NULL : "provisioning/$transition";  
  $ttl = 60 * 60 * 24 * 14;

  // set given values
  foreach (array('masteragent', 'agent', 'distributor', 'store', 'userid') as $key)
    if (! empty($$key))
    {
      $redis->set("$customer_key/activation_$key", $$key, $ttl);
      if ($transition_key)
        $redis->set("$transition_key/activation_$key", $$key, $ttl);
    }

  return NULL;
}

/**
 * get_redis_provisioning_values
 *
 * returns redis values saved by set_redis_provisioning_values
 *
 * @uses: list($masteragent, $agent, $distributor, $store, $userid) = get_redis_provisioning_values($customer_id);
 */
function get_redis_provisioning_values($customer_id, $transition=NULL, $redis=NULL)
{
  $params = array();

  if (empty($customer_id))
  {
    dlog('', 'ERROR: missing customer ID');
    return array(NULL, NULL, NULL, NULL, NULL);
  }

  // prepare redis and keys
  if (! $redis)
    $redis = new \Ultra\Lib\Util\Redis;

  // get values
  foreach (array('masteragent', 'agent', 'distributor', 'store', 'userid') as $key)
  {
    $value = '';

    // try by customer ID first then transition UUID
    if ( ( ! $value = $redis->get('provisioning/'.$customer_id."/activation_$key") ) && $transition )
      $value = $redis->get('provisioning/'.$transition."/activation_$key");

    $params[] = $value;
  }

  dlog('', 'values: %s', $params);
  return $params;
}

/**
 * activations_enabled
 *
 * check if activations are currently enabled
 * @param bool dealerPortal: also check if Dealer Portal activations are enabled
 * @return bool TRUE if enabled FALSE otherwise
 */
function activations_enabled($dealerPortal = FALSE)
{
  // instantiate a Ultra\Lib\Util\Settings object
  $settings = new \Ultra\Lib\Util\Settings;

  // get the appropriate setting
  $all    = $settings->checkUltraSettings('ultra/activations/enabled');
  $portal = $settings->checkUltraSettings('ultra/activations/dealer_portal/enabled');

  if ( $dealerPortal && ( ( $portal == '0' ) || ( $portal == 'FALSE' ) ) )
    return FALSE;

  return ! ( ( $all == '0' ) || ( $all == 'FALSE' ) );
}


<?php

/* functions used by runners/monthly-charge.php */ 

require_once 'db.php';
require_once 'cosid_constants.php';
require_once 'lib/messaging/functions.php';
require_once 'lib/partner-validate.php';
require_once 'lib/payments/functions.php';
require_once 'lib/state_machine/functions.php';
require_once 'Ultra/Lib/Util/Settings.php';
require_once 'Ultra/Lib/Billing/functions.php';
require_once 'Ultra/Lib/DB/MRC/Groups.php';
require_once 'lib/util-runners.php';
require_once 'classes/CustomerOptions.php';
require_once 'classes/MRC/FlexMRC.php';
require_once 'classes/Flex.php';

function main($input_values)
{
  global $settings;
  $redis = new \Ultra\Lib\Util\Redis;

  teldata_change_db(); // connect to DB

  $settings = new \Ultra\Lib\Util\Settings;

  // API-212: group initialization
  if ($input_values['daily_init'])
    return Ultra\Lib\DB\MRC\initGroups($redis);

  // API-214: invoke old processing logic when groups is default -1 or when subscriber list is given
  if ($input_values['groups'][0] == -1 || count($input_values['customer_id_list']))
    process_without_groups($input_values, $redis);
  else
    process_groups($input_values, $redis);
}

/**
 * process_without_groups
 * old processing logic without groups (will also process remaining subscribers not covered by groups)
 * @param Array command line parameters
 */
function process_without_groups($input_values, $redis)
{
  // acquire customer list
  $main_response = acquire_customer_list($input_values);

  if ( count($main_response['errors']) )
  {
    dlog('',"errors = %s",$main_response['errors']);
  }
  elseif ( count($main_response['customer_id_list']) )
  {
    if ( $input_values['cancellation'] )
    {
      process_cancellation($main_response['customer_id_list'],$main_response['customer_plan_states']);
    }
    elseif ($input_values['retry'])
    {
      $skip_can_process = TRUE;
      process_customers($main_response['customer_id_list'], $redis, $skip_can_process);
    }
    else
    {
      fix_monthly_charge_zombies();
      process_customers($main_response['customer_id_list'], $redis);
    }
  }
  else
  {
    dlog('', "No customers to process.");
  }
}

/**
 * progress_groups
 * process previously initialized subscribers according to groups
 * Array command line parameters
 */
function process_groups($params, $redis)
{
  // initialize
  $groups = \Ultra\Lib\DB\MRC\loadGroups($params['groups'], $redis);
  if (empty($groups))
    return logError('failed to load MRC groups');
  $timer = calculate_runtime();

  // loop through given groups
  foreach ($groups as $group)
  {
    // load customers associated with the group
    $subscribers = load_group_subscribers($group->MRC_GROUP_ID, $params['max_customers']);
    logInfo("processing group {$group->MRC_GROUP_ID} ({$group->DESCRIPTION}): " . count($subscribers) . ' subscribers found');

    // process each subscriber
    foreach ($subscribers as $subscriber)
    {
      // check if we are allowed to execute
      delay_execution();
      if (time() > $timer)
        return logWarn('maximum execution time reached');

      // process subscriber, log but ignore any errors
      process_group_subscriber($subscriber, $params, $redis);

      // update and check max count if present
      if ($params['max_customers'])
        if ( ! --$params['max_customers'])
          return logInfo("finished processing max number of subscribers");
    }
  }
}

/**
 * process_group_subscriber
 * process customer belonging to an MRC group
 * @param Object subscriber (DB row)
 * @param Arary script arguments
 * @param Object redis instance
 */
function process_group_subscriber($subscriber, $params, $redis)
{
  logInfo("processing subscriber {$subscriber->CUSTOMER_ID}");
  $success = FALSE; // successfully processed

  // get customer from DB
  $customer = getCustomerFromCustomerIdForMRCProc($subscriber->CUSTOMER_ID);
  if (empty($customer))
    return logError("failed to get customer from DB for subscriber {$subscriber->CUSTOMER_ID}");

  // check plan state, open transitions and other reservations
  $can_process = can_process_monthly_charge_customer($subscriber->CUSTOMER_ID);

  // reserve subscriber, skip if failed
  if ( ! \Ultra\Lib\DB\MRC\reserveSubscriber($subscriber, $redis))
    return logWarn("failed to reserve subscriber {$subscriber->CUSTOMER_ID}");
  $lock = array('lock_id' => TRUE, 'attempt_count' => $subscriber->ATTEMPT_COUNT++, 'status' => 'RESERVED'); // legacy

  // process customer
  if ($params['cancellation'])
    $result['errors'] = process_customer_cancellation($customer, $customer->plan_state, TRUE);
  elseif ($params['retry'] || $can_process)
    $result = process_customer($subscriber->CUSTOMER_ID, $lock, $redis, $customer);
  else
    logWarn("unable to process subscriber {$subscriber->CUSTOMER_ID}");

  // handle errors
  if ( ! empty($result['transient_errors']))
    monthly_service_charge_log_transient_error($subscriber->CUSTOMER_ID, $lock['attempt_count'], $result['transient_errors']);
  elseif ( ! empty($result['rejection_errors']))
    monthly_service_charge_log_fatal_error($subscriber->CUSTOMER_ID, $result['rejection_errors']);
  elseif ( ! empty($result['errors']))
    monthly_service_charge_log_fatal_error($subscriber->CUSTOMER_ID, $result['errors']);
  else
    $success = TRUE;

  // un-reserve regardless of error
  \Ultra\Lib\DB\MRC\releaseSubscriber($subscriber, $redis, $success);
}

/**
 * process_cancellation
 * cancel subscribers (for various reasons)
 * @param Array of customer IDs
 * @param Array of corresponding plan states
 */
function process_cancellation($customer_id_list,$customer_plan_states)
{
  /* process cancellation */

  dlog('', '(%s)', func_get_args());

  foreach( $customer_id_list as $i => $customer_id ) // loop through customer ids
  {
    delay_execution();

    // transition the customer to 'Cancelled'

    if ( \Ultra\UltraConfig\is_monthly_charge_enabled() )
    {
      $customer = getCustomerFromCustomerIdForMRCProc($customer_id);

      $errors = process_customer_cancellation($customer,$customer_plan_states[ $i ], TRUE);

      if ( count($errors) )
      {
        dlog('',json_encode($errors));

        dlog('', "Error: cannot cancel customer %d", $customer_id);
      }
    }
  }
}

/**
 * process_customers
 * renew active subscribers according to their plan
 * @param Array of customer IDs
 * @param Object redis instance
 * @param Boolean TRUE to skip check for of subscriber state, open transitions and previous processing errors, FALSE to perform  the check
 */
function process_customers($customer_id_list, $redis, $skip_can_process = FALSE)
{
  /* process monthly charge for the customers */
  $time_allowed_end = calculate_runtime();
  foreach( $customer_id_list as $i => $customer_id ) // loop through customer ids
  {
    delay_execution();

    if ( time() >= $time_allowed_end )
    {
      dlog('', "process_customers won't process any more customers because it took already more than 1 hour");
      break;
    }

    dlog('', "Processing customer id %d", $customer_id);

    if ( ($skip_can_process || can_process_monthly_charge_customer( $customer_id )) && \Ultra\UltraConfig\is_monthly_charge_enabled() )
    {
      $redis_lock_result = $redis->get(MRC_RESERVATION_KEY . $customer_id);
      if ( ! $redis_lock_result )
      {
        // reserve customer using table HTT_MONTHLY_SERVICE_CHARGE_LOG
        $lock_result = monthly_service_charge_log_lock($customer_id);

        dlog('', "Lock attempt for customer id %d : %d", $customer_id, $lock_result['lock_id']);

        if ( $lock_result['lock_id'] )
        {
          // reservation success

          $redis->setnx(MRC_RESERVATION_KEY . $customer_id, $lock_result['lock_id'], MRC_RESERVATION_TTL);

          usleep(300000);

          $result = process_customer($customer_id, $lock_result, $redis);

          if ( isset($result['transient_errors']) && count($result['transient_errors']) )
          {
            dlog('', "Recording transient error(s) for customer id %d (%d)", $customer_id, $lock_result['attempt_count']);

            // record transient error in HTT_MONTHLY_SERVICE_CHARGE_LOG
            monthly_service_charge_log_transient_error($customer_id,$lock_result['attempt_count'],$result['transient_errors']);
          }
          elseif ( count($result['rejection_errors']) )
          {
            dlog('', "Recording rejection error(s) for customer id %d", $customer_id);

            // record error in HTT_MONTHLY_SERVICE_CHARGE_LOG
            monthly_service_charge_log_fatal_error($customer_id,$result['rejection_errors']);
          }
          elseif ( count($result['errors']) )
          {
            dlog('', "Recording fatal error(s) for customer id %d", $customer_id);

            // record error in HTT_MONTHLY_SERVICE_CHARGE_LOG
            monthly_service_charge_log_fatal_error($customer_id,$result['errors']);
          }
          else
          {
            dlog('', "Releasing customer id %d", $customer_id);

            // free (unreserve) customer in HTT_MONTHLY_SERVICE_CHARGE_LOG
            monthly_service_charge_log_release($customer_id);
          }
        }
      }
    }
    else
    {
      dlog('', "Monthly Charge skipped for customer id %d", $customer_id);
    }
  }
}

/**
 * process_customer_suspend
 * create a suspend transition for the given subscriber
 * @param Integer customer ID
 * @return Array
 */
function process_customer_suspend($customer)
{
// the suspend transition needs to trigger an SMS to the customer informing them of their suspend status and asking for payment.
// we will track their suspend period by using htt_customers_overlay_ultra.plan_expires, once sysdate>90 days, we will transition that customer to deactivate: TODO separate deactivate job.

  dlog('', "about to suspend customer %d", $customer->customer_id);

  $context = array(
    'customer_id' => $customer->customer_id
  );

  $result = suspend_account($customer,$context);

  if ( count($result['errors']) )
  { dlog('', "Error: cannot suspend customer %d", $customer->customer_id); }

  return $result;
}

/**
 * process_customer_cancellation
 * create cancellation transition for the given customer
 * @param Object customer DB row
 * @param String plan state
 * @param Boolean TRUE to check for DEV or provisioning customer, FALSE otherwise
 * @retur Array
 */
function process_customer_cancellation($customer, $plan_state, $check)
{
  dlog('',"about to cancel customer %s which is in state %s",$customer->customer_id,$plan_state);
  $errors = array();
  $context = array('customer_id' => $customer->customer_id);

  if ($check)
  {
    if (is_dev_customer($customer))
      return $errors[] = "will not cancel DEV/QA customer $customer";
    elseif (delay_provisioned_customer_cancellation($customer, $customer->plan_state))
      return $errors[] = "will not cancel Provisioned customer $customer";
  }

  /* process cancellation for a customer */

  $notes = build_cancellation_caption( $plan_state );

  $result_status = cancel_account($customer,$context,NULL,'Monthly Runner',$notes,'Monthly Runner');

  $errors = $result_status['errors'];

  if ( count($errors) )
  { dlog('', "Error: cannot cancel customer %d", $customer->customer_id); }

  return $errors;
}

/**
 * process_customer
 * renew active subscriber according to account type and plan
 * @param Integer customer ID
 * @param Array customer reservation result
 * @param Object redis instance
 * @param Object customer DB record
 * @return Array processing result
 */
function process_customer($customer_id, $lock_result, $redis, $customer = NULL)
{
  $lock_id = $lock_result['lock_id'];

  /* process monthly renewal for a customer */

  $process_result = array(
    'errors'           => array(),
    'transient_errors' => array(),
    'rejection_errors' => array()
  );

  if ( ! $customer)
    $customer = getCustomerFromCustomerIdForMRCProc($customer_id);

  if (\Ultra\Lib\Flex::isFlexPlan($customer->cos_id))
  {
    $flexMRC = new \FlexMRC();
    return $flexMRC->processCustomer($customer, $lock_result);
  }

  $allowed_targets = get_ultra_plans_short();

  if ( $customer )
  {
    if ( $customer->days_before_expire > 0 )
    {
      return array(
        'transient_errors' => array(),
        'rejection_errors' => array(),
        'errors'           => array('Customer expiration date is in the future')
      );
    }

      // we only process Active customers
      if ( $customer->plan_state == 'Active' || ( ! empty($lock_result['status']) && $lock_result['status'] == 'RETRY_CC') )
      {
        $customer_options  = new \CustomerOptions($customer->CUSTOMER_ID);
        $process_mrc_error = null;

        // if customer has any options
        if (count($customer_options->options))
        {
          // check if it's a house account
          list( $process_mrc_error, $customer ) = process_house_account( $customer, $customer_options );

          // check BILLING.MRC_DEALER_PROMO
          list( $process_mrc_error, $customer ) = process_dealer_promo( $customer, $customer_options );

          // check BILLING.MRC_PROMO_PLAN
          list( $process_mrc_error, $customer ) = process_promo_plan( $customer, $customer_options );

          // check BILLING.MRC_BOGO_MONTH
          list ($process_mrc_error, $customer) = process_bogo_month($customer, $redis, $customer_options);

          // check BILLING.MRC_7_11
          list ($process_mrc_error, $customer) = process_promo_7_11($customer, $customer_options);

          // check BILLING.MRC_MULTI_MONTH
          list ($process_mrc_error, $customer) = process_multi_month($customer, $customer_options);

          // check BRAND.UV.SUBPLAN.$x
          list ($process_mrc_error, $customer) = process_uv_subplan($customer, $customer_options);
        }

        if ( $process_mrc_error )
        {
          dlog('',"Error: $process_mrc_error");
        }
        elseif ( $customer->MONTHLY_RENEWAL_TARGET === 'Cancel' )
        {
          $process_result['errors'] = process_customer_cancellation($customer,'MONTHLY_RENEWAL_TARGET was set to Cancel', FALSE);
        }
        else
        {
          $subscriber_owed_amount = compute_subscriber_owed_amount($customer);

          $result = array('success' => FALSE );

          if ( ( ! is_null($customer->MONTHLY_RENEWAL_TARGET) ) && ( $customer->MONTHLY_RENEWAL_TARGET != '' ) && ( ! in_array($customer->MONTHLY_RENEWAL_TARGET,$allowed_targets) ) )
          {
            dlog('', "MONTHLY_RENEWAL_TARGET for customer %d is not valid : %s", $customer_id, $customer->MONTHLY_RENEWAL_TARGET);

            $result['errors'] = "MONTHLY_RENEWAL_TARGET for customer $customer_id is not valid : ".$customer->MONTHLY_RENEWAL_TARGET;
          }
          else
          {
            dlog('', "invoking processCustomerCharge (balance:%d ; stored_value:%d)", $customer->BALANCE, $customer->stored_value);

            $result = \Ultra\Lib\Billing\processCustomerCharge($customer,$subscriber_owed_amount,$lock_result,'RECHARGE');

            $process_result['errors']           = $result['errors'];
            $process_result['transient_errors'] = $result['transient_errors'];
            $process_result['rejection_errors'] = $result['rejection_errors'];
          }

          if ( $result['success'] )
          {
            // the customer has enough funds

            // we reload customer because MONTHLY_RENEWAL_TARGET may have been changed by processCustomerCharge
            $customer = getCustomerFromCustomerIdForMRCProc($customer_id);

            $subscriber_owed_amount = compute_subscriber_owed_amount($customer);

            dlog('', "subscriber_owed_amount = %d ; BALANCE = %d ; stored_value = %d", $subscriber_owed_amount, $customer->BALANCE, $customer->stored_value);

            // if mint customer get multi month overlay info
            $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customer_id);

            if ( $customer->MONTHLY_RENEWAL_TARGET                                                         // the customer wishes to change plan
              && ( get_cos_id_from_plan( $customer->MONTHLY_RENEWAL_TARGET ) != $customer->COS_ID )
              && ( $subscriber_owed_amount <= ( ( $customer->BALANCE + $customer->stored_value ) * 100 ) ) // the customer's new plan is funded
              && ( ! $multiMonthOverlay || $multiMonthOverlay->MONTHS_REMAINING === 0 )
            )
            {
              // the customer has enough funds for the new plan

              $result = process_customer_transition_new_plan($customer);

              dlog('', "process_customer_transition_new_plan result = ".json_encode($result));
            }
            else
            {
              // the customer has enough funds for the current plan

              $renewalTransitionLabel = 'Monthly Renewal';

              // alternate transition labels for MINT renewals
              if (\Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID) == 'MINT')
              {
                $renewalTransitionLabel = ($multiMonthOverlay->MONTHS_REMAINING === 0)
                  ? 'Mint End Monthly Renewal'
                  : 'Mint Mid Monthly Renewal';
              }
              elseif (\Ultra\UltraConfig\isBPlan($customer->COS_ID))
              {
                $renewalTransitionLabel = ($multiMonthOverlay->MONTHS_REMAINING === 0)
                  ? 'End Monthly Renewal'
                  : 'Mid Monthly Renewal';
              }

              $result = transition_customer_state($customer, $renewalTransitionLabel);

              dlog('', "transition_customer_state result = ".json_encode($result));
            }

            $process_result['errors'] = $result['errors'];
          }
          elseif ( (( ! count($result['errors']) ) && ( ! count($result['transient_errors']) ))
                || ( count($result['transient_errors']) && in_array($result['transient_errors'][0], \Ultra\UltraConfig\getInsufficientFundsErrorCodes()) ) )
          {
            // the customer hasn't enough funds or CC charge failed badly: transition from Active to Suspend
            $result = process_customer_suspend($customer);

            $process_result['errors'] = $result['errors'];
          }
          else
          {
            $process_result['errors'] = $result['errors'];
          }
        }

      }
      else
      {
        $process_result['errors'][] = "Customer state is not Active";

        dlog('', "Customer state is not Active");
      }
  }
  else
  {
    $process_result['errors'][] = "Cannot load customer $customer_id";

    dlog('', "Cannot load customer %d", $customer_id);
  }

  logInfo("process_customer final result for customer $customer_id = " . jsonEncode($process_result));

  return $process_result;
}

/**
 * process_customer_transition_new_plan
 * transition from $customer->COS_ID to $customer->MONTHLY_RENEWAL_TARGET
 * @param Object customer DB record
 * @return Array result
 */
function process_customer_transition_new_plan($customer)
{

  $current_plan = get_plan_from_cos_id( $customer->COS_ID );
  $targetPlan   = $customer->MONTHLY_RENEWAL_TARGET;

  dlog('', "process_customer_transition_new_plan from %s to %s", $current_plan, $targetPlan);

  return transition_customer_plan($customer,$current_plan,$targetPlan,'Monthly');
}

/**
 * acquire_customer_list
 * legacy logic: collect a list of subscribers due to expire for processing
 * @param Array script arguments
 * @return Array with result members
 */
function acquire_customer_list($input_values)
{
  /* get list of customers id to be processed in this run */

  $result = array(
    'errors'               => array(),
    'customer_id_list'     => array(),
    'customer_plan_states' => array()
  );

  $query_params = array(
    'customer_id_list' => $input_values['customer_id_list'],
    'select_fields'    => array('customer_id'),
    'plan_state'       => 'ACTIVE',
    'expired'          => TRUE,
    'cancellation'     => FALSE,
    'retry'            => FALSE,
  );

  if ( isset($input_values['date']) && ( $input_values['date'] != 'today' ) )
  {
    $query_params['plan_expires_dd_mm_yyyy'] = $input_values['date'];
  }

  if ( $input_values['max_customers'] )
  {
    $query_params['sql_limit'] = $input_values['max_customers'];
  }

  if ( $input_values['skip_cc'] )
  {
    $query_params["monthly_cc_renewal"] = '0';
  }

  if ( $input_values['cancellation'] )
  {
    $query_params["cancellation"] = TRUE;
  }

  if ( $input_values['retry'] )
  {
    $query_params['retry'] = TRUE;
  }

  if ( $query_params["cancellation"] )
  {
    $result = get_customers_for_cancellation( $query_params );
  }
  elseif ($query_params['retry'])
  {
    $result = get_customer_ids_for_retry( $query_params );
  }
  else
  {
    $query_result = mssql_fetch_all_rows( logged_mssql_query( customers_for_monthly_charge_query( $query_params ) ) );

    if ( $query_result && is_array($query_result) )
    {
      foreach($query_result as $data)
      {
        $result['customer_id_list'][] = $data[0];
      }

      if ( count($result['customer_id_list']) > 1 )
      {
        shuffle( $result['customer_id_list'] );
      }
    }
  }

  return $result;
}

/**
 * get_customers_for_cancellation
 * find inactive subscribers which should be cancelled (e.g. denined port in, provisioned too long but unpaid, etc)
 * @param Array conditions
 * @return Array result
 */
function get_customers_for_cancellation( $query_params )
{
  dlog('', "(%s)", func_get_args());

  $result = array(
    'errors'               => array(),
    'customer_id_list'     => array(),
    'customer_plan_states' => array()
  );

  $data = array();

  $states = array('Promo Unused','Neutral', 'Provisioned', 'Suspended', 'Port-In Requested', 'Port-In Denied');

  foreach( $states as $status )
  {
    $query_params['status']   = $status;
    $query_params['days_ago'] = \Ultra\UltraConfig\monthly_charge_cancellation_delay( $query_params['status'] );

    $query = customers_for_cancellation_query( $query_params );

    $query_result = mssql_fetch_all_rows(logged_mssql_query($query));

    if ( $query_result && is_array( $query_result ) && count( $query_result ) )
    {
      foreach( $query_result as $row )
        $data[ $row[0] ] = $row[1];
    }
  }

  if ( count( $data ) )
  {
    ksort($data);

    while ( count($data) > $query_params['sql_limit'] )
      array_pop($data);

    foreach( $data as $id => $status )
    {
      $result['customer_id_list'][]     = $id;
      $result['customer_plan_states'][] = $status;
    }
  }
  else
  {
    dlog('',"No customers should be cancelled today");
  }

  dlog('',"data = %s",$data);
  dlog('',"result = %s",$result);

  return $result;
}

/**
 * input_values
 * read and adjust command line parameters
 * @returns Array of paramter => values
 */
function input_values()
{
  // default command line parameters and their types
  $defaults = array(
    'daily_init'        => array('type' => 'boolean', 'default' => FALSE),
    'date'              => array('type' => 'string',  'default' => 'today'),
    'cancellation'      => array('type' => 'boolean', 'default' => FALSE),
    'customer_id_list'  => array('type' => 'array',   'default' => array()),
    'groups'            => array('type' => 'array',   'default' => array(-1)),
    'max_customers'     => array('type' => 'integer', 'default' => 0),
    'retry'             => array('type' => 'boolean', 'default' => FALSE),
    'skip_cc'           => array('type' => 'boolean', 'default' => FALSE),
    'legacy'            => array('type' => 'boolean', 'default' => FALSE));

  // get script input
  $input_values = readNormalizedCommandLineArguments($defaults, FALSE);

  // validate date parameter
  if ($input_values['date'] != 'today' && ! validate_date_dd_mm_yyyy($input_values['date']))
  {
    logError("Invalid date input (format should be dd-mm-yyyy) {$input_values['date']}");
    exit(1);
  }

  dlog('', "input_values = ".json_encode($input_values));

  return $input_values;
}

/**
 * delay_due_to_transitions
 *
 * If the amount of OPEN transitions is more than $max_limit, we will sleep for 15 seconds.
 * We'll wait until the count will be less than $max_limit_low
 */
function delay_due_to_transitions()
{
  # If the amount of OPEN transitions is more than $max_limit, we will sleep for 30 seconds.
  # We'll wait until the count will be less than $max_limit_low

  $max_limit     = ( date('H') > 11 ) ? 280 : 240 ;
  $max_limit_low = ( date('H') > 11 ) ? 160 : 120 ;

  $htt_transition_log_open_count = htt_transition_log_open_count();

  while ( $htt_transition_log_open_count > $max_limit )
  {
    dlog('',"Sleeping for 15 seconds since we have $htt_transition_log_open_count open transitions (limit = $max_limit).");

    sleep(15);

    $max_limit = $max_limit_low;

    $htt_transition_log_open_count = htt_transition_log_open_count();
  }
}

/**
 * delay_due_to_settings
 *
 * check 'runner/monthly_charge/enabled' setting, if false sleep for 60 seconds
 */
function delay_due_to_settings()
{
  global $settings;

  $value = $settings->checkUltraSettings('runner/monthly_charge/enabled');

  while ( ( $value == 'FALSE' ) || ( $value == '0' ) )
  {
    dlog('',"Sleeping for 60 seconds since 'runner/monthly_charge/enabled' is set to FALSE");
    sleep(60);
    $value = $settings->checkUltraSettings('runner/monthly_charge/enabled');
  }
}

/**
 * build_cancellation_caption
 *
 * We are about to transition a customer to Cancelled from $plan_state , we need to provide a caption which will be stored in HTT_CUSTOMERS_NOTES.CONTENTS
 *
 * @return string
 */
function build_cancellation_caption( $plan_state )
{
  $caption = 'unknown';

  $days_ago = \Ultra\UltraConfig\monthly_charge_cancellation_delay( $plan_state );

  if ( $days_ago )
    $caption = 'Cancelled due to being in state "'.$plan_state.'" for '.$days_ago.' days or more';

  return $caption;
}

/**
 * process_uv_subplan
 *
 * process customers with customer option BRAND.UV.SUBPLAN.A
 *
 * @param  Object $customer
 * @param  Object $customer_options CustomerOptions
 * @return Array [string, $customer]
 */
function process_uv_subplan($customer, $customer_options)
{
  $error = '';

  // check if customer has brand option, sub plan A
  if ($subplan = $customer_options->getBrandUVSubplan())
  {
    // get customer data from htt_plan_tracker
    $customer_plan = mssql_fetch_all_objects(logged_mssql_query(
      htt_plan_tracker_select_query(array('customer_id' => $customer->CUSTOMER_ID))
    ));

    if ( ! count($customer_plan))
    {
      $error = 'Failed retrieving from HTT_PLAN_TRACKER for CUSTOMER_ID : ' . $customer->CUSTOMER_ID;
      \logit($error);
      return array($error, $customer);
    }
    else
    {
      $changedSubPlan = false;

      $customer_plan = $customer_plan[count($customer_plan) - 1];

      if ($subplan == 'A')
      {
        // get the last entry

        \logit('customer_plan: ' . json_encode($customer_plan));

        // if second renewal, add customer to BRAND.UV.SUBPLAN.B
        if ($customer_plan->period >= 3)
        {
          \logit('changing customer option to ' . BRAND_OPTION_SUBPLAN_B);

          // remove sub plan A and add sub plan B
          if ( \remove_customer_options($customer->CUSTOMER_ID, array(BRAND_OPTION_SUBPLAN_A)) )
          {
            $result = \set_brand_uv_subplan($customer->CUSTOMER_ID, 'B', $customer_plan->period);

            if ($result->is_failure())
            {
              $error = (count($errors = $result->get_errors()) > 0) ? $errors[0] : 'DB error';
              \logit("ERROR: $error");
            }
            else
              $changedSubPlan = true;
          }
          else
            \logit('DB error removing customer option');
        }
      }

      if ( ! $changedSubPlan)
      {
        \logit("Customer in subplan $subplan, udpate number of months since import");

        $result = \update_brand_uv_subplan($customer->CUSTOMER_ID, $subplan, $customer_plan->period);
        if ($result->is_failure())
        {
          $error = (count($errors = $result->get_errors()) > 0) ? $errors[0] : 'DB error updating customer option';
          \logit("ERROR: $error");
        }
      }
    }
  }

  // success, empty error
  return array($error, $customer);
}

/**
 * process_multi_month
 *
 * @param  Object $customer
 * @param  Object $customer_options CustomerOptions
 * @return Array [string, $customer]
 */
function process_multi_month($customer, $customer_options)
{
  $multi_month_info = $customer_options->multiMonthInfo();

  // (months_total => , months_left => )
  if ($multi_month_info && $multi_month_info['months_left'] > 0)
  {
    // Multimonth SIM data must belong to activation SIM
    $sim = get_htt_inventory_sim_from_iccid($customer->ACTIVATION_ICCID);
    if ( ! $sim)
    {
      $error = 'failed retrieving sim from HTT_INVENTORY_SIM';
      dlog('', $error . ' for CUSTOMER_ID : %d', $customer->CUSTOMER_ID);

      return array($error, $customer);
    }

    $reference  = BILLING_OPTION_MULTI_MONTH;
    $reason     = BILLING_OPTION_MULTI_MONTH;

    $result = func_courtesy_add_stored_value(
      array(
        'terminal_id' => basename(__FILE__),
        'amount'      => $sim->STORED_VALUE,
        'reference'   => $reference,
        'reason'      => $reason,
        'customer'    => $customer,
        'detail'      => __FUNCTION__
    ));

    if (count($result['errors']) > 0)
    {
      $error = 'func_courtesy_add_stored_value failed';
      dlog('', $error . ' : %s', $result['errors']);

      return array($error, $customer);
    }
    else
    {
      $customer = getCustomerFromCustomerIdForMRCProc($customer->CUSTOMER_ID);

      $result = deduct_multi_month($customer->CUSTOMER_ID);

      if ($result->is_failure())
      {
        $error = 'deduct_multi_month deduction failed';
        dlog('', $error . ' : %s', $result->get_errors());

        return array($error, $customer);
      }
      else
        dlog('', 'MULTI_MONTH deduction was successful for CUSTOMER_ID : %d', $customer->CUSTOMER_ID);
    }
  }

  return array('', $customer);
}

/**
 * process_promo_7_11
 *
 * @param  object $customer
 * @param  object $customer_options CustomerOptions
 * @return array [string, $customer]
 */
function process_promo_7_11($customer, $customer_options)
{
  $error = '';

  if ($customer_options->hasPromo711())
  {
    $incomm_inventory_sim = get_ultra_incomm_inventory_sim_by_customer_id($customer->CUSTOMER_ID);

    if (!count($incomm_inventory_sim))
    {
      dlog('','failed retrieving from INCOMM_INVENTORY_SIM for CUSTOMER ID : %d', $customer->CUSTOMER_ID);
      $error = 'failed retrieving from INCOMM_INVENTORY_SIM';
    }
    else
    {
      $gross_add_date = strtotime($customer->GROSS_ADD_DATE);

      if (!($gross_add_date > strtotime($incomm_inventory_sim->START_DATE) && $gross_add_date < strtotime($incomm_inventory_sim->EXPIRES_DATE)))
      {
        dlog('','Customer GROSS_ADD_DATE not between INCOMM_INVENTORY_SIM START_DATE and EXPIRES_DATE');
        $error = 'Customer GROSS_ADD_DATE not between INCOMM_INVENTORY_SIM START_DATE and EXPIRES_DATE';
      }
      else
      {
        $customer_plan = mssql_fetch_all_objects(logged_mssql_query(
          htt_plan_tracker_select_query(array('customer_id' => $customer->CUSTOMER_ID))));

        if (!count($customer_plan))
        {
          dlog('', 'failed retrieving from HTT_PLAN_TRACKER for CUSTOMER_ID : %d', $customer->CUSTOMER_ID);
          $error = 'failed retrieving from HTT_PLAN_TRACKER';
        }
        else
        {
          $customer_plan = $customer_plan[count($customer_plan) - 1];

          if ($customer_plan->period >= 1 && $customer_plan->period <= 2)
          {
            $agent_name = 'monthly-charge.php';
            $reference  = 'BILLING.MRC_7_11';
            $reason     = 'BILLING.MRC_7_11';

            $result = func_courtesy_add_stored_value(
              array(
                'terminal_id' => $agent_name,
                'amount'      => $incomm_inventory_sim->STORED_VALUE,
                'reference'   => $reference,
                'reason'      => $reason,
                'customer'    => $customer,
                'detail'      => __FUNCTION__
            ));

            if (count($result['errors']) > 0)
            {
              dlog('', "func_courtesy_add_stored_value errors : %s", $result['errors']);
              $error = "func_courtesy_add_stored_value failed";
            }
            else
            {
              $customer = getCustomerFromCustomerIdForMRCProc( $customer->CUSTOMER_ID );

              if ($customer_plan->period == 2)
              {
                // remove PROMO 7_11 customer option
                $result = \redeemed_promo_7_11($customer->CUSTOMER_ID);

                if ($result->is_failure())
                {
                  dlog('', "redeemed_promo_7_11 failed - %s", $result->get_errors());
                  $error = 'redeemed_promo_7_11 failed';
                }
                else
                {
                  dlog('', 'PROMO_7_11 redemption was successful for CUSTOMER_ID : %d', $customer->CUSTOMER_ID);
                }
              }
            }
          }
          else
          {
            $error = 'CUSTOMER ' . $customer->CUSTOMER_ID . ' has 7-11 promo option but is outside plan period eligibility';
            dlog('', $error);
          }
        }
      }
    }
  }

  return array($error, $customer);
}

/**
 * process_bogo_month
 * 
 * @param  object $customer
 * @param  object $redis
 * @param  object $customer_options CustomerOptions
 * @return array [string, $customer]
 */
function process_bogo_month($customer, $redis, $customer_options)
{
  $error = '';

  $billing_mrc_bogo_month = $customer_options->bogoMonthInfo();

  if (count($billing_mrc_bogo_month))
  {
    // if $billing_mrc_bogo_month[1] == 0, customer is leaving second month
    // remove the option and return
    if ($billing_mrc_bogo_month[1] == 0)
    {
      $result = \redeemed_bogo_month($customer->CUSTOMER_ID);
      return array($error, $customer);
    }

    $incomm_inventory_sim = get_ultra_incomm_inventory_sim_by_customer_id($customer->CUSTOMER_ID);

    if (!count($incomm_inventory_sim))
    {
      dlog('','failed retrieving from INCOMM_INVENTORY_SIM for CUSTOMER ID : %d', $customer->CUSTOMER_ID);
      $error = 'failed retrieving from INCOMM_INVENTORY_SIM';
    }
    else
    {
      $gross_add_date = strtotime($customer->GROSS_ADD_DATE);

      if (!($gross_add_date > strtotime($incomm_inventory_sim->START_DATE) && $gross_add_date < strtotime($incomm_inventory_sim->EXPIRES_DATE)))
      {
        dlog('','Customer GROSS_ADD_DATE not between INCOMM_INVENTORY_SIM START_DATE and EXPIRES_DATE');
        $error = 'Customer GROSS_ADD_DATE not between INCOMM_INVENTORY_SIM START_DATE and EXPIRES_DATE';
      }
      else
      {
        $customer_plan = mssql_fetch_all_objects(logged_mssql_query(
          htt_plan_tracker_select_query(array('customer_id' => $customer->CUSTOMER_ID))));

        if (!count($customer_plan))
        {
          dlog('', 'failed retrieving from HTT_PLAN_TRACKER for CUSTOMER_ID : %d', $customer->CUSTOMER_ID);
          $error = 'failed retrieving from HTT_PLAN_TRACKER';
        }
        else
        {
          $customer_plan = $customer_plan[count($customer_plan) - 1];

          if ($customer_plan->period == 1)
          {
            $agent_name = 'monthly-charge.php';
            $reference  = 'BILLING.MRC_BOGO_MONTH';
            $reason     = 'BILLING.MRC_BOGO_MONTH';

            $result = func_courtesy_add_stored_value(
              array(
                'terminal_id' => $agent_name,
                'amount'      => $billing_mrc_bogo_month[1],
                'reference'   => $reference,
                'reason'      => $reason,
                'customer'    => $customer,
                'detail'      => __FUNCTION__
            ));

            if (count($result['errors']) > 0)
            {
              dlog('', "func_courtesy_add_stored_value errors : %s", $result['errors']);
              $error = "func_courtesy_add_stored_value failed";
            }
            else
            {
              $customer = getCustomerFromCustomerIdForMRCProc( $customer->CUSTOMER_ID );

              // remove BOGO customer option
              $result = \update_customer_option($customer->CUSTOMER_ID, $billing_mrc_bogo_month[0], 0);

              if ($result->is_failure())
              {
                $error = 'update_customer_option failed';
                dlog('', "$error - %s", $result->get_errors());
              }
              else
              {
                dlog('', 'BOGO redemption was successful for CUSTOMER_ID : %d', $customer->CUSTOMER_ID);

                $sendSMSResult = funcSendSMSBOGORedeemed(array('customer_id' => $customer->CUSTOMER_ID), $billing_mrc_bogo_month[0]);
                if (count($sendSMSResult['errors']))
                {
                  dlog('', 'funcSendSMSBogoRedeemed ERROR - %s', $sendSMSResult['errors'][0]);
                  $error = 'funcSendSMSBogoRedeemed ERROR - ' . $sendSMSResult['errors'][0];
                }

                // renewal cost check
                $renewal_cost = substr($customer->MONTHLY_RENEWAL_TARGET, 1, strlen($customer->MONTHLY_RENEWAL_TARGET) - 1);

                if ($renewal_cost > $customer->BALANCE + $customer->stored_value)
                {
                  $owed_amount = $renewal_cost - $customer->stored_value + $customer->BALANCE;

                  // ttl = 2 days
                  $redis->set('ultra/bogo_suspended/customer_id/' . $customer->CUSTOMER_ID, $billing_mrc_bogo_month[1] . ',' . $owed_amount, 60 * 60 * 24, 172800);

                  $result = suspend_account($customer, array('customer_id' => $customer->customer_id));
                  if (count($result['errors']))
                  {
                    dlog('', 'suspend_account ERROR - %s', $result['errors'][0]);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  return array($error, $customer);
}

/**
 * process_promo_plan
 *
 * Customers associated with Promo Plans should not be expired due to lack of balance.
 * Instead courtesy credit should be added and the customer renews their current plan.
 *
 * @param  Object $customer
 * @param  Object $customer_options CustomerOptions
 * @return Array [string, $customer]
 */
function process_promo_plan( $customer, $customer_options )
{
  $error = '';

  $billing_mrc_promo_plan_info = $customer_options->promoPlanInfo();

  if ( $billing_mrc_promo_plan_info && is_array( $billing_mrc_promo_plan_info ) )
  {
    dlog('',"billing_mrc_promo_plan_info = %s",$billing_mrc_promo_plan_info);

    if ( $billing_mrc_promo_plan_info['months'] > 0 )
    {
      $agent_name = 'monthly-charge.php';
      $amount     = ( substr($billing_mrc_promo_plan_info['plan'],-2) * 100 );
      $reason     = 'BILLING.MRC_PROMO_PLAN';
      $reference  = implode(',',array( $billing_mrc_promo_plan_info['plan'] , $billing_mrc_promo_plan_info['ultra_promotional_plans_id'] , $billing_mrc_promo_plan_info['months'] ) );

      $result = func_courtesy_add_stored_value(
        array(
          'terminal_id'      => $agent_name,
          'amount'           => ($amount/100),
          'reference'        => $reference,
          'reason'           => $reason,
          'customer'         => $customer,
          'detail'           => __FUNCTION__
        )
      );

      if ( count($result['errors']) > 0 )
      {
        dlog('',"func_courtesy_add_stored_value errors : %s",$result['errors']);

        $error = "func_courtesy_add_stored_value failed";
      }
      else
      {
        // decrement promo plan months by 1
        $result = \decrement_billing_mrc_promo_plan( $customer->CUSTOMER_ID );

        if ( $result->is_failure() )
          dlog('',"decrement_billing_mrc_promo_plan failed - %s",$result->get_errors());

        $customer = getCustomerFromCustomerIdForMRCProc( $customer->CUSTOMER_ID );
      }
    }
    else
      dlog('',"billing_mrc_promo_plan expired");
  }

  return array( $error , $customer );
}

/**
 * process_house_account
 *
 * Customers associated with House Accounts should not be expired due to lack of balance.
 * Instead courtesy credit should be added and the customer renews their current plan.
 *
 * @param  Object  $customer
 * @param  Object $customer_options CustomerOptions
 * @return Array [string, $customer]
 */
function process_house_account( $customer, $customer_options )
{
  $error = '';

  if ( count($customer_options->options) && isset($customer_options->options[MRC_HOUSE_ACCOUNT]) )
  {
    // $ultra_customer_options[1] contains the employee name

    $plan   = get_plan_from_cos_id( $customer->COS_ID );
    $amount = ( substr($plan, -2) * 100 );

    $result = func_courtesy_add_stored_value(
      array(
        'terminal_id'      => 'monthly-charge.php',
        'amount'           => ($amount/100),
        'reference'        => $customer_options->options[MRC_HOUSE_ACCOUNT],
        'reason'           => MRC_HOUSE_ACCOUNT,
        'customer'         => $customer,
        'detail'           => __FUNCTION__
      )
    );

    if ( count($result['errors']) > 0 )
    {
      dlog('',"func_courtesy_add_stored_value errors : %s",$result['errors']);

      $error = "func_courtesy_add_stored_value failed";
    }
    else
      $customer = getCustomerFromCustomerIdForMRCProc( $customer->CUSTOMER_ID );
  }

  return array( $error , $customer );
}

/**
 * process_dealer_promo
 *
 * @param  Object $customer
 * @param  Object $customer_options CustomerOptions
 * @return Array [string, $customer]
 */
function process_dealer_promo( $customer, $customer_options )
{
  $error = '';

  if ( count($customer_options->options) && isset($customer_options->options[BILLING_OPTION_DEMO_LINE]) )
  {
    // $ultra_customer_options[1] contains the dealer id
    // Customer should not be expired due to lack of balance.
    // Instead the value of the current plan should be added to stored value.

    $plan = get_plan_from_cos_id( $customer->COS_ID );

    $agent_name = 'monthly-charge.php';
    $amount     = ( substr($plan,-2) * 100 );
    $reason     = BILLING_OPTION_DEMO_LINE;

    $result = func_courtesy_add_stored_value(
      array(
        'terminal_id'      => $agent_name,
        'amount'           => ($amount/100),
        'reference'        => $customer_options->options[BILLING_OPTION_DEMO_LINE],
        'reason'           => $reason,
        'customer'         => $customer,
        'detail'           => __FUNCTION__
      )
    );

    if ( count($result['errors']) > 0 )
    {
      dlog('',"func_courtesy_add_stored_value errors : %s",$result['errors']);

      $error = "func_courtesy_add_stored_value failed";
    }
    else
    {
      $customer = getCustomerFromCustomerIdForMRCProc( $customer->CUSTOMER_ID );
    }
  }

  return array( $error , $customer );
}

/**
 * delay_due_to_makeitso
 * delay execution due to large number of PENDING MakeItSo
 * @see https://issues.hometowntelecom.com:8443/browse/MVNO-2469
 */
function delay_due_to_makeitso()
{
  try
  {
    // compute limits based on PT time
    $max_limit     = date('H') > 11 ? 280 : 240;
    $max_limit_low = date('H') > 11 ? 240 : 200;
    $sleep = 16;

    // connect to ULTRA_ACC
    $connection = \Ultra\Lib\DB\ultra_acc_connect();
    if (! $connection)
      throw new Exception('failed to connect to ULTRA_ACC database');

    // prepare SQL
    $table = 'MAKEITSO_QUEUE';
    if ( ! $sql = Ultra\Lib\DB\makeSelectQuery($table, NULL, 'count(*) AS COUNT', array('STATUS' => 'PENDING'), NULL, NULL, TRUE))
      throw new Exception('failed to prepare MISO statement');

    do
    {
      // get count of PENDING MISO
      $result = mssql_fetch_all_objects(logged_mssql_query($sql));
      if (empty($result) || ! isset($result[0]->COUNT))
        throw new Exception('failed to get MISO count');

      // delay if PENDING > $max_limit, resume when PENDING < $max_limit_low
      if ($result[0]->COUNT > $max_limit)
      {
        dlog('', "sleeping for $sleep secs due to PENDING MISO {$result[0]->COUNT} > $max_limit");
        sleep($sleep);
        $max_limit = $max_limit_low; // reset to lower threshold
      }
    }
    while ($result[0]->COUNT > $max_limit);

  }
  catch (\Exception $e)
  {
    dlog('', 'EXCEPTION: ' . $e->getMessage());
  }

  // reconnect back to ULTRA_DATA
  teldata_change_db();
}

/**
 * calculate_runtime
 * caclulate our maximum execution timestamp
 * @return Integer UNIX time
 */
function calculate_runtime()
{
  $time_allowed_minutes = \Ultra\UltraConfig\monthly_charge_exec_minutes();
  return time() + ($time_allowed_minutes * 60);
}

/**
 * delay_execution
 * pause runner due to various obstacles
 */
function delay_execution()
{
  delay_due_to_settings();
  // delay_due_to_transitions();
  // delay_due_to_makeitso();
}


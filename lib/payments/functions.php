<?php


use Ultra\Container\AppContainer;
use Ultra\Customers\Customer;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Lib\Flex;
use Ultra\Taxes\TaxesAndFeesCalculator;

include_once('db.php');
include_once('db/accounts.php');
include_once('db/billing.php');
include_once('db/incomm.php');
include_once('db/htt_billing_history.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('db/htt_inventory_pin.php');
include_once('db/ipcommand.php');
include_once('db/webcc.php');
include_once('fields.php');
include_once('fraud.php');
include_once('lib/state_machine/functions.php');
include_once('lib/util-common.php');
require_once('Ultra/Lib/BoltOn/functions.php');
include_once('Ultra/Lib/CC/Queue.php');
include_once('Ultra/Lib/inComm/Incomm.php');
require_once 'Ultra/Lib/SureTax.php';
require_once 'classes/SalesTaxCalculator.php';

require_once 'Ultra/Orders/Repositories/Mssql/OrderRepository.php';
require_once 'Ultra/Customers/Repositories/Mssql/CustomerRepository.php';
require_once 'Ultra/CreditCards/Repositories/Mssql/CreditCardRepository.php';
require_once 'Ultra/Lib/Services/OnlineSalesAPI.php';
require_once 'classes/Flex.php';

// financial constants
define('MAX_ACCOUNT_BALANCE', 20000); // maximum total account balance in cents
define('ULTRA_PIN_LENGTH', 18);

# synopsis


# $r = func_validate_pin_cards( array( 'pin_list' => array("test","12345678901234567890") ) ); print_r($r);


# note: parameters validation is to be performed before invoking those functions #


## == functions == ##


# returns TRUE if $customer has at least $amount (in cents) in his wallet, FALSE otherwise
function func_assert_balance($customer_id, $amount)
{
  $return = array( 'errors' => array() , 'success' => FALSE );

  $customer = get_account_from_customer_id( $customer_id, array( 'BALANCE' ) );
  if ( ! $customer )
    $return['errors'][] = 'Customer not found';  
  else
    $return['success'] = ! ! ( $amount <= ( $customer->BALANCE * 100 ) );
  
  if ( ! $return['success'] )
    $return['errors'][] = 'Not enough money in customer\'s wallet.';
  
  return $return;
}

/**
 * func_spend_from_balance_explicit
 *
 * For ORANGE SIMS we have to retrieve the cos_id if it's ``Neutral``
 */
function func_spend_from_balance_explicit($customer_id, $amount, $detail, $reason, $reference, $source, $reference_source, $target_cos_id = NULL, $type = null)
{
  # wrapper of func_spend_from_balance which can be used in actions definitions

  sleep(1);

  $errors = array();

  if (!isset($customer_id))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: missing parameter customer_id';

  if (!isset($amount))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: missing parameter amount';

  if (!isset($detail))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: missing parameter detail';

  if (!isset($reason))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: missing parameter reason';

  if (!isset($reference))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: missing parameter reference';

  if (!isset($source))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: missing parameter source';

  if (!isset($reference_source))
    $errors[] = 'ERR_API_INVALID_ARGUMENTS: missing parameter reference_source';

  if (count($errors))
    return array('success' => FALSE, 'errors' => $errors);

  $customer = get_customer_from_customer_id($customer_id);

  $standby_cos_id = get_cos_id_from_plan('STANDBY');

  // check if cos_id is ``Neutral``
  if ( $customer->cos_id == $standby_cos_id )
  {
    $sim_data = get_htt_inventory_sim_from_iccid($customer->current_iccid);

    // check if customer SIM is orange
    if ( $sim_data && $sim_data->PRODUCT_TYPE === 'ORANGE' )
      $target_cos_id = get_cos_id_from_plan( func_get_orange_plan_from_sim_data($sim_data) );
  }

  return func_spend_from_balance(
    array(
      'customer'    => $customer,
      'amount'      => $amount, # in dollars
      'detail'      => $detail,
      'reason'      => $reason,
      'reference'   => $reference,
      'source'      => $source,
      'target_cos_id'    => $target_cos_id,
      'reference_source' => $reference_source,
      'commissionable' => $type == 'immediate' ? 1 : 0
    )
  );
}

function func_apply_balance_to_stored_value($params)
{
  # $amount will be substracted from BALANCE and added to STORED_VALUE

  $amount         = $params['amount']; # in dollars
  $reason         = $params['reason']; # "reason" => "description"
  $reference      = $params['reference'];
  $source         = $params['source'];
  $detail         = $params['detail'];
  $customer       = $params['customer'];

  $return = array( 'errors' => array() , 'success' => FALSE );

  if ( $amount == 0 )
    return array( 'errors' => array() , 'success' => TRUE );

  $is_commissionable = 0;

  # update ACCOUNTS.BALANCE
  $accounts_update_query = accounts_update_query(
    array(
      'subtract_from_balance'   => $amount,
      'customer_id'             => $customer->CUSTOMER_ID
    )
  );

  # update HTT_CUSTOMERS_OVERLAY_ULTRA.stored_value += $amount
  $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
    array(
      'balance_to_add' => $amount,
      'customer_id'    => $customer->CUSTOMER_ID
    )
  );

  $return = exec_queries_in_transaction( array ( $accounts_update_query , $htt_customers_overlay_ultra_update_query ) );

  if ( $return['success'] )
  {
    # insert into HTT_BILLING_HISTORY

    $reference_source = ''; # for transaction uuids

    if ( isset( $params['reference_source'] ) && $params['reference_source'] )
    {
      $reference_source = $params['reference_source'];
    }

    $history_params = array(
      "customer_id"            => $customer->CUSTOMER_ID,
      "date"                   => 'now',
      "cos_id"                 => $customer->COS_ID,
      "entry_type"             => 'SPEND',
      "stored_value_change"    => $amount,
      "balance_change"         => - $amount,
      "package_balance_change" => 0,
      "charge_amount"          => 0,
      "reference"              => $reference,
      "reference_source"       => $reference_source,
      "detail"                 => $detail,
      "description"            => $reason,
      "result"                 => 'COMPLETE',
      "source"                 => $source,
      "is_commissionable"      => $is_commissionable
    );

    $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

    if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
    {
      $return["success"] = TRUE;
    }
    else
    { $return['errors'][] = "ERR_API_INTERNAL: DB error)"; }
  }

  return $return;
}

function func_spend_from_balance($params)
{
  #  A customer ID is passed in as well as a balance to remove, and the reason (DETAIL,DESCRIPTION) for the spend.. optional Reference for a transitionID to be stored inREFERENCE (set REFERENCE_SOURCE to TRANSITION then), 
  #  Verify that there is enough in customer.balance to spend.
  #  account.balance -=  balance_to_take.
  #  A row is inserted into HTT_BILLING_HISTORY, noting the customer_id, the balance removed (-CHARGE_AMOUNT and- BALANCE_CHANGE), and the reason.
  #  Make sure to check the HTT_BILLING_HISTORY section of API Database Structure#viewer-b64b12cc=1

  # "store_zipcode","store_id","clerk_id","terminal_id" nullable

  $amount         = $params['amount']; # in dollars
  $reason         = $params['reason']; # "reason" => "description"
  $reference      = $params['reference'];
  $source         = $params['source'];
  $detail         = $params['detail'];

  $customer = NULL;

  if ( isset($params['customer']) )
  {
    $customer = $params['customer'];
  }
  else
  {
    $customer = get_customer_from_customer_id($params['customer_id']);
  }

  $return = array( 'errors' => array() , 'success' => FALSE );

  if ( ! $customer )
  {
    $return["errors"][] = "ERR_API_INVALID_ARGUMENTS: customer not found.";

    return $return;
  }

  dlog('',"amount = $amount ; customer balance = ".$customer->BALANCE." ; customer stored_value = ".$customer->stored_value);

  $customer_id    = $customer->CUSTOMER_ID;
  $cos_id         = $customer->COS_ID; # customer COS_ID at time of transaction
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) # COS_ID associated with the reason of the transaction
  {
    $cos_id = $params['target_cos_id'];
  }

  # a) should refer to webcc in case of a CC - see htt_billing_history docs. b) should be epay and reference is epay trans id in case of epay
  $reference_source = ''; # for transaction uuids

  if ( isset( $params['reference_source'] ) && $params['reference_source'] )
  {
    $reference_source = $params['reference_source'];
  }

  $is_commissionable = ( isset( $params['commissionable'] ) && $params['commissionable'] ) ? 1 : 0 ;

  if ( $amount <= $customer->BALANCE )
  {
    # update ACCOUNTS

    $accounts_update_query = accounts_update_query(
      array(
        'subtract_from_balance'   => $amount,
        'customer_id'             => $customer_id,
      )
    );

    if ( is_mssql_successful(logged_mssql_query($accounts_update_query)) )
    {
      # insert into HTT_BILLING_HISTORY

      $history_params = array(
        "customer_id"            => $customer_id,
        "date"                   => 'now',
        "cos_id"                 => $cos_id,
        "entry_type"             => 'SPEND',
        "stored_value_change"    => 0,
        "balance_change"         => $amount == 0 ? 0 : - $amount,
        "package_balance_change" => 0,
        "charge_amount"          => 0,
        "reference"              => $reference,
        "reference_source"       => $reference_source,
        "detail"                 => $detail,
        "description"            => $reason,
        "result"                 => 'COMPLETE',
        "source"                 => $source,
        "is_commissionable"      => $is_commissionable
      );

      foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
      {
        if ( isset( $params[$nullable_field] ) )
        {
          $history_params[$nullable_field] = $params[$nullable_field];
        }
      }

      $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

      if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
      {
        $return["success"] = TRUE;
      }
      else
      { $return['errors'][] = "ERR_API_INTERNAL: DB error)"; }

    }
    else
    { $return['errors'][] = "ERR_API_INTERNAL: DB error)"; }

  }
  else
  {
    $return["errors"][] = "ERR_API_INVALID_ARGUMENTS: insufficient funds.";
  }

  return $return;
}


/**
 * func_add_balance
 *
 * A customer ID is passed in as well as an amount to add, balance_to_add.
 * account.balance +=  balance_to_add
 * A row is inserted into HTT_BILLING_HISTORY, noting the customer_id, the balance added (AMOUNT), the reason (if any), the source of the funds, the destination.
 * Input parameters:
 *  - 'customer'
 *  - 'amount'
 *  - 'reason'
 *  - 'reference'
 *  - 'source'
 *  - 'reactivate_now' (not needed)
 *  - 'entry_type'     (not needed)
 *  - 'target_cos_id'  (not needed)
 *  - 'pay_quicker'    (not needed)
 */
function func_add_balance($params)
{
  $customer_id    = $params['customer']->CUSTOMER_ID;
  $amount         = $params['amount'];
  $reason         = $params['reason'];    // $reason is caller name (API) -> $detail
  $reference      = $params['reference'];
  $source         = $params['source']; # 'EPAY' or 'WEBCC'
  $is_commissionable = 1;

  $cos_id         = $params['customer']->COS_ID; # customer COSID at time of transaction
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) # COS_ID associated with the reason of the transaction
  {
    $cos_id = $params['target_cos_id'];
  }

  $entry_type        = 'LOAD';
  if ( isset( $params['entry_type'] ) )
    $entry_type = $params['entry_type'];

  $reactivate_now = TRUE;
  if ( isset( $params['reactivate_now'] ) && ( $params['reactivate_now'] === FALSE ) )
    $reactivate_now = FALSE;

  $pay_quicker = ( isset( $params['pay_quicker'] ) && $params['pay_quicker'] ) ? $params['pay_quicker'] : FALSE ;

  $return = array( 'errors' => array() , 'success' => FALSE , 'activated' => FALSE );

  $accounts_update_query = accounts_update_query(
    array(
      'add_to_balance'          => $amount,
      'customer_id'             => $customer_id,
      'last_recharge_date_time' => 'now'
    )
  );

  if ( is_mssql_successful(logged_mssql_query($accounts_update_query)) )
  {
    $description = 'Wallet Balance Added';
    try
    {
      if ( $pay_quicker )
      {
        $description .= ' (PQ)';
        if ( ! start_mssql_transaction() )
          throw new \Exception( "Error while starting a DB transaction" );
      }

      $paymentDate = mssql_fetch_all_objects(
        logged_mssql_query(
          "SELECT TOP 1 CREATED 
          FROM HTT_BILLING_ACTIONS 
          WHERE CUSTOMER_ID = $customer_id 
          AND PRODUCT_ID = 'ULTRA_ADD_BALANCE' 
          ORDER BY CREATED DESC"
        )
      );

      if (!empty($paymentDate)) {
        $grossAddDate = mssql_fetch_all_objects(
          logged_mssql_query(
            htt_customers_overlay_ultra_select_query([
              'select_fields' => ['GROSS_ADD_DATE'],
              'customer_id'=> $customer_id
            ])
          )
        );

        $paymentDate = (new \DateTime($paymentDate[0]->CREATED))->getTimestamp();
        $grossAddDate = empty($grossAddDate) ? time() : (new \DateTime($grossAddDate[0]->GROSS_ADD_DATE))->getTimestamp();

        if ($source == 'EPAY' && $entry_type == 'LOAD' && $paymentDate < $grossAddDate) {
          $is_commissionable = 0;
        }
      }

      $history_params = array(
        "customer_id"            => $customer_id,
        "date"                   => 'now',
        "cos_id"                 => $cos_id,
        "entry_type"             => $entry_type,
        "stored_value_change"    => '0',
        "balance_change"         => $amount,
        "package_balance_change" => '0',
        "charge_amount"          => $amount,
        "reference"              => $reference,
        "reference_source"       => get_reference_source($source),
        "detail"                 => $reason,
        "description"            => $description,
        "result"                 => 'COMPLETE',
        "source"                 => $source,
        "is_commissionable"      => $is_commissionable
      );

      foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
        if ( isset( $params[$nullable_field] ) )
          $history_params[$nullable_field] = $params[$nullable_field];

      $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

      if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
        throw new \Exception( "Error while inserting into htt_billing_history (".mssql_get_last_message().")" );

      if ( $pay_quicker )
      {
        // MVNO-2478: add PQ specific additional row to HTT_BILLING_HISTORY
        $history_params['entry_type']        = 'LOAD';
        $history_params['balance_change']    = 0;
        $history_params['charge_amount']     = 0;
        $history_params['reference_select']  = htt_billing_history_reference_select( $customer_id , $reference ); // the HTT_BILLING_HISTORY.TRANSACTION_ID of the first row
        $history_params['reference_source']  = get_reference_source('EPAYPQ');
        $history_params['detail']            = $reason;
        $history_params['description']       = 'Pay Quicker SKU ('.$pay_quicker.')';
        $history_params['source']            = $source;
        $history_params['is_commissionable'] = 0;

        $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

        if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
          throw new \Exception( "Error while inserting into htt_billing_history (".mssql_get_last_message().")" );

        if ( ! commit_mssql_transaction() )
          throw new \Exception( "Error while committing a DB transaction" );
      }

      $return["success"] = TRUE;

      // try to activate customer if Provisioned, non-Flex plans
      if (!Flex::isFlexPlan($cos_id)) {
        $customer = $params['customer'];

        $context = array(
          'customer_id' => $customer->CUSTOMER_ID
        );

        $state = internal_func_get_state(array(
          'states' => NULL,
          'context' => $context
        ));

        $result_status = activate_provisioned_account($state, $customer, $context);
        if ($result_status['errors'] && count($result_status['errors']))
          dlog('', "Errors after activate_provisioned_account: " . json_encode($result_status['errors']));

        if (!$result_status['activated']) {
          $result_status = reactivate_suspended_account($state, $customer, $context, $reactivate_now);
          if ($result_status['errors'] && count($result_status['errors']))
            dlog('', "Errors after reactivate_suspended_account: " . json_encode($result_status['errors']));
        }

        if (isset($result_status['activated']))
          $return['activated'] = $result_status['activated'];

        if (isset($result_status['reactivated']))
          $return['activated'] = $result_status['reactivated'];
      } else {
        $return['activated'] = false;
      }
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage() );

      $return['errors'][] = $e->getMessage();
    }
  }
  else
  { $return['errors'][] = "Error while updating accounts (".mssql_get_last_message().")"; }

  return $return;
}


function func_add_ild_minutes($params)
{
  #1  A customer ID is passed in as well as an amount to add, balance_to_add.
  #2  account.package_balance1 +=  balance_to_add*100.
  #3  A row is inserted into HTT_BILLING_HISTORY, noting the customer_id, the balance added (QUANTITY), the reason (if any), the source of the funds.

  $customer_id       = $params['customer']->CUSTOMER_ID;
  $amount            = $params['amount'];
  $reference         = $params['reference'];
  $source            = $params['source']; # 'EPAY' or 'CUSTOMERSERVICE' or 'WEBCC'
  $is_commissionable = isset($params['commissionable']) ? $params['commissionable'] : 0; # add minutes not commissionable

  $cos_id         = isset( $params['target_cos_id']  ) ? $params['target_cos_id']  : $params['customer']->COS_ID ;
  $bonus          = isset( $params['bonus']          ) ? $params['bonus']          : 0 ;
  $balance_change = isset( $params['balance_change'] ) ? $params['balance_change'] : 0 ;

  $return = array( 'errors' => array() , 'success' => FALSE );

  if ( $balance_change )
  {
    // debit the customer's balance
    $accounts_update_query = accounts_update_query(
      array(
        'add_to_balance'          => ( - $balance_change ),
        'customer_id'             => $customer_id
      )
    );

    if ( ! is_mssql_successful(logged_mssql_query($accounts_update_query)) )
      return array( 'errors' => array("Error while updating accounts (".mssql_get_last_message().")") , 'success' => FALSE );
  }

  $accounts_update_query = accounts_update_query(
    array(
      'add_to_package_balance1' => ( $amount * ( 1 + $bonus ) ) * 10 ,
      'customer_id'             => $customer_id
    )
  );

  if ( is_mssql_successful(logged_mssql_query($accounts_update_query)) )
  {
    # a) should refer to webcc in case of a CC - see htt_billing_history docs. b) should be epay and reference is epay trans id in case of epay
    $reference_source = get_reference_source($source);

    $history_params = array(
      "customer_id"            => $customer_id,
      "date"                   => 'now',
      "cos_id"                 => $cos_id,
      "entry_type"             => 'SPEND',
      "stored_value_change"    => 0,
      "balance_change"         => ( - $balance_change ),
      "package_balance_change" => ( ( $amount * ( 1 + $bonus ) ) * 1000 * 60 ),
      "charge_amount"          => 0,
      "reference"              => $reference,
      "reference_source"       => $reference_source,
      "detail"                 => $params['reason'],
      "description"            => empty($params['description']) ? 'INTL Purchase' : $params['description'],
      "result"                 => 'COMPLETE',
      "source"                 => $source,
      "is_commissionable"      => $is_commissionable
    );

    foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
      if ( isset( $params[$nullable_field] ) )
        $history_params[$nullable_field] = $params[$nullable_field];

    $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

    if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
      $return["success"] = TRUE;
    else
      $return['errors'][] = "Error while inserting into htt_billing_history (".mssql_get_last_message().")";
  }
  else
  { $return['errors'][] = "Error while updating accounts (".mssql_get_last_message().")"; }

  return $return;
}


/**
 * func_add_stored_value
 *
 * A customer ID is passed in as well as an amount to add, balance_to_add.
 * stored_value += balance_to_add
 * A row is inserted into HTT_BILLING_HISTORY, noting the customer_id, the balance added (AMOUNT), the reason (if any), the source of the funds, the destination.
 * Input parameters:
 *  - 'customer'
 *  - 'amount'
 *  - 'reason'
 *  - 'reference'
 *  - 'source'
 *  - 'reactivate_now' (not needed)
 *  - 'entry_type'     (not needed)
 *  - 'target_cos_id'  (not needed)
 *  - 'pay_quicker'    (not needed)
 */
function func_add_stored_value($params)
{
  $customer_id       = $params['customer']->CUSTOMER_ID;
  $amount            = $params['amount'];
  $reason            = $params['reason'];   // $reason is caller name (API) -> $detail
  $reference         = $params['reference'];
  $source            = $params['source']; # 'EPAY' or 'WEBCC'
  $is_commissionable = 1;

  $entry_type        = 'LOAD';

  if ( isset( $params['entry_type'] ) )
    $entry_type = $params['entry_type'];

  $cos_id            = $params['customer']->COS_ID; # customer COSID at time of transaction
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) # COS_ID associated with the reason of the transaction
    $cos_id = $params['target_cos_id'];

  $reactivate_now = TRUE;
  if ( isset( $params['reactivate_now'] ) && ( $params['reactivate_now'] === FALSE ) )
    $reactivate_now = FALSE;

  $pay_quicker = ( isset( $params['pay_quicker'] ) && $params['pay_quicker'] ) ? $params['pay_quicker'] : FALSE ;

  $return = array( 'errors' => array() , 'success' => FALSE , 'activated' => FALSE );

  # update HTT_CUSTOMERS_OVERLAY_ULTRA.stored_value += $amount
  $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
    array(
      'balance_to_add' => $amount,
      'customer_id'    => $customer_id
    )
  );

  if ( is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_update_query)) )
  {
    $description = 'Plan Recharge';
    try
    {
      if ( $pay_quicker )
      {
        $description .= ' (PQ)';
        if ( ! start_mssql_transaction() )
          throw new \Exception( "Error while starting a DB transaction" );
      }

      $history_params = array(
        "customer_id"            => $customer_id,
        "date"                   => 'now',
        "cos_id"                 => $cos_id,
        "entry_type"             => $entry_type,
        "stored_value_change"    => $amount,
        "balance_change"         => 0,
        "package_balance_change" => 0,
        "charge_amount"          => $amount,
        "reference"              => $reference,
        "reference_source"       => get_reference_source($source),
        "description"            => $description,
        "detail"                 => $reason,
        "result"                 => 'COMPLETE',
        "source"                 => $source,
        "is_commissionable"      => $is_commissionable
      );

      foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
        if ( isset( $params[$nullable_field] ) )
          $history_params[$nullable_field] = $params[$nullable_field];

      $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

      if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
        throw new \Exception( "Error while inserting into htt_billing_history (".mssql_get_last_message().")" );

      if ( $pay_quicker )
      {
        // MVNO-2478: add PQ specific additional row to HTT_BILLING_HISTORY
        $history_params['entry_type']        = 'LOAD';
        $history_params['stored_value_change'] = 0;
        $history_params['charge_amount']     = 0;
        $history_params['reference_select']  = htt_billing_history_reference_select( $customer_id , $reference ); // the HTT_BILLING_HISTORY.TRANSACTION_ID of the first row
        $history_params['reference_source']  = get_reference_source('EPAYPQ');
        $history_params['detail']            = $reason;
        $history_params['description']       = 'Pay Quicker SKU ('.$pay_quicker.')';
        $history_params['source']            = $source;
        $history_params['is_commissionable'] = 0;

        $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

        if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
          throw new \Exception( "Error while inserting into htt_billing_history (".mssql_get_last_message().")" );

        if ( ! commit_mssql_transaction() )
          throw new \Exception( "Error while committing a DB transaction" );
      }

      $return["success"] = TRUE;

      // try to activate customer if Provisioned, non-Flex plans
      if (!Flex::isFlexPlan($cos_id)) {
        $customer = $params['customer'];

        $context = array(
          'customer_id' => $customer->CUSTOMER_ID
        );

        $state = internal_func_get_state(array(
          'states' => NULL,
          'context' => $context
        ));

        $result_status = activate_provisioned_account($state, $customer, $context);
        if ($result_status['errors'] && count($result_status['errors']))
          dlog('', "Errors after activate_provisioned_account: " . json_encode($result_status['errors']));

        if (!$result_status['activated']) {
          $result_status = reactivate_suspended_account($state, $customer, $context, $reactivate_now);
          if ($result_status['errors'] && count($result_status['errors']))
            dlog('', "Errors after reactivate_suspended_account: " . json_encode($result_status['errors']));
        }

        if (isset($result_status['activated']))
          $return['activated'] = $result_status['activated'];

        if (isset($result_status['reactivated']))
          $return['activated'] = $result_status['reactivated'];
      } else {
        $return['activated'] = false;
      }
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage() );

      $return['errors'][] = $e->getMessage();
    }
  }
  else
  { $return['errors'][] = "Error while updating accounts (".mssql_get_last_message().")"; }

  return $return;
}

/**
 * func_sweep_stored_value_explicit
 *
 * For ORANGE SIMS we have to retrieve the cos_id if it's ``Neutral``
 */
function func_sweep_stored_value_explicit($customer_id, $source, $reference, $target_cos_id=NULL, $store_zipcode=NULL, $store_id=NULL, $clerk_id=NULL, $terminal_id=NULL)
{
  if ( ! $customer_id ) { return array( 'errors' => array('func_sweep_stored_value_explicit invoked with no customer_id') , 'success' => FALSE ); }

  $customer = get_customer_from_customer_id($customer_id);

  $standby_cos_id = get_cos_id_from_plan('STANDBY');

  // check if cos_id is ``Neutral``
  if ( $customer->cos_id == $standby_cos_id )
  {
    $sim_data = get_htt_inventory_sim_from_iccid($customer->current_iccid);

    // check if customer SIM is orange
    if ( $sim_data && $sim_data->PRODUCT_TYPE === 'ORANGE' )
    {
      if ( empty($sim_data->STORED_VALUE) )
      {
        return array(
          'errors'  => array('ORANGE SIM is missing STORED_VALUE with ICCID ' . $sim_data->ICCID_FULL),
          'success' => FALSE
        );
      }
      $target_cos_id = get_cos_id_from_plan( func_get_orange_plan_from_sim_data($sim_data) );
    }
  }

  return func_sweep_stored_value(
    array(
      'customer'  => $customer,
      'source'    => $source,
      'reference' => $reference,
      'target_cos_id' => $target_cos_id,
      'store_zipcode' => $store_zipcode,
      'store_id'      => $store_id,
      'clerk_id'      => $clerk_id,
      'terminal_id'   => $terminal_id
    )
  );
}

/**
 * func_get_orange_plan_from_sim_data
 *
 * For ORANGE SIMS grab the correct prefix based on sim data DURATION
 *
 * @param  Object $sim_data
 * @return String plan_short
 */
function func_get_orange_plan_from_sim_data($sim_data)
{
  // if OFFER_ID != NULL, get short plan name from orange plan map
  if (! empty($sim_data->OFFER_ID))
  {
    $map = get_orange_plan_map();
    $cosid_duration = ($map[$sim_data->OFFER_ID]) ? $map[$sim_data->OFFER_ID] : null;
    return get_plan_from_cos_id($cosid_duration[0]);
  }

  $prefix = ( ! empty($sim_data->DURATION) && $sim_data->DURATION > 1) ? 'M' : null;
  if ( ! $prefix && ! empty($sim_data->BRAND_ID))
  {
    switch ($sim_data->BRAND_ID)
    {
      case 1: $prefix = $sim_data->STORED_VALUE == 39 ? 'D' : 'L';  break;
      case 2: $prefix = 'UV'; break;
    }
  }

  return $prefix . $sim_data->STORED_VALUE;
}

function func_sweep_stored_value($params)
{
  #-- capture the following into a number, expressed as %f, and do the following iff %f > 0
  #SELECT stored_value FROM htt_customers_overlay_ultra WHERE customer_id = %d

  # BEGIN TRAN
  #UPDATE accounts SET balance = balance + %f WHERE customer_id = %d
  #UPDATE htt_customers_overlay_ultra SET stored_value = stored_value - %f WHERE customer_id = %d
  # COMMIT/ROLLBACK

  #INSERT billing row with customer_id = %d, AMOUNT = 0, comment 'swept stored_value by %f'

  dlog('',"func_sweep_stored_value params = ".json_encode($params));

  $customer_id       = $params['customer']->CUSTOMER_ID;
  $source            = $params['source'];
  $reference         = $params['reference'];
  $is_commissionable = 0;

  dlog('',"customer stored_value = ".$params['customer']->stored_value);

  $cos_id            = $params['customer']->COS_ID; # customer COSID at time of transaction
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) # COS_ID associated with the reason of the transaction
  {
    $cos_id = $params['target_cos_id'];
  }

  $should_rollback = FALSE;

  $return = array( 'errors' => array() , 'success' => FALSE );

  # retrieve HTT_CUSTOMERS_OVERLAY_ULTRA.stored_value
  $return = get_htt_customers_overlay_ultra( array( 'customer_id' => $customer_id ) );

  $return['success'] = FALSE;

  if ( count($return['errors']) > 0 )
  {
    return $return;
  }

  $customer_stored_value = $return['data']->stored_value;
  $stored_value          = $return['data']->stored_value;

  dlog('',"customer stored_value = $customer_stored_value");

  if ( $stored_value < 0 )
  {
    $return['errors'][] = "Invalid stored value.";

    return $return;
  }
  else if ( $stored_value == 0 )
  {
    $return['success'] = TRUE;

    return $return;
  }

  if ( isset($params['sweep_amount']) && $params['sweep_amount'] ) # in $
  {
    if ( $params['sweep_amount'] > $stored_value )
    {
      $return['errors'][] = "The given sweep_amount exceeds customer's stored value.";

      return $return;
    }
    else
    {
      $stored_value = $params['sweep_amount'];
    }
  }

  if ( ! start_mssql_transaction() )
  {
    $return['errors'][] = "DB error";

    return $return;
  }

  # update ACCOUNTS.balance
  $accounts_update_query = accounts_update_query(
    array(
      'add_to_balance' => $stored_value,
      'customer_id'    => $customer_id
    )
  );

  if ( is_mssql_successful(logged_mssql_query($accounts_update_query)) )
  {
    # update HTT_CUSTOMERS_OVERLAY_ULTRA.stored_value to 0
    $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
      array(
        'stored_value' => ( $customer_stored_value - $stored_value ),
        'customer_id'  => $customer_id
      )
    );

    if ( is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_update_query)) )
    {
      if ( commit_mssql_transaction() )
      {
        # a) should refer to webcc in case of a CC - see htt_billing_history docs. b) should be epay and reference is epay trans id in case of epay
        $reference_source = get_reference_source($source);

        $history_params = array(
          "customer_id"            => $customer_id,
          "date"                   => 'now',
          "cos_id"                 => $cos_id,
          "entry_type"             => 'SWEEP_LOAD',
          "stored_value_change"    => ( - $stored_value ),
          "balance_change"         => $stored_value,
          "package_balance_change" => 0,
          "charge_amount"          => 0,
          "reference"              => $reference,
          "reference_source"       => $reference_source,
          "detail"                 => 'SweepStoredValue',
          "description"            => 'Sweep stored value',
          "result"                 => 'COMPLETE',
          "source"                 => $source,
          "is_commissionable"      => $is_commissionable
        );

        foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
        {
          if ( isset( $params[$nullable_field] ) )
          {
            $history_params[$nullable_field] = $params[$nullable_field];
          }
        }

        $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

        if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
        {
          $return["success"] = TRUE;
        }
        else
        { $return['errors'][] = "Error while inserting into htt_billing_history (".mssql_get_last_message().")"; }

      }
      else
      { $return['errors'][] = "Cound not commit a mssql transaction (".mssql_get_last_message().")"; }

    }
    else
    {
      $return['errors'][] = "Error while updating htt_customers_overlay_ultra (".mssql_get_last_message().")";

      $should_rollback = TRUE;
    }

  }
  else
  {
    $return['errors'][] = "Error while updating accounts (".mssql_get_last_message().")"; 

    $should_rollback = TRUE;
  }

  if ( $should_rollback )
  {
    if ( ! rollback_mssql_transaction() )
    { $return['errors'][] = "Cound not rollback a mssql transaction (".mssql_get_last_message().")"; }
  }

  return $return;
}


function func_courtesy_add_ild_minutes($params)
{
#  A customer ID is passed in as well as an amount to add, balance_to_add.
#  account.package_balance1 +=  balance_to_add*100.
#  A row is inserted into HTT_BILLING_HISTORY, noting the customer_id, the balance added (QUANTITY), the customer service reason, the source of the funds, the assocaited CS agent or ticket n.

  $source            = 'CSCOURTESY';
  $entry_type        = 'LOAD';
  $is_commissionable = 0;
  $detail            = $params['detail'];
  $reference         = $params['reference'];
  $amount            = $params['amount']; # in $ (positive or negative)
  $customer_id       = $params['customer']->CUSTOMER_ID;

  $cos_id            = $params['customer']->COS_ID; # customer COSID at time of transaction
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) # COS_ID associated with the reason of the transaction
  {
    $cos_id = $params['target_cos_id'];
  }

  $return = array( 'errors' => array() , 'success' => FALSE );

  $accounts_update_query = accounts_update_query(
    array(
      'add_to_package_balance1' => $amount*10,
      'customer_id'             => $customer_id
    )
  );

  if ( is_mssql_successful(logged_mssql_query($accounts_update_query)) )
  {
    # a) should refer to webcc in case of a CC - see htt_billing_history docs. b) should be epay and reference is epay trans id in case of epay
    $reference_source = get_reference_source($source);

    $history_params = array(
      "customer_id"            => $customer_id,
      "date"                   => 'now',
      "cos_id"                 => $cos_id,
      "entry_type"             => $entry_type,
      "stored_value_change"    => 0,
      "balance_change"         => 0,
      "package_balance_change" => $amount*1000,
      "charge_amount"          => 0,
      "reference"              => $reference,
      "reference_source"       => $reference_source,
      "detail"                 => $detail,
      "description"            => ( ( $amount > 0 ) ? 'Courtesy International Credit Added' : 'Courtesy International Credit Deducted' ),
      "result"                 => 'COMPLETE',
      "source"                 => $source,
      "is_commissionable"      => $is_commissionable
    );

    foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
    {
      if ( isset( $params[$nullable_field] ) && $params[$nullable_field] != '' )
      {
        $history_params[$nullable_field] = $params[$nullable_field];
      }
    }

    $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

    if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
    {
      $return["success"] = TRUE;
    }
    else
    { $return['errors'][] = "Error while inserting into htt_billing_history (".mssql_get_last_message().")"; }

  }
  else
  { $return['errors'][] = "Error while updating accounts (".mssql_get_last_message().")"; }

  return $return;
}


function func_courtesy_void_add_balance($params)
{
/*

1) Verify the first transaction matches the amount,
   and that the first transaction hasn't been voided already. (verify by REFERENCE and REFERENCE_SOURCE) Make sure it was a cash transaction, not minutes.

in a transaction

2) Write a 'reverse' entry to HTT_BILLING_HISTORY; ENTRY_TYPE VOID, BALANCE_CHARGE = -AMOUNT, CHARGE_AMOUNT = -AMOUNT, REFERENCE=transaction_id, REFERENCE_SOURCE=new #, detail = 
Courtesy Wallet Balance Removed – reset is the same as the initial insert.

3) Deduct the amount from the customer balance

*/

  dlog('',"func_courtesy_remove_balance params = ".json_encode($params));

  $source            = 'CSCOURTESY';
  $entry_type        = 'LOAD';
  $is_commissionable = 0;
  $amount            = $params['amount'];
  $reason            = $params['reason'];
  # $reference         = $params['reference']; # TODO: Riz
  $customer_id       = $params['customer']->CUSTOMER_ID;
  $terminal_id       = $params['terminal_id'];
  $transaction_id    = $params['transaction_id'];
  $reference_source  = get_reference_source($source);

  $cos_id            = $params['customer']->COS_ID; # customer COSID at time of transaction
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) # COS_ID associated with the reason of the transaction
  {
    $cos_id = $params['target_cos_id'];
  }

  $return = array( 'errors' => array() , 'success' => FALSE );

  // get row from HTT_BILLING_HISTORY, the one we wish to void

  $htt_billing_history_select_query = htt_billing_history_select_query(
    array(
      'transaction_id'    => $transaction_id,
      'customer_id'       => $customer_id,
      'terminal_id'       => $terminal_id,
      'result'            => 'COMPLETE',
      'balance_change'    => $amount,
      'charge_amount'     => $amount,
      'entry_type'        => 'LOAD',
      'description'       => $reason,
      'reference_source'  => $reference_source,
      # 'reference'         => $reference, # TODO: Riz
      'source'            => $source
    )
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_billing_history_select_query));

  if ( is_array($query_result) )
  {

    if ( count($query_result) > 0 )
    {

      // check that transaction hasn't been voided already

      $htt_billing_history_select_query = htt_billing_history_select_query(
        array(
          'customer_id'       => $customer_id,
          'terminal_id'       => $terminal_id,
          'result'            => 'COMPLETE',
          'balance_change'    => - $amount,
          'charge_amount'     => - $amount,
          'entry_type'        => 'VOID',
          'reference'         => $transaction_id,
          'description'       => $reason,
          'reference_source'  => get_reference_source('VOID'),
          'detail'            => 'Courtesy Wallet Balance Removed',
          'source'            => $source
        )
      );

      $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_billing_history_select_query));

      if ( is_array($query_result) )
      {

        if ( count($query_result) == 0 )
        {

          $sql_queries = array();

          $history_params = array(
            "customer_id"            => $customer_id,
            "date"                   => 'now',
            "cos_id"                 => $cos_id,
            "entry_type"             => 'VOID',
            "stored_value_change"    => '0',
            "balance_change"         => - $amount,
            "package_balance_change" => '0',
            "charge_amount"          => - $amount,
            "reference"              => $transaction_id,
            "reference_source"       => get_reference_source('VOID'),
            "detail"                 => 'Courtesy Wallet Balance Removed',
            "description"            => $reason,
            "result"                 => 'COMPLETE',
            "source"                 => $source,
            "terminal_id"            => $terminal_id,
            "is_commissionable"      => $is_commissionable
          );

          $sql_queries[] = htt_billing_history_insert_query( $history_params );

          $sql_queries[] = accounts_update_query(
            array(
              'add_to_balance'          => - $amount,
              'customer_id'             => $customer_id
            )
          );

          $return = exec_queries_in_transaction( $sql_queries );

        }
        else # the transaction has been voided already
        { $return['errors'][] = "ERR_API_INTERNAL: transaction already voided."; }

      }
      else # cannot query HTT_BILLING_HISTORY
      { $return['errors'][] = "ERR_API_INTERNAL: Database error (2)."; }

    }
    else # could not find the transaction to be voided
    { $return['errors'][] = "ERR_API_INTERNAL: transaction not found."; }

  }
  else # cannot query HTT_BILLING_HISTORY
  { $return['errors'][] = "ERR_API_INTERNAL: Database error (1)."; }

  return $return;
}


function func_courtesy_add_stored_value($params)
{
  $source            = 'CSCOURTESY';
  $entry_type        = 'LOAD';
  $is_commissionable = 0;
  $reference         = $params['reference'];
  $amount            = $params['amount'];
  $reason            = $params['reason'];
  $terminal_id       = $params['terminal_id'];
  
  $return = array( 'errors' => array() , 'success' => FALSE );

  $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
    array(
      'balance_to_add' => $amount,
      'customer_id'    => $params['customer']->CUSTOMER_ID
    )
  );

  if ( is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_update_query)) )
  {
    $reference_source = get_reference_source($source);

    $history_params = array(
      "customer_id"            => $params['customer']->CUSTOMER_ID,
      "date"                   => 'now',
      "cos_id"                 => $params['customer']->COS_ID,
      "entry_type"             => $entry_type,
      "stored_value_change"    => $amount,
      "balance_change"         => '0',
      "package_balance_change" => '0',
      "charge_amount"          => $amount,
      "reference"              => $reference,
      "reference_source"       => $reference_source,
      "detail"                 => $params['detail'],
      "description"            => $reason,
      "result"                 => 'COMPLETE',
      "source"                 => $source,
      "terminal_id"            => $terminal_id,
      "is_commissionable"      => 0
    );

    foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
    {
      if ( isset( $params[$nullable_field] ) && $params[$nullable_field] != '' )
      {
        $history_params[$nullable_field] = $params[$nullable_field];
      }
    }

    $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

    if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
    {
      $return["success"] = TRUE;
    }
    else
    { $return['errors'][] = "ERR_API_INTERNAL: DB error"; }
  }
  else
  { $return['errors'][] = "ERR_API_INTERNAL: DB error"; }

  return $return;
}

function func_courtesy_add_balance($params)
{
#  A customer ID is passed in as well as an amount to add, balance_to_add.
#  account.balance +=  balance_to_add.
#  A row is inserted into HTT_BILLING_HISTORY, noting the customer_id, the balance added (AMOUNT), the customer service reason, the source of the funds, the assocaited CS agent or ticket #

  $source            = 'CSCOURTESY';
  $entry_type        = 'LOAD';
  $is_commissionable = 0;
  $reference         = $params['reference'];
  $amount            = $params['amount']; # amount can be positive or negative
  $reason            = $params['reason'];
  $customer_id       = $params['customer']->CUSTOMER_ID;

  $cos_id            = $params['customer']->COS_ID; # customer COSID at time of transaction
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) # COS_ID associated with the reason of the transaction
  {
    $cos_id = $params['target_cos_id'];
  }

  $return = array( 'errors' => array() , 'success' => FALSE );

  $accounts_update_query = accounts_update_query(
    array(
      'add_to_balance'          => $amount,
      'customer_id'             => $customer_id,
      'last_recharge_date_time' => 'now'
    )
  );

  if ( is_mssql_successful(logged_mssql_query($accounts_update_query)) )
  {
    # a) should refer to webcc in case of a CC - see htt_billing_history docs. b) should be epay and reference is epay trans id in case of epay
    $reference_source = get_reference_source($source);

    $history_params = array(
      "customer_id"            => $customer_id,
      "date"                   => 'now',
      "cos_id"                 => $cos_id,
      "entry_type"             => $entry_type,
      "stored_value_change"    => '0',
      "balance_change"         => $amount, # TODO: Riz, absolute value?
      "package_balance_change" => '0',
      "charge_amount"          => $amount, # TODO: Riz, absolute value?
      "reference"              => $reference,
      "reference_source"       => $reference_source,
      "detail"                 => $params['detail'],
      "description"            => empty($reason) ? ('Courtesy Wallet Balance ' . ($amount > 0 ? 'Added' : 'Deducted')) : $reason,
      "result"                 => 'COMPLETE',
      "source"                 => $source,
      "is_commissionable"      => $is_commissionable
    );

    foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
    {
      if ( isset( $params[$nullable_field] ) && $params[$nullable_field] != '' )
      {
        $history_params[$nullable_field] = $params[$nullable_field];
      }
    }

    $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

    if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
    {
      $return["success"] = TRUE;

      // try to activate customer if Provisioned, non-Flex plans
      if (!Flex::isFlexPlan($cos_id)) {
        $customer = $params['customer'];

        $context = array(
          'customer_id' => $customer->CUSTOMER_ID
        );

        $state = internal_func_get_state(array(
          'states'  => NULL,
          'context' => $context
        ));

        $result_status = activate_provisioned_account($state,$customer,$context);
        if ( $result_status['errors'] && count( $result_status['errors'] ) )
        { dlog('',"Errors after activate_provisioned_account: ".json_encode($result_status['errors'])); }

        if ( !$result_status['activated'] )
        {
          $result_status = reactivate_suspended_account($state,$customer,$context);
          if ( $result_status['errors'] && count( $result_status['errors'] ) )
          { dlog('',"Errors after reactivate_suspended_account: ".json_encode($result_status['errors'])); }
        }
      }
    }
    else
    { $return['errors'][] = "Error while inserting into htt_billing_history (".mssql_get_last_message().")"; }
  }
  else
  { $return['errors'][] = "Error while updating accounts (".mssql_get_last_message().")"; }

  return $return;
}


/**
 * func_validate_pin_cards
 *
 * Given an array of PINs - $params["pin_list"] contains the list of PINs
 * The numbers are encrypted/obfuscated and checked against HTT_INVENTORY_PIN (unless they are Incomm)
 * Max validation of 6 pin cards at once.
 * Returns an array describing the PIN Card's value (initialValue), whether the PIN Card has been used (customerUsed), and whether the PIN Card has been administratively disabled. (stillValid)
 *
 * @return array
 */
function func_validate_pin_cards($params)
{
  #sleep(5); # prevent brute forcing

  $return = array(
    'errors'                     => array(),
    'result'                     => array(),
    'values'                     => array(),
    'at_least_one_not_found'     => FALSE,
    'at_least_one_customer_used' => FALSE, // for Incomm, status 4003
    'at_least_one_at_foundry'    => FALSE, // for Incomm, status 4002
    'status_count'               => array() # array of '$status' => '$count'
  );

  if ( ! is_array( $params["pin_list"] ) )
  {
    return array( 'errors' => array("Invalid input in func_validate_pin_cards") );
  }

  if ( count( $params["pin_list"] ) > 6 ) #  maximum validation of 6 pin cards at once
  {
    return array( 'errors' => array("Maximum validation of 6 PIN in func_validate_pin_cards") );
  }

  # Chris: not necessary
  #if ( fraudCheckValidatePINCards() )
  #{
  #  return array( 'errors' => array("Fraud check prevented execution of func_validate_pin_cards") );
  #}

  foreach ( $params["pin_list"] as $pin)
  {
    // fork logic based on PIN type/length
    $length = strlen($pin);
    if ($length == INCOMM_PIN_LENGTH) // inComm PIN
      $result = func_validate_pin_cards_incomm($pin);
    elseif ($length == ULTRA_PIN_LENGTH) // Ultra PIN
      $result = func_validate_pin_cards_ultra($pin);
    else
    {
      $result = NULL;
      $return['errors'][] = "Invalid PIN $pin";
    }

    if (is_object($result))
    {
      dlog('',"$pin STATUS = ". $result->status . "; VALUE = " . $result->pin_value);

      $return['result'][$pin] = get_object_vars($result);
      $return['result'][$pin]['not_found'] = 0;
      $return['values'][] = $result->pin_value;

      if ( ! isset( $return['status_count'][ $result->status ] ) )
        $return['status_count'][$result->status] = 0;
      $return['status_count'][$result->status]++;

      if (( isset($result->customer_used)) && ($result->customer_used))
        $return['at_least_one_customer_used'] = TRUE;

      if ($result->status == 'AT_FOUNDRY')
        $return['at_least_one_at_foundry'] = TRUE;
    }
    else
    {
      $return['result'][$pin] = array( 'not_found' => 1 );
      $return['at_least_one_not_found'] = TRUE;
    }
  }

  return $return;
}


/**
 * func_apply_pin_cards
 *
 * Passed a customer_id, and an array of PIN Cards.  The numbers are encrypted/obfuscated and checked against HTT_INVENTORY_PIN, they must all be valid.
 * The PIN Inventory is marked as Used, linking to the customer.
 * payments::AddBalance is called once for each topup card applied.
 *
 * @return array
 */
function func_apply_pin_cards($params)
{
  $pin_list          = $params["pin_list"];
  $customer_id       = $params['customer']->CUSTOMER_ID;
  $source            = $params['source']; # CSPIN or WEBPIN or PHONEPIN
  $reference         = $params['reference'];
  $reason            = $params['reason'];
  $entry_type        = 'LOAD';

  if ( isset( $params['entry_type'] ) )
  {
    $entry_type = $params['entry_type'];
  }

  # If the value isn't set use func_apply_pin_cards. Likely values are 'CS_IVR', 'CS_AGENT', 'CS_ACTIVATION', 'PORTAL_ACTIVATION'
  $last_changed_by = 'func_apply_pin_cards';

  if ( isset( $params['changed_by'] ) )
  {
    $last_changed_by = $params['changed_by'];
  }

  # default destination is customer's balance
  $destination = "WALLET";

  if (isset( $params['destination']) && ! multi_month_info($customer_id))
    $destination = $params['destination'];

  # validate PINs
  if (empty($params['validated']))
    $return = func_validate_pin_cards($params);
  else // already validated
    $return = $params['validated'];
  $return['success']      = FALSE;
  $return['activated']    = FALSE;
  $return['total_amount'] = 0;

  if (!isset($return['warnings']))
    $return['warnings'] = array();

  if ( count($return['errors']) > 0 )
  {
    return $return;
  }
  else if ( $return['at_least_one_at_foundry'] )
  {
    # no PINS should be in AT_FOUNDRY status

    $return['errors'] = array("Some of the given PINs are not active.");

    return $return;
  }
  else if ( $return['at_least_one_not_found'] )
  {
    # all PINS must be valid and in the DB

    $return['errors'] = array("Some of the given PINs are not valid.");

    return $return;
  }
  else if ( ( isset( $return['status_count']['USED'] ) ) && ( $return['status_count']['USED'] > 0 ) )
  {
    # the value of HTT_INVENTORY_PIN.status shoud not be USED

    $return['errors'] = array("Some of the given PINs are already used.");
  }
  else if ( ( isset( $return['status_count']['LOST_INVENTORY'] ) ) && ( $return['status_count']['LOST_INVENTORY'] > 0 ) )
  {
    # the value of HTT_INVENTORY_PIN.status shoud not be LOST_INVENTORY

    $return['errors'] = array("Some of the given PINs are lost.");
  }
  else if ( ( isset( $return['status_count']['AT_CELLUPHONE'] ) ) && ( $return['status_count']['AT_CELLUPHONE'] > 0 ) )
  {
    # the value of HTT_INVENTORY_PIN.status shoud not be AT_CELLUPHONE

    $return['errors'] = array("Some of the given PINs are at Celluphone.");
  }
  else if ( ( isset( $return['at_least_one_customer_used'] ) ) && ( $return['at_least_one_customer_used'] ) )
  {
    # the value of HTT_INVENTORY_PIN.customer_used should not be set

    $return['errors'] = array("Some of the given PINs are already assigned to a customer.");
  }

  # loop though all PINs
  foreach ( $params["pin_list"] as $pin)
  {
    if ($return['result'][$pin] && 
      in_array($return['result'][$pin]['status'], array('USED', 'LOST_INVENTORY', 'AT_CELLUPHONE')))
    {
      $return['warnings'][] = "There was a problem with PIN $pin.";
    }
    else
    {
      $check_ultra_pin = TRUE;
      $store_id        = '';
      $terminal_id     = '';

      if (strlen($pin) == ULTRA_PIN_LENGTH) // Ultra PIN
      {
        # assign PIN to customer in HTT_INVENTORY_PIN and mark status as USED
        $htt_inventory_pin_update_query = htt_inventory_pin_update_query(
          array(
            "pin"             => $pin,
            "status"          => 'USED',
            "customer_used"   => $customer_id,
            "last_changed_by" => $last_changed_by
          )
        );
        $check_ultra_pin = is_mssql_successful(logged_mssql_query($htt_inventory_pin_update_query));
        if (! $check_ultra_pin)
          $return['errors'][] = "Error while updating HTT_INVENTORY_PIN (".mssql_get_last_message().")";
      }
      else // inComm PIN
      {
        // charge inComm PIN and collect response info
        $result = \Ultra\Lib\Incomm\pinRedemption($pin, $params['customer']);
        if ($result->is_success())
        {
          $store_id        = $result->get_data_key('StoreID');
          $terminal_id     = $result->get_data_key('MerchantName');
          $source          = 'INCOMM';
        }
        elseif ($result->is_timeout())
        {
          // always reverse PIN on timeout
          $check_ultra_pin = FALSE;
          $return['errors'][] = "Failed to redeem PIN $pin due to a network error.";
          \Ultra\Lib\Incomm\pinReversal(array('pin' => $pin, 'SrcRefNum' => $result->get_data_key('SrcRefNum')), $params['customer']);
        }
        else
        {
          $check_ultra_pin = FALSE;
          $return['errors'][] = $result->get_errors();
          $return['warnings'][] = "There was a problem with PIN $pin.";
        }
      }

      if ( $check_ultra_pin )
      {
        #  payments::AddBalance is called once for each topup card applied.

        $add_value_params = array(
          'customer'    => $params['customer'],
          'amount'      => $return['result'][$pin]['pin_value'], # SKU # "PIN Card's value (initialValue)"
          'reason'      => $reason,
          'reference'   => $reference,
          'source'      => $source,
          'entry_type'  => $entry_type,
          'store_id'    => $store_id,
          'terminal_id' => $terminal_id
        );

        $return['total_amount'] += $return['result'][$pin]['pin_value'];

        foreach ( array('target_cos_id', 'store_zipcode', 'clerk_id', 'store_id', 'terminal_id') as $nullable_field )
          if ( ! empty( $params[$nullable_field] ) )
            $add_value_params[$nullable_field] = $params[$nullable_field];
                                          
        $add_value_result = NULL;

        if ( $destination == "WALLET" )
          // load value to ACCOUNTS.BALANCE
          $add_value_result = func_add_balance($add_value_params);
        else # ( $destination == "MONTHLY" )
          // load value to htt_customers_overlay_ultra.stored_value
          $add_value_result = func_add_stored_value($add_value_params);

        // save inComm transaction
        if ($add_value_result['success'] && strlen($pin) == INCOMM_PIN_LENGTH)
          if (! log_incomm_transaction($customer_id, $reference, $result->get_data_array()))
            $return['errors'][] = 'Failed to log inComm transaction in DB';

        $return['errors'] = array_merge( $return['errors'] , $add_value_result['errors'] );

        $return['success'] = ( count($return['errors']) == 0 );

        $return['activated'] = $add_value_result['activated'];
      }
    }
  } # loop though all PINs

  dlog('',"return = %s", $return);

  return $return;
}

/**
 * func_add_balance_by_tokenized_cc
 *
 * Charge customer's credit card
 * Add funds to ACCOUNTS.balance
 *
 * @return Result object
 */
function func_add_balance_by_tokenized_cc($params, $disableFraudCheck=false, $isCommissionable = 1)
{
  $params['fund_destination'] = WALLET;

  return func_add_funds_by_tokenized_cc($params, $disableFraudCheck, $isCommissionable);
}

/**
 * func_add_stored_value_by_tokenized_cc
 *
 * Charge customer's credit card
 * Add funds to HTT_CUSTOMERS_OVERLAY_ULTRA.stored_value
 *
 * @return Result object
 */
function func_add_stored_value_by_tokenized_cc($params, $disableFraudCheck=false)
{
  $params['fund_destination'] = MONTHLY;

  return func_add_funds_by_tokenized_cc($params, $disableFraudCheck);
}

/**
 * func_add_funds_by_tokenized_cc
 *
 * We have a primary and a secondary gateway,
 * we try the 1st one, if the error is due to temporary unavailability we try the secondary (this is a TODO feature).
 * We never attempt a third gateway.
 * Fraud checks:
 *  - $99 is the max 'charge_amount' allowed
 *  - $200 is the max cumulative customer's wallet.
 * Input:
 *  - 'customer'
 *  - 'charge_amount'
 *  - 'description'
 *  - 'session'
 *  - 'reason'
 *  - 'fund_destination'
 *  - 'detail'
 *  - 'target_cos_id'          ( not required )
 *  - 'include_taxes_fees'     ( not required )
 *  - 'skip_max_balance_check' ( not required )
 *  - 'store_zipcode'          ( not required )
 *  - 'store_id'               ( not required )
 *  - 'clerk_id'               ( not required )
 *  - 'terminal_id'            ( not required )
 *  - 'transaction_type'       ( not required )
 *
 * @return Result object
 */
function func_add_funds_by_tokenized_cc($params, $disableFraudCheck=false, $isCommissionable = 1)
{
  dlog('', '(%s)', func_get_args());

  $return = \make_ok_Result();

  $warnings         = array();
  $rejection_errors = array();
  $transient_errors = FALSE;
  $cc_transactions_id = '';

  $recurring_payment      = ( isset( $params['recurring_payment']      ) && $params['recurring_payment']      );
  $include_taxes_fees     = ( isset( $params['include_taxes_fees']     ) && $params['include_taxes_fees']     );
  $skip_max_balance_check = ( isset( $params['skip_max_balance_check'] ) && $params['skip_max_balance_check'] );
  $productType            = !empty($params['product_type']) ? $params['product_type'] : null;

  if ($params['customer'] instanceof Customer) {
    $params['customer'] = (object) (array_change_key_case((array) $params['customer'], CASE_UPPER));
  }

  // transition functions rely on lower case cos_id
  if (!empty($params['customer']->COS_ID) && empty($params['customer']->cos_id)) {
    $params['customer']->cos_id = $params['customer']->COS_ID;
  }

  // COS_ID associated with the reason of the transaction
  $cos_id = $params['customer']->COS_ID;
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] )
    $cos_id = $params['target_cos_id'];

  try
  {
    if ( ! customer_has_credit_card( $params['customer'] ) )
    {
      $error = 'ERR_API_INVALID_ARGUMENTS: There are no credit card information associated to the customer.';
      $rejection_errors[] = $error;
      throw new \Exception( $error );
    }

    if ( ! \Ultra\UltraConfig\cc_transactions_enabled() )
    {
      $error = 'Credit Card transactions have been disabled temporarily';
      $transient_errors[] = $error;
      throw new \Exception( $error );
    }

    if ($disableFraudCheck) {
      $fraud_error = null;
      $fraud_code = null;
    } else {
      list( $fraud_error , $fraud_code ) = cc_transactions_fraud_check($params['customer']->CUSTOMER_ID, $cos_id);
    }

    if ( $fraud_error )
    {
      log_cc_transaction_fraud_event( $params , $fraud_code );
      $rejection_errors[] = $fraud_error;
      throw new \Exception( $fraud_error );
    }

    if (property_exists($params['customer'], "STORED_VALUE")) {
      $params['customer']->stored_value = $params['customer']->STORED_VALUE;
    }

    if ( ( ( $params['customer']->BALANCE + $params['customer']->stored_value + $params['charge_amount'] ) > 600 ) && !$skip_max_balance_check && !$disableFraudCheck )
    {
      $error = 'ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet. Please use up some of your current balance before adding more.';
      $rejection_errors[] = $error;
      throw new \Exception( $error );
    }

    if ( $params['charge_amount'] > 500 )
    {
      $error = 'ERR_API_INVALID_ARGUMENTS: Maximum amount exceeded.';
      $rejection_errors[] = $error;
      throw new \Exception( $error );
    }

    // get zip code
    $zip = $params['customer']->CC_POSTAL_CODE;

    if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
      $zip = $params['customer']->POSTAL_CODE;

    $taxes_and_fees_data = default_taxes_and_fees_data();

    // check for international CC for charging taxes
    if (empty($params['customer']->CC_POSTAL_CODE) && $params['customer']->BRAND_ID == 3) {
      // check for campus sim customer
      if (is_campus_sim_customer($params['customer']->CUSTOMER_ID)) {
        $include_taxes_fees = false;
      }
    }

    // add surcharges to amount if needed
    if ( $include_taxes_fees )
    {
      list( $errors , $rejection_errors , $transient_errors , $surcharges , $taxes_and_fees_data ) = func_calculate_taxes_fees(
        $zip,
        $params['customer']->CUSTOMER_ID,
        $params['customer']->ACCOUNT,
        $params['charge_amount'] * 100,
        (isset($params['transaction_type'])) ? $params['transaction_type'] : NULL,
        $productType
      );

      if ($params['customer']->BRAND_ID == 3 && $params['reason'] == 'ADD_BALANCE')
      {
        $surcharges = $surcharges - $taxes_and_fees_data['recovery_fee'];
        $taxes_and_fees_data['recovery_fee'] = 0;
      }

      if ( $errors )
        throw new \Exception( $errors[0] );
    }
    else
      $surcharges = 0;

    // "combined amount" refers to "base amount" plus the "surcharges amount"
    $combined_amount = $params['charge_amount'] + $surcharges / 100;

    $ccQueue = new \Ultra\Lib\CC\Queue();

    // prepare required transaction details
    $details = array(
      'destination'     => $params['fund_destination'],
      'cos_id'          => $cos_id,
      'detail'          => $params['detail'], // gah!
      'zipcode'         => empty($params['store_zipcode']) ? $zip : $params['store_zipcode'],
      'commissionable'  => $isCommissionable);

    // prepare optional transaction details
    if ( ! empty($taxes_and_fees_data))
      $details['surcharges'] = $taxes_and_fees_data;
    foreach (array('store_id', 'clerk_id', 'terminal_id') as $nullable_field)
      if ( ! empty($params[$nullable_field]))
        $details[$nullable_field] = $params[$nullable_field];

    // check encoded length against ULTRA.CC_TRANSACTIONS.DETAILS and discard surcharge info (since it is the largest) instead of failing the transaction entirely
    if (strlen(json_encode($details) > 512))
    {
      logWarn('exceeded max length of ULTRA.CC_TRANSACTIONS.DETAILS, surchages discarded');
      unset($details['surcharges']);
      $combined_amount = $params['charge_amount'];
    }

    // prepare transaction parameters
    $chargeParams = array(
      'customer_id'         => $params['customer']->CUSTOMER_ID,
      'charge_amount'       => $combined_amount,
      'description'         => $params['description'],
      'gateway'             => \Ultra\UltraConfig\getPrimaryCCGateway(),
      'details'             => $details
    );

    $return = ( $recurring_payment ) ? $ccQueue->enqueueRecurringcharge( $chargeParams ) : $ccQueue->enqueueCharge( $chargeParams ) ;

    if ( $return->is_failure() )
      throw new \Exception( 'ccQueue::enqueue failed' );

    dlog('',"ccQueue returned %s",$return->data_array);

    $cc_transactions_id = $return->data_array['cc_transactions_id'];

    // primary gateway
    list( $success , $timeout , $status , $result , $error ) = \Ultra\Lib\CC\waitForTransaction( $ccQueue , $return->data_array['cc_transactions_id'] );

/*
    // TODO: secondary gateway
    if ( ! $success && $timeout && \Ultra\UltraConfig\getSecondaryCCGateway() )
    {
      list( $success , $timeout , $status , $result , $error ) = \Ultra\Lib\CC\waitForTransaction( $ccQueue , $return->data_array['cc_transactions_id'] );
    }
*/

    if ( $timeout )
      $return = \make_timeout_Result( $result , array( 'error_codes' => 'TI0001' ) );
    elseif( $success )
    {
      // try to activate customer if Provisioned, non-Flex
      $return = \make_ok_Result();

      if (!Flex::isFlexPlan($cos_id)) {
        $context = array('customer_id' => $params['customer']->CUSTOMER_ID);

        $state = internal_func_get_state(array('states' => NULL, 'context' => $context));

        $result_status = activate_provisioned_account($state, $params['customer'], $context);

        if (isset($result_status['errors']) && count($result_status['errors']))
          dlog('', "Errors after activate_provisioned_account : " . json_encode($result_status['errors']));

        if ($result_status['activated'])
          // log FUNDING_SOURCE and FUNDING_AMOUNT in ULTRA.HTT_ACTIVATION_HISTORY
          log_funding_in_activation_history($params['customer']->CUSTOMER_ID, 'CCARD', $params['charge_amount']);
        else {
          // try to activate customer if Suspended

          $result_status = reactivate_suspended_account($state, $params['customer'], $context);

          if ($result_status['errors'] && count($result_status['errors']))
            dlog('', "Errors after reactivate_suspended_account: " . json_encode($result_status['errors']));
        }

        if (isset($result_status['activated']))
          $return->data_array['activated'] = $result_status['activated'];

        if (isset($result_status['reactivated']))
          $return->data_array['activated'] = $result_status['reactivated'];
      } else {
        $return->data_array['activated'] = false;
      }
    }
    else
    {
      dlog('',"CC transaction failed with status = %s , result = %s , error = %s", $status, $result, $error);
      $return = \make_error_Result( $error );
    }
  }
  catch( \Exception $e )
  {
    dlog( '' , $e->getMessage() );

    $return = \make_error_Result( $e->getMessage() );

    $return->data_array['rejection_errors'] = $rejection_errors;
    $return->data_array['transient_errors'] = $transient_errors;
  }

  if ( count($warnings) )
    $return->add_warnings( $warnings );

  $return->data_array['cc_transactions_id'] = $cc_transactions_id;

  return $return;
}

function func_add_balance_by_credit_card($params)
{
#  A customer ID is passed in as well as an amount to add, balance_to_add.
#  An entry is placed into WEBCC to charge the credit card for a particular amount.
#  If the Credit Card charge succeeds; then
#        A row is inserted into BILLING, noting the customer_id, the balance added (AMOUNT), the reason (if any), the webcc details, the destination.
#        account.balance +=  balance_to_add.

  $customer             = $params['customer']; # the customer object
  $amount               = $params['amount'];   # in $
  $reason               = $params['reason'];
  $session              = $params['session'];
  $upgrade_seconds_gain = $params['upgrade_seconds_gain']; # generally, the packaged_balance1 increment on a plan upgrade. (Ted)

  $include_taxes_fees   = ( isset( $params['include_taxes_fees'] ) && $params['include_taxes_fees'] );

  $cos_id = $customer->COS_ID;
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) # COS_ID associated with the reason of the transaction
    $cos_id = $params['target_cos_id'];

  $skip_max_balance_check = FALSE;
  if ( isset( $params['skip_max_balance_check'] ) && $params['skip_max_balance_check'] )
    $skip_max_balance_check = TRUE;

  $date = date('Y-m-d H:i:s');
  $time = date('12/30/1899 H:i:s');

  $return = array( 'errors' => array() , 'success' => FALSE );

  $return['activated']        = FALSE;
  $return['rejection_errors'] = array();
  $return['transient_errors'] = FALSE;

  if ( ( ( $customer->BALANCE + $customer->stored_value + $amount ) > 200 ) && !$skip_max_balance_check )
  {
    $return['errors'][] = "ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet. Please use up some of your current balance before adding more.";
    return $return;
  }

  if ( $amount > 99 )
  {
    $return['errors'][] = "ERR_API_INVALID_ARGUMENTS: Maximum amount exceeded.";
    return $return;
  }

  $webcc_fraud_check = webcc_fraud_check($customer);

  if ( $webcc_fraud_check )
  {
    $return['errors'][] = $webcc_fraud_check;
    $return['rejection_errors'][] = $webcc_fraud_check;
    $return['transient_errors'] = TRUE;
    $return['customer_id'] = $customer->customer_id;
    return $return;
  }

  $bypass_billing_info = ( isset($params["bypass_billing_info"]) && is_array($params["bypass_billing_info"]) )
                         ?
                         $params["bypass_billing_info"]
                         :
                         NULL
                         ;

  $surcharge = 0;

  // get zip code
  $zip = $customer->CC_POSTAL_CODE;

  if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
    $zip = $customer->POSTAL_CODE;

  $taxes_and_fees_data = default_taxes_and_fees_data();

  // add surcharges to amount if needed
  if ( $include_taxes_fees )
  {
    if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
    {
      $return['errors'][]           = 'No zip code found for this customer';
      $return['rejection_errors'][] = 'No zip code found for this customer';
      $return['transient_errors']   = FALSE;
      $return['customer_id']        = $customer->customer_id;
      return $return;
    }

    $result_taxes_fees = calculate_taxes_fees( ($amount*100) , $customer->customer_id , $zip , $customer->ACCOUNT );

    if ( $result_taxes_fees->is_failure() )
    {
      $return['errors']           = $result_taxes_fees->get_errors();
      $return['rejection_errors'] = $result_taxes_fees->get_errors();
      $return['transient_errors'] = FALSE;
      $return['customer_id']      = $customer->customer_id;
      return $return;
    }

    dlog('',"result_taxes_fees sales_tax    = %s",$result_taxes_fees->data_array['sales_tax']);
    dlog('',"result_taxes_fees recovery_fee = %s",$result_taxes_fees->data_array['recovery_fee']); # in cents

    // surcharge in $
    $surcharge = ( $result_taxes_fees->data_array['sales_tax'] + $result_taxes_fees->data_array['recovery_fee'] ) / 100 ;

    $taxes_and_fees_data = $result_taxes_fees->data_array;
  }

  // "combined amount" refers to "base amount" plus the "surcharges amount"
  $combined_amount = $amount + $surcharge;

  $webcc_insert_query = webcc_insert_query(
    array(
      'customer'              => $customer,
      'session'               => $session,
      'charge_amount'         => $combined_amount, // the "combined amount" should be charged to the credit card
      'credit_amount'         => 0,
      'status'                => 'WPENDING',
      'charge_description'    => $reason,
      'charge_detail'         => 'AddBalanceByCreditCard',
      'upgrade_seconds_gain'  => $upgrade_seconds_gain,
      'date'                  => $date,
      'time'                  => $time,
      'bypass_billing_info'   => $bypass_billing_info
    )
  );

  if ( is_mssql_successful(logged_mssql_query($webcc_insert_query)) )
  {
    # WebCC runners will automatically transfer the amount to ACCOUNTS.balance

    $return = web_cc_lookup_status($return,$date,$time,$session,$customer);

    if ( count($return['errors']) < 1 )
    {

      if ( $return['status'] == 'APPROVED' )
      {
        # the Credit Card charge succeeded

        # account.balance +=  balance_to_add
        $accounts_update_query = accounts_update_query(
          array(
            'add_to_balance'          => $amount,
            'customer_id'             => $customer->CUSTOMER_ID,
            'last_recharge_date_time' => 'now'
          )
        );

        if ( is_mssql_successful(logged_mssql_query($accounts_update_query)) )
        {
           # add a row into HTT_BILLING_HISTORY

           # a) should refer to webcc in case of a CC - see htt_billing_history docs. b) should be epay and reference is epay trans id in case of epay
           $reference_source = get_reference_source('WEBCC');

           $history_params = array(
             "customer_id"            => $customer->CUSTOMER_ID,
             "date"                   => 'now',
             "cos_id"                 => $cos_id,
             "entry_type"             => 'LOAD',
             "stored_value_change"    => '0',
             "balance_change"         => $amount,
             "package_balance_change" => '0',
             "reference"              => $session,
             "reference_source"       => $reference_source,
             "detail"                 => $params['detail'],
             "description"            => empty($reason) ? 'Wallet Balance Added Using Credit Card' : $reason, 
             "result"                 => 'COMPLETE',
             "source"                 => 'WEBCC',
             "is_commissionable"      => 1,
             "charge_amount"                => $combined_amount,
             "commissionable_charge_amount" => $amount,
             "surcharge_amount"             => $surcharge
           );

           foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
             if ( isset( $params[$nullable_field] ) && $params[$nullable_field] != '' )
               $history_params[$nullable_field] = $params[$nullable_field];

           # STORE_ZIPCODE = zipcode of customer CC
           if ( $customer->CC_POSTAL_CODE && ( ! isset($history_params['store_zipcode']) ) )
             $history_params['store_zipcode'] = $customer->CC_POSTAL_CODE;

           $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

           if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
           {
             # add rows into ULTRA.SURCHARGE_HISTORY

             $return['success'] = add_rows_to_ultra_surcharge_history(
               $taxes_and_fees_data,
               array(
                 'location'       => $zip,

                 // those are needed to retrieve HTT_BILLING_HISTORY.TRANSACTION_ID
                 'customer_id'    => $customer->CUSTOMER_ID,
                 'entry_type'     => 'LOAD',
                 'reference'      => $session
               )
             );

             if ( ! $return['success'] )
             {
               $return['errors'][] = "Error while inserting into ULTRA.SURCHARGE_HISTORY";

               return $return;
             }

             // try to activate customer if Provisioned, non-Flex plans
             if (!Flex::isFlexPlan($cos_id)) {
               $context = array(
                 'customer_id' => $customer->CUSTOMER_ID
               );

               $state = internal_func_get_state(array(
                 'states'  => NULL,
                 'context' => $context
               ));

               $result_status = activate_provisioned_account($state,$customer,$context);
               if ( isset($result_status['errors']) && count($result_status['errors']) )
               { dlog('',"Errors after activate_provisioned_account : ".json_encode($result_status['errors'])); }

               if ( $result_status['activated'] )
               {
                 // log FUNDING_SOURCE and FUNDING_AMOUNT in ULTRA.HTT_ACTIVATION_HISTORY

                 log_funding_in_activation_history( $customer->CUSTOMER_ID , 'CCARD' , $amount );
               }
               else
               {
                 $result_status = reactivate_suspended_account($state,$customer,$context);
                 if ( $result_status['errors'] && count( $result_status['errors'] ) )
                 { dlog('',"Errors after reactivate_suspended_account: ".json_encode($result_status['errors'])); }
               }

               if ( isset($result_status['activated']) )
                 $return['activated'] = $result_status['activated'];

               if ( isset($result_status['reactivated']) )
                 $return['activated'] = $result_status['reactivated'];
             } else {
               $return['activated'] = false;
             }
           }
           else
           # Error while inserting into htt_billing_history
           { $return['errors'][] = "ERR_API_INTERNAL: DB error"; }
        }
        else
        # Error while updating accounts
        { $return['errors'][] = "ERR_API_INTERNAL: DB error"; }
      }
      else
      # webcc transaction not successful
      { $return['errors'][] = "ERR_API_INTERNAL: Credit Card Transaction not successful (status = ".$return['status'].")"; }

    }

  }
  else
  { $return['errors'][] = "ERR_API_INTERNAL: DB error"; }

  return $return;
}

/**
 * A sim and a customer object are passed.
 * We move HTT_INVENTORY_SIM.STORED_VALUE to htt_customers_overlay_ultra.stored_value
 * An entry is placed into HTT_BILLING_HISTORY.
 */
function func_add_stored_value_from_orange_sim($params)
{
  $amount               = $params['amount']; # HTT_INVENTORY_SIM.STORED_VALUE
  $customer             = $params['customer'];
  $session              = $params['session'];

  $cos_id = $customer->COS_ID;
  if ( isset($params['cos_id'] ) )
    $cos_id = $params['cos_id'];

  $result = new Result;

  try
  {
    # update HTT_CUSTOMERS_OVERLAY_ULTRA.stored_value += $amount
    $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
      array(
        'balance_to_add' => $amount,
        'customer_id'    => $customer->CUSTOMER_ID
      )
    );

    if ( ! is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_update_query)) )
      throw new Exception("ERR_API_INTERNAL: DB error");

    # add a row into HTT_BILLING_HISTORY

    $source = 'ORANGE_SIM';

    $history_params = array(
      "customer_id"            => $customer->CUSTOMER_ID,
      "date"                   => 'now',
      "cos_id"                 => $cos_id,
      "entry_type"             => 'LOAD',
      "stored_value_change"    => $amount,
      "balance_change"         => '0',
      "package_balance_change" => '0',
      "charge_amount"          => '0',
      "reference"              => $session, # actcode
      "reference_source"       => get_reference_source( $source ),
      "detail"                 => $params['detail'],
      "description"            => 'Added Stored Value From Orange SIM',
      "result"                 => 'COMPLETE',
      "source"                 => $source,
      "is_commissionable"      => 0
    );

    # STORE_ZIPCODE = zipcode of customer CC
    if ( $customer->CC_POSTAL_CODE && ( ! isset($history_params['store_zipcode']) ) )
    {
      $history_params['store_zipcode'] = $customer->CC_POSTAL_CODE;
    }

    $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

    if ( ! is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
      throw new Exception("ERR_API_INTERNAL: DB error");

    $result->succeed();
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
    $result->add_error( $e->getMessage() );
  }

  return $result;
}

/**
 * A customer ID is passed in as well as an amount to add, balance_to_add.
 * An entry is placed into WEBCC to charge the credit card for a particular amount.
 * If the Credit Card charge succeeds; then
 *   A row is inserted into BILLING, noting the customer_id, the balance added (AMOUNT), the reason (if any), the webcc details, the destination.
 *   stored_value+=  balance_to_add.
 */
function func_add_stored_value_by_credit_card($params)
{
  $customer             = $params['customer']; # the customer object
  $amount               = $params['amount'];   # in $
  $description          = $params['description'];
  $detail               = $params['detail'];
  $session              = $params['session'];
  $upgrade_seconds_gain = $params['upgrade_seconds_gain']; # generally, the packaged_balance1 increment on a plan upgrade. (Ted)

  $include_taxes_fees   = ( isset( $params['include_taxes_fees'] ) && $params['include_taxes_fees'] );

  $cos_id = $customer->COS_ID;
  if ( isset( $params['target_cos_id'] ) && $params['target_cos_id'] ) # COS_ID associated with the reason of the transaction
    $cos_id = $params['target_cos_id'];

  $source = 'WEBCC';

  $date = date('Y-m-d H:i:s');
  $time = date('12/30/1899 H:i:s');

  $return = array(
    'errors'  => array(),
    'success' => FALSE,
    'activated' => FALSE,
    'activate_provisioned_account' => FALSE,
    'reactivate_suspended_account' => FALSE
  );

  $webcc_fraud_check = webcc_fraud_check($customer);

  if ( $webcc_fraud_check )
  {
    $return['errors'][] = $webcc_fraud_check;
    $return['rejection_errors'][] = $webcc_fraud_check;
    $return['transient_errors'] = TRUE;
    $return['customer_id'] = $customer->customer_id;
    return $return;
  }

  $bypass_billing_info = ( isset($params["bypass_billing_info"]) && is_array($params["bypass_billing_info"]) )
                         ?
                         $params["bypass_billing_info"]
                         :
                         NULL
                         ;

  $surcharge = 0;

  // get zip code
  $zip = $customer->CC_POSTAL_CODE;

  if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
    $zip = $customer->POSTAL_CODE;

  $taxes_and_fees_data = default_taxes_and_fees_data();

  // add surcharges to amount if needed
  if ( $include_taxes_fees )
  {
    if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
    {
      $return['errors'][]           = 'No zip code found for this customer';
      $return['rejection_errors'][] = 'No zip code found for this customer';
      $return['transient_errors']   = FALSE;
      $return['customer_id']        = $customer->customer_id;
      return $return;
    }

    $result_taxes_fees = calculate_taxes_fees( ($amount*100) , $customer->customer_id , $zip , $customer->ACCOUNT );

    if ( $result_taxes_fees->is_failure() )
    {
      $return['errors']           = $result_taxes_fees->get_errors();
      $return['rejection_errors'] = $result_taxes_fees->get_errors();
      $return['transient_errors'] = FALSE;
      $return['customer_id']      = $customer->customer_id;
      return $return;
    }

    dlog('',"result_taxes_fees sales_tax    = %s",$result_taxes_fees->data_array['sales_tax']);
    dlog('',"result_taxes_fees recovery_fee = %s",$result_taxes_fees->data_array['recovery_fee']); # in cents

    // surcharge in $
    $surcharge = ( $result_taxes_fees->data_array['sales_tax'] + $result_taxes_fees->data_array['recovery_fee'] ) / 100 ;

    $taxes_and_fees_data = $result_taxes_fees->data_array;
  }

  // "combined amount" refers to "base amount" plus the "surcharges amount"
  $combined_amount = $amount + $surcharge;

  $webcc_insert_query = webcc_insert_query(
    array(
      'customer'              => $customer,
      'session'               => $session,
      'charge_amount'         => $combined_amount, // TRF-22 - the "combined amount" should be charged to the credit card
      'credit_amount'         => 0,
      'status'                => 'WPENDING',
      'charge_description'    => $description,
      'charge_detail'         => 'AddStoredValueByCreditCard',
      'upgrade_seconds_gain'  => $upgrade_seconds_gain,
      'date'                  => $date,
      'time'                  => $time,
      'bypass_billing_info'   => $bypass_billing_info
    )
  );

  if ( is_mssql_successful(logged_mssql_query($webcc_insert_query)) )
  {
    $return = web_cc_lookup_status($return,$date,$time,$session,$customer);

    $return['activate_provisioned_account'] = FALSE;
    $return['reactivate_suspended_account'] = FALSE;

    if ( count($return['errors']) < 1 )
    {

      if ( $return['status'] == 'APPROVED' )
      {
        # the Credit Card charge succeeded

        # update HTT_CUSTOMERS_OVERLAY_ULTRA.stored_value += $amount
        $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
          array(
            'balance_to_add' => $amount,
            'customer_id'    => $customer->CUSTOMER_ID
          )
        );

        if ( is_mssql_successful(logged_mssql_query($htt_customers_overlay_ultra_update_query)) )
        {
           # add a row into HTT_BILLING_HISTORY

           # a) should refer to webcc in case of a CC - see htt_billing_history docs. b) should be epay and reference is epay trans id in case of epay
           $reference_source = get_reference_source($source);

           $history_params = array(
             "customer_id"            => $customer->CUSTOMER_ID,
             "date"                   => 'now',
             "cos_id"                 => $cos_id,
             "entry_type"             => 'LOAD',
             "stored_value_change"    => $amount,
             "balance_change"         => '0',
             "package_balance_change" => '0',
             "reference"              => $session,
             "reference_source"       => $reference_source,
             "detail"                 => $detail,
             "description"            => $description,
             "result"                 => 'COMPLETE',
             "source"                 => $source,
             "is_commissionable"      => 1,
             "charge_amount"                => $combined_amount,
             "commissionable_charge_amount" => $amount,
             "surcharge_amount"             => $surcharge
           );

           foreach ( array("store_zipcode","store_id","clerk_id","terminal_id") as $nullable_field )
             if ( isset( $params[$nullable_field] ) && $params[$nullable_field] != '' )
               $history_params[$nullable_field] = $params[$nullable_field];

           # STORE_ZIPCODE = zipcode of customer CC
           if ( $customer->CC_POSTAL_CODE && ( ! isset($history_params['store_zipcode']) ) )
             $history_params['store_zipcode'] = $customer->CC_POSTAL_CODE;

           $htt_billing_history_insert_query = htt_billing_history_insert_query( $history_params );

           if ( is_mssql_successful(logged_mssql_query($htt_billing_history_insert_query)) )
           {
             # add rows into ULTRA.SURCHARGE_HISTORY

             $return['success'] = add_rows_to_ultra_surcharge_history(
               $taxes_and_fees_data,
               array(
                 'location'       => $zip,

                 // those are needed to retrieve HTT_BILLING_HISTORY.TRANSACTION_ID
                 'customer_id'    => $customer->CUSTOMER_ID,
                 'entry_type'     => 'LOAD',
                 'reference'      => $session
               )
             );

             if ( ! $return['success'] )
             {
               $return['errors'][] = "Error while inserting into ULTRA.SURCHARGE_HISTORY";

               return $return;
             }

             // try to activate customer if Provisioned, non-Flex plans
             if (!Flex::isFlexPlan($cos_id)) {
               $context = array(
                 'customer_id' => $customer->CUSTOMER_ID
               );

               $state = internal_func_get_state(array(
                 'states' => NULL,
                 'context' => $context
               ));

               $result_status = activate_provisioned_account($state, $customer, $context);
               if (isset($result_status['errors']) && count($result_status['errors'])) {
                 dlog('', "Errors after activate_provisioned_account : " . json_encode($result_status['errors']));
               } else {
                 $return['activate_provisioned_account'] = $result_status['activated'];
               }

               if (!$return['activate_provisioned_account']) {
                 $result_status = reactivate_suspended_account($state, $customer, $context);
                 if ($result_status['errors'] && count($result_status['errors'])) {
                   dlog('', "Errors after reactivate_suspended_account: " . json_encode($result_status['errors']));
                 } else {
                   $return['reactivate_suspended_account'] = TRUE;
                 }
               }

               $return['activated'] = $return['activate_provisioned_account'] || $return['reactivate_suspended_account'];
             } else {
               $return['activated'] = false;
             }
           }
           else
           { $return['errors'][] = "Error while inserting into htt_billing_history (".mssql_get_last_message().")"; }

        }
        else
        { $return['errors'][] = "Error while updating HTT_CUSTOMERS_OVERLAY_ULTRA (".mssql_get_last_message().")"; }

      }
      else
      {
        $return['errors'][] = "webcc transaction not successful";
      }

    }

  }
  else
  { $return['errors'][] = "Error while inserting into WEBCC (".mssql_get_last_message().")"; }

  return $return;
}

/**
 * Update Credit Card attributes using the ugly ``fields`` code
 */
function func_update_stored_credit_card($params)
{
  dlog('',"OBSOLETE - TO REMOVE");

  $func_result = array('errors'=>array());

  $customer = $params['customer'];

  $fields = array(
    'account_cc_exp'    => $params['account_cc_exp'],
    'account_cc_cvv'    => $params['account_cc_cvv'],
    'account_cc_number' => $params['account_cc_number']
  );

  $cc_params = array(
    'cc_name',
    'account_first_name',
    'account_last_name',
    'account_address1',
    'account_address2',
    'account_city',
    'account_state_or_region',
    'account_postal_code',
    'account_country',
    'account_number_phone',
    'account_number_email'
  );

  foreach( $cc_params as $cc_param )
  {
    if ( isset($params[ $cc_param ]) )
    {
      $fields[ $cc_param ] = $params[ $cc_param ];
    }
  }

  $fields_update_results = fields($fields, $customer, FALSE);

  $func_result['all_validated']     = FALSE;
  $func_result['all_db_queries_ok'] = NULL;
  $func_result['error_codes']       = array();

  foreach( $fields_update_results as $result_id => $result )
  {
    if ( is_array( $result ) )
    {
      if ( isset($result['validated'] ) && is_array($result['validated']) && ( ! $result['validated']['ok'] ) )
      {
        $func_result['errors'][] = "ERR_API_INVALID_ARGUMENTS: invalid ".$result['validated']['field_name'];

        if ( isset($result['validated']['error_code']) )
          $func_result['error_codes'][] = $result['validated']['error_code'];
      }
      else if ( isset($result['all_validated'] ) )
      {
        if ( $result['all_validated'] )
        {
          $func_result['all_validated'] = TRUE;
        }
      }
      else if ( isset($result['sql'] ) )
      {
        if ( $result['sql'][1] != 1 )
        {
          $func_result['errors'][] = "ERR_API_INTERNAL: Database write error";

          $func_result['all_db_queries_ok'] = FALSE;
        }
        else if ( is_null($func_result['all_db_queries_ok']) )
        {
          $func_result['all_db_queries_ok'] = TRUE;
        }
      }
    }
  }

  return $func_result;
}


## == sub-functions == ##


/**
 * Checks if the given CC numbers should always fail
 */
function overridden_web_cc_return_fail($customer)
{
  $cc_exp    = find_credential('webcc/always_fail/cc_exp');
  $cc_number = find_credential('webcc/always_fail/cc_number');

  dlog('',"find_credential: $cc_exp $cc_number '".$customer->LAST_NAME."'");

  return ( ( substr($cc_number,-4) == substr($customer->CC_NUMBER,-4) )
      && ( ( $cc_exp == $customer->CC_EXP_DATE ) || ( decoct($cc_exp) == $customer->CC_EXP_DATE ) || ( "0".decoct($cc_exp) == $customer->CC_EXP_DATE ) )
      && ( $customer->LAST_NAME == 'Ultrasmith' ) );
}

/**
 * Checks if the given CC numbers should always succeed
 */
function overridden_web_cc_return_success($customer)
{
  $cc_exp    = find_credential('webcc/always_succeed/cc_exp');
  $cc_number = find_credential('webcc/always_succeed/cc_number');

  dlog('',"find_credential: $cc_exp $cc_number".$customer->LAST_NAME."'");

  return ( ( substr($cc_number,-4) == substr($customer->CC_NUMBER,-4) )
      && ( ( $cc_exp == $customer->CC_EXP_DATE ) || ( decoct($cc_exp) == $customer->CC_EXP_DATE ) || ( "0".decoct($cc_exp) == $customer->CC_EXP_DATE ) )
      && ( $customer->LAST_NAME == 'Ultrasmith' ) );
}

/**
 * Checks if the credit card charge result should be overridden
 */
function overridden_web_cc_return($customer)
{
  $overridden_web_cc_return = FALSE;

  if ( overridden_web_cc_return_fail($customer) )
  {
    $overridden_web_cc_return = array(
      'errors'           => array(),
      'result'           => 'overridden_web_cc_return_fail',
      'status'           => 'REJECTED',
      'rejection_errors' => array('overridden_web_cc_return_fail'),
      'transaction_id'   => 1
    );
  }
  elseif ( overridden_web_cc_return_success($customer) )
  {
    $overridden_web_cc_return = array(
      'errors'           => array(),
      'result'           => 'overridden_web_cc_return_success',
      'status'           => 'APPROVED',
      'transaction_id'   => 1
    );
  }

  if ( $overridden_web_cc_return )
  { dlog('',"overridden_web_cc_return is TRUE"); }
  else
  { dlog('',"overridden_web_cc_return is FALSE"); }

  return $overridden_web_cc_return;
}

/**
 * Lookup the credit card charge result in the WEBCC table
 */
function web_cc_lookup_status($return,$date,$time,$session,$customer)
{
  # wait for WebCC status change

  $return['rejection_errors'] = array();
  $return['transient_errors'] = FALSE;

  $overridden_web_cc_return = overridden_web_cc_return($customer);
  if ( $overridden_web_cc_return )
  { return $overridden_web_cc_return; }

  for ($i = 0; $i < 10; $i++)
  {
    sleep(5);

    $webcc_select_query = webcc_select_query(
      array(
        'date'    => $date,
        'time'    => $time,
        'session' => $session
      )
    );

    $cc = mssql_fetch_all_objects(logged_mssql_query($webcc_select_query));

    if ($cc && is_array($cc) && count($cc) > 0)
    {
      $return['status']         = strtoupper($cc[0]->STATUS);
      $return['transaction_id'] = $cc[0]->transactionid;
      $return['result']         = $cc[0]->RESULT;

## TEST ##
#if ( $i == 4 ) { $return['status'] = 'APPROVED'; }

      switch ( $return['status'] )
      {
        case 'APPROVED':

          $i = 10; # last
          break;

        case 'REJECTED':

          # parse $return['result'] for errors

          $parse_result = parse_webcc_rejection_errors( $return['result'] );

          $return['rejection_errors'] = $parse_result['rejection_errors'];
          $return['transient_errors'] = $parse_result['transient'];

          $i = 10; # last
          break;

        case "PENDING":
        case "WPENDING":
        case "LOOKUP":

          // do nothing, continue the loop
          break;

        default:
          $return['errors'][] = "WebCC status unhandled";

          $i = 10; # last
          break;
      }
    }
    else
    {
      $return['errors'][] = "Error while querying webcc (".mssql_get_last_message().")";
      $i = 10; # last
    }
  }

  return $return;
}

/**
 * parse_webcc_rejection_errors
 *
 * Parse WEBCC Rejection Errors
 *
 * @return array
 */
function parse_webcc_rejection_errors($result)
{
  $parse_result = array(
    'rejection_errors' => array(),
    'transient'        => FALSE
  );

/*
RESULT: CCV - No Credit Card Specified
RESULT: SGS-000001: D:DeclinedNNNM:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedNNNN:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedNYZM:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedNYZN:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedXXRN:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedXXSM:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedXXUM:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedXXUN:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedXXUP:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedXXUX:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedYNAN:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedYYYM:;AVS=;CVV2=
RESULT: SGS-000001: D:DeclinedYYYN:;AVS=;CVV2=
RESULT: SGS-000002: R:Referral (call voice cente
RESULT: SGS-002000: D:DeclinedNNNM:;AVS=;CVV2=
RESULT: SGS-002000: D:DeclinedNYZM:;AVS=;CVV2=
RESULT: SGS-002000: D:DeclinedXXUM:;AVS=;CVV2=
RESULT: SGS-002000: D:DeclinedYNAM:;AVS=;CVV2=
RESULT: SGS-002000: D:DeclinedYYYM:;AVS=;CVV2=
RESULT: SGS-002304: Credit card is expired.;AVS=
RESULT: SGS-005005: Duplicate transaction.;AVS=;
RESULT: SGS-020005: Error (Merchant config file 
RESULT: SGS-020008: Invalid Merchant ID.;AVS=;CV
RESULT: Connect timed out.;AVS=;CVV2=
*/

  if ( preg_match("/Declined/", $result) )
  {
    if ( preg_match("/NN/", $result) )
    {
      $parse_result['rejection_errors'][] = "Neither the address nor the zip code match.";
    }
    else if ( preg_match("/XX/", $result) )
    {
      $parse_result['rejection_errors'][] = "Card number not on file.";
    }
    else if ( preg_match("/NY/", $result) )
    {
      $parse_result['rejection_errors'][] = "Only the zip code matches.";
    }
    else if ( preg_match("/YN/", $result) )
    {
      $parse_result['rejection_errors'][] = "Only the address matches.";
    }
    else if ( preg_match("/DeclinedYYYM/", $result) )
    {
      $parse_result['rejection_errors'][] = "Declined (YYYM).";
    }
    else
    {
      $parse_result['rejection_errors'][] = $result;
    }
  }

  else if ( preg_match("/Connect timed out/", $result) )
  {
    $parse_result['rejection_errors'][] = "Webcc connect timed out.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/Referral/", $result) && preg_match("/call voice/", $result) )
  {
    $parse_result['rejection_errors'][] = "Referral (call voice center).";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/Credit card is expired/", $result) )
  {
    $parse_result['rejection_errors'][] = "Credit card is expired.";
  }
  else if ( preg_match("/Duplicate transaction/", $result) )
  {
    $parse_result['rejection_errors'][] = "Duplicate transaction.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/Please contact merchant/", $result) )
  {
    $parse_result['rejection_errors'][] = "Please contact merchant services.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/unknown error/", $result) )
  {
    $parse_result['rejection_errors'][] = "Unknown error.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/Error/", $result) && preg_match("/Merchant config file/", $result) )
  {
    $parse_result['rejection_errors'][] = "Error - Merchant config file.";
  }
  else if ( preg_match("/Invalid Merchant ID/", $result) )
  {
    $parse_result['rejection_errors'][] = "Invalid Merchant ID.";
  }
  else if ( preg_match("/Invalid XML/", $result) )
  {
    $parse_result['rejection_errors'][] = "Invalid XML.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/There was a gateway configuration/", $result) )
  {
    $parse_result['rejection_errors'][] = "Gateway configuration error.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/No credit card expiration year/", $result) )
  {
    $parse_result['rejection_errors'][] = " No credit card expiration year provided.";
  }
  else if ( preg_match("/Invalid credit card number/", $result) )
  {
    $parse_result['rejection_errors'][] = "Invalid credit card number.";
  }
  else if ( preg_match("/server encountered an error: Unsupported/", $result) )
  {
    $parse_result['rejection_errors'][] = "Unsupported credit card type.";
  }
  else if ( preg_match("/server encountered an error: General/", $result) )
  {
    $parse_result['rejection_errors'][] = "General Processor Error.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/order already exists/", $result) )
  {
    $parse_result['rejection_errors'][] = "The order already exists.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/The merchant is not setup/", $result) )
  {
    $parse_result['rejection_errors'][] = "The merchant is not setup to support the requested service.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/No approved authorization found/", $result) )
  {
    $parse_result['rejection_errors'][] = "No approved authorization found.";
  }
  else if ( preg_match("/No transaction to void found/", $result) )
  {
    $parse_result['rejection_errors'][] = "No transaction to void found.";
  }
  else if ( preg_match("/database error/", $result) )
  {
    $parse_result['rejection_errors'][] = "Database error.";
    $parse_result['transient']          = TRUE;
  }
  else if ( preg_match("/Not authorized to run a Credit Transaction/", $result) )
  {
    $parse_result['rejection_errors'][] = "Not authorized to run a Credit Transaction.";
  }
  else if ( preg_match("/Creditcard or check information is required/", $result) )
  {
    $parse_result['rejection_errors'][] = "Creditcard or check information is required.";
  }
  else if ( preg_match("/Unexpected AuthService Response/", $result) )
  {
    $parse_result['rejection_errors'][] = "Unexpected AuthService Response.";
    $parse_result['transient']          = TRUE;
  }
  else
  {
    $parse_result['rejection_errors'][] = $result;
  }

  return $parse_result;
}

/**
 * get_htt_customers_overlay_ultra
 *
 * What the heck is this doing here ???
 */
function get_htt_customers_overlay_ultra($params)
{
  $return = array( 'errors' => array() , 'success' => FALSE );

  $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query($params);

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));

  if ( ! is_array($query_result) )
  {
    $return['errors'][] = "Error while selecting from htt_customers_overlay_ultra (".mssql_get_last_message().")";
  }
  else
  if ( count($query_result) > 0 )
  {
    $return['data']    = $query_result[0];
    $return['success'] = TRUE;
  }
  else
  { $return['errors'][] = "No data from htt_customers_overlay_ultra"; }

  return $return;
}

/**
 * func_sweep_balance_to_stored_value
 *
 * opposite of func_sweep_stored_value:
 * move $params['sweep_amount'] from ACCOUNTS.BALANCE to HTT_CUSTOMERS_OVERLAY_ULTRA.STORED_VALUE
 * except we do not return the customer object
 *
 * @return array(array of strings: errors, boolean: success)
 */
function func_sweep_balance_to_stored_value($params)
{
  // init
  $result = array('errors' => array(), 'success' => FALSE);

  if ($params['customer'] instanceof Customer) {
    $params['customer'] = (object) (array_change_key_case((array) $params['customer'], CASE_UPPER));
  }

  $customer_id = $params['customer']->CUSTOMER_ID;
  $cos_id = empty($params['target_cos_id']) ? $params['customer']->COS_ID : $params['target_cos_id'];
  $commissionable = 0;

  // get customer info and check ACCOUNT.BALACE
  $customer = find_customer(make_find_ultra_customer_query_from_customer_id($customer_id));
  if (! $customer || $customer->BALANCE < 0)
  {
    $result['errors'][] = 'Invalid customer account or balance';
    return $result;
  }

  // determine sweep_value: given or full
  $sweep_value = empty($params['sweep_value']) ? $customer->BALANCE : $params['sweep_value'];
  if ($sweep_value > $customer->BALANCE)
  {
    $result['errors'][] = 'Given amount exceeds customer balance';
    return $result;
  }

  // begin transaction
  if ( ! start_mssql_transaction())
  {
    $result['errors'][] = 'Failed to begin DB transaction';
    return $result;
  }

  // update HTT_CUSTOMERS_OVERLAY_ULTRA.STORED_VALUE
  $query = htt_customers_overlay_ultra_update_query(array(
    'balance_to_add' => $sweep_value,
    'customer_id' => $customer_id));
  if (empty($query) || ! is_mssql_successful(logged_mssql_query($query)))
    $result['errors'][] = 'Failed to update HTT_CUSTOMERS_OVERLAY_ULTRA: ' . mssql_get_last_message();  

  // update ACCOUNT.BALANCE
  if (! count($result['errors']))
  {
    $query = accounts_update_query(array(
      'subtract_from_balance' => $sweep_value,
      'customer_id' => $customer_id));
    if (empty($query) || ! is_mssql_successful(logged_mssql_query($query)))
      $result['errors'][] = 'Failed to update ACCOUNTS: ' . mssql_get_last_message();
  }

  // record billing history
  if (! count($result['errors']))
  {
    // basic parameters
    $history_params = array(
      'customer_id'            => $customer_id,
      'date'                   => 'now',
      'cos_id'                 => $cos_id,
      'entry_type'             => 'REVERSE_SWEEP',
      'stored_value_change'    => $sweep_value,
      'balance_change'         => -$sweep_value,
      'package_balance_change' => 0,
      'charge_amount'          => 0,
      'reference'              => $params['reference'],
      'reference_source'       => get_reference_source($params['source']),
      'detail'                 => empty($params['detail']) ? 'ReverseSweepStoredValue' : $params['detail'],
      'description'            => 'Reverse sweep stored value',
      'result'                 => 'COMPLETE',
      'source'                 => $params['source'],
      'is_commissionable'      => $commissionable);

    // optional parameters
    foreach ( array('store_zipcode', 'store_id', 'clerk_id', 'terminal_id') as $nullable_field)
      if ( isset( $params[$nullable_field] ) )
        $history_params[$nullable_field] = $params[$nullable_field];
   
    // execute and check
    $query = htt_billing_history_insert_query($history_params);
    if (empty($query) || ! is_mssql_successful(logged_mssql_query($query)))
      $result['errors'][] = 'Failed to insert into htt_billing_history: ' . mssql_get_last_message();
  }

  // any errors at this point mean transaction must be rolled back
  if (count($result['errors']) && ! rollback_mssql_transaction())
    $result['errors'][] = 'Failed to rollback DB transaction: ' . mssql_get_last_message();
  elseif (! commit_mssql_transaction())
    $result['errors'][] = 'Failed to commit DB transaction: ' . mssql_get_last_message();
  else
    $result['success'] = TRUE;

  return $result;
}

/*
 * func_validate_pin_cards_ultra
 *
 * Validate Ultra 18 digit PIN
 *
 * @param string PIN
 * @return valid PIN object or NULL
 */
function func_validate_pin_cards_ultra($pin)
{
  $query = htt_inventory_pin_select_query(array( "pin" => $pin ));
  $pinObject = find_first($query);
  return $pinObject;
}

/**
 * func_validate_pin_cards_incomm
 *
 * Validate inComm 10 digit PIN
 *
 * @param string PIN
 * @return valid PIN object or NULL
 */
function func_validate_pin_cards_incomm($pin)
{
  $pinObject = NULL;
  
  $result = \Ultra\Lib\Incomm\statusInquiry($pin);
  $code = $result->get_data_key('RespCode');

  // return good response only if PIN is active, used or inactive
  if (in_array($code, array(INCOMM_CARD_ACTIVE, INCOMM_CARD_REDEEMED, INCOMM_CARD_DEACTIVE)))
  {
    // create a standard object and copy all inComm values
    $pinObject = new stdClass();
    foreach ($result->get_data_array() as $name => $value)
      $pinObject->$name = $value;
    $pinObject->pin_value = $result->get_data_key('FaceValue');

    // translate inComm return code into Ultra-style PIN status
    switch ($code)
    {
      case INCOMM_CARD_ACTIVE:
        $pinObject->status = 'AT_MASTER';
        break;

      case INCOMM_CARD_REDEEMED:
        $pinObject->status = 'USED';
        $pinObject->customer_used = 1; // fake customer
        $pinObject->last_changed_date = date_to_datetime(NULL, FALSE, TRUE); // fake date
        break;

      case INCOMM_CARD_DEACTIVE:
        $pinObject->status = 'AT_FOUNDRY';
        break;
    }
  }

  return $pinObject;  
}

/**
 * get_recovery_fee
 *
 * Recovery Fee should be calculated based on the following rules: 
 * - If the service charge amount is $10.00 or less, then the regulatory recovery fee is $0.50 per transaction. 
 * - If the service charge amount is $10.01 or more, then the regulatory recovery fee is $1.00 per transaction.
 * Amounts are in cents.
 *
 * @return array
 */
function get_recovery_fee( $amount )
{
  $recovery_fee_rule = "CC Recovery Rule v1";

  if ( ! $amount )
    return array(   0 , $recovery_fee_rule , '0' );
  elseif ( $amount > 1000 )
    return array( 100 , $recovery_fee_rule , 'Charge > $10' );
  else
    return array(  50 , $recovery_fee_rule , 'Charge <= $10' );
}

/**
 * get_sales_tax
 *
 * Invokes SureTax API
 * Sales Tax is dependent on the customer's billing zip code
 * If $billing_zip_code is NULL, we have to retrieve it from the DB (using $customer_id)
 * $amount is in cents
 *
 * @returns a Result object
 */
function get_sales_tax( $amount , $customer_id=NULL , $billing_zip_code=NULL , $account=NULL )
{
  dlog('', '(%s)', func_get_args());

  if ( ! $customer_id && ! $billing_zip_code && ! $account )
    return make_error_Result('get_sales_tax invoked with missing parameters');

  if ( !$billing_zip_code || !is_numeric($billing_zip_code) || ( strlen($billing_zip_code) != 5 ) || ! $account )
  {
    // get customer info from DB
    $customer = get_customer_from_customer_id($customer_id);

    if ( !$customer || !is_object($customer) )
      return make_error_Result("get_sales_tax could not load customer id $customer_id");

    // first we check CC_POSTAL_CODE, then POSTAL_CODE
    if     ( ( !$billing_zip_code || !is_numeric($billing_zip_code) || ( strlen($billing_zip_code) != 5 ) ) && $customer->CC_POSTAL_CODE )
      $billing_zip_code = $customer->CC_POSTAL_CODE;
    elseif ( ( !$billing_zip_code || !is_numeric($billing_zip_code) || ( strlen($billing_zip_code) != 5 ) ) && $customer->POSTAL_CODE )
      $billing_zip_code = $customer->POSTAL_CODE;

    if ( !$billing_zip_code || !is_numeric($billing_zip_code) || ( strlen($billing_zip_code) != 5 ) )
      return make_error_Result("get_sales_tax could not find a zip code for customer id $customer_id");

    if ( !$account )
      $account = $customer->ACCOUNT;
  }

  // invoke SureTax API
  return \Ultra\Lib\SureTax\perform_call(
    array(
      'amount'            => ($amount/100),
      'zip_code'          => trim($billing_zip_code),
      'account'           => $account
    )
  );
}

/**
 * calculate_taxes_fees
 *
 * Determine the Sales Tax and Regulatory fees that would be charged for a potential Credit Card purchase
 * Input:
 *  $amount is in cents
 * Output:
 *  $result_data['recovery_fee'] is in cents
 *  $result_data['sales_tax']    is in cents
 *
 * @returns a Result object
 */
function calculate_taxes_fees( $amount , $customer_id=NULL , $billing_zip_code=NULL , $account=NULL )
{
  dlog('', '(%s)', func_get_args());

  // invokes SureTax API
  $sales_tax_result = get_sales_tax( $amount , $customer_id , $billing_zip_code , $account );

  $result_data = array(
    'zip_code'             => '00000',

    'sales_tax'            => 0,
    'sales_tax_rule'       => 'SureTax API error',
    'sales_tax_percentage' => 0,

    'recovery_fee'       => 0,
    'recovery_fee_rule'  => 0,
    'recovery_fee_basis' => 0
  );

  if ( $sales_tax_result->is_failure() )
  {
    dlog('',"ESCALATION IMMEDIATE SureTax API error : call failed : %s",$sales_tax_result->get_errors());

    return make_ok_Result( $result_data , NULL , 'SureTax API error : call failed' );
  }

  // sanity check
  if ( !isset($sales_tax_result->data_array['TotalTax']) )
  {
    dlog('',"ESCALATION IMMEDIATE SureTax API error : call did not return meaningful data");

    return make_ok_Result( $result_data , NULL , 'SureTax API error : call did not return meaningful data' );
  }

  // logic for recovery fee
  list( $recovery_fee , $recovery_fee_rule , $recovery_fee_basis ) = get_recovery_fee( $amount );

  $result_data = array(
    'zip_code'             => $sales_tax_result->data_array['zip_code'],

    'sales_tax'            => 0,
    'sales_tax_rule'       => '',
    'sales_tax_percentage' => 0,

    'recovery_fee'       => $recovery_fee,
    'recovery_fee_rule'  => $recovery_fee_rule,
    'recovery_fee_basis' => $recovery_fee_basis
  );

  // for small amounts we may have no tax
  if ( $sales_tax_result->data_array['TotalTax'] == "0.00" )
  {
    dlog('','SureTax API returned 0 taxes');

    return make_ok_Result( $result_data );
  }

  // error handling
  if ( isset( $sales_tax_result->data_array['ItemMessages'] )
    && is_array( $sales_tax_result->data_array['ItemMessages'] )
    && count( $sales_tax_result->data_array['ItemMessages'] )
    && is_object($sales_tax_result->data_array['ItemMessages'][0])
    && property_exists( $sales_tax_result->data_array['ItemMessages'][0] , 'Message' )
  )
  {
    dlog('',"ESCALATION IMMEDIATE SureTax API error : message = %s",$sales_tax_result->data_array['ItemMessages'][0]->Message);

    return make_ok_Result( $result_data , NULL , 'SureTax API error : message = '.$sales_tax_result->data_array['ItemMessages'][0]->Message );
  }

  if ( !isset( $sales_tax_result->data_array['GroupList'] )
    || !is_array( $sales_tax_result->data_array['GroupList'] )
    || !count( $sales_tax_result->data_array['GroupList'] )
    || !property_exists( $sales_tax_result->data_array['GroupList'][0] , 'TaxList' )
    || !is_array( $sales_tax_result->data_array['GroupList'][0]->TaxList )
    || !count( $sales_tax_result->data_array['GroupList'][0]->TaxList )
  )
  {
    dlog('',"ESCALATION IMMEDIATE SureTax API error : API response invalid - %s",$sales_tax_result->data_array);

    return make_ok_Result( $result_data , NULL , 'SureTax API error : API response invalid' );
  }

  dlog('',"TaxList   = %s",$sales_tax_result->data_array['GroupList'][0]->TaxList );

  foreach ( $sales_tax_result->data_array['GroupList'][0]->TaxList as $tax_entry )
  {
    $suffix = substr( $tax_entry->TaxTypeCode , -2 );

    if ( in_array( $suffix , array('01','02','03','04','05') ) )
    {
      $result_data['sales_tax']            += ( $tax_entry->TaxAmount * 100 );
      $result_data['sales_tax_percentage'] += ( $tax_entry->TaxRate * $tax_entry->PercentTaxable );
    }
  }

  // prepare output data

  $result_data['sales_tax']          = floor( $result_data['sales_tax'] );
  $result_data['sales_tax_rule']     = 'SureTax Rule v1';

  $sales_tax_result->data_array = $result_data;

  dlog('', 'returning %s',$sales_tax_result->data_array);

  return $sales_tax_result;
}

/**
 * func_calculate_taxes_fees
 *
 * @param integer 5 digit zipcode
 * @param integer customer ID
 * @param integer account ID
 * @param integer amount in cents
 * @param string transaction type 'RECHARGE' or other
 * @return array (array errors, array rejection_errors, boolean transient_errors, array surcharges, array taxes_and_fees_data)
 */
function func_calculate_taxes_fees( $zip , $customer_id , $account , $amount , $transaction_type = NULL, $productType = null)
{
  $errors              = array();
  $rejection_errors    = array();
  $transient_errors    = FALSE;
  $surcharges          = 0;
  $taxes_and_fees_data = array();

  if ( !$zip || !is_numeric($zip) || ( strlen($zip) != 5 ) )
  {
    $errors[]           = 'No zip code found for this customer';
    $rejection_errors[] = 'No zip code found for this customer';
  }
  else
  {
    if (!empty($productType)) {
      $taxAndFeesCalc = (new AppContainer())->make(TaxesAndFeesCalculator::class);
      $customer = (new CustomerRepository())->getCombinedCustomerByCustomerId(
        $customer_id,
        ['u.BRAND_ID', 'a.COS_ID', 'POSTAL_CODE']
      );

      $result_taxes_fees = $taxAndFeesCalc->calculateTaxesAndFees($customer->brand_id, $customer->cos_id, $productType, $amount, $zip, $customer->postal_code);
    } else {
      $taxCalc = new \SalesTaxCalculator();
      $taxCalc->setBillingInfoByCustomerId($customer_id);

      // will set if input not NULL
      $taxCalc->setAccount($account);
      $taxCalc->setBillingZipCode($zip);
      $taxCalc->setAmount($amount);

      $result_taxes_fees = $taxCalc->calculateTaxesFees($transaction_type);
    }

    if ( $result_taxes_fees->is_failure() )
    {
      $errors           = $result_taxes_fees->get_errors();
      $rejection_errors = $result_taxes_fees->get_errors();
    }
    else
    {
      $taxes_and_fees_data = $result_taxes_fees->data_array;

      // surcharges and fees (in cents)
      foreach (array('mts_tax', 'sales_tax', 'recovery_fee') as $item)
        if ( ! empty($result_taxes_fees->data_array[$item])) // omit zero amount surcharges
          $surcharges += $result_taxes_fees->data_array[$item];
    }
  }

  return array( $errors , $rejection_errors , $transient_errors , $surcharges , $taxes_and_fees_data );
}

/**
 * default_taxes_and_fees_data
 */
function default_taxes_and_fees_data()
{
  return array(
    'mts_tax'              => 0,
    'sales_tax'            => 0,
    'sales_tax_rule'       => '',
    'sales_tax_percentage' => 0,

    'recovery_fee'       => 0,
    'recovery_fee_rule'  => '',
    'recovery_fee_basis' => ''
  );
}

/**
 * default_history_params_data
 */
function default_history_params_data( $customer_id , $cos_id , $description , $source , $is_commissionable )
{
  return array(
    'customer_id'       => $customer_id,
    'cos_id'            => $cos_id,
    'date'              => 'now',
    'description'       => $description,
    'result'            => 'COMPLETE',
    'source'            => $source,
    'is_commissionable' => $is_commissionable
  );
}

/**
 * compute_subscriber_owed_amount
 *
 * Determine what the subscriber owes us - in cents - taking MONTHLY_RENEWAL_TARGET into account
 *
 * @return integer
 */
function compute_subscriber_owed_amount($customer)
{
  dlog('',"customer COS_ID = ".$customer->COS_ID." ; MONTHLY_RENEWAL_TARGET = ".$customer->MONTHLY_RENEWAL_TARGET);

  $plan = ( ( ! is_null($customer->MONTHLY_RENEWAL_TARGET) ) && ( $customer->MONTHLY_RENEWAL_TARGET != '' ) )
    ? $customer->MONTHLY_RENEWAL_TARGET
    : get_plan_from_cos_id( $customer->COS_ID );

  // rather not give plans away if error
  $subscriber_owed_amount = 0;

  $cos_id = \get_cos_id_from_plan($plan);

  // // if is mint plan, get multi month overlay
  // check if renewal or reset by calculating months remaining
  if ($multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customer->CUSTOMER_ID))
  {
    $monthsRemaining   = $multiMonthOverlay->TOTAL_MONTHS - $multiMonthOverlay->UTILIZED_MONTHS;

    \logit('multi month overlay ' . json_encode($multiMonthOverlay));

    // if there are months remaining, plan reset
    // else customer owes money
    $subscriber_owed_amount = ( $monthsRemaining > 0 )
      ? 0
      : get_plan_cost_from_cos_id($cos_id);
  }
  else
    $subscriber_owed_amount = get_plan_cost_from_cos_id($cos_id);

  dlog('',"customer applicable plan = $plan ; customer applicable cost = $subscriber_owed_amount");

  return $subscriber_owed_amount;
}

function subscriber_owed_amount_recurring_bolt_ons($customer_id)
{
  $bolt_ons = get_bolt_ons_values_from_customer_options( $customer_id );

  dlog('',"bolt_ons = %s",$bolt_ons);

  return ( $bolt_ons )
    ? ( ( array_sum( array_values($bolt_ons) ) ) * 100 )
    : 0;
}

/**
 * compute_subscriber_owed_amount_current_plan
 *
 * Determine what the subscriber owes us if he stays in the current plan - in cents
 *
 * @return array
 */
function compute_subscriber_owed_amount_current_plan( $customer )
{
  dlog('',"customer COS_ID = ".$customer->COS_ID);

  $data = array(
    'subscriber_owed_amount_current_plan'       => 0,
    'subscriber_owed_amount_recurring_bolt_ons' => 0
  );

  // Plan Cost

  $data['subscriber_owed_amount_current_plan'] = get_plan_cost_from_cos_id( $customer->COS_ID );

  // Recurring Bolt On Cost

  $bolt_ons = get_bolt_ons_values_from_customer_options( $customer->customer_id );

  dlog('',"bolt_ons = %s",$bolt_ons);

  if ( $bolt_ons )
    $data['subscriber_owed_amount_recurring_bolt_ons'] = ( ( array_sum( array_values($bolt_ons) ) ) * 100 ) ;

  return $data;
}

/**
 * process_recurring_bolt_ons
 *
 * Triggers Recurring Bolt Ons effects for a customer
 * Note, by the time we reach this step, we have already swept the STORED_VALUE into the wallet (BALANCE).
 *
 * @return array
 */
function process_recurring_bolt_ons( $params )
{
  // load customer data from DB table ACCOUNTS
  $customer_data = mssql_fetch_all_objects(
    logged_mssql_query(
      \Ultra\Lib\DB\makeSelectQuery(
        'ACCOUNTS',
        NULL,
        array('CUSTOMER_ID','BALANCE'),
        array('CUSTOMER_ID' => $params['customer_id']),
        NULL
      )
    )
  );

  if ( ! ( $customer_data && is_array($customer_data) && count($customer_data) ) )
  {
    dlog('',"Customer not found");

    return array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
  }

  dlog('',"customer_data = %s",$customer_data);

  // add Recurring Bolt Ons if customer's balance is sufficient
  $result = \Ultra\Lib\BoltOn\addBoltOnsRecurring( $params['customer_id'] , $customer_data[0]->BALANCE, $params['plan'] );

  // $result is ignored

  // does not return errors or failures because it's used in the state machine
  return array( 'sent' => TRUE, 'success' => TRUE, 'errors' => array() );
}

/**
 * checkExcessBalance
 *
 * Verify that applying a given amount will not exceed subscribers total balance
 *
 * @param Object customer
 * @param Integer amount to apply in cents
 * @return Boolean TRUE if balance will exceed maximum value, FALSE otherwise
 */
function checkExcessBalance($customer, $amount)
{
  if ( ! is_object($customer) || ! property_exists($customer, 'BALANCE') || ! property_exists($customer, 'stored_value'))
  {
    dlog('', 'ERROR: invalid parameter customer');
    return TRUE;
  }

  if ( ! ctype_digit((string)$amount))
  {
    dlog('', "ERROR: parameter amount '$amount' is not integer");
    return TRUE;
  }

  if ($customer->BALANCE * 100 + $customer->stored_value * 100 + $amount > MAX_ACCOUNT_BALANCE)
  {
    dlog('', 'will exceed max allowed balance: monthly $%.2f + wallet $%.2f + amount $%.2f > $%.2f', $customer->stored_value, $customer->BALANCE, $amount / 100, MAX_ACCOUNT_BALANCE / 100);
    return TRUE;
  }

  return FALSE;
}

/**
 * verifyAmountDestination
 *
 * Verify (or deduce) that suggested destination matches the amount for the given subscriber
 *
 * @example: call verifyProviderSkuAmount to get destination, then call verifyAmountDestination to confirm that amount and destination are applicable to subscriber
 * @param Object subscriber
 * @param String destination MONTHLY, WALLET, SMART (will change on return)
 * @param Integer amount in cents
 * @return String destination WALLET, MONTHLY or NULL on error
 */
function verifyAmountDestination($customer, $destination, $amount)
{
  // check input parameters
  if ( ! is_object($customer) || ! property_exists($customer, 'customer_id') || ! property_exists($customer, 'COS_ID') || ! property_exists($customer, 'MONTHLY_RENEWAL_TARGET'))
  {
    dlog('', 'ERROR: invalid parameter customer');
    return NULL;
  }
  if (empty($destination) || ! in_array($destination, array('MONTHLY', 'WALLET', 'SMART')))
  {
    dlog('', "ERROR: invalid parameter destination '$destination'");
    return NULL;
  }

  // accept any amount into WALLET
  if ($destination == 'WALLET')
    return $destination;

  // MONTHLY amount must be equal of higher than the base plan cost plus remaining balance
  $base = get_plan_cost_by_cos_id(empty($customer->MONTHLY_RENEWAL_TARGET) ? $customer->COS_ID : get_cos_id_from_plan($customer->MONTHLY_RENEWAL_TARGET)) * 100;
  if ($destination == 'MONTHLY')
  {
    if ($amount >= $base - ($customer->stored_value + $customer->BALANCE) * 100)
      return $destination;

    // unsiffucient amount to cover MONTHLY
    dlog('', 'ERROR: cannot apply $%.2f to %s because it is less than subscriber %s base plan $%d less existing funds $%.2f', $amount / 100, $destination, empty($customer->MONTHLY_RENEWAL_TARGET) ? 'current' : 'future', $base / 100, $customer->stored_value + $customer->BALANCE);
    return NULL;
  }
  
  // map SMART into MONTHLY when exact match (base of full cost) or WALLET otherwise
  if ($amount == $base + ($customer->stored_value + $customer->BALANCE) * 100)
    return 'MONTHLY';
  $full = $base + get_bolt_ons_costs_by_customer_id($customer->customer_id) * 100;
  if ($amount == $full)
    return 'MONTHLY';

  // no match: apply to WALLET
  return 'WALLET';
}


/**
 * verifyProviderSku
 *
 * validate provider, SKU and its brand in this environment
 *
 * @param String provider name
 * @param String SKU or UPC
 * @param Object customer
 * @return Boolean TRUE if SKU is valid or FALSE otherwise
 */
function verifyProviderSku($provider, $sku, $customer = NULL)
{
  // get provider SKUs
  if ( ! $map = \Ultra\UltraConfig\getPaymentProviderSkuMap($provider))
    return logError("unknown provider '$provider'");

  // confirm that SKU is in the map
  if (empty($map->$sku))
    return logError("unknown SKU '$sku'");

  // see API-346
  if (Ultra\UltraConfig\isPaymentProviderBrandValidationEnabled($provider))
  {
    // validate SKU brand
    list($min, $max, $destination, $source, $brand) = $map->$sku;
    if ( ! Ultra\UltraConfig\isBrandNameAllowed($brand))
      return logError("environment forbids $provider SKU $sku brand $brand");

    // if customer is given then also validate payment brand against customer
    if ($customer)
    {
      $customerBrand = Ultra\UltraConfig\getBrandFromBrandId(empty($customer->BRAND_ID) ? 1 : $customer->BRAND_ID);
      if ($brand != $customerBrand['short_name'])
        return logError("payment brand $brand does not match customer brand {$customerBrand['short_name']}");
    }
  }

  return TRUE;
}


/**
 * verifyProviderSkuAmount
 *
 * Check if provider SKU is known and matches the amount given; return its destination
 *
 * @param String provider name
 * @param String SKU or UPC
 * @param Integer amount in cents
 * @return String SKU destination (WALLET, MONTHLY, SMART) or NULL on failure
 */
function verifyProviderSkuAmount($provider, $sku, $amount)
{
  // get provider SKUs
  if ( ! $map = \Ultra\UltraConfig\getPaymentProviderSkuMap($provider))
    return logError("unknown provider '$provider'");

  // find the SKU in the map
  if (empty($map->$sku))
    return logError("unknown SKU '$sku'");

  // get SKU details
  list($min, $max, $destination, $source, $brand) = $map->$sku;
  logInfo("mapped $provider SKU $sku -> MAX $max, MIN: $min, DESTINATION: $destination, SOURCE: $source, BRAND: $brand");

  // verify amount
  if ($amount < $min || $amount > $max)
    return logError("amount $amount is out of bounds $min - $max");

  return $destination;
}


/**
 * funcBogoEnrollment
 *
 * Enroll in BOGO if ICCID is within a specific range
 *
 * @see BOGO-30
 * @param Integer customer ID
 * @param String ICCID
 * @return Object Result
 */
function funcBogoEnrollment($customer_id, $iccid, $cos_id)
{
  $result = new Result();

  // check parameters
  if (empty($customer_id) || empty($iccid))
  {
    dlog('', 'ERROR: missing parameters: %s', func_get_args());
    return $result;
  }
  $iccid = luhnenize($iccid);

  // check if ICCID is within range
  $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.INCOMM_INVENTORY_SIM', 1,
    array('ICCID', 'CUSTOMER_ID', 'TYPE'),
    array('ICCID' => $iccid, 'EXPIRES_DATE' => 'future'),
    NULL, NULL, TRUE);

  if ($row = find_first($sql))
  {
    // check if not already used by another subscriber
    if ($row->CUSTOMER_ID && $row->CUSTOMER_ID != $customer_id)
    {
      dlog('', "ICCID $iccid already used for customer $customer_id");
      return $result;
    }

    $option = NULL;

    switch ($row->TYPE)
    {
      case 'BOGO':
        $option = BILLING_OPTION_ATTRIBUTE_BOGO_MONTH;
        break;
      case '7_11_BOGO':
        $option = BILLING_OPTION_ATTRIBUTE_7_11_BOGO;
        break;
      default:
        dlog('', 'ERROR: unhandled bogo promotion type');
        return $result;
    }

    // check if not already enrolled
    $enrolled = get_ultra_customer_options_by_customer_id($customer_id, $option);
    if ( ! count($enrolled) && $option)
    {
      $plan_cost = get_plan_cost_by_cos_id($cos_id);

      // BOGO: buy one get one [free]
      if ( ! enroll_bogo_month($customer_id, $plan_cost, $option) )
        dlog('', "ERROR: failed BOGO enroll for customer $customer_id for ICCID $iccid");

      $sql = ultra_incomm_inventory_sim_update_customer_id_sql($iccid, $customer_id, $plan_cost);
      if ( ! is_mssql_successful(logged_mssql_query($sql)))
        dlog('', "ERROR: updating INCOMM_INVENTORY_SIM for customer $customer_id for ICCID $iccid");
    }
  }

  $result->succeed();
  return $result;
}

/**
 * funcMultiMonthEnrollment
 *
 * Enroll in MULTI_MONTH for $customer_id and $iccid
 *
 * @param  Integer customer ID
 * @param  String ICCID
 * @return Object Result
 */
function funcMultiMonthEnrollment($customer_id, $iccid)
{
  $result = new Result();

  if (empty($customer_id) || empty($iccid))
  {
    dlog('', 'ERROR: missing required parameters: %s', func_get_args());
    return $result;
  }

  $iccid = luhnenize($iccid);
  $sim   = get_htt_inventory_sim_from_iccid($iccid);

  if ($sim)
  {
    if ( ! empty($sim->STORED_VALUE) && $sim->DURATION > 1 )
    {
      if (multi_month_info($customer_id))
      {
        dlog('', 'ERROR: Customer %d already enrolled in a MULTI_MONTH plan', $customer_id);
        return $result;
      }

      $plan_short = 'M' . $sim->STORED_VALUE;

      if ( ! enroll_multi_month($customer_id, $plan_short, $sim->DURATION))
        dlog('', "ERROR: failed MULTI_MONTH enroll for customer $customer_id for ICCID $iccid");
    }
  }

  $result->succeed();
  return $result;
}

/**
 * funcCheckFinalMultiMonth
 */
function funcCheckFinalMultiMonth($customer_id)
{
  $result = new Result();

  if (empty($customer_id))
  {
    dlog('', 'ERROR: missing required parameters: %s', func_get_args());
    return $result;
  }

  if ($multi_month_info = multi_month_info($customer_id))
  {
    if ($multi_month_info['months_left'] == 0)
    {
      $account = get_account_from_customer_id($customer_id, array('COS_ID'));

      if ( empty($account->COS_ID) )
      {
        dlog('', 'ERROR: Could not find account for customer_id : ' . $customer_id);
        return $result;
      }

      $new_cos_id = get_cos_id_from_plan(get_monthly_plan_from_cos_id($account->COS_ID));

      if ( empty($new_cos_id) )
      {
        dlog('', 'ERROR: get_monthly_plan_from_cos_id could not find plan for cos_id : ' . $account->COS_ID);
        return $result;
      }

      $sql = accounts_update_query(array('customer_id' => $customer_id, 'cos_id' => $new_cos_id));
      if ( ! is_mssql_successful(logged_mssql_query($sql)) )
      {
        dlog('', 'ERROR: Could not update account for customer_id : ' . $customer_id);
        return $result;
      }

      $result = remove_multi_month($customer_id);
      if ($result->is_failure())
      {
        dlog('', 'ERROR: Could not update ultra_customer_options for customer_id : ' . $customer_id);
        return $result;
      }
    }
  }

  $result->succeed();
  return $result;
}


/**
 * validateFactoredCommission
 *
 * check that subscriber's SIM commissions can be factored (aka instand spiff): SIM inventory assigned to Ultra 'alternative' master agents cannot be factored
 * @see PROD-1855
 * @param Integer customer ID, required if ICCID is not given
 * @param String ICCID, required if customer ID is not given
 * @returns String NULL if factoring is allowed or error message otherwise
 */
function validateFactoredCommission($master_agent)
{
  // check this master against known alternative master agents
  if (empty($master_agent))
    return NULL;

  if ( ! $masters = find_credential('ultra/master_agents/alternative'))
    return __FUNCTION__ . ': failed to retrieve list of alternative master agents';

  if (in_array($master_agent, explode(' ', $masters)))
    return __FUNCTION__ . ": subscriber does not quality for factored commissions";

  return NULL;
}


/**
 * refundWebPosCustomer
 * validate that given customer is acquired via Web POS and refund plan cost back to ACCOUNTS.BALANCE
 * @param Integer customer ID
 */
function refundWebPosCustomer($customerId)
{
  // validate that given subscriber is Web POS
  if ( ! $customer = get_webpos_customer($customerId))
  {
    logInfo("Customer $customerId is not acquired via Web POS");
    return make_ok_Result();
  }

  // reverse all SPEND transactions
  $transactions = get_current_cycle_purchase_history($customerId, 'SPEND');
  foreach ($transactions as $transaction)
  {
    $params = array(
      'customer'          => $transaction,
      'amount'            => -$transaction->BALANCE_CHANGE,
      'reason'            => __FUNCTION__,
      'reference'         => $customer->WEBPOS_ACTIONS_ID,
      'source'            => 'WEBPOS',
      'reference_source'  => get_reference_source('WEBPOS'),
      'entry_type'        => 'REVERSE_SWEEP',
      'reactivate_now'    => FALSE);
    func_add_balance($params);
   }

   return make_ok_Result();
}

/**
 * online_auto_enroll
 * supports claiming of credit card and auto enrollment if ONLINE_ORDERS row exists
 * @param  Integer customer ID
 * @param  String  iccid
 * @param  Array   repos for mock injection to support tests
 * @return Result object
 */
function online_auto_enroll($customer_id, $iccid, $repos = null)
{
  $result = new Result();

  teldata_change_db();

  $orderAPI       = null;
  $customerRepo   = null;
  $creditCardRepo = null;

  // requried repos
  if ($repos && is_array($repos))
  {
    // supports tests
    $orderAPI       = $repos['order_api'];
    $customerRepo   = $repos['customer_repo'];
    $creditCardRepo = $repos['credit_card_repo'];
  }
  else
  {
    $orderAPI       = new \Ultra\Lib\Services\OnlineSalesAPI();
    $customerRepo   = new \Ultra\Customers\Repositories\Mssql\CustomerRepository();
    $creditCardRepo = new \Ultra\CreditCards\Repositories\Mssql\CreditCardRepository();
  }

  $apiResult = $orderAPI->getOrder($iccid);
  if (!$apiResult->is_success()) {
    foreach ($apiResult->get_errors() as $error) {
      \logError($error);
    }

    $result->succeed();
    return $result;
  }

  $online_sale = $apiResult->data_array['order'];

  // don't do anything if auto_enroll is disabled
  if (!$online_sale->auto_enroll) {
    \logInfo('Auto enroll is disabled');
    $result->succeed();
    return $result;
  }

  if ($creditCardRepo->getUltraCCHolderByDetails(
    [
      'customer_id'   => $customer_id,
      'bin'           => $online_sale->bin,
      'last_four'     => $online_sale->last_four,
      'expires_date'  => $online_sale->expires_date
    ]
  ))
  {
    \logInfo('Credit card already registered with CUSTOMER_ID ' . $customer_id);
  }
  else
  {
    // returns scope identity on true
    $check_cc_holders = $creditCardRepo->addToUltraCCHolders(
      [
        'customer_id'    => $customer_id,
        'bin'            => $online_sale->bin,
        'last_four'      => $online_sale->last_four,
        'expires_date'   => $online_sale->expires_date,
        'cvv_validation' => $online_sale->cvv_validation,
        'avs_validation' => $online_sale->avs_validation
      ]
    );

    if ( ! $check_cc_holders)
    {
      $result->add_error('error inserting cc holder');
      return $result;
    }

    if ($cc_holders_id = $check_cc_holders)
    {
      $check_cc_holder_tokens = $creditCardRepo->addToUltraCCHolderTokens(
        [
          'cc_holders_id'    => $cc_holders_id,
          'gateway'          => $online_sale->gateway,
          'merchant_account' => $online_sale->merchant_account,
          'token'            => $online_sale->token
        ]
      );
    }

    if ( ! $check_cc_holder_tokens)
    {
      $result->add_error('error inserting cc holder token');
      return $result;
    }
  }

  // update credit card info in customers table
  $check_update_customer = $customerRepo->updateCustomerByCustomerId($customer_id, [
    'cc_name'         => $online_sale->first_name . ' ' . $online_sale->last_name,
    'cc_address1'     => $online_sale->address1,
    'cc_address2'     => $online_sale->address2,
    'cc_city'         => $online_sale->city,
    'cc_state_region' => $online_sale->state,
    'cc_postal_code'  => $online_sale->postal_code,
    'cc_exp_date'     => $online_sale->expires_date
  ]);

  if ( ! $check_update_customer)
  {
    $result->add_error('error updating customer info');
    return $result;
  }

  if ($online_sale->auto_enroll) {
    \Ultra\Lib\DB\Customer\updateAutoRecharge($customer_id, $online_sale->auto_enroll);
    (new \Ultra\Messaging\Messenger())->enqueueImmediateSms(
      $customer_id,
      'auto_recharge_active',
      ['last_four' => $online_sale->last_four]
    );
  }

  $result->succeed();
  return $result;
}

/**
 * inserts initial entry into ULTRA.MULTI_MONTH_OVERLAY
 * @param  Integer customer ID
 * @param  String  cos_id
 * @return Result  object
 */
function multiMonthOverlayEnrollment($customer_id, $cos_id)
{
  $result = new \Result();

  $total_months = \Ultra\UltraConfig\planMonthsByCosId($cos_id);
  if ( ! $total_months)
  {
    $result->succeed();
    return $result; // not a multi month plan
  }

  // TODO abstract cycle expires calculation here and checkMonthOverlay
  $total_days    = ($total_months == 12) ? 365 : $total_months * 30;
  $cycle_expires = date('Y-m-d H:i:s', strtotime("$total_days days - 1 day", time()));

  // insert or update multi_month_overlay record
  $sqlResult = ultra_multi_month_overlay_set(
    $customer_id,
    $cycle_expires,
    $total_months // first month deducted
  );

  if ($sqlResult) $result->succeed();
  else $result->add_error('error inserting into multi month overlay');

  return $result;
}

/**
 * checks for multi month overlay
 * on reset, increases UTILIZED_MONTHS
 * on renewal, updates cycle expires
 * @param  Integer customer ID
 * @param  String  cos_id
 * @return Result  object
 */
function checkMonthOverlay($customer_id, $cos_id)
{
  $result = new \Result();

  // skip if not mint cos_id
  $total_months = \Ultra\UltraConfig\planMonthsByCosId($cos_id);
  if ( ! $total_months)
  {
    $result->succeed();
    return $result;
  }

  $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customer_id);

  // if not multi month
  if ( ! $multiMonthOverlay)
  {
    $result->add_error('error : is multi month cos_id but missing multi month overlay for customer');
    return $result;
  }

  $params = NULL;

  // is renewal, then reset row
  if ($multiMonthOverlay->MONTHS_REMAINING === 0)
  {
    $total_days = ($total_months == 12) ? 365 : $total_months * 30;

    $cycle_expires = date('Y-m-d H:i:s', strtotime("$total_days days - 1 day", time()));

    $params = [
      'cycle_started'   => 'GETUTCDATE()',
      'cycle_expires'   => "'$cycle_expires'",
      'total_months'    => $total_months,
      'utilized_months' => 1
    ];
  }
  // is reset, incrase utilized months
  else
  {
    $params = [
      'utilized_months' => $multiMonthOverlay->UTILIZED_MONTHS + 1
    ];
  }

  // run sql and check
  if ( ! $params || ! ultra_multi_month_overlay_update($customer_id, $params) )
    $result->add_error('error updating multi month overlay for customer');
  else
    $result->succeed();

  return $result;
}

/**
 * calculate the plan cost DELTA for MINT mulit-month plans
 * @param  Integer customer ID
 * @param  String  from plan short name
 * @param  String  to plan short name
 * @return Result  object
 */
function calculateMintPlanDeltaCost($customer_id, $from_plan, $to_plan)
{
  $result = new \Result();

  $multiMonthOverlay = ultra_multi_month_overlay_from_customer_id($customer_id);

  $timeNow = time();

  $cycleStarted = $multiMonthOverlay->CYCLE_STARTED;

  // $daysInto = (int)(($timeNow - strtotime($cycleStarted)) / 86400);

  $dateTime = new \DateTime(date('y-m-d', strtotime($cycleStarted)));
  $daysInto = $dateTime->diff(new \DateTime())->days;

  $fromCosId = get_cos_id_from_plan($from_plan);
  $toCosId   = get_cos_id_from_plan($to_plan);

  $fromMonths = \Ultra\UltraConfig\planMonthsByCosId($fromCosId);
  $toMonths   = \Ultra\UltraConfig\planMonthsByCosId($toCosId);

  $fromLengthDays = ($fromMonths == 12) ? 365 : $fromMonths * 30;
  $toLengthDays   = ($toMonths   == 12) ? 365 : $toMonths   * 30;

  $fromCost = get_plan_cost_from_cos_id($fromCosId);
  $toCost   = get_plan_cost_from_cos_id($toCosId);

  if ($fromLengthDays == 0 || $toLengthDays == 0)
    return make_error_Result('ERROR: terminating before dividing by zero');

  $amountOwed = ((($daysInto / $fromLengthDays) * $fromCost) + ((($toLengthDays - $daysInto) / $toLengthDays) * $toCost)) - $fromCost;

  if ($amountOwed <= 0)
    return make_error_Result('ERROR: calculateMintPlanDeltaCost should not return 0 amount owed');

  $result->add_to_data_array('amount_owed', (int)$amountOwed);

  return $result;
}

function checkProductV1($subscriber, $product, $subproduct, $amount)
{
  // check max account balance
  if (\checkExcessBalance($subscriber, $amount))
    throw new \Exception('ERR_API_INVALID_ARGUMENTS: Your charge would exceed the maximum allowed balance on your wallet');

  // if applying to MONTHLY then load amount must cover plan cost (with bolt ons or without)
  if ($product == 'ULTRA_PLAN_RECHARGE')
  {
    // subproduct is required
    if (empty($subproduct))
      throw new \Exception('ERR_API_INVALID_ARGUMENTS: One or more required parameters are missing');

    $subproduct_numeric = preg_replace("/[^0-9,.]/", "", $subproduct);

    // subproduct must match amount
    if ($subproduct_numeric * 100 != $amount)
    {
      dlog('', 'subproduct amount %s does not match charge amount %s', $subproduct_numeric * 100, $amount);
      throw new \Exception('ERR_API_INVALID_ARGUMENTS: Invalid product or subproduct');
    }

    // verify that amount covers minium recharge amount without bolt ons
    $cost = \get_plan_cost_by_cos_id(
      $subscriber->MONTHLY_RENEWAL_TARGET
        ? \get_cos_id_from_plan($subscriber->MONTHLY_RENEWAL_TARGET)
        : $subscriber->COS_ID
    );

    if ($amount < ($cost - $subscriber->stored_value - $subscriber->BALANCE) * 100)
    {
      dlog('', 'insufficient recharge amount: load $%.2f < (cost $%.2f - stored $%.2f - balance $%.2f)', 
        $amount / 100,
        $cost,
        $subscriber->stored_value,
        $subscriber->BALANCE
      );

      throw new \Exception('ERR_API_INVALID_ARGUMENTS: The given plan does not match the given amount');
    }

    // convert subproduct from L19-L59 to PQ19-PQ59 if activating
    if (in_array($subscriber->plan_state, array(STATE_NEUTRAL, STATE_PORT_IN_REQUESTED, STATE_PROVISIONED)))
      $subproduct = 'PQ' . $subproduct_numeric;
  }

  return $subproduct;
}
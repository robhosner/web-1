<?php

require_once('cosid_constants.php');
require_once('classes/Result.php');
require_once('db.php');
require_once('db/accounts.php');
require_once('db/htt_action_log.php');
require_once('db/htt_action_parameter_log.php');
require_once('db/htt_customers_overlay_ultra.php');
require_once('db/htt_plan_tracker.php');
require_once('db/htt_transition_log.php');
require_once('db/ultra_activation_history.php');
require_once('db/ultra_promotional_plans.php');
require_once('lib/inventory/functions.php');
require_once('lib/inventory/functions_shipwire.php');
require_once('lib/messaging/functions.php');
require_once('lib/partner-validate.php');
require_once('lib/payments/functions.php');
require_once('lib/provisioning/functions.php');
require_once('lib/state_machine/aspider.php');
require_once('lib/transitions/actions.php');
require_once('lib/transitions/html.php');
require_once('lib/underscore/underscore.php');
require_once('lib/util-common.php');
require_once('classes/postageapp.inc.php');
require_once('Ultra/Lib/MiddleWare/Adapter/Control.php');
require_once('Ultra/Lib/MVNE/Adapter.php');
require_once('Ultra/Lib/Util/Redis.php');
require_once('Ultra/Lib/Util/Redis/Action.php');
require_once('Ultra/Lib/Util/Redis/Transition.php');


// new state machine
require_once 'Ultra/Lib/StateMachine.php';
#require_once 'Primo/Lib/StateMachine/DataEngineMongoDB.php';
require_once 'Ultra/Lib/StateMachine/DataEngineMSSQL.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRAM.php';
require_once 'Ultra/Lib/StateMachine/DataEngineRedis.php';

# Ultra specific actions and requirements
require_once 'Ultra/Lib/StateMachine/Action/functions.php';
require_once 'Ultra/Lib/StateMachine/Requirement/functions.php';


# constants
if(!defined('ACTIONS'))           define ('ACTIONS',           'actions');
if(!defined('ARRIVAL_ACTIONS'))   define ('ARRIVAL_ACTIONS',   'arrival_actions');
if(!defined('DEPARTURE_ACTIONS')) define ('DEPARTURE_ACTIONS', 'departure_actions');

// transition resolver redis key which is appended with either CUSTOMER_ID or TRANSITION_UUID
define('TRANSITION_SEMAPHORE_PREFIX', 'transition/resolving/');
define('CUSTOMER_SEMAPHORE_PREFIX', 'transition/resolving/customer_id/');

# global variables
$blocked = array();
$current_action = NULL;
$action_history = array(); # used in run_or_enqueue_db_action

register_shutdown_function('cleanup_current_action');

/**
 * cleanup_current_action
 *
 * Try to handle a fatal error during execution of an action. Introduced because MS SQL driver creates uncatchable exceptions.
 *
 * @return NULL
 */
function cleanup_current_action()
{
  global $current_action;

  if ($current_action == NULL) return;

  $error = error_get_last();
  if($error !== NULL)
  {
    dlog('',
         "[SHUTDOWN] while running action %s! file: %s, line %s, message %s",
         $current_action->ACTION_UUID,
         $error['file'],
         $error['line'],
         $error['message']);

    /* try to clean up the action */
    $check = run_sql_and_check(sprintf("UPDATE htt_action_log
SET status = 'ABORTED'
WHERE action_uuid = '%s' AND status LIKE 'RUNNING%'",
                              $current_action->ACTION_UUID));

    if ( ! $check ) { dlog('',"error while updating htt_action_log"); }
  }
  else
  {
    dlog('', "[SHUTDOWN] while running an action!");
  }

  /* make sure this is not run again even accidentally */
  $current_action = NULL;
}

/**
 * get_lonely_states
 *
 * States without a plan
 *
 * @return array
 */
function get_lonely_states()
{
  return array('Cancelled', 'Neutral');
}

/**
 * get_states
 *
 * @return array
 */
function get_states()
{
  // Active          has SERVICE,SIM,MSISDN
  // Provisioned     has SIM,MSISDN
  // Promo Unused    has SIM,MSISDN
  // PortInRequested has SIM,MSISDN,BALANCE
  // PortInDenied    has SIM,MSISDN,BALANCE
  // Pre-Funded      has BALANCE

  return array('Active', 'Provisioned', 'Suspended', 'Port-In Requested', 'Port-In Denied', "Pre-Funded" , 'Promo Unused');
}

/**
 * get_all_states
 *
 * @return array
 */
function get_all_states()
{
  return array_merge(
    get_states(),
    get_lonely_states()
  );
}

/**
 * ultra_plans
 *
 * Returns an array of Ultra Plans
 *
 * @return array
 */
function ultra_plans()
{
  global $credentials;
  /* $credentials['ultra_develop_tel_api_accounts'] = $cred_config; */

  $plans = array();

  $plan_substates = get_states();

  $planConfig = \PlanConfig::Instance();
  $plans = $planConfig->ultraPlans();
  foreach ($plans as &$plan)
    $plan['states'] = $plan_substates;

  /*
  if ( !$credentials || !is_array($credentials) )
    dlog('',"FATAL ERROR : credentials not available");
  else
    foreach ($credentials as $cname => $cred)
    foreach ($cred as $key => $val)
    {
      if (preg_match(',plans/(Ultra)/([^/]+)/(.+),', $key, $matches))
      {
        $plans[$matches[2]][$matches[3]] = $val;
        $plans[$matches[2]]['states'] = $plan_substates;
        $plans[$matches[2]]['plan_group'] = $matches[1];
      }
    }
  */

  $plans['STANDBY']['states'] = get_lonely_states();

  return $plans;
}

/**
 * customer_plans
 *
 * Returns an array of Ultra Plans
 *
 * @return array
 */
function customer_plans()
{
  $plans = array();

  $plan_substates = array(STATE_ACTIVE, STATE_PROVISIONED, STATE_SUSPENDED, STATE_PORT_IN_REQUESTED, STATE_PORT_IN_DENIED, STATE_PRE_FUNDED, STATE_PROMO_UNUSED);

  foreach (get_ultra_plans_long_definitions() as $plan)
  {
    $plans[$plan] = \Ultra\UltraConfig\getUltraPlanConfiguration($plan);
    $plans[$plan]['plan_group'] = 'Ultra';
    $plans[$plan]['states'] = $plan_substates;

    $plans[$plan]['zero_used_minutes'] = ($zeroMinutes = \Ultra\UltraConfig\zeroUsedMinutes($plan)) ? $zeroMinutes : 0;
  }

  $plans['STANDBY']['states'] = get_lonely_states();

  foreach ($plans as $key => $val)
  {
    foreach ($plans[$key] as $subKey => $subVal)
    {
      if ($plans[$key][$subKey] === null)
        unset($plans[$key][$subKey]);
    }
  }

  return $plans;
}

/**
 * make_action
 *
 * Creates an action, given a type and up to 4 parameters, none of which is required
 *
 * @return array or string
 */
function make_action($type, $p1=NULL, $p2=NULL, $p3=NULL, $p4=NULL, $p5 = null)
{
  if ( ( ! isset($type) ) || ( $type == '' ) )
  { $type = 'MAKE_ACTION_TYPE_UNDEFINED'; }

  switch($type)
  {
  case "mvne_provision":
    return array('funcall' => "mvneProvisionSIM",
                 'fparams' => array("__customer:ICCID__",          # ICCID
                                    "__customer:ACTIVATION_ZIP__", # zip
                                    "__customer:LANGUAGE__",       # language
                                    $p1,                           # plan
                                    "__customer_id__",             # customer_id
                                    "__this_transition__",
                                    "__this_action__"));
  case "mvne_activate":
    return array('funcall' => "mvneActivateSIM",
                 'fparams' => array("__customer:ICCID__",          # ICCID
                                    "__customer:ACTIVATION_ZIP__", # zip
                                    "__customer:LANGUAGE__",       # language
                                    $p1,                           # plan
                                    "__customer_id__",             # customer_id
                                    "__this_transition__",
                                    "__this_action__"));
  case "mvneEnsureSuspend":
    return array('funcall' => "mvneEnsureSuspend",
                 'fparams' => array("__this_action__",
                                    "__customer_id__",
                                    "__customer:MSISDN__",
                                    "__customer:ICCID__"));
  case "mvneEnsureCancel":
    return array('funcall' => "mvneEnsureCancel",
                 'fparams' => array("__customer:MSISDN__",
                                    "__customer:ICCID__",
                                    "__this_transition__",
                                    "__this_action__",
                                    "__customer_id__"));

  case "mvneAddRecurringUpData":
    return array('funcall' => "mvneAddRecurringUpData",
                 'fparams' => array("__this_action__",
                                    "__customer_id__",
                                    $p1,
                                    $p2,
                                    $p3));
  case "mvneMakeitsoUpgradePlan":
    return array('funcall' => "mvneMakeitsoUpgradePlan",
                 'fparams' => array("__this_action__",
                                    "__customer_id__",
                                    $p1));

  case 'mvneIntraPort':
    return array( 'funcall' => 'mvneIntraPort',
                  'fparams' => array(
                    '__customer_id__',
                    '__customer:MSISDN__',
                    '__old_iccid__',      // old ICCID from cloned customer
                    '__port_in_iccid__',  // new porting ICCID to activate/provision
                    $p1,                  // short plan name
                    '__customer:LANGUAGE__',
                    $p2,                  // final state (Active or Provisioned)
                    '__port_in_account_number__',
                    '__old_brand_id__'));

  case "assert_balance":
    return array('funcall' => "func_assert_balance",
                 'fparams' => array("__customer_id__",
                                    $p1));

  case "record_customer_soc":
    return array('funcall' => "trackDataSOC",
                 'fparams' => array("__customer_id__",
                                    $p1));

  case "record_recurring_bolt_on":
    return array('funcall' => "trackBoltOn",
                 'fparams' => array("__customer_id__",
                                    $p1,        // source
                                    'DONE',     // status
                                    $p2,        // sku
                                    $p3,        // product
                                    'MONTHLY'   // type
  ));

  case "record_immediate_bolt_on":
    return array('funcall' => "trackBoltOn",
                 'fparams' => array("__customer_id__",
                                    $p1,        // source
                                    'DONE',     // status
                                    $p2,        // sku
                                    $p3,        // product
                                    'IMMEDIATE' // type
  ));

  case 'reset_zero_minutes':
    return array('funcall' => 'func_reset_zero_minutes',
                 'fparams' => array('__customer_id__', $p1));

  // record_ICCID_activation ; update ACTIVATION_ICCID ; reserve iccid
  case 'sim_activation_handler':
    return array('funcall' => 'sim_activation_handler',
                 'fparams' => array('__customer_id__', '__this_transition__', $p1));

  // insert into ACCOUNT_ALIASES ; insert into DESTINATION_ORIGIN_MAP
  case 'msisdn_activation_handler':
    return array('funcall' => 'msisdn_activation_handler',
                 'fparams' => array('__customer_id__'));

  case 'funcSendExemptCustomerSMSProvisionedReminder':
    return array('pfuncall' => 'funcSendExemptCustomerSMSProvisionedReminder',
                 'params' => array('customer_id'   => '__customer_id__', 'cos_id'=> $p1));

  case 'funcSendExemptCustomerSMSActivationNewPhone':
    return array('pfuncall' => 'funcSendExemptCustomerSMSActivationNewPhone',
                 'params'   => array('customer_id' => '__customer_id__'));

  case 'send_portin_success_email':
    return array('funcall'  => 'send_portin_success_email',
                 'fparams'  => array('__customer_id__'));

  case "spend_from_balance":
    return array('funcall' => "func_spend_from_balance_explicit",
                 'fparams' => array(
                   "__customer_id__",
                   $p1, # amount in $
                   $p2, # detail
                   $p3, # reason
                   "__this_transition__", # reference
                   "SPEND",               # source
                   $p4,                   # reference_source
                   null,                  # target_cos_id
                   $p5                    # type
  ));

  case "send_sms_bolt_on_data_immediate":
    return array('funcall' => "send_sms_bolt_on_data_immediate",
                 'fparams' => array(
                   "__customer_id__",
                   $p1, // boltOnInfo
  ));

  case "send_sms_bolt_on_data_recurring":
    return array('funcall' => "send_sms_bolt_on_data_recurring",
                 'fparams' => array(
                   "__customer_id__",
                   $p1, // boltOnInfo
  ));

  case "send_sms_data_recharge":
    return array('funcall' => "send_sms_data_recharge",
                 'fparams' => array(
                   "__customer_id__",
                   $p1 # data_MB
  ));

  case "empty_minutes":
    return array(
      "sql"     => "UPDATE ACCOUNTS SET packaged_balance1 = 0 WHERE CUSTOMER_ID = %d",
      'fparams' => array("__customer_id__")
    );
  case "expire_sim_card":
    return array(
      "sql"     => "UPDATE HTT_INVENTORY_SIM SET EXPIRES_DATE = getutcdate() WHERE ICCID_NUMBER = '%s'",
      'fparams' => array("__customer:ICCID__")
    );
  case "expire_msisdn":
    return array(
      "sql"     => "UPDATE HTT_ULTRA_MSISDN SET EXPIRES_DATE = getutcdate() WHERE MSISDN = '%s'",
      'fparams' => array("__customer:MSISDN__")
    );
  case "ICCID_status_used":
    return array( // I am not sure 'USED' is a legal state, I did not find the one we want in the docs
      "sql"     => "UPDATE HTT_INVENTORY_SIM SET INVENTORY_STATUS = 'USED' WHERE ICCID_NUMBER = '%s'",
      'fparams' => array("__customer:ICCID__")
    );
  case "disable_account":
    return array(
      "sql"     => 'UPDATE ACCOUNTS SET ' . account_enabled_assignment(FALSE) . ' WHERE customer_id = %d',
      'fparams' => array("__customer_id__")
    );
  case "deactivate_ultra_msisdn":
    return array(
      "sql"     => "UPDATE HTT_ULTRA_MSISDN SET MSISDN_ACTIVATED = 0 , LAST_TRANSITION_UUID = '%s' WHERE MSISDN = '%s'",
      'fparams' => array(
                     "__this_transition__",
                     "__customer:MSISDN__"
                   )
    );
  // to be invoked when we assign a PORT ICCID to a Pre-Funded customer
  case "log_port_iccid_in_activation_history":
    return array('funcall' => "log_port_iccid_in_activation_history",
                 'fparams' => array(
                   "__customer_id__",
                   "__customer:ICCID__"
                 ));
  // to be invoked when we assign a NEW ICCID to a Pre-Funded customer
  case "log_new_iccid_in_activation_history":
    return array('funcall' => "log_new_iccid_in_activation_history",
                 'fparams' => array(
                   "__customer_id__",
                   "__customer:ICCID__"
                 ));
  // to be invoked at the beginning of a non-port (NEW) activation
  case "log_new_activation_in_activation_history":
    return array('funcall' => "log_new_activation_in_activation_history",
                 'fparams' => array(
                   "__this_transition__",
                   "__customer_id__"
                 ));
  // to be invoked at the beginning of every port transition
  case "log_port_transition_in_activation_history":
    return array('funcall' => "log_port_transition_in_activation_history",
                 'fparams' => array(
                   "__this_transition__",
                   "__customer_id__"
                 ));

  // to be invoked at the beginning of every intraport transition
  case "log_intraport_transition_in_activation_history":
    return array('funcall' => "log_intraport_transition_in_activation_history",
                     'fparams' => array(
                       "__this_transition__",
                       "__customer_id__"
                     ));

  // to be invoked in every transition with the exception of cancellation, plan renewals, suspensions, reactivations, plan changes
  case "log_transition_in_activation_history":
    return array('funcall' => "log_transition_in_activation_history",
                 'fparams' => array(
                   "__this_transition__",
                   "__customer_id__"
                 ));
  // to be invoked in every Cancellation transition with the exception of 'Suspended'=>'Cancelled' and 'Active'=>'Cancelled'
  case "log_cancellation_transition_in_activation_history":
    return array('funcall' => "log_cancellation_transition_in_activation_history",
                 'fparams' => array(
                   "__this_transition__",
                   "__customer_id__"
                 ));
  // to be invoked right after HTT_CUSTOMERS_OVERLAY_ULTRA.PLAN_STARTED is initialized
  case "log_plan_started_in_activation_history":
    return array('funcall' => "log_plan_started_in_activation_history",
                 'fparams' => array(
                   "__customer_id__"
                 ));
  case "delete_account_aliases_by_msisdn":
    return array(
      "sql"     => "DELETE FROM ACCOUNT_ALIASES WHERE ALIAS = '%s'",
      'fparams' => array("__customer:MSISDN__")
    );
  case "delete_account_aliases_by_account_id":
    return array(
      "sql"     => "
IF EXISTS (SELECT a.ACCOUNT_ID FROM ACCOUNT_ALIASES a where a.ALIAS = '%s')
BEGIN
DELETE FROM ACCOUNT_ALIASES WHERE ALIAS = '%s'
END",
      'fparams' => array(
                     "__customer:MSISDN__",
                     "__customer:MSISDN__"
                   )
    );
  case "active->maybe_set_ACTIVATION_DATE_TIME":
    return array(
      "sql"     => "UPDATE ACCOUNTS SET ACTIVATION_DATE_TIME = GETUTCDATE() WHERE customer_id = %d AND ACTIVATION_DATE_TIME IS NULL",
      'fparams' => array("__customer_id__")
    );
  case "active->plan_started=X":
    return array(
      "sql"     => "UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET plan_started = %s WHERE customer_id = %d",
      'fparams' => array("$p1", "__customer_id__")
    );
  case 'save_gross_add_date':
    return array(
      "sql"     => "UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET GROSS_ADD_DATE = GETUTCDATE() WHERE customer_id = %d",
      'fparams' => array('__customer_id__')
    );

  case 'renewal->active.plan_expires': // Active/Suspended -> Active: entire 1st day counts
    return array(
      'sql' => 'UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET plan_expires = ' .
        'dbo.PT_TO_UTC(convert(date, dateadd(d, %d, dbo.UTC_TO_PT(%s)))) ' .
        'WHERE customer_id = %d',
      'fparams' => array("$p2", "$p1", "__customer_id__"));

  case 'initial->active.plan_expires': // initial -> 'Active': 1st day counts only when start time < 14:00
    return array(
      'sql' => 'UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET plan_expires = ' .
        'dbo.PT_TO_UTC(convert(date, dateadd(s, 36000, dateadd(d, %d, dbo.UTC_TO_PT(%s))))) ' .
        'WHERE customer_id = %d',
      'fparams' => array("$p2", "$p1", "__customer_id__"));

  case 'nullify_sim_and_msisdn':
    return array(
      "sql"     => "UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET CURRENT_MOBILE_NUMBER = NULL , CURRENT_ICCID_FULL = NULL , CURRENT_ICCID = NULL WHERE CUSTOMER_ID = %d",
      'fparams' => array("__customer_id__")
    );
  case 'sleep':
    return array("funcall" => "sleep",
                 'fparams' => array($p1));

  // PROD-789: isolate Process Recurring Bolt Ons action since it does not apply to all transitions to Active
  case 'process_recurring_bolt_ons':
    return array(
      'pfuncall' => 'process_recurring_bolt_ons',
      'params' => array('customer_id' => '__customer_id__', 'plan' => $p1));

  case 'disable_cc_holders_entries':
    return array(
      'pfuncall' => 'disable_cc_holders_entries',
      'params' => array('customer_id' => '__customer_id__'));

  // PROD-1647: finalize dealer demo line activation
  case 'finalize_demo_line_activation':
    return array(
      'funcall' => 'Ultra\Lib\DemoLine\finalizeActivation',
      'fparams' => array('__customer_id__', '__demo_line__', '__email__', '__activations__'));
  }

  return "make_action TODO $type";
}

/**
 * ultra_states
 *
 * Returns an array of Customer states
 *
 * @return array
 */
function ultra_states()
{
  $states = array();

  $plans = ultra_plans();

  foreach ($plans as $plan_name => $plan_info)
  {
    foreach ($plan_info['states'] as $plan_state)
    {
      $s = array('plan'  => $plan_name,
                 'info'  => $plans[$plan_name],
                 'name'  => canonify("state_${plan_name}_$plan_state"),
                 'print' => "$plan_info[name]/$plan_state",
                 'state' => $plan_state,
                 // DEPARTURE_ACTIONS => array("departure action $plan_name/$plan_state")
      );

      if ($plan_state === 'Active')
      {
        $s[DEPARTURE_ACTIONS] = departure_actions_active( $plan_info );

        $s[ARRIVAL_ACTIONS]   = arrival_actions_active( $plan_info , $plan_name );
      }
      else if ( ($plan_state === 'Port-In Requested') || ($plan_state === 'Provisioned') || ($plan_state === 'Promo Unused') )
      {
        $s[ARRIVAL_ACTIONS] = array();
      }
      else if ($plan_state === 'Cancelled')
      {
        $s[ARRIVAL_ACTIONS] = arrival_actions_cancelled();
      }

      $s['extract'] = extract_actions($s);

      $states[] = $s;
    }
  }
  return $states;
}

/**
 * state_filter
 *
 * Returns TRUE id $state matches $state_regex, $plan_regex, $cos_id
 *
 * @return boolean
 */
function state_filter($state_regex, $plan_regex, $cos_id, $state)
{
  $yes = (NULL == $state_regex ?
          TRUE :
          preg_match("/$state_regex/", $state['state']));

  if (NULL != $plan_regex)
  {
    $names = array($state['plan']);
    if (isset($state['info']['aka']))
    {
      foreach (explode(' ', "".$state['info']['aka']) as $aka)
      {
        $names[] = $aka;
      }
    }

    $plan_pass = FALSE;

    foreach ($names as $name)
    {
      $plan_pass = $plan_pass || $plan_regex == $name;
      if ($plan_pass) break;
    }

    $yes = $yes && $plan_pass;
  }

  if (NULL != $cos_id) $yes = $yes && ("".$state['info']['cos_id'] === "".$cos_id);

  return $yes;
}

/**
 * find_state
 *
 * Searches for a valid state, given $state_regex, $plan_regex, $cos_id
 *
 * @return array
 */
function find_state($states,
                    $state_regex, $plan_regex, $cos_id=NULL)
{
  /* dlog("", "find_state() called with: state_regex = $state_regex, plan_regex = $plan_regex, cos_id = $cos_id"); */

  foreach ($states == NULL ? ultra_states() : $states as $state)
  {
    /* dlog("", "Checking state " . json_encode($state)); */
    if (state_filter($state_regex, $plan_regex, $cos_id, $state))
    {
      return $state;
    }
  }
}

/**
 * ultra_transitions
 *
 * Ultra Transition Main configuration multi-layered array
 *
 * @return array
 */
function ultra_transitions()
{
  $transitions = array();
  $plans       = ultra_plans();
  $states      = ultra_states();

  $state_standby_neutral = find_state($states, 'Neutral',   'STANDBY');
  $cancelled             = find_state($states, 'Cancelled', 'STANDBY');

  foreach ($plans as $plan_name => $plan_info)
  {
    $group = $plan_info['plan_group'];

    if ( ( $group === 'Ultra' ) && ( $plan_name != "STANDBY" ) )
    {
      /* these are transitions WITHIN a plan! */
      $provisioned =  find_state($states, 'Provisioned',       $plan_name);
      $promounused =  find_state($states, 'Promo Unused',      $plan_name);
      $prefunded =    find_state($states, 'Pre-Funded',        $plan_name);
      $active =       find_state($states, 'Active',            $plan_name);
      $suspended =    find_state($states, 'Suspended',         $plan_name);
      $portinreq =    find_state($states, 'Port-In Requested', $plan_name);
      $portinden =    find_state($states, 'Port-In Denied',    $plan_name);

      // 'source'      source state
      // 'dest'        destination state
      // 'requires'    pre-conditions ( array of validations ? )
      // ACTIONS     array of actions. An action is a couple ( function call , function parameters )
      // 'label'       human readable caption

      $actions_list = actions_list( $active , $plan_info );

      // provisioning, ports and activation transitions for currently offered plans
      if ( ! is_grandfathered_plan($active['plan']))
      {
        if (is_orange_plan($active['plan']))
          $transitions[] = array(
            'source'   => $state_standby_neutral,
            'dest'     => $active,
            'label'    => "Orange Sim Activation $active[plan]",
            'desc'     => "Orange Sim Activation $active[plan]",
            'requires' => array('explicit' => TRUE, "whole_value_min" => $active['info']['cost']/100),
            ACTIONS    => array_merge(
              $actions_list['orange_activation_actions'],
              $actions_list['activation_actions'][ $active['plan'] ],
              array(
                array(
                'funcall' => "func_reset_zero_minutes",
                'fparams' => array("__customer_id__", $active['info']['cos_id'])
                )
              )
            ),
            'priority' => 1
          );

        // intra-brand port activation: see http://wiki.hometowntelecom.com:8090/display/SPEC/Intra+brand+port+in
        $transitions[] = array( 'source' => $state_standby_neutral,
                                'dest' => $active,
                                'requires' => array('whole_value_min' => $active['info']['cost']/100),
                                ACTIONS => array_merge(
                                  array(
                                    make_action('log_intraport_transition_in_activation_history'),
                                    make_action('log_transition_in_activation_history'),
                                    make_action('mvneIntraPort', $active['info']['aka'], $active['state']),
                                    make_action('reset_zero_minutes', $active['info']['cos_id']),
                                    make_action('save_gross_add_date')),
                                  $actions_list['post_activation_actions'],
                                  array(
                                    make_action('process_recurring_bolt_ons', $active['info']['aka']),
                                    make_action('send_portin_success_email')
                                  )
                                ),
                               'transaction' => "__customer_id__",
                               'label' => "Activate Intra Port $active[plan]",
                               'priority' => 1
        );

        $transitions[] = array('source' => $portinreq,
                               'dest' => $active,
                               'label' => 'Port Activated',
                               'desc' => 'Port was requested and was activated successfully.',
                               'requires' => array('explicit' => TRUE,
                                                   "whole_value_min" => $active['info']['cost']/100),                             
                               ACTIONS => array_merge(
                                 $actions_list['port_activated_actions'],
                                 $actions_list['port_add_to_htt_ultra_msisdn'],
                                 array(
                                   array('funcall' => "func_reset_zero_minutes",
                                         'fparams' => array(
                                           "__customer_id__",
                                           $active['info']['cos_id']
                                         )
                                   )
                                 )
                               ),
                               'priority' => 3
        );

        $transitions[] = array('source' => $state_standby_neutral,
                               'dest' => $portinreq,
                               'requires' => array('credential_off' => 'ultra/disable/porting'),
                               'label' => "Request Port $active[plan]",
                               'desc' => 'Customer as requested a port. Start a Fund request.',
                               ACTIONS => array(
                                 make_action('log_port_transition_in_activation_history'),
                                 array('funcall' => 'mvneRequestPortIn',
                                       'fparams' => array(
                                         '__customer_id__',
                                         '__port_in_msisdn__',
                                         '__port_in_iccid__',
                                         '__port_in_account_number__',
                                         '__port_in_account_password__',
                                         '__port_in_zipcode__',
                                         $active['info']['cos_id'],
                                         '__this_transition__',
                                         '__this_action__'
                                       )
                                 )
                               ),
                               'priority' => 1
        );

      if ( ! in_array($plan_name, ['TWENTY_NINE','FORTY_FOUR'])) {

        $transitions[] = array('source' => $state_standby_neutral,
                               'dest'   => $promounused,
                               'label'  => "Promo Prepare $active[plan]",
                               ACTIONS  => $actions_list['promo_prepare_actions'][ $active['plan'] ],
                               'priority' => 7
        );

        $transitions[] = array('source'   => $prefunded,
                               'dest'     => $portinreq,
                               'requires' => array('whole_value_min' => $active['info']['cost']/100, // No real way an account should end up here without being funded
                                                   'credential_off'  => 'ultra/disable/porting'),
                               'label'    => 'SIM Received Request Port',
                               'desc'     => 'Customer has ordered and received a SIM Card. Customer wishes to port in.',
                               ACTIONS    => array(
                                 make_action('log_port_transition_in_activation_history'),
                                 make_action('log_port_iccid_in_activation_history'),
                                 array('funcall' => 'mvneRequestPortIn',
                                       'fparams' => array(
                                         '__customer_id__',
                                         '__port_in_msisdn__',
                                         '__port_in_iccid__',
                                         '__port_in_account_number__',
                                         '__port_in_account_password__',
                                         '__port_in_zipcode__',
                                         $active['info']['cos_id'],
                                         '__this_transition__',
                                         '__this_action__'
                                       )
                                 )
                               ),
                               'priority' => 1
        );

        $transitions[] = array('source' => $state_standby_neutral,
                               'dest' => $provisioned,
                               'label' => "Provision $active[plan]",
                               ACTIONS => $actions_list['provisioning_actions'][ $active['plan'] ],
                               'priority' => 1
        );

        $transitions[] = array('source'   => $state_standby_neutral,
                               'dest'     => $prefunded,
                               'label'    => "Pre-Fund SIM $active[plan]",
                               'requires' => array('explicit' => TRUE,
                                                   "whole_value_min" => $active['info']['cost']/100),
                               ACTIONS => array(
                                 make_action('log_transition_in_activation_history')
                               ),
                               'priority' => 1
        );

        $transitions[] = array('source'   => $state_standby_neutral,
                               'dest'     => $prefunded,
                               'label'    => "Ship Ordered SIM $active[plan]",
                               'requires' => array('explicit'             => TRUE,
                                                   'has_shipping_address' => TRUE,
                                                   "whole_value_min"      =>  $active['info']['cost']/100  #shipping amount already taken out. TODO do in trans.
                               ),
                               ACTIONS  => $actions_list['ship_sim_actions'],
                               'priority' => 1
        );

        $transitions[] = array('source' => $prefunded,
                               'dest' => $active,
                               'label' => 'Activate Shipped SIM',
                               'desc' => 'Customer received SIM card and activates a new number.',
                               ACTIONS => array_merge(
                                 $actions_list['activation_actions'][ $active['plan'] ],
                                 array(
                                   array('funcall' => "func_reset_zero_minutes",
                                         'fparams' => array(
                                           "__customer_id__",
                                           $active['info']['cos_id']
                                         )
                                   ),
                                   make_action('process_recurring_bolt_ons', $active['info']['aka']),
                                   make_action('save_gross_add_date'),
                                   make_action('log_new_iccid_in_activation_history')
                                 )
                               ),
                               'priority' => 1
        );

        $transitions[] = array('source' => $state_standby_neutral,
                               'dest' => $active,
                               'requires' => array('whole_value_min' => $active['info']['cost']/100),
                               ACTIONS => array_merge(
                                 $actions_list['activation_actions'][ $active['plan'] ],
                                 array(
                                   array('funcall' => "func_reset_zero_minutes",
                                         'fparams' => array(
                                           "__customer_id__",
                                           $active['info']['cos_id']
                                         )
                                   ),
                                   make_action('process_recurring_bolt_ons', $active['info']['aka']),
                                   make_action('save_gross_add_date')
                                 )
                               ),
                               'transaction' => "__customer_id__", # ?
                               'label' => "Activate $active[plan]",
                               'priority' => 1
        );

        // Dealer Demo Line activation: same as regular activation plus an action to update the Demo Line DB records
        $transitions[] = array('source' => $state_standby_neutral,
                               'dest' => $active,
                               'requires' => array('whole_value_min' => $active['info']['cost']/100),
                               ACTIONS => array_merge(
                                 $actions_list['activation_actions'][ $active['plan'] ],
                                 array(
                                   array('funcall' => "func_reset_zero_minutes",
                                         'fparams' => array(
                                           "__customer_id__",
                                           $active['info']['cos_id'])
                                   ),
                                   make_action('save_gross_add_date'),
                                   make_action('finalize_demo_line_activation'))
                               ),
                               'transaction' => "__customer_id__",
                               'label' => "Activate Dealer Demo $active[plan]",
                               'priority' => 1
        );

        // intra-brand port provisioning: see http://wiki.hometowntelecom.com:8090/display/SPEC/Intra+brand+port+in
        $transitions[] = array( 'source' => $state_standby_neutral,
                                'dest' => $provisioned,
                                ACTIONS => array(
                                  make_action('log_intraport_transition_in_activation_history'),
                                  make_action('log_transition_in_activation_history'),
                                  make_action('mvneIntraPort', $active['info']['aka'], $provisioned['state']),
                                  make_action('sim_activation_handler', $plan_info['cos_id']),
                                  make_action('msisdn_activation_handler'),
                                  make_action('funcSendExemptCustomerSMSProvisionedReminder', $active['info']['cos_id']),
                                  make_action('funcSendExemptCustomerSMSActivationNewPhone')
                                ),
                               'transaction' => "__customer_id__",
                               'label' => "Provision Intra Port $active[plan]",
                               'priority' => 1
        );

        $transitions[] = array('source' => $portinreq,
                               'dest' => $provisioned,
                               'label' => 'Port Provisioned',
                               'desc' => 'Port was requested and was provisioned successfully.',
                               ACTIONS => array_merge(
                                 $actions_list['post_provisioning_actions'],
                                 $actions_list['port_add_to_htt_ultra_msisdn']
                               ),
                               'priority' => 3
        );

        $transitions[] = array('source' => $provisioned,
                               'dest' => $active,
                               'label' => "Activate Provisioned $active[plan]",
                               'requires' => array('whole_value_min' => $active['info']['cost']/100),
                               ACTIONS => $actions_list['provisioned_to_active_actions'],
                               'priority' => 1
        );

      }} // end of transitions for for currently offered plans only

      $transitions[] = array('source' => $portinreq,
                             'dest' => $portinden,
                             'label' => 'Can Not Port',
                             'desc' => '(WHAT)? sets this state, noting that the port attempt failed.',
                             ACTIONS => $actions_list['post_portindenied_actions'],
                             'priority' => 4
      );

      $transitions[] = array('source' => $portinden,
                             'dest' => $portinreq,
                             'label' => 'Retry Port',
                             'requires' => array('credential_off' => 'ultra/disable/porting'),
                             'desc' => 'Failed Port was resubmitted for approval.',
                             ACTIONS => array(
                               make_action('log_transition_in_activation_history')
                             ),
                             'priority' => 1
      );

      $transitions[] = array('source' => $portinden,
                             'dest'   => $cancelled,
                             'label'  => 'Cancel Unportable',
                             'desc'   => 'Port request to be cancelled. (Note, all ports cannot be cancelled.) Plan cannot have been activated already.',
                             ACTIONS  => array(
                               array('funcall' => 'mvneCancelPortIn',
                                 'fparams' => array(
                                   '__customer:MSISDN__',
                                   '__this_transition__',
                                   '__this_action__'
                                 )
                               ),
                               make_action('log_cancellation_transition_in_activation_history')
                             ),
                             'priority' => 6
      );

      $transitions[] = array('source' => $state_standby_neutral,
                             'dest'   => $cancelled,
                             'label'  => 'Cancel Neutral',
                             'desc'   => 'Customer in Neutral state for a long time.',
                             ACTIONS => array(
                               make_action('log_cancellation_transition_in_activation_history')
                             ),
                             'priority' => 6
      );

      $transitions[] = array('source' => $prefunded,
                             'dest' => $prefunded,
                             'label' => 'Re-ship Ordered SIM',
                             'requires' => array('has_shipping_address'      => TRUE,
                                                 'last_shipped_days_ago_min' => 7,
                                                 'sim_shipments_max'         => 2
                             ),
                             ACTIONS  => $actions_list['ship_sim_actions'],
                             'priority' => 1
      );

      $transitions[] = array('source' => $provisioned,
                             'dest' => $cancelled,
                             'desc' => 'Customer has gotten a phone number and SIM card, but has not paid for service, and cancels/expires out.',
                             ACTIONS => $actions_list['provisioned_to_cancelled_actions'],
                             'label' => 'Cancel Inactive',
                             'priority' => 6
      );

      $transitions[] = array('source'   => $promounused,
                             'dest'     => $active,
                             'label'    => "Activate Promo $active[plan]",
                             'requires' => array('whole_value_min' => $active['info']['cost']/100),
                             ACTIONS    => $actions_list['promo_activation_actions'],
                             'priority' => 1
      );

      $transitions[] = array('source' => $promounused,
                             'dest'   => $cancelled,
                             'desc'   => 'Cancel Promo',
                             ACTIONS  => $actions_list['provisioned_to_cancelled_actions'],
                             'label'  => 'Cancel Promo',
                             'priority' => 6
      );

      $transitions[] = array('source'      => $active,
                             'dest'        => $suspended,
                             ACTIONS       => active_to_suspended_actions( $active ),
                             'transaction' => "__customer_id__", # ?
                             'label'       => 'Suspend',
                             'priority'    => 5
      );

      $transitions[] = array('source' => $active,
                             'dest' => $active,
                             'requires' => array('whole_value_min' => ($active['info']['cost']/100),
                                                 'plan_renewal_due' => '1'),
                             ACTIONS => $actions_list['monthly_renewal_actions'],
                             'transaction' => "__customer_id__",
                             'label' => 'Monthly Renewal', // same plan
                             'priority' => 4
      );

      $transitions[] = array('source' => $active,
                             'dest' => $cancelled,
                             ACTIONS => $actions_list['active_to_cancelled_actions'],
                             'transaction' => "__customer_id__",
                             'label' => 'Cancel Active',
                             'priority' => 6
      );

      $transitions[] = array('source' => $active,
                             'dest'   => $cancelled,
                             ACTIONS  => $actions_list['active_to_cancelled_actions'],
                             'transaction' => "__customer_id__",
                             'label'  => 'Cancel Active due to fraud',
                             'priority' => 6
      );

      $transitions[] = array('source' => $active,
                             'dest' => $cancelled,
                             ACTIONS => $actions_list['intra_brand_cancellation_actions'],
                             'transaction' => "__customer_id__",
                             'label' => "Intra brand cancellation $active[plan] Active",
                             'priority' => 6
      );

      $transitions[] = array('source' => $suspended,
                             'dest' => $cancelled,
                             ACTIONS => $actions_list['intra_brand_cancellation_actions'],
                             'transaction' => "__customer_id__",
                             'label' => "Intra brand cancellation $active[plan] Suspended",
                             'priority' => 6
      );

      $transitions[] = array('source' => $suspended,
                             'dest' => $active,
                             'requires' => array('whole_value_min' => $active['info']['cost']/100),
                             ACTIONS => $actions_list['suspended_to_active_actions'],
                             'transaction' => "__customer_id__",
                             'label'       => 'Reactivate from Suspend',
                             'priority' => 2
      );

      $transitions[] = array('source' => $suspended,
                             'dest' => $cancelled,
                             'label' => 'Cancel Suspended',
                             ACTIONS => $actions_list['suspended_to_cancelled_actions'],
                             'priority' => 6
      );

      $transitions[] = array('source' => $suspended,
                             'dest'   => $cancelled,
                             'label'  => 'Cancel Suspended due to fraud',
                             ACTIONS  => $actions_list['suspended_to_cancelled_actions'],
                             'priority' => 6
      );

      // allow Suspended -> Suspended for every plan combination except for changing brands or changing to restricted or grandfathered plans
      foreach ($plans as $to_plan_name => $to_plan_info)
      {
        if ($to_plan_info['plan_group'] === 'Ultra' && 
          $to_plan_name != $plan_name && 
          (empty($plan_info['brand_id']) || empty($to_plan_info['brand_id']) || $plan_info['brand_id'] == $to_plan_info['brand_id']) && // no brand or brand is same
          ! is_restricted_plan($to_plan_name) &&
          ! is_grandfathered_plan($to_plan_name))
        {
          $to_suspended = find_state($states, 'Suspended', $to_plan_name);
          $aka_from = $suspended['info']['aka'];
          $aka_to   = $to_suspended['info']['aka'];

          if ( ($to_suspended != NULL) && ($to_suspended != $suspended) )
          {
            $transitions[] = array(
              'priority'    => 5,
              'source'      => $suspended,
              'dest'        => $to_suspended,
              'requires'    => NULL,
              'transaction' => "__customer_id__",
              'label'       => "Suspended Change Plan $aka_from to $aka_to",
              ACTIONS       => ( $aka_from > $aka_to )
                               ?
                               actions_suspended_downgrade( $suspended , $to_suspended )
                               :
                               actions_suspended_upgrade( $suspended , $to_suspended )
            );
          }
        }
      }

      // allow Active -> Active for every plan except for changing brands or changing to restricted or grandfathered plans
      foreach ($plans as $to_plan_name => $to_plan_info)
      {
        if ($to_plan_info['plan_group'] === 'Ultra' &&
          (empty($plan_info['brand_id']) || empty($to_plan_info['brand_id']) || $plan_info['brand_id'] == $to_plan_info['brand_id']) && // no brand or brand is same
          $to_plan_name != $plan_name &&
          ! is_restricted_plan($to_plan_name) &&
          ! is_grandfathered_plan($to_plan_name))
        {
          $to_active = find_state($states, 'Active', $to_plan_name);

          $aka_from = $active['info']['aka'];
          $aka_to   = $to_active['info']['aka'];

          if ($to_active != NULL)
          {
            $active_to_active_actions = actions_change_plan_on_renewal($active,$to_active,$to_plan_info);

            $transitions[] = array(
              'priority'    => 4,
              'source'      => $active,
              'dest'        => $to_active,
              'requires'    => array('whole_value_min'  => ($to_active['info']['cost']/100),
                                     'plan_renewal_due' => '1'),
              ACTIONS       => $active_to_active_actions,
              'transaction' => "__customer_id__",
              'label'       => "Monthly Renewal and Change Plan $aka_from to $aka_to"
            );

            // Only do this if it's an upgrade; 'Change Plan Mid-Cycle' only applies on upgrades.
            $delta_cost = ($to_active['info']['cost'] - $active['info']['cost']) / 100; // cost difference between current plan and future plan
            if ($delta_cost > 0 || ($delta_cost == 0 && $to_active['info']['aka'] != $active['info']['aka']) && ! is_grandfathered_plan($to_active['info']['aka'])) // see API-366
            {
              $actions_change_plan_midcycle = actions_change_plan_midcycle($delta_cost,$active,$to_active,$to_plan_info);

              // Change Plan Mid-Cycle L49 to L59
              if ( ( $aka_from == 'L49' ) && ( $aka_to == 'L59' ) )
                $actions_change_plan_midcycle[] = action_midcycle_packaged_balance_check();

              $transitions[] = array(
                'priority'    => 2,
                'source' => $active,
                'dest' => $to_active,
                'label' => "Change Plan Mid-Cycle $aka_from to $aka_to",
                ACTIONS => $actions_change_plan_midcycle,
                'requires' => array('whole_value_min' => $delta_cost),
                'transaction' => "__customer_id__"
              );
            }
          }
        }
      }
    }
  }

  $rewrite = array();
  foreach ($transitions as $t)
  {
    $source = $t['source'];
    $dest = $t['dest'];

    $t['print'] = "''".$t['label']."'' from [ ".$source['print']." ] to [ ".$dest['print']." ]";
    if (!isset($t[ACTIONS]))
    {
      /* $t[ACTIONS] = array("edge action ($t[print])"); */
    }

    $t['extract'] = array_merge(extract_requires($t), extract_actions($t));
    $rewrite[] = $t;
  }

  return $rewrite;
}

/**
 * change_state
 *
 * $customer_full = the usual customer status object
 * $resolve_now is boolean
 * $plan = NINETEEN or {TWENTY,THIRTY,FORTY,FIFTY}_NINE or STANDBY
 * $state = Active, Cancelled, etc.
 * It returns { success: true/false, aborted: true/false, transitions: [...], errors: [...], log: [...], old_state: OS, old_plan: OP, state: S, plan: P }
 *
 * @return array
 */
function change_state($context, $resolve_now, $plan, $state_name, $dry_run=FALSE, $max_path_depth=99)
{
  \dlog('transitions', "(%s)", func_get_args());

  $ret = array(
    'success'     => FALSE,
    'aborted'     => FALSE,
    'errors'      => array(),
    'log'         => array(),
    'transitions' => array()
  );

  // select transition and check pre-reqs will throw errors
  if ($dry_run)
  {
    $ret['success'] = TRUE;
    return $ret;
  }

  // check for open transitions
  $active = get_by_column('htt_transition_log',
                           array('customer_id' => $context['customer_id']),
                           " AND status <> 'ABORTED' AND status <> 'CLOSED'
                             AND to_cos_id IS NOT NULL
                             AND to_plan_state IS NOT NULL");

  if ($active != NULL)
  {
    $ret['errors'][] = "ERR_API_INTERNAL: Customer already has active state change " . $active->TRANSITION_UUID;
    return $ret;
  }

  // instantiate state machine
  $redis = new \Ultra\Lib\Util\Redis();
  $dataEngineMSSQLObject = new \Ultra\Lib\StateMachine\DataEngineMSSQL;

  $sm = new \Ultra\Lib\StateMachine($dataEngineMSSQLObject, NULL, NULL, $redis);

  // set customer data as array
  // $customer = (array)get_customer_from_customer_id($context['customer_id']);

  $account  = (array)get_account_from_customer_id($context['customer_id'], ['cos_id', 'balance']);
  $customer = (array)customers_get_customer_by_customer_id(
    $context['customer_id'], [
      'postal_code',
      'first_name',
      'last_name',
  ]);
  $overlay  = (array)get_ultra_customer_from_customer_id($context['customer_id'], [
    'customer_id',
    'current_iccid',
    'current_iccid_full',
    'current_mobile_number',
    'plan_started',
    'plan_expires',
    'plan_state',
    'preferred_language',
    'stored_value',
    'BRAND_ID',
    'GROSS_ADD_DATE',
  ]);

  $customer = array_merge($account, $customer, $overlay);

  $sm->setCustomerData($customer);

  // if $state_name is 'take transition'
  // $plan will == a transition label
  if ($state_name == 'take transition')
  {
    // set transition configuration
    $sm->selectTransitionByLabel(
      $plan,
      $customer['plan_state'],
      get_plan_name_from_short_name(get_plan_from_cos_id($customer['cos_id']))
    );
  }
  // else $plan will be a short name, ex L19
  // and $tate_name will be a final state
  // see: lib/state_machine/activate_provisioned_account()
  else
  {
    // set transition configuration
    $sm->selectTransitionByStatesAndPlan(
      $customer['plan_state'],
      $state_name,
      get_plan_name_from_short_name(get_plan_from_cos_id($customer['cos_id'])),
      get_plan_name_from_short_name($plan)
    );
  }

  // store transition data in DB 
  $genTransitionResult   = $sm->generateTransitionData($context);
  $currentTransitionData = $sm->getCurrentTransitionData();
  $ret['transitions'][]  = $currentTransitionData['transition_uuid'];
  $ret['success']        = $genTransitionResult;
  if ( ! $genTransitionResult)
    $ret['errors'][] = 'Failed generating transition';

  // if immediate transition and generation was successful
  if ($resolve_now && $genTransitionResult)
  {
    $runTransitionResult = $sm->runTransition($reserve = TRUE);
    $ret['success'] = $runTransitionResult;
    if ( ! $runTransitionResult)
      $ret['errors'][] = 'Failed running transition';
  }

  return $ret;
}

/**
 * append_make_funcall_action
 *
 * Invokes function append_action for a 'funcall' type
 *
 * @return array
 */
function append_make_funcall_action( $action_type , $context , $action_transaction , $action_seq , $p1=NULL , $p2=NULL , $p3=NULL , $p4=NULL , $trans_label=NULL, $p5 = null)
{
  $funcall_action = make_action($action_type,$p1,$p2,$p3,$p4,$p5);

  return append_action(
    $context,
    array('type'        => 'funcall',
          'name'        => $funcall_action['funcall'],
          'transaction' => $action_transaction,
          'seq'         => $action_seq
    ),
    $funcall_action['fparams'],
    FALSE,
    NULL,
    NULL,
    $trans_label
  );
}

/**
 * append_action
 *
 * creates and append an action to a transition
 * if no existing transition is given ($params['transition_id') then a new one will be created
 * WARNING: transition will be reserved but it is the caller's responsibility to un-reserve it
 * @param array context (integer customer_id required, string transition_id optional)
 * @param array context ('type' => $action_type, 'name' => $action_name, 'transaction' => $action_transaction, 'seq' => $action_seq)
 * @return array (boolean success, boolean aborted, array errors, array log, array transitions, array actions)
 */
function append_action($context, $aparams, $params, $pending, $action_id = NULL, $parent_action_id = NULL, $trans_label = NULL)
{
  $ret = array(
    'success' => FALSE,
    'aborted' => FALSE,
    'errors' => array(),
    'log' => array(),
    'transitions' => array(),
    ACTIONS => array(),
  );

  $transition_given = (array_key_exists('transition_id', $context) &&
                       $context['transition_id'] != NULL);
  $transition_exists = FALSE;

  if ($transition_given)
  {
    $transition_exists = get_by_column('htt_transition_log',
                                       array('transition_uuid' => $context['transition_id']));
  }

  $transition_uuid = $transition_given ? $context['transition_id'] : getNewTransitionUUID('transition ' . time());

  $context['transition_id'] = $transition_uuid;

  $action_uuid = NULL == $action_id ? getNewActionUUID('action ' . time()) : $action_id;

  if (!$transition_exists)
  {
    if ( ! $trans_label )
    {
      $trans_label = ( is_array($aparams) && isset($aparams['name']) )
                     ?
                     $aparams['name']
                     :
                     'append_action'
                     ;
    }

    // add transition to HTT_TRANSITION_LOG and reserve it
    $check = log_transition($transition_uuid, $context, NULL, NULL, NULL, NULL, $trans_label);

    if (!$check)
    {
      $ret['errors'][] = "Could not log transition!";

      // unreserve transition
      unreserve_transition_uuid_by_pid( $transition_uuid );

      return $ret;
    }
  }

  $ret['transitions'][] = $transition_uuid;
  $ret[ACTIONS][] = $action_uuid;

  $check2 = log_action($transition_uuid,
                       $action_uuid,
                       $aparams,
                       $params,
                       $pending ? 'PENDING' : 'OPEN',
                       $parent_action_id,
                       NULL); # Redis

  if (!$check2)
  {
    $ret['errors'][] = "Could not log action!";
    return $ret;
  }

  $ret['success'] = TRUE;
  return $ret;
}

/**
 * log_actions
 *
 * Add one or more actions to HTT_ACTION_LOG and Redis
 *
 * @return array
 */
function log_actions($transition_uuid, $context, $thing, $first, $last, $seq, $redis=NULL)
{
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $redis_action = new \Ultra\Lib\Util\Redis\Action( $redis );

  $check = TRUE;
  $log = array();
  if (is_array($thing))
  {
    $log['print'] = $thing['print'];

    $types = array();

    if (json_encode($thing) != json_encode($first))
    {
      dlog('transitions', "Adding arrival for $thing[print]");
      $types[] = ARRIVAL_ACTIONS;
    }

    $types[] = ACTIONS;

    if (json_encode($thing) != json_encode($last))
    {
      dlog('transitions', "Adding departure for $thing[print]");
      $types[] = DEPARTURE_ACTIONS;
    }

    foreach ($types as $type)
    {
      dlog('even_more_transitions',
           "looking for $type out of %s in %s",
           $types,
           $thing);

      if (isset($thing[$type]) && is_array($thing[$type]))
      {
        foreach ($thing[$type] as $action)
        {
          $action_uuid = getNewActionUUID('action ' . time());

          if (is_array($action))
          {
            $aparams = array( 'seq' => $seq++ );

            $type_found = FALSE;
            foreach (array('funcall' => 'fparams',
                           'pfuncall' => 'params',
                           'command' => 'params',
                           'async_command' => 'params', # TODO: async_command MUST DIE
                           'sql' => 'fparams') as $type => $ptype)
            {
              if (isset($action[$type]))
              {
                $aparams['type'] = $type;
                $aparams['name'] = $action[$type];

                $check2 = log_action($transition_uuid,
                                     $action_uuid,
                                     $aparams,
                                     $action[$ptype],
                                     'OPEN',
                                     NULL,
                                     $redis,
                                     $redis_action
                );

                $check = $check2 && $check;
                $type_found = TRUE;
                break;
              }
            }

            if (!$type_found)
            {
              dlog('', "FATAL ERROR - Unknown action array in run_actions: %s", $action);
              $check = FALSE;
            }
          }
          else
          {
            $check2 = log_action($transition_uuid,
                                 $action_uuid,
                                 array('type' => 'TODO',
                                       'name' => "$action",
                                       'seq' => $seq++),
                                 NULL,
                                 'OPEN',
                                 NULL,
                                 $redis,
                                 $redis_action
            );
            $check = $check2 && $check;
          }
        }
      }
    }
  }

  if (!$check)
  {
    return FALSE;
  }

  return $log;
}

/**
 * is_transition_uuid_reserved_by_pid
 *
 * Returns TRUE if the Transition UUID or customer ID is reserved by a PID
 *
 * @return boolean
 */
function is_transition_uuid_reserved_by_pid($transition_uuid, $redis = NULL, $customer_id = NULL)
{
  $t_uuid = preg_replace( '/[\{\}]/' , '' , $transition_uuid );

  // we need a \Ultra\Lib\Util\Redis object
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  // PROD-1660: also check customer ID if given
  $reserved = $customer_id ? $redis->get(CUSTOMER_SEMAPHORE_PREFIX . $customer_id) : FALSE;

  $redis_key = TRANSITION_SEMAPHORE_PREFIX . $t_uuid;

  return $redis->get($redis_key) || $reserved;
}

/**
 * reserve_transition_uuid_by_pid
 *
 * Attempts to reserve a Transition UUID and optionally customer ID
 *
 * @return boolean - TRUE if success , FALSE if failure
 */
function reserve_transition_uuid_by_pid($transition_uuid, $redis = NULL, $customer_id = NULL)
{
  $reserved = FALSE;
  $pid = getmypid();

  dlog('',"Process ID  $pid attempts to reserve transition id $transition_uuid for customer ID " . ($customer_id ? $customer_id : 'NULL'));

  // we need a \Ultra\Lib\Util\Redis object
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $t_uuid = preg_replace( '/[\{\}]/' , '' , $transition_uuid );

  $transition_key = TRANSITION_SEMAPHORE_PREFIX . $t_uuid;
  $customer_key = $customer_id ? CUSTOMER_SEMAPHORE_PREFIX . $customer_id : NULL;

  // get current reservations
  $transition_value = $redis->get($transition_key);
  $customer_value = $customer_id ? $redis->get($customer_key) : FALSE;
  dlog('', 'reservation status: transition %s, customer: %s', $transition_value ? "PID $transition_value" : 'none', $customer_value ? "PID $customer_value" : 'none');

  // reserve both transition and customer ID if no reservations present or already reserved for the same process ID
  if (( ! $transition_value && ! $customer_value) || ($transition_value == $pid && $customer_value == $pid))
  {
    $redis->setnx($transition_key, $pid, 20 * SECONDS_IN_MINUTE); # ttl is 20 minutes
    $reserved = $pid == $redis->get($transition_key);

    if ($reserved && $customer_id)
    {
      $redis->setnx($customer_key, $pid, 10 * SECONDS_IN_MINUTE);
      $reserved_pid = $redis->get($customer_key);
      $reserved = ( $reserved_pid == $pid );
    }
  }

  return $reserved;
}

/**
 * reserve_ensure_msisdn_by_pid
 *
 * Attempts to reserve a MSISDN for the ensure task
 *
 * @return boolean - TRUE if success , FALSE if failure
 */
function reserve_ensure_msisdn_by_pid( $redis , $msisdn )
{
  dlog('',"Process Id ".getmypid()." attempts to reserve msisdn $msisdn");

  $reserved = FALSE;

  if ( strlen($msisdn) == 11 )
    $msisdn = substr($msisdn, 1);

  $redis_key = 'ensure/msisdn/'.$msisdn;

  $value = $redis->get($redis_key);

  if ( ( ! $value ) || ( $value == getmypid() ) )
  {
    $redis->set( $redis_key , getmypid() , 20*60 ); # ttl is 20 minutes

    $value = $redis->get($redis_key);

    $reserved = ! ! ( $value == getmypid() );
  }

  return $reserved;
}

/**
 * unreserve_ensure_msisdn_by_pid
 *
 * Attempts to unreserve a MSISDN for the ensure task
 *
 * @return boolean - TRUE if success , FALSE if failure
 */
function unreserve_ensure_msisdn_by_pid( $redis , $msisdn )
{
  dlog('',"Process Id ".getmypid()." attempts to unreserve msisdn $msisdn");

  if ( strlen($msisdn) == 11 )
    $msisdn = substr($msisdn, 1);

  $redis_key = 'ensure/msisdn/'.$msisdn;

  return $redis->del( $redis_key );
}

/**
 * unreserve_transition_uuid_by_pid
 *
 * Attempts to unreserve a Transition UUID and optionally customer ID
 *
 * @return boolean - TRUE if success , FALSE if failure
 */
function unreserve_transition_uuid_by_pid($transition_uuid, $redis = NULL, $customer_id = NULL)
{
  dlog('',"Process Id ".getmypid()." attempts to unreserve transition id $transition_uuid for customer " .($customer_id ? $customer_id : 'NULL'));

  // we need a \Ultra\Lib\Util\Redis object
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  // PROD-1660: also un-reserve customer ID if given
  if ($customer_id)
    $redis->del(CUSTOMER_SEMAPHORE_PREFIX . $customer_id);

  $t_uuid = preg_replace( '/[\{\}]/' , '' , $transition_uuid );

  $redis_key = TRANSITION_SEMAPHORE_PREFIX . $t_uuid;

  return $redis->del( $redis_key );

}

/**
 * remove_db_transition_from_redis
 *
 * Given a $transition_uuid, we remove the related transition and actions data from Redis
 *
 * @return string
 */
function remove_db_transition_from_redis($transition_uuid, $redis=NULL)
{
  dlog('', '(%s)', func_get_args());

  $error = '';

  // we need a \Ultra\Lib\Util\Redis object
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $redis_transition = new \Ultra\Lib\Util\Redis\Transition( $redis );

  $redis_action = new \Ultra\Lib\Util\Redis\Action( $redis );

  try
  {
    $success = reserve_transition_uuid_by_pid( $transition_uuid );

    if ( ! $success )
      throw new Exception("Cannot reserve transition_uuid $transition_uuid : another process is handling it");

    $t_uuid = preg_replace( '/[\{\}]/' , '' , $transition_uuid );

    $transition_data = $redis->hmget( 'ultra/transition/'.$t_uuid , array('htt_environment','priority') );

    dlog('',"environment = %s , priority = %s",$transition_data['htt_environment'],$transition_data['priority']);

    $listByEnvironmentAndPriority =
      $redis->zrevrangebyscore( 'ultra/sortedset/transitions/'.$transition_data['htt_environment'].'/'.$transition_data['priority'] , '+inf' , '-inf' , TRUE );

    dlog('',"listByEnvironmentAndPriority = %s",$listByEnvironmentAndPriority);

    // remove transition
    if ( in_array( $transition_uuid , $listByEnvironmentAndPriority ) )
    {
      dlog('',"transition_uuid $transition_uuid found in sorted set");
      $redis->zrem( 'ultra/sortedset/transitions/'.$transition_data['htt_environment'].'/'.$transition_data['priority'] , $transition_uuid );
      $redis_transition->deleteObject( $t_uuid );
      dlog('',"transition_uuid $transition_uuid removed from sorted set");
    }
    else
    {
      dlog('',"transition_uuid $transition_uuid not found in sorted set");
    }

    // remove actions
    $actionsByTransition = $redis->zrevrangebyscore( 'ultra/sortedset/transitions/actions/'.$t_uuid , '+inf' , '-inf' , TRUE );

    dlog('',"actionsByTransition = %s",$actionsByTransition);

    foreach ( $actionsByTransition as $action_uuid )
    {
      $a_uuid = preg_replace( '/[\{\}]/' , '' , $action_uuid );

      // remove action from Redis sorted set
      $redis->zrem( 'ultra/sortedset/transitions/actions/'.$t_uuid , $a_uuid );

      // destroy action in Redis
      $redis_action->deleteObject( $a_uuid );
    }

    $success = unreserve_transition_uuid_by_pid( $transition_uuid );

    if ( ! $success )
      throw new Exception("Could not unreserve transition_uuid $transition_uuid");
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    dlog('', "error: $error");
  }

  return $error;
}

/**
 * transition_garbage_collector
 *
 * Get all environments in htt_transition_log and invokes redis_db_transition_garbage_collector
 */
function transition_garbage_collector()
{
  $sql = 'SELECT DISTINCT HTT_ENVIRONMENT FROM HTT_TRANSITION_LOG';

  $data = mssql_fetch_all_rows(logged_mssql_query($sql));

  dlog('',"data = %s",$data);

  if ( $data && is_array($data) && count($data) )
  {
    $environments = array();

    foreach( $data as $row )
      $environments[] = $row[0];

    $errors = redis_db_transition_garbage_collector( $environments );

    dlog('',"errors = %s",$errors);
  }
}

/**
 * redis_db_transition_garbage_collector
 *
 * Loop through Redis Data Structure and remove transitions if already closed
 *
 * @param array $environments
 *
 * @return string
 */
function redis_db_transition_garbage_collector( $environments )
{
  dlog('', '(%s)', func_get_args());

  if ( ! is_array($environments) )
    $environments = array($environments);

  $error = '';

  $redis            = new \Ultra\Lib\Util\Redis;
  $redis_transition = new \Ultra\Lib\Util\Redis\Transition( $redis );
  $redis_action     = new \Ultra\Lib\Util\Redis\Action( $redis );

  try
  {
    $priority = '0'; # [0..9]

    // loop through priorities
    while( $priority < 10 )
    {
      // loop through environments
      foreach( $environments as $environment )
      {
        // get transition sorted set (by time) identified by $environment and $priority
        $transition_list = $redis_transition->getListByEnvironmentAndPriority( $environment , $priority );

        // loop through transitions
        foreach( $transition_list as $transition_uuid )
        {
          dlog('',"priority = $priority ; environment = $environment ; transition uuid = $transition_uuid");

          // load $transition from DB table HTT_TRANSITION_LOG
          $transition = get_transition_by_uuid( $transition_uuid );

          if ( ! $transition )
          {
            dlog('',"transition $transition_uuid not found in DB");

            if ( reserve_transition_uuid_by_pid($transition_uuid) )
            {
              remove_transition_from_redis( $transition_uuid , $environment , $priority , $redis_action , $redis_transition , $redis );
              unreserve_transition_uuid_by_pid($transition_uuid);
            }
            else
            {
              dlog('',"we could not reserve transition $transition_uuid");
            }
          }
          elseif( $transition->STATUS != 'OPEN' )
          {
            dlog('',"transition $transition_uuid not OPEN");

            if ( reserve_transition_uuid_by_pid($transition_uuid) )
            {
              remove_transition_from_redis( $transition_uuid , $environment , $priority , $redis_action , $redis_transition , $redis );
              unreserve_transition_uuid_by_pid($transition_uuid);
            }
            else
            {
              dlog('',"we could not reserve transition $transition_uuid");
            }
          }

        } // loop through transitions

      } // loop through environments

      $priority++;

    } // loop through priorities
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    dlog('', "error: $error");
  }

  return $error;
}

/**
 * remove_transition_from_redis
 */
function remove_transition_from_redis( $transition_uuid , $environment , $priority , $redis_action , $redis_transition , $redis )
{
  // destroy all transition actions in Redis
  $redis_action->deleteObjectsByTransition( $transition_uuid );

  $redis_transition->deleteObject( $transition_uuid );

  // remove transition from sorted set
  $redis->zrem( 'ultra/sortedset/transitions/'. $environment . '/' . $priority , $transition_uuid );
}

/**
 * recover_zombie_action_and_transition
 *
 * @return string
 */
function recover_zombie_action_and_transition($action_uuid, $redis=NULL)
{
  dlog('', '(%s)', func_get_args());

  // we need a \Ultra\Lib\Util\Redis object
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $sql = htt_action_log_select_query(
    array(
      'action_uuid' => $action_uuid
    )
  );

  $action_data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if (!( $action_data && is_array($action_data) && count($action_data) ))
    return "Action $action_uuid not found in DB";

  if ( ! preg_match('/^RUNNING /', $action_data[0]->STATUS ) )
    return "Action $action_uuid not running : ".$action_data[0]->STATUS;

  if ( ! reopen_action( $action_uuid ) )
    return "Cannot re-open action";

  $error = remove_db_transition_from_redis( $action_data[0]->TRANSITION_UUID );

  if ( $error != '' )
    return $error;

  return save_db_transition_to_redis( $action_data[0]->TRANSITION_UUID );
}

/**
 * save_db_transition_to_redis
 *
 * Given a $transition_uuid, we reserve the transition and we save all its data in Redis
 * This is particularly useful when transitioning between old state machine code (DB based) and new one (Redis based)
 *
 * @return string
 */
function save_db_transition_to_redis($transition_uuid, $redis=NULL)
{
  dlog('', '(%s)', func_get_args());

  $error = '';

  // we need a \Ultra\Lib\Util\Redis object
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $redis_transition = new \Ultra\Lib\Util\Redis\Transition( $redis );

  $redis_action = new \Ultra\Lib\Util\Redis\Action( $redis );

  try
  {
    $success = reserve_transition_uuid_by_pid( $transition_uuid );

    if ( ! $success )
      throw new Exception("Cannot reserve transition_uuid $transition_uuid : another process is handling it");

    // get data from HTT_TRANSITION_LOG
    $sql = htt_transition_log_select_query(
      array(
        "transition_uuid" => $transition_uuid
      )
    );

    $transition_data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( ! ( $transition_data && is_array( $transition_data ) && count( $transition_data ) ) )
    {
      // unreserve this TRANSITION_UUID
      $success = unreserve_transition_uuid_by_pid( $transition_uuid );
      throw new Exception("No HTT_TRANSITION_LOG data found");
    }

    // get data from HTT_ACTION_LOG
    $sql = htt_action_log_select_query(
      array(
        "transition_uuid" => $transition_uuid
      )
    );

    $actions_data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( ! ( $actions_data && is_array( $actions_data ) && count( $actions_data ) ) )
    {
      // unreserve this TRANSITION_UUID
      $success = unreserve_transition_uuid_by_pid( $transition_uuid );
      throw new Exception("No HTT_ACTION_LOG data found");
    }

    dlog('',"transition_data = %s",$transition_data);
    dlog('',"actions_data    = %s",$actions_data);

    // add new transition object in Redis
    if ( $transition_data[0]->STATUS == 'OPEN' )
    {
      $redis_transition->addObject(
        $transition_uuid,
        $transition_data[0]->PRIORITY,
        $transition_data[0]->CUSTOMER_ID,
        $transition_data[0]->FROM_COS_ID,
        $transition_data[0]->FROM_PLAN_STATE,
        $transition_data[0]->TO_COS_ID,
        $transition_data[0]->TO_PLAN_STATE,
        $transition_data[0]->TRANSITION_LABEL,
        $transition_data[0]->CREATED
      );

      // add new action objects in Redis
      foreach( $actions_data as $action_data )
      {
        if ( $action_data->STATUS == 'OPEN' )
        {
          $redis_action->addObject(
            $action_data->ACTION_UUID,
            $transition_uuid,
            $action_data->ACTION_TYPE,
            $action_data->ACTION_NAME,
            $action_data->STATUS,
            $action_data->ACTION_SEQ
          );
        }
      }
    }

    $success = unreserve_transition_uuid_by_pid( $transition_uuid );

    if ( ! $success )
      throw new Exception("Could not unreserve transition_uuid $transition_uuid");
  }
  catch (Exception $e)
  {
    $error = $e->getMessage();
    dlog('', "error: $error");
  }

  return $error;
}

/**
 * reset_redis_transition
 *
 * Remove a transition from Redis and add the transition again
 * Used to recover stuck transitions
 *
 * @return string
 */
function reset_redis_transition( $transition_uuid )
{
  $error = remove_db_transition_from_redis( $transition_uuid );

  // there may be RUNNING actions
  if ( ! $error )
    $error = reset_running_actions( $transition_uuid );

  if ( ! $error )
    $error = save_db_transition_to_redis( $transition_uuid );

  return $error;
}

/**
 * run_db_transitions
 *
 * Given one or more environments, and optionally a $customer_id, resolve up to $max_transitions but stops after $max_seconds
 * Input:
 *  $customer_id      (if needed)
 *  $environments     maps HTT_TRANSITION_LOG.HTT_ENVIRONMENT
 *  $max_transitions  maximun number of transitions to be run
 *  $max_seconds      time limit in seconds
 *  $redis            \Ultra\Lib\Util\Redis object
 *
 * Output:
 *  $result_transitions['success']              FALSE if at least 1 transition failed
 *  $result_transitions['open_transitions']     Transitions still in status OPEN after run_db_transitions call
 *  $result_transitions['closed_transitions']   Resolved transitions with status CLOSED
 *  $result_transitions['aborted_transitions']  Resolved transitions with status ABORTED
 *
 * @return array
 */
function run_db_transitions($customer_id=NULL, $environments=NULL, $max_transitions=10, $max_seconds=60, $redis=NULL)
{
  #dlog('', '(%s)', func_get_args());

  // initialize function result
  $result_transitions = array(
    'success'             => TRUE,
    'open_transitions'    => array(),
    'closed_transitions'  => array(),
    'aborted_transitions' => array()
  );

  // we need a \Ultra\Lib\Util\Redis object
  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis;

  $redis_transition = new \Ultra\Lib\Util\Redis\Transition( $redis );

  $redis_action = new \Ultra\Lib\Util\Redis\Action( $redis );

  $time_seconds_start = time();

  $current_transition_ord = 1;

  // get the next OPEN transition to resolve
  $transition = get_next_transition($customer_id, $environments);

  dlog('',"get_next_transition returned %s",$transition);

  $reserved_action = TRUE;

  // attempt the execution of more than one transitions,
  // the time limit is $max_seconds.
  while (
    ( ( $time_seconds_start + $max_seconds ) >= time() )
    &&
    ( $result_transitions['success'] )
    &&
    ( $current_transition_ord <= $max_transitions )
    &&
    ( NULL != $transition )
    &&
    ( $reserved_action )
  )
  {
    // reserve the next action which belongs to TRANSITION_UUID = $transition->TRANSITION_UUID
    $reserved_action = reserve_next_open_action( $transition->TRANSITION_UUID , $redis );
    dlog('',"reserve_next_open_action returned %s",$reserved_action);

    if ( $reserved_action )
    {
      // execute action

      $result_transitions['open_transitions'] = array( $transition->TRANSITION_UUID );

      $check2 = NULL;
      $error  = NULL;
      $result = NULL;
      $a_uuid = preg_replace( '/[\{\}]/' , '' , $reserved_action->ACTION_UUID );
      $t_uuid = preg_replace( '/[\{\}]/' , '' , $transition->TRANSITION_UUID  );

      try
      {
        global $current_action;
        $current_action = $reserved_action;

        dlog('',"run_or_enqueue_db_action invoked with TRANSITION_UUID = %s ; reserved_action = %s", $transition->TRANSITION_UUID, $reserved_action->ACTION_UUID );

        $check2 = run_or_enqueue_db_action($transition, $reserved_action);

        $current_action = NULL;
      }
      catch (Exception $e)
      {
        $error = $e->getMessage();
        dlog('', "Action execution error: $error");
      }

      $result_type = 'result';
      $abort_rest = FALSE;
      $errors = array();
      $toset = array('status' => 'ABORTED');

      if (NULL == $check2 || NULL != $error)
      {
        /* ABORTED/NULL */
        $abort_rest = TRUE;
        $result = $error;
        $result_type = 'error_result';
      }
      else if (is_array($check2))
      {
        if (TRUE == $check2['success'])
        {
          $result = $check2['result'];
          $toset['status'] = 'CLOSED';
          $toset['CLOSED'] = array('GETUTCDATE()');
        }
        else
        {
          $result = $check2['result'];
          $abort_rest = TRUE;
          $toset['CLOSED'] = array('GETUTCDATE()');

          if (array_key_exists('errors', $check2))
          {
            $errors = $check2['errors'];
          }
        }
      }

      if ( isset($result_transitions['errors']) )
      {
        $result_transitions['errors'] = array_merge( $result_transitions['errors'] , $errors );
      }
      else
      {
        $result_transitions['errors'] = $errors;
      }

      dlog('', "Based on check2 = %s we got %s",
             $check2,
             $toset,
             array('result'      => $result,
                   'result_type' => $result_type,
                   'abort_rest'  => $abort_rest));

      $toset['action_result'] = enclose_JSON($result);

      // update DB table HTT_ACTION_LOG, setting STATUS and/or RESULTS
      $check2_update = update_column2('htt_action_log',
                                      array('action_uuid' => $reserved_action->ACTION_UUID),
                                      $toset);

      if ( ! $check2_update )
      {
        dlog('',"htt_action_log update result failed");
        $result_transitions['success'] = FALSE;
      }

      // update action in Redis
      $redis->hset( 'ultra/action/'.$a_uuid , 'status' , $toset['status'] );

      // remove action from Redis sorted set
      $redis->zrem( 'ultra/sortedset/transitions/actions/'.$t_uuid , $a_uuid );

      // destroy action in Redis
      $redis_action->deleteObject( $a_uuid );

      if ($abort_rest)
      {
        // action and transition failed

        dlog('',"action and transition failed");

        // set all actions belonging to $transition->TRANSITION_UUID to 'ABORTED'
        // If this fails, there's not much we can do.
        $db_check = update_column2('htt_action_log',
                                 array('transition_uuid' => $transition->TRANSITION_UUID,
                                       'status' => 'OPEN'),
                                 array('status' => 'ABORTED',
                                       'CLOSED' => array('GETUTCDATE()')));

        if ( ! $db_check )
        {
          dlog('',"htt_action_log update failed");
          $result_transitions['success'] = FALSE;
        }

        // destroy all transition actions in Redis
        $redis_action->deleteObjectsByTransition( $transition->TRANSITION_UUID );

        // set transition $transition->TRANSITION_UUID to 'ABORTED'
        $db_check = abort_transition( $transition->TRANSITION_UUID , $redis , $redis_transition , $transition->CUSTOMER_ID );

        if ( ! $db_check )
        {
          dlog('',"htt_transition_log update failed");
          $result_transitions['success'] = FALSE;
        }

        $result_transitions['aborted_transitions'][] = $transition->TRANSITION_UUID;
        $result_transitions['open_transitions']      = array();

        // unreserve this TRANSITION_UUID
        $success = unreserve_transition_uuid_by_pid( $transition->TRANSITION_UUID, $redis, $transition->CUSTOMER_ID );

        // remove transition from sorted set
        $redis->zrem( 'ultra/sortedset/transitions/'. $transition->HTT_ENVIRONMENT . '/' . $transition->PRIORITY , $transition->TRANSITION_UUID );

        // discard transition
        $transition = NULL;
      }
      else
      {
        // action succeeded

        // if all actions are CLOSED, close the Transition
        if ( ! $redis->zcard( 'ultra/sortedset/transitions/actions/'.$t_uuid ) )
        {
          if ( ! close_transition( $transition->TRANSITION_UUID , $redis , $redis_transition , $transition->CUSTOMER_ID ) )
            dlog('',"htt_transition_log update to CLOSED failed");

          save_state($transition->CUSTOMER_ID,
                     $transition->TO_COS_ID,
                     $transition->TO_PLAN_STATE,
                     $transition->FROM_COS_ID,
                     $transition->FROM_PLAN_STATE,
                     $transition->TRANSITION_LABEL,
                     $transition->TRANSITION_UUID
          );

          $result_transitions['closed_transitions'][] = $transition->TRANSITION_UUID;
          $result_transitions['open_transitions']     = array();

          // unreserve this TRANSITION_UUID
          $success = unreserve_transition_uuid_by_pid( $transition->TRANSITION_UUID, $redis, $transition->CUSTOMER_ID );

          // remove transition from sorted set
          $redis->zrem( 'ultra/sortedset/transitions/'. $transition->HTT_ENVIRONMENT . '/' . $transition->PRIORITY , $transition->TRANSITION_UUID );

          // discard transition
          $transition = NULL;
        }
      }
    }
    else
    {
      // check if this transition has actions in the DB
      $actions = get_actions(
        array(
          "transition_uuid" => $transition->TRANSITION_UUID
        )
      );

      if ( $actions && is_array($actions) && count($actions) )
      {
        dlog('',"ESCALATION ALERT IMMEDIATE - we could reserve transition id %s, but we could not reserve any of its actions",$transition->TRANSITION_UUID);

        $running    = FALSE; // action currently running
        $all_closed = TRUE;  // are all actions closed?

        // loop through actions which belong to this transition
        foreach( $actions as $action )
        {
          if ( preg_match('/^RUNNING/', $action->STATUS) )
          {
            $running = $action;
            $all_closed = FALSE;
          }
          elseif ( $action->STATUS != 'CLOSED' )
          {
            $all_closed = FALSE;
          }
        }

        // check if an action of this transition is running for more than 21 minutes, if yes we have to reset it to OPEN
        if ( $running && is_object($running) )
        {
          if ( $running->PENDING_SINCE_MINUTES > 21 )
          {
            // this action is running for more than 21 minutes, we have to reset it to OPEN
            dlog('',"this action is running for more than 21 minutes, we have to reset it to OPEN : %s",$running);

            if ( reopen_action( $running->ACTION_UUID ) )
            {
              // update Redis object
              $a_uuid = preg_replace( '/[\{\}]/' , '' , $running->ACTION_UUID );

              $redis->hset( 'ultra/action/'.$a_uuid , 'status' , 'OPEN' );
            }
            else
            {
              dlog('',"cannot reopen action");
            }
          }
          elseif ( ( $running->ACTION_TYPE == 'sql' ) && ( $running->PENDING_SINCE_MINUTES > 1 ) )
          {
            // this SQL action is running for more than 1 minute, we have to reset it to OPEN
            dlog('',"this SQL action is running for more than 1 minutes, we have to reset it to OPEN : %s",$running);

            if ( reopen_action( $running->ACTION_UUID ) )
            {
              // update Redis object
              $a_uuid = preg_replace( '/[\{\}]/' , '' , $running->ACTION_UUID );

              $redis->hset( 'ultra/action/'.$a_uuid , 'status' , 'OPEN' );
            }
            else
            {
              dlog('',"cannot reopen action");
            }
          }
        }
        elseif ( $all_closed )
        {
          // all actions are closed, we can close the transition
          if ( ! close_transition( $transition->TRANSITION_UUID , $redis , $redis_transition , $transition->CUSTOMER_ID ) )
            dlog('',"htt_transition_log update to CLOSED failed");

          save_state($transition->CUSTOMER_ID,
                     $transition->TO_COS_ID,
                     $transition->TO_PLAN_STATE,
                     $transition->FROM_COS_ID,
                     $transition->FROM_PLAN_STATE,
                     $transition->TRANSITION_LABEL,
                     $transition->TRANSITION_UUID
          );

          $result_transitions['closed_transitions'][] = $transition->TRANSITION_UUID;
          $result_transitions['open_transitions']     = array();

          // unreserve this TRANSITION_UUID
          $success = unreserve_transition_uuid_by_pid( $transition->TRANSITION_UUID, $redis, $transition->CUSTOMER_ID );

          // remove transition from sorted set
          $redis->zrem( 'ultra/sortedset/transitions/'. $transition->HTT_ENVIRONMENT . '/' . $transition->PRIORITY , $transition->TRANSITION_UUID );

          // discard transition
          $transition = NULL;
        }
        else
        {
          dlog('',"problem case : actions = %s",$actions);

          // remove transition from sorted set
          $redis->zrem( 'ultra/sortedset/transitions/'. $transition->HTT_ENVIRONMENT . '/' . $transition->PRIORITY , $transition->TRANSITION_UUID );
        }
      }
      else
      {
        // this transition has no actions, we can close it

        if ( ! close_transition( $transition->TRANSITION_UUID , $redis , $redis_transition , $transition->CUSTOMER_ID ) )
          dlog('',"htt_transition_log update to CLOSED failed");

        save_state($transition->CUSTOMER_ID,
                   $transition->TO_COS_ID,
                   $transition->TO_PLAN_STATE,
                   $transition->FROM_COS_ID,
                   $transition->FROM_PLAN_STATE,
                   $transition->TRANSITION_LABEL,
                   $transition->TRANSITION_UUID
        );

        $result_transitions['closed_transitions'][] = $transition->TRANSITION_UUID;
        $result_transitions['open_transitions']     = array();

        // unreserve this TRANSITION_UUID
        $success = unreserve_transition_uuid_by_pid( $transition->TRANSITION_UUID, $redis, $transition->CUSTOMER_ID );

        // remove transition from sorted set
        $redis->zrem( 'ultra/sortedset/transitions/'. $transition->HTT_ENVIRONMENT . '/' . $transition->PRIORITY , $transition->TRANSITION_UUID );

        // discard transition
        $transition = NULL;
      }
    }

    // if there are the conditions, we get the next transition
    if ( ( NULL == $transition )
      && ( ( $time_seconds_start + $max_seconds ) >= time() )
      && ( $result_transitions['success'] )
      && ( $current_transition_ord <= $max_transitions )
    )
    {
      // get the next OPEN transition to resolve
      $transition = get_next_transition($customer_id, $environments);

      dlog('',"get_next_transition returned %s",$transition);

      if ( $transition )
        $current_transition_ord++;
    }
  }

  if ( $transition )
  {
    // unreserve this Transition UUID
    $success = unreserve_transition_uuid_by_pid( $transition->TRANSITION_UUID, $redis, $transition->CUSTOMER_ID );
  }

  return $result_transitions;
}

/**
 * run_or_enqueue_db_action
 *
 * Executes a single action
 *
 * @returns array or boolean
 */
function run_or_enqueue_db_action($transition, $action)
{
  global $action_history;

  $errors = array();

  $action_history[] = $action->ACTION_UUID;

  // we could keep those info on Redis and avoid this additional query
  $parameters = get_by_column('htt_action_parameter_log',
                              array('action_uuid' => $action->ACTION_UUID),
                              'ORDER BY param',
                              NULL);

  if ( ( ! $parameters ) || ( ! is_array($parameters) ) )
  {
    dlog('', "Could not query HTT_ACTION_PARAMETER_LOG (%s)",$action->ACTION_UUID);

    return array('success'            => FALSE,
                 $action->ACTION_TYPE => $action->ACTION_NAME,
                 'pending'            => FALSE,
                 'errors'             => array( "Could not query HTT_ACTION_PARAMETER_LOG ".$action->ACTION_UUID ),
                 'result'             => ''
    );
  }

  $context = disclose_JSON($transition->CONTEXT);

  if (NULL == $context)
  {
    dlog('',
         "Could not decode context = '%s' for transition %s",
         $transition->CONTEXT,
         $transition->TRANSITION_UUID);
    $context = array();
  }

  /* add this_transition and this_action_seq context vals */
  $context['this_transition'] = $transition->TRANSITION_UUID;
  $context['this_action']     = $action->ACTION_UUID;
  $context['this_action_seq'] = $action->ACTION_SEQ;
  $context['action_history']  = $action_history;

  $context['this_transition_FROM_COS_ID'] = $transition->FROM_COS_ID;
  $context['this_transition_FROM_PLAN_STATE'] = $transition->FROM_PLAN_STATE;
  $context['this_transition_TO_COS_ID'] = $transition->TO_COS_ID;
  $context['this_transition_TO_PLAN_STATE'] = $transition->TO_PLAN_STATE;

  if (isset($context['customer_id']))
  {
    $customer = customers_get_customer_by_customer_id($context['customer_id'], ['POSTAL_CODE']);
    $overlay  = get_ultra_customer_from_customer_id($context['customer_id'], [
      'current_mobile_number',
      'CURRENT_ICCID_FULL',
      'preferred_language'
    ]);

    if ($customer && $overlay)
    {
      $context['customer:MSISDN']         = $overlay->current_mobile_number;
      $context['customer:ICCID']          = $overlay->CURRENT_ICCID_FULL;
      $context['customer:ACTIVATION_ZIP'] = $customer->POSTAL_CODE;
      $context['customer:LANGUAGE']       = strtolower($overlay->preferred_language);
    }
    else
    { dlog('',"Cannot find customer"); }
  }

  $decoded_parameters = array();
  foreach ($parameters as $p)
  {
    if (NULL === $p->VAL)
    {
      dlog('',
           "Could not decode parameter %s = '%s' for action %s",
           $p->PARAM,
           $p->VAL,
           $action->ACTION_UUID);
    }
    else
    {
      // propagate value to context, if applicable
      $value = disclose_JSON($p->VAL);
      if (is_string($value))
      {
        dlog('__detail__', "Replacing '$value' with context %s", $context);
        foreach ($context as $ck => $cv)
        {
          if ( ! is_array( $cv ) )
          {
            dlog('__detail__', "preg_replace(/__${ck}__/, $cv, $value)");
            $value = preg_replace("/__${ck}__/", "$cv", $value);
          }
        }
        dlog('__detail__', "value is now '$value'");
      }

      // this attempts to preserve order in the array
      if (is_numeric($p->PARAM))
        $decoded_parameters[] = $value;
      else
        $decoded_parameters[$p->PARAM] = $value;
    }
  }

  dlog('', "Running action %s", $action);

  // checking $action->ACTION_TYPE for all possible types

  if ($action->ACTION_TYPE === 'TODO')
  {
    dlog('', "Running DB TODO transition+action: %s", array($transition, $action));

    return array('success' => TRUE,
                 $action->ACTION_TYPE => $action->ACTION_NAME,
                 'pending' => FALSE,
                 'errors' => $errors,
                 'result' => array());
  }
  else if ($action->ACTION_TYPE === 'funcall')
  {
    dlog('', "Running DB funcall transition+action: %s",
         array($transition,
               $action,
               $decoded_parameters));

    $is_ok = function_exists($action->ACTION_NAME);
    $result = NULL;
    $is_pending = FALSE;
    $errors = array();

    if ($is_ok)
    {
      dlog('', "we are executing the function %s with parameters: %s",
           $action->ACTION_NAME,
           $decoded_parameters);

      $result = call_user_func_array($action->ACTION_NAME,
                                     $decoded_parameters);

      if ($result instanceof Result)
      {
        $result = $result->to_array();
      }

      dlog('',json_encode($result));

      if (is_array($result) && array_key_exists('pending', $result))
      {
        $is_pending = $result['pending'];
      }

      if (is_array($result) && array_key_exists('success', $result))
      {
        $is_ok = $result['success'];
      }

      if (is_array($result) && array_key_exists('errors', $result))
      {
        $errors = $result['errors'];
      }
    }
    else
    {
      dlog('', "PROBLEM: the function %s does not exist in the included code (1)",
           $action->ACTION_NAME);
    }

    return array('success' => $is_ok,
                 $action->ACTION_TYPE => $action->ACTION_NAME,
                 'pending' => $is_pending,
                 'errors' => $errors,
                 'result' => $result);
  }
  else if ($action->ACTION_TYPE === 'pfuncall')
  {

    dlog('', "Running DB pfuncall transition+action: %s",
         array($transition,
               $action,
               $decoded_parameters));

    $is_ok = function_exists($action->ACTION_NAME);
    $result = NULL;
    $is_pending = FALSE;

    if ($is_ok)
    {
      dlog('', "we are executing the function %s with parameters: %s",
           $action->ACTION_NAME,
           $decoded_parameters);

      $result = call_user_func_array($action->ACTION_NAME,
                                     array($decoded_parameters));

      if (is_array($result) && array_key_exists('pending', $result))
        $is_pending = $result['pending'];

      if (is_array($result) && array_key_exists('success', $result))
        $is_ok = $result['success'];

      if (is_array($result) && array_key_exists('errors', $result))
        $errors = $result['errors'];
    }
    else
    {
      dlog('', "PROBLEM: the function %s does not exist in the included code (2)",
           $action->ACTION_NAME );
    }

    return array('success' => $is_ok,
                 $action->ACTION_TYPE => $action->ACTION_NAME,
                 'pending' => $is_pending,
                 'errors' => $errors,
                 'result' => $result);
  }
  else if ($action->ACTION_TYPE === 'sql')
  {
    $params = array($action->ACTION_SQL);
    $params = array_merge($params, $decoded_parameters);

    dlog('', "Running DB SQL transition+action: %s",
         array($transition,
               $action,
               $params));

    $q = call_user_func_array('sprintf', $params);
    return array('success' => run_sql_and_check($q),
                 $action->ACTION_TYPE => $action->ACTION_NAME,
                 'pending' => FALSE,
                 'errors' => $errors,
                 'result' => $q);
  }

  return FALSE;
}

// crazy old callback logic from internal__UpdateActionResult
function update_action($context, $action_id, $status, $result, $resolve_now)
{
  dlog('','Entering update_action.');
  $ret = array(
    'success' => FALSE,
    'errors' => array('OBSOLETE LOGIC')
  );
  return $ret;
}

/**
 * extract_actions
 *
 * Extract_actions gets the actions (departure, arrival, or regular) from a state or a transition
 * 'extract' will contain the extracted actions for a single transition.
 * Not used by the action code
 *
 * @return array
 */
function extract_actions($thing, $given_type=NULL)
{
  $types = $given_type == NULL ? array(ARRIVAL_ACTIONS, ACTIONS, DEPARTURE_ACTIONS,'todo') : array($given_type);

  $print = array();
  if (is_array($thing))
  {
    foreach ($types as $type)
    {
      if (isset($thing[$type]) && is_array($thing[$type]))
      {
        foreach ($thing[$type] as $action)
        {
          if (is_string($action))
          {
            $print[] = indicate_action($type, $action);
          }
          else if (isset($action['funcall']))
          {
            $print[] = indicate_action($type, $action['funcall'], $action['fparams']);
          }
          else if (isset($action['pfuncall']))
          {
            $print[] = indicate_action($type, $action['pfuncall'], $action['params']);
          }
          else if (isset($action['async_command']))
          {
            $print[] = indicate_action($type, $action['async_command'], $action['params']);
          }
          else if (isset($action['sql']))
          {
            $print[] = indicate_action($type, $action['sql'], $action['fparams']);
          }
        }
      }
    }
  }

  return $print;
}

/**
 * indicate_action
 *
 * @return array
 */
function indicate_action($type, $head, $data2=NULL)
{
  if ($type === ARRIVAL_ACTIONS)
    $head = "-- $head";
  else if ($type === DEPARTURE_ACTIONS)
    $head =  "${head} --";
  else if ($type === ACTIONS)
    $head = "- $head";

  if ($data2 == NULL)
    return array("TODO" => $head);

  return array($head => $data2);
}

/**
 * extract_requires
 *
 * @return array
 */
function extract_requires($thing)
{
  $print = array();
  foreach (array('requires') as $type)
  {
    if (is_array($thing) && isset($thing[$type]) && is_array($thing[$type]))
    {
      $print[] = array($type => $thing[$type]);
    }
  }

  return $print;
}

/**
 * state_equal
 *
 * @return boolean
 */
function state_equal($state1, $state2)
{
  $names = array($state2['plan']);
  if (isset($state2['info']['aka']))
  {
    foreach (explode(' ', "".$state2['info']['aka']) as $aka)
    {
      $names[] = $aka;
    }
  }

  foreach ($names as $name)
  {
    if (state_equal2($state1, $name, $state2['state']))
    {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * state_equal2
 *
 * @return boolean
 */
function state_equal2($state, $plan, $state_name)
{
  $names = array($state['plan']);
  if (isset($state['info']['aka']))
  {
    foreach (explode(' ', "".$state['info']['aka']) as $aka)
    {
      $names[] = $aka;
    }
  }

  return
    in_array($plan, $names) &&
    $state['state'] === $state_name;
}

/**
 * find_path
 *
 * Attempts to find a path in the state graph from $current .
 * A special case is when ( 'take transition' == $state_name ) and we wish to pass through a specific edge of the graph.
 *
 * @return array or NULL
 */
function find_path($max_path_depth, $transitions, $context, $current, $plan, $state_name, $visited, $edge=NULL)
{
  global $blocked;

  $explicit_edge = 'take transition' == $state_name;

  dlog('transitions',
       "max path depth = $max_path_depth, entering $current[print]; looking for $plan/$state_name, edge = %s",
      $edge);

  if ($max_path_depth < 0) return NULL;

  // match transitions by name
  if ($explicit_edge && null != $edge && $plan === $edge['label'])
  {
    return NULL == $edge ? array($current) : array($current, $edge);
  }
  if (NULL != $edge && state_equal2($current, $plan, $state_name))
  {
    return NULL == $edge ? array($current) : array($current, $edge);
  }

  foreach ($visited as $check)
  {
    if (json_encode($check) === json_encode($current))
    {
      //loop found
      return NULL;
    }
  }

  $visited[] = $current;

  /* inefficient breadth-first search */
  $depth = 99;
  $solution = NULL;
  foreach ($transitions as $transition)
  {
    $source = $transition['source'];
    $dest = $transition['dest'];

    /* if it's a transition from the current state... */
    if (state_equal($source, $current))
    {
      $check = TRUE;
      $check_requires = TRUE;

      dlog('transitions', "found useful transition $transition[print]");
      /* try to recurse to find a path */

      if ($check)
      {
        $check = find_path($max_path_depth-1, $transitions, $context, $dest, $plan, $state_name, $visited, $transition);
      }

      if ($check && isset($transition['requires']))
      {
        dlog('', "REQUIREMENT CHECK FOR $transition[print]");

        if (!isset($context['customer']))
        {
          $context['customer'] = find_customer(customer_liberal_select_query($context));
        }

        $context['explicit'] = $explicit_edge;
        $check_requires = validate_requires($context, $transition['requires'], $context['customer']);
        if (!$check_requires)
        {
          $blocked[] = $transition['print'];
        }
      }

      if ($check && $check_requires)
      {
        if (NULL == $solution || $depth > count($check))
        {
          $solution = $check;
          $depth = count($check);
        }
      }
    }
    else
    {
      dlog('transitions',
           "states not equal $source[print] != $current[print]");
    }
  }

  if ($solution)
  {
    $solution[] = $current;

    if (NULL != $edge)
    {
      $solution[] = $edge;
    }

    foreach ($solution as $step)
    {
      $step['source_transition'] = $transition['print'];
    }

    return $solution;
  }

  return NULL;
}

/**
 * load_state
 *
 * @return array or NULL
 */
function load_state($states=NULL, $context, $given_plan=NULL, $given_state_name=NULL, $customer_full=NULL)
{
  if (NULL != $given_state_name && NULL != $given_plan)
    return find_state($states, $given_state_name, $given_plan);

  $customer_id = $context['customer_id'];

  if (NULL == $customer_id)
  {
    dlog('', "Can't load state from context %s", $context);
    return NULL;
  }

  if ( ! $customer_full )
    $customer_full = get_customer_from_customer_id($customer_id);

  if (NULL == $customer_full)
  {
    dlog('', "Can't load customer %d", 0+$customer_id);
    return NULL;
  }

  if (NULL == $customer_full)
  {
    dlog('', "Can't load ultra state for customer %d", 0+$customer_id);
    return find_state($states, 'Cancelled', NULL, $customer_full->COS_ID);
  }

  $states = $states == NULL ? ultra_states() : $states;

  $cos_id = ( $customer_full->plan_state === 'Cancelled' ) ? NULL : $customer_full->COS_ID ;

  return find_state($states,
                    $customer_full->plan_state,
                    NULL,
                    $cos_id);
}

/**
 * save_state
 *
 * update
 * - ACCOUNTS.COS_ID
 * - HTT_CUSTOMERS_OVERLAY_ULTRA.PLAN_STATE
 * - ULTRA.HTT_ACTIVATION_HISTORY
 *
 * @return boolean
 */
function save_state($customer_id, $cos_id, $state_name, $from_cos_id=NULL, $from_state_name=NULL, $label=NULL, $uuid=NULL)
{
  dlog('', '(%s)', func_get_args());

  if (NULL == $customer_id)
  {
    dlog('', "Can't save state, no customer ID");
    return FALSE;
  }

  if (NULL == $cos_id || NULL == $state_name)
  {
    dlog('', "nothing to save, cos_id or state name are NULL");
    return FALSE;
  }

  if ( ! start_mssql_transaction() )
    return FALSE;

  $history_params = array();

  // should we update ACCOUNTS.COS_ID ?
  if ( $cos_id != $from_cos_id )
  {
    $check = run_sql_and_check(
      accounts_update_query(
        array(
          'customer_id' => $customer_id,
          'cos_id'      => $cos_id
        )
      )
    );

    if (!$check)
    {
      rollback_mssql_transaction();

      return FALSE;
    }
  }

  if ( $cos_id )
    $history_params['cos_id'] = $cos_id;

  // should we update HTT_CUSTOMERS_OVERLAY_ULTRA.PLAN_STATE ?
  if ( $state_name != $from_state_name )
  {
    $check = run_sql_and_check(
      htt_customers_overlay_ultra_update_query(
        array(
          'customer_id' => $customer_id,
          'plan_state'  => $state_name
        )
      )
    );

    if (!$check)
    {
      rollback_mssql_transaction();

      return FALSE;
    }
  }

  if ( $state_name
    && ( $state_name != 'Suspended' )
    && !( ( $state_name == 'Active'    ) && ( $from_state_name == 'Active'    ) )
    && !( ( $state_name == 'Active'    ) && ( $from_state_name == 'Suspended' ) )
  )
    $history_params['final_state'] = ( $state_name == 'Active' ) ? FINAL_STATE_COMPLETE : $state_name;

/*
  if ( ( ( $state_name == 'Active'       ) && ( $from_state_name == 'Neutral'      ) )
    || ( ( $state_name == 'Active'       ) && ( $from_state_name == 'Promo Unused' ) )
    || ( ( $state_name == 'Promo Unused' ) && ( $from_state_name == 'Neutral' ) )
    || ( ( $state_name == 'Provisioned'  ) && ( $from_state_name == 'Neutral' ) )
  )
    $history_params['activation_type'] = 'NEW';
*/

  if ( $state_name
    && $uuid
    && ( $state_name == 'Active' )
    && ( $from_state_name != 'Active' )
    && ( $from_state_name != 'Suspended' )
  )
    $history_params['activation_transition'] = $uuid;

  if ( $state_name == 'Port-In Requested' )
    $history_params['activation_type'] = 'PORT';

  if ( $label )
    $history_params['last_attempted_transition'] = $label;

  // ULTRA.HTT_ACTIVATION_HISTORY.FINAL_STATE should remain FINAL_STATE_COMPLETE if a customer has been 'Active' once.
  $skip_cancelled_history = (
       ( $from_state_name == 'Suspended' )
    || ( $from_state_name == 'Active'    )
  );

  // should we update ULTRA.HTT_ACTIVATION_HISTORY
  if ( count($history_params) && !$skip_cancelled_history )
  {
    $history_params['customer_id'] = $customer_id;

    $sql = ultra_activation_history_update_query( $history_params );

    $check = is_mssql_successful(logged_mssql_query($sql));

    // non-fatal error
    if (!$check)
      dlog('',"ULTRA.HTT_ACTIVATION_HISTORY could not be updated");

    flush_ultra_activation_history_cache_by_customer_id( $customer_id );
  }

  return commit_mssql_transaction();
}

/**
 * set_delta_minutes
 *
 * Set the value of package_balance1
 *
 * @return array
 */
function set_delta_minutes($customer_id, $talk_minutes, $intl_minutes)
{
  // compares the current plan's intl_minutes (mid transistion) with the $intl_minutes provided; and 
  // adds additional minutes to the plan, if necessary; to make up the difference.

  $ret = array(
    'success' => FALSE,
    'aborted' => FALSE,
    'errors' => array(),
    'log' => array(),
  );

  dlog('', "customer_id  = $customer_id ; talk_minutes = $talk_minutes ; intl_minutes = $intl_minutes");

  /* we ignore the talk minutes, they are always inf for now */
  $delta = 0;

  $current = load_state(NULL, array('customer_id' => $customer_id));

  if ($current == NULL)
  {
    $ret['errors'][] = "ERR_API_INTERNAL: could not load the customer plan";
    return $ret;
  }

  if (isset($current['info']['intl_minutes']))
  {
    $current_minutes = 0;
    if ($current['state'] === 'Active')
    {
      $current_minutes = $current['info']['intl_minutes'];
      dlog('',
           "current minutes %d came from an %s plan",
           $current_minutes,
           $current['state']);
    }
    else
    {
      dlog('',
           "current plan %s has no minutes",
           $current['state']);
    }

    $delta = $intl_minutes - $current_minutes;
    dlog('', "delta minutes of customer %d: %d - %d = %d",
         $customer_id,
         $intl_minutes,
         $current_minutes,
         $delta);
  }

  dlog('', "Delta minutes = $delta");

  if ($delta > 0 &&
      !run_sql_and_check(
        accounts_update_query(
          array(
            'add_to_package_balance1' => ( $delta / 10 ),
            'customer_id'             => $customer_id
          )
        )
      )
    )
  {
    $ret['errors'][] = "ERR_API_INTERNAL: could not update the minutes balance";
    return $ret;
  }
  else if ($delta <= 0)
  {
    dlog('', "Delta minutes $delta are 0 or less, nothing to do");
  }

  $ret['success'] = TRUE;
  return $ret;
}

// this function is currently used only in internal__CheckTransition which apparently is not used anywhere
function explain_transition($transition_id)
{
  $r = array();

  $transition = get_by_column('htt_transition_log',
                              array('transition_uuid' => $transition_id));

  $r[] = sprintf("
Transition %s: ( SELECT * FROM htt_action_log a, htt_transition_log t WHERE t.transition_uuid = a.transition_uuid AND t.transition_uuid = %s ORDER BY t.created, a.transition_uuid, action_seq )
  STATUS %s
  CUSTOMER_ID %s HTT_ENVIRONMENT %s
  CREATED %s
  CLOSED  %s
  FROM_COS_ID %8s FROM_PLAN_STATE %s
  TO_COS_ID   %8s TO_PLAN_STATE   %s
  CONTEXT %s
",
                 mssql_quote_or_null($transition->TRANSITION_UUID),
                 mssql_quote_or_null($transition->TRANSITION_UUID),
                 mssql_quote_or_null($transition->STATUS),
                 mssql_quote_or_null($transition->CUSTOMER_ID), mssql_quote_or_null($transition->HTT_ENVIRONMENT),
                 mssql_quote_or_null($transition->CREATED),
                 mssql_quote_or_null($transition->CLOSED),
                 mssql_quote_or_null($transition->FROM_COS_ID), mssql_quote_or_null($transition->FROM_PLAN_STATE),
                 mssql_quote_or_null($transition->TO_COS_ID), mssql_quote_or_null($transition->TO_PLAN_STATE),
                 mssql_quote_or_null($transition->CONTEXT));

  $actions = get_by_column('htt_action_log',
                           array('transition_uuid' => $transition_id),
                           'ORDER BY action_seq',
                           '');

  foreach ($actions as $a)
  {
    $parameters = '';

    foreach (get_by_column('htt_action_parameter_log',
                           array('action_uuid' => $a->ACTION_UUID),
                           '',
                           '') as $p)
    {
      $parameters .= sprintf("\n    -> Parameter %s = %s", $p->PARAM, $p->VAL);
    }

  $r[] = sprintf("
  Action %s ( SELECT * FROM htt_action_log WHERE action_uuid = %s )
    STATUS %s
    CREATED       %s
    PENDING_SINCE %s
    CLOSED        %s
    ACTION_SEQ %s ACTION_TYPE %s ACTION_NAME %s%s
    ACTION_RESULT %s
    ACTION_SQL %s
",

                 mssql_quote_or_null($a->ACTION_UUID),
                 mssql_quote_or_null($a->ACTION_UUID),
                 mssql_quote_or_null($a->STATUS),
                 mssql_quote_or_null($a->CREATED),
                 mssql_quote_or_null($a->PENDING_SINCE),
                 mssql_quote_or_null($a->CLOSED),
                 mssql_quote_or_null($a->ACTION_SEQ), mssql_quote_or_null($a->ACTION_TYPE), mssql_quote_or_null($a->ACTION_NAME),
                 $parameters,
                 mssql_quote_or_null($a->ACTION_RESULT),
                 mssql_quote_or_null($a->ACTION_SQL));

  }

  return $r;
}

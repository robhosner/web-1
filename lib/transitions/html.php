<?php

function ultra_transitions2html($transitions)
{
  $ret = '<html>

<head>
  <title>Ultra Transitions</title>

  <script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

  <style type="text/css">
  </style>

    <script type="text/javascript" charset="utf-8">
      jQuery(document).ready(function()
      {
        $("h4").hide();
        $("h5").hide();
        $("h6").hide();
        $("h3").click(function()
        {
          $(this).find(".indicator").fadeOut().fadeIn();
          $(this).nextUntil("h3").toggle();
        });
      });

    </script>

</head>

<body>
';

  $byplan = array();

  $colors = array(
    'Active' => 'green',
    'Provisioned' => 'grey',
    'Suspended' => 'grey',
    'Cancelled' => 'red',
    'Port-In Requested' => 'purple',
    'Port-In Denied' => 'red',
    'Port-In Approved' => 'green',
    'Pre-Funded' => 'brown',
    'Neutral' => 'blue'
    );


  foreach ($transitions == NULL ? ultra_transitions() : $transitions as $transition)
  {
    $source = $transition['source'];
    $dest = $transition['dest'];

    $byplan[$source['info']['name']][] = $transition;
  }

  $later = array();

  $ret = $ret . "<h1>Plans</h1>";

  foreach ($byplan as $plan => $transitions)
  {
    $ret = $ret . "<h2>Plan $plan</h2>";

    foreach ($transitions as $transition)
    {
      $source = $transition['source'];
      $source_info = $source['info'];

      $dest = $transition['dest'];
      $dest_info = $dest['info'];

      $node_color = isset($colors[$source['state']]) ? $colors[$source['state']] : 'lightgrey';

      $edge_color = isset($colors[$dest['state']]) ? $colors[$dest['state']] : 'lightgrey';
      $internal = $source['plan'] === $dest['plan'];

      $indicator = '<span class="indicator">__INDICATOR__</span>';

      $ret = $ret . sprintf("<h3>%s → %s$indicator</h3>
%s
%s
%s",
                            html_span($source['state'], $node_color),
                            html_span($dest[$internal ? 'state' : 'print'], $edge_color),
                            (isset($transition['desc']) ? "<div>Transition desc: $transition[desc]</div>" : ''),
                            (isset($source['desc']) ? "<div>Source desc: $source[desc]</div>" : ''),
                            (isset($dest['desc']) ? "<div>Destination desc: $dest[desc]</div>" : ''));

      $count = 0;
      if (count($source['extract']) > 0)
      {
        $count += count($source['extract']);
        $ret = $ret . "<h4>Source</h4>";
        $ret = $ret . extract2html('h5', $source['extract']);
      }

      if (count($transition['extract']) > 0)
      {
        $count += count($transition['extract']);
        $ret = $ret . "<h4>Transition</h4>";
        $ret = $ret . extract2html('h5', $transition['extract']);
      }

      if (count($dest['extract']) > 0)
      {
        $count += count($dest['extract']);
        $ret = $ret . "<h4>Destination</h4>";
        $ret = $ret . extract2html('h5', $dest['extract']);
      }

      $indicator_inside = $count > 0 ? "+$count" : "";
      $ret = preg_replace("/__INDICATOR__/",$indicator_inside,$ret);
    }
  }

  return $ret . "
</body>
</html>";
}

function extract2html($tag, $extracted)
{
  $ret = '';
  foreach ($extracted as $e)
  {
    foreach ($e as $k => $v)
    {
      if (is_string($v))
      {
        $ret = $ret . sprintf("<$tag>%s = %s</$tag>",
                              $k, $v);
      }
      else
      {
        $ret = $ret . sprintf("<$tag>%s",
                              $k);

        foreach ($v as $kk => $vv)
        {
          $ret = $ret . sprintf(", %s = %s",
                                $kk, $vv);
        }

        $ret = $ret . "</$tag>";
      }
    }
  }

  return $ret;
}

function html_span($text, $color)
{
  return "<span style=\"color:$color\">" . $text . "</span>";
}

function ultra_transitions2dot($transitions)
{
  $ret = "digraph ultra_transitions {
graph [compound=true remincross=true root=\"state_STANDBY_Neutral\"];
node [shape=record];

subgraph cluster_cancelled {
label = \"Ultra SIM Cancel\";
color=red;
node [style=filled,color=lightgrey];
state_STANDBY_Cancelled [ group = \"STANDBY\" color = \"red\", label = \"Cancelled\" ];
}

";
  $shown_states = array();

  $byplan = array();

  $colors = array(
    'Active' => 'green',
    'Provisioned' => 'grey',
    'Suspended' => 'grey',
    'Cancelled' => 'red',
    'Port-In Requested' => 'purple',
    'Port-In Denied' => 'red',
    'Port-In Approved' => 'green',
    'Pre-Funded' => 'brown',
    'Neutral' => 'blue'
    );


  foreach ($transitions == NULL ? ultra_transitions() : $transitions as $transition)
  {
    $source = $transition['source'];
    $dest = $transition['dest'];

    $byplan[$source['info']['name']][] = $transition;
  }

  $later = array();

  foreach ($byplan as $plan => $transitions)
  {
    $ret = $ret . "subgraph cluster_$source[plan] {
label = \"$plan\";
color=blue;
node [style=filled,color=lightgrey];
";

    foreach ($transitions as $transition)
    {
      $source = $transition['source'];
      $source_info = $source['info'];
      $source_label = '"' . ($source['print'] ? $source['print'] : $source['label']) . '"';

      $dest = $transition['dest'];
      $dest_info = $dest['info'];
      $dest_label = '"' . ($dest['print'] ? $dest['print'] : $dest['label']) . '"';

      $internal = $source['plan'] === $dest['plan'];

      $node_color = isset($colors[$source['state']]) ? $colors[$source['state']] : 'lightgrey';
      $edge_color = isset($colors[$dest['state']]) ? $colors[$dest['state']] : 'lightgrey';

      $transition_label = "$source_info[name]/$source[state] to $dest_info[name]/$dest[state]";
      $to_cluster = NULL;

      if (isset($transition['label']))
      {
        $transition_label = '"' . $transition['label'] . '"';

        if ($source['state'] === 'Active' && $dest['state'] === $source['state'])
        {
          $to_cluster = "cluster_$dest[plan]";
        }
      }
      else if ($internal)
      {
        $transition_label = "$source[state] to $dest[state]";
      }

      if (!isset($shown_states[$source['name']]))
      {
        $ret = $ret . "$source[name] [ group = \"$source[plan]\" color = \"$node_color\", label = $source_label ]\n";
        $shown_states[$source['name']] = 1;
      }

      if ($internal)
      {
        if (!isset($shown_states[$dest['name']]))
        {
          $ret = $ret . "$dest[name] [ group = \"$dest[plan]\" color = \"$edge_color\", label = $dest_label ]\n";
          $shown_states[$dest['name']] = 1;
        }

        $ret = $ret . "$source[name] -> $dest[name] [ color = \"$edge_color\" label = $transition_label ]\n";
      }
      else if ($to_cluster == NULL)
      {
        $later[] = "$source[name] -> $dest[name] [ color = \"$edge_color\" label = $transition_label ]\n";
      }
      else
      {
        $later[] = "$source[name] -> $dest[name] [ lhead=\"cluster_$source[plan]\" ltail=\"$to_cluster\" color = \"$edge_color\" label = $transition_label ]\n";
      }
    }

    $ret = $ret . "}\n";

  }

  $ret = $ret . implode("\n", $later) . "}\n";

  return $ret;
}

?>

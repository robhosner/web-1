<?php

require_once 'db.php';
require_once 'lib/util-runners.php';

class FindObjectByPropertyTest extends PHPUnit_Framework_TestCase
{
  function test_readCommandLineArguments()
  {
    // we need to modify globals in order to execute this test
    global $argc, $argv;
    $argc = 3;
    $argv[1] = 'today=Monday'; // valid argument
    $argv[2] = 'tomorrow-Sunday'; // invalid argument

    // blank defaults test
    $result = readCommandLineArguments();
    print_r($result);
    $this->assertContains('Monday', $result);

    // default override test
    $params = array('today' => 'Sunday');
    $result = readCommandLineArguments($params);
    print_r($result);
    $this->assertContains('Monday', $result);

    // detect malformed parameters test
    $result = readCommandLineArguments($params, FALSE);
    print_r($result);
    $this->assertEquals(NULL, $result);
  }


  function test_pidFile()
  {
    $dir = '/tmp';
    $pid = 'phpunit';
    $result = createPidFile($dir, $pid);
    $this->assertTrue($result);
    $result = unlinkPidFile($dir, $pid);
    $this->assertTrue($result);
  }

  function test_readNormalizedCommandLineArguments()
  {
    $defaults = array(
    'daily_init'        => array('type' => 'boolean', 'default' => FALSE),
    'date'              => array('type' => 'string',  'default' => 'today'),
    'cancellations'     => array('type' => 'boolean', 'default' => FALSE),
    'customer_id_list'  => array('type' => 'array',   'default' => array()),
    'groups'            => array('type' => 'array',   'default' => array()),
    'mac_customers'     => array('type' => 'integer', 'default' => NULL),
    'retry'             => array('type' => 'boolean', 'default' => FALSE),
    'skip_cc'           => array('type' => 'boolean', 'default' => FALSE),
    'value'             => array('type' => 'integer', 'default' => 29));

    // we need to modify globals in order to execute this test
    global $argc, $argv;
    $argc = 3;
    $argv[1] = 'date=2015-10-10';
    $argv[2] = 'skip_cc=yes';
    $argv[3] = 'groups=2,3,4';
    $argv[4] = 'value=6';

    // get script input
    $result = readNormalizedCommandLineArguments($defaults, FALSE);
    print_r($result);
    $this->assertContains('date', $result);
    $this->assertEquals($result['date'], '2015-10-10');
    $this->assertContains('skip_cc', $result);
    $this->assertTrue($result['skip_cc']);
    $this->assertContains('groups', $result);
    $this->assertTrue(is_array($result['groups']));
    foreach (array(2, 3, 4) as $value)
      $this->assertContains($value, $result['groups']);
  }


  function test_readOrderedCommandLineArguments()
  {
    global $argc, $argv;
    $argv[1] = '2016-01-31';
    $argv[2] = 'nein';
    $argv[3] = '25';
    $names = array('today', 'decision', 'count', 'option');

    // test missing required parameter
    $result = readOrderedCommandLineArguments($names, 1, TRUE);
    $this->assertTrue($result === NULL);

    // test optional parameter not provided
    $result = readOrderedCommandLineArguments($names, 1, FALSE);
    $this->assertTrue(is_array($result));
    $this->assertTrue($result['today'] == '2016-01-31');
    $this->assertTrue($result['option'] === NULL);

    // test empty string (valid parameter even without a value)
    $argv[4] = '';
    $result = readOrderedCommandLineArguments($names, 1, TRUE);
    $this->assertTrue(is_array($result));
    $this->assertTrue($result['option'] === '');
  }

}

<?php

require_once 'db.php';
require_once 'lib/portin/functions.php';

/**

  USAGE:
    php lib/portin/functions_test.php validatePortability $MSISDN $TARGETPLAN $ACCOUNT
*/


/**
 * testFunction
 *
 * a generic function for testing other functions from command line
 * read given command line arguments and execute the specified function with parameters
 * @param Integer argc
 * @param Array argv
 * @param String namespace where to look for subject function
 * @return Mixed called function result or NULL on failure
 */
function testFunction($argc, $argv, $namespace)
{
  // first parameter is function name to test
  if (empty($argv[1]))
  {
    echo "ERROR: missing function name to execute\n";
    return NULL;
  }

  $test = "$namespace\\{$argv[1]}";
  if ( ! is_callable($test))
  {
    echo "ERROR: function $test does not exist\n";
    return NULL;
  }

  // validate number of function parameters
  $info = new ReflectionFunction($test);
  $count = $info->getNumberOfParameters();
  if ($count > $argc - 2)
  {
    echo "ERROR: function $test expects $count parameters, " . ($argc - 2) . " is given\n";
    return NULL;
  }

  // collect remaning arguments into parameters array
  for ($i = 2, $params = array(); $i < 2 + $count; $i++)
    $params[] = $argv[$i];

  // execute the subject function
  teldata_change_db();
  return call_user_func_array($test, $params);
}


/**
 * main
 */
$result = testFunction($argc, $argv, NULL);
if (in_array(gettype($result), array('Array', 'Object', 'Resource')))
  echo print_r($result, TRUE) . PHP_EOL;
else
  echo 'type: ' . gettype($result) . ', value: ' . print_r($result, TRUE) . PHP_EOL;

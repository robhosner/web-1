<?php

include_once('classes/Process.php');
include_once('cosid_constants.php');
include_once('db.php');
include_once('db/account_aliases.php');
include_once('db/destination_origin_map.php');
include_once('db/htt_action_log.php');
include_once('db/htt_celluphone_transactions.php');
include_once('db/htt_customers_overlay_ultra.php');
include_once('db/htt_inventory_sim.php');
include_once('db/htt_messaging_queue.php');
include_once('db/htt_transition_log.php');
include_once('db/htt_ultra_locations.php');
include_once('lib/messaging/functions.php');
include_once('lib/state_machine/aspider.php');
include_once('lib/transitions.php');
include_once('lib/util-common.php');
require_once 'Ultra/Lib/Util/Redis.php';

function crone_run($mode)
{
  $ret = array(
    'success' => FALSE,
    'errors'  => array(),
  );

  teldata_change_db();

  $query = 'NO_QUERY_GIVEN';

  switch($mode)
  {
    // I really really hate api_accounts having access to this DB. TODO RIZ, have this be a different user.

  case 'CHECK_AND_ENROLL_PROMO_7_11':
    if (!\Ultra\UltraConfig\promo_7_11_enabled())
      return;

    check_and_enroll_promo_7_11();
    $query = '-- nothing to do';
  break;

  case 'ARCHIVE_MESSAGING_QUEUE':
      $query = archive_messaging_queue_query();
  break;

  case 'ARCHIVE_TRANSITIONS':
      $query = archive_transitions_query();
  break;

  case 'CELLUPHONE_NEWPERIOD':
      $query = celluphone_transaction_newperiod_log_query();
  break;

  case 'CELLUPHONE_PQ':
      $query = celluphone_pq_query();
  break;

  case 'SECOND_MONTH_RECHARGE':
      $query = fill_second_month_recharge_rate_query();
  break;

  case 'UPDATE_DEALER_LOC':      # we invoke perform_htt_ultra_locations_import rather than executing a single query
  case 'MW_QUEUES_HOUSEKEEPING': # we invoke perform_mw_queues_housekeeping     rather than executing a single query
  break;

  case 'CBS_ACT_LOG':
    $query = celluphone_activation_log_query();
  break;

  case 'CBS_TRANS_LOG':
    # Clones Payment Transaction information to Celluphone's Transaction Database.
    $query = celluphone_transaction_log_query();
  break;

  case 'CBS_TRANS_CS_LOG':
    # Clones Payment Transaction information to Celluphone's Transaction Database for Customer Courtesy Events -- see MVNO-1416
    $query = celluphone_transaction_log_customercare_query();
  break;

  case 'CBS_TRANS_QS_LOG':
    # Clones Payment Transaction information to Celluphone's Transaction Database for Qualified Spend Events -- see MVNO-2838
    $query = celluphone_qualified_spend_query();
  break;

case 'recover_actions':
    # We detect actions that are running for too long, and we try to re-run them.
    recover_actions( $mode );
    $query = '--nothing to do';
  break;

  case 'activation_fixups':
    $query = destination_origin_map_fixup_query();
    $query[] = account_aliases_fixup_query();
  break;

  case 'open_actions_reaper':
    $max_age = 5*24*60*60; # 5 days
    $query   = open_actions_reaper( $mode , $max_age );
  break;

  case 'open_transactions_reaper':
    $max_age = 5*24*60*60; # 5 days
    $query   = open_transactions_reaper( $mode , $max_age );
  break;

  case 'reaper_crone':
    $max_age = 2*60*60;
    $query   = pending_actions_reaper( $mode , $max_age );
  break;

  case 'GLOBALSIM_CRONE':
    $query = globally_used_update_query();
  break;

/*
MVNE1 obsolete

  case 'threeci_test':
    $today = date("Y-m-d H:i:s");
    $three_cinteractive_output = three_cinteractive_send_message(
      array(
        'message'      => "Yet another test SMS from Ultra - does 3ci work now? It's $today",
        'msisdn'       => '7143693384',
        'client_tag'   => 'threeci_test'
      )
    );
    dlog('',"three_cinteractive_testcrone: returns $three_cinteractive_output");
    $three_cinteractive_output = three_cinteractive_send_message(
      array(
        'message'      => "Yet another test SMS from Ultra - does 3ci work now? It's $today",
        'msisdn'       => '6262008911',
        'client_tag'   => 'threeci_test'
      )
    );
    dlog('',"three_cinteractive_testcrone: returns $three_cinteractive_output");
    $three_cinteractive_output = three_cinteractive_send_message(
      array(
        'message'      => "Yet another test SMS from Ultra - does 3ci work now? It's $today",
        'msisdn'       => '7143693151',
        'client_tag'   => 'threeci_test'
      )
    );
    dlog('',"three_cinteractive_testcrone: returns $three_cinteractive_output");
    $query = '--nothing to do';
    break;
*/

  default:
    dlog('', "Unknown crone mode $mode");

    return array('success'=>FALSE,'errors'=>array("Unknown crone mode $mode"));
  }

  dlog('sql', "Crone mode $mode => query %s", $query);

  if ($mode == "MW_QUEUES_HOUSEKEEPING")
  {
    $errors = perform_mw_queues_housekeeping();
    dlog('',$errors);
    $ret['success'] = ! ! ( count($errors) == 0 );
  }
  elseif ($mode == "UPDATE_DEALER_LOC")
  {
    $errors = perform_htt_ultra_locations_import();
    dlog('',$errors);
    $ret['success'] = ! ! ( count($errors) == 0 );
  }
  else
  {
    if (is_array($query))
    {
      $ret['success'] = TRUE;
      foreach ($query as $sql)
        $ret['success'] &= run_sql_and_check($sql);
    }
    else
      $ret['success'] = run_sql_and_check($query);
  }

  return $ret;
}

/**
 * check_and_enroll_promo_7_11
 * 
 * check for PROMO 7_11 SIMs added in last 12 hours
 * add BILLING.MRC_7_11 option for returned CUSTOMER_IDs
 */
function check_and_enroll_promo_7_11()
{
  $recently_activated_sims = select_recently_activated_sims(12);

  dlog('', 'PROMO_7_11, recently activated sims: %s', $recently_activated_sims);

  foreach ($recently_activated_sims as $row)
  {
    enroll_promo_7_11($row->ACTIVATED_CUSTOMER_ID, $row->PROMISED_AMOUNT);
    run_sql_and_check(ultra_incomm_inventory_sim_update_sql(array(
      'iccid'           => $row->ICCID,
      'customer_id'     => $row->ACTIVATED_CUSTOMER_ID,
      'redemption_date' => 'now'
    )));
  }
}

/**
 * perform_mw_queues_housekeeping
 *
 * cleanup inbound MW queues from lost messages
 *
 * @return array
 */
function perform_mw_queues_housekeeping()
{
  require_once 'db.php';
  require_once 'Ultra/Lib/MQ/ControlChannel.php';
  require_once 'Ultra/Lib/Util/Redis.php';

  $controlChannel = new \Ultra\Lib\MQ\ControlChannel;
  $redis          = new \Ultra\Lib\Util\Redis();

  cleanup_inbound( $controlChannel->inboundACCMWControlChannel()   , $redis , $controlChannel );
  cleanup_inbound( $controlChannel->inboundUltraMWControlChannel() , $redis , $controlChannel );

  return array();
}

/**
 * cleanup_inbound
 *
 * Attempt to cleanup lost messages in $messageQueue
 *
 * @return NULL
 */
function cleanup_inbound( $messageQueue , $redis , $controlChannel )
{
  $members = $redis->smembers( $messageQueue );

  dlog('',"mq = $messageQueue ; members = %s",$members);

  // loop though channels
  foreach( $members as $channelName )
    cleanup_inbound_message( $channelName , $redis , $controlChannel , $messageQueue );

  return NULL;
}

/**
 * cleanup_inbound_message
 *
 * Attempt to cleanup $channelName if lost
 *
 * @return NULL
 */
function cleanup_inbound_message( $channelName , $redis , $controlChannel , $messageQueue )
{
  $content = $controlChannel->peekControlChannel( $channelName );

  dlog('',"channelName = $channelName , content = $content");

  if ( ! $content )
  {
    dlog('',"deleting $channelName");

    $redis->del( 'CM_' . $channelName );
    $redis->del( 'CS_' . $channelName );
    $redis->srem( $messageQueue , $channelName );
  }

  return NULL;
}

function adjust_assurance_value_by_customer_state( $assurance_value , $msisdn )
{
  $customer_state = get_latest_customer_state_from_transition(
    array(
      'msisdn' => $msisdn
    )
  );

  dlog('',"customer_state is $customer_state ; assurance_value = $assurance_value");

  if ( ( $customer_state == 'Active' ) && ( ( $assurance_value != 'Activate' ) && ( $assurance_value != 'Resume' ) ) )
  {
    $assurance_value = 'Activate';
    dlog('',"assurance_value overridden to $assurance_value");
  }
  elseif ( ( $customer_state == 'Suspended' ) && ( $assurance_value != 'Suspend' ) )
  {
    $assurance_value = 'Suspend';
    dlog('',"assurance_value overridden to $assurance_value");
  }
  elseif ( $customer_state == 'Cancelled' )
  {
    $assurance_value = 'Withdraw';
    dlog('',"assurance_value overridden to $assurance_value");
  }

  return $assurance_value;
}

function recover_actions( $mode )
{
  $hanging_actions = get_hanging_actions( array() );

  if ( is_array($hanging_actions) && count($hanging_actions) )
  {
    foreach ($hanging_actions as $hanging_action)
    {
      recover_hanging_action($hanging_action);
    }
  }
}

function recover_hanging_action($hanging_action)
{
  dlog('',"hanging_action = %s",$hanging_action);

  dlog('', "Action recovery not handled: ".$hanging_action->ACTION_NAME);
}

function get_max_processes_by_crone_name( $crone_name )
{
  $max_processes = 1; // default

  $max_processes_by_crone_name = array();

  if ( isset($max_processes_by_crone_name[ $crone_name ]) )
    $max_processes = $max_processes_by_crone_name[ $crone_name ];

  return $max_processes;
}

// limit crone runs by checking the processes running at the same time on this server
function should_interrupt_crone_execution( $crone_name )
{
  $should_interrupt_crone_execution = FALSE;

  $process = new Process();

  // refresh processes snapshot report
  $process->refresh();

  // get all processes that match $crone_name
  $processes = $process->getProcessesByNameMatch( $crone_name );

  // the list includes two processes for each crone
  $n_processes = intval(count($processes)/2);

  $this_process = '';

  $max_processes = get_max_processes_by_crone_name( $crone_name );

  // there are more $crone_name processes running
  if ( $n_processes > $max_processes )
  {
    dlog('',"There are $n_processes $crone_name processes running");

    // check if we are the older process

    $this_process = $process->getProcessByPID( getmypid() );

    $should_interrupt_crone_execution = TRUE;

    foreach( $processes as $process )
    {
      if ( $this_process['TIME'] != $process['TIME'] )
      {
        $this_process_time = $this_process['TIME'];
        $loop_process_time = $process['TIME'];

        $pattern = '/\D/';

        $this_process_time = preg_replace($pattern, '', $this_process_time);
        $loop_process_time = preg_replace($pattern, '', $loop_process_time);

        // there is an older process, we should not interrupt the execution of this instance
        if ( $this_process_time <= $loop_process_time )
          $should_interrupt_crone_execution = FALSE;
      }
    }
  }

  if ( $should_interrupt_crone_execution )
  {
    // this instance is the older $crone_name process
    dlog('',"Processes : %s",$processes);
    dlog('',"Process ".getmypid()." will terminate (%s)",$this_process);
  }

  return $should_interrupt_crone_execution;
}

/*

Porting completion/crone logic

1) check porting status

2) if it's not final, come back later

3) if it's final and PENDING or COMPLETE (so expected to be successful),
move to Activated

4) if it's final and not PENDING or COMPLETE, move to Port-In Denied

On the gateway side, after we place the port-in request, we only look
for the result of QueryPortIn.  The result can be affected by the
callback of the initial port request (if an R-code is provided or for
other reasons), but we always check the current status and parse the
result.

So both sides, essentially, are idempotent and don't care much about
history, they just check the current port status.  Where history
matters, it's on the gateway side so the PHP code is ``simpler`` and
stateless.

*/

?>

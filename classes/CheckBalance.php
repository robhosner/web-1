<?php

namespace Ultra\Lib;

class CheckBalance
{
  private $mwControl;
  private $defs = [
    'DA1 Voice Base bucket' => ['voice_minutes'],
    'DA2 SMS Base bucket'   => ['sms_messages'],
    'DA7 Voice - Add on'    => ['roaming_balance'],
    'WPRBLK20 Used'         => ['a_data_blk_plan_used',  'a_data_blk_dr_used', 'a_data_mint_3_used'],
    'WPRBLK20 Limit'        => ['a_data_blk_plan_limit', 'a_data_blk_dr_limit', 'a_data_mint_3_limit'],
    '7007 Used'             => ['a_data_blk_used'],
    '7007 Limit'            => ['a_data_blk_limit'],
    'WPRBLK33S Used'        => ['a_data_blk_plan_used',  'a_data_blk1536_plan_used'],
    'WPRBLK33S Limit'       => ['a_data_blk_plan_limit', 'a_data_blk1536_plan_limit'],
    'WPRBLK35S Used'        => ['a_data_blk_plan_used',  'a_data_blk1024_plan_used'],
    'WPRBLK35S Limit'       => ['a_data_blk_plan_limit', 'a_data_blk1024_plan_limit'],
    'WPRBLK34S Used'        => ['a_data_blk_used',  'a_data_blk1536_used'],
    'WPRBLK34S Limit'       => ['a_data_blk_limit', 'a_data_blk1536_limit'],
    'WPRBLK36S Used'        => ['a_data_blk_used',  'a_data_blk1024_used'],
    'WPRBLK36S Limit'       => ['a_data_blk_limit', 'a_data_blk1024_limit'],
    'WPRBLK39S Used'        => ['a_data_blk256_v2_used'],
    'WPRBLK39S Limit'       => ['a_data_blk256_v2_limit'],
    'WHPADJB1 Used'         => ['a_data_pool_used'],
    'WHPADJB1-BAN Used'     => ['a_data_pool_1_used'],
    'WHPADJB1-BAN Limit'    => ['a_data_pool_1_limit'],
    'WPRBLK30 Used'         => ['a_data_blk_used'],
    'WPRBLK30 Limit'        => ['a_data_blk_limit'],
    'WPRBLK25 Used'         => ['a_data_mint_1_used'],
    'WPRBLK25 Limit'        => ['a_data_mint_1_limit'],
  ];

  public function __construct()
  {
    $this->mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
  }

  public function byCustomerId($customerId)
  {
    $customer = get_ultra_customer_from_customer_id($customerId, ['current_mobile_number']);
    return $this->execute(['msisdn' => $customer->current_mobile_number]);
  }

  public function byMSISDN($msisdn)
  {
    return $this->execute(['msisdn' => $msisdn]);
  }

  public function byICCID($iccid)
  {
    return $this->execute(['iccid' => $iccid]);
  }

  private function execute($params)
  {
    if (!isset($params['actionUUID']))
      $params['actionUUID'] = getNewActionUUID('CheckBalance' . time());

    $result = $this->mwControl->mwCheckBalance($params);

    return $result->is_success() ? $this->parseBalanceValueList($result) : null;
  }

  protected function parseBalanceValueList($result)
  {
    $parsed = [];

    foreach ($result->data_array['body']->BalanceValueList->BalanceValue as $balanceValue)
      foreach ($this->defs as $key => $aliases)
        foreach ($aliases as $alias)
          if ($balanceValue->Type . ' ' . $balanceValue->Name == $key)
            if ( ! array_key_exists($alias, $parsed))
              $parsed[$alias] = $balanceValue->Value;
            else
              $parsed[$alias] += $balanceValue->Value;

    return $parsed;
  }
}

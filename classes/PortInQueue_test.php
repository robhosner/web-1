<?php

require_once 'db.php';
require_once 'classes/PortInQueue.php';
require_once 'lib/util-common.php';


# Usage:
#    - php classes/PortInQueue_test.php endAsSuccess         $MSISDN
#    - php classes/PortInQueue_test.php initiate             $CUSTOMER_ID $MSISDN
#    - php classes/PortInQueue_test.php loadByMsisdn         $MSISDN
#    - php classes/PortInQueue_test.php loadByCustomerId     $CUSTOMER_ID
#    - php classes/PortInQueue_test.php updateCarrierInfo    $MSISDN
#    - php classes/PortInQueue_test.php updateOnQueryStatus  $MSISDN
#    - php classes/PortInQueue_test.php updateOnUpdatePortIn $MSISDN
#    - php classes/PortInQueue_test.php endAsCancelled       $MSISDN
#    - php classes/PortInQueue_test.php updateOnCancelPortIn $MSISDN
#    - php classes/PortInQueue_test.php updateFailedToCancel $MSISDN
#    - php classes/PortInQueue_test.php updateOnProvisioningStatus $MSISDN $PROVSTATUS_ERROR_CODE $PROVSTATUS_ERROR_MSG $PROVSTATUS_DESCRIPTION


abstract class AbstractTestStrategy
{
  abstract function test( $argv );
}


class Test_PortInQueue_updateOnProvisioningStatus extends AbstractTestStrategy
{
  function test( $argv )
  {
    $portInQueue = new \PortInQueue();

    $result = $portInQueue->loadByMsisdn( $argv[2] );

    if ( $result->is_failure() )
      die('No PortIn data found for MSISDN '.$argv[2]);

    $updateParams = array(
      'provstatus_error_code'  => $argv[3],
      'provstatus_error_msg'   => $argv[4],
      'provstatus_description' => $argv[5]
    );

    $r = $portInQueue->updateOnProvisioningStatus( $updateParams );

    print_r( $r );
  }
}


class Test_PortInQueue_initiate extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $date = '2014-01-16T15:41:53-08:00';

    $r = $p->initiate(
      array(
        'customer_id' => $argv[2],
        'msisdn'      => $argv[3],
        'old_service_provider' => 'ACME cell',
        'port_request_id' => 'port_request_id',
        'due_date_time'   => date_to_datetime($date, FALSE, TRUE)
      )
    );

    print_r( $r );
  }
}

/*
class Test_PortInQueue_confirmOpen extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->initiate(
      array(
        'customer_id' => $argv[2],
        'msisdn'      => $argv[3]
      )
    );

    print_r( $r );

    $r = $p->confirmOpen(
      array(
        'port_request_id' => 'port_request_id',
        'due_date_time'   => 'Mar 12 2014 06:56:26:927PM'
      )
    );

    print_r( $r );
  }
}
*/

class Test_PortInQueue_updateOnUpdatePortIn extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->loadByMsisdn( $argv[2] );

    print_r( $p );

    print_r( $r );

    if ( $r->is_success() )
    {
      $r = $p->updateOnUpdatePortIn(
        array(
          'account_number'   => '77777',
          'account_password' => '88888',
          'account_zipcode'  => '99999'
        )
      );

      print_r( $r );
    }
  }
}

class Test_PortInQueue_updateFailedToCancel extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->loadByMsisdn( $argv[2] );

    print_r( $p );

    print_r( $r );

    if ( $r->is_success() )
    {
      $r = $p->updateFailedToCancel();

      print_r( $r );
    }
  }
}

class Test_PortInQueue_updateOnCancelPortIn extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->loadByMsisdn( $argv[2] );

    print_r( $p );

    print_r( $r );

    if ( $r->is_success() )
    {
      $r = $p->updateOnCancelPortIn();

      print_r( $r );
    }
  }
}

class Test_PortInQueue_endAsCancelled extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->loadByMsisdn( $argv[2] );

    print_r( $p );

    print_r( $r );

    if ( $r->is_success() )
    {
      $r = $p->endAsCancelled();

      print_r( $r );
    }
  }
}

class Test_PortInQueue_updateCarrierInfo extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->loadByMsisdn( $argv[2] );

    print_r( $p );

    print_r( $r );

    if ( $r->is_success() )
    {
      $r = $p->updateCarrierInfo();

      print_r( $r );
    }
  }
}

class Test_PortInQueue_updateOnQueryStatus extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->loadByMsisdn( $argv[2] );

    print_r( $p );

    print_r( $r );

    if ( $r->is_success() )
    {
      $r = $p->updateOnQueryStatus(
        array(
          'port_status'   => 'queryStatusDataPortStatus', //  <PortStatus> tag from QueryStatusResponse
          'port_error'    => 'queryStatusDataResultMsg',  //   <ResultMsg> tag from QueryStatusResponse
          'due_date_time' => date_to_datetime('2014-01-16T15:41:53-08:00') // <portDueTime> tag from QueryStatusResponse
        )
      );

      print_r( $r );
    }
  }
}

class Test_PortInQueue_endAsSuccess extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->loadByMsisdn( $argv[2] );

    print_r( $p );

    print_r( $r );

    if ( $r->is_success() )
    {
      $r = $p->endAsSuccess();
    }

    print_r( $r );
  }
}

class Test_PortInQueue_loadByMsisdn extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->loadByMsisdn( $argv[2] );

    print_r( $p );

    print_r( $r );
  }
}

class Test_PortInQueue_loadByCustomerId extends AbstractTestStrategy
{
  function test( $argv )
  {
    $p = new \PortInQueue();

    $r = $p->loadByCustomerId( $argv[2] );

    print_r( $p );

    print_r( $r );
  }
}


# perform test #


$testClass = 'Test_PortInQueue_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

// connect to ULTRA_ACC DB
\Ultra\Lib\DB\ultra_acc_connect();

$testObject->test( $argv );


/*
php classes/PortInQueue_test.php updateOnProvisioningStatus 6504549913 8A 'ACCOUNT NUMBER REQUIRED OR INCORRECT' PORT_RESOLUTION_REQUIRED
*/

?>

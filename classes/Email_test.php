<?php

require_once 'db.php';
require_once 'classes/Email.php';
require_once 'test/util.php';


$mail = new Email;

// invalid recipient
/* $error = $mail->initialize('bananas@universe', 'activated', 'UNIVISION', 'ZH', array());
assert( ! empty($error)); */

// invalid template
/* $error = $mail->initialize('vtarasov@ultra.me', 'papaya', 'UNIVISION', 'ZH', array());
assert( ! empty($error));*/

// unknown tempate paramters
/* $params = array
(
  'firstname'      => 'Justin',
  'lastname'       => 'Taylor');
$error = $mail->initialize('vtarasov@ultra.me', 'activated', 'ULTRA', 'ZH', $params);
assert( ! empty($error)); */


// valid template
$params = array
(
  'first_name'      => 'Justin',
  'last_name'       => 'Taylor',
  'msisdn'          => '5555555555',
  'plan_name'       => 'Ultra $19',
  'expiration_date' => '3/20/2016'
);
$error = $mail->initialize('vtarasov@ultra.me', 'activated', 'ULTRA', 'ZH', $params);
assert(empty($error));

// valid message
$error = $mail->queue();
assert(empty($error));

// process
$handler = new Email;
$id = $handler->next();
assert( ! empty($id));
$error = $handler->process($id);
assert(empty($error));


// mandrill study
/*
  $mandrill = new Mandrill('R_KD36K1hK8LJ-vqTXrxJA');
  $id = '204ad565e0d34f8d85049d0e554172f6';
  $id = 'ff66e0d38dc24ecb8c9cc1b113f7f456';
  $result = $mandrill->messages->info($id);
  $to = 'vtarasov@ultra.mes';
  $result = $mandrill->messages->listScheduled();
  $result = $mandrill->messages->search(NULL, '2016-02-01', '2016-02-21');
  print_r($result);
*/

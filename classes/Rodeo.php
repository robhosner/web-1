<?php

// set_include_path(get_include_path() . ':/home/ht/www/james_dev/web/');
require_once 'db.php';

class Rodeo
{
  protected $varTypes = [
    'to_plan',
    'to_plan_short',
    'from_plan',
    'from_plan_short',
    'to_cost',
    'diff_cost'
  ];

  public $json;
  public $map;
  public $transition; // current transition

  public $output;

  function setMap($map)
  {
    $this->map = $map;
  }

  /*
   * "all":         [short, short, short],
   * "provision":   [short, short, short],
   * "dealer_demo": [short]
   *
   */
  function setPlanGroups(array $plans)
  {
    $this->json->plan_groups = (object)$plans;
  }

  function loadFile($file)
  {
    $this->json = json_decode(file_get_contents($file));
  }

  function getJSON()
  {
    return $this->output;
  }

  function getRequirements($transition)
  {
    // organize and quote requirements
    $requirements = ['metro' => [], 'local' => []];

    foreach ($transition->requirements as $requirement)
    {
      $found = FALSE;

      foreach ($this->varTypes as $varType)
      {
        // check if requirement has variables
        if (strpos($requirement, "[$varType]") !== FALSE)
        {
          $requirements['local'][] = "\"$requirement\"";
          $found = TRUE;
          break;
        }
      }

      if ( ! $found)
        $requirements['metro'][] = "\"$requirement\"";
    }

    return $requirements;
  }

  function getActions($transition)
  {
    // quote actions
    $actions = [];
    foreach ($transition->actions as &$action)
      $actions[] = "\"$action\"";

    return $actions;
  }

  function getTransitionLabels($transition, $requirements)
  {
    $labels = [];

    switch ($transition->connection)
    {
      case 'speaker':    $labels = $this->typeSpeaker   ($transition, $requirements); break;
      case 'rodeo':      $labels = $this->typeRodeo     ($transition, $requirements); break;
      case 'rodeo|high': $labels = $this->typeRodeoHigh ($transition, $requirements); break;
      case 'self':       $labels = $this->typeSelf      ($transition, $requirements); break;
      case 'tramp':      $labels = $this->typeTramp     ($transition, $requirements); break;
      case 'one':        $labels = $this->typeOne       ($transition, $requirements); break;
    }

    return $labels;
  }

  function parseFile()
  {
    $this->output = "";

    $size = count($this->json->transitions);
    $i = 0;

    foreach ($this->json->transitions as &$transition)
    {
      $i++;

      $this->transition   = $transition;

      $requirements = $this->getRequirements($transition);
      $actions      = $this->getActions($transition);

      $transitionLabels  = $this->getTransitionLabels($transition, $requirements);
      $transitionLabels  = implode(",\n", $transitionLabels);
      $metroRequirements = implode(",\n        ", $requirements['metro']);
      $actions           = implode(",\n        ", $actions);

      $this->output .= "
    {
      \"from_plan_state\": \"{$transition->from_state}\",
      \"to_plan_state\":   \"{$transition->to_state}\",
      \"priority\":        \"{$transition->priority}\",
      \"transition_labels\": [
        $transitionLabels
      ],
      \"actions\": [
        $actions
      ],
      \"requirements\": [
        $metroRequirements
      ]
    }
      ";

      if ($i < $size)
        $this->output .= ',';
    }

    $states = json_encode($this->json->states);
    $this->output = "
{
  \"states\": {$states},
  \"transitions\": [{$this->output}]
}
    ";

  }

  function variables($string, $from_plan, $to_plan)
  {
    try
    {
      $to_plan_short   = get_short_name_from_plan_name($to_plan);
      $from_plan_short = get_short_name_from_plan_name($from_plan);

      $new_cost  = get_plan_cost_from_cos_id(get_cos_id_from_plan($to_plan)) / 100;
      $from_cost = get_plan_cost_from_cos_id(get_cos_id_from_plan($from_plan)) / 100;

      // TODO move to map
      $string = str_replace('[to_plan]',         $to_plan, $string);
      $string = str_replace('[to_plan_short]',   $to_plan_short, $string);
      $string = str_replace('[from_plan]',       $from_plan, $string);
      $string = str_replace('[from_plan_short]', $from_plan_short, $string);
      $string = str_replace('[to_cost]',         $new_cost, $string);
      $string = str_replace('[diff_cost]',       ($new_cost - $from_cost), $string);
    }
    catch (Exception $e)
    {
      echo $e->getMessage(); die;
    }

    return $string;
  }

  function TypeSpeaker($transition, $requirements)
  {
    $labels = [];

    foreach ($this->map as $to_plan => $to_values)
    {
      if (!in_array($this->map[$to_plan][1], $this->json->plan_groups->{$transition->allowed}))
        continue;

      $label = $this->variables($transition->label, $transition->from_plan, $to_plan);

      $localRequirements = [];
      foreach ($requirements['local'] as $requirement)
        $localRequirements[] = $this->variables($requirement, $transition->from_plan, $to_plan);
      $localRequirements = implode(",\n      ", $localRequirements);

      $labels[] = "
        {
          \"from_plan\": \"{$transition->from_plan}\",
          \"to_plan\":   \"$to_plan\",
          \"transition_label\": \"$label\",
          \"requirements\": [
            $localRequirements
          ]
        }";
    }

    return $labels;
  }

  function typeSelf($transition, $requirements)
  {
    $labels = [];

    foreach ($this->map as $from_plan => $from_values)
    {
      // check if from_plan allowed
      if (!in_array($this->map[$from_plan][1], $this->json->plan_groups->{$transition->allowed}))
        continue;

      $label = $this->variables($transition->label, $from_plan, $from_plan);

      $localRequirements = [];
      foreach ($requirements['local'] as $requirement)
        $localRequirements[] = $this->variables($requirement, $from_plan, $from_plan);
      $localRequirements = implode(",\n      ", $localRequirements);

      $labels[] = "
        {
          \"from_plan\": \"$from_plan\",
          \"to_plan\":   \"$from_plan\",
          \"transition_label\": \"$label\",
          \"requirements\": [
            $localRequirements
          ]
        }";
    }

    return $labels;
  }

  function typeOne($transition, $requirements)
  {
    $label = $this->variables($transition->label, $transition->from_plan, $transition->from_plan);

    $localRequirements = [];
    foreach ($requirements['local'] as $requirement)
      $localRequirements[] = $this->variables($requirement, $transition->from_plan, $transition->from_plan);
    $localRequirements = implode(",\n      ", $localRequirements);

    $label = "
        {
          \"from_plan\": \"{$transition->from_plan}\",
          \"to_plan\":   \"{$transition->from_plan}\",
          \"transition_label\": \"$label\",
          \"requirements\": [
            $localRequirements
          ]
        }";

    return [$label];
  }

  function typeRodeoHigh($transition, $requirements)
  {
    $labels = [];

    foreach ($this->map as $from_plan => $from_values)
    {
      // check if from_plan is allowed
      if (!in_array($this->map[$from_plan][1], $this->json->plan_groups->{$transition->allowed_from}))
        continue;

      foreach ($this->map as $to_plan => $to_values)
      {
        // check if to_plan is allowed
        if (!in_array($this->map[$to_plan][1], $this->json->plan_groups->{$transition->allowed_to}))
          continue;

        // check if from_plan is not to_plan 
        // or from_cost > to_cost
        if ($from_plan == $to_plan || $from_values[0] > $to_values[0])
          continue;

        $label = $this->variables($transition->label, $from_plan, $to_plan);

        $localRequirements = [];
        foreach ($requirements['local'] as $requirement)
          $localRequirements[] = $this->variables($requirement, $from_plan, $to_plan);
        $localRequirements = implode(",\n      ", $localRequirements);

        $labels[] = "
        {
          \"from_plan\": \"$from_plan\",
          \"to_plan\":   \"$to_plan\",
          \"transition_label\": \"$label\",
          \"requirements\": [
            $localRequirements
          ]
        }";
      }
    }

    return $labels;
  }

  function typeRodeo($transition, $requirements)
  {
    $labels = [];

    foreach ($this->map as $from_plan => $from_values)
    {
      // check if from plan is allowed
      if (!in_array($this->map[$from_plan][1], $this->json->plan_groups->{$transition->allowed_from}))
        continue;

      foreach ($this->map as $to_plan => $to_values)
      {
        // check if to_plan is allowed
        if (!in_array($this->map[$to_plan][1], $this->json->plan_groups->{$transition->allowed_to}))
          continue;

        // skip if same plan
        if ($from_plan == $to_plan)
          continue;

        $label = $this->variables($transition->label, $from_plan, $to_plan);

        $localRequirements = [];
        foreach ($requirements['local'] as $requirement)
          $localRequirements[] = $this->variables($requirement, $from_plan, $to_plan);
        $localRequirements = implode(",\n      ", $localRequirements);

        $labels[] = "
        {
          \"from_plan\": \"$from_plan\",
          \"to_plan\":   \"$to_plan\",
          \"transition_label\": \"$label\",
          \"requirements\": [
            $localRequirements
          ]
        }";
      }
    }

    return $labels;
  }

  function typeTramp($transition, $requirements)
  {
    $labels = [];

    foreach ($this->map as $from_plan => $from_values)
    {
      // check if from_plan is in allowed plans
      if (!in_array($this->map[$from_plan][1], $this->json->plan_groups->{$transition->allowed}))
        continue;

      $label = $this->variables($transition->label, $from_plan, $transition->to_plan);

      $localRequirements = [];
      foreach ($requirements['local'] as $requirement)
        $localRequirements[] = $this->variables($requirement, $from_plan, $transition->to_plan);
      $localRequirements = implode(",\n      ", $localRequirements);

      $labels[] = "
        {
          \"from_plan\":        \"$from_plan\",
          \"to_plan\":          \"{$transition->to_plan}\",
          \"transition_label\": \"$label\",
          \"requirements\": [
            $localRequirements
          ]
        }";
    }

    return $labels;
  }
}

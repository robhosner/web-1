<?php

require_once 'classes/PHPUnitBase.php';
require_once 'db.php';
require_once 'classes/CustomerOptions.php';

class CustomerOptionsTest extends PHPUnitBase
{
  public function __construct()
  {
    teldata_change_db();

    $this->sql = 'SELECT TOP 1 CUSTOMER_ID FROM ULTRA.CUSTOMER_OPTIONS WHERE OPTION_ATTRIBUTE = \'%s\' ORDER BY CUSTOMER_ID DESC';
  }

  public function test__hasOption()
  {
    $res = find_first(sprintf($this->sql, BILLING_OPTION_ATTRIBUTE_BOGO_MONTH));
    $this->bogoMonthInfoCustomer = $res->CUSTOMER_ID;

    $customerOptions = new \CustomerOptions($this->bogoMonthInfoCustomer);

    echo PHP_EOL;

    $hasOption = $customerOptions->hasOption(BILLING_OPTION_ATTRIBUTE_BOGO_MONTH);
    $this->assertTrue($hasOption);

    $hasOption = $customerOptions->hasOption(array(BILLING_OPTION_ATTRIBUTE_BOGO_MONTH));
    $this->assertTrue($hasOption);

    $hasOption = $customerOptions->hasOption('GODZILLA');
    $this->assertFalse($hasOption);
  }

  public function test__bogoMonthInfo()
  {
    $res = find_first(sprintf($this->sql, BILLING_OPTION_ATTRIBUTE_BOGO_MONTH));
    $this->bogoMonthInfoCustomer = $res->CUSTOMER_ID;

    $customerOptions = new \CustomerOptions($this->bogoMonthInfoCustomer);
    $info = $customerOptions->bogoMonthInfo();

    print_r($info);
  }

  public function test__promoPlanInfo()
  {
    $res = find_first(sprintf($this->sql, BILLING_OPTION_PROMO_PLAN));
    $this->promoPlanInfoCustomer = $res->CUSTOMER_ID;

    $customerOptions = new \CustomerOptions($this->promoPlanInfoCustomer);
    $info = $customerOptions->promoPlanInfo();

    print_r($info);
  }

  public function test__hasPromo711()
  {
    $res = find_first(sprintf($this->sql, BILLING_OPTION_ATTRIBUTE_7_11));
    $this->hasPromo711Customer = $res->CUSTOMER_ID;

    $customerOptions = new \CustomerOptions($this->hasPromo711Customer);
    $has = $customerOptions->hasPromo711();

    echo "\n\n";
    echo ($has) ? 'has promo 711' : 'does not have promo 711';
    echo "\n\n";
  }

  public function test__hasMultiMonthInfo()
  {
    $res = find_first(sprintf($this->sql, BILLING_OPTION_MULTI_MONTH));
    $this->multiMonthInfoCustomer = $res->CUSTOMER_ID;

    $customerOptions = new \CustomerOptions($this->multiMonthInfoCustomer);
    $info = $customerOptions->multiMonthInfo();

    print_r($info);
  }

  public function test__getBrandUVSubplan()
  {
    $res = find_first(sprintf($this->sql, 'BRAND.UV.SUBPLAN.A'));
    $this->getBrandUVSubplanCustomer = $res->CUSTOMER_ID;

    $customerOptions = new \CustomerOptions($this->getBrandUVSubplanCustomer);
    $subplan = $customerOptions->getBrandUvSubplan();

    echo "\nSUBPLAN: $subplan\n";
  }
}

<?php

namespace Ultra\Lib;

require_once 'db.php';

class MessagingEvent
{
  protected $customerId;
  protected $currentMobileNumber;
  protected $brandId;
  protected $label;
  protected $def;
  protected $params;

  /**
   * @param  integer
   * @param  string
   * @param  array
   * @return null
   */
  public function __construct(
    $customerId,
    $label,
    $params = [],
    $currentMobileNumber = null,
    $preferredLanguage = null,
    $brandId = null
  )
  {
    $params['customerId'] = $customerId;

    $defs = new MessagingEventDefinitions();

    if ( ! $brandId || ! $currentMobileNumber)
    {
      $customer = get_ultra_customer_from_customer_id($customerId, [
        'BRAND_ID',
        'current_mobile_number',
        'preferred_language'
      ]);

      if ( ! $customer)
      {
        // handle error
      }

      $brandId = $customer->BRAND_ID;
      $currentMobileNumber = $customer->current_mobile_number;
      $preferredLanguage = $customer->preferred_language;
    }

    $this->brandId = $brandId;
    $params['brandId'] = $brandId;

    $this->preferredLanguage = $preferredLanguage;
    $params['preferredLanguage'] = $preferredLanguage;

    $this->currentMobileNumber = $currentMobileNumber;
    $params['currentMobileNumber'] = $currentMobileNumber;

    $this->customerId = $customerId;
    $this->label = $label;
    $this->params = new MessagingEventParams($params);
    $this->def = $defs->get($label);

    foreach ($this->def->params as $param)
      $this->params->queue($param);

    $this->params->fetchQueue();
  }

  /**
   * returns aggregated info about MessagingEvent
   * @return Array
   */
  public function get()
  {
    return [
      'customerId' => $this->customerId,
      'label'      => $this->label,
      'templates'  => $this->def->templates,
      'params'     => $this->params->get()
    ];
  }

  /**
   * enqueues event templates to be sent
   * @return null
   */
  public function enqueue()
  {
    // foreach ($this->def->templates as $template)
    //   enqueue_daytime_sms($this->customerId, $template, $this->params->get);
  }

  /**
   * immediately send event templates
   */
  public function send()
  {
    foreach ($this->def->templates as $template)
    {
      $params = array_merge($this->params->get(), ['message_type' => $template]);
      $message = \Ultra\Messaging\Templates\SMS_by_language(
        $params,
        $this->preferredLanguage,
        $this->brandId
      );

      print_r($message);

      // enqueue_immediate_sms
    }
  }

  /**
   * get all built messages
   * @return array
   */
  public function getMessages()
  {
    $messages = [];

    foreach ($this->def->templates as $template)
    {
      $params = array_merge($this->params->get(), ['message_type' => $template]);
      $message = \Ultra\Messaging\Templates\SMS_by_language(
        $params,
        $this->preferredLanguage,
        $this->brandId
      );

      $messages[] = $message;
    }

    return $messages;
  }

  /**
   * immediately send to customer not on our network
   */
  public function sendExternal()
  {
    //
  }
}

//
class MessagingEventDefinitions
{
  public function __construct()
  {
    $file = file_get_contents('classes/MessagingEvent/definitions.json');
    $defs = json_decode($file);

    $this->defs = $defs;
  }

  /**
   * @param  string
   * @return object
   */
  public function get($event)
  {
    return (empty($this->defs->$event)) ? null : $this->defs->$event;
  }
}

class MessagingEventParams
{
  // params fetch is currently working with
  public $working = [];

  public $data = [];
  public $queue  = [];

  /**
   * @param  array
   * @return null
   */
  public function __construct($params)
  {
    foreach ($params as $key => $val)
      $this->set($key, $val);

    $file = file_get_contents('classes/MessagingEvent/map.json');
    $map  = json_decode($file);

    $this->map = $map;
  }

  /**
   * @return array
   */
  public function get()
  {
    return $this->data;
  }

  /**
   * @param  string
   * @param  mixed
   * @return null
   */
  public function set($name, $val)
  {
    $this->data[$name] = $val;
  }

  /**
   * @param  array
   * @return null
   */
  public function merge($with)
  {
    $this->data = array_merge($this->data, $with);
  }

  /**
   * @param  string
   * @return bool
   */
  public function working($needle)
  {
    return in_array($needle, $this->working);
  }

  /**
   * @param  string
   * @return null
   */
  public function queue($name)
  {
    $this->queue[] = $name;
  }

  /**
   * run fetch commands
   * @return null
   */
  public function fetchQueue()
  {
    $pending = [];
    foreach ($this->queue as $param)
      foreach ($this->map as $source => $params)
        if (in_array($param, $params))
          $pending[$source][] = $param;

    foreach ($pending as $source => $params)
    {
      $source = ucfirst($source);
      $this->working = $params;
      $this->{"fetch$source"}();
    }

    unset($this->data['customerId']);
    unset($this->data['currentMobileNumber']);
    unset($this->data['preferredLanguage']);
    unset($this->data['brandId']);
  }

  /**
   * fetch customer using get_ultra_customer_from_customer_id
   * @param  array
   * @return null
   */
  public function fetchCustomer()
  {
    \teldata_change_db();

    $cols = [];
    foreach ($this->working as $param)
    {
      if ($param == 'msisdn')
      {
        $cols[] = 'current_mobile_number as msisdn';
        continue;
      }

      $cols[] = $param;
    }

    $customer = get_ultra_customer_from_customer_id($this->data['customerId'], $cols);

    if (isset($customer->plan_expires))
      $customer->plan_expires = date('M d', strtotime($customer->plan_expires));

    $this->merge((array)$customer);
  }

    /**
   * fetch customer info using CheckBalance class
   * @param  array
   * @return null
   */
  public function fetchCheckBalance()
  {
    $checkBalance = new \Ultra\Lib\CheckBalance();
    $cbResult = $checkBalance->byMSISDN($this->data['currentMobileNumber']);

    \logDebug(sprintf('CheckBalance result: %s', json_encode($cbResult)));

    // if ( ! $cbResult)
    //  throw new Exception('ERR_API_INTERNAL: error receiving network information');

    if ($this->working('roaming_balance'))
    {
      $roamingBalance = ($cbResult && isset($cbResult['roaming_balance']))
        ? $cbResult['roaming_balance'] / 100
        : 0;

      $this->set('roaming_balance', $roamingBalance);
    }
  }

  /**
   * fetch customer using get_account_from_customer_id
   * @param  array
   * @return null
   */
  public function fetchAccount()
  {
    \teldata_change_db();

    $cols = [];
    foreach ($this->working as $param)
    {
      $cols[] = $param;
    }

    $customer = get_account_from_customer_id($this->data['customerId'], $cols);
    $this->data = array_merge($this->data, (array)$customer);
  }

  /**
   * get info regarding flex
   * @param  array
   * @return null
   */
  public function fetchFlexInfo()
  {
    $info = \Ultra\Lib\Flex::getSharedIld($this->data['customerId']);

    $this->data['flex_plan_credits'] = $this->working('flex_plan_credits') ? $info->data_array['planCredits'] : 0;
    $this->data['flex_intl_credits'] = $this->working('flex_intl_credits') ? $info->data_array['sharedIntl'] / 60000 : 0;
  }

  /**
   * fetch balance data using mvneGet4gLTE
   * @param  array
   * @return null
   */
  public function fetchMvneGet4gLTE()
  {
    $actionUUID = getNewActionUUID('mvne ' . time());

    list(
      $remaining,
      $usage,
      $mintAddOnRemaining,
      $mintAddOnUsage,
      $breakDown,
      $mvneError,
      $threeGBreakDown
    ) = mvneGet4gLTE($actionUUID, $this->data['currentMobileNumber'], $this->data['customerId']);

    //
    $b = &$breakDown;

    // flex personal usage
    if ($this->working('flex_usage'))
    {
      $usage = isset($b['WHPADJB1']['WHPADJB1 Used'])
        ? $b['WHPADJB1']['WHPADJB1 Used']
        : 0;

      $this->set('flex_usage', beautify_4g_lte_string($usage, true));
    }

    // flex family usage
    if ($this->working('flex_family_usage'))
    {
      $familyUsage = isset($b['WHPADJB1-BAN']['WHPADJB1-BAN Used'])
        ? $b['WHPADJB1-BAN']['WHPADJB1-BAN Used']
        : 0;

      $this->set('flex_family_usage', beautify_4g_lte_string($familyUsage, true));
    }

    // flex remaining
    if ($this->working('flex_remaining'))
    {
      $remaining = isset($b['WHPADJB1-BAN']['WHPADJB1-BAN Limit'])
        ? $b['WHPADJB1-BAN']['WHPADJB1-BAN Limit'] - $b['WHPADJB1-BAN']['WHPADJB1-BAN Used']
        : 0;

      $this->set('flex_remaining', beautify_4g_lte_string($remaining, false));
    }

    // flex limit
    if ($this->working('flex_limit'))
    {
      $limit = isset($b['WHPADJB1-BAN']['WHPADJB1-BAN Limit'])
        ? $b['WHPADJB1-BAN']['WHPADJB1-BAN Limit']
        : 0;

      $this->set('flex_limit', beautify_4g_lte_string($limit, false));
    }
  }
}

// teldata_change_db();
// $smsEvent = new \Ultra\Lib\MessagingEvent($argv[1], $argv[2], []);
// print_r($smsEvent->get());
// $smsEvent->send();

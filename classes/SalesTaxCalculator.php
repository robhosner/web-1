<?php

/**
 * $taxCalc = new \SalesTaxCalculator();
  *
 * $taxCalc->setBillingInfoByCustomerId($customer_id);
 * $taxCalc->setAccount($account);    (optional)
 * $taxCalc->setBillingZipCode($zip); (optional)
 * $taxCalc->setAmount($amount * 100);
 *
 * $transaction_type  = 'RECHARGE';
 * $result_taxes_fees = $taxCalc->calculateTaxesFees($transaction_type);
 */

require_once 'Ultra/Lib/SureTax.php';
require_once 'Ultra/Configuration/Configuration.php';
require_once 'Ultra/Accounts/Interfaces/AccountsRepository.php';
require_once 'Ultra/Accounts/Repositories/Mssql/AccountsRepository.php';
require_once 'Ultra/Customers/Interfaces/CustomerRepository.php';
require_once 'Ultra/Customers/Repositories/Mssql/CustomerRepository.php';
require_once 'Ultra/Payments/TaxCalculator/SureTax.php';

use Ultra\Configuration\Configuration;
use Ultra\Customers\Repositories\Mssql\CustomerRepository;
use Ultra\Payments\TaxCalculator\SureTax;
use Ultra\Accounts\Repositories\Mssql\AccountsRepository;

class SalesTaxCalculator
{
  const TRANSACTION_TYPE_CODE_RECHARGE = '500000';
  const TRANSACTION_TYPE_CODE_MINT     = '510000';
  const TRANSACTION_TYPE_CODE_DEFAULT  = '040105';

  public $customer_id;

  public $account;
  public $billingZipCode;
  public $amount = 0;
  public $brandId = 1;
  public static $cosId;
  public $activationZipCode;
  public $retryCount = 0;

  /**
   * @var Configuration
   */
  private static $configuration;

  /**
   * @var SureTax
   */
  private $sureTax;

  /**
   * @var AccountsRepository
   */
  private $accountsRepository;

  /**
   * @var CustomerRepository
   */
  private $customerRepository;

  /**
   * SalesTaxCalculator constructor.
   */
  public function __construct()
  {
    self::getConfiguration();
  }


  /**
   * Sets billingZipCode and accout from $customer_id information
   * @param  Int $customer_id
   * @return Array of member values set {'customer_id': int, 'account': string, 'billingZipCode': string}
   */
  public function setBillingInfoByCustomerId($customer_id)
  {
    if (!$customer_id) {
      return;
    }

    $overlay  = $this->getCustomerRepository()->getCustomerById($customer_id, ['BRAND_ID'], true);
    $account  = $this->getAccountRepository()->getAccountFromCustomerId($customer_id, ['ACCOUNT']);
    $customer = $this->getCustomerRepository()->getCustomerFromCustomersTable($customer_id, ['CC_POSTAL_CODE','POSTAL_CODE']);

    $this->customer_id = $customer_id;

    if ($account) {
      $this->account = $account->account;
    }

    if ($customer) {
      if ($customer->cc_postal_code) $this->billingZipCode = $customer->cc_postal_code;
      elseif ($customer->postal_code) $this->billingZipCode = $customer->postal_code;

      $this->activationZipCode = $customer->postal_code;
    }

    $this->brandId = $overlay->brand_id;

    return [
      'customer_id'    => $this->customer_id,
      'account'        => $this->account,
      'billingZipCode' => $this->billingZipCode
    ];
  }

  /**
   * sets account to $account if not NULL
   * @param  int $account
   * @return NULL
   */
  public function setAccount($account)
  {
    if ($account)
      $this->account = $account;
  }

  /**
   * sets amount to $amount if not NULL
   * amount in cents
   * @param  int $amount
   * @return NULL
   */
  public function setAmount($amount)
  {
    if (is_numeric($amount))
      $this->amount = $amount;
  }

  /**
   * Adds (addition) amount to member amount
   * amount in cents
   * @param  int $amount
   * @return NULL
   */
  public function addAmount($amount)
  {
    $this->amount += $amount;
  }

  /**
   * sets billingZipCode to $zipCode if not NULL
   * @param  String $zipCode
   * @return NULL
   */
  public function setBillingZipCode($zipCode)
  {
    if ($zipCode)
      $this->billingZipCode = $zipCode;
  }

  /**
   * if zip code between 90001 and 96162, return TRUE
   * @param  String $zipCode
   * @return Boolean
   */
  public static function isCaliforniaZip($zipCode)
  {
    $zipCode = (int)substr("$zipCode", 0, 5);
    return ($zipCode > 90001 && $zipCode < 96162) ? TRUE : FALSE;
  }

  /**
   * returns result object with data_array of taxes and fees
   * @param  $transactionType
   * @return Result object
   */
  public function calculateTaxesFees($transactionType = NULL)
  {
    // default result array
    $result_data = array(
      'zip_code'             => '00000',

      'mts_tax'              => 0,
      'sales_tax'            => 0,
      'sales_tax_rule'       => 'SureTax API error',
      'sales_tax_percentage' => 0,

      'recovery_fee'       => 0,
      'recovery_fee_rule'  => 0,
      'recovery_fee_basis' => 0
    );

    // selects parsing rule and transaction type code based on transaction type
    $parsingRule         = NULL;
    $salesTaxRule        = NULL;
    $transactionTypeCode = NULL;

    $recoveryFeeMultiplier = 1;
    if (strpos($transactionType, 'FLEX_RECHARGE_') == 0)
    {
      $meta = explode('_', $transactionType);
      if (isset($meta[2]) && $meta[2] > 0)
        $recoveryFeeMultiplier = $meta[2];

      $transactionType = 'RECHARGE';
    }

    $cosId = self::$cosId;

    // use cos_id of MONTHLY_RENEWAL_TARGET
    if ($transactionType == 'RECHARGE')
    {
      $result = $this->getCustomerRepository()->getCustomerById($this->customer_id, ['MONTHLY_RENEWAL_TARGET']);
      if (!empty($result->MONTHLY_RENEWAL_TARGET))
      {
        $cosId = get_cos_id_from_plan($result->MONTHLY_RENEWAL_TARGET);
      }
    }

    if (empty($cosId))
    {
      $result = $this->getAccountRepository()->getAccountFromCustomerId($this->customer_id, ['COS_ID']);
      $cosId = $result->cos_id;
    }

    $fee = $this->getRecoveryFee($this->amount, $cosId);

    // get recovery fee and et our result data array

    $result_data['recovery_fee']       = $fee['recovery_fee'] * $recoveryFeeMultiplier;
    $result_data['recovery_fee_basis'] = $fee['recovery_basis'];
    $result_data['recovery_fee_rule']  = $fee['recovery_rule'];

    switch ($transactionType)
    {
      case 'RECHARGE':
        $parsingRule         = 'Jan2017';
        $transactionTypeCode = ($this->brandId == 3)
          ? self::TRANSACTION_TYPE_CODE_MINT
          : self::TRANSACTION_TYPE_CODE_RECHARGE;
        $salesTaxRule = "SureTax Rule $transactionTypeCode";
        break;
      // default rule and code
      default:
        $parsingRule         = 'Jan2017';
        $transactionTypeCode = self::TRANSACTION_TYPE_CODE_DEFAULT;
        $salesTaxRule        = 'SureTax Rule v1';
        break;
    }

    // be sure parsing method exists
    if ( ! method_exists($this, "parseTaxResult" . $parsingRule))
      return make_error_Result('Invalid suretax parsing rule');

    $this->amount += $result_data['recovery_fee'];

    // validate input information, call SureTax
    $salesTax = $this->getSalesTax($transactionTypeCode);

    // gracefully handle SureTax error
    if (count($errors = $salesTax->get_errors()))
      return make_ok_Result($result_data, NULL, $errors[0]);

    // if suretax returned 0 tax, exit with default result
    if ( $salesTax->data_array['TotalTax'] == "0.00" )
      return make_ok_Result($result_data, NULL, 'SureTax API returned 0 taxes');

    $result_data['zip_code'] = $salesTax->data_array['zip_code'];

    // this is our sales tax parsed by our parsing rule
    $parsedSalesTax = $this->{"parseTaxResult" . $parsingRule}($salesTax);
    $parsedSalesTax['sales_tax_rule'] = $salesTaxRule;

    $result_data['mts_tax']              = $parsedSalesTax['mts_tax'];
    $result_data['sales_tax']            = $parsedSalesTax['sales_tax'];
    $result_data['sales_tax_percentage'] = $parsedSalesTax['sales_tax_percentage'];
    $result_data['sales_tax_rule']       = $parsedSalesTax['sales_tax_rule'];

    \logit(sprintf("calculateTaxesFees mts_tax      = %s",$result_data['mts_tax']));
    \logit(sprintf("calculateTaxesFees sales_tax    = %s",$result_data['sales_tax']));
    \logit(sprintf("calculateTaxesFees recovery_fee = %s",$result_data['recovery_fee'])); # in cents

    // everything was OK
    return make_ok_Result($result_data);
  }

/**
   * suretax parsing for January 2017
   * in this rule, we are calculating each GroupList[i] with no TaxTypeCode filer
   * @param  Result of calculateSalesTax
   * @return Array {'sales_tax': int, 'sales_tax_percentage': float, 'sales_tax_rule': string}
   */
  public function parseTaxResultJan2017($salesTax)
  {
    $result = array(
      'mts_tax'              => 0,
      'sales_tax'            => 0,
      'sales_tax_percentage' => 0,
      // 'sales_tax_rule'       => 'SureTax Rule 500000'
    );

    $isCaliforniaZip = $this->isCaliforniaZip($this->billingZipCode);

    foreach ($salesTax->data_array['GroupList'] as $groupList)
      foreach ($groupList->TaxList as $taxEntry)
      {
        // if is MTS tax
        if ($isCaliforniaZip)
          $result['mts_tax']   += ( $taxEntry->TaxAmount * 100 );
        else
          $result['sales_tax'] += ( $taxEntry->TaxAmount * 100 );
      }

    $result['mts_tax']   = floor($result['mts_tax']);
    $result['sales_tax'] = floor($result['sales_tax']);

    $result['sales_tax_percentage'] = ($result['sales_tax'] + $result['mts_tax']) / $this->amount * 100;

    return $result;
  }

  /**
   * default suretax parsing
   * in this rule, we are parsing GroupList[0] with a TaxTypeCode filter
   * this was the original implementation before the MTS update
   * @param  Result of calculateSalesTax
   * @return Array {'sales_tax': int, 'sales_tax_percentage': float, 'sales_tax_rule': string}
   */
  public function parseTaxResultDefault($salesTax)
  {
    $result = array(
      'mts_tax'              => 0,
      'sales_tax'            => 0,
      'sales_tax_percentage' => 0,
      'sales_tax_rule'       => 'SureTax Rule v1'
    );

    foreach ( $salesTax->data_array['GroupList'][0]->TaxList as $taxEntry )
    {
      $suffix = substr($taxEntry->TaxTypeCode , -2);

      if ( in_array( $suffix , array('01','02','03','04','05') ) )
      {
        $result['sales_tax']            += ( $taxEntry->TaxAmount * 100 );
        $result['sales_tax_percentage'] += ( $taxEntry->TaxRate * $taxEntry->PercentTaxable );
      }
    }

    $result['sales_tax'] = floor($result['sales_tax']);

    return $result;
  }

  /**
   * suretax parsing after MTS update
   * in this rule, we are calculating each GroupList[i] with no TaxTypeCode filer
   * @param  Result of calculateSalesTax
   * @return Array {'sales_tax': int, 'sales_tax_percentage': float, 'sales_tax_rule': string}
   */
  public function parseTaxResultMTSUpdate($salesTax)
  {
    $result = array(
      'mts_tax'              => 0,
      'sales_tax'            => 0,
      'sales_tax_percentage' => 0,
      'sales_tax_rule'       => 'SureTax Rule 500000'
    );

    $isCaliforniaZip = $this->isCaliforniaZip($this->billingZipCode);

    foreach ($salesTax->data_array['GroupList'] as $groupList)
    {
      foreach ($groupList->TaxList as $taxEntry)
      {
        // if is MTS tax
        if ($isCaliforniaZip && strpos(strtolower($taxEntry->TaxTypeDesc), 'mts') !== FALSE)
        {
          $result['mts_tax']   += ( $taxEntry->TaxAmount * 100 );
        }
        elseif (strpos(strtolower($taxEntry->TaxTypeDesc), 'sales tax') !== FALSE)
        {
          $result['sales_tax'] += ( $taxEntry->TaxAmount * 100 );
        }
        else
        {
          // no sales tax or mts tax for tax entry
        }
      }
    }

    $result['mts_tax']   = floor($result['mts_tax']);
    $result['sales_tax'] = floor($result['sales_tax']);

    $result['sales_tax_percentage'] = ($result['sales_tax'] + $result['mts_tax']) / $this->amount * 100;

    return $result;
  }

  /**
   * calculate recovery fee based on $amount
   * @param  int $amount
   * @param $cosId
   * @return array {'recovery_fee': int, 'recovery_basis': string, 'recovery_rule', string}
   */
  public function getRecoveryFee($amount, $cosId)
  {
    $fee = array(
      'recovery_fee'   => NULL,
      'recovery_basis' => NULL,
      'recovery_rule'   => 'CC Recovery Rule v1'
    );

    if ( ! $amount)
    {
      $fee['recovery_fee']   = 0;
      $fee['recovery_basis'] = '0';
    }
    elseif ($amount > 1000)
    {
      $fee['recovery_fee']   = 100;
      $fee['recovery_basis'] = 'Charge > $10';
    }
    else
    {
      $fee['recovery_fee']   = 50;
      $fee['recovery_basis'] = 'Charge <= $10';
    }

    if (self::getConfiguration()->getBrandIDFromCOSID($cosId) == 1
      && self::getConfiguration()->planMonthsByCosID($cosId) != null
      && $amount >= 5700
    ) {
      // Ultra multi month plans have $3 recovery fee, the assumption is this is for purchasing a plan and not a bolton
      // have to change $amount if the cheapest multi month price changes :(
      $fee['recovery_fee']   = 300;
      $fee['recovery_basis'] = 'Multi month plan';
    }

    if (self::getConfiguration()->isMintPlan($cosId))
    {
      $totalMonths = self::getConfiguration()->mintMonthsByCosId($cosId);

      // 1 month: $1, 3 month: $1.75, 6 month: $2.50, 12 month: $4
      if ($totalMonths == 1)
      {
        $fee['recovery_fee']   = 100;
        $fee['recovery_basis'] = '1 Month Plan';
      }
      elseif ($totalMonths == 3)
      {
        $fee['recovery_fee']   = 175;
        $fee['recovery_basis'] = '3 Month Plan';
      }
      elseif ($totalMonths == 6)
      {
        $fee['recovery_fee']   = 250;
        $fee['recovery_basis'] = '6 Month Plan';
      }
      elseif ($totalMonths == 12)
      {
        $fee['recovery_fee']   = 400;
        $fee['recovery_basis'] = '12 Month Plan';
      }
      else
      {
        $fee['recovery_fee']   = 0;
        $fee['recovery_basis'] = '0';
      }
    }

    return $fee;
  }

  /**
   * class input validation before calling callSureTax
   * wrapped around callSureTax
   * @param  String $transactionTypeCode
   * @return Result object
   */
  public function getSalesTax($transactionTypeCode)
  {
    try
    {
      if ( ! $this->amount || ! is_numeric($this->amount))
        throw new Exception('No amount provided');

      if ( ! $this->billingZipCode || ! is_numeric($this->billingZipCode) || strlen($this->billingZipCode) != 5)
        throw new Exception('No zip code provided');

      if ( ! $this->account)
        throw new Exception('No account provided');

      // is Result object
      $salesTaxResult = $this->callSureTax($this->amount, $this->billingZipCode, $this->account, $transactionTypeCode);

      return $salesTaxResult;
    }
    catch(Exception $e)
    {
      return make_error_Result($e->getMessage());
    }
  }

  /**
   * STUB for recording quote
   * @param  String $quoteId
   * @return Result
   */
  public function recordSureTaxQuote($quoteId)
  {
    // callSureTax with quoteId and returnFileCode == 'O'
  }

  /**
   * calls SureTax API, handles API result errors
   * @param  Int    $amount
   * @param  String $billingZipCode
   * @param  String $account
   * @param  String $transactionTypeCode
   * @param  Char   $returnFileCode defaults to Q, pass O (oh) to record quote
   * @return Result object
   */
  public function callSureTax($amount, $billingZipCode, $account, $transactionTypeCode, $returnFileCode = NULL)
  {
    $sureTaxResult = $this->getSureTax()->performCall(
      array(
        'amount'                => ($amount / 100),
        'zip_code'              => trim($billingZipCode),
        'account'               => $account,
        'transaction_type_code' => $transactionTypeCode,
        'return_file_code'      => $returnFileCode
      )
    );

    try
    {
      if ( $sureTaxResult->is_failure() )
      {
        dlog('',"ESCALATION IMMEDIATE SureTax API error : call failed : %s", $sureTaxResult->get_errors());
        throw new Exception( 'SureTax API error : call failed' );
      }

      if ( ! isset($sureTaxResult->data_array['TotalTax']))
      {
        dlog('',"ESCALATION IMMEDIATE SureTax API error : call did not return meaningful data");
        throw new Exception( 'SureTax API error : call did not return meaningful data' );
      }

      // there is an error message
      if ( isset( $sureTaxResult->data_array['ItemMessages'] )
        && is_array( $sureTaxResult->data_array['ItemMessages'] )
        && count( $sureTaxResult->data_array['ItemMessages'] )
        && is_object( $sureTaxResult->data_array['ItemMessages'][0] )
        && property_exists( $sureTaxResult->data_array['ItemMessages'][0], 'Message' )
      )
      {
        if (
          $this->retryCount == 0
          && property_exists($sureTaxResult->data_array['ItemMessages'][0], 'ResponseCode')
          && $sureTaxResult->data_array['ItemMessages'][0]->ResponseCode == 9152
        )
        {
          $this->retryCount++;
          dlog('', "Retry $this->retryCount " . __FUNCTION__ . " with customer's activation zip code $this->activationZipCode");
          return $this->callSureTax($amount, $this->activationZipCode, $account, $transactionTypeCode, $returnFileCode);
        } else {
          dlog('',"ESCALATION IMMEDIATE SureTax API error : message = %s", $sureTaxResult->data_array['ItemMessages'][0]->Message);
          throw new Exception( 'SureTax API error : message = ' . $sureTaxResult->data_array['ItemMessages'][0]->Message );
        }
      }

      // there is no grouplist to parse
      if ( ! isset( $sureTaxResult->data_array['GroupList'] )
        || ! is_array( $sureTaxResult->data_array['GroupList'] )
        || ! count( $sureTaxResult->data_array['GroupList'] )
        || ! property_exists( $sureTaxResult->data_array['GroupList'][0], 'TaxList' )
        || ! is_array( $sureTaxResult->data_array['GroupList'][0]->TaxList )
        || ! count( $sureTaxResult->data_array['GroupList'][0]->TaxList )
      )
      {
        dlog('',"ESCALATION IMMEDIATE SureTax API error : API response invalid - %s", $sureTaxResult->data_array);
        throw new Exception( 'SureTax API error : API response invalid' );
      }
    }
    catch(Exception $e)
    {
      return make_error_Result($e->getMessage());
    }

    return $sureTaxResult;
  }

  /**
   * Set the cosId.
   *
   * @param $cosId
   */
  public function setCosId($cosId)
  {
    return self::$cosId = $cosId;
  }

  public static function setConfiguration(Configuration $configuration)
  {
    return self::$configuration = $configuration;
  }

  private static function getConfiguration()
  {
    return empty(self::$configuration) ? self::setConfiguration(new Configuration()) : self::$configuration;
  }

  public function setSureTax(SureTax $sureTax)
  {
    return $this->sureTax = $sureTax;
  }

  private function getSureTax()
  {
    return empty($this->sureTax) ? $this->setSureTax(new SureTax()) : $this->sureTax;
  }

  public function setAccountRepository(AccountsRepository $accountsRepository)
  {
    return $this->accountsRepository = $accountsRepository;
  }

  private function getAccountRepository()
  {
    return empty($this->accountsRepository) ? $this->setAccountRepository(new AccountsRepository()) : $this->accountsRepository;
  }

  public function setCustomerRepository(CustomerRepository $customerRepository)
  {
    return $this->customerRepository = $customerRepository;
  }

  private function getCustomerRepository()
  {
    return empty($this->customerRepository) ? $this->setCustomerRepository(new CustomerRepository()) : $this->customerRepository;
  }
}

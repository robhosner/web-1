<?php

require_once 'db.php';
require_once 'Ultra/MvneConfig.php';
require_once 'Ultra/UltraConfig.php';
require_once 'classes/Throttling.php';

class Throttling_test
{
  public $throttling = null;
  public $banThrottling = null;

  public $errors = [];

  public function addError($error)
  {
    $this->errors[] = $error;
  }

  public function __construct()
  {
    try
    {
      $this->runTests();
      $this->runBanTests();
    }
    catch (Exception $e)
    {
      $this->addError($e->getMessage());
    }

    print_r($this->errors);
  }

  public function runTests()
  {
    // planCanUpdateThrottleSpeed
    $b = \Throttling::planCanUpdateThrottleSpeed('UV30');
    if ($b) throw new \Exception('UV30 should not be able to update throttle');

    $b = \Throttling::planCanUpdateThrottleSpeed('UF15');
    if ( ! $b) throw new \Exception('UF15 should be able to update throttle');

    // throttleSpeedMarketingTerms
    $term = \Throttling::throttleSpeedMarketingTerms('full');
    if ($term != 'Full Speed')
      throw new \Exception('Fetching marketing term');

    // throttlingSpeedMap
    $map = \Throttling::throttlingSpeedMap();

    if ($map['plan']['1536']['config'] != 'a_data_blk1536_plan')
      throw new \Exception('Getting throttling speed map (1)');

    if ($map['block']['1536']['config'] != 'a_data_blk1536')
      throw new \Exception('Getting throttling speed map (2)');

    // throttlingSpeedMapBan
    $map = \Throttling::throttlingSpeedMapBan();
    if ($map['plan']['1536']['config'] != 'n_pool_thr1536')
      throw new \Exception('Getting throttling speed map for BAN');

    // ultra plan
    $throttling = new \Throttling('L19');
    $throttling->setCurrentThrottleSpeed('1536');

    if ($throttling->type != null)
      throw new \Exception('Setting throttling type');

    if ($throttling->currentSpeed != '1536')
      throw new \Exception('Setting current speed');

    // detectThrottleSpeedFromBalanceValues
    $balancesValues = $this->balanceValues();
    $speed = $throttling->detectThrottleSpeedFromBalanceValues($balancesValues);
    if ($speed != '1536')
      throw new \Exception('detecting throttle speed from balance values');

    // adjustPlanConfigForThrottle
    $planConfig = \Ultra\MvneConfig\getAccSocsUltraPlanConfig('STWENTY_NINE', 33404, null);
    $throttling->adjustPlanConfigForThrottle($planConfig);
    if ( ! isset($planConfig['a_data_blk1536_plan']))
      throw new \Exception('Error adjusting plan config for throttle');

    // detectThrottleSpeedFromAddOnFeatureInfo
    $addOnFeatureInfo = $this->addOnFeatureInfo();
    $speed = $throttling->detectThrottleSpeedFromAddOnFeatureInfo($addOnFeatureInfo);
    if ($speed != '1536')
      throw new \Exception('detcting throttle speed from add on feature info');

    // checkThrottleUpdateRequired
    $b = $throttling->checkThrottleUpdateRequired($addOnFeatureInfo, 'full');
    if ( ! $b) throw new \Exception('checking if throttle update required (1)');

    $b = $throttling->checkThrottleUpdateRequired($addOnFeatureInfo, '1536');
    if ($b) throw new \Exception('checking if throttle update required (2)');

    // getThrottleSpeedFeatureFromSocId
    $feature = $throttling->getThrottleSpeedFeatureFromSocId(12909);
    if ($feature != 'plan')
      throw new \Exception('getting feature from soc id');

    // getThrottleSpeedSocType
    $name = $throttling->getThrottleSpeedSocType('plan', '1536');
    if ($name != 'WPRBLK33S')
      throw new \Exception('getting soc type (1)');

    // getThrottleSpeedFeatureFromType
    $feature = $throttling->getThrottleSpeedFeatureFromType('WPRBLK33S');
    if ($feature != 'plan')
      throw new \Exception('getting feature from type');

    // getThrottleSpeedTypeFromSocId
    $name = $throttling->getThrottleSpeedTypeFromSocId(12917);
    if ($name != 'WPRBLK33S')
      throw new \Exception('getting soc type (2)');

    // detectRoamingSoc
    $roamingSocId = $throttling->detectRoamingSoc($addOnFeatureInfo);
    if ($roamingSocId != 12814)
      throw new \Exception('detecting roaming soc from add on feature info');

    // detectRoamingWalletSoc
    $walletSocId = $throttling->detectRoamingWalletSoc($addOnFeatureInfo);
    if ($walletSocId != 12707)
      throw new \Exception('detecting roaming soc from add on feature info');

    // throttleValuesFromBalanceValues
    $mvneGet4gLTE = json_decode('[100,0,0,0,{"DA1":{"DA1":8250},"DA2":{"DA2":9000},"DA7":{"DA7":"500"},"Total Data Usage":{"Total Data Usage Used":"0"},"Base Data Usage":{"Base Data Usage Used":"0","Base Data Usage Limit":"0"},"IN Rated Data Usage":{"IN Rated Data Usage Used":"0"},"WPRBLK33S":{"WPRBLK33S Used":"0","WPRBLK33S Limit":"100"}},null,{"total3G":0,"used3G":0,"remaining3G":0}]', true);

    $values = $throttling->throttleValuesFromBalanceValues($mvneGet4gLTE[4]);
    if ( ! isset($values['WPRBLK33S']['Limit']))
      throw new \Exception('error getting balance values from mvneGet4gLTE');

    // adjustDataSocForThrottle
    $adjusted = $throttling->adjustDataSocForThrottle('a_data_blk', '1536');
    if ($adjusted != 'a_data_blk1536')
      throw new \Exception('adjusted data soc for throttle');
  }

  public function runBanTests()
  {
    //
    $throttling = new \Throttling('UF15');
    $throttling->setCurrentThrottleSpeed('full');

    //
    if ($throttling->type != 'flex')
      throw new \Exception('Setting throttling type');

    //
    if ($throttling->currentSpeed != 'full')
      throw new \Exception('Setting current speed');

    //
    $balancesValues = $this->flexBalanceValues();
    $speed = $throttling->detectThrottleSpeedFromBalanceValues($balancesValues);
    if ($speed != 'full')
      throw new \Exception('Error detecting throttle speed from balance values');

    //
    $addOnFeatureInfo = $this->addOnFeatureInfo();
    $speed = $throttling->detectThrottleSpeedFromAddOnFeatureInfo($addOnFeatureInfo);
    if ($speed != 'full')
      throw new \Exception('detcting throttle speed from add on feature info');

    // checkThrottleUpdateRequired
    $b = $throttling->checkThrottleUpdateRequired($addOnFeatureInfo, '1536');
    if ( ! $b) throw new \Exception('checking if throttle update required (1)');

    $b = $throttling->checkThrottleUpdateRequired($addOnFeatureInfo, 'full');
    if ($b) throw new \Exception('checking if throttle update required (2)');
  }

  public function flexBalanceValues()
  {
    $flexBalanceValues = '[{"Value":8250,"Type":"DA1","Unit":"Unit","Name":"Voice Base bucket","UltraType":"DA1","UltraName":"Voice Minutes"},{"Value":9000,"Type":"DA2","Unit":"Unit","Name":"SMS Base bucket","UltraType":"DA2","UltraName":"SMS Messages"},{"Value":"0","Type":"Total Data Usage","Unit":"KB","Name":"Used","UltraType":"Total Data Usage Used","UltraName":""},{"Value":"0","Type":"Base Data Usage","Unit":"KB","Name":"Used","UltraType":"Base Data Usage Used","UltraName":""},{"Value":"0","Type":"Base Data Usage","Unit":"KB","Name":"Limit","UltraType":"Base Data Usage Limit","UltraName":""},{"Value":"0","Type":"IN Rated Data Usage","Unit":"KB","Name":"Used","UltraType":"IN Rated Data Usage Used","UltraName":""},{"Value":"0","Type":"WHPADJB1","Unit":"KB","Name":"Used","UltraType":"WHPADJB1 Used","UltraName":""},{"Value":"0","Type":"WHPADJB1-BAN","Unit":"KB","Name":"Used","UltraType":"WHPADJB1-BAN Used","UltraName":""},{"Value":7000,"Type":"WHPADJB1-BAN","Unit":"MB","Name":"Limit","UltraType":"WHPADJB1-BAN Limit","UltraName":""}]';
    return json_decode($flexBalanceValues);
  }

  public function balanceValues()
  {
    $balancesValues = '[{"Value":8250,"Type":"DA1","Unit":"Unit","Name":"Voice Base bucket","UltraType":"DA1","UltraName":"Voice Minutes"},{"Value":9000,"Type":"DA2","Unit":"Unit","Name":"SMS Base bucket","UltraType":"DA2","UltraName":"SMS Messages"},{"Value":"500","Type":"DA7","Unit":"Unit","Name":"Voice - Add on","UltraType":"DA7","UltraName":""},{"Value":"0","Type":"Total Data Usage","Unit":"KB","Name":"Used","UltraType":"Total Data Usage Used","UltraName":""},{"Value":"0","Type":"Base Data Usage","Unit":"KB","Name":"Used","UltraType":"Base Data Usage Used","UltraName":""},{"Value":"0","Type":"Base Data Usage","Unit":"KB","Name":"Limit","UltraType":"Base Data Usage Limit","UltraName":""},{"Value":"0","Type":"IN Rated Data Usage","Unit":"KB","Name":"Used","UltraType":"IN Rated Data Usage Used","UltraName":""},{"Value":"0","Type":"IN Rated Data Usage","Unit":"KB","Name":"Used","UltraType":"IN Rated Data Usage Used","UltraName":""},{"Value":"0","Type":"WPRBLK33S","Unit":"KB","Name":"Used","UltraType":"WPRBLK33S Used","UltraName":""},{"Value":"100","Type":"WPRBLK33S","Unit":"MB","Name":"Limit","UltraType":"WPRBLK33S Limit","UltraName":""}]';

    return json_decode($balancesValues);
  }

  public function addOnFeatureInfo()
  {
    $addOnFeatureInfo = '[{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12802","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"Voicemail English","UltraServiceName":"V-ENGLISH"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12701","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"Voice Minutes","UltraServiceName":"B-VOICE"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12702","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"SMS Messages","UltraServiceName":"B-SMS"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12902","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"4G data, then block","UltraServiceName":"B-DATA-BLK"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12808","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"LTE Access","UltraServiceName":"N-LTE"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12707","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"INTL Roaming Voice + SMS Wallet","UltraServiceName":"A-VOICESMSWALLET-IR"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12917","ExpirationDate":"2017-04-06T00:00:00","UltraDescription":"4G Data, block after n Mb","UltraServiceName":"A-DATA-BLK1536-PLAN"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-01T16:07:15","FeatureID":"12814","ExpirationDate":"2020-08-24T00:00:00","UltraDescription":"Simple Global International Roaming (No Domestic Roaming)","UltraServiceName":"N-ROAM-SMPGLB"},{"FeatureID":"12006","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Retail Plan #1","UltraServiceName":"R-UNLIMITED"},{"FeatureID":"111","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Secondary Wholesale","UltraServiceName":"W-SECONDARY"}]';

    return json_decode($addOnFeatureInfo);
  }

  public function flexAddOnFeatureInfo()
  {
    $flexAddOnFeatureInfo = '[{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12802","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"Voicemail English","UltraServiceName":"V-ENGLISH"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12701","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"Voice Minutes","UltraServiceName":"B-VOICE"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12702","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"SMS Messages","UltraServiceName":"B-SMS"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12902","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"4G data, then block","UltraServiceName":"B-DATA-BLK"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12707","ExpirationDate":"2017-04-13T00:00:00","UltraDescription":"INTL Roaming Voice + SMS Wallet","UltraServiceName":"A-VOICESMSWALLET-IR"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:26:17","FeatureID":"12814","ExpirationDate":"2020-08-24T00:00:00","UltraDescription":"Simple Global International Roaming (No Domestic Roaming)","UltraServiceName":"N-ROAM-SMPGLB"},{"AutoRenewalIndicator":"false","FeatureDate":"2017-03-08T16:37:17","FeatureID":"12924","ExpirationDate":"2020-03-22T00:00:00","UltraDescription":"4G data, then cutoff data","UltraServiceName":"A-DATA-POOL-1"},{"FeatureID":"12006","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Retail Plan #1","UltraServiceName":"R-UNLIMITED"},{"FeatureID":"111","FeatureDate":"","AutoRenewalIndicator":"","ExpirationDate":"","UltraDescription":"Secondary Wholesale","UltraServiceName":"W-SECONDARY"}]';

    return json_decode($flexAddOnFeatureInfo);
  }
}

$test = new Throttling_test();

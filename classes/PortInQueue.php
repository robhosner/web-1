<?php


require_once 'classes/UltraAcc.php';
require_once 'db/ultra_acc/portin_queue.php';
require_once 'lib/portal/functions.php';


/**
 * Port In Queue class
 *
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/Fallouts%2C+Async%2C+and+Porting+Behavior
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project MVNE2
 */
class PortInQueue extends UltraAcc
{
  /**
   * Class constructor
   */
  public function __construct( $params=array() )
  {
    parent::__construct( $params );
  }

  /**
   * getParametersNames
   *
   * List of object members names, which correspond to ULTRA_ACC..PORTIN_QUEUE columns
   *
   * @return array
   */
  protected function getParametersNames()
  {
    $schema = \Ultra\Lib\DB\getTableSchema( 'PORTIN_QUEUE' );
    $schema = array_keys( $schema );
    return array_map('strtolower',$schema );
  }

  /**
   * addToPortInQueue
   */
  private function addToPortInQueue()
  {
    $params = $this->getParameters();

    $this->dbConnect();

    $result = add_to_ultra_acc_portin_queue($params);

    if ( $result->is_success() )
    {
      $redis = new \Ultra\Lib\Util\Redis();

      $redis->set( 'ultra/port/update_timestamp/' . $this->msisdn , time() , 60*60*24*7 );
      $redis->set( 'ultra/port/status/' . $params['msisdn'] , 'initialized' , 60*60*24*7 );
    }

    return $result;
  }

  /**
   * load
   *
   * Load data as object members
   *
   * @return object of class \Result
   */
  public function load( $data )
  {
    if ( $data && is_object($data) )
    {
      dlog('',"data = %s",$data);

      $this->loadMembers( $data );

      $this->start_epoch = '';
      if ( property_exists( $data , 'START_EPOCH' ) && $data->START_EPOCH )
        $this->start_epoch = $data->START_EPOCH;

      $this->provstatus_epoch = '';
      if ( property_exists( $data , 'PROVSTATUS_EPOCH' ) && $data->PROVSTATUS_EPOCH )
        $this->provstatus_epoch = $data->PROVSTATUS_EPOCH;

      $this->updated_seconds_ago = '';
      if ( property_exists( $data , 'UPDATED_SECONDS_AGO' ) && $data->UPDATED_SECONDS_AGO )
        $this->updated_seconds_ago = $data->UPDATED_SECONDS_AGO;

      $this->provstatus_description = '';
      if ( property_exists( $data , 'PROVSTATUS_DESCRIPTION' ) && $data->PROVSTATUS_DESCRIPTION )
        $this->provstatus_description = $data->PROVSTATUS_DESCRIPTION;

      return make_ok_Result();
    }
    else
      dlog('','no data found');

    return make_error_Result( 'no data found' );
  }

  /**
   * loadByCustomerId
   *
   * Get the latest port in attempt for $customer_id
   *
   * @return object
   */
  public function loadByCustomerId( $customer_id )
  {
    $this->dbConnect();

    $data = get_portin_queue_by_customer_id( $customer_id );

    return $this->load( $data );
  }

  /**
   * loadByMsisdn
   *
   * Get the latest port in attempt for $msisdn
   *
   * @return object
   */
  public function loadByMsisdn( $msisdn )
  {
    $this->dbConnect();

    $data = get_portin_queue_by_msisdn( $msisdn );

    return $this->load( $data );
  }

  /**
   * initiate
   *
   * A Port In is initiated - we are about to perform the SOAP call (A0)
   * - add a new row to ULTRA_ACC..PORTIN_QUEUE
   * - leave UPDATED_DATE_TIME NULL until confirmOpen will be called
   *
   * @return object of class \Result
   */
  public function initiate( $params=array() )
  {
    dlog('', "(%s)", func_get_args());

    if ( count($params) )
      $this->setParameters($params);

    if ( ! $this->customer_id )
      return make_error_Result( 'customer_id is missing' );

    if ( ! $this->msisdn )
      return make_error_Result( 'msisdn is missing' );

// TODO: check for active PORTIN_QUEUE rows with the same customer_id/msisdn?

    return $this->addToPortInQueue();
  }

  /**
   * confirmOpen
   *
   * A Port In call (ActivateSIM) went through successfully.
   * - update ULTRA_ACC..PORTIN_QUEUE.UPDATED_DATE_TIME
   *
   * @return object of class \Result
   */
/*
  public function confirmOpen( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    return update_ultra_acc_portin_queue(
      array(
        'port_request_id'   => $this->port_request_id, // <portRequestID> tag from ActivateSubscriberResponse
        'due_date_time'     => $this->due_date_time    //   <portDueTime> tag from ActivateSubscriberResponse
      ),
      array(
        'msisdn'            => $this->msisdn,
        'portin_queue_id'   => $this->portin_queue_id
      )
    );
  }
*/

  /**
   * updateOnUpdatePortIn
   *
   * Whenever UpdatePortIn returns successfully
   *
   * @return object of class \Result
   */
  public function updateOnUpdatePortIn( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    if ( $this->isClosed() )
      return make_error_Result( 'PortIn queue element is '.$this->port_status );

    $result = update_ultra_acc_portin_queue(
      array(
        'last_acc_api'           => 'UpdatePortIn',
        'port_status'            => 'UPDATED',
        'provstatus_description' => 'NULL',
        'provstatus_date'        => 'NULL',
        'provstatus_error_code'  => 'NULL',
        'provstatus_error_msg'   => 'NULL',
        'account_number'         => $this->account_number,
        'account_password'       => $this->account_password,
        'account_zipcode'        => $this->account_zipcode,
        'updated_date_time'      => 'NOW'
      ),
      array(
        'msisdn'            => $this->msisdn,
        'portin_queue_id'   => $this->portin_queue_id
      )
    );

    if ( $result->is_success() )
    {
      $redis = new \Ultra\Lib\Util\Redis();

      $redis->set( 'ultra/port/update_timestamp/' . $this->msisdn , time() , 60*60*24*7 );
      $redis->set( 'ultra/port/status/' . $this->msisdn , 'Update Submitted' , 60*60*24*7 );
    }

    return $result;
  }

  /**
   * updateOnQueryStatus
   *
   * Whenever QueryStatus returns and the Port is not complete
   *
   * @return object of class \Result
   */
  public function updateOnQueryStatus( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    $setParams = array(
      'last_acc_api'      => 'QueryStatus',
      'port_status'       => $this->port_status,    //  <PortStatus> tag from QueryStatusResponse
      'port_error'        => $this->port_error,     //   <ResultMsg> tag from QueryStatusResponse
      'due_date_time'     => $this->due_date_time,  // <portDueTime> tag from QueryStatusResponse
      'querystatus_portstatus' => $this->port_status,
      'querystatus_errormsg'   => $this->port_error,
      'querystatus_date_time'  => 'NOW',
      'updated_date_time'      => 'NOW'
    );

    $whereParams = array(
      'msisdn'            => $this->msisdn,
      'portin_queue_id'   => $this->portin_queue_id
    );

    dlog('',"invoking update_ultra_acc_portin_queue with setParams = %s , whereParams = %s",$setParams,$whereParams);

    return update_ultra_acc_portin_queue( $setParams , $whereParams );
  }

  /**
   * updateOnProvisioningStatus
   *
   * Whenever ProvisioningStatus returns
   *
   * @return object of class \Result
   */
  public function updateOnProvisioningStatus( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    if ( $this->isClosed() )
      return make_error_Result( 'PortIn queue element is '.$this->port_status );

    $provstatus_error_code = property_exists( $this , 'provstatus_error_code' ) ? $this->provstatus_error_code : 'NULL' ;
    $provstatus_error_msg  = property_exists( $this , 'provstatus_error_msg'  ) ? $this->provstatus_error_msg  : 'NULL' ;

    $result = update_ultra_acc_portin_queue(
      array(
        'last_acc_api'           => 'ProvisioningStatus',
        'updated_date_time'      => 'NOW',
        'provstatus_date'        => 'NOW',
        'provstatus_description' => $this->provstatus_description,
        'provstatus_error_code'  => $provstatus_error_code,
        'provstatus_error_msg'   => $provstatus_error_msg
      ),
      array(
        'msisdn'            => $this->msisdn,
        'portin_queue_id'   => $this->portin_queue_id
      )
    );

    if ( $result->is_success() && $provstatus_error_msg )
    {
      $redis = new \Ultra\Lib\Util\Redis();

      $redis->set( 'ultra/port/update_timestamp/'     . $this->msisdn , time()                , 60*60*24*7 );

      if ( $this->provstatus_description == 'PORT_RESOLUTION_REQUIRED' )
      {
        $redis->set( 'ultra/port/status/'               . $this->msisdn , 'RESOLUTION REQUIRED' , 60*60*24*7 );
        $redis->set( 'ultra/port/provstatus_error_msg/' . $this->msisdn , $provstatus_error_code . '-' . $provstatus_error_msg );
      }
      elseif ( $this->provstatus_description == 'PORT_CONCURRENCE' )
      {
        $redis->set( 'ultra/port/status/'               . $this->msisdn , 'CONCURRENCE' , 60*60*24*7 );
        $redis->del( 'ultra/port/provstatus_error_msg/' . $this->msisdn );
      }
      elseif ( ( $this->provstatus_description == 'PORT_JEOPARDY' ) || ( $this->provstatus_description == 'PORT_DUE_DATE_CHANGED' ) )
      {
        $redis->set( 'ultra/port/status/'               . $this->msisdn , 'IN PROGRESS' , 60*60*24*7 );
        $redis->del( 'ultra/port/provstatus_error_msg/' . $this->msisdn );
      }
      else
      {
        dlog('','ERROR ESCALATION ALERT DAILY - cannot map provstatus_description = '.$this->provstatus_description);

        $redis->del( 'ultra/port/provstatus_error_msg/' . $this->msisdn );
        $redis->del( 'ultra/port/status/'               . $this->msisdn );
      }
    }

    return $result;
  }

  /**
   * updateCarrierInfo
   *
   * Retrieves carrier info and update PORTIN_QUEUE
   *
   * @return object of class \Result
   */
  public function updateCarrierInfo( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    if ( ! $this->msisdn )
      return make_error_Result( 'msisdn not available in updateCarrierInfo' );

    // query comcetera.com
    $carrier_data = funcCarrierLookup( array( 'msisdn_list' => '1'.$this->msisdn ) );

    dlog('',"carrier_data = %s",$carrier_data);

    // check result (http://www.numberportabilitylookup.com/api)
    if ( $carrier_data && ! count($carrier_data['errors']) && isset($carrier_data['portability']) && is_array($carrier_data['portability']) )
    {
      // ignore results with error codes returned as ERRnn in the first data field
      if (substr($carrier_data['portability'][0][ '1'.$this->msisdn ][0], 0, 3) != 'ERR')
      {
        $ocn          = $carrier_data['portability'][0][ '1'.$this->msisdn ][0];
        $carrier_name = $carrier_data['portability'][0][ '1'.$this->msisdn ][1];

        return update_ultra_acc_portin_queue(
          array(
            'ocn'              => $ocn,
            'carrier_name'     => $carrier_name,
            'ocn_request_date' => 'NOW'
          ),
          array(
            'msisdn'              => $this->msisdn,
            'portin_queue_id'     => $this->portin_queue_id
          )
        );
      }
    }

    return make_ok_Result();
  }

  /**
   * updateOnCancelled
   *
   * Whenever a CancelPortIn is attempted
   *
   * @return object of class \Result
   */
  public function updateOnCancelPortIn( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    if ( $this->isClosed() )
      return make_error_Result( 'PortIn queue element is '.$this->port_status );

    $result = update_ultra_acc_portin_queue(
      array(
        'last_acc_api'           => 'CancelPortIn',
        'port_status'            => 'CANCEL_ATTEMPTED',
        'port_error'             => 'NULL',
        'updated_date_time'      => 'NOW'
      ),
      array(
        'msisdn'            => $this->msisdn,
        'portin_queue_id'   => $this->portin_queue_id
      )
    );

    if ( $result->is_success() )
    {
      $redis = new \Ultra\Lib\Util\Redis();

      $redis->set( 'ultra/port/update_timestamp/' . $this->msisdn , time() , 60*60*24*7 );
      $redis->set( 'ultra/port/status/' . $this->msisdn , 'CANCEL_ATTEMPTED' , 60*60*24*7 );
    }

    return $result;
  }

  /**
   * updateFailedToCancel
   *
   * Whenever a CancelPortIn fails
   *
   * @return object of class \Result
   */
  public function updateFailedToCancel( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    if ( $this->isClosed() )
      return make_error_Result( 'PortIn queue element is '.$this->port_status );

    $result = update_ultra_acc_portin_queue(
      array(
        'last_acc_api'           => 'CancelPortIn',
        'port_status'            => 'CANCEL_FAILED',
        'port_error'             => 'CANCEL_FAILED',
        'updated_date_time'      => 'NOW'
      ),
      array(
        'msisdn'            => $this->msisdn,
        'portin_queue_id'   => $this->portin_queue_id
      )
    );

    if ( $result->is_success() )
    {
      $redis = new \Ultra\Lib\Util\Redis();

      $redis->set( 'ultra/port/update_timestamp/' . $this->msisdn , time() , 60*60*24*7 );
      $redis->set( 'ultra/port/status/' . $this->msisdn , 'CANCEL_FAILED' , 60*60*24*7 );
    }

    return $result;
  }

  /**
   * endAsSuccess
   *
   * Whenever the Port is completed successfully
   *
   * @return object of class \Result
   */
  public function endAsSuccess( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    if ( $this->isClosed() )
      dlog('',"warning, port_status is ".$this->port_status);

    $result = update_ultra_acc_portin_queue(
      array(
        'port_status'         => 'COMPLETE',
        'port_error'          => 'NULL',
        'completed_date_time' => 'NOW'
      ),
      array(
        'msisdn'              => $this->msisdn,
        'portin_queue_id'     => $this->portin_queue_id
      )
    );

    if ( $result->is_success() )
    {
      $redis = new \Ultra\Lib\Util\Redis();
  
      $redis->set( 'ultra/port/update_timestamp/' . $this->msisdn , time() , 60*60*24*7 );
      $redis->set( 'ultra/port/status/' . $this->msisdn , 'COMPLETE' , 60*60*24*7 );
    }

    return $result;
  }

  /**
   * endAsCancelled
   *
   * Whenever a CancelPortIn completes successfully (Synch+Asynch)
   *
   * @return object of class \Result
   */
  public function endAsCancelled( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    if ( $this->isClosed() )
      dlog('',"warning, port_status is ".$this->port_status);

    $result = update_ultra_acc_portin_queue(
      array(
        'due_date_time'          => 'NULL',
        'last_acc_api'           => 'CancelPortIn',
        'port_status'            => 'CANCELLED',
        'port_error'             => 'NULL',
        'updated_date_time'      => 'NOW'
      ),
      array(
        'msisdn'            => $this->msisdn,
        'portin_queue_id'   => $this->portin_queue_id
      )
    );

    if ( $result->is_success() )
    {
      $redis = new \Ultra\Lib\Util\Redis();
  
      $redis->set( 'ultra/port/update_timestamp/' . $this->msisdn , time() , 60*60*24*7 );
      $redis->set( 'ultra/port/status/' . $this->msisdn , 'CANCELLED' , 60*60*24*7 );
    }

    return $result;
  }

  /**
   * isClosed
   *
   * Determines if the current Port is completed successfully or not.
   * This should not allow further updates.
   *
   * @return boolean
   */
  public function isClosed()
  {
    if ( ! property_exists( $this , 'port_status' ) )
      return FALSE;

    return ! ! ( ( $this->port_status == 'COMPLETED' )
              || ( $this->port_status == 'COMPLETE' )
              #|| ( $this->port_status == 'TIMEOUT' )
              || ( $this->port_status == 'CANCELLED' ) );
  }

  /**
   * endAsFailure
   *
   * @return object of class \Result
   */
  public function endAsFailure( $params=array() )
  {
// TODO: ?
  }

}

function interpret_port_status( $portInQueue, $transitionCheck = NULL )
{
  $port_success    = FALSE;
  $port_pending    = TRUE;
  $port_status     = 'IN PROGRESS';
  $port_resolution = '';

  if ( ( $portInQueue->port_status == 'COMPLETE' ) || ( $portInQueue->port_status == 'COMPLETED' ) )
  {
    $port_success = TRUE;
    $port_pending = FALSE;
    $port_status  = 'COMPLETE';
  }
  elseif( ( $portInQueue->provstatus_description == '' ) && ( $portInQueue->port_status == 'CANCEL_FAILED' ) )
    $port_status = 'contact care';
  elseif( ( $portInQueue->provstatus_description == '' ) && ( $portInQueue->port_status == 'CANCELLED' ) )
    $port_status = 'CANCELLED';
  elseif( ( $portInQueue->provstatus_description == '' ) && ( $portInQueue->port_status == 'CANCEL_ATTEMPTED' ) )
    $port_status = 'CANCELLED';
  elseif( ( $portInQueue->provstatus_description == '' && $portInQueue->port_status == 'IN_PROGRESS' ) || portUpdatePending($transitionCheck) )
    $port_status = 'IN PROGRESS';
  elseif( ( $portInQueue->provstatus_description == '' ) && ( $portInQueue->port_status == 'TIMEOUT'   ) )
    $port_status = 'contact care';
  elseif( ( $portInQueue->provstatus_description == '' ) && ( $portInQueue->port_status == 'ERROR'     ) )
    $port_status = 'contact care';
  elseif( ( $portInQueue->provstatus_description == '' ) && ( $portInQueue->port_status == 'UPDATED'   ) )
    $port_status = 'Update Submitted';
  elseif( ( $portInQueue->provstatus_description == '' ) && ( $portInQueue->port_status == 'NEW' ) )
    $port_status = 'initialized';
  elseif( ( $portInQueue->provstatus_description == '' ) && ( $portInQueue->port_status == 'SUBMITTED' ) )
    $port_status = 'initialized';
  elseif( $portInQueue->provstatus_description == 'PORT_RESOLUTION_REQUIRED' )
  {
    $provstatus_error_code = ( property_exists( $portInQueue , 'provstatus_error_code' ) )
                             ?
                             $portInQueue->provstatus_error_code . '-'
                             :
                             ''
                             ;

    $port_resolution = $provstatus_error_code . $portInQueue->provstatus_error_msg;
    $port_status     = 'RESOLUTION REQUIRED';
    $port_pending    = FALSE;
  }
  elseif( $portInQueue->provstatus_description == 'PORT_CONCURRENCE' )
    $port_status = 'CONCURRENCE';
  elseif( $portInQueue->provstatus_description == 'PORT_DUE_DATE_CHANGED' )
    $port_status = 'IN PROGRESS';
  elseif( $portInQueue->provstatus_description == 'PORT_JEOPARDY' )
    $port_status = 'IN PROGRESS';
  elseif( $portInQueue->provstatus_description == 'ERROR' )
    $port_status = 'contact care';
  elseif( $portInQueue->provstatus_description == 'TIMEOUT' )
    $port_status = 'contact care';
  else
    dlog('','ERROR ESCALATION ALERT DAILY - case not handled for provstatus_description = '.$portInQueue->provstatus_description.
            ' ; port_status = '.$portInQueue->port_status);

  return array(
    $port_success,
    $port_pending,
    $port_status,
    $port_resolution
  );
}


/**
 * portUpdatePending
 * check transition status result to determine if a port update is pending
 * @param Array result of provision_check_transition()
 * @return Boolean TRUE if pending, FALSE otherwise
 */
function portUpdatePending($result)
{
  // true only if we have an OPEN mvneUpdatePortIn transition
  return ! empty($result['transition_status']) && $result['transition_status'] == STATUS_OPEN
    && ! empty($result['transition_label']) && $result['transition_label'] == 'mvneUpdatePortIn';
}


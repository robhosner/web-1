<?php


require_once 'db.php';
require_once 'classes/PromoBroadcast.php';
require_once 'lib/util-common.php';


/*
php classes/PromoBroadcast_test.php campaignDirectory
php classes/PromoBroadcast_test.php addCampaign
php classes/PromoBroadcast_test.php initializeCampaign
php classes/PromoBroadcast_test.php loadCampaign
php classes/PromoBroadcast_test.php loadCampaigns
php classes/PromoBroadcast_test.php loadCustomers
php classes/PromoBroadcast_test.php loadOngoingCampaign
php classes/PromoBroadcast_test.php createCampaignDirectories
php classes/PromoBroadcast_test.php generateFiles
php classes/PromoBroadcast_test.php runCampaign
php classes/PromoBroadcast_test.php updateThreadStatus
php classes/PromoBroadcast_test.php update_ultra_promo_broadcast_campaign
php classes/PromoBroadcast_test.php get_customers_for_promo_broadcast_campaign
php classes/PromoBroadcast_test.php removeAgedFiles
*/


abstract class AbstractTestStrategy
{
  abstract function test( $argv  );
}


class TestPromoBroadcast_loadCustomers extends PromoBroadcast
{
  // test the protected method loadCustomers
  public function __construct( $params=array() )
  {
    // http://us1.php.net/reflectionmethod.setaccessible
    $method = new ReflectionMethod('PromoBroadcast', 'loadCustomers');
    $method->setAccessible(true);

    $pb = new PromoBroadcast;

    $r = $pb->loadCampaign(
      array( 'name' => 'test1411080708' )
    );

    print_r($pb);

    // invoke loadCustomers
    $r = $method->invoke( $pb );

    print_r($r);
  }
}


class Test_parseUploadedFile extends PromoBroadcast
{
  // test the protected method parseUploadedFile
  public function test( $params=array() )
  {
    // http://us1.php.net/reflectionmethod.setaccessible
    $method = new ReflectionMethod('PromoBroadcast', 'parseUploadedFile');
    $method->setAccessible(true);

    $pb = new PromoBroadcast;

    $result = $pb->parseUploadedFile('./uploaded_data.csv');

    print_r(array('count' => count($result)));
  }
}


class TestPromoBroadcast_createCampaignDirectories extends PromoBroadcast
{
  // test the protected method createCampaignDirectories
  public function __construct( $params=array() )
  {
    // http://us1.php.net/reflectionmethod.setaccessible
    $method = new ReflectionMethod('PromoBroadcast', 'createCampaignDirectories');
    $method->setAccessible(true);

    $pb = new PromoBroadcast;

    $r = $pb->loadCampaign(
      array( 'name' => 'test campaign 2' )
    );

    print_r($pb);

    // invoke createCampaignDirectories
    $r = $method->invoke( $pb );

    print_r($r);
  }
}


class TestPromoBroadcast_generateFiles extends PromoBroadcast
{
  // test the protected method generateFiles
  public function __construct( $params=array() )
  {
    // http://us1.php.net/reflectionmethod.setaccessible
    $method_1 = new ReflectionMethod('PromoBroadcast', 'loadCustomers');
    $method_1->setAccessible(true);
    $method_2 = new ReflectionMethod('PromoBroadcast', 'generateFiles');
    $method_2->setAccessible(true);

    $pb = new PromoBroadcast;

    $r = $pb->loadCampaign(
      array( 'name' => 'test campaign 2' )
    );

    print_r($pb);

    // invoke loadCustomers
    $r = $method_1->invoke( $pb );

    print_r($r);

    // invoke generateFiles
    $r = $method_2->invoke( $pb );

    print_r($r);
  }
}


class TestPromoBroadcast_updateThreadStatus extends PromoBroadcast
{
  // test the protected method updateThreadStatus
  public function __construct( $params=array() )
  {
    // http://us1.php.net/reflectionmethod.setaccessible
    $method_1 = new ReflectionMethod('PromoBroadcast', 'loadCustomers');
    $method_1->setAccessible(true);
    $method_2 = new ReflectionMethod('PromoBroadcast', 'updateThreadStatus');
    $method_2->setAccessible(true);

    $pb = new PromoBroadcast;

    $r = $pb->loadCampaign(
      array( 'name' => 'test1411080708' )
    );

    print_r($pb);

    // invoke loadCustomers
    $r = $method_1->invoke( $pb );

    print_r($r);

    // invoke updateThreadStatus
    $r = $method_2->invoke( $pb );

    print_r($r);
  }
}


class Test_generateFiles
{
  function test( $argv )
  {
    $test = new TestPromoBroadcast_generateFiles();
  }
}


class Test_updateThreadStatus
{
  function test( $argv )
  {
    $test = new TestPromoBroadcast_updateThreadStatus();
  }
}


class Test_loadCustomers
{
  function test( $argv )
  {
    $test = new TestPromoBroadcast_loadCustomers();
  }
}


class Test_createCampaignDirectories
{
  function test( $argv )
  {
    $test = new TestPromoBroadcast_createCampaignDirectories();
  }
}


class Test_campaignDirectory
{
  function test( $argv )
  {
    $pb = new PromoBroadcast();

    $r = $pb->loadCampaign(
      array( 'name' => $argv[2] )
    );

    echo "\ncampaignDirectory = ".$pb->campaignDirectory()."\n\n";
  }
}


class Test_generateFilesFromUploadedFile
{
  function test( $argv )
  {
    $pb = new PromoBroadcast();

    $r = $pb->loadCampaign(
      array( 'name' => $argv[2] )
    );

    print_r($r);

    if ( $r->is_failure() )
      die("loadCampaign failed");

    print_r($pb);

    $result = $pb->createCampaignDirectories();

    print_r($result);

    if ( $result->is_failure() )
      die("createCampaignDirectories failed");

    $r = $pb->generateFilesFromUploadedFile();

    print_r($r);
  }
}


class Test_runCampaign
{
  function test( $argv )
  {
    $pb = new PromoBroadcast();

    $r = $pb->loadCampaign(
      array(
        'name' => $argv[2]
#'test1411080708' 
      )
    );

    print_r($pb);

    $r = $pb->runCampaign();

    print_r($r);
  }
}


class Test_loadOngoingCampaign
{
  function test( $argv )
  {
    $pb = new PromoBroadcast();

    $r = $pb->loadOngoingCampaign();

    print_r($pb);
  }
}


class Test_loadCampaign
{
  function test( $argv )
  {
    $pb = new PromoBroadcast();

    $r = $pb->loadCampaign(
      array( 'name' => 'Ultra Free Data' )
    );

    print_r($pb);
  }
}


class Test_loadCampaigns
{
  function test( $argv )
  {
    $pb = new PromoBroadcast();

    $r = $pb->loadCampaigns(
      array( 'status' => 'NEW' )
    );

    print_r($r);
  }
}


class Test_initializeCampaign
{
  function test( $argv )
  {
    $pb = new PromoBroadcast();

    $promo_broadcast_campaign_id = $argv[2];

    $r = $pb->loadCampaign(
      array( 'promo_broadcast_campaign_id' => $promo_broadcast_campaign_id )
    );

    print_r($r);

    if ( $r->is_failure() )
      die("loadCampaign failed");

    print_r($pb);

    $r = $pb->initializeCampaign();

    print_r($r);
  }
}


class Test_addCampaign
{
  function test( $argv )
  {
    $pb = new PromoBroadcast();

    $r = $pb->addCampaign(
      array(
        'name'            => 'test'.time(),
        'message_en'      => 'test'.time(),
        'customer_clause' => 'test'
      )
    );

    print_r($r);
  }
}


class Test_get_customers_for_promo_broadcast_campaign
{
  function test( $argv )
  {
    $evil = array('DROP','ALTER','DELETE','INSERT','UPDATE','--','@@',';;');

    foreach( $evil as $forbidden )
    {
      $x = get_customers_for_promo_broadcast_campaign( $forbidden );

      print_r($x);
    }

    $x = get_customers_for_promo_broadcast_campaign( ' o.customer_id = 31 ' );

    print_r($x);
  }
}


class Test_update_ultra_promo_broadcast_campaign
{
  function test( $argv )
  {
    $r = update_ultra_promo_broadcast_campaign(
      array(
        'promo_broadcast_campaign_id' => 1,
        'status'                      => 'ERROR'
      )
    );

    print_r($r);
  }
}


class Test_tzquery
{
  function test( $argv )
  {
    // RAF's test for initialization with time zones

    $time_zone_mapping = time_zone_mapping();

    $sql = 'select top 10 customer_id from htt_customers_overlay_ultra where plan_state = "Active" order by customer_id desc';

    $data = mssql_fetch_all_rows( logged_mssql_query( $sql ) );

    if ( $data && is_array( $data ) && count( $data ) )
    {
      $data = \__::flatten( $data );

      dlog('',"data = %s",$data);

      $sql = "
SELECT TOP 100
     o.CUSTOMER_ID,
     o.CURRENT_MOBILE_NUMBER,
     o.PREFERRED_LANGUAGE,
     z.TZ
FROM CUSTOMERS                     c WITH (NOLOCK)
JOIN HTT_CUSTOMERS_OVERLAY_ULTRA   o WITH (NOLOCK)
ON   o.CUSTOMER_ID = c.CUSTOMER_ID
LEFT OUTER JOIN HTT_LOOKUP_REFERENCE_ZIP      z WITH (NOLOCK)
ON   z.ZIP = c.POSTAL_CODE
WHERE o.CUSTOMER_ID IN ( ".implode(',',$data)." )";

      $data = mssql_fetch_all_rows( logged_mssql_query( $sql ) );

      if ( $data && is_array( $data ) && count( $data ) )
      {
        dlog('',"data = %s",$data);

        foreach( $data as $row )
        {
          if ( empty( $time_zone_mapping[ $row[3] ] ) )
            $row[3] = -6; // CST is default
          else
            $row[3] = $time_zone_mapping[ $row[3] ];

          dlog('',"row = %s",$row);
        }
      }
      else
        dlog('',"No data found (2)");

    }
    else
      dlog('',"No data found (1)");

    exit;
  }
}


class Test_removeAgedFiles
{
  function test( $argv )
  {
    $pb = new PromoBroadcast();
    $pb->removeAgedFiles();
  }
}


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

// connect to DB
teldata_change_db();

$testObject->test( $argv );


?>

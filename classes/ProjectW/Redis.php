<?php

namespace ProjectW;

define( 'PROJECTW_MSISDN_RUNNER_PREFIX' , 'projectw/runner/msisdn/' );
define( 'PROJECTW_MSISDN_IMPORT_PREFIX' , 'projectw/id/msisdn/'     );

/**
 * reserve_msisdn
 *
 * Reserve a msisdn
 *
 * @return boolean
 */
function reserve_msisdn( $msisdn , $redis=NULL )
{
  $redis = empty( $redis ) ? new \Ultra\Lib\Util\Redis() : $redis ;

  if ( $redis->get( PROJECTW_MSISDN_RUNNER_PREFIX . $msisdn ) )
    return FALSE;

  if ( ! $redis->setnx( PROJECTW_MSISDN_RUNNER_PREFIX . $msisdn , getmypid() , 60*10 ) )
    return FALSE;

  return ! ! ( getmypid() == $redis->get( PROJECTW_MSISDN_RUNNER_PREFIX .$msisdn ) );
}

/**
 * release_msisdn
 *
 * Release a msisdn
 *
 * @return NULL
 */
function release_msisdn( $msisdn , $redis=NULL )
{
  $redis = empty( $redis ) ? new \Ultra\Lib\Util\Redis() : $redis ;

  $redis->del( PROJECTW_MSISDN_RUNNER_PREFIX . $msisdn );

  return NULL;
}

/**
 * record_univision_msisdn
 *
 * Record Univision MSISDN in Redis
 *
 * @return NULL
 */
function record_univision_msisdn( $univision_import_id , $msisdn , $redis=NULL )
{
  $redis = empty( $redis ) ? new \Ultra\Lib\Util\Redis() : $redis ;

  // $univision_import_id => $msisdn
  $redis->set( PROJECTW_MSISDN_IMPORT_PREFIX . $msisdn , $univision_import_id , 60 * 60 * 12 ); // TTL is 12 hours
}

/**
 * get_univision_msisdn
 *
 * Get UNIVISION_IMPORT_ID from MSISDN
 *
 * @return NULL
 */
function get_univision_msisdn( $msisdn , $redis=NULL )
{
  $redis = empty( $redis ) ? new \Ultra\Lib\Util\Redis() : $redis ;

  return $redis->get( PROJECTW_MSISDN_IMPORT_PREFIX . $msisdn );
}


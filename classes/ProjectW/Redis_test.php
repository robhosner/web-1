<?php

require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis.php';
require_once 'classes/ProjectW/Redis.php';

$msisdn = '1001001000';

if ( \ProjectW\reserve_msisdn( $msisdn ) )
  echo "Reserved $msisdn\n";
else
  echo "Not Reserved $msisdn\n";

\ProjectW\release_msisdn( $msisdn );

if ( \ProjectW\reserve_msisdn( $msisdn ) )
  echo "Reserved $msisdn\n";
else
  echo "Not Reserved $msisdn\n";

if ( \ProjectW\reserve_msisdn( $msisdn ) )
  echo "Reserved $msisdn\n";
else
  echo "Not Reserved $msisdn\n";

$univision_import_id = 123;

\ProjectW\record_univision_msisdn( $univision_import_id , $msisdn );

echo \ProjectW\get_univision_msisdn($msisdn)."\n";


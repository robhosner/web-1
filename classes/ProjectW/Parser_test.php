<?php

require_once 'db.php';
require_once 'classes/ProjectW.php';
require_once 'classes/ProjectW/Parser.php';
require_once 'test/util.php';


class Test_cleanseBalance
{
  function test( $parserObject , $argv )
  {
    $examples = [
      '0',
      '1',
      '1.234',
      '-1',
      ' 1 ',
      ' 1.3 ',
      '#N/A'
    ];

    foreach( $examples as $balance )
    {
      echo "$balance\n";
      echo $parserObject->cleanseBalance( $balance )."\n\n";
    }
  }
}

class Test_import
{
  function test( $parserObject , $argv )
  {
    $filePath      = $argv[2];
    $fileName      = $argv[3];
    $migrationDate = $argv[4];

    $parserObject->import( $filePath , $fileName , $migrationDate );
  }
}

class Test_processRow
{
  function test( $parserObject , $argv )
  {
    #$row = 'A,23456,4253652000,5-PST,,7864320,50000,7814320,2,45,49,8001260892100985536,8001260892110070022F,310260890098553,0000,1/17/2016 1:00:00 AM,,,1/17/2016 1:00:00 AM,1/17/2016 1:00:00 AM,3446998,First,Last,Springfield,first.last@email.com,80918,225,#N/A,11/01/2015 1:00:00 AM';
    $row = 'A,1004700,7875607000,1-PR,0,2097152,25561,2071591,2,7,,1901260892100428768,1901260892109070728F,110260890042876,7419,11-FEB-16,58,,11-FEB-17,,4701852,,,  -1,o.ma73@gmail.com,00915,225,111111111111111,22-MAY-14';

    $outcome = $parserObject->processRow( $row , 1 , 'test.test' );

    print_r( $outcome );
  }
}

class Test_formatFileDate
{
  function test( $parserObject , $argv )
  {
    $examples = [
      '2-3-2013',
      '2-20-2014',
      '02-20-2015',
      '02/20/2016',
      '02/20/2016 08:00:00:000AM',
      '2/20/2016 08:00:00:000AM',
      '11-FEB-16',
      '22-MAR-2016',
      '1-Aug-16',
      '2-Oct-2016',
      '1-nov-15',
      '2-jan-2015',
      '11-FEB-16 08:00:00:000AM',
      '22-MAR-2016 08:00:00:000AM',
      '1-Aug-16 08:00:00:000AM',
      '2-Oct-2016 08:00:00:000AM',
      '1-nov-15 08:00:00:000AM',
      '2-jan-2015 08:00:00:000AM'
    ];

    foreach( $examples as $date )
    {
      $cleaned = $parserObject->formatFileDate( $date );
      echo "$date->\n$cleaned\n\n";
    }
  }
}

class Test_validateRow
{
  function test( $parserObject , $argv )
  {
    $row = array(
'1', // subs_number
'1', // subs_id
'45 ', // plan_id
' 49', // features
\randomICCID(), // iccid
'1234', // pin
'11/11/2016', // renewal_date
'1', // future_dated_plan
'1', // future_dated_feature
'11/11/2016', // future_dated_featuredate
'11/11/2016', // record_date
'2', // status_id
'1', // dealer_code
'abcd', // first_name
'abcd', // last_name
'abcd', // address
'abcd', // city
'abcd', // state
'abcd', // email
'12345', // zip_code
'EN', // language
\randomIMSI(), // imei
\randomMSISDN(), // msisdn
'1', // balance
\randomICCID(), // temp_iccid
'12345', // data_allotted
'12345', // data_used
'12345'  // data_remaining
    );

    list( $rowErrors , $rowErrorCodes ) = $parserObject->validateRow( $row );

    print_r( $rowErrors );
    print_r( $rowErrorCodes );
  }
}

teldata_change_db();

$parserObject = new \ProjectW\Parser();

$testClass='Test_'.$argv[1];
print "$testClass\n\n";

$testObject = new $testClass();
$testObject->test( $parserObject , $argv );

/*

Examples:

php classes/ProjectW/Parser_test.php validateRow
php classes/ProjectW/Parser_test.php import 'test/ProjectW/Parser/' test_file_2.csv '1/21/2106'
php classes/ProjectW/Parser_test.php import /var/tmp/Ultra/ProjectW/  ProjectW-2016-01-27-7.csv ''

*/


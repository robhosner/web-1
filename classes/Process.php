<?php

// Simple class to access Linux processes info

class Process
{
  private $processes = array();

  public function __construct()
  {

  }

  // returns all processes, filtered by $filters
  public function getProcesses($filters=array())
  {
    $processes = $this->processes;

    // loop through each filter
    foreach( $filters as $filter )
    {
      // apply single filter
      $processes = array_filter($processes, "filter_".$filter);
    }

    return $processes;
  }

  public function getProcessesByNameMatch($processName)
  {
    $processesByNameMatch = array();

    foreach( $this->processes as $process )
    {
      if ( preg_match('/'.$processName.'/', $process['CMD'] ) )
      {
        $processesByNameMatch[] = $process;
      }
    }

    return $processesByNameMatch;
  }

  public function getProcessByPID($pid)
  {
    $processByPID = FALSE;

    foreach( $this->processes as $process )
    {
      if ( $process['PID'] == $pid )
      {
        $processByPID = $process;
      }
    }

    return $processByPID;
  }

  // refresh Linux processes info to the latest snapshot report using "ps"
  public function refresh()
  {
    $rawProcessArray = explode("\n", trim(`ps -ef`));

    // reset processes
    $this->processes = array();

    // loop through each row of ps -ef output
    foreach( $rawProcessArray as $rawProcess )
    {
      // interpret ps -ef output
      if ( preg_match('/^(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(.+)$/', $rawProcess, $values) )
      {
        // format ps -ef output
        $this->processes[] = array(
          'UID'   => $values[1],
          'PID'   => $values[2],
          'PPID'  => $values[3],
          'C'     => $values[4],
          'STIME' => $values[5],
          'TTY'   => $values[6],
          'TIME'  => $values[7],
          'CMD'   => $values[8]
        );
      }
    }
  }
}


// filters to be used in getProcesses

# ... add more filters here

function filter_php($processData)
{
  return preg_match('/php/', $processData['CMD']);
}


function filter_user_apache($processData)
{
  return ( $processData['UID'] == 'apache' );
}


?>

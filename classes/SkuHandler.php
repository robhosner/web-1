<?php

class SkuHandler
{
  protected $skuMap = null;

  protected $subproductInfoLPQ = [
    19,24,23,28,29,33,34,38,39,43,44,45,49,50,54,55,59,
    60,64,65,69,70,74,75,79,80,84,85,89,90,94,95,99,
    57,87,117,147
  ];

  protected $subproductInfoS = [
    23,28,29,33,38,39,43,44,45,49,54,
    57,87,117,147
  ];

  public function __construct()
  {
    try
    {
      $this->skuMap = json_decode(file_get_contents('sku_config.json'));
      $this->verifySkuMap();
    }
    catch (\Exception $e)
    {
      $err = $e->getMessage();
      \dlog('',$err);
    }
  }

  protected function verifySkuMap()
  {
    if ( ! $this->skuMap)
      throw new \Exception('ERROR: sku map is not loaded');
  }

  public function getPaymentProviderSkuMap($provider)
  {
    $result = new \Result();

    try
    {
      $this->verifySkuMap();
      
      if (empty($this->skuMap->$provider))
        throw new \Exception('ERROR: missing sku map for provider');

      $result->add_to_data_array('sku_map', $this->skuMap->$provider);
    }
    catch (\Exception  $e)
    {
      $err = $e->getMessage();
      $result->add_error($err);
      \dlog('', $err);
    }

    $result->succeed();
    return $result;
  }

  public function getPaymentProviderSubproductInfo($subproduct)
  {
    $result = new \Result();

    try
    {
      $this->verifySkuMap();

      if (empty($subproduct))
        throw new \Exception('ERROR: call missing param subproduct');

      $map = [];

      foreach ($this->subproductInfoLPQ as $value)
      {
        $map["L$value"] =  ['value' => $value, 'commissionable' => true,  'brand' => 'ULTRA'];
        $map["PQ$value"] = ['value' => $value, 'commissionable' => false, 'brand' => 'ULTRA'];
      }

      foreach ($this->subproductInfoS as $value)
        $map["S$value"] = ['value' => $value, 'commissionable' => true, 'brand' => 'ULTRA'];

      $result->add_to_data_array(
        'subproduct_info',
        empty($map[$subproduct]) ? null : $map[$subproduct]
      );
    }
    catch (\Exception $e)
    {
      $err = $e->getMessage();
      $result->add_error($err);
      \dlog('', $err);
    }

    $result->succeed();
    return $result;
  }

  public function getPaymentProviderSkuAttribute($provider, $sku, $attribute)
  {
    $result = new \Result();

    try
    {
      if ( ! $map = $this->getPaymentProviderSkuMap($provider))
        throw new \Exception("ERROR: could not find sku map for $provider");

      $map = $map->get_data_key('sku_map');

      if (empty($map->$sku))
        throw new \Exception('ERROR: sku is invalid');

      $attribute = strtolower($attribute);

       list($minimun, $maximum, $destination, $source) = $map->$sku;

      if (empty($$attribute))
        throw new \Exception("ERROR: attribute '$attribute' for $provider $sku does not exist");

      $result->add_to_data_array('attribute', $$attribute);
    }
    catch (\Exception $e)
    {
      $err = $e->getMessage();
      $result->add_error($err);
      \dlog('', $err);
    }

    $result->succeed();
    return $result;
  }

  public function getPaymentProviderDelay($provider)
  {
    switch (strtoupper($provider))
    {
      case 'EPAY':
        return 75;

      case 'WEBPOS':
        return 90;

      case 'QPAY':
        return 120;

      case 'INCOMM':
        return 300;

      case 'TCETRA':
        return 120;

      default:
        return 120;
    }
  }
}
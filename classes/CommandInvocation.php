<?php

require_once 'db/ultra_acc/command_invocations.php';
require_once 'classes/UltraAcc.php';

/**
 * Command Invocations class
 *
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/Fallouts+and+Async+Behavior
 * This class should be the only way to access ULTRA_ACC..COMMAND_INVOCATIONS
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project MVNE2
 */
class CommandInvocation extends UltraAcc
{
  /**
   * Class constructor
   */
  public function __construct( $params=array() )
  {
    parent::__construct( $params );
  }

  /**
   * addToCommandInvocations
   *
   * Add a new row to ULTRA_ACC..COMMAND_INVOCATIONS
   *
   * @return object of class \Result
   */
  private function addToCommandInvocations()
  {
    $params = $this->getParameters();

    $this->dbConnect();

    return add_to_ultra_acc_command_invocations($params);
  }

  /**
   * loadRandomMissing
   *
   * @return boolean
   */
  public function loadRandomMissing()
  {
    $this->dbConnect();

    $command_invocations = get_ultra_acc_command_invocations(
      array(
        'status' => 'MISSING'
      ),
      1,
      'NEWID()' // random
    );

    if ( count($command_invocations) )
    {
      dlog('',"command_invocations = %s",$command_invocations);

      // load data in object
      $this->loadMembers( $command_invocations[0] );

      return TRUE;
    }

    return FALSE;
  }

  /**
   * isOccupied
   *
   * Checks if the given customer is occupied according to ULTRA_ACC..COMMAND_INVOCATIONS.STATUS
   * returns TRUE if at least one row is found with STATUS in ('ERROR','INITIATED','OPEN','REPORTED ERROR','REPORTED MISSING','MISSING')
   *
   * @return boolean
   */
  public function isOccupied( $params=array() )
  {
    $this->dbConnect();

    $command_invocations = get_ultra_acc_command_invocations(
      array(
        'customer_id' => $this->customer_id,
        'status'      => array('ERROR','MISSING','OPEN','INITIATED','REPORTED ERROR','REPORTED MISSING')
      )
    );

    return !!( $command_invocations && is_array($command_invocations) && count($command_invocations) );
  }

  /**
   * loadError
   *
   * Loads a row from ULTRA_ACC..COMMAND_INVOCATIONS with STATUS = 'ERROR'
   * Fails if no rows found.
   * Fails if more than 1 row is found. - verify with Riz
   *
   * @return object of class \Result
   */
  public function loadError( $params=array() )
  {
    $result = new \Result();

    if ( count($params) )
      $this->setParameters($params);

    $this->status = 'ERROR';

    $params = $this->getParameters();

    $this->dbConnect();

    $command_invocations = get_ultra_acc_command_invocations($params);

    if ( count($command_invocations) )
    {
      dlog('',"command_invocations = %s",$command_invocations);

      if ( count($command_invocations) == 1 )
      {
        // load data in object
        $this->loadMembers( $command_invocations[0] );

        $result->succeed();
      }
      else
      {
        $result->add_data_array( $params );
        $result->add_error('COMMAND_INVOCATIONS inconsistency: '.count($command_invocations).' ERROR invocations found.'); // TODO: should we always resolve errors?
      }
    }
    else
    {
      // no ERROR COMMAND_INVOCATIONS found

      $result->add_data_array( $params );
      $result->add_error('NO COMMAND_INVOCATIONS found with status = "ERROR"');
    }

    return $result;
  }

  /**
   * loadOpenByCommand
   *
   * $acc_transition_id is returned in the ProvisioningStatus asynch notification in the tag TransactionIDAnswered
   *
   * @return object of class \Result
   */
  public function loadOpenByCommand( $command , $acc_transition_id )
  {
    return $this->loadOpen(
      array(
        'command'           => $command,
        'acc_transition_id' => $acc_transition_id
      )
    );
  }

  /**
   * loadOpenByCorrelationID
   *
   * Loads a row from ULTRA_ACC..COMMAND_INVOCATIONS with STATUS = 'OPEN' or 'MISSING' or 'REPORTED MISSING' for the given correlation ID
   *
   * @return object of class \Result
   */
  public function loadOpenByCorrelationID( $correlation_id , $command=NULL )
  {
    if ( $command == '' )
      $command = NULL;

    return $this->loadOpen(
      array(
        'command'           => $command,
        'correlation_id'    => $correlation_id
      )
    );
  }

  /**
   * loadOpenByCustomerId
   *
   * Loads a row from ULTRA_ACC..COMMAND_INVOCATIONS with STATUS = 'OPEN' or 'MISSING' or 'REPORTED MISSING' for the given $customer_id
   *
   * @return object of class \Result
   */
  public function loadOpenByCustomerId( $customer_id )
  {
    return $this->loadOpen(
      array(
        'customer_id' => $customer_id
      )
    );
  }

  /**
   * loadPending
   *
   * Loads a row from ULTRA_ACC..COMMAND_INVOCATIONS with STATUS not "RESOLVED","COMPLETED DELAYED","COMPLETED","FAILED"
   *
   * @return object of class \Result
   */
  public function loadPending( $params=array() )
  {
    $result = new \Result();

    if ( count($params) )
      $this->setParameters($params);

    $this->status = NULL;

    $params = $this->getParameters();

    $params['status'] = array(
      "INITIATED", "OPEN", "ERROR", "REPORTED ERROR", "REPORTED MISSING", "MISSING"
    );

    $this->dbConnect();

    $command_invocations = get_ultra_acc_command_invocations($params);

    if ( count($command_invocations) )
    {
      dlog('',"command_invocations = %s",$command_invocations);

      if ( count($command_invocations) ) // == 1 )
      {
        // load data in object
        $this->loadMembers( $command_invocations[0] );

        $this->started_hours_ago = $command_invocations[0]->STARTED_HOURS_AGO;

        dlog('',"%s",( (array) $this ));

        $result->set_pending();
        $result->succeed();
      }
      else
      {
        $result->add_data_array( $params );
        $result->add_error("COMMAND_INVOCATIONS inconsistency: ".count($command_invocations)." pending invocations found.");
      }
    }
    else
    {
      // no pending COMMAND_INVOCATIONS found
      $result->succeed();
    }

    return $result;
  }

  /**
   * loadOpen
   *
   * Loads a row from ULTRA_ACC..COMMAND_INVOCATIONS with STATUS = 'OPEN'
   *
   * @return object of class \Result
   */
  public function loadOpen( $params=array() )
  {
    $result = new \Result();

    if ( count($params) )
      $this->setParameters($params);

    // $this->status = 'OPEN';

    $params = $this->getParameters();

    $params['status'] = array( 'OPEN' , 'MISSING' , 'REPORTED MISSING' );

    $this->dbConnect();

    $command_invocations = get_ultra_acc_command_invocations($params);

    if ( count($command_invocations) )
    {
      dlog('',"command_invocations = %s",$command_invocations);

      if ( count($command_invocations) == 1 )
      {
        // load data in object
        $this->loadMembers( $command_invocations[0] );

        dlog('',"%s",( (array) $this ));

        $result->set_pending();
        $result->succeed();
      }
      else
      {
        $result->add_data_array( $params );
        $result->add_error("COMMAND_INVOCATIONS inconsistency: ".count($command_invocations)." OPEN invocations found.");
      }
    }
    else
    {
      // no OPEN COMMAND_INVOCATIONS found
      $result->succeed();
    }

    return $result;
  }

  /**
   * confirmOpen
   *
   * An Async ACC Call has the Sync Returned successfully (M0 - was A1).
   * Modify an existing row in ULTRA_ACC..COMMAND_INVOCATIONS , STATUS='OPEN'
   *
   * @return object of class \Result
   */
  public function confirmOpen( $params=array() )
  {
    $this->status = 'OPEN';

    if ( count($params) )
      $this->setParameters($params);

    return update_ultra_acc_command_invocations(
      array(
        'customer_id'            => $this->customer_id,
        'from_status'            => 'INITIATED',
        'to_status'              => $this->status,
        'acc_transition_id'      => $this->acc_transition_id,
        'status_history'         => $this->status_history.';'.$this->status.'|'.date("Y-m-d H:i:s").'|'
      )
    );
  }

  /**
   * initiate
   *
   * An Async ACC Call is initiated - we are about to perform the SOAP call (A0)
   * Add a new row to ULTRA_ACC..COMMAND_INVOCATIONS , STATUS='INITIATED'
   * Method blockOccupied should be checked beforehand.
   *
   * @return object of class \Result
   */
  public function initiate( $params=array() )
  {
    $this->status_history = 'INITIATED|'.date("Y-m-d H:i:s").'|';
    $this->status         = 'INITIATED';

    if ( count($params) )
      $this->setParameters($params);

    if ( ! property_exists( $this , 'customer_id' ) || empty( $this->customer_id ) )
      return make_error_Result( 'customer_id is missing' );

    $redis = new \Ultra\Lib\Util\Redis;

    if ( ! $this->reserveCommandInvocationByCustomerId( $this->customer_id , $redis ) )
      return make_error_Result( 'Race condition error - customer_id already reserved' );

    $result = $this->addToCommandInvocations();

    $this->unreserveCommandInvocationByCustomerId( $this->customer_id , $redis );

    return $result;
  }

  /**
   * escalation
   *
   * Escalations gets a ticket number from AMDOCS WSD (M1)
   * Modify an existing row in ULTRA_ACC..COMMAND_INVOCATIONS , STATUS='REPORTED ERROR' or 'REPORTED MISSING'
   *
   * @return object of class \Result
   */
  private function escalation( $params )
  {
    if ( count($params) )
      $this->setParameters($params);

    return update_ultra_acc_command_invocations(
      array(
        'command_invocations_id' => $this->command_invocations_id,
        'to_status'              => $this->status,
        'last_status_updated_by' => $this->last_status_updated_by,
        'ticket_details'         => ( property_exists($this,'ticket_details') ? $this->ticket_details : '' ),
        'status_history'         => $this->status_history.';'.$this->status.'|'.date("Y-m-d H:i:s").'|'.$this->last_status_updated_by,
        'reported_date_time'     => 'GETUTCDATE()'
      )
    );
  }

  /**
   * escalationError
   *
   * @return object of class \Result
   */
  public function escalationError( $params=array() )
  {
    $this->status = 'REPORTED ERROR';

    return $this->escalation( $params );
  }

  /**
   * escalationMissing
   *
   * @return object of class \Result
   */
  public function escalationMissing( $params=array() )
  {
    $this->status = 'REPORTED MISSING';

    return $this->escalation( $params );
  }

  /**
   * endAsPortCancelled
   *
   * @return object of class \Result
   */
  public function endAsPortCancelled( $params=array() )
  {
    return $this->endAsSuccess( $params , 'PORT_CANCELLED' );
  }

  /**
   * endAsSuccess
   *
   * An Async ACC Call arrives in time with a Success (C3)
   * Modify an existing row in ULTRA_ACC..COMMAND_INVOCATIONS , STATUS='COMPLETED'
   *
   * @return object of class \Result
   */
  public function endAsSuccess( $params=array() , $event=NULL )
  {
    if ( count($params) )
      $this->setParameters($params);

    $this->status = 'COMPLETED';

    if ( ! $event )
      $event = $this->status;

    return update_ultra_acc_command_invocations(
      array(
        'command_invocations_id' => $this->command_invocations_id,
        'to_status'              => $this->status,
        'status_history'         => $this->status_history.';'.$event.'|'.date("Y-m-d H:i:s").'|',
        'reply_date_time'        => 'GETUTCDATE()',
        'completed_date_time'    => 'GETUTCDATE()'
      )
    );
  }

  /**
   * delayedSuccess
   *
   * An Async ACC Call has the Async Return Late with a Success (C2)
   * Modify an existing row in ULTRA_ACC..COMMAND_INVOCATIONS , STATUS='COMPLETED DELAYED'
   *
   * @return object of class \Result
   */
  public function delayedSuccess( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    $this->status = 'COMPLETED DELAYED';

    return update_ultra_acc_command_invocations(
      array(
        'command_invocations_id' => $this->command_invocations_id,
        'to_status'              => $this->status,
        'status_history'         => $this->status_history.';'.$this->status.'|'.date("Y-m-d H:i:s").'|',
        'reply_date_time'        => 'GETUTCDATE()',
        'completed_date_time'    => 'GETUTCDATE()'
      )
    );
  }

  /**
   * escalationSuccess
   *
   * Escalations closed a ticket from AMDOCS WSD (C1)
   * Modify an existing row in ULTRA_ACC..COMMAND_INVOCATIONS , STATUS='RESOLVED'
   * Requires $params['from_status'] as input
   *
   * @return object of class \Result
   */
  public function escalationSuccess( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    $this->status = 'RESOLVED';

    if ( property_exists($this,'status_history') )
      $this->status_history = $this->status_history.';'.$this->status.'|'.date("Y-m-d H:i:s").'|'.$this->last_status_updated_by;
    else
      $this->status_history = $this->status.'|'.date("Y-m-d H:i:s").'|'.$this->last_status_updated_by;

    return update_ultra_acc_command_invocations(
      array(
        'customer_id'            => $this->customer_id,
        'from_status'            => $params['from_status'],
        'to_status'              => $this->status,
        'last_status_updated_by' => $this->last_status_updated_by,
        'status_history'         => $this->status_history,
        'reply_date_time'        => 'GETUTCDATE()',
        'completed_date_time'    => 'GETUTCDATE()',
        'ticket_details'         => ( property_exists($this,'ticket_details') ? $this->ticket_details : '' )
      )
    );
  }

  /**
   * failOnSync
   *
   * An Async ACC Call has the Sync Return with an Error (C4)
   * Modify an existing row in ULTRA_ACC..COMMAND_INVOCATIONS , STATUS='FAILED'
   *
   * @return object of class \Result
   */
  public function failOnSync( $params=array() , $to_status='FAILED' )
  {
    if ( count($params) )
      $this->setParameters($params);

    $this->status = $to_status;

    if ( empty( $this->acc_transition_id ) )
      $this->acc_transition_id = NULL;

    $this->dbConnect();

    return update_ultra_acc_command_invocations(
      array(
        'customer_id'            => $this->customer_id,
        'from_status'            => 'INITIATED',
        'to_status'              => $this->status,
        'status_history'         => $this->status_history.';'.$this->status.'|'.date("Y-m-d H:i:s").'|',
        'reply_date_time'        => 'GETUTCDATE()',
        'completed_date_time'    => ( ($to_status=='FAILED') ? 'GETUTCDATE()' : 'NULL' ),
        'error_code_and_message' => $this->error_code_and_message,
        'acc_transition_id'      => $this->acc_transition_id
      )
    );
  }

  /**
   * failOnAsync
   *
   * An Async ACC Call has the Async Return Late with an Error (M2)
   * Modify an existing row in ULTRA_ACC..COMMAND_INVOCATIONS , STATUS='ERROR'
   *
   * @return object of class \Result
   */
  public function failOnAsync( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    $this->status = 'ERROR';

    return update_ultra_acc_command_invocations(
      array(
        'customer_id'            => $this->customer_id,
        'from_status'            => 'OPEN',
        'to_status'              => $this->status,
        'status_history'         => $this->status_history.';'.$this->status.'|'.date("Y-m-d H:i:s").'|',
        'reply_date_time'        => 'GETUTCDATE()',
        'completed_date_time'    => 'GETUTCDATE()',
        'error_code_and_message' => $this->error_code_and_message
      )
    );
  }

  /**
   * failOnTimeout
   *
   * An Async ACC Call does not return within the allowable Async time (M3)
   * Modify an existing row in ULTRA_ACC..COMMAND_INVOCATIONS , STATUS='MISSING'
   *
   * @return object of class \Result
   */
  public function failOnTimeout( $params=array() )
  {
    if ( count($params) )
      $this->setParameters($params);

    $this->status = 'MISSING';

    return update_ultra_acc_command_invocations(
      array(
        'customer_id'            => $this->customer_id,
        'from_status'            => 'OPEN',
        'to_status'              => $this->status,
        'status_history'         => $this->status_history.';'.$this->status.'|'.date("Y-m-d H:i:s").'|'
      )
    );
  }

  /**
   * getParametersNames
   *
   * List of object members names, which correspond to ULTRA_ACC..COMMAND_INVOCATIONS columns
   *
   * @return array
   */
  protected function getParametersNames()
  {
    $schema = \Ultra\Lib\DB\getTableSchema( 'COMMAND_INVOCATIONS' );
    $schema = array_keys( $schema );
    return array_map('strtolower',$schema );
  }

  /**
   * reserveCommandInvocationByCustomerId
   *
   * @return boolean
   */
  private function reserveCommandInvocationByCustomerId( $customer_id , $redis=NULL )
  {
    // we need a \Ultra\Lib\Util\Redis object
    if ( is_null($redis) )
      $redis = new \Ultra\Lib\Util\Redis;

    $key = 'ultra/command/invocation/customer_id/'.$customer_id;

    if ( $redis->get( $key ) )
      return FALSE;

    $redis->set( $key , 1 , 60*10 ); // 10 minutes

    return TRUE;
  }

  /**
   * unreserveCommandInvocationByCustomerId
   *
   * @return NULL
   */
  private function unreserveCommandInvocationByCustomerId( $customer_id , $redis=NULL )
  {
    // we need a \Ultra\Lib\Util\Redis object
    if ( is_null($redis) )
      $redis = new \Ultra\Lib\Util\Redis;

    $key = 'ultra/command/invocation/customer_id/'.$customer_id;

    $redis->del( $key );
  }

  /**
   * housekeeping
   *
   * Generic method for COMMAND_INVOCATIONS housekeeping
   *
   * @return object of class \Result
   */
  public function housekeeping()
  {
    $datetime = new \DateTime();
    if ( $datetime->format('i') % 3 )
      return \make_ok_Result();
    else // once every 3 minutes
      // if a command invocation is OPEN for more than 480 seconds and is not a Port Request, update status to MISSING
      return \gone_missing_ultra_acc_command_invocations();
  }

  /**
   * resynchronize
   *
   * Does a QuerySubscriber on that customer.
   * If CurrentAsyncService is NOT EMPTY, move on.
   * If CurrentAsyncService is EMPTY then set STATUS='RESOLVED', TICKET_DETAILS='MISSING resolved by Invocations Resync' 
   *
   * @return object of class \Result
   */
  public function resynchronize( $agent , $ticket_details=NULL )
  {
    teldata_change_db();

    $ICCID = \Ultra\Lib\DB\Getter\getScalar('CUSTOMER_ID', $this->customer_id, 'CURRENT_ICCID_FULL');

    if ( ! $ICCID )
      return make_error_Result( 'no ICCID found for customer id '.$this->customer_id );

    $this->dbConnect();

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $result = $mwControl->mwQuerySubscriber(
      array(
        'actionUUID' => __FUNCTION__.time(),
        'iccid'      => $ICCID
      )
    );

    if ( $result->is_failure() )
      return $result;

    if ( isset($result->data_array['body']->CurrentAsyncService)
      && $result->data_array['body']->CurrentAsyncService
    )
      return make_error_Result('There is a pending asynch command ('.$result->data_array['body']->CurrentAsyncService.').');

    // there are no pending asynch commands

    $this->dbConnect();

    return $this->escalationSuccess(
      array(
        'command_invocations_id' => $this->command_invocations_id,
        'from_status'            => $this->status,
        'ticket_details'         => $ticket_details,
        'last_status_updated_by' => $agent
      )
    );
  }
}

/**
 * command_invocations_initiated_cleanup
 *
 * @return NULL
 */
function command_invocations_initiated_cleanup()
{
  // get all stuck INITIATED COMMAND_INVOCATIONS
  $command_invocations_initiated = get_ultra_acc_command_invocations(
    array(
      'status'          => 'INITIATED',
      'start_date_time' => 'past_10_minutes'
    ),
    NULL,
    NULL,
    TRUE // nolock
  );

  if ( $command_invocations_initiated && is_array($command_invocations_initiated) && count($command_invocations_initiated) )
  {
    dlog('',"command_invocations_initiated = %s",$command_invocations_initiated);
    dlog('','ESCALATION CLEANUP RUNNER (COMMANDINVOCATIONS) === ('.count($command_invocations_initiated).')');
  }

  return NULL;
}

?>

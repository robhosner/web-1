<?php

require_once 'classes/Result.php';

/**
 * General purpose class to be used as a result for functions and methods if used in API version > 1.
 *
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project Primo API
 */
class Outcome extends Result
{
  protected $user_errors = array();
  protected $error_codes = array();

  public function __construct( $new_arg=NULL, $success=FALSE )
  {
    parent::__construct( $new_arg, $success );
  }

  /**
   * resetErrors
   *
   * Resets all errors
   * 
   * @return NULL
   */
  public function resetErrors()
  {
    $this->error_codes = array();
    $this->user_errors = array();
    $this->errors      = array();

    return NULL;
  }

  /**
   * get_error_codes
   *
   * Returns array of error codes
   * 
   * @return array
   */
  public function get_error_codes()
  {
    return $this->error_codes;
  }

  /**
   * get_user_errors
   *
   * Returns array of user friendly errors
   * 
   * @return array
   */
  public function get_user_errors()
  {
    return $this->user_errors;
  }

  /**
   * add_errors_and_code
   *
   * Add error, user friendly error and error_code
   *
   * @param string $error
   */
  public function add_errors_and_code( $error , $error_code , $user_error=NULL )
  {
    $this->add_error( $error );
    $this->error_codes[] = $error_code;
    $this->user_errors[] = $user_error;
  }

  /**
   * get_errors_and_code
   *
   * Returns error, user friendly error and error_code
   * 
   * @return array
   */
  public function get_errors_and_code()
  {
    return array(
      $this->get_errors(),
      $this->get_error_codes(),
      $this->get_user_errors()
    );
  }

  /**
   * merge
   *
   * Merges the given Outcome object with $this
   *
   * @return NULL
   */
  public function merge( \Outcome $outcome )
  {
    $this->error_codes = $outcome->get_error_codes();
    $this->errors      = $outcome->get_errors();
    $this->user_errors = $outcome->get_user_errors();
    $this->success     = $outcome->is_success();
    $this->pending     = $outcome->is_pending();
    $this->timeout     = $outcome->is_timeout();
    $this->warnings    = $outcome->get_warnings();
    $this->log         = $outcome->get_log();
    $this->data_array  = array_merge(
      $this->data_array , $outcome->data_array
    );
  }

}

/**
 * make_error_Result
 * @param  string $error
 * @param  mixed $data
 * @return object Result
 */
function make_error_Outcome( $error , $error_code , $user_error=NULL )
{
  $o = new Outcome();

  $o->add_errors_and_code( $error , $error_code , $user_error );

  return $o;
}


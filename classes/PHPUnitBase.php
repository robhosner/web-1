<?php

require_once('web.php');
require_once('Ultra/tests/API/DefaultUltraAPITest.php');

class PHPUnitBase extends PHPUnit_Framework_testCase
{
  // options and their defaults
  private $bath = 'rest';
  private $partner = NULL;
  private $api = NULL;
  private $version = 2;
  private $wsdl = NULL;


  public function setUp()
  {
    // All APIs tested with this class will have be reachable with this base URL:
    $this->base_url = find_site_url().'/ultra_api.php?bath=rest&partner=internal&version=2';

    // our credentials
    $this->apache_username = 'dougmeli';
    $this->apache_password = 'Flora';

    // options for our curl commands
    $this->curl_options = array(
        CURLOPT_USERPWD  => "$this->apache_username:$this->apache_password",
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
    );
  }


  /**
   * callerConfig
   * configure API options by caller's name
   * a convenience function for PHPUnit tests of format 'test__partner__API' (e.g. 'test__dealerportal__ReshipSIMCard')
   * @param String caller name (typically magic constant __FUNCTION__)
   * @param Integer API version
   * @param String partner (if different partner than derived from caller name)
   */
  public function callerConfig($function, $version, $partner = NULL)
  {
    $parts = explode('__', $function);
    if (count($parts) != 3)
      return logError("failed to parse caller name $function");

    list($test, $prefix, $command) = $parts;
    $this->setOptions(array('api' => "{$prefix}__{$command}", 'version' => $version, 'partner' => $partner ? $partner : $prefix));
  }


  public function setPartner($partner)
  {
    $this->partner = $partner;
    $this->base_url = find_site_url().'/ultra_api.php?bath=rest&partner='.$partner.'&version=2';
  }


  public function callApi($params, $options = array())
  {
    // check against old usage
    if ( ! is_array($params))
      return logError('improper usage, this function takes only one parameter');

    // validate pre-requisites
    foreach (array('partner', 'api') as $option)
      if (empty($this->$option))
        return logError("missing required option $option - use setOptions()");

    // determine API URL
    if ( ! $location = $this->getApiUrl(find_domain_url()))
      return logError('failed to determine API URL');

    // set API options
    $type = $this->bath == 'rest' ? 'pr' : 'ps';
    $options['location'] = "$location/$type/{$this->partner}/{$this->version}/ultra/api/{$this->api}";
    $options['username'] = $this->apache_username;
    $options['password'] = $this->apache_password;
    $options['debug']    = TRUE;

    // JSON
    if ($this->bath == 'rest')
    {
      return callJsonApi($this->api, $params, $options);
    }

    // SOAP
    else
    {
      // validate and set SOAP pre-requisites
      if ( ! $this->wsdl)
        return logError("missing WSDL - use setOptions()");
      $options['wsdl'] = $this->wsdl;

      return callSoapApi($this->api, $params, $options);
    }
  }

  /**
   * setOptions
   * set supported class options
   * @param Array option => value
   */
  public function setOptions($options)
  {
    // supported options and their acceptable values
    $supported = array(
      'bath'        => array('rest', 'soap'),
      'version'     => array(1, 2),
      'wsdl'        => NULL, // NULL means do not validate value, always accept
      'api'         => NULL,
      'debug'       => array(TRUE, FALSE),
      'partner'     => NULL);

    foreach ($options as $option => $value)
    {
      // validate option
      if ( ! array_key_exists($option, $supported))
        echo "invalid option $option";

      // validate option value
      elseif (is_array($supported[$option]) &&  ! in_array($value, $supported[$option]))
        echo "invalid option $option value $value";

      // set the option
      else
        $this->$option = $value;
    }
  }


  public function invokeAPI( array $params )
  {
    // required
    if ( empty( $params['api'] ) )
    {
      return false;
    }

    // defaults
    $params['parameters']     = !empty( $params['parameters'] )     ? $params['parameters']     : array();
    $params['version']        = !empty( $params['version'] )        ? $params['version']        : 2;
    $params['partner']        = !empty( $params['partner'] )        ? $params['partner']        : substr( $params['api'], 0, strpos( $params['api'], '__' ) );
    $params['api_runtime']    = !empty( $params['api_runtime'] )    ? $params['api_runtime']    : 60;
    $params['connect_time']   = !empty( $params['connect_time'] )   ? $params['connect_time']   : 1;
    $params['domain']         = !empty( $params['domain'] )         ? $params['domain']         : find_domain_url();
    $params['user']           = !empty( $params['user'] )           ? $params['user']           : $this->apache_username;
    $params['password']       = !empty( $params['password'] )       ? $params['password']       : $this->apache_password;
    $params['cookie']         = !empty( $params['cookie'] )         ? $params['cookie']         : null;
    $params['protocol']       = !empty( $params['protocol'] )       ? $params['protocol']       : 'https';
    $params['headers']        = !empty( $params['headers'] )        ? $params['headers']        : array();
    $params['options']        = !empty( $params['options'] )        ? $params['options']        : array();
    $params['cgi']            = !empty( $params['cgi'] )            ? $params['cgi']            : null;
    $params['bath']           = !empty( $params['bath'] )           ? $params['bath']           : null;

    return callUltraApi(
      $params['api'],
      $params['parameters'],
      $params['version'],
      $params['partner'],
      $params['api_runtime'],
      $params['connect_time'],
      $params['domain'],
      $params['user'],
      $params['password'],
      $params['cookie'],
      $params['protocol'],
      $params['headers'],
      $params['options'],
      $params['cgi'],
      $params['bath']
    );
  }


  /** 
   * getApiUrl
   * parse somewhat inconsistent response of find_domain_url() into a proper URL
   * @param String input URL
   * @param String output URL
   */
  private function getApiUrl($domain)
  {
    // remove trailing slash if present
    if (substr($domain, -1) == '/')
      $domain = substr($domain, 0, strlen($domain) - 1);

    // parse and analyse result
    $parts = explode(':', $domain);

    // domain only
    if (count($parts) == 1)
      return 'http://' . $parts[0];

    // confusing data: attempt to parse 2 parts
    elseif (count($parts) == 2)
    {
      // domain:port, no protocol
      if (is_numeric($parts[1]))
      {
        if ($parts[1] == '443')
          return 'https://' . $parts[0];
        elseif ($parts[1] == '80')
          return 'http://' . $parts[0];
        else // custom port: we do not know if this is an SSL connection
          return logError("unable to use custom port {$parts[1]} from domain $domain");
      }

      // protocol:domain, no port
      else
        return $domain;
    }

    // protocol:domain:port
    elseif (count($parts) == 3)
      $location = $domain;

    // did not parse as expected
    else
      return logError("unable to parse domain $domain");
  }


  /**
   * makeZsession
   * get customer login info from DB, call portal__Login and return result zsession
   * for testing v1 API that take zsession
   * @param int customer_id
   * @return String zsession or NULL on failure
   * @author: VYT, 14-02-11
  */
  public function makeZsession($customer_id)
  {
    if (! $customer_id)
    {
      echo "ERROR: invalid customer_id parameter \n";
      return NULL;
    }

    // get customer info
    teldata_change_db();
    $customer = get_customer_from_customer_id($customer_id);
    if (! $customer)
    {
      echo "ERROR: failed to get customer for customer_id $customer_id \n";
      return NULL;
    }
    if (strlen($customer->LOGIN_NAME) < 4 || strlen($customer->LOGIN_PASSWORD) < 4)
    {
      echo "ERROR: username or password is missing or too short for customer_id $customer_id \n";
      return NULL;
    }

    // prepare API URL and parameters
    $url = find_site_url() . '/pr/portal/1/ultra/api/portal__Login';
    $params = array(
      'account_login'    => $customer->LOGIN_NAME,
      'account_password' => $customer->LOGIN_PASSWORD);

    echo "LOGIN    : ".$customer->LOGIN_NAME."\n";
    echo "PASSWORD : ".$customer->LOGIN_PASSWORD."\n";

    // execute login and check results
    $result = curl_post($url, $params, array(), NULL, 120);
    $decoded_result = json_decode($result);

    print_r($decoded_result);

    if (! $decoded_result->success != TRUE && count($decoded_result->errors))
    {
      echo "ERROR: failed to login customer $customer_id \n";
      return NULL;
    }
    if (empty($decoded_result->zsession))
    {
      echo "ERROR: failed to get zsession for customer $customer_id: \n";
      foreach ($decoded_result->errors as $message)
        echo "$message \n";
      return NULL;
    }

    // all's well
    return $decoded_result->zsession;
  }
}

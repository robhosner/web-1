<?php

/**
 * Univision Migration Extesnion Plan
 *
 * http://wiki.hometowntelecom.com:8090/display/SPEC/MEP+-+Migration+Extension+Plan
 * http://wiki.hometowntelecom.com:8090/display/SPEC/Project+W+-+Database+Considerations+During+Migration
 *
 * @see PJW-80, API-387, API-388, API-388
 * @author VYT, 2016
 */

class MigrationExtensionPlan
{
  const MEP_INTL_BALANCE  = 150000; // $2.50 of INTL credit
  const MEP_DATA_BALANCE  = 500; // 500MB data balance
  const MEP_LAST_DATE     = '2/11/2016  8:00:00 AM'; // UTC, end of MEP eligibility window
  const MEP_MAX_DURATION  = 6; // customers may stay in MEP up to 6 days


  /**
   * getNewMepCustomers
   * returns customers whose MEP started since yesterday
   * @return Array of Objects
   */
  public function getNewMepCustomers()
  {
    $sql = sprintf('SELECT CUSTOMER_ID, OPTION_VALUE FROM ULTRA.CUSTOMER_OPTIONS WITH (NOLOCK) WHERE OPTION_ATTRIBUTE = \'%s\'',
      MIGRATION_EXTENSION_PLAN_START_DATE);
    $result = mssql_fetch_all_objects(logged_mssql_query($sql));
    logInfo(sprintf('found %d customers', count($result)));

    // filter out subscribers whose MEP started before yesterday or has not started yet:
    // we cannot filter out MEP.STAT_DATE value at DB level because is VARCHAR
    // if we cast/convert it to DAYTIME then the entire query may fail on a single bad row
    $today = time();
    $yesterday = $today - SECONDS_IN_DAY;
    foreach ($result as $index => $value)
    {
      if ( ! $date = date_to_epoch($value->OPTION_VALUE, 'UTC'))
      {
        logError("invalid MEP date {$value->OPTION_VALUE} for customer ID {$value->CUSTOMER_ID}");
        unset($result[$index]);
      }
      if ($date < $yesterday || $date > $today)
        unset($result[$index]);
    }

    logInfo(sprintf('processing %d customers after filtering', count($result)));
    return $result;
  }


  /**
   * getFundedMepCustomers
   * returns MEP customers with sufficient funds to renew out of MEP
   * @return Array of Objects
   */
  public function getFundedMepCustomers()
  {
    // get all subs with sufficient balance to cover plan cost
    $sql = sprintf('SELECT o.CUSTOMER_ID
      FROM ULTRA.CUSTOMER_OPTIONS o WITH (NOLOCK)
      JOIN ACCOUNTS a WITH (NOLOCK) ON o.CUSTOMER_ID = a.CUSTOMER_ID
      JOIN HTT_CUSTOMERS_OVERLAY_ULTRA u WITH (NOLOCK) ON o.CUSTOMER_ID = u.CUSTOMER_ID
      JOIN PARENT_COS p WITH (NOLOCK) ON a.COS_ID = p.COS_ID
      WHERE o.OPTION_ATTRIBUTE = \'%s\'
      AND ((a.BALANCE + u.STORED_VALUE >= 2 * p.PLAN_COST) OR (a.BALANCE + u.STORED_VALUE >= p.PLAN_COST AND u.PLAN_EXPIRES > \'%s\'))',
      MIGRATION_EXTENSION_PLAN_START_DATE,
      self::MEP_LAST_DATE);
    $result = mssql_fetch_all_objects(logged_mssql_query($sql));
    logInfo(sprintf('found %d customers', count($result)));
    return $result;
  }


  /**
   * getExpiredMepCustomers
   * get customers overstayed in MEP by MEP_MAX_DURATION days
   * @return Array of Objects
   */
  function getExpiredMepCustomers()
  {
    // MEP start date cannot be less than LAST_MOD_TIMESTAMP (e.g. we do not backdate MEP),
    // therefore it is safe to filter out those added after the current expiration
    $expired = time() - self::MEP_MAX_DURATION * SECONDS_IN_DAY;
    $sql = sprintf('SELECT CUSTOMER_ID, OPTION_VALUE FROM ULTRA.CUSTOMER_OPTIONS WITH (NOLOCK) WHERE OPTION_ATTRIBUTE = \'%s\' AND LAST_MOD_TIMESTAMP < \'%s\'',
      MIGRATION_EXTENSION_PLAN_START_DATE, gmdate(MSSQL_DATE_FORMAT, $expired));
    $result = mssql_fetch_all_objects(logged_mssql_query($sql));
    logInfo(sprintf('found %d customers', count($result)));

    // filter out those who have not overstayed yet (since MEP can start after LAST_MOD_TIMESTAMP)
    foreach ($result as $index => $value)
      if (date_to_epoch($value->OPTION_VALUE, 'UTC') > $expired)
        unset($result[$index]);

    logInfo(sprintf('processing %d customers after filtering', count($result)));
    return $result;
  }


  /**
   * fetchCustomerRecord
   * get specific fields of combined ACCOUNTS, HTT_CUSTOMERS_OVERLAY_ULTRA records from DB but only if sub is Active
   * @param Integer CUSTOMER_ID
   * @return Object on success or NULL on failure
   */
  public function fetchCustomerRecord($customerId)
  {
    $sql = sprintf('SELECT u.CUSTOMER_ID, u.CURRENT_MOBILE_NUMBER, u.CURRENT_ICCID_FULL, u.PREFERRED_LANGUAGE, a.PACKAGED_BALANCE1, a.COS_ID
      FROM HTT_CUSTOMERS_OVERLAY_ULTRA u WITH (NOLOCK)
      JOIN ACCOUNTS a WITH (NOLOCK) ON u.CUSTOMER_ID = a.CUSTOMER_ID
      WHERE u.CUSTOMER_ID = %d AND u.PLAN_STATE = \'%s\'',
      $customerId, STATE_ACTIVE);
    $result = mssql_fetch_all_objects(logged_mssql_query($sql));
    if (count($result) != 1)
      return logError('failed to retrieve customer info: ' . count($result) . ' records found');
    return $result[0];
  }


  /**
   * adjustIntlBalance
   *
   * set international call credit to MEP_INTL_BALANCE
   * @param Object customer
   * @return NULL
   */
  public function adjustIntlBalance($customer)
  {
    logInfo("processing customer ID {$customer->CUSTOMER_ID}, PACKAGED_BALANCE1 {$customer->PACKAGED_BALANCE1}");

    // do not lower PACKAGED_BALANCE1 below MEP_INTL_BALANCE
    if ($customer->PACKAGED_BALANCE1 <= self::MEP_INTL_BALANCE)
      return logInfo("skipping PACKAGED_BALANCE1 {$customer->PACKAGED_BALANCE1}");

    $sql = accounts_update_query(array('customer_id' => $customer->CUSTOMER_ID, 'packaged_balance1' => self::MEP_INTL_BALANCE));
    if ( ! is_mssql_successful(logged_mssql_query($sql)))
      return logError('failed to decrease PACKAGED_BALANCE1');

    \logInfo( "PACKAGED_BALANCE1 = {$customer->PACKAGED_BALANCE1}" );
    \logInfo( "MEP_INTL_BALANCE  = ". self::MEP_INTL_BALANCE );

    // add HTT_BILLING_HISTORY record ( see section 5.3.2 )
    $sql = htt_billing_history_insert_query([
      'customer_id'            => $customer->CUSTOMER_ID,
      'date'                   => 'now',
      'cos_id'                 => $customer->COS_ID,
      'entry_type'             => 'UVIMPORT',
      'stored_value_change'    => 0,
      'balance_change'         => 0,
      #'package_balance_change' => ( $customer->PACKAGED_BALANCE1 - self::MEP_INTL_BALANCE ) * 1000 * 60,
      'package_balance_change' => ( self::MEP_INTL_BALANCE - $customer->PACKAGED_BALANCE1 ),
      'charge_amount'          => 0,
      'reference'              => create_guid( __FUNCTION__ ),
      'reference_source'       => get_reference_source('UVIMPORT'),
      'detail'                 => __FUNCTION__,
      'description'            => 'UVIMPORT.MEP_INTL_DECREASE',
      'result'                 => 'COMPLETE',
      'source'                 => 'CSCOURTESY',
      'is_commissionable'      => 0
    ]);
    if ( ! is_mssql_successful(logged_mssql_query($sql)))
      logError('failed to create HTT_BILLING_HISTORY record');

    return NULL;
  }


  /**
   * adjustDataBalance
   * set A-DATA-BLK-PLAN SOC to MEP_DATA_BALANCE
   * @param Object customer
   */
  public function adjustDataBalance($customer)
  {
    logInfo("processing customer ID {$customer->CUSTOMER_ID}");

    // there is little point in checking the current balance before resetting the data SOC because:
    //   1) subs are just migrated and have full data buckets
    //   2) we reset it to a fixed value without any calculations
    //   3) all migrated subs are expected to be in UV30-UV55 plans which include A-DATA-BLK-PLAN SOC
    $acc = new \Ultra\Lib\MiddleWare\ACC\Control;
    $result = $acc->processControlCommand(array
    (
      'command'    => 'UpdatePlanAndFeatures',
      'actionUUID' => getNewActionUUID(),
      'parameters' => array(
        'msisdn'             => $customer->CURRENT_MOBILE_NUMBER,
        'iccid'              => $customer->CURRENT_ICCID_FULL,
        'customer_id'        => $customer->CUSTOMER_ID,
        'wholesale_plan'     => \Ultra\Lib\DB\Customer\getWholesalePlan($customer->CUSTOMER_ID),
        'ultra_plan'         => get_plan_from_cos_id($customer->COS_ID),
        'preferred_language' => $customer->PREFERRED_LANGUAGE,
        'option'             => 'A-DATA-BLK-PLAN|' . self::MEP_DATA_BALANCE,
        'keepDataSocs'       => TRUE
    )));
    teldata_change_db();
    if ($result->is_failure())
      logError('failed to update data SOC: ' . json_encode($result->get_errors()));
  }


  /**
   * renewCustomerPlan
   * renew MEP customer plan and remove MEP option
   * @param Object customer
   * @return Boolean sucess
   */
  public function renewCustomerPlan($customer)
  {
    logInfo("processing customer ID {$customer->CUSTOMER_ID}");

    $result = transition_customer_state($customer, 'Monthly Renewal');
    if ( ! $result['success'])
      return logError("failed to renew plan for customer ID {$customer->CUSTOMER_ID}: {$result['errors'][0]}");

    // remove from MEP
    $result = remove_customer_options($customer->CUSTOMER_ID, array(MIGRATION_EXTENSION_PLAN_START_DATE));
    if ($result->is_failure())
      return logError("failed to remove MEP option for customer ID {$customer->CUSTOMER_ID}");

    logInfo("successfully promoted customer {$customer->CUSTOMER_ID} out of MEP");
    return TRUE;
  }


  /**
   * suspendRemoveCustomer
   * suspect customer and remove from MEP
   * @param Object customer
   * @return Boolean sucess
   */
  public function suspendRemoveCustomer($subscriber)
  {
    $id = $subscriber->CUSTOMER_ID;
    logInfo("processing customer ID $id");

    if ( ! $customer = get_customer_from_customer_id($id))
      return logError("failed to fetch record for customer ID $id");

    $result = suspend_account($customer, array('customer_id' => $id));
    if (count($result['errors']))
      return logError("failed to suspend customer ID $id: {$result['errors'][0]}");

    // remove from MEP
    $result = remove_customer_options($id, array(MIGRATION_EXTENSION_PLAN_START_DATE));
    if ($result->is_failure())
      return logError("failed to remove MEP option for customer ID $id");

    logInfo("successfully suspended and removed customer ID $id from MEP");
    return TRUE;
  }
}

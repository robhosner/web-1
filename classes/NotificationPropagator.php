<?php

require_once 'db.php';
require_once 'Ultra/Lib/MiddleWare/ACC/Notification.php';

/**
 * Instantiated by runner
 *
 * purpose of class is to retrieve TYPE_ID 3 notifications from the SOAP_LOG
 * and divert them to DEV and QA based on rules involving CorrelationID
 *
 * Will only run in PROD
 * Checks config "acc/notifications/propagate/enabled" before running
 * Locks SOAP_LOG_IDs for 1 hour in Redis to prevent duplicate processing
 */
class NotificationPropagator
{
  private $redis;

  // last processed SOAP_LOG_ID
  const REDIS_LAST_PROCESSED    = 'ultra/runner/notification_propagator/last';
  // used to LOCK SOAP_LOG_ID in redis
  const REDIS_PREFIX_PROCESSING = 'ultra/runner/notification_propagator/processing/';
  // max iterations per thread
  const MAX_ITERATIONS          = 1000;

  public function __construct($redis = NULL)
  {
    $this->redis = ($redis) ? $redis : new \Ultra\Lib\Util\Redis();
  }

  public function run( $argv )
  {
    if ( ! \Ultra\UltraConfig\isProdEnvironment())
      return \LogDebug('Not a production environment, exiting.');

    // check if this runner is enabled
    if ( ! \Ultra\UltraConfig\notifications_propagate_enabled())
      return \LogDebug('Not enabled.');

    \Ultra\Lib\DB\ultra_acc_connect();

    if ( $this->forceSoapIDList( $argv ) )
      $recentSoapLogs = $this->getSoapLogsFromIDList( $argv );
    else
    {
      // last processed SOAP_LOG_ID
      $lastProcessedId = $this->getLastProcessedSoapLogId();

      if ( ! $lastProcessedId )
        return \logFatal( 'Cannot continue: lastProcessedId not defined' );

      // recent SOAP_LOG rows FROM last processed
      $recentSoapLogs = $this->getRecentSoapLogs($lastProcessedId);
    }

    \logInfo( 'found '.count($recentSoapLogs).' rows' );

    $i = 0; // to count iterations
    foreach ($recentSoapLogs as $soapLog)
    {
      \logInfo( json_encode( $soapLog ) );

      if ( ! $this->forceSoapIDList( $argv ) && ( ! $this->lockSoapLogId($soapLog->SOAP_LOG_ID, SECONDS_IN_MINUTE * 10 ) ) )
      {
        \logDebug("SKIPPING SOAP_LOG_ID {$soapLog->SOAP_LOG_ID} : already processing");
        continue;
      }

      // set last processed SOAP_LOG_ID
      // this is how getRecentSoapLogs knows where to start
      $this->redis->set(self::REDIS_LAST_PROCESSED, $soapLog->SOAP_LOG_ID, SECONDS_IN_MINUTE * 60);

      if ( property_exists($soapLog,'xml') )
        $soapLog->DATA_XML = $soapLog->xml;

      // this returns XML as an ARRAY
      $data = $this->parseXMLString($soapLog->DATA_XML);

      $endPoints = $this->getEndPoints($soapLog->TAG);

      if ( empty( $endPoints ) )
      {
        \logError("NO AVAILABLE ENDPOINTS for TAG {$soapLog->TAG}");

        continue;
      }

      $error = $this->sendToEndpoints($soapLog->COMMAND, $data, $this->getEndPoints($soapLog->TAG));

      if ($error)
      {
        \logError("EXITING UNSUCCESSFULLY : $error");
        exit;
      }

      $i++;
      if ($i >= self::MAX_ITERATIONS)
      {
        \logDebug('Reached max iterations');
        break;
      }
    }

    \logDebug('EXITING SUCCESSFULLY');
  }

  /**
   * getLastProcessedSoapLogId
   *
   * Returns the last processed SOAP_LOG_ID.
   * If not defined, returns the MAX(SOAP_LOG_ID), minus 100
   * @return Integer
   */
  protected function getLastProcessedSoapLogId()
  {
    // last processed SOAP_LOG_ID
    $lastProcessedId = $this->redis->get(self::REDIS_LAST_PROCESSED);

    if ( ! $lastProcessedId )
    {
      // we need to get a recent SOAP_LOG_ID

      $lastProcessedId = \get_max_soap_log_id();

      if ( ! empty( $lastProcessedId ) )
        $lastProcessedId -= 100;
    }

    return $lastProcessedId;
  }

  /**
   * lockSoapLogId
   *
   * locks a SOAP_LOG_ID in redis for this runner
   * returns FALSE if already locked
   *
   * @param  Integer $soapLogId
   * @return Boolean was lock successful?
   */
  private function lockSoapLogId($soapLogId, $ttl = 600)
  {
    // used to LOCK SOAP_LOG_ID in redis
    $redisKeyProcessing = self::REDIS_PREFIX_PROCESSING . $soapLogId;

    if ( $this->redis->get( $redisKeyProcessing ) )
      return FALSE;

    // set SOAP_LOG_ID as PROCESSING in redis
    return $this->redis->setnx( $redisKeyProcessing , 1, $ttl);
  }

  /**
   * sendToEndpoints
   *
   * performs a $command SOAP call to each $endpoints
   *
   * @param  String $command to execute
   * @param  Array  $data from SOAP notification
   * @param  Array  $endpoints to send to
   * @return String on ERROR | NULL on SUCCESS
   */
  private function sendToEndpoints($command, $data, $endpoints)
  {
    \logDebug("DIVERTING NOTIFICATION to " . json_encode($endpoints));
    \logDebug("COMMAND $command | DATA TO PROCESS: " . json_encode($data));

    $wsdl = $this->getWSDL();

    foreach ($endpoints as $endpoint)
    {
      \logDebug( "endpoint = $endpoint" );

      // divert notification to endpoint through SOAP call
      $result = \Ultra\Lib\MiddleWare\ACC\Notification::divertNotification(
        $wsdl,
        $endpoint,
        $command,
        $data
      );

      if (count($errors = $result->get_errors()))
      {
        \logError($errors[0]);
        return $errors[0];
      }
      else
        \logDebug("SUCCESS diverting to $endpoint");
    }

    return NULL;
  }

  /**
   * forceSoapIDList
   *
   * @return boolean
   */
  public function forceSoapIDList( $argv )
  {
    $forceSoapIDList = ! empty( $argv[1] );

    return $forceSoapIDList;
  }

  /**
   * getSoapLogsFromIDList
   */
  private function getSoapLogsFromIDList( $argv )
  {
    return get_soap_log(
      array(
        'sql_limit'   => 1000,
        'soap_log_id' => explode(',',$argv[1])
      )
    );
  }

  /**
   * getRecentSoapLogs
   *
   * returns recent SOAP_LOG rows with TYPE_ID 3
   *
   * @param  Integer SELECT WHERE SOAP_LOG_ID > $startAfter
   * @param  Integer $typeId returns rows with TYPE_ID equals to this
   * @param  Integer $newerThanInMinutes returns rows newer than NOW - $newerThanInMinutes
   * @return Array of SOAP_LOG rows
   */
  private function getRecentSoapLogs($startAfter = 0, $typeId = 3, $newerThanInMinutes = 5)
  {    
    return soap_log_select_for_propagation($startAfter, $typeId, $newerThanInMinutes);
  }

  /**
   * getWSDL
   *
   * returns the ACC Notification WSDL
   *
   * @return String path to WSDL
   */
  private function getWSDL()
  {
    $wsdl = \Ultra\UltraConfig\acc_notification_wsdl();
    $wsdl = ($wsdl) ? 'runners/amdocs/' . $wsdl : NULL;

    if ($wsdl)
      \logDebug('Using ACC notification WSDL: ' . $wsdl);
    else
      \logError('ERROR getting ACC notification WSDL name');
    
    return $wsdl;
  }

  /**
   * parseXMLString
   *
   * returns an Array given an XML string
   *
   * @param  String $xmlString
   * @return Array
   */
  public static function parseXMLString($xmlString)
  {
    $xml = simplexml_load_string($xmlString);
    return json_decode(json_encode($xml), TRUE);
  }

  /**
   * getEndPoints
   *
   * inspects correction ID and returns an array of endpoints to redirect to
   * if notifications_propagate_all_enabled redirect NULL/EMPTY correlationID to DEV/QA
   * if correlationID DEV-* send to DEV
   * if correlationID QA-*  send to QA
   *
   * @param  String $correlationID
   * @return Array
   */
  private function getEndPoints($correlationID)
  {
    $endpoints = array();

    if (empty($correlationID) && \Ultra\UltraConfig\notifications_propagate_all_enabled())
    {
      if (! empty($endpoint = \Ultra\UltraConfig\getDevMiddlewareEndpoint()))
        $endpoints[] = $endpoint;

      if (! empty($endpoint = \Ultra\UltraConfig\getQAMiddlewareEndpoint()))
        $endpoints[] = $endpoint;
    }
    else
    {
      if (strpos($correlationID, 'DEV') === 0)
      {
        if (! empty($endpoint = \Ultra\UltraConfig\getDevMiddlewareEndpoint()))
          $endpoints[] = $endpoint;
      }
      else if (strpos($correlationID, 'QA') === 0)
      {
        if (! empty($endpoint = \Ultra\UltraConfig\getQAMiddlewareEndpoint()))
          $endpoints[] = $endpoint;
      }
    }

    return $endpoints;
  }
}

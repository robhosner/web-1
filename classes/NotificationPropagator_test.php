<?php

require_once 'classes/NotificationPropagator.php';
require_once 'Ultra/Lib/Util/Redis.php';

$propagator = new \NotificationPropagator();

$redis = new \Ultra\Lib\Util\Redis();

$class = new ReflectionClass ($propagator);

// test lockSoapLogId
$testId = 'TEST-' . time();

$method = $class->getMethod ('lockSoapLogId');
$method->setAccessible(true);

if ($method->invoke($propagator, $testId, 10))
  echo $testId . ' locked' . PHP_EOL;
else
{
  echo "ERROR $testId did not lock";
  exit;
}

if ( ! $method->invoke($propagator, $testId, 10))
  echo $testId . ' still locked' . PHP_EOL;
else
{
  echo "ERROR $testId should still be locked";
  exit;
}

// wait here to let TTL die
echo 'Waiting 11 seconds' . PHP_EOL;
sleep(11);

if ( $method->invoke($propagator, $testId, 10))
  echo $testId . ' locked again' . PHP_EOL;
else
{
  echo "ERROR $testId did not lock again";
  exit;
}
// END test lockSoapLogId

// test getWSDL
$method = $class->getMethod ('getWSDL');
$method->setAccessible(true);
$result = $method->invoke($propagator);

// test parseXMLString
$xml = <<<XML
<?xml version='1.0'?> 
<document>
 <title>Don Muerte?</title>
 <from>James</from>
 <to>Mozart</to>
 <body>
  The quick brown fox.
 </body>
</document>
XML;

$method = $class->getMethod ('parseXMLString');
$method->setAccessible(true);
$result = $method->invoke($propagator, $xml);
print_r($result);

// test getEndpoints
$method = $class->getMethod ('getEndpoints');
$method->setAccessible(true);

// empty at time of testing
$result = $method->invoke($propagator, 'QA-TESTID');
print_r($result);

$result = $method->invoke($propagator, 'DEV-TESTID');
print_r($result);

// test sendToEndpoints
$endpoints = $result;
$command = 'ProvisioningStatus';
$data = (array)json_decode('{
  "PortErrorCode": "",
  "UserData": {
    "senderId": "ACC",
    "timeStamp": "2015-12-22T12:13:20"
  },
  "ProvisioningType": "ChangeFeatures",
  "MSISDN": "5555559606",
  "TransactionIDAnswered": "API-ULTRA-288745931415",
  "StatusDescription": "",
  "CorrelationID": "DEV-1450831415-UpdatePlanAndFeatures-{AX-56678CD3F3431415-EF2171D159D31415}",
  "ProvisioningStatus": 1,
  "PortErrorDesc": ""
}');

$method = $class->getMethod('sendToEndpoints');
$method->setAccessible(true);
$result = $method->invoke($propagator, $command, $data, $endpoints);
print_r($result);

// test getRecentSoapLogs
$typeId = 2; // queries for TYPE_ID 3 does not return results in DEV
$newerThanInMinutes = 60 * 10;

exit;

$propagator->run();

exit;

$method = $class->getMethod ('getRecentSoapLogs');
$method->setAccessible(true);
$result = $method->invoke($propagator, 0, $typeId, $newerThanInMinutes);
print_r($result);

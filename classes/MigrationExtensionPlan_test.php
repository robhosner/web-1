<?php

require_once 'db.php';
require_once 'classes/MigrationExtensionPlan.php';

$mep = new MigrationExtensionPlan;

teldata_change_db();

$customer = new stdClass();

$customer->CUSTOMER_ID       = 1;
$customer->PACKAGED_BALANCE1 = 152222;
$customer->COS_ID            = 98294;

$mep->adjustIntlBalance( $customer );


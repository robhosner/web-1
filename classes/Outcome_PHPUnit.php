<?php

require_once 'classes/Outcome.php';

class OutcomeTest extends PHPUnit_Framework_testCase
{
  /**
   * test__Outcome
   */
  public function test__Outcome()
  {
    $o = new \Outcome();

    $this->assertInstanceOf( 'Outcome' , $o );

    $o->succeed();

    $this->assertTrue( $o->is_success() );

    $o->add_errors_and_code(
      'error',
      'error_code',
      'user_error'
    );

    $this->assertTrue( $o->is_failure() );

    $this->assertContains( 'error'      , $o->get_errors() );
    $this->assertContains( 'error_code' , $o->get_error_codes() );
    $this->assertContains( 'user_error' , $o->get_user_errors() );

    $this->assertCount( 3 , $o->get_errors_and_code() );

    $x = make_error_Outcome( 'a' , 'b' );

    $this->assertTrue( $x->is_failure() );

    $this->assertContains( 'a' , $x->get_errors() );
    $this->assertContains( 'b' , $x->get_error_codes() );

  }
}


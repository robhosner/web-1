<?php

namespace Ultra\Lib;

/**
 * This class is to handle the distribution of free
 * roaming credit to subs activated before $activatedBeforeDate
 */
class RoamCreditPromo
{
  public $activatedBeforeDate = null;
  public $activationDate      = null;
  public $cos_id              = null;

  public function __construct()
  {
    // change to pull from configuration
    $this->setActivatedBeforeDate('11/15/2016');
  }

  /**
   * sets the date sub should be activated before
   * to be eligible for free roaming credit
   */
  private function setActivatedBeforeDate($dateString)
  {
    $this->activatedBeforeDate = strtotime($dateString);
  }

  /**
   * set the customers activation date
   */
  public function setActivationDate($activationDate)
  {
    $this->activationDate = strtotime($activationDate);
  }

  /**
   * set the customers renewal date
   */
  public function setRenewalDate($renewalDate)
  {
    $this->renewalDate = strtotime($renewalDate);
  }

  /**
   * set customers cos_id
   */
  public function setCosId($cos_id)
  {
    $this->cos_id = $cos_id;
  }

  /**
   * add applicable roaming feature info
   * use while building UpdatePlanAndFeatures call to ACC
   */
  public static function proccesParamsWithAddOnFeatureInfo($params, $addOnFeatureInfo)
  {
    // if parameter roam_credit_promo does not exist
    // add nothing
    if ( ! isset($params[self::getParam()]) || ! $params[self::getParam()])
      return $params;

    // if we are adding roaming credit, and roaming soc does not exist
    // we should add the correct roaming soc for brand_id
    if ( ! \Ultra\MvneConfig\detectRoamingSoc($addOnFeatureInfo))
      $params = self::replaceParams($params, \Ultra\UltraConfig\roamingSocFeatureIdByBrandId($params['brand_id']), '');

    // feature id for the roaming wallet soc
    $walletFeatureId = \Ultra\MvneConfig\getAccSocDefinitionField('a_voicesmswallet_ir', 'plan_id');

    // if roaming wallet soc is detected
    if ( \Ultra\MvneConfig\detectRoamingWalletSoc($addOnFeatureInfo)
      && isset($params['PlanData']['AddOnFeatureList']['AddOnFeature'])
    )
    {
      // loop through already built params
      // this should come at the end of the UpdatePlanAndFeatures build process
      // set to INCREMENT with roam_credit_promo_value
      foreach ($params['PlanData']['AddOnFeatureList']['AddOnFeature'] as &$setAddOnFeature)
        if ($setAddOnFeature['FeatureID'] == $walletFeatureId)
        {
          $setAddOnFeature['Action']       = 'INCREMENT';
          $setAddOnFeature['FeatureValue'] = 0;
          break;
        }
    }
    else // else add with roam_credit_promo value
      $params = self::replaceParams($params, $walletFeatureId, 0);

    return $params;
  }

  public static function replaceParams($params, $featureId, $value)
  {
    $replaced = 0;
    foreach ($params['PlanData']['AddOnFeatureList']['AddOnFeature'] as &$setAddOnFeature)
      if ($setAddOnFeature['FeatureID'] == $featureId)
      {
        $setAddOnFeature['FeatureValue'] = $value;
        $replaced = 1;
        break;
      }

    if ( ! $replaced)
      $params['PlanData']['AddOnFeatureList']['AddOnFeature'][] =
        [
          'Action'       => 'ADD',
          'autoRenew'    => 'false',
          'FeatureID'    => $featureId,
          'FeatureValue' => $value
        ];

    return $params;
  }

  /**
   * determintes credit amount to give to sub
   * amount of credit is mapped to COS_ID
   */
  public function determineRoamCreditAmount($cos_id)
  {
    // time promotional period should start
    if ( ! $this->activatedBeforeDate)
    {
      \logError('activatedBeforeDate is NULL (determineRoamCreditAmount)');
      return 0;
    }

    // must be existing customer before this date
    if ( ! $this->activationDate)
    {
      \logError('activatedBeforeDate is NULL (determineRoamCreditAmount)');
      return 0;
    }

    // if activated after return 0 free credit
    if ($this->activatedBeforeDate < $this->activationDate)
      return 0;

    // if promotional period is over, return 0 free credit
    if ($this->renewalDate > strtotime('+1 month', $this->activatedBeforeDate))
    {
      \logit('Remove RoamCreditPromo, free credit is no longer being applied.');
      return 0;
    }

    // map COS_ID to roam credit values in dollars
    $map = [
      COSID_ULTRA_NINETEEN          => 5,
      COSID_ULTRA_TWENTY_FOUR       => 10,
      COSID_ULTRA_STWENTY_NINE      => 10,
      COSID_ULTRA_MULTI_TWENTY_NINE => 10,
      COSID_ULTRA_THIRTY_FOUR       => 10,
      COSID_ULTRA_THIRTY_NINE       => 10,
      COSID_ULTRA_DTHIRTY_NINE      => 10,
      COSID_ULTRA_STHIRTY_NINE      => 10,
      COSID_ULTRA_FORTY_FOUR        => 15,
      COSID_ULTRA_SFORTY_FOUR       => 15,
      COSID_ULTRA_FORTY_NINE        => 15,
      COSID_ULTRA_SFIFTY_FOUR       => 15,
      COSID_ULTRA_FIFTY_NINE        => 15,

      COSID_UNIVISION_UVSNINETEEN   => 10,
      COSID_UNIVISION_TWENTY        => 10,
      COSID_UNIVISION_UVSTWENTYFOUR => 20,
      COSID_UNIVISION_UVSTWENTYNINE => 20,
      COSID_UNIVISION_THIRTY        => 20,
      COSID_UNIVISION_UVSTHIRTYFOUR => 20,
      COSID_UNIVISION_THIRTYFIVE    => 20,
      COSID_UNIVISION_UVSTHIRTYNINE => 20,
      COSID_UNIVISION_UVSFORTYFOUR  => 30,
      COSID_UNIVISION_FORTYFIVE     => 30,
      COSID_UNIVISION_FIFTY         => 30,
      COSID_UNIVISION_UVSFIFTYFOUR  => 30,
      COSID_UNIVISION_FIFTYFIVE     => 30
    ];

    // return dollars to cents
    // return 0 credit if customer cos_id is not mapped
    return (isset($map[$cos_id])) ? $map[$cos_id] * 100 : 0;
  }

  /**
   * adds roam_credit_promo parameter to makeitso params
   */
  public function addMakeitsoParam(&$params)
  {
    if ( ! $this->cos_id)
    {
      \logError('COS_ID was not provided to RoamCreditPromo');
      return;
    }

    $params[self::getParam()] = $this->determineRoamCreditAmount($this->cos_id);
  }

  /**
   * returns the value of roam crtedit promo param
   */
  public static function promoAmountFromParams($params)
  {
    return (isset($params[self::getParam()])) ? $params[self::getParam()] : 0;
  }

  /**
   * returns the roam credit promo param name
   */
  public static function getParam()
  {
    return 'roam_credit_promo';
  }
}
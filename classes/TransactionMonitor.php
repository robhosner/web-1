<?php

/** 
 * @author John Dohoney, Ultra Mobile
 * @see MVNO-1700, Sprint Aug 26, 2013
 * @abstract This class maintains a list of calls by IP address, this is used to throttle the 
 * calls to the server.  The business rule is, a partner is allowed to make <= 4 calls in a 60
 * second time window, from the current time.  If this threshold is exceeded, the Partner is marked 
 * with the current time, and the denied flag is set.
 * 
 */
class TransactionMonitor
{
    /**
     * 
     * @var string $referringIPAddress - Reffering IP address, obtained from $_SERVER['REMOTE_ADDR']
     */
    private  $referringIPAddress;
    
    /**
     * 
     * @var array $accessList - List of access time that make use of time() call
     */
    private  $accessList;
    
    /**
     * 
     * @var boolean $isDenied - Set when the throttling limit is exceeded
     */
    private  $isDenied;
    
    /**
     * 
     * @var int $isDeniedSince - time stamp of when the Denial was posted.  This can be lifter in AT LEAST 120s
     */
    private  $isDeniedSince;
    
    
    /**
     * Initializes all instance variables
     * 
     * @param string(IP Address) $callingServer
     */
    function __construct ($callingServer)
    {
       $this->referringIPAddress = $callingServer; // IP Address of the partner making the API call 
       $this->accessList = array();
       $this->isDenied = FALSE;
       $this->isDeniedSince = 0;    
    }

    /**
     */
    function __destruct ()
    {
        if( isset($this->accessList)) 
        {
            unset($this->accessList);
        }
    }
    
	/**
     * @return the $referringIPAddress
     */
    public function getReferringIPAddress ()
    {
        return $this->referringIPAddress;
    }

	/**
     * @return the $accessList
     */
    public function getAccessList ()
    {
        return $this->accessList;
    }

	/**
     * @return the $isDenied
     */
    public function getIsDenied ()
    {
        return $this->isDenied;
    }

	/**
     * @return the $isDeniedSince
     */
    public function getIsDeniedSince ()
    {
        return $this->isDeniedSince;
    }

	/**
     * @param string $referringIPAddress
     */
    public function setReferringIPAddress ($referringIPAddress)
    {
        $this->referringIPAddress = $referringIPAddress;
    }

	/**
     * @param int $accessTime -- The time to add to the access list
     */
    public function setAccessList ($accessTime)
    {
        $this->accessList[] = $accessTime;
    }

	/**
     * @param boolean $isDenied
     */
    protected function setIsDenied ($isDenied)
    {
        $this->isDenied = $isDenied;
    }

	/**
     * @param number $isDeniedSince
     */
    protected function setIsDeniedSince ($isDeniedSince)
    {
        $this->isDeniedSince = $isDeniedSince;
    }
    
    /**
     * Grooms the internal accessList of any entries > 70 seconds
     */
    private function groomAccessArray()
    {
        $now = time();
        for ($x = 0 ; $x < count($this->accessList) ; $x++ )
        {
            if( ( $now - $this->accessList[$x] ) > 70 )
            {
                unset( $this->accessList[$x] ); // delete the times > 70s
            }
        }       
        $this->accessList = array_values($this->accessList); // Fix up the the array from the deleted values.
    }

    /**
     * Throws Exception('Invalid Server Address') for $remoteServerAddress != the value of this object.
     * If the business rule is violated, they will be denied for 120 seconds and true returned; otherwise
     * false is returned.
     * 
     * @param string $remoteServerAddress
     */
    public function needsThrottling( $remoteServerAddress )
    {
        if( $remoteServerAddress == $this->referringIPAddress )
        {
           $this->groomAccessArray();  // remove old non-relevant API accesses.
           
           $now              = time();
           $callPendingCount = 0;
           
           for ($x = 0 ; $x < count($this->accessList) ; $x++ )
           {
               $diff = $now - $this->accessList[$x];
               if( ( $now - $this->accessList[$x] ) < 60 )
               {
                   $callPendingCount++;
               }
           }
           //
           // Now apply our business rule, the number of calls > 6 in the last 60 seconds
           // Deny them access for 120s (updated 9/5 per Riz to up count from 4 -> 6 for non-IVR)
           //
           if( $callPendingCount > 6 )
           {
               $this->isDenied = TRUE;
               $this->isDeniedSince = $now;
               return TRUE;
           } 
           else {
               return FALSE;
           }
        }
        throw new Exception('Invalid Server Address');
    }

    /**
     * Throws Exception('Invalid Server Address') for $remoteServerAddress != the value of this object.
     * ASSUMPTION:  the accessor getIsDenied() == TRUE, so the caller invokes this method to see if the 
     * throttling can be lifted.  If the throttle was set > 120 seconds ago, the internal object state will
     * remove the throttle, and return TRUE, if not, FALSE is returned.
     *
     * @param string $remoteServerAddress
     */
   public function canReleaseThrottle( $remoteServerAddress ) 
   {
       if( $remoteServerAddress == $this->referringIPAddress )
       {
           $this->groomAccessArray(); // remove old non-relevant API accesses.
           $now = time();
           
           if( ( $now - $this->isDeniedSince ) > 120 ) 
           {
             $this->isDenied      = FALSE;
             $this->isDeniedSince = 0;
             
             return TRUE;
           }
           else {
               return FALSE;
           }
       }
       throw new Exception('Invalid Server Address');
   }

}

?>
<?php

require_once 'Ultra/Lib/Util/Redis.php';

/**
 * A class for retrieving and parsing customer options in ULTRA.CUSTOMER_OPTIONS
 */
class CustomerOptions
{
  public $options = array();
  public static $redisKey = 'ultra/api/customer_options/cache/';

  public function __construct($customer_id, $cache = null)
  {
    $redis = ($cache) ? new \Ultra\Lib\Util\Redis() : null;

    if ($cache && $cachedOptions = $redis->get(CUSTOMER_OPTIONS_REDIS_KEY . $customer_id))
      $this->options = (array)json_decode($cachedOptions);
    else
    {
      $this->options = get_ultra_customer_options_by_customer_id_assoc($customer_id);
      
      if ($cache)
        $redis->set(CUSTOMER_OPTIONS_REDIS_KEY . $customer_id, json_encode($this->options), 10 * 60);
    }

    \logit('ultra_customer_options = ' . json_encode($this->options));
  }

  public static function delCache($customer_id)
  {
    $redis = new \Ultra\Lib\Util\Redis();
    $redis->del(CUSTOMER_OPTIONS_REDIS_KEY . $customer_id);
  }

  //
  public function getThrottleSpeed()
  {
    $throttleSpeeds = [
      'Full Speed'              => 'full',
      'Optimized - 1536 kbps'   => '1536',
      'Super Saver - 1024 kbps' => '1024'
    ];

    foreach ($this->options as $attribute => $value)
      if (strpos($attribute, 'LTE_SELECT') === 0)
        return $throttleSpeeds[$value];

    return $throttleSpeeds['Full Speed'];
  }

  //
  public function getE911Address()
  {
    return (isset($this->options['ACC.ADDRESS_E911']))
      ? $this->options['ACC.ADDRESS_E911']
      : NULL;
  }

  //
  public function getBoltOnsInfo($brandId)
  {
    $brand = \Ultra\UltraConfig\getShortNameFromBrandId($brandId);

    $products = [
      'MINT'      => ['MDATA'],
      'ULTRA'     => ['DATA','IDDCA','VOICE','GLOBE','SHAREDILD','SHAREDDATA'],
      'UNIVISION' => ['DATA','IDDCA','VOICE']
    ];

    $bolt_ons = array();

    $bolt_ons_definitions = \Ultra\UltraConfig\getBoltOnsMapping();

    foreach( $bolt_ons_definitions as $id => $data )
      // for ( $i = 0 ; $i < count($ultra_customer_options) ; $i = $i + 2 )
      foreach ($this->options as $attribute => $value)
      {
        // $attribute = $ultra_customer_options[ $i ] ;
        // $value     = $ultra_customer_options[ $i+1 ] ;

        if (
          $attribute == $data['option_attribute']
          && $value  == $data['cost']
          && in_array($data['product'], $products[$brand])
        )
        {
          $bolt_on = $data;
          $bolt_on['id'] = $id;
          $bolt_ons[] = $bolt_on;
        }
      }

    return $bolt_ons;
  }

  /**
   * takes an string option name or an array of option names to check against customers option
   * @param  String || Array $attributes
   * @return Bool
   */
  public function hasOption($attributes)
  {
    if ( ! is_array($attributes))
      $attributes = array($attributes);

    foreach ($attributes as $attribute)
    {
      if (isset($this->options[$attribute]))
        return TRUE;
    }

    return FALSE;
  }
 
  /**
   * parses bogo customer options and returns [ATTRIBUTE, VALUE]
   * array is empty if no bogo option is found
   * @return Array
   */
  public function bogoMonthInfo()
  {
    $index = false;
    $validOptions = \Ultra\UltraConfig\getValidBOGOTypes();
    
    foreach ($this->options as $attr => $val)
    {
      if (in_array($attr, $validOptions))
        return array($attr, $val);
    }

    return array();
  }

  /**
   * parses customer option BILLING_OPTION_PROMO_PLAN
   * {
   *    [plan] => L19
   *    [ultra_promotional_plans_id] => 123
   *    [months] => 10
   * }
   * returns NULL if customer option does not have
   * @return Array assoc || NULL
   */
  public function promoPlanInfo()
  {
    if ( ! isset($this->options[BILLING_OPTION_PROMO_PLAN]))
      return null;

    $values = explode(',', $this->options[BILLING_OPTION_PROMO_PLAN]);

    return array(
      'plan'                       => $values[0],
      'ultra_promotional_plans_id' => $values[1],
      'months'                     => $values[2]
    );
  }

  /**
   * searches for customer option BILLING.MRC_7_11
   * returns bool, if has
   * @return Bool
   */
  public function hasPromo711()
  {
    return ! ! isset($this->options[BILLING_OPTION_ATTRIBUTE_7_11]);
  }

  /**
   * parses customer option BILLING.MRC_MULTI_MONTH
   * {
   *    [ultra_plan] => M29
   *    [months_total] => 3
   *    [months_left] => 0
   * }
   * returns FALSE if does not have
   * @return Bool
   */
  public function multiMonthInfo()
  {
    if ( ! isset($this->options[BILLING_OPTION_MULTI_MONTH]))
      return FALSE;

    $values = explode(',', $this->options[BILLING_OPTION_MULTI_MONTH]);

    return array('ultra_plan' => $values[0], 'months_total' => $values[1], 'months_left' => $values[2]);
  }

  /**
   * searches for SUBPLAN customer options BRAND.UV.SUBPLAN*
   * returns subplan letter if exists, NULL if does not
   * @return Char || NULL
   */
  public function getBrandUVSubplan()
  {
    foreach ($this->options as $attr => $val)
    {
      if (strpos($attr, 'BRAND.UV.SUBPLAN') === 0)
        return substr($attr, -1);
    }

    return NULL;
  }
}

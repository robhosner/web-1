<?php

/**
 * Billing Runner
 * PHP application that handles ePay/Emida RTR
 *
 * See http://wiki.hometowntelecom.com:8090/display/SPEC/How+it+works+-+Payments+and+Billing+Runner
 * See http://wiki.hometowntelecom.com:8090/pages/viewpage.action?title=Commands+-+ExternalPayments&spaceKey=SPEC
 * See https://issues.hometowntelecom.com:8443/browse/MVNO-2508
 */

require_once 'db.php';
require_once 'Ultra/Lib/Billing/functions.php';
require_once 'Ultra/Lib/Util/Redis/Billing.php';


// how many seconds we sleep between one cycle and the next
define ('SLEEP_SECONDS'     , 5  );

// how many cycles we perform per thread
define ('MAX_CYCLES'        , 12 );

// for how many seconds should a thread run at most
define ('MAX_EXECUTION_TIME', 60);


// if a UUID is provided to the runner, process only one cycle for the given UUID
$uuid = NULL;
if ( isset( $argv[1] ) && $argv[1] )
  $uuid = $argv[1];


main( $uuid );


exit;


/**
 * main
 *
 * Main runner body
 *
 * @return NULL
 */
function main( $uuid=NULL )
{
  $timestampStart = time();
  $countCycles    = 0;
  $error          = FALSE;

  // connect to DB
  teldata_change_db();

  $redisBilling = new \Ultra\Lib\Util\Redis\Billing();

  // perform several cycles, until the limits are met or a fatal error occurs
  while( ! $error && continueExecution( $countCycles , $timestampStart ) )
  {
    if ( $countCycles > 0 )
      sleep(SLEEP_SECONDS);

    #dlog('',"Cycle # ".++$countCycles);

    if ( $uuid )
      $countCycles = MAX_CYCLES;

    $error = \Ultra\Lib\Billing\billingTask( $redisBilling , $uuid );
  }

  return NULL;
}

/**
 * continueExecution
 *
 * Returns TRUE if the runner should continue
 *
 * @return boolean
 */
function continueExecution( $countCycles , $timestampStart )
{
  if ( ( $timestampStart + MAX_EXECUTION_TIME ) <= time() )
  {
    dlog('',"Execution won't continue after ".MAX_EXECUTION_TIME." seconds.");

    return FALSE;
  }

  if ( $countCycles < MAX_CYCLES )
    return TRUE;

  #dlog('',"Cycles are over.");

  return FALSE;
}


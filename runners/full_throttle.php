<?php

/*
                                /T /I
                              / |/ | .-~/
                          T\ Y  I  |/  /  _
         /T               | \I  |  I  Y.-~/
        I l   /I       T\ |  |  l  |  T  /
     T\ |  \ Y l  /T   | \I  l   \ `  l Y
 __  | \l   \l  \I l __l  l   \   `  _. |
 \ ~-l  `\   `\  \  \ ~\  \   `. .-~   |
  \   ~-. "-.  `  \  ^._ ^. "-.  /  \   |
.--~-._  ~-  `  _  ~-_.-"-." ._ /._ ." ./
 >--.  ~-.   ._  ~>-"    "\   7   7   ]
^.___~"--._    ~-{  .-~ .  `\ Y . /    |
 <__ ~"-.  ~       /_/   \   \I  Y   : |
   ^-.__           ~(_/   \   >._:   | l______
       ^--.,___.-~"  /_/   !  `-.~"--l_ /     ~"-.
              (_/ .  ~(   /'     "~"--,Y   -=b-. _)
               (_/ .  \  :           / l      c"~o \
                \ /    `.    .     .^   \_.-~"~--.  )
                 (_/ .   `  /     /       !       )/
                  / / _.   '.   .':      /        '
                  ~(_/ .   /    _  `  .-<_
                    /_/ . ' .-~" `.  / \  \          ,z=.
                    ~( /   '  :   | K   "-.~-.______//
                      "-,.    l   I/ \_    __{--->._(==.
                       //(     \  <    ~"~"     //
                      /' /\     \  \     ,v=.  ((
                    .^. / /\     "  }__ //===-  `
                   / / ' '  "-.,__ {---(==-
                 .^ '       :  T  ~"   ll
                / .  .  . : | :!        \
               (_/  /   | | j-"          ~^
                 ~-<_(_.^-~"
 !
 H__________________________________
 H|* * * * * *|---------------------|
 H| * * * * * |---------------------|
 H|* * * * * *|---------------------|
 H| * * * * * |---------------------|
 H|---------------------------------|
 H|---------------------------------|
 H|---------------------------------|
 H|---------------------------------|
 H-----------------------------------
 H
 H
 H
 H
 H
 H
 H
 H
 H
*/

require_once 'db.php';
require_once 'lib/util-common.php';

define('FT_COMMIT', true);
define('FT_TABLE', 'ultra.full_throttle_migration ');
define('FT_COL_CUSTOMER_ID', 'customer_id');
define('FT_COL_PROCESSED', 'processed');
define('FT_COL_PROCESSED_DATE', 'processed_date');
define('FT_COL_OPTION', 'option_attribute');
define('FT_COL_VALUE', 'option_value');
define('FT_UPDATE_LIMIT', 500);
define('REDIS_PREFIX_KEY', 'full_throttle_update/customer_id/');

class FullThrottle
{
  private $args;
  private $commit           = FT_COMMIT;
  private $tblFullThrottle  = FT_TABLE;
  private $colCustomerId    = FT_COL_CUSTOMER_ID;
  private $colProcessed     = FT_COL_PROCESSED;
  private $colProcessedDate = FT_COL_PROCESSED_DATE;
  private $colOption        = FT_COL_OPTION;
  private $colValue         = FT_COL_VALUE;
  private $updateLimit      = FT_UPDATE_LIMIT;
  private $retryCustomerCount = [];

  /**
   * @var Redis
   */
  private $redis;

  public function __construct(Ultra\Lib\Util\Redis $redis)
  {
    \dlog('', 'WELCOME TO FULL THROTTLE');
    $this->redis = $redis;
  }

  public function run($_args = [])
  {
    $this->args = $_args;

    if (!empty($this->args[2]) && is_numeric($this->args[2])) {
      $this->updateLimit = $this->args[2];
    }

    try
    {
      // TODO improve precision
      $start = time();

      \dlog('', 'START %d', $start);

      teldata_change_db();
      $customers = $this->getCustomerList();
      foreach ($customers as $customer) {
        if (!$this->reserveCustomer($customer->customer_id)) {
          \dlog('', 'Skipping reserved customer_id = '. $customer->customer_id);
          continue;
        }

        if ($this->processCustomer($customer->customer_id)) {
          $this->markAsProcessed($customer);
        }
      }

      $end = time();

      \dlog('', 'END %d', $end);
      \dlog('', 'DURATION %d', $end - $start);
    }
    catch (\Exception $e)
    {
      \logError($e->getMessage());
    }
  }

  /* outputs the queries */
  public function testQueries()
  {
    echo PHP_EOL . 'getCustomerList' . PHP_EOL;
    echo $this->getCustomerListQuery();
    echo PHP_EOL;

    echo PHP_EOL . 'markAsProcessed' . PHP_EOL;
    echo $this->markAsProcessedQuery(11, 'LTE_SELECT.CARRIER_MIGRATED_RT_19', 'Optimized - 1536 kbps');
    echo PHP_EOL;
  }

  /* exit if mode not set to commit */
  private function committing($sql)
  {
    if ( ! $this->commit)
    {
      \logError("Was attempting to commit $sql");
      exit;
    }
  }

  /* if there is a first argument
     script will only process that customer_id
     should be digit
  */
  private function singleCustomer()
  {    
    $customer_id = isset($this->args[1]) ? $this->args[1] : false;

    if ($customer_id)
      \dlog('', "Only processing $customer_id");

    return $customer_id; 
  }

  /* returns [] of customer ids to be processed */
  private function getCustomerList()
  {
    try
    {
      if ($customer_id = $this->singleCustomer())
        return [
          'customer_id'     => $customer_id,
          'option_attrubte' => '',
          'option_value'    => ''
        ];

      $sql = $this->getCustomerListQuery();
      $this->committing($sql);
      $customers = mssql_fetch_all_objects(logged_mssql_query($sql));
      if (empty($customers))
        throw new \Exception('Failure getting customer list');

      shuffle($customers);
      return $customers;
    }
    catch (\Exception $e)
    {
      \logError($e->getMessage());
      return [];
    }
  }

  /* sets throttle speed to full for customer */
  private function processCustomer($customer_id)
  {
    $config = array(
      'command' => 'internal__UpdateThrottleSpeed',
      'version' => 2
    );

    $params = [
      'customer_id'    => $customer_id,
      'throttle_speed' => 'full',
      'channel'        => 'CARRIER_SELECTED_DEFAULT'
    ];

    // call internal__UpdateThrottleSpeed
    $result = $this->callUltraApi($config, $params);
//    print_r($result);

    if ($result === false) {
      if (isset($this->retryCustomerCount[$customer_id])) {
        $this->retryCustomerCount[$customer_id] += 1;
      } else {
        $this->retryCustomerCount[$customer_id] = 1;
      }

      if ($this->retryCustomerCount[$customer_id] < 4) {
        dlog('', "Retrying processing customer_id $customer_id");
        return $this->processCustomer($customer_id);
      }
    }

    if ($result === false || !$result->success)
    {
      $err = "There was a problem switching customer_id $customer_id to full";
      \logError($err);
      return false;
    }

    return $result->success;
  }

  /* mark customer as processed in database */
  private function markAsProcessed($customer)
  {
    $sql = $this->markAsProcessedQuery(
      $customer->customer_id,
      $customer->option_attribute,
      $customer->option_value
    );

    $this->committing($sql);
    if ( ! logged_mssql_query($sql))
      \logError("Failure marking $customer->customer_id as processed");

    dlog('', "Successfully processed customer_id $customer->customer_id");
  }

  /* calls an ultra api */
  function callUltraApi($config, $params)
  {
    list($domain, $username, $password) = \Ultra\UltraConfig\ultra_api_internal_info();

    $site['url'] = "https://$domain";
    $site['username'] = $username;
    $site['password'] = $password;

    // gather API environment configuration
    if ( ! $site)
      return logError('unable to get API site config');

    if (empty($config['partner']))
      list($partner) = explode('__', $config['command']);

    $options = array(
      'location' => "{$site['url']}/pr/$partner/{$config['version']}/ultra/api/{$config['command']}.json",
      'username' => empty($site['username']) ? NULL : $site['username'],
      'password' => empty($site['password']) ? NULL : $site['password']);

    // call API
    return callJsonApi($config['command'], $params, $options);
  }

  private function getCustomerListQuery()
  {
    return "SELECT TOP {$this->updateLimit}
            {$this->colCustomerId},
            {$this->colOption},
            {$this->colValue}
            FROM  {$this->tblFullThrottle}
            WHERE {$this->colProcessed} = 0
            ORDER BY {$this->colCustomerId} DESC";
  }

  private function markAsProcessedQuery($customer_id, $prev_option, $prev_value)
  {
    return "UPDATE {$this->tblFullThrottle}
            SET    {$this->colProcessed}     = 1,
                   {$this->colProcessedDate} = GETUTCDATE(),
                   {$this->colOption}        = '$prev_option',
                   {$this->colValue}         = '$prev_value'
            WHERE  {$this->colCustomerId}    = $customer_id";
  }

  protected function reserveCustomer($customerId)
  {
    $reserved = FALSE;

    $myPID = gethostname() . "-" . getmypid() . "-" . rand(1, 9999);

    $redisKey = REDIS_PREFIX_KEY . $customerId;

    $value = $this->redis->get($redisKey);

    if ( ! $value )
    {
      $this->redis->setnx( $redisKey , $myPID , 60*60 );

      $value = $this->redis->get($redisKey);

      $reserved = ! ! ( $value == $myPID );
    }

    if ($reserved) {
      dlog('','Process Id '.$myPID.' successfully reserved customer_id = "'. $customerId .'"');
    }

    return $reserved;
  }
}

/* ------------------ */
$x = new FullThrottle(new \Ultra\Lib\Util\Redis());
// $x->testQueries();
$x->run($argv);
/* ------------------ */


<?php

/**
 * SMS Amdocs Runner
 * PHP application that handles SOAP SendSMS calls to Amdocs
 *
 * Runner status:
 * %> ps aux | grep sms_acc_dispatcher.php | grep bash
 *
 * Redis list length:
 * %> llen SMS/LIST/OUTBOUND/SYNCH
 *
 * See MVNO-2573 and MVNO-2778
 * @author Raffaello Galli <rgalli@ultra.me>
 * @project SMS Delivery
 */

require_once 'db.php';
require_once("Ultra/Lib/MQ/ListEndPoint.php");
require_once 'Ultra/Lib/Util/Soap/SendSMSPayload.php'; // Generate SendSMS payload
require_once 'Ultra/Lib/Amdocs/Control/ControlSendSMS.php';

// how many seconds we sleep between one cycle and the next
define ('SLEEP_SECONDS'     , 2  );

// how many cycles we perform per thread
define ('MAX_CYCLES'        , 29 );

// for how many seconds should a thread run at most
define ('MAX_EXECUTION_TIME', 60 );


main( $argv );


exit;


/**
 * main
 *
 * Main runner body
 *
 * @return NULL
 */
function main( $argv )
{
  $timestampStart = time();
  $countCycles    = 0;
  $error          = FALSE;

  // connect to DB
  teldata_change_db();

  $listEndPoint = \Ultra\Lib\MQ\getSMSListEndPoint();

  $maxCycles = ( isset( $argv[1] ) && is_numeric($argv[1])      ) ? $argv[1] : MAX_CYCLES ;
  $verbose   = ( isset( $argv[2] ) && ( $argv[2] == 'verbose' ) ) ?     TRUE :      FALSE ;

  while( ! $error && continueExecution( $countCycles , $timestampStart , $maxCycles ) )
  {
    if ( $countCycles > 0 )
      sleep(SLEEP_SECONDS);

    dlog( ($verbose?'':'silent') , "Cycle # ".++$countCycles );

    list( $messageKey , $message ) = $listEndPoint->popMessage();

    if ( $messageKey )
    {
      dlog('',"messageKey = $messageKey ; message = $message");

      // send the message
      $error = dispatch( $listEndPoint , $messageKey , $message );
    }
  }

  dlog( ($verbose?'':'silent') , 'exit' );
}

/**
 * replyError
 *
 * Compose a reply message in case of error
 *
 * @return string
 */
function replyError( $listEndPoint , $header='ERROR' , $error )
{
  return $listEndPoint->buildMessage(
    array(
      'header'    => $header,
      'body'      => array(
        'success'   => FALSE,
        'errors'    => array( $error )
      ),
      'actionUUID' => getNewActionUUID('middleware ' . time())
    )
  );
}

/**
 * dispatchSendSMS
 *
 * Perform a SendSMS API call
 *
 * @return object of class \Result
 */
function dispatchSendSMS( $params , $actionUUID )
{
  dlog('', '(%s)', func_get_args());

  try
  {
    $controlSendSMSObject = new \Ultra\Lib\Amdocs\Control\ControlSendSMS( $actionUUID );

    $result = $controlSendSMSObject->call(
      array(
        'MSISDN'   => $params['msisdn'],
        'language' => 'en',
        'smsText'  => $params['sms_text']
      )
    );
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());

    $result = make_error_Result( $e->getMessage() );
  }

  return $result;
}

/**
 * dispatch
 *
 * Extract message and send it
 *
 * @return string
 */
function dispatch( $listEndPoint , $messageKey , $message )
{
  $error = NULL;

  if ( $message )
  {
    $data = $listEndPoint->extractFromMessage( $message );

    try
    {
      if ( $data )
      {
        dlog('',"data = %s",$data);

        // sanity check
        if ( ! is_object($data))
          throw new \Exception("Malformed message $message");

        if ( empty($data->header) )
          throw new \Exception("Missing header in message $message");

        // sanity check
        if ( empty($data->body) )
          throw new \Exception("Missing body in message $message");

        if ( ! is_object($data->body))
          throw new \Exception("Invalid body in message $message");

        // make sure we have an action UUID
        $actionUUID = ( empty( $data->_actionUUID ) ) ? 'test' : $data->_actionUUID ;

        // build dispatch function name using message header
        $dispatchFunction = 'dispatch'.$data->header;

        // sanity check
        if ( ! is_callable($dispatchFunction) )
          throw new \Exception( 'Header '.$data->header.' not handled' );

        // invoke SOAP call using dispatch function
        $result = $dispatchFunction( (array) $data->body , $actionUUID );

        dlog('',"%s result = %s",$dispatchFunction,$result);

        // timeout case
        if ( $result->is_timeout() )
          throw new \Exception( "{$data->header} timed out" );

        if ( $result->has_errors() )
        {
          $errors = $result->has_errors();

          dlog('',"errors = %s",$errors);

          throw new \Exception( $errors[0] );
        }

        if ( ! is_array($result->data_array) )
          throw new \Exception( "Missing data in $dispatchFunction response" );

        if ( ! count($result->data_array) )
          throw new \Exception( "No data in $dispatchFunction response" );

        if ( empty( $result->data_array['ResultCode'] ) )
          throw new \Exception( "Missing ResultCode in $dispatchFunction response" );

        $body = array(
          'success' => TRUE,
          'errors'  => array()
        );

        foreach( $result->data_array as $node => $value )
          $body[ $node ] = $value ;

        if ( $result->data_array['ResultCode'] != '100' )
        {
          $body['success'] = FALSE;
          $body['errors'][] = $result->data_array['ResultCode'] . ' - ' . $result->data_array['ResultMsg'];
        }

        // build reply message
        $reply = $listEndPoint->buildMessage(
          array(
            'header'    => $data->header,
            'body'      => $body,
            'actionUUID' => $data->_actionUUID
          )
        );
      }
      else
        throw new \Exception( "Could not extract any information from message $message" );
    }
    catch(\Exception $e)
    {
      dlog('', $e->getMessage());

      // build reply message
      $reply = $listEndPoint->buildReplyErrorMessage( 'ERROR' , $e->getMessage() );
    }

    // reply to the client
    $status = $listEndPoint->replyMessage( $messageKey , $reply );

    dlog('',"replyMessage returned status $status");
  }
  else
    dlog('',"No message associated with messageKey $messageKey");

  // returns only fatal errors
  return $error;
}

/**
 * continueExecution
 *
 * Returns TRUE if the runner should continue
 *
 * @return boolean
 */
function continueExecution( $countCycles , $timestampStart , $maxCycles )
{
  if ( ( $timestampStart + MAX_EXECUTION_TIME ) <= time() )
  {
    #dlog('',"Execution won't continue after ".MAX_EXECUTION_TIME." seconds.");

    return FALSE;
  }

  if ( $countCycles < $maxCycles )
    return TRUE;

  #dlog('',"Cycles are over.");

  return FALSE;
}


<?php

/**
 * Fix for customer's inconsistency
 * - same ICCID for multiple customers
 * - same MSISDN for multiple customers
 *
 * Usage:
 * %> php runners/clear_duplicate_customers.php $MAX_ITERATIONS $DAYS_AGO_LIMIT
 *
 * @author rgalli
 */


require_once 'db.php';
require_once 'lib/util-runners.php';


$dir  = '/tmp';
$name = 'clear_duplicate_customers';

dlog('',"clear_duplicate_customers start ".time());

// create our PID file
if ( ! createPidFile($dir, $name) )
{
  dlog('',"failed to create PID file");
  exit;
}

$max_iterations = 50;
if ( isset( $argv[1] ) && is_numeric($argv[1]) )
  $max_iterations = $argv[1];

$days_ago_limit = 24;
if ( isset( $argv[2] ) && is_numeric($argv[2]) )
  $days_ago_limit = $argv[2];

teldata_change_db();

clear_duplicate_customers_main( $max_iterations , $days_ago_limit );

unlinkPidFile($dir, $name);

dlog('',"clear_duplicate_customers end ".time());


exit;


/**
 * get_min_customer_id
 *
 * A lower limit to HTT_CUSTOMERS_OVERLAY_ULTRA.CUSTOMER_ID is needed in order to execute lighter queries
 *
 * @return integer
 */
function get_min_customer_id( $days_ago_limit )
{
  // max - ( days * 2k )
  $sql = 'SELECT MAX(CUSTOMER_ID) FROM HTT_CUSTOMERS_OVERLAY_ULTRA';

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
    return ( $query_result[0][0] - ( $days_ago_limit * 2750 ) );
  else
    return 0;
}

function duplicate_msisdns_query( $max_iterations , $min_customer_id )
{
  return sprintf("
    SELECT TOP %d CURRENT_MOBILE_NUMBER, COUNT(*)
    FROM   HTT_CUSTOMERS_OVERLAY_ULTRA o
    WHERE  CURRENT_MOBILE_NUMBER IS NOT NULL
    AND    CURRENT_MOBILE_NUMBER != ''
    AND    CURRENT_MOBILE_NUMBER > '0'
    AND    o.CUSTOMER_ID > %s
    GROUP BY CURRENT_MOBILE_NUMBER
    HAVING   COUNT(*) > 1
    ORDER BY CURRENT_MOBILE_NUMBER",
    $max_iterations,
    $min_customer_id
  );
}

/**
 * duplicate_iccids_query
 *
 * SQL query which returns all duplicate ICCIDs we should fix
 *
 * @return string
 */
function duplicate_iccids_query( $max_iterations , $min_customer_id )
{
  return sprintf("
    SELECT TOP %d CURRENT_ICCID_FULL, COUNT(*)
    FROM   HTT_CUSTOMERS_OVERLAY_ULTRA o WITH (NOLOCK)
    WHERE  CURRENT_ICCID_FULL IS NOT NULL
    AND    CURRENT_ICCID_FULL != ''
    AND    CURRENT_ICCID_FULL > '0'
    AND    o.CUSTOMER_ID > %s
    GROUP BY CURRENT_ICCID_FULL
    HAVING   COUNT(*) > 1
    ORDER BY CURRENT_ICCID_FULL",
    $max_iterations,
    $min_customer_id
  );
}

/**
 * clear_duplicate_customers_main
 *
 * Main script function
 *
 * @return NULL
 */
function clear_duplicate_customers_main( $max_iterations , $days_ago_limit )
{
  $min_customer_id = get_min_customer_id( $days_ago_limit );

  if ( ! $min_customer_id )
  {
    dlog('',"DB error");

    return NULL;
  }

  dlog('',"max_iterations = $max_iterations ; days_ago_limit = $days_ago_limit ; min_customer_id = $min_customer_id");

  // fix duplicate ICCIDs

  $sql = duplicate_iccids_query( $max_iterations , $min_customer_id );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
    fix_duplicate_iccids( $query_result );
  else
    dlog('',"No duplicate ICCIDs found");

  // fix duplicate MSISDNs

  $sql = duplicate_msisdns_query( $max_iterations , $min_customer_id - ( $days_ago_limit * 50000 ) );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
    fix_duplicate_msisdns( $query_result );
  else
    dlog('',"No duplicate MSISDNs found");

  return NULL;
}

function fix_duplicate_msisdns( $data )
{
  dlog('',"data = %s",$data);

  // get all customers with the same CURRENT_MOBILE_NUMBER as $data[0]
  $sql = \Ultra\Lib\DB\makeSelectQuery(
    'HTT_CUSTOMERS_OVERLAY_ULTRA',
    NULL,
    array('CUSTOMER_ID', 'CURRENT_MOBILE_NUMBER', 'CURRENT_ICCID_FULL', 'PLAN_STATE'),
    array('CURRENT_MOBILE_NUMBER' => $data[0])
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $result && is_array($result) && count($result) )
  {
    foreach( $result as $row )
    {
      dlog('',"row = %s",$row);

      fix_inactive_iccid( $row->CUSTOMER_ID , $row->CURRENT_MOBILE_NUMBER , $row->CURRENT_ICCID_FULL , $row->PLAN_STATE );
    }
  }
}

/**
 * fix_inactive_iccid
 *
 * Do a QuerySubscriber - if error, cancel customer
 *
 * @return NULL
 */
function fix_inactive_iccid( $customer_id , $msisdn , $iccid , $plan_state )
{
  $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

  $result = $mwControl->mwQuerySubscriber(
    array(
      'actionUUID' => time(),
      'iccid'      => $iccid
    )
  );

  dlog('',"mwQuerySubscriber data = %s",$result->data_array);

  if ( $result->is_failure() )
    dlog('',"mwQuerySubscriber failed");
  elseif( !empty($result->data_array['ResultCode']) && !empty($result->data_array['ResultMsg']) )
  {
    dlog('',"ResultCode = %s",$result->data_array['ResultCode']);
    dlog('',"ResultMsg  = %s",$result->data_array['ResultMsg']);

    if ( ( $result->data_array['ResultCode'] == '200' )
      && ( $result->data_array['ResultMsg']  == 'Invalid ICCID' )
    )
      delete_duplicate( $customer_id , $plan_state , $iccid , $msisdn );
    else
      dlog('',"case currently not handled (6)");
  }
}

/**
 * fix_duplicate_iccids
 *
 * Fix customers with duplicate ICCIDs
 *
 * @return NULL
 */
function fix_duplicate_iccids( $data )
{
  foreach( $data as $row )
    fix_duplicate_iccid( $row );
}

/**
 * fix_duplicate_iccid
 *
 * Fix ICCID
 *
 * @return NULL
 */
function fix_duplicate_iccid( $row )
{
  dlog('',"Found ".$row[1]." customers with ICCID ".$row[0]);

  if ( ! $row[0] || ! is_numeric($row[0]) )
  {
    dlog('',"ICCID ".$row[0]." is invalid");

    return NULL;
  }

  // get all customers with the same CURRENT_ICCID_FULL $row[0]
  $sql = \Ultra\Lib\DB\makeSelectQuery(
    'HTT_CUSTOMERS_OVERLAY_ULTRA',
    NULL,
    array('CUSTOMER_ID', 'CURRENT_MOBILE_NUMBER', 'CURRENT_ICCID_FULL', 'PLAN_STATE'),
    array('CURRENT_ICCID_FULL' => $row[0])
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $result && is_array($result) && count($result) )
  {
    dlog('',"data = %s",$result);

    fix_logic( $result , $row[0] );
  }

}

function fix_logic( $result , $iccid )
{
  $plan_state_count = compute_plan_state_count( $result );

  dlog('',"plan_state_count = %s",$plan_state_count);

  if ( ( $plan_state_count[ STATE_ACTIVE ]            == count($result) )
    || ( $plan_state_count[ STATE_NEUTRAL ]           == count($result) )
    || ( $plan_state_count[ STATE_PROVISIONED ]       == count($result) )
    || ( $plan_state_count[ STATE_PORT_IN_REQUESTED ] == count($result) )
    || ( $plan_state_count[ STATE_PORT_IN_DENIED ]    == count($result) )
  )
  {
    // all those customers are in the same state

    clear_duplicate_same_state( $result , $iccid , $plan_state_count );
  }
  elseif ( ( ( $result[0]->PLAN_STATE == STATE_NEUTRAL           ) && ( $result[1]->PLAN_STATE == STATE_ACTIVE            ) )
        || ( ( $result[0]->PLAN_STATE == STATE_NEUTRAL           ) && ( $result[1]->PLAN_STATE == STATE_PORT_IN_REQUESTED ) )
        || ( ( $result[0]->PLAN_STATE == STATE_NEUTRAL           ) && ( $result[1]->PLAN_STATE == STATE_PROVISIONED       ) )
        || ( ( $result[0]->PLAN_STATE == STATE_NEUTRAL           ) && ( $result[1]->PLAN_STATE == STATE_SUSPENDED         ) )
        || ( ( $result[0]->PLAN_STATE == STATE_NEUTRAL           ) && ( ( count($result) > 3 ) && ( $result[3]->PLAN_STATE == STATE_ACTIVE            ) ) )
        || ( ( $result[0]->PLAN_STATE == STATE_NEUTRAL           ) && ( ( count($result) > 2 ) && ( $result[2]->PLAN_STATE == STATE_ACTIVE            ) ) )
        || ( ( $result[0]->PLAN_STATE == STATE_NEUTRAL           ) && ( ( count($result) > 2 ) && ( $result[2]->PLAN_STATE == STATE_PORT_IN_REQUESTED ) ) )
        || ( ( $result[0]->PLAN_STATE == STATE_NEUTRAL           ) && ( ( count($result) > 2 ) && ( $result[2]->PLAN_STATE == STATE_PROVISIONED       ) ) )
        || ( ( $result[0]->PLAN_STATE == STATE_PORT_IN_REQUESTED ) && ( $result[1]->PLAN_STATE == STATE_PROVISIONED       ) )
        || ( ( $result[0]->PLAN_STATE == STATE_PORT_IN_REQUESTED ) && ( $result[1]->PLAN_STATE == STATE_ACTIVE            ) )
        || ( ( $result[0]->PLAN_STATE == STATE_PORT_IN_DENIED    ) && ( $result[1]->PLAN_STATE == STATE_ACTIVE            ) )
        || ( ( $result[0]->PLAN_STATE == STATE_PROVISIONED       ) && ( $result[1]->PLAN_STATE == STATE_ACTIVE            ) ) )
  {
    if (delete_duplicate( $result[0]->CUSTOMER_ID , $result[0]->PLAN_STATE , $result[0]->CURRENT_ICCID_FULL , $result[0]->CURRENT_MOBILE_NUMBER ))
      assign_customer_id_to_iccid($result[1]->CUSTOMER_ID, $iccid);
  }
  elseif ( ( ( $result[1]->PLAN_STATE == STATE_NEUTRAL     ) && ( $result[0]->PLAN_STATE == STATE_ACTIVE      ) )
        || ( ( $result[1]->PLAN_STATE == STATE_PORT_IN_REQUESTED ) && ( $result[0]->PLAN_STATE == STATE_ACTIVE ) )
        || ( ( $result[1]->PLAN_STATE == STATE_PROVISIONED ) && ( $result[0]->PLAN_STATE == STATE_ACTIVE      ) )
        || ( ( $result[1]->PLAN_STATE == STATE_NEUTRAL     ) && ( ( count($result) > 2 ) && ( $result[2]->PLAN_STATE == STATE_ACTIVE ) ) )
        || ( ( $result[1]->PLAN_STATE == STATE_PROVISIONED ) && ( ( count($result) > 2 ) && ( $result[2]->PLAN_STATE == STATE_ACTIVE ) ) )
        || ( ( $result[1]->PLAN_STATE == STATE_NEUTRAL     ) && ( $result[0]->PLAN_STATE == STATE_SUSPENDED   ) )
        || ( ( $result[1]->PLAN_STATE == STATE_NEUTRAL     ) && ( $result[0]->PLAN_STATE == STATE_PROVISIONED ) ) )
  {
    if (delete_duplicate( $result[1]->CUSTOMER_ID , $result[1]->PLAN_STATE , $result[1]->CURRENT_ICCID_FULL , $result[1]->CURRENT_MOBILE_NUMBER ))
      assign_customer_id_to_iccid($result[0]->CUSTOMER_ID, $iccid);
  }
  elseif ( ( $result[0]->PLAN_STATE == STATE_NEUTRAL ) && ( $result[1]->PLAN_STATE == STATE_NEUTRAL ) )
  {
    delete_duplicate( $result[0]->CUSTOMER_ID , $result[0]->PLAN_STATE , $result[0]->CURRENT_ICCID_FULL , $result[0]->CURRENT_MOBILE_NUMBER );
  }
  else
    dlog('',"case currently not handled (1)");
}

/**
 * assign_htt_inventory_sim 
 * assigns $customer_id to $iccid
 * 
 * @param  $customer_id
 * @param  $iccid      
 * @return             
 */
function assign_customer_id_to_iccid($customer_id, $iccid)
{
  dlog('',"Updating htt_inventory_sim $customer_id ( ICCID = $iccid )");

  $sql = htt_inventory_sim_update_query(array(
    'customer_id'       => $customer_id,
    'last_changed_date' => 'GETUTCDATE()',
    'last_changed_by'   => 'fix_duplicate_customers.php',
    'batch_sim_set'     => array($iccid)
  ));
    
  if (is_mssql_successful(logged_mssql_query($sql)))
  {
    dlog('',"htt_inventory_sim update succeeded");
  }
  else
    dlog('',"htt_inventory_sim update failed");
}

/**
 * delete_duplicate
 *
 * Delete a customer and save the appropriate info in HTT_CANCELLATION_REASONS
 *
 * @return NULL
 */
function delete_duplicate( $customer_id , $plan_state , $iccid , $msisdn)
{
  dlog('',"Deleting customer_id $customer_id ( ICCID = $iccid ; MSISDN = $msisdn )");

  $sql = htt_cancellation_reasons_insert_query(
    array(
      'customer_id'   => $customer_id,
      'reason'        => 'duplicate bug',
      'type'          => 'VOID',
      'status'        => $plan_state,
      'agent'         => 'fix_duplicate_customers.php',
      'ever_active'   => 0
    )
  );

  if ( is_mssql_successful(logged_mssql_query($sql) ) )
    dlog('',"insert into htt_cancellation_reasons succeeded");
  else
  {
    dlog('',"insert into htt_cancellation_reasons failed");
    return FALSE;
  }

  $sql = sprintf(
    "UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA
     SET PLAN_STATE = 'Cancelled' , CURRENT_ICCID = NULL , CURRENT_ICCID_FULL = NULL , ACTIVATION_ICCID = NULL , ACTIVATION_ICCID_FULL = NULL , CURRENT_MOBILE_NUMBER = NULL
     WHERE CUSTOMER_ID = %d",
    $customer_id
  );

  if ( is_mssql_successful(logged_mssql_query($sql) ) )
  {
    dlog('',"customer deletion succeeded");
    return TRUE;
  }
  else
  {
    dlog('',"customer deletion failed");
    return FALSE;
  }
}

/**
 * clear_duplicate_same_state
 *
 * Multiple customer are in the same state and have the same ICCID assigned. We keep the one we found in HTT_INVENTORY_SIM.CUSTOMER_ID
 *
 * @return NULL
 */
function clear_duplicate_same_state( $customers , $iccid , $plan_state_count )
{
  // get info from HTT_INVENTORY_SIM
  $sql = \Ultra\Lib\DB\makeSelectQuery(
    'HTT_INVENTORY_SIM',
    NULL,
    array( 'CUSTOMER_ID' ),
    array( 'ICCID_FULL' => $iccid )
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $result && is_array($result) && count($result) )
  {
    dlog('',"data = %s",$result);

    if ( $result[0]->CUSTOMER_ID && is_numeric( $result[0]->CUSTOMER_ID ) )
    {
      dlog('',"ICCID $iccid belongs to customer id ".$result[0]->CUSTOMER_ID);

      // the one we want to keep is HTT_INVENTORY_SIM.CUSTOMER_ID

      foreach( $customers as $data )
      {
        if ( $data->CUSTOMER_ID != $result[0]->CUSTOMER_ID )
          delete_duplicate( $data->CUSTOMER_ID , $data->PLAN_STATE , $data->CURRENT_ICCID_FULL , $data->CURRENT_MOBILE_NUMBER );
        else
        {
          dlog('',"Keeping customer id ".$data->CUSTOMER_ID);
          assign_customer_id_to_iccid($data->CUSTOMER_ID, $iccid);
        }
      }
    }
    else
    {
      dlog('',"ICCID $iccid is not assigned to any customer");

      #if ( ( $plan_state_count['Neutral'] == count($customers) ) || ( $plan_state_count['Port-In Denied'] == count($$customers) ) )
      if ( $plan_state_count[ STATE_NEUTRAL ] == count($customers) )
      {
        // keep only the last one
        if (delete_duplicate( $customers[0]->CUSTOMER_ID , $customers[0]->PLAN_STATE , $customers[0]->CURRENT_ICCID_FULL , $customers[0]->CURRENT_MOBILE_NUMBER ))
          assign_customer_id_to_iccid($customers[1]->CUSTOMER_ID, $iccid);
      }
      elseif ( $plan_state_count[ STATE_PORT_IN_REQUESTED ] == count($customers) )
      {
        clear_duplicate_port_in( $customers , $iccid );
      }
      else
        dlog('',"case currently not handled (2)");
    }
  }
  else
  {
    dlog('',"ICCID not found in HTT_INVENTORY_SIM");
    dlog('',"case currently not handled (3)");
  }
}

/**
 * clear_duplicate_port_in
 *
 * Check ULTRA_ACC..PORTIN_QUEUE 
 *
 * @return NULL
 */
function clear_duplicate_port_in( $customers , $iccid )
{
  $customer_ids    = array();
  $customers_by_id = array();

  foreach( $customers as $customer )
  {
    $customer_ids[] = $customer->CUSTOMER_ID;
    $customers_by_id[ $customer->CUSTOMER_ID ] = $customer;
  }

  // connect to ULTRA_ACC DB
  \Ultra\Lib\DB\ultra_acc_connect();

  $table = 'PORTIN_QUEUE';

  // make select query
  $sql = \Ultra\Lib\DB\makeSelectQuery(
    $table,  // table
    NULL,    // limit
    array( ' * ' ), // attributes
    array( 'CUSTOMER_ID IN ('.implode(' , ',$customer_ids).')' ), // where clause
    NULL,    // having clause
    array('PORTIN_QUEUE_ID') // order by clause
  );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  dlog('',"data = %s",$data);

  teldata_change_db();

  if ( count( $data ) > 1 )
  {
    if ( ( $data[0]->PORT_STATUS = 'CANCELLED' ) && ( $data[1]->PORT_STATUS = 'ERROR' ) )
    {
      dlog('',"keeping  customer id ".$data[1]->CUSTOMER_ID);
      dlog('',"deleting customer id ".$data[0]->CUSTOMER_ID);
      dlog('',"%s",$customers_by_id[ $data[0]->CUSTOMER_ID ]);
      if (delete_duplicate(
        $data[0]->CUSTOMER_ID ,
        $customers_by_id[ $data[0]->CUSTOMER_ID ]->PLAN_STATE ,
        $customers_by_id[ $data[0]->CUSTOMER_ID ]->CURRENT_ICCID_FULL ,
        $customers_by_id[ $data[0]->CUSTOMER_ID ]->CURRENT_MOBILE_NUMBER
      ))
      {
        assign_customer_id_to_iccid($data[1]->CUSTOMER_ID, $iccid);
      }
    }
    else
      dlog('',"case currently not handled (5)");
  }
  else
    dlog('',"case currently not handled (4)");

}

/**
 * compute_plan_state_count
 *
 * Compute a statistics for plan states
 *
 * @return array
 */
function compute_plan_state_count( $result )
{
  $plan_state_count = array(
    STATE_ACTIVE            => 0,
    STATE_CANCELLED         => 0,
    STATE_NEUTRAL           => 0,
    STATE_PORT_IN_DENIED    => 0,
    STATE_PORT_IN_REQUESTED => 0,
    STATE_PRE_FUNDED        => 0,
    STATE_PROMO_UNUSED      => 0,
    STATE_PROVISIONED       => 0,
    STATE_SUSPENDED         => 0
  );

  // loop through duplicate customers
  foreach( $result as $data )
  {
    if ( ! isset($plan_state_count[ $data->PLAN_STATE ]) )
      $plan_state_count[ $data->PLAN_STATE ] = 1;
    else
      $plan_state_count[ $data->PLAN_STATE ]++;
  }

  return $plan_state_count;
}

?>

#!/usr/bin/perl

use warnings;
use strict;

# needed modules
use CGI;
use Data::Dumper;
use XML::Compile::WSDL11;
use XML::Compile::SOAP11;
use XML::Compile::SOAP::Trace;
use XML::Compile::Transport::SOAPHTTP;
use FindBin;
use lib "$FindBin::Bin/";

# compile WSDL
my $wsdl = XML::Compile::WSDL11->new("$FindBin::Bin/MVNOWSAPI-20121012/MVNOWSAPIService.wsdl");
$wsdl->importDefinitions("$FindBin::Bin/MVNOWSAPI-20121012/xsd0.xsd");
$wsdl->importDefinitions("$FindBin::Bin/MVNOWSAPI-20121012/xsd1.xsd");
$wsdl->importDefinitions("$FindBin::Bin/MVNOWSAPI-20121012/xsd2.xsd");
$wsdl->importDefinitions("$FindBin::Bin/MVNOWSAPI-20121012/xsd3.xsd");
$wsdl->compileCalls;


my $cgi = new CGI;
# $command = $cgi->param('command');
# $msisdn  = $cgi->param('msisdn');
# $iccid   = $cgi->param('iccid');
# $service = $cgi->param('service');


my $command = 'ResumeMSISDN';
my $params  =
{
  msisdn => '15189860718',
  #iccid  => '',
};

$wsdl->compileClient( $command );

my ($answer, $trace) = $wsdl->call( $command => { parameters => $params } );

print $cgi->header;

print <<END;
<title>Thank you!</title>
<h1>Thank you!</h1>
<p>bye</p>
END
    exit;

#print STDOUT "\n\nANSWER:\n";
#print Dumper($answer);
#print STDOUT "\n\nTRACE:\n";
#print Dumper($trace);

#print STDOUT "\n\nTRACE request:\n";
#print Dumper($trace->request->content);

#print STDOUT "\n\nTRACE response:\n";
#print Dumper($trace->response->content);

__END__

SOAP calls via CGI


<?php

/**
 * runners/resolve_pending_transitions
 * 
 * Purpose: resolves pending transitions
 */

require_once 'db.php';

$customer_ids = ( isset($argv[1]) ) ? $argv[1] : NULL ;
$environments = ( isset($argv[2]) ) ? $argv[2] : NULL ;

if (!is_null($customer_ids) && !is_null($environments))
{
  teldata_change_db();

  $customers_ids = explode(',', $customer_ids);

  foreach ($customers_ids as $customer_id)
  {
    $params = array( 'customer_id' => $customer_id, 'environments' => $environments );
    $result = internal_func_resolve_pending_transitions($params);

    if (!$result['success'])
    {
      if (isset($result['errors']))
        dlog('', 'ERROR: internal_func_resolve_pending_transitions - %s', $result['errors']);
    }
  }
}
else
  dlog('', 'customer_id or environments parameter is null');

<?php

/*
Purpose: Tokenizer runner

How to test:
a) enqueue a sale
%> php Ultra/Lib/CC/Queue_test.php enqueueCharge
b) activate a runner thread
%> php runners/cc_processor.php
*/

require_once 'db.php';
require_once 'Ultra/Lib/CC/Queue.php';

$cc_transactions_id = ( isset($argv[1]) ) ? $argv[1] : NULL ;

$tokenizerQueueObject = new \Ultra\Lib\CC\Queue();

teldata_change_db();

$result = $tokenizerQueueObject->processNext( $cc_transactions_id );

?>

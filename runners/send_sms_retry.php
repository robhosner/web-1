<?php


/**
 * AMDOCS-275
 *
 * runner to retry SendSMS
 *
 * @example php runners/send_sms_retry.php $MAX_ATTEMPT
 * @params int MAX_ATTEMPT, default is 1
 */


require_once 'db.php';
require_once 'Ultra/Lib/Util/Redis/SendSMS.php';


// defaults
define('MAX_ATTEMPTS', 1   ); // maximum retry attempt
define('MAX_SECONDS' , 1800); // maximum seconds execution time ( 30 minutes )
define('MINUTES_BEFORE_DISCARD',   43200); // discard after MINUTES_BEFORE_DISCARD minutes ( 12 hours )
define('MINUTES_AFTER_FIRST_RETRY', 1800); // retry only if the timestamp is older than MINUTES_AFTER_FIRST_RETRY seconds ( 30 minutes )

$time_start = time();

$count     = 0; // attempts count
$discarded = 0; // discarded count

$timestamp = '1'; // dummy value

$min_timestamp = time() - MINUTES_BEFORE_DISCARD;    // discard after MINUTES_BEFORE_DISCARD minutes
$max_timestamp = time() - MINUTES_AFTER_FIRST_RETRY; // retry only if the timestamp is older than MINUTES_AFTER_FIRST_RETRY

$max_attempts = empty($argv[1]) ? MAX_ATTEMPTS : $argv[1]; // attempts count limit

// main loop
while( ( (time()-$time_start) < MAX_SECONDS ) // time limit
    && $timestamp                             // priority queue not empty
    && ( $count < $max_attempts ) )           // attempts count limit
{
  // check if the priority queue is not empty
  list( $timestamp , $retry_data ) = \Ultra\Lib\Util\Redis\SendSMS\peekNext();

  if ( ! is_null($timestamp) )
  {
    // priority queue is not empty

    if ( $timestamp < $max_timestamp )
    {
      // extract next element from priority queue
      list( $timestamp , $retry_data ) = \Ultra\Lib\Util\Redis\SendSMS\dequeueNext();

      if ( $timestamp > $min_timestamp )
      {
        $count++;

        if ( $retry_data )
          $success = retrySendSMS( $retry_data );
        else
          dlog('',"retry_data missing");
      }
      else
      {
        // discard element because it's too old

        dlog('',"discarded element dated %s : %s",timestamp_to_date($timestamp),$retry_data);

        $discarded++;
      }
    }
    else
    {
      // skipped because it's too early for a retry

      dlog('',"Retry skipped, we must wait. First attempt date is %s , it will be retried after %s",timestamp_to_date($timestamp),timestamp_to_date($timestamp+MINUTES_AFTER_FIRST_RETRY));

      $timestamp = 0; // end while loop
    }
  }
}

// attempt another SendSMS call
function retrySendSMS( $retry_data )
{
  dlog('', "(%s)", func_get_args());

  // extract data object from json encoded $retry_data
  $data = json_decode( $retry_data );
  
  // AMDOCS-485: validate retry_data
  if (strlen($data->MSISDN) != 10)
  {
    dlog('', "Invalid MSISDN {$data->MSISDN}, aborting");
    return FALSE;
  }

  $result = mvneBypassListSynchronousSendSMS($data->smsText, $data->MSISDN, getNewActionUUID('send_sms_retry ' . time()));

  dlog('',"mvneBypassListSynchronousSendSMS = %s",$result);

  // all good
  if ( $result->is_success() )
    return TRUE;

  // something went wrong
  dlog('',"SendSMS failed for %s : %s",$data->MSISDN, $result->get_errors());

  return FALSE;
}

dlog('', "END : $discarded discarded , $count retried , time elapsed = ".(time()-$time_start).' seconds');

exit(0);

?>

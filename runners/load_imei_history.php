<?php

// USAGE: load_imei_history.php [$ACTIVATION_START_DATE] [$MAX_SUBSCRIBERS]
// e.g.: %> php runners/load_imei_history.php 2015-02-06 300

include_once 'db.php';
require_once 'Ultra/Lib/Util/Redis.php';

// defaults
define('ACTIVATION_RANGE', 14); // how may days of activations unless argv[1] is given
define('MAX_SUBSCRIBERS', 200); // max number of sucribers unless argv[2] is given
define('REDIS_KEY_PREFIX', 'ultra/subscriber/msisdn/imei/'); // where we store MSISDN-IMEI association
define('REDIS_IMEI_TTL', 604800); // time to keep in redis, secs (7 days)

dlog('',"start : %s",$argv);

// init
$start = time();
$subscribers = 0; // activated subscribers
$accQueries = 0; // queried MVNE2 subscribers
$failures = 0; // number of failed enabler calls
$start = time(); // start execution time
$redis = new \Ultra\Lib\Util\Redis;
$debug = FALSE; // development debug flag

// prepare query parameters

$top = (empty($argv[2]) || ! is_numeric($argv[2]) ? MAX_SUBSCRIBERS : $argv[2]);

$date_clause = null;

if (!empty($argv[1]) && date_to_datetime($argv[1])) {
  $date_clause = "@DATE = '" . date_to_datetime($argv[1]) . "', ";
}

// get MSISDN and MVNE of recently activated subscribers with missing IMEI
$sql = sprintf("EXEC [ULTRA].[GetGrossAddMSISDN] $date_clause @TOP = %d", $top);

teldata_change_db();

$rows = mssql_fetch_all_objects(logged_mssql_query($sql));
if ( ! $rows || ! count($rows) )
{
  dlog('', 'ERROR: no activated subscribers found.');
  exit;
}

// decrease probability of collisions
shuffle($rows);

foreach($rows as $row)
{
  // check if this MSISDN record is already in redis
  $subscribers++;
  $msisdn = normalize_msisdn($row->MSISDN, TRUE);
  $imei = NULL;
  $key = REDIS_KEY_PREFIX . $msisdn;
  dlog('', "Processing MSISDN $msisdn on MVNE {$row->MVNE}");
  if (! $redis->get($key))
  {
    // redis record not found: query appropriate enabler
    $accQueries++;
    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;
    $result = $mwControl->mwGetNetworkDetails(
      array(
        'actionUUID' => getNewActionUUID('load_imei_history ' . time()),
        'iccid'      => $row->ICCID_FULL
      )
    );

    // check result
    if ($result->is_failure() || empty($result->data_array) || ! $result->data_array['success'])
      dlog('', "WARNING: failed to query MSISDN $msisdn on ACC");
    elseif (empty($result->data_array['body']) || empty($result->data_array['body']->ResultCode) || $result->data_array['body']->ResultCode != '100')
      dlog('', "WARNING: ACC returned invalid result code for MSISDN $msisdn");
    elseif (empty($result->data_array['body']->IMEI) || strlen($result->data_array['body']->IMEI) < 10)
      dlog('', "WARNING: ACC returned invalid IMEI for MSISDN $msisdn" . (! empty($result->data_array['body']->MSStatus) ? '; ' . $result->data_array['body']->MSStatus : NULL));
    else
       $imei = $result->data_array['body']->IMEI;

    if ($debug)
      dlog('', 'result: ' . print_r($result, TRUE));
    
    if ($imei)
    {
      $redis->set($key, $imei, REDIS_IMEI_TTL);
      dlog('', "INFO: got IMEI $imei for MSISDN $msisdn");
    }
    else
      $failures++;
  }
  else
    if ($debug)
      dlog('', "MSISDN {$row->MSISDN} found in redis, skipping");
}

dlog('', 'DONE. Runtime: ' . (time() - $start) . " sec, subscribers: $subscribers, ACC queries: $accQueries, failures: $failures");


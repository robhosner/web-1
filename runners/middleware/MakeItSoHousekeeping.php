<?php

/*
Identical to middleware__MakeItSoHousekeeping

Purpose: invokes CommandInvocation::housekeeping for needed cleanups
*/

require_once 'db.php';
require_once 'Ultra/Lib/MVNE/MakeItSo.php';

$result = \Ultra\Lib\MVNE\MakeItSo\makeitso_housekeeping();

if ( $result->is_failure() )
  dlog('',"errors = %s",$result->get_errors());

// every 5 minutes
$date   = new DateTime();
$minute = $date->format('i');
if ( ! ( $minute % 5 ) )
  \command_invocations_initiated_cleanup();


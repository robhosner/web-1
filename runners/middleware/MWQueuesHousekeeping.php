<?php

/*
Purpose: cleanup inbound MW queues from lost messages
ACC_MW/INBOUND/SYNCH
ULTRA_MW/INBOUND/SYNCH
*/

require_once 'db.php';
require_once 'Ultra/Lib/MQ/ControlChannel.php';
require_once 'Ultra/Lib/Util/Redis.php';

$controlChannel = new \Ultra\Lib\MQ\ControlChannel;
$redis          = new \Ultra\Lib\Util\Redis();

cleanup_inbound( $controlChannel->inboundACCMWControlChannel()   , $redis , $controlChannel );
cleanup_inbound( $controlChannel->inboundUltraMWControlChannel() , $redis , $controlChannel );

/**
 * cleanup_inbound
 *
 * Attempt to cleanup lost messages in $messageQueue
 *
 * @return NULL
 */
function cleanup_inbound( $messageQueue , $redis , $controlChannel )
{
  $members = $redis->smembers( $messageQueue );

  dlog('',"mq = $messageQueue ; members = %s",$members);

  // loop though channels
  foreach( $members as $channelName )
    cleanup_inbound_message( $channelName , $redis , $controlChannel , $messageQueue );

  return NULL;
}

/**
 * cleanup_inbound_message
 *
 * Attempt to cleanup $channelName if lost
 *
 * @return NULL
 */
function cleanup_inbound_message( $channelName , $redis , $controlChannel , $messageQueue )
{
  $content = $controlChannel->peekControlChannel( $channelName );

  dlog('',"channelName = $channelName , content = $content");

  if ( ! $content )
  {
    dlog('',"deleting $channelName");

    $redis->del( 'CM_' . $channelName );
    $redis->del( 'CS_' . $channelName );
    $redis->srem( $messageQueue , $channelName );
  }

  return NULL;
}

?>

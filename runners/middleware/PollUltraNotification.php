<?php

/*
Identical to middleware__PollUltraNotification

Purpose: Extracts an Inbound Notification Message from the Ultra MW Message queue (MW logic layer) and process it.
*/

require_once 'db.php';
require_once 'Ultra/Lib/MQ/EndPoint.php';
require_once 'Ultra/Lib/MiddleWare/Adapter/Notification.php';

if ( ! \Ultra\UltraConfig\middleware_enabled_amdocs() )
{
  dlog('','middleware currently disabled');
}
else
{
  teldata_change_db();

  $endPoint = new \Ultra\Lib\MQ\EndPoint;

  // is there an inbound notification waiting in the ULTRA MW Notification Channel? (from the MW logic layer)
  if ( $endPoint->peekNotificationChannelUltraMW() )
  {
    // dequeue inbound ULTRA MW Notification Channel
    $message = $endPoint->dequeueNotificationChannelULTRAMW();

    if ( $message )
    {
      dlog('',"message = %s",$message);

      $data = $endPoint->extractFromMessage($message);

      if ( $data )
      {
        $data = (array) $data;

        dlog('',"data = %s",$data);

        $ultraMiddleware = new \Ultra\Lib\MiddleWare\Adapter\Notification;

        $result = $ultraMiddleware->processNotification(
          array(
            'actionUUID' => $data['_actionUUID'],
            'uuid'       => $data['_uuid'],
            'command'    => $data['header'],
            'parameters' => $data['body']
          )
        );
      }
      else
        dlog('','Cannot extract data from message');
    }
  }
}

?>

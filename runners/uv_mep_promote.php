<?php

/**
 * Univision Migration Extesnion Plan runner
 * promote MEP subs with sufficient balance out of MEP
 * suggested execution time: after daily monthly runner completion
 * @see PJW-80, API-388
 */

require_once 'db.php';
require_once 'classes/MigrationExtensionPlan.php';


exit(main($argc, $argv));


/**
 * main
 */
function main($count, $arguments)
{
  logInfo('starting ' . __FILE__ . ' at ' . date('c'));

  teldata_change_db();
  $mep = new MigrationExtensionPlan;
  foreach ($mep->getFundedMepCustomers() as $customer)
  {
    $customer->customer_id = $customer->CUSTOMER_ID;
    if ( ! $mep->renewCustomerPlan($customer))
      logError("failed to promote customer ID {$customer->CUSTOMER_ID} out of MEP");
  }

  logInfo('finished ' . __FILE__ . ' at ' . date('c'));
  return 0;
}


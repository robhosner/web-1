PHP_ARG_ENABLE(cryptography, whether to enable cryptography extension, [ --enable-cryptography   Enable cryptography extension])

if test "$PHP_CRYPTOGRAPHY" = "yes"; then
  AC_DEFINE(HAVE_CRYPTOGRAPHY, 1, [Whether you have cryptography extension])
  PHP_NEW_EXTENSION(cryptography, cryptography.c, $ext_shared)
fi


<?php

include_once("Validate.php");
include_once("Validate/Finance/CreditCard.php");
include_once("lib/util-common.php");
include_once('Ultra/UltraConfig.php');

$cc_types = array("MasterCard", "Visa", "AMEX", "Discover");

$field_types = array(
  'all characters'                           => array('string', ''),
  'numeric'                                  => array('string', VALIDATE_NUM),
  'alphanumeric'                             => array('string', VALIDATE_ALPHA . VALIDATE_NUM),
  'alphanumeric and spaces'                  => array('string', VALIDATE_ALPHA . VALIDATE_NUM . ' '),
  'letters, spaces, apostrophe'        		 => array('string', VALIDATE_ALPHA . "' "),
  'letters, numbers, spaces, or punctuation' => array('string', VALIDATE_STREET . VALIDATE_PUNCTUATION),
  'letters, numbers, spaces, punctuation, #' => array('string', VALIDATE_STREET . VALIDATE_PUNCTUATION . '#'),
  'letters, numbers, or spaces'              => array('string', VALIDATE_STREET),
  'alphanumeric or punctuation'              => array('string', VALIDATE_ALPHA . VALIDATE_NUM . VALIDATE_PUNCTUATION),
  'E_MAIL'                                   => array('e_mail', array()),
  'LOGIN_E_MAIL'                             => array('login_e_mail', array()),
  'LOGIN_NAME'                               => array('login_name', array()),
  'COUNTRY'                                  => array('choice', array(
                                                        "US" => "USA",
                                                        "AU" => "Australia",
                                                        "BH" => "Bahrain",
                                                        "CA" => "Canada",
                                                        "FR" => "France",
                                                        "HK" => "Hong Kong",
                                                        "IN" => "India",
                                                        "IE" => "Ireland",
                                                        "JP" => "Japan",
                                                        "NL" => "Netherlands",
                                                        "NZ" => "New Zealand",
                                                        "NO" => "Norway",
                                                        "SG" => "Singapore",
                                                        "ES" => "Spain",
                                                        "GB" => "UK",
                                                        )),
  'MFUNDS_AUTHENTICATION'                    => array('choice', array(
                                                        "ssn" => "SSN",
                                                        "passport" => "Passport",
                                                        "us_license" =>"US Driver's License",
                                                        "us_work_visa" => "US Work Visa",
                                                        "us_student_visa" => "US Student Visa",
                                                        "us_green_card" => "US Green Card",
                                                        "mexico_matricula" => "Mexican Matricula Consular",
                                                        "philippines_sss" => "Philippines SSS ID",
                                                        )),
  'MMYY'                       => array('mmyy'),
  'MMDDYY'                     => array('mmddyy'), /* TODO */
  'DIALER'                     => array('dialer'),
);

$field_savers = array(
  'cfs' => "UPDATE customers SET %s = %s WHERE %s", // customer field saver
  /* note we don't want to save fields for customers that have already submitted */
  'mfs' => "UPDATE htt_mfunds_applications SET %s = %s WHERE %s AND (application_lifecycle IS NULL OR application_lifecycle < 40)", // mFunds field saver
  'dialer_saver' => "",
  'null' => "", // do nothing
  // CC updater
  'cc_and_pin_saver' => "insert into IPCOMMAND
(DATE, RECIPIENT, SENDER, COMMAND, VALUE1, VALUE2, VALUE3, VALUE4, VALUE5)
values (getUTCDate(), 'CCV SERVER', 'WEB INTERFACE', 'UPDATE_CCARD',
        %s, %s, '', '', '')",
  );

function field_definitions() {
  return array(
# '$FIELD_NAME'               => array($min_length,$max_length,$type,$saver,$saver_params,$error_code),
  'account_password'          => array(6, 20, 'all characters', 'cfs', 'LOGIN_PASSWORD'),
  'account_login'             => array(2, 20, 'LOGIN_NAME',   'cfs', 'LOGIN_NAME'),
  'account_first_name'        => array(1, 25, 'letters, spaces, apostrophe', 'cfs', 'First_Name' , 'VV0038'),
  'account_last_name'         => array(1, 25, 'letters, spaces, apostrophe', 'cfs', 'Last_Name'  , 'VV0039'),

  'account_address1'          => array(3, 80, 'letters, numbers, spaces, punctuation, #', 'cfs', 'Address1' , 'VV0040'),
  'account_address2'          => array(0, 40, 'letters, numbers, spaces, punctuation, #', 'cfs', 'Address2' , 'VV0041'),

  'account_city'              => array(2, 25, 'alphanumeric or punctuation', 'cfs', 'City'         , 'VV0042'),
  'account_state_or_region'   => array(2, 40, 'alphanumeric or punctuation', 'cfs', 'State_Region' , 'VV0043'),
  'account_postal_code'       => array(2, 20, 'alphanumeric or punctuation', 'cfs', 'Postal_Code'  , 'VV0044'),
  'account_country'           => array(2, 25, 'COUNTRY',                     'cfs', 'Country'      , 'VV0058'),

  'cc_name'              => array(3, 50, 'alphanumeric and spaces', 'cfs', 'cc_name'               , 'VV0046'),
  'cc_address1'          => array(3, 80, 'letters, numbers, spaces, punctuation, #', 'cfs', 'cc_address1', 'VV0047'),
  'cc_address2'          => array(0, 40, 'letters, numbers, spaces, punctuation, #', 'cfs', 'cc_address2', 'VV0048'),
  'cc_city'              => array(2, 25, 'alphanumeric or punctuation', 'cfs', 'cc_city'         , 'VV0049'),
  'cc_state_or_region'   => array(2, 40, 'alphanumeric or punctuation', 'cfs', 'cc_state_region' , 'VV0050'),
  'cc_postal_code'       => array(2, 20, 'alphanumeric or punctuation', 'cfs', 'cc_postal_code'  , 'VV0051'),
  'cc_country'           => array(2, 25, 'COUNTRY', 'cfs', 'cc_country'                          , 'VV0052'),

  'shipping_address1'          => array(3, 80, 'letters, numbers, spaces, punctuation, #', 'cfs', 'Address1', 'VV0053'),
  'shipping_address2'          => array(0, 40, 'letters, numbers, spaces, punctuation, #', 'cfs', 'Address2', 'VV0054'),
  'shipping_city'              => array(2, 25, 'alphanumeric or punctuation', 'cfs', 'City'                 , 'VV0055'),
  'shipping_state_or_region'   => array(2, 40, 'alphanumeric or punctuation', 'cfs', 'State_Region'         , 'VV0056'),
  'shipping_postal_code'       => array(2, 20, 'alphanumeric or punctuation', 'cfs', 'Postal_Code'          , 'VV0057'),
  'shipping_country'           => array(2, 25, 'COUNTRY', 'cfs', 'Country' , 'VV0058'),

  'account_number_phone'      => array(6, 24, 'numeric', 'cfs', 'Local_Phone' , 'VV0999'),

  'account_number_email'      => array(5, 80, 'LOGIN_E_MAIL', 'cfs', 'E_mail' , 'VV0060'),
  'e_mail'                    => array(5, 80, 'E_MAIL',       'cfs', 'E_mail' , 'VV0060'),

  'dialer'                    => array(0, 4096, 'DIALER', 'dialer_saver', ''     , 'VV0064'),

  'mfunds_first_name'        => array(1, 80, 'alphanumeric', 'mfs', 'first_name'),
  'mfunds_middle_initial'    => array(0, 20, 'alphanumeric', 'mfs', 'middle_initial'),
  'mfunds_last_name'         => array(1, 80, 'alphanumeric', 'mfs', 'last_name'),
  'mfunds_address1'          => array(3, 80, 'letters, numbers, or spaces', 'mfs', 'address1'),
  'mfunds_address2'          => array(0, 40, 'letters, numbers, or spaces', 'mfs', 'address2'),

  'mfunds_city'              => array(2, 25, 'alphanumeric or punctuation', 'mfs', 'city'),
  'mfunds_state_or_region'   => array(2, 25, 'alphanumeric or punctuation', 'mfs', 'state_region'),
  'mfunds_postal_code'       => array(2, 20, 'alphanumeric or punctuation', 'mfs', 'postal_code'),
  'mfunds_country'           => array(2, 25, 'COUNTRY', 'mfs', 'country'),

  'mfunds_home_phone'        => array(6, 24, 'numeric', 'mfs', 'home_phone'),
  'mfunds_work_phone'        => array(0, 24, 'numeric', 'mfs', 'work_phone'),
  'mfunds_email'             => array(5, 80, 'E_MAIL', 'mfs', 'e_mail'),
  'mfunds_date_of_birth'     => array(8, 40, 'letters, numbers, spaces, or punctuation', 'mfs', 'date_of_birth'),
  'mfunds_social_security'   => array(9, 9, 'numeric', 'mfs', 'social_security'),
  'mfunds_authentication'    => array(2, 25, 'MFUNDS_AUTHENTICATION', 'mfs', 'alternate_id_type'),
  'mfunds_auth_number'       => array(4, 40, 'letters, numbers, spaces, or punctuation', 'mfs', 'alternate_id_number'),
  'mfunds_birth_country'     => array(0, 40, 'letters, numbers, spaces, or punctuation', 'mfs', 'country_of_birth'),
  'mfunds_birth_province'    => array(0, 40, 'letters, numbers, spaces, or punctuation', 'mfs', 'province_of_birth'),
  'mfunds_preferred_language'=> array(0, 40, 'letters, numbers, spaces, or punctuation', 'mfs', 'preferred_language'),
  'mfunds_image'             => array(0, 255, 'letters, numbers, spaces, or punctuation', 'mfs', 'image_file_path'),
  );
}

function fields($fields, $customer, $disabled, $modifiers=array())
{
  global $uvsite;
  global $out;
  $validator = new \Validate;

  dlog('', 'Oh, not fields.php again! (%s)', func_get_args());

  $ret      = array();
  $sql_todo = array();

  global $field_types;
  global $field_savers;
  $field_definitions = field_definitions();

  $all_validated = TRUE;

  $cvv = NULL;
  $ccexp = NULL;

  if ( ! isset( $modifiers['f/ok_cc_dup'] ) )
    $modifiers['f/ok_cc_dup']    = \Ultra\UltraConfig\allowInfiniteCCCount();

  if ( ! isset( $modifiers['f/ok_email_dup'] ) )
    $modifiers['f/ok_email_dup'] = \Ultra\UltraConfig\allowInfiniteEmailCount();

  if (NULL != $customer)
  {
    $full = find_customer(make_find_new_signup_customer_query($customer->CUSTOMER));
    if (NULL != $full)
    {
      $cvv = $full->CCV;
      $ccexp = $full->CC_EXP_DATE;
    }

    $fields['pin_number'] = $customer->CUSTOMER;
  }

  foreach ($field_definitions as $field_name => $field_definition)
  {
    if (array_key_exists($field_name, $fields))
    {
      $value = $fields[$field_name];
      if ($value == NULL) $value = '';
      $min_length = $field_definition[0];
      $max_length = $field_definition[1];
      $type = $field_definition[2];
      $saver = $field_definition[3];
      $saver_params = array_slice($field_definition, 4);
      $type_detail = $field_types[$type];

      $ret[] = "--value = '$value'; field definition for $field_name: min/max length $min_length/$max_length, type '$field_definition[2]'";
      $validated = TRUE;
      $message = '';

      if (!is_array($value) && strlen($value) < $min_length)
      {
        $validated = FALSE;
        $message = sprintf($field_name.' value too short (minimum %d chars)', $min_length);
      }
      else if (!is_array($value) && strlen($value) > $max_length)
      {
        $validated = FALSE;
        $message = sprintf($field_name.' value too long (maximum %d chars)', $max_length);
      }
      else if ($type_detail[0] === 'string')
      {
        if ($validator->string($value, array('format' => $type_detail[1])))
        {
        }
        else
        {
          $validated = FALSE;
          $message = sprintf($field_name.' value fails validation (%s)', $type);
        }
      }
      else if ($type_detail[0] === 'e_mail')
      {
        if ($validator->email($value, $type_detail[1]))
        {
          $validated = TRUE;
          $message = '';
        }
        else
        {
          $validated = FALSE;
          $message = $field_name.' value fails e-mail validation';
        }
      }
      else if ($type_detail[0] === 'e_mail')
      {
        if ($validator->email($value, $type_detail[1]))
        {
          $validated = TRUE;
        }
        else
        {
          $validated = FALSE;
          $message = $field_name.' value fails e-mail validation';
        }
      }
      else if ($type_detail[0] === 'login_e_mail')
      {
        if ($validator->email($value, $type_detail[1]))
        {
          if ( $modifiers['f/ok_email_dup'] )
          {
            $validated = TRUE;
            $message = '';
          }
          else
          {
            $q = sprintf('SELECT TOP 1 e_mail FROM customers c, accounts a ' .
              'WHERE c.customer_id = a.customer_id AND a.%s AND a.account_id <> %s AND e_mail = %s',
              account_enabled_clause(TRUE),
              $customer == NULL ? -1 : $customer->ACCOUNT_ID,
              mssql_escape($value));
            if (mssql_has_rows($q))
            {
              $validated = FALSE;
              $message = $field_name." is in use by an existing customer";
            }
            else
            {
              $validated = TRUE;
              $message = '';
            }
          }
        }
        else
        {
          $validated = FALSE;
          $message = $field_name.' value fails e-mail validation';
        }
      }
      else if ($type_detail[0] === 'login_name')
      {
        $validated = FALSE;
        $message = "Login name is not valid";
        if ($validator->string($value, array('format' => VALIDATE_NUM . VALIDATE_ALPHA)))
        {
          $q = sprintf('SELECT TOP 1 login_name FROM customers c, accounts a ' .
            'WHERE c.customer_id = a.customer_id AND a.%s AND a.account_id <> %s AND login_name = %s',
            account_enabled_clause(TRUE),
            $customer == NULL ? -1 : $customer->ACCOUNT_ID,
            mssql_escape($value));
          if (mssql_has_rows($q))
          {
            $validated = FALSE;
            $message = "Login name is in use";
          }
          else
          {
            $validated = TRUE;
            $message = '';
          }
        }
      }
      else if ($type_detail[0] === 'choice')
      {
        if (array_key_exists($value, $type_detail[1]))
        {
        }
        else
        {
          $validated = FALSE;
          $message = $type_detail[1];
        }
      }
      else if ($type_detail[0] === 'mmyy')
      {
        if ($validator->string($value, array('format' => VALIDATE_NUM)))
        {
          if (strlen($value) == 4)
          {
            $month = substr($value, 0, 2);
            $year = substr($value, -2);
            if ($month < 1 || $month > 12)
            {
              $validated = FALSE;
              $message = "MMYY value $value has invalid month $month";
            }
            else if ($year < 12 || $year > 99)
            {
              $validated = FALSE;
              $message = "MMYY value $value has invalid year $year";
            }
            else
            {
              $ccexp = $value;
              $validated = TRUE;
              $message = "";

            }
          }
          else
          {
            $validated = FALSE;
            $message = "MMYY value $value is not 4 digits";
          }
        }
        else
        {
          $validated = FALSE;
          $message = "MMYY value $value is not numeric";
        }
      }
      else if ($type_detail[0] === 'cc_number')
      {
        $validated = FALSE;
        $message = $field_name." is not a valid credit card";
        foreach ($type_detail[1] as $cc_type)
        {
          if (Validate_Finance_CreditCard::number($value, $cc_type))
          {
            if (NULL == $ccexp)
            {
              $validated = FALSE;
              $message = 'Could not get the CC expiration for comparison';
            }
            else if (NULL == $cvv)
            {
              $validated = FALSE;
              $message = 'Could not get validation code for comparison';
            }
            else
            {
              if ($uvsite && !preg_match("/^$uvsite/", $value))
              {
                $validated = FALSE;
                $message = "Invalid UV credit card number.";
              }
              else if ( overridden_cc_dupe_check($cvv,$value,$ccexp,$customer->LAST_NAME) )
              {
                dlog("", "Skipped CC check for a overridden_cc_dupe_check");
                $validated = TRUE;
                $message = 'Yo overridden_cc_dupe_check';
              }
              else
              {
                $maxDuplicateCCCount = \Ultra\UltraConfig\maxDuplicateCCCount();

                if ( $modifiers['f/ok_cc_dup'] )
                {
                  dlog("","Skipped CC check due to f/ok_cc_dup setting");
                  $validated = TRUE;
                  $message = '';
                }
                else if ( $maxDuplicateCCCount )
                {
                  $sql = sprintf('SELECT COUNT(*) FROM customers c, accounts a ' .
                    'WHERE c.customer_id = a.customer_id AND a.%s AND a.account_id <> %s ' .
                    'AND RIGHT(c.cc_number, 4) = %s AND c.cc_exp_date = %s AND c.ccv = %s',
                    account_enabled_clause(TRUE),
                    $customer == NULL ? -1 : $customer->ACCOUNT_ID,
                    mssql_escape_with_zeroes(substr($value, -4)),
                    mssql_escape_with_zeroes($ccexp),
                    mssql_escape_with_zeroes($cvv));

                  $cc_dup_count_n = 0;
                  $cc_dup_count = mssql_fetch_all_rows(logged_mssql_query($sql));

                  if ( $cc_dup_count && is_array( $cc_dup_count ) && count($cc_dup_count) )
                    $cc_dup_count_n = $cc_dup_count[0][0];

                  dlog("","cc_dup_count_n = $cc_dup_count_n");

                  if ( $cc_dup_count_n >= $maxDuplicateCCCount )
                  {
                    dlog("","This credit card is already in use by other $maxDuplicateCCCount users in our system");
                    $validated = FALSE;
                    $message = 'This credit card is already in use by other users in our system';
                    $field_definition[ 5 ] = 'CC0003'; // appropriate error code for Ultra API 2.0
                  }
                  else
                  {
                    $validated = TRUE;
                    $message = '';
                  }
                }
                else
                {
                  dlog("","Skipped CC check due to maxDuplicateCCCount not defined or 0");
                  $validated = TRUE;
                  $message = '';
                }
              }
            }
            break;
          }
        }
      }
      else if ($type_detail[0] === 'cc_cvv')
      {
        $validated = FALSE;
        $message = "Value fails CVV number validation";
        foreach ($type_detail[1] as $cc_type)
        {
          if (Validate_Finance_CreditCard::cvv($value, $cc_type))
          {
            $cvv = $value;

            $validated = TRUE;
            $message = '';
            break;
          }
        }
      }
      else if ($type_detail[0] === 'dialer')
      {
        $validated = TRUE;
        $message = "";

        if (is_array($value))
        {
          foreach ($value as $type => $dinfo)
          {
            if ($type === 'alias')
            {
              if (isVector($dinfo))
              {
                $counts = array_count_values($dinfo);
                foreach ($dinfo as $number)
                {
                  if ($validated &&
                      $validator->string($number,
                                       array('format' => VALIDATE_NUM)))
                  {
                    $aq = sprintf('SELECT TOP 1 alias FROM account_aliases aa, accounts a ' .
                      "WHERE aa.dnis = '*' AND aa.account_alias_type = 1 AND a.account_id = aa.account_id " .
                      'AND aa.alias = %s AND a.%s AND a.account_id <> %s',
                        mssql_escape_with_zeroes($number),
                        account_enabled_clause(TRUE),
                        $customer->ACCOUNT_ID);
                    $rs = mssql_fetch_all_objects(logged_mssql_query($aq));
                    foreach ($rs as $alias)
                    {
                      $validated = FALSE;
                      $message = "This number is being used as a pinless number in another account. A phone number can only pinlessly authenticate to one account at a time.";
                      break;
                    }

                    if ($validated && $counts[$number] > 1)
                    {
                      $validated = FALSE;
                      $message = "Pinless (alias) number '$number' can't be given more than once, sorry.";
                    }
                  }
                  else if ($validated)
                  {
                    $validated = FALSE;
                    $message = "Pinless (alias) number '$number' is not a number, sorry.";
                  }
                }
              }
              else
              {
                $validated = FALSE;
                $message = "Pinless (alias) info is not a vector, sorry.";
              }
            }
            else if ($type === 'auto_set_n')
            {
              if ($validator->string($dinfo,
                                   array('format' => VALIDATE_NUM)) &&
                $dinfo >= 5 && $dinfo <= 20)
              {
              }
              else
              {
                $validated = FALSE;
                $message = "auto_set_n parameter '$dinfo' is not a number between 5 and 20, sorry.";
              }
            }
            else if ($type === 'speed' || $type === 'auto')
            {
              if (isVector($dinfo))
              {
                foreach ($dinfo as $speed_or_auto_spec)
                {
                  if ($validated && isAssociative($speed_or_auto_spec))
                  {
                    $speed_spec = $type === 'speed';
                    $auto_spec = $type === 'auto';

                    $number_key = $speed_spec ? 'number' : 'dnis';
                    $entry_key = $speed_spec ? 'entry' : 'destination';

                    $number = array_key_exists($number_key, $speed_or_auto_spec) ? $speed_or_auto_spec[$number_key] : "NOT GIVEN";
                    $entry = array_key_exists($entry_key, $speed_or_auto_spec) ? $speed_or_auto_spec[$entry_key] : "NOT GIVEN";
                    $desc = array_key_exists('description', $speed_or_auto_spec) ? $speed_or_auto_spec['description'] : "";
                    $uid = array_key_exists('autodialer_id', $speed_or_auto_spec) ? $speed_or_auto_spec['autodialer_id'] : "";

                    $validated = TRUE;
                    $message = "";

                    if ($validated &&
                        $validator->string($number,
                                         array('format' => VALIDATE_NUM)))
                    {
                    }
                    else
                    {
                      $validated = FALSE;
                      $message = "$type $number_key '$number' is not a number, sorry.";
                    }

                    if ($validated &&
                        $validator->string($entry,
                                         array('format' => VALIDATE_NUM)))
                    {
                    }
                    else
                    {
                      $validated = FALSE;
                      $message = "$type $entry_key '$entry' is not a number, sorry.";
                    }

                    if ($validated && $auto_spec)
                    {
                      if ($validator->string($uid,
                                           array('format' => VALIDATE_NUM)))
                      {
                      }
                      else
                      {
                        $validated = FALSE;
                        $message = "$type autodialer_id '$uid' is not a number, sorry.";
                      }
                    }
                  }
                  else if ($validated && !isAssociative($speed_or_auto_spec))
                  {
                    $validated = FALSE;
                    $message = "$type dial entry is not assoc. array, sorry.";
                  }
                }
              }
              else
              {
                $validated = FALSE;
                $message = "$type dial list is not a vector, sorry.";
              }
            }
            else
            {
              $validated = FALSE;
              $message = "Dialer type $type is unhandled yet, sorry.";
            }
          }
        }
        else
        {
          $validated = FALSE;
          $message = "Value fails dialer validation: not an array";
        }
      }
      else
      {
        $validated = FALSE;
        $message = "Unknown field type $type_detail[0], sorry.";
      }

      if (NULL != $customer)
      {
        $customer_clause = sprintf("customer_id = %d", $customer->CUSTOMER_ID);
      }
      else /* NEVER UPDATE IN THIS CASE */
      {
        $customer_clause = sprintf("customer_id = -1");
      }

      if (isset($out['mfunds_child_null']) && $out['mfunds_child_null'])
      {
        $customer_clause = sprintf("customer_id IN
(SELECT customer_id FROM htt_mfunds_applications
 WHERE mfunds_person_id IS NULL AND parent_mfunds_person_id IN
 (SELECT mfunds_person_id FROM htt_mfunds_applications
  WHERE customer_id = %d AND parent_mfunds_person_id IS NULL
 )
)", $customer->CUSTOMER_ID);
      }

      if ($validated && $saver === 'cfs' && $customer != NULL)
      {
        $sql_todo[$field_name] = '';
        foreach ($saver_params as $saver_param)
        {
          if (!preg_match("/^VV0/", $saver_param)) # ignore $error_code
          {
          $sql_todo[$field_name] = $sql_todo[$field_name] . "; " .
            sprintf($field_savers[$saver],
                    $saver_param,
                    mssql_escape_with_zeroes($value),
                    $customer_clause);
          }
        }
      }

      if ($validated && $saver === 'mfs' && $customer != NULL)
      {
        $sql_todo[$field_name] = '';

        foreach ($saver_params as $saver_param)
        {
          if (!preg_match("/^VV0/", $saver_param)) # ignore $error_code
          {
          $sql_todo[$field_name] = $sql_todo[$field_name] . "; " .
            sprintf($field_savers[$saver],
                    $saver_param,
                    mssql_escape_with_zeroes($value),
                    $customer_clause);
          }
        }
      }

      if ($validated && $saver === 'cc_and_pin_saver' && $customer != NULL)
      {
        $sql_todo[$field_name] = sprintf($field_savers[$saver], mssql_escape_with_zeroes($fields['pin_number']), mssql_escape_with_zeroes($value));
      }

      if ($validated && $saver === 'dialer_saver' && $customer != NULL)
      {
        $q = '';

        if (array_key_exists('alias', $value))
        {
          $numbers = $value['alias'];

          $alias_numbers = get_account_aliases($customer);

          foreach ($alias_numbers as $alias)
          {
            $key = array_search($alias, $numbers);
            if(isset($key) && $key != FALSE)
            {
              unset($numbers[$key]);
            }
            else
            {
              $q = $q . sprintf("DELETE FROM account_aliases
WHERE dnis = '*' AND account_alias_type = 1
AND account_id = '%s' AND alias = '%s'; ",
                                $customer->ACCOUNT_ID, $alias);
            }
          }

          foreach ($numbers as $number)
          {
            $q = $q . sprintf('IF NOT EXISTS ' .
              '(SELECT alias FROM account_aliases aa, accounts a ' .
              "WHERE aa.dnis = '*' AND aa.account_alias_type = 1 " .
              "AND a.account_id = aa.account_id AND aa.alias = '%s' " .
              'AND a.%s AND a.account_id <> %s) ' .
              'BEGIN ' .
              'INSERT INTO account_aliases ' .
              '(dnis, alias, account_id, account_alias_type) ' .
              "VALUES ('*', '%s', '%s', 1) " .
              'END;',
              $number,
              account_enabled_clause(TRUE),
              $customer->ACCOUNT_ID,
              $number,
              $customer->ACCOUNT_ID);
          }
        }

        if (array_key_exists('auto_set_n', $value))
        {
          $n = $value['auto_set_n'];
          $autoq = sprintf("SELECT autodialer_id, destination, description
FROM autodialer
WHERE account='%s' AND description NOT LIKE 'htt_special_%'",
                           $customer->ACCOUNT);
          $existing = mssql_fetch_all_objects(logged_mssql_query($autoq));
          if (is_array($existing))
          {
            $existing_count = count($existing);

            if ($n < $existing_count)
            {
              $q = $q . sprintf("DELETE FROM autodialer WHERE autodialer_id IN
  (SELECT TOP %d autodialer_id FROM autodialer
   WHERE account = '%s' AND description IS NULL AND destination IS NULL
   ORDER BY autodialer_id DESC); ",
                                $existing_count - $n,
                                $customer->ACCOUNT);
            }
            else if ($n > $existing_count)
            {
              $type = $customer->COS_ID == 100729 ? 'uv' : 'indiald';
              $q = $q . sprintf("INSERT INTO autodialer
      (dnis, account, destination, description)
VALUES ((%s), '%s', NULL, NULL); ",
                                make_find_autodial_numbers_for_customer_query($customer->ACCOUNT,
                                                                              $type),
                                $customer->ACCOUNT);
            }
          }
        }

        if (array_key_exists('auto', $value))
        {
          $autos = $value['auto'];

          foreach ($autos as $auto)
          {
            $q = $q . sprintf("UPDATE autodialer
SET destination = %s, description = %s
WHERE dnis = '%s' AND autodialer_id = %s AND account='%s'; ",
                              mssql_quote_or_null($auto['destination']),
                              mssql_escape_or_null($auto['description']),
                              $auto['dnis'],
                              mssql_escape($auto['autodialer_id']),
                              $customer->ACCOUNT);
          }
        }

        if (array_key_exists('speed', $value))
        {
          $speeds = $value['speed'];

          $sq = sprintf("SELECT entry, number, description FROM speed
WHERE account = '%s'",
                        $customer->ACCOUNT);

          $by_entry = array();

          foreach ($speeds as $speed)
          {
            $by_entry[$speed['entry']] = $speed;
          }

          $entries = array_keys($by_entry);
          $to_update = array();

          foreach (mssql_fetch_all_objects(logged_mssql_query($sq))
                   as $speed_row)
          {
            $key = array_search($speed_row->entry, $entries);
            if(isset($key) && $key != FALSE)
            {
              //dlog("", "set key [$key] = " . $speed_row->entry);
              unset($entries[$key]);
              $to_update[$speed_row->entry] = sprintf("%s;%s",
                                                      $speed_row->number,
                                                      $speed_row->description);
            }
            else
            {
              //dlog("", "not found key ". $speed_row->entry);
              $q = $q . sprintf("DELETE FROM speed
WHERE entry = '%s' AND account = '%s'; ",
                                $speed_row->entry,
                                $customer->ACCOUNT);
            }
          }

          /* now, $entries has the entry keys to insert
             and $to_update has updates */

          foreach ($to_update as $entry => $old_comparo)
          {
            $new_comparo = sprintf("%s;%s",
                                   $by_entry[$entry]['number'],
                                   $by_entry[$entry]['description']);
            if ($old_comparo === $new_comparo)
            {
              // do nothing, the update is not necessary
            }
            else
            {
              $q = $q . sprintf("UPDATE speed
SET entry = '%s', number = '%s', description = %s
WHERE account='%s' AND entry='%s'; ",
                                $by_entry[$entry]['entry'],
                                $by_entry[$entry]['number'],
                                mssql_escape($by_entry[$entry]['description']),
                                $customer->ACCOUNT,
                                $by_entry[$entry]['entry']);
            }
          }

          foreach ($entries as $entry)
          {
            $q = $q . sprintf("INSERT INTO speed
(account, entry, number, description)
VALUES ('%s', '%s', '%s', %s); ",
                              $customer->ACCOUNT,
                              $by_entry[$entry]['entry'],
                              $by_entry[$entry]['number'],
                              mssql_escape($by_entry[$entry]['description']));
          }
        }

        $sql_todo[$field_name] = $q;
      }

      $ret[] = array('validated' => array('ok'             => $validated,
                                          'max_length'     => $max_length,
                                          'v_type'         => $field_definition,
                                          'error_code'     => $field_definition[ 5 ],
                                          'field_name'     => $field_name,
                                          'field_value'    => $value,
                                          'validation_msg' => $message));

      if (!$validated) $all_validated = FALSE;
    }
  }

  $ret[] = array('all_validated' => $all_validated);

  if ($all_validated && !$disabled)
  {
    foreach ($sql_todo as $field => $q)
    {
      $ret[] = array('sql' => array($field, is_mssql_successful(logged_mssql_query($q))));
    }
  }

  return $ret;
}

function validate_fields($fields)
{
  $fcheck = fields($fields, NULL, TRUE); // just verify the fields

  dlog('fields',
       "Checking %s, fields() returned %s",
       json_encode($fields),
       json_encode($fcheck));

  $all_validated = FALSE;
  if (is_array($fcheck))
  {
    foreach ($fcheck as $checkpoint)
    {
      if (is_array($checkpoint) &&
          array_key_exists('all_validated', $checkpoint))
      {
        $all_validated = $checkpoint['all_validated'];
      }
    }
  }

  return $all_validated;
}


function get_account_aliases($customer_full)
{
  $aliasq = sprintf("SELECT alias FROM account_aliases
WHERE dnis = '*' AND account_alias_type = 1 AND account_id = '%s'",
                    $customer_full->ACCOUNT_ID);

  $numbers_objects = mssql_fetch_all_objects(logged_mssql_query($aliasq));

  $numbers = array();

  foreach ($numbers_objects as $no)
  {
    $numbers[] = $no->alias;
  }

  return $numbers;
}

?>

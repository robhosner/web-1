<?php
include_once 'lib/util-common.php';

function interpolate_fields($customer_full, $val, $options)
{
  global $out;
  $mfunds_status = NULL;

  $matches = array();
  while (preg_match("/(.*)(CUSTOMERS|MFUNDS):(\w+)(.*)/", $val, $matches))
  {
    /* preg_replace gave me trouble, TODO later */
    $table = $matches[2];
    $field = $matches[3];

    switch ($table)
    {
    case 'CUSTOMERS':
      $val = $matches[1] . $customer_full->$field . $matches[4];
      break;

    case 'MFUNDS':
      if (NULL == $mfunds_status)
      {
        if (array_key_exists('child_null', $out) && $out['child_null'])
        {
          $mfunds_status = mfunds_status($customer_full->CUSTOMER_ID, TRUE);
        }
        else if (array_key_exists('person_id', $options) && $options['person_id'])
        {
          $mfunds_status = mfunds_status_mperson($options['person_id']);
        }
        else
        {
          $mfunds_status = mfunds_status($customer_full->CUSTOMER_ID);
        }
      }

      if (NULL == $mfunds_status)
      {
        $val = "Error: could not retrieve field $field";
      }
      else
      {
        $val = $matches[1] . $mfunds_status->$field . $matches[4];
      }
    }
  }

  return $val;
}

function mfunds_password($person_id)
{
  return "sikrit password for HTT-$person_id";
}

function mfunds_update_field($customer_full, $person_id, $field, $data)
{
  global $out;

  $q = sprintf("
        UPDATE htt_mfunds_applications
        SET $field = %s
        WHERE mfunds_person_id = %d",
               mssql_escape_with_zeroes($data),
               $person_id);

  $check = run_sql_and_check($q);
  if (!$check)
  {
    $out['errors'][] = "Sorry, the field '$field' could not be updated to '$data'";
  }

  return $check;
}

function mfunds_update_expiration($customer_full, $person_id)
{
  global $out;

  $params = array(
    'personIdentifier' => 'MFUNDS:mfunds_person_id',
    'last4' => 'MFUNDS:last_four',
    );

  $details = mfunds_CardDetails($params, $customer_full, $person_id);

  $res = $details->CardDetailsResult;
  $exp = $res->ExpireDateTime; // "6\\/30\\/2015 11:59:59 PM"

  if ($exp)
  {
    $matches = array();
    if (preg_match("/^(\d+)\/\d+\/20(\d+)/", $exp, $matches))
    {
      $exp = sprintf("%02d%02d", $matches[1], $matches[2]);

      mfunds_update_field($customer_full, $person_id, 'expiration_mmyy', $exp);
    }
  }
}

function mfunds_set_lifecycle($customer_id, $person_id, $stage, $last4=NULL, $notes=NULL, $status=NULL)
{
  global $out;

  $clause = NULL;
  if (NULL == $person_id)
  {
    $clause = sprintf("WHERE customer_id = %d", $customer_id);
  }
  else
  {
    $clause = sprintf("WHERE mfunds_person_id = %d", $person_id);
  }

  $q = sprintf("
UPDATE htt_mfunds_applications
 SET application_lifecycle = %d
$clause",
               $stage,
               $person_id);

  if ($last4)
  {
    $q = "$q; " . sprintf("
UPDATE htt_mfunds_applications
 SET last_four = %d
$clause",
                          $last4,
                          $person_id);
  }

  if ($notes)
  {
    $q = "$q; " . sprintf("
UPDATE htt_mfunds_applications
 SET notes = convert(nvarchar(max),notes) + ';;;' + CAST(%s AS VARCHAR(MAX))
$clause",
                          mssql_escape_with_zeroes($notes),
                          $person_id);
  }

  if ($status)
  {
    $q = "$q; " . sprintf("
UPDATE htt_mfunds_applications
 SET status = %s
$clause",
                          mssql_escape_with_zeroes($status),
                          $person_id);
  }

  return run_sql_and_check($q);
}

function mfunds_status($customer_id, $nullchild=FALSE, $child=FALSE)
{
  $q = $nullchild ? make_find_status_customer_nullchild_query_mfunds($customer_id) :
    make_find_status_customer_query_mfunds($customer_id, $child);
//  dlog("", "mfunds statue query = " . $q);
  // there should be only one record returned
/*   if ($nullchild) */
/* $q = "SELECT application_id FROM htt_mfunds_applications WHERE parent_mfunds_person_id IN ( SELECT mfunds_person_id FROM htt_mfunds_applications WHERE customer_id = 236511 ) AND mfunds_person_id IS NULL"; */
  $all = mssql_fetch_all_objects(logged_mssql_query($q));

  foreach ($all as $c)
  {
    /* dlog("", "child 0 " . json_encode(array($q, $c))); */
    return $c;
  }

  return NULL;
}

function mfunds_status_mperson($person_id)
{
  $q = make_find_status_person_id_query_mfunds($person_id);
  // there should be only one record returned
  foreach (mssql_fetch_all_objects(logged_mssql_query($q)) as $c)
  {
    return $c;
  }

  return NULL;
}

function mfunds_children($customer_id)
{
  $ret = array();

  $q = make_find_children_query_mfunds($customer_id);
  foreach (mssql_fetch_all_objects(logged_mssql_query($q)) as $c)
  {
    $ret[] = $c->child;
  }

  return $ret;
}

function stream_image($file, $unlink=FALSE)
{
  if (file_exists($file))
  {
    $size = getimagesize($file);
    $fp = fopen($file, 'rb');

    if ($size > 0 and $fp)
    {
      // Optional never cache
      //  header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
      //  header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      //  header('Pragma: no-cache');
      // Optional cache if not changed
      //  header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file)).' GMT');

      // Optional send not modified
      //  if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) and
      //      filemtime($file) == strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']))
      //  {
      //      header('HTTP/1.1 304 Not Modified');
      //  }

        header('Content-Type: '.$size['mime']);
        header('Content-Length: '.filesize($file));

        fpassthru($fp);
        if ($unlink) unlink($file);
        exit;
    }
  }
  else
  {
    $out['errors'][] = "Could not open $file";
  }

}

function mfunds_image_name($url)
{
  $path=explode("?",$url);
  $fname=basename($path[0]);
  return md5($url) . "-" . $fname;
}

function mfunds_stamp_image($blank, $tmpdir, $customer_full, $person_id, $details)
{
  $dim = trim(shell_exec("/usr/bin/convert $blank -format \"%w\" info:"));
  $res = $details->CardDetailsResult;

  if (!$res)
  {
    return "no image, we're missing the result";
  }

  $cvv = $res->Cvv;
  $num = $res->CardNumber;
  $exp = $res->ExpireDateTime; // "6\\/30\\/2015 11:59:59 PM"

  $name = strtoupper($customer_full->FIRST_NAME . ' ' . $customer_full->LAST_NAME);

  if ($cvv && $num && $exp)
  {
    $matches = array();
    if (preg_match("/^(\d+)\/\d+\/20(\d+)/", $exp, $matches))
    {
      $exp = sprintf("%02d/%02d", $matches[1], $matches[2]);
    }

    $of = "$tmpdir/mfunds-" . substr(create_guid('mFunds'), 1, -1) . ".gif";
    $master_command = sprintf('/usr/bin/convert "%%s" \( -size %dx -geometry +0+0 -background none -fill %%s -font Courier-Bold -stroke silver -pointsize %%d -gravity west caption:%%s \) -gravity north -geometry +%%d+%%d -compose over -composite "%%s"', $dim);

    $num = substr($num,0,4) . ' ' .  substr($num,4,4) . ' ' .  substr($num,8,4) . ' ' .  substr($num,12,4);

    $command = sprintf($master_command, $blank, 'white', 41, escapeshellarg($num), 55, 209, $of);
    dlog("","convert command 1 [$command]");
    system($command);

    $command = sprintf($master_command, $of, 'black', 34, escapeshellarg($cvv), 295, 407, $of);
    dlog("","convert command 2 [$command]");
    system($command);

    $command = sprintf($master_command, $of, 'white', 31, escapeshellarg($exp), 263, 278, $of);
    dlog("","convert command 3 [$command]");
    system($command);

    $command = sprintf($master_command, $of, 'white', 24, escapeshellarg($name), 60, 320, $of);
    dlog("","convert command 4 [$command]");
    system($command);

    return $of;
  }
  else
  {
    return "no image, we're missing the details";
  }
}

function mfunds_config($key)
{
  global $mfunds_config;

  return $mfunds_config[$key];
}

$mfunds_soap = new SoapClient((mfunds_config('url/service') . "HTTWebService.asmx?wsdl"),
                      array("exceptions" => 1));

//  HT = 17898
//  IndiaLD = 17897
//  UVCard = 17896
$mfunds_soap_company_id = "17898";

$mfunds_soap_params = array();
$mfunds_soap_nonet = FALSE;

function mfunds_call($function, $given_params, $customer_full, $person_id)
{
  global $mfunds_soap;
  global $mfunds_soap_company_id;
  global $mfunds_soap_params;
  global $mfunds_soap_nonet;
  global $mfunds_config;

  $prep_params = array_merge(array(
                               'partnerPassword' => 'U1tr@vi0let',
                               'ClientId' => mfunds_config('id/client'),
                               'SubProgramId' => mfunds_config('id/sub/p'),
                               'PackageId' => mfunds_config('id/pkg/pv'),
                               'CompanyId' => $mfunds_soap_company_id,
                               'clientRefId' => 'HTT-' . $function . '-' . create_guid('mFunds'),
                               ), $given_params);
  $params = array();

  foreach ($prep_params as $key => $val)
  {
    $params[$key] = interpolate_fields($customer_full,
                                       $val,
                                       array("person_id" => $person_id));
    /* for mFunds, replace blank strings with spaces with an empty string */
    if (preg_match("/^\s+$/", $params[$key]))
    {
      $params[$key] = "";
    }
  }

  dlog("", "Built mFunds parameter list " . json_encode($params) .
            " from spec " . json_encode($given_params));

  $mfunds_soap_params = $params;
  try
  {
    if ($mfunds_soap_nonet) return NULL;
    $l_xSoap = $mfunds_soap->$function($params);

    logged_mssql_query('SOAP_response: ' . serialize($l_xSoap), TRUE);

    return $l_xSoap;

    //return $mfunds_soap_nonet ? NULL : $mfunds_soap->$function($params);
  }
  catch (SoapFault $E)
  {
    logged_mssql_query("SOAP error: " . $E->faultstring, TRUE);
  }
}

// status can be 1 or 2
function mfunds_CardStatus($params=array(), $customer_full, $person_id)
{
  return mfunds_call('CardStatus', $params, $customer_full, $person_id);
}

function mfunds_OrderCardHTT($params=array(), $customer_full, $person_id)
{
  return mfunds_call('OrderCardHTT', $params, $customer_full, $person_id);
}

function mfunds_OrderFamilyCardHTT($params=array(), $customer_full, $person_id)
{
  return mfunds_call('OrderFamilyCardHTT', $params, $customer_full, $person_id);
}

function mfunds_CardDetails($params=array(), $customer_full, $person_id)
{
  return mfunds_call('CardDetails', $params, $customer_full, $person_id);
}

function mfunds_AccountBalance($params=array(), $customer_full, $person_id)
{
  return mfunds_call('AccountBalance', $params, $customer_full, $person_id);
}

function mfunds_ChangeMobileNumber($params=array(), $customer_full, $person_id)
{
  return mfunds_call('ChangeMobileNumber', $params, $customer_full, $person_id);
}

function mfunds_GetMobileNumber($params=array(), $customer_full, $person_id)
{
  $m = mfunds_status($customer_full->CUSTOMER_ID, $person_id);
  return $m->mobile_number;
}

function mfunds_LoadAccountHTT($params=array(), $customer_full, $person_id)
{
  return mfunds_call('LoadAccountHTT', $params, $customer_full, $person_id);
}

function mfunds_ConvertCardHTT($params=array(), $customer_full, $person_id)
{
  return mfunds_call('ConvertCardHTT', $params, $customer_full, $person_id);
}

function mfunds_CreateMobileWallet($params=array(), $customer_full, $person_id)
{
  return mfunds_call('CreateMobileWallet', $params, $customer_full, $person_id);
}

function mfunds_DirectAccessNumber($params=array(), $customer_full, $person_id)
{
  return mfunds_call('DirectAccessNumber', $params, $customer_full, $person_id);
}

function mfunds_UpdateCardholderDetails($params=array(), $customer_full, $person_id)
{
  return mfunds_call('UpdateCardholderDetails', $params, $customer_full, $person_id);
}

?>
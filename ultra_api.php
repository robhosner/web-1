<?php

// http://wiki.hometowntelecom.com:8090/display/SPEC/API+Standard+Guide+2.0
putenv("UNIT_TESTING=0");

require_once("core.php");
require_once("session.php");
require_once("web.php");
require_once('lib/util-common.php');
require_once('classes/postageapp.inc.php');
require_once('Ultra/Lib/Api.php');

$api = new \Ultra\Lib\Api;

if ( $api->result->has_errors() )
{
  $api->terminate( array() );
}
else
{
  $api->execute();
}

register_shutdown_function(function() {
  \Ultra\Lib\DB\closeDatabaseConnection();
});
/*
curl -i 'https://$DOMAIN/ultra_api.php?bath=rest&partner=internal&version=1&command=internal__ResolvePendingTransitions&format='
*/


#!/bin/bash
output=`CONFIG_SERVICE=$CONFIG_SERVICE php LoadConfiguration.php $PHP_ENV`;

if [ ! -z "$output" ]
then
    echo $output;
    exit;
fi

if [ -z "$SITE" ]
then
    site=api-qa.ultra.me;
else
    site=$SITE;
fi

`sed -ie "s/ultra_api_dev/$PHP_ENV/g; s/api-dev.ultra.me/$site/g;" /etc/nginx/sites-enabled/api.ultra.me.conf`

`rm /etc/nginx/sites-enabled/api.ultra.me.confe`

`ln -s /home/ht/www/ultra_api_dev /home/ht/www/$PHP_ENV`

`echo "" > /etc/opt/remi/php71/php.d/15-xdebug.ini`

supervisord -n
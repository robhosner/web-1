<?php
require_once 'Ultra/Lib/DB/ImeiHistory.php';
require_once 'Ultra/Lib/DB/MSSQL.php';
/*
add_to_imei_history(
  array(
    'msisdn' => '1001001008',
    'iccid'  => '1234567890123456789',
    'imei'   => '2123456789012345'
  )
);
*/

/**
 * get_imei_info
 *
 * Retrieve phone/imei info
 * Example: {"IMEI":"35829903","make":"NOKIA","model":"5230","model_name":" ","os":"SYMBIAN OS V9.4, SERIES 60 REL. 5"}
 *
 * @param  string $imei
 * @return object or NULL
 */
function get_imei_info( $imei )
{
  if ( ! preg_match("/^[0-9]+$/",$imei) )
    return NULL;

  $sql = sprintf(
    "EXEC ULTRA.Get_IMEI_INFO '%s'",
    $imei
  );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) && count($data) )
    return $data[0];

  return NULL;
}

/**
 * get_imei_history
 * Queries IMEI_HISTORY by MSISDN or ICCID
 * $params: msisdn
 *          iccid
 *
 * @param  array $params
 * @return object or NULL
 */
function get_imei_history( $params )
{
  $where = '';

  if ( isset($params['msisdn']) )
  {
    $where = sprintf(" MSISDN = %s",mssql_escape_with_zeroes($params['msisdn']));
  }
  elseif ( isset($params['iccid']) )
  {
    $where = sprintf(" ICCID = %s",mssql_escape_with_zeroes($params['iccid']));
  }
  else
  {
    return NULL;
  }

  $sql = " SELECT TOP 1 * FROM ULTRA.IMEI_HISTORY WITH (nolock) WHERE $where ORDER BY IMEI_RECORD_DATE DESC";

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) && count($data) )
    return $data[0];

  return NULL;
}

/**
 * add_to_imei_history
 * Adds a row into IMEI_HISTORY
 * $params: imei
 *          msisdn
 *          iccid
 *
 * @param  array $params
 * @return NULL
 */
function add_to_imei_history( $params , $redis=NULL )
{
  try
  {
    if ( !is_array( $params ) || !isset($params['imei']) || ( $params['imei'] == '' ) )
      throw new \Exception("No IMEI given");

    if ( is_null($redis) )
      $redis = new \Ultra\Lib\Util\Redis;

    $redisKey = 'ultra/mvne/imei/'.$params['imei'];

    if ( $redis->get( $redisKey ) )
      throw new \Exception("IMEI ".$params['imei']." has been stored recently in the DB");

    if ( strlen($params['msisdn']) == 11 )
      $params['msisdn'] = substr( $params['msisdn'] , 1 );

    if ( strlen( $params['imei'] ) == 14 )
      $params['imei'] = luhn_append( $params['imei'] );

    $params['iccid'] = luhnenize( $params['iccid'] );

    if ( insert_imei_history( $params ) )
    {
      $redis->set( $redisKey , 1 , 30 * 60 );
    }
    else
    {
      throw new \Exception("insertImeiHistory error");
    }
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());
  }
}

/**
 * insert_imei_history
 * Adds a row into IMEI_HISTORY
 * $params: imei
 *          iccid
 *          msisdn
 *
 * @param  array $params
 * @return boolean
 */
function insert_imei_history( $params )
{
  $sql = sprintf("
    IF NOT EXISTS (
      SELECT IMEI
      FROM   ULTRA.IMEI_HISTORY
      WHERE  IMEI   = '%d'
      AND    ICCID  = '%d'
      AND    MSISDN = '%d'
    )
    BEGIN
      INSERT INTO ULTRA.IMEI_HISTORY
      ( IMEI , ICCID , MSISDN )
      VALUES
      (   '%d' , '%d' , '%d' )
    END ",
    $params['imei'],
    $params['iccid'],
    $params['msisdn'],
    $params['imei'],
    $params['iccid'],
    $params['msisdn']
  );

  return run_sql_and_check($sql);
}

?>

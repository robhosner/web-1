<?php

/**
 * trackBoltOn
 *
 * Add a new row to ULTRA.BOLTON_TRACKER
 *  $status              - "DONE","ISF", "REFUND"
 *  $sku                 - Example: 500MB@$5.00
 *  $product             - "IDDCA" , "DATA"
 *  $type                - "IMMEDIATE" , "MONTHLY"
 *  $transaction_id      - foreign key to HTT_BILLING_HISTORY
 *  $htt_plan_tracker_id - foreign key to HTT_PLAN_TRACKER
 *
 * @param  integer $customer_id
 * @param  string $source
 * @param  string $status
 * @param  string $sku
 * @param  string $product
 * @param  string $type
 * @param  integer $transaction_id
 * @param  integer $htt_plan_tracker_id
 * @return object of class \Result
 */
function trackBoltOn( $customer_id , $source , $status , $sku , $product , $type , $transaction_id=NULL , $htt_plan_tracker_id=NULL )
{
  if ( empty($product) )
    return make_error_Result('trackBoltOn error: product is missing');

  if ( empty($type) )
    return make_error_Result('trackBoltOn error: type is missing');

// TODO: clarify $transaction_id and $htt_plan_tracker_id

  if ( $transaction_id )
    $transaction_id = mssql_escape_with_zeroes( $transaction_id );
  else
  {
    $description = ($product === 'IDDCA' ? 'INTL' : $product) . ' Purchase';

    $transaction_id = sprintf(
      " ( SELECT TOP 1 TRANSACTION_ID
          FROM   HTT_BILLING_HISTORY H
          WHERE  DESCRIPTION = %s
          AND    ENTRY_TYPE  = 'SPEND'
          AND    RESULT      = 'COMPLETE'
          AND    CUSTOMER_ID = %d
          ORDER BY TRANSACTION_ID DESC ) ",
      mssql_escape_with_zeroes( $description ),
      $customer_id
    );
  }

  if ( $htt_plan_tracker_id && is_numeric( $htt_plan_tracker_id ) )
    $htt_plan_tracker_id = sprintf(" %d " , $htt_plan_tracker_id );
  else
    $htt_plan_tracker_id = sprintf(
      " ( SELECT TOP 1 HTT_PLAN_TRACKER_ID
          FROM   HTT_PLAN_TRACKER
          WHERE  CUSTOMER_ID = %d
          ORDER BY HTT_PLAN_TRACKER_ID DESC ) ",
      $customer_id
    );

  $sql = sprintf("
INSERT INTO ULTRA.BOLTON_TRACKER
(
CUSTOMER_ID,
SOURCE,
STATUS,
SKU,
PRODUCT,
REQUEST_TYPE,
TRANSACTION_ID,
HTT_PLAN_TRACKER_ID
)
VALUES
(
%d,
%s,
%s,
%s,
%s,
%s,
%s,
%s
) ",
    $customer_id,
    mssql_escape_with_zeroes( $source ),
    mssql_escape_with_zeroes( $status ),
    mssql_escape_with_zeroes( $sku ),
    mssql_escape_with_zeroes( $product ),
    mssql_escape_with_zeroes( $type ),
    $transaction_id,
    $htt_plan_tracker_id
  );

  return run_sql_and_check_result($sql);
}


/**
 * getCurrentTrackedBoltOns
 * return a list of tracked bolt ons applied during the current customer's plan period
 * @param Integer customer ID
 * @param Srray bolt on type (IMMEDIATE | MONTHLY) or NULL for any
 * @param String product type (DATA | IDDCA | VOICE) or NULL for any
 * @return Array of bolt on objects
 */
function getCurrentTrackedBoltOns($customer_id, $type = NULL, $product = NULL)
{
  $type = $type ? mssql_escape_with_zeroes($type) : NULL;
  $product = $product ? mssql_escape_with_zeroes($product) : NULL;
  
  $sql = sprintf('SELECT SKU, PRODUCT, REQUEST_DATE, STATUS
    FROM ULTRA.BOLTON_TRACKER t WITH (NOLOCK)
    JOIN HTT_CUSTOMERS_OVERLAY_ULTRA u WITH (NOLOCK) ON t.CUSTOMER_ID = u.CUSTOMER_ID
    WHERE t.CUSTOMER_ID = %d
    AND t.REQUEST_DATE >= u.PLAN_STARTED' .
    ($type ? " AND t.REQUEST_TYPE = $type" : NULL) . 
    ($product ? " AND t.PRODUCT = $product" : NULL) .
    ' ORDER BY t.REQUEST_DATE',
    $customer_id);

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

?>

<?php

/**
 * celluphone_pq_query 
 * @return string SQL
 */
function celluphone_pq_query()
{
  $txnDateStart = date('Y-m-d', strtotime('-5 months', time()));

  $sql = "set transaction isolation level read uncommitted;
    INSERT INTO [htt_celluphone_transactions]
    ([customer_id],
    transaction_date,
    plan_code,
    trans_type,
    charge_amount,
    payment_type,
    is_commissionable,
    bill_ref_id)
      SELECT h.customer_id,
        [transaction_date],
        CASE h.[cos_id]";

  $sql .= htt_celluphone_when_cos_id_then_plan_short();

  $sql .= "
        END                AS PLAN_CODE,
        'PQ'               AS TRANS_TYPE,
        0                  AS CHARGE_AMOUNT,
        'NA'               AS PAYMENT_TYPE,
        is_commissionable  AS IS_COMMISSIONABLE,
        h.[transaction_id] AS BILL_REF_ID
      FROM [htt_billing_history] h
      JOIN htt_customers_overlay_ultra o
      ON   o.customer_id = h.customer_id
      WHERE h.source IN ('EPAY', 'TCETRA', 'INCOMM', 'QPAY', 'WEBPOS')
      AND o.BRAND_ID != 3
      AND h.description IN ('Plan Recharge (PQ)','Wallet Balance Added (PQ)')
      AND h.cos_id IN ('" . htt_celluphone_cos_id_case_values() . ")
      AND h.transaction_date >= '$txnDateStart'
      AND h.transaction_id NOT IN (SELECT bill_ref_id
                               FROM   htt_celluphone_transactions WITH (nolock)
                               WHERE  bill_ref_id IS NOT NULL
                               AND [trans_type] = 'PQ')
    ORDER  BY transaction_date";

  return $sql;
}

/**
 * celluphone_transaction_newperiod_log_query 
 * @return string SQL
 */
function celluphone_transaction_newperiod_log_query()
{
  $txnDateStart = date('Y-m-d', strtotime('-5 months', time()));

  $sql = "set transaction isolation level read uncommitted;
          INSERT INTO [htt_celluphone_transactions]
            ([customer_id],
             transaction_date,
             plan_code,
             trans_type,
             charge_amount,
             payment_type,
             is_commissionable,
             bill_ref_id)
  SELECT h.customer_id,
       [transaction_date],
       CASE h.[cos_id]";

  $sql .= htt_celluphone_when_cos_id_then_plan_short();

  $sql .= "
        END                AS PLAN_CODE,
       'NP'               AS TRANS_TYPE,
       0                  AS CHARGE_AMOUNT,
       'NA'               AS PAYMENT_TYPE,
       1                  AS IS_COMMISSIONABLE,
       h.[transaction_id] AS BILL_REF_ID
  FROM   [htt_billing_history] h
  JOIN htt_customers_overlay_ultra o
  ON   o.customer_id = h.customer_id
  WHERE  h.transaction_date >= '$txnDateStart'
       AND o.BRAND_ID != 3
       AND entry_type = 'SPEND'
       AND detail IN ( 'Monthly Renewal', 'Plan Change Renewal', 'Reactivation', 'First Monthly Fee', 'Activation' )
       AND h.cos_id IN ('" . htt_celluphone_cos_id_case_values() . ")
       AND h.[transaction_id] NOT IN (SELECT bill_ref_id
                                      FROM   htt_celluphone_transactions WITH (nolock)
                                      WHERE  bill_ref_id IS NOT NULL
                                             AND [trans_type] = 'NP')
  ORDER  BY transaction_date";

  return $sql;
}

/**
 * celluphone_activation_log_query 
 * @return string SQL
 */
function celluphone_activation_log_query()
{
  $txnDateStart = date('Y-m-d', strtotime('-5 months', time()));

  $sql = "set transaction isolation level read uncommitted;
  insert into [HTT_CELLUPHONE_TRANSACTIONS] ([CUSTOMER_ID], TRANSACTION_DATE, PLAN_CODE, TRANS_TYPE, CHARGE_AMOUNT, PAYMENT_TYPE, IS_COMMISSIONABLE) 
  SELECT
    ACTIVATED_CUSTOMER_ID as CUSTOMER_ID,
    ACTIVATED_DATE as TRANSACTION_DATE,
    CASE h.INITIAL_COS_ID";

  $sql .= htt_celluphone_when_cos_id_then_plan_short();

  $sql .= "\nEND as PLAN_CODE,
    'AC' as TRANS_TYPE,
    0 as CHARGE_AMOUNT,
    'AC' as PAYMENT_TYPE,
    CASE
      WHEN s.[PRODUCT_TYPE] = 'ORANGE' THEN 0
      WHEN s.[ICCID_BATCH_ID] = 'UVIMPORT' THEN 0
      WHEN o.[NOTES] LIKE 'intra-brand port-in%' THEN 0
      ELSE 1
    END as IS_COMMISSIONABLE
  FROM [HTT_ACTIVATION_LOG] h
  JOIN [HTT_INVENTORY_SIM] s on h.[ICCID_FULL] = s.[ICCID_FULL]
  JOIN [HTT_CUSTOMERS_OVERLAY_ULTRA] o on o.[CUSTOMER_ID] = h.[ACTIVATED_CUSTOMER_ID]
  WHERE h.[ACTIVATED_CUSTOMER_ID] NOT IN (select customer_id from [HTT_CELLUPHONE_TRANSACTIONS] WITH (NOLOCK) where TRANS_TYPE='AC')
  AND o.BRAND_ID != 3
  AND ACTIVATED_DATE >= '$txnDateStart'
  AND h.INITIAL_COS_ID IN ('";

  $sql .= htt_celluphone_cos_id_case_values();

  $sql .= ") order by ACTIVATED_DATE";

  return $sql;
}

/**
 * celluphone_transaction_log_query 
 * @return string SQL
 */
function celluphone_transaction_log_query()
{
  $txnDateStart = date('Y-m-d', strtotime('-5 months', time()));

  $sql = "set transaction isolation level read uncommitted;
insert into [HTT_CELLUPHONE_TRANSACTIONS] ([CUSTOMER_ID], TRANSACTION_DATE, PLAN_CODE, TRANS_TYPE, CHARGE_AMOUNT, PAYMENT_TYPE, IS_COMMISSIONABLE, BILL_REF_ID) 
select h.CUSTOMER_ID, [TRANSACTION_DATE], case h.[COS_ID]";

  $sql .= htt_celluphone_when_cos_id_then_plan_short();

  $sql .= "\nEND as PLAN_CODE, 'BL' as TRANS_TYPE,
case
when COMMISSIONABLE_CHARGE_AMOUNT IS NOT NULL THEN COMMISSIONABLE_CHARGE_AMOUNT
else CHARGE_AMOUNT
end as CHARGE_AMOUNT,
case SOURCE
  WHEN 'EPAY' THEN 'EP'
  WHEN 'WEBCC' THEN 'CC'
  WHEN 'WEBPIN' THEN 'PN'
  WHEN 'PHONEPIN' THEN 'PN'
  WHEN 'INCOMM' THEN 'IN'
  WHEN 'CSCOURTESY' THEN 'CS'
  WHEN 'ORANGE_SIM' THEN 'IO'
  WHEN 'QPAY' THEN 'QP'
  WHEN 'IAS' THEN 'QP'
  WHEN 'TCETRA' THEN 'TC'
END as PAYMENT_TYPE, 
IS_COMMISSIONABLE, h.[TRANSACTION_ID] as BILL_REF_ID
from [HTT_BILLING_HISTORY] h
JOIN htt_customers_overlay_ultra o
ON   o.customer_id = h.customer_id
where ([IS_COMMISSIONABLE] =1 or SOURCE='ORANGE_SIM') and ENTRY_TYPE='LOAD' and SOURCE IN ('EPAY','WEBCC','WEBPIN','PHONEPIN','CSPIN','CSCOURTESY','ORANGE_SIM','INCOMM','QPAY', 'TCETRA', 'IAS')
AND o.BRAND_ID != 3
AND h.transaction_date >= '$txnDateStart'
and h.COS_ID in ('";

  $sql .= htt_celluphone_cos_id_case_values();

  $sql .= ") and h.[TRANSACTION_ID] not in  (select BILL_REF_ID from HTT_CELLUPHONE_TRANSACTIONS WITH (NOLOCK) where BILL_REF_ID is not null AND [trans_type] = 'BL')
order by TRANSACTION_DATE";

  return $sql;
}

/**
 * celluphone_qualified_spend_query
 * @see: MVNO-2838
 *
 * @return string SQL
 */
function celluphone_qualified_spend_query()
{
  return sprintf("EXEC [ULTRA].[InsertHTTCelluphoneTransactions] @TransactionDate = '%s'", date('Y-m-d', strtotime('-5 months', time())));
}

/**
 * celluphone_transaction_log_customercare_query 
 * @return string SQL
 */
function celluphone_transaction_log_customercare_query()
{
  $txnDateStart = date('Y-m-d', strtotime('-5 months', time()));

#only commision the first customer care event per customer that fits the metric
#therefore subsequent customer events won't qualify
  $sql = "set transaction isolation level read uncommitted;
with commissionable_transacts as (
select h.[TRANSACTION_ID], row_number() over (partition by h.[CUSTOMER_ID] order by [TRANSACTION_ID] asc) as rn
from HTT_BILLING_HISTORY h join [ACCOUNTS] a on  h.[CUSTOMER_ID]=a.customer_id
where [SOURCE]='CSCOURTESY'
and detail in ('customercare__AddCourtesyCash','customercare__AddCourtesyStoredValue')
and charge_amount > 18
and h.[TRANSACTION_DATE] between a.[CREATION_DATE_TIME] and dateadd(d,15,a.[CREATION_DATE_TIME]) 
and h.COS_ID in ('";

  $sql .= htt_celluphone_cos_id_case_values();

  $sql .= ")
)
insert into [HTT_CELLUPHONE_TRANSACTIONS] ([CUSTOMER_ID], TRANSACTION_DATE, PLAN_CODE, TRANS_TYPE, CHARGE_AMOUNT, PAYMENT_TYPE, IS_COMMISSIONABLE, BILL_REF_ID)
select  h.[CUSTOMER_ID], h.[TRANSACTION_DATE], case h.[COS_ID]";

  $sql .= htt_celluphone_when_cos_id_then_plan_short();

  $sql .= "\nEND as PLAN_CODE,
'BL' as TRANS_TYPE, 
case 
when h.charge_amount>59 then 59
when h.charge_amount<0 then 0
else h.charge_amount
end as CHARGE_AMOUNT,
'CS' as PAYMENT_TYPE, 1 as IS_COMMISSIONABLE, h.[TRANSACTION_ID] as BILL_REF_ID
 from commissionable_transacts t
join  HTT_BILLING_HISTORY h on t.transaction_id=h.[TRANSACTION_ID]
JOIN htt_customers_overlay_ultra o
ON   o.customer_id = h.customer_id
where t.rn=1
and h.[TRANSACTION_ID] not in  (select BILL_REF_ID from HTT_CELLUPHONE_TRANSACTIONS WITH (NOLOCK) where BILL_REF_ID is not null)
AND o.BRAND_ID != 3
AND h.transaction_date >= '$txnDateStart'
order by TRANSACTION_DATE";

  return $sql;
}

/**
 * celluphone_changeplan_log_query 
 * @return string SQL
 */
function celluphone_changeplan_log_query()
{
  $htt_celluphone_cos_id_case_values = htt_celluphone_cos_id_case_values();

  $sql = "declare @start_dt datetime = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 1, 0)
declare @end_dt datetime = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0);

--CTE_HPT_GA (sorted asc to grab initial plan tracker record) includes all plan_tracker records for each sub as long as the plan started on/before the reporting end date
with cte_hpt_ga as
(select customer_id, cos_id, period, plan_started, plan_expires, row_number() over (partition by customer_id order by period, plan_updated, plan_expires, plan_started) as 'RN'
 from htt_plan_tracker where plan_started < @end_dt and cos_id in
('" .

  $htt_celluphone_cos_id_case_values .

  ")
  and datediff(dd, plan_started, plan_expires) >= 30)
 
--CTE_HPT (sorted descending to grab latest plan tracker record)
,cte_hpt as
(select customer_id, cos_id, period, plan_started, plan_expires, row_number() over (partition by customer_id order by period desc, plan_updated desc, plan_expires desc, plan_started desc) as 'RN'
 from htt_plan_tracker where plan_started >= @start_dt and plan_started < @end_dt and cos_id in ('".get_cos_id_from_plan('L19')."','".

  $htt_celluphone_cos_id_case_values .

  ") and datediff(dd, plan_started, plan_expires) >= 30 and period = 1)

insert into [HTT_CELLUPHONE_TRANSACTIONS] ([CUSTOMER_ID], TRANSACTION_DATE, PLAN_CODE, TRANS_TYPE, CHARGE_AMOUNT, PAYMENT_TYPE, IS_COMMISSIONABLE) 
select u.[customer_id] as CUSTOMER_ID,
              getutcdate() as TRANSACTION_DATE,
              case hpt.cos_id";

  $sql .= htt_celluphone_when_cos_id_then_plan_short();

  $sql .= "\nEND as PLAN_CODE,
              'CP' as TRANS_TYPE,
              0 as CHARGE_AMOUNT,
              'CP' as PAYMENT_TYPE,
              case when s.[PRODUCT_TYPE] = 'ORANGE' THEN 0 else 1 end as IS_COMMISSIONABLE
from [htt_customers_overlay_ultra] u join [ACCOUNTS] a on u.[customer_id]=a.[CUSTOMER_ID]
join [HTT_INVENTORY_SIM] s on u.[ACTIVATION_ICCID_FULL]=s.[ICCID_FULL]
inner join cte_hpt_ga ga on u.customer_id = ga.customer_id and ga.rn = 1 and ga.plan_started >= @start_dt
inner join cte_hpt hpt on ga.customer_id = hpt.customer_id and hpt.rn = 1 and ga.cos_id != hpt.cos_id
where u.BRAND_ID != 3
order by u.[customer_id]
-- and a.COS_ID 
-- order by ACTIVATED_DATE";

  return $sql;
}

/**
 * htt_celluphone_when_cos_id_then_plan_short
 *
 * encapsulates a repeating SQL CASE pattern
 * WHEN '$cos_id' THEN '$plan_short'
 * WHEN '$cos_id' THEN '$plan_short'
 * [...]
 *
 * @return String SQL chunk
 */
function htt_celluphone_when_cos_id_then_plan_short()
{
  $sql = '';
  foreach (get_ultra_plans_short() as $plan_short)
    $sql .= "\nWHEN '" . get_cos_id_from_plan($plan_short) . "' THEN '$plan_short'";
  return $sql;
}

/**
 * htt_celluphone_cos_id_case_values
 *
 * encapsulates a repeating SQL IN pattern
 * generates:
 * 98280','98283','98274','98284','98275','98285','98276','98279'
 *
 * @return String SQL chunk
 */
function htt_celluphone_cos_id_case_values()
{
  $plan_names_short = get_ultra_plans_short();

  $sql = '';

  $i = 0;

  foreach ($plan_names_short as $plan_short)
  {
    $i++;
    $sql .= get_cos_id_from_plan($plan_short);
    $sql .= ($i != count($plan_names_short)) ? "','" : "'";
  }

  return $sql;
}


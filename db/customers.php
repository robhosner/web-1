<?php

# php component to access DB table CUSTOMERS

# synopsis

#echo customers_update_query(
#  array(
#    'first_name'  => 'Billy',
#    'last_name'   => 'Idol',
#    'customer_id' => 12345
#  )
#)."\n\n";

#echo customers_select_query(
#  array(
#    'select_fields' => array('CUSTOMER', 'LOGIN_NAME'),
#    'e_mail'        => 'e'
#  )
#)."\n\n";

#echo customers_select_query(
#  array(
#    'login_name' => 'l'
#  )
#)."\n\n";

/**
 * customer_reset_first_last_name 
 * @param  integer $customer_id
 * @return boolean
 */
function customer_reset_first_last_name($customer_id)
{
  $sql = sprintf("
    UPDATE CUSTOMERS
    SET    FIRST_NAME = '',
           LAST_NAME  = '',
           E_MAIL     = ''
    WHERE  CUSTOMER_ID = %d",
    $customer_id
  );

  return run_sql_and_check($sql);
}

/**
 * reset_credit_card 
 * @param  integer $customer_id
 * @return boolean
 */
function reset_credit_card($customer_id)
{
  $sql = sprintf("
    UPDATE CUSTOMERS
    SET    CC_NUMBER   = NULL,
           CC_EXP_DATE = NULL,
           CCV         = NULL
    WHERE  CUSTOMER_ID = %d",
    $customer_id
  );

  return run_sql_and_check($sql);
}

/**
 * reset_credit_card_info 
 * @param  integer $customer_id
 * @return boolean
 */
function reset_credit_card_info($customer_id)
{
  $sql = sprintf("
    UPDATE CUSTOMERS
    SET    CC_NAME         = NULL,
           CC_ADDRESS1     = NULL,
           CC_ADDRESS2     = NULL,
           CC_CITY         = NULL,
           CC_COUNTRY      = NULL,
           CC_STATE_REGION = NULL,
           CC_POSTAL_CODE  = NULL
    WHERE  CUSTOMER_ID     = %d",
    $customer_id
  );

  return run_sql_and_check($sql);
}

function customers_select_customer($params)
{
  $params['sql_limit'] = 1;

  $query     = customers_select_query($params);
  $customers = mssql_fetch_all_objects(logged_mssql_query($query));

  $customer = null;
  if (count($customers)) $customer = $customers[0];
  return $customer;
}

/**
 * customers_get_customer_by_customer_id
 *
 * @param  Integer $customer_id
 * @param  Array   $select_fields
 * @return Object or NULL, row from customers
 */
function customers_get_customer_by_customer_id($customer_id, $select_fields = NULL)
{
  $query = customers_select_query(array('customer_id' => $customer_id, 'select_fields' => $select_fields));
  return find_first($query);
}

/**
 * customers_select_query 
 * $params: select_fields
 *          e_mail
 *          login_name
 *          customer_id
 * 
 * @param  array $params
 * @return string SQL
 */
function customers_select_query($params)
{
  $top_clause = get_sql_top_clause($params);

  $select_fields = '*';

  $where_clause = '';

  if ( isset($params['select_fields']) )
  {
    $select_fields = implode(',',$params['select_fields']);
  }

  if ( isset($params['e_mail']) )
  {
    $where_clause = sprintf("E_MAIL = %s",
      mssql_escape_with_zeroes($params['e_mail'])
    );
  }

  if ( isset($params['login_name']) )
  {
    $where_clause = sprintf("LOGIN_NAME = %s",
      mssql_escape_with_zeroes($params['login_name'])
    );
  }

  if ( isset($params['customer_id']) && is_numeric($params['customer_id']) )
  {
    $where_clause = sprintf("customer_id = %d",
      $params['customer_id']
    );
  }

  $query = "SELECT $top_clause $select_fields FROM CUSTOMERS WHERE $where_clause";

  return $query;
}

/**
 * reset_customer_password 
 * @param  integer $customer_id
 * @param  string  $password to reset to
 * @return boolean
 */
function reset_customer_password( $customer_id, $password = null )
{
  $password = ($password) ? mssql_escape_with_zeroes($password) : "'NULL'";
  if ( is_numeric( $customer_id ) )
  {
    $sql = sprintf("UPDATE CUSTOMERS SET LOGIN_PASSWORD = %s WHERE CUSTOMER_ID = %d",$password,$customer_id);

    return is_mssql_successful(logged_mssql_query($sql));
  }

  return FALSE;
}

/**
 * 
 * @param string $new_login_name
 * @return boolean -- true == it is not present, false == the name exists
 */
function check_unique_login_name( $new_login_name )
{
  $query = sprintf(
          "SELECT COUNT(*) FROM CUSTOMERS
          WHERE LOGIN_NAME = %s",
          mssql_escape_with_zeroes($new_login_name) 
    );
  
  $query_result = mssql_fetch_all_rows(logged_mssql_query($query));

  if( $query_result[0][0] === 1 )
  {
    dlog("", "UNIQUE LOGIN NAME UNSUCCESSFUL %s, USER NAME EXISTS", $new_login_name);
    return false;
  }
  else 
  {
    dlog("", "UNIQUE LOGIN NAME SUCCESSFUL %s DOES NOT EXIST", $new_login_name);
    return true;
  }
  
}

/**
 * customers_update_query
 * $params: FIRST_NAME
 *          LAST_NAME
 *          COMPANY
 *          ADDRESS1
 *          ADDRESS2
 *          CITY
 *          STATE_REGION
 *          POSTAL_CODE
 *          COUNTRY
 *          LOCAL_PHONE
 *          FAX
 *          E_MAIL
 *          LOGIN_NAME
 *          LOGIN_PASSWORD
 *          CC_NAME
 *          CC_ADDRESS1
 *          CC_ADDRESS2
 *          CC_CITY
 *          CC_COUNTRY
 *          CC_STATE_REGION
 *          CC_POSTAL_CODE
 *          CC_NUMBER
 *          CC_EXP_DATE
 *          CCV
 *          SET_NULL_LOGIN
 *          SET_NULL_CREDIT_CARD_DATA
 *          
 * @param  array $params
 * @return string SQL
 */
function customers_update_query($params)
{
  dlog('', "(%s)", func_get_args());

  $set_clause_array = array();

  $update_fields = array(
   'FIRST_NAME',
   'LAST_NAME',
   'COMPANY',
   'ADDRESS1',
   'ADDRESS2',
   'CITY',
   'STATE_REGION',
   'POSTAL_CODE',
   'COUNTRY',
   'LOCAL_PHONE',
   'FAX',
   'E_MAIL',
   'LOGIN_NAME',
   'LOGIN_PASSWORD',
   'CC_NAME',
   'CC_ADDRESS1',
   'CC_ADDRESS2',
   'CC_CITY',
   'CC_COUNTRY',
   'CC_STATE_REGION',
   'CC_POSTAL_CODE',
   'CC_EXP_DATE'
   # we may add new fields in this list
  );

  foreach( $update_fields as $update_field )
    if ( isset($params[ strtolower($update_field) ]) && ( $params[ strtolower($update_field) ] != "" ) )
    {
      $set_clause_array[] = sprintf(
        $update_field." = %s",
        mssql_escape_with_zeroes($params[ strtolower($update_field) ])
      );
    }

  if ( isset($params['SET_NULL_LOGIN']) )
    $set_clause_array[] = " LOGIN_NAME = NULL , LOGIN_PASSWORD = NULL ";

  if ( isset($params['SET_NULL_CREDIT_CARD_DATA']) )
    $set_clause_array[] = " CC_NUMBER = NULL , CC_EXP_DATE = NULL , CC_NAME = NULL, CCV = NULL ";

  $set_clause = implode(" , ", $set_clause_array);

  $query = sprintf(
    "UPDATE CUSTOMERS
    SET $set_clause
    WHERE CUSTOMER_ID = %d",
    $params['customer_id']
  );

  return $query;
}

/**
 * customer_liberal_select_query 
 * this query does not check the cos_id or ultra overlay for validity
 * $params: customer_id
 * 
 * @param  array $params
 * @return string SQL
 */
function customer_liberal_select_query($params)
{
  $query = sprintf(
    "SELECT * FROM customers c
LEFT JOIN accounts a ON c.customer_id = a.customer_id
LEFT JOIN htt_customers_overlay_ultra u ON c.customer_id = u.customer_id
WHERE c.customer_id = %d",
    $params['customer_id']
  );

  return $query;
}

/**
 * exists_customer_number 
 * @param  string $customer_n
 * @return boolean
 */
function exists_customer_number($customer_n)
{
  $exists_customer_number = FALSE;

  $sql = "SELECT customer FROM customers WHERE customer = '$customer_n'";

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  return ! ! ( $query_result && is_array($query_result) && ( count($query_result) == 1 ) );
}

/**
 * get_unique_customer_number 
 * @return integer $customer_n
 */
function get_unique_customer_number()
{
  $customer_n = generate_unique_customer_number();

  while( exists_customer_number($customer_n) )
  {
    sleep(1);

    $customer_n = generate_unique_customer_number();
  }

  return $customer_n;
}

/**
 * generate_unique_customer_number 
 * @return integer
 */
function generate_unique_customer_number()
{
  // 4 digits random number
  // last 2 digits of getmypid()
  // last 6 digits of time()

  return rand(1000,9999). substr( getmypid(), -2 ) . substr( time() , -6);
}

/**
 * reset_user_8_select_query 
 * @return string SQL
 */
function reset_user_8_select_query()
{
  return "SELECT customer, user_8
FROM customers
WHERE user_8 = '1'";
}

/**
 * reset_user_8_update_query 
 * @return string SQL
 */
function reset_user_8_update_query()
{
  return "UPDATE customers
SET user_8 = '4'
WHERE user_8 = '1'";
}

/**
 * customer_set_null_credit_card_data 
 * $params: SET_NULL_CREDIT_CARD_DATA
 *          customer_id
 * 
 * @param  array $params
 * @return boolean
 */
function customer_set_null_credit_card_data($params)
{
  $customers_update_query = customers_update_query(
    array(
      'SET_NULL_CREDIT_CARD_DATA' => $params['SET_NULL_CREDIT_CARD_DATA'],
      'customer_id'               => $params['customer_id']
    )
  );

  return run_sql_and_check($customers_update_query);
}

/**
 * customer_set_password 
 * @param  string $account_password
 * @param  integer $customer_id
 * @return boolean
 */
function customer_set_password($account_password, $customer_id)
{
  $query = customers_update_query(
    array(
      'login_password' => $account_password,
      'customer_id'    => $customer_id
    )
  );

  return is_mssql_successful(logged_mssql_query($query));
}

/**
 * customers_who_need_passwords_encrypted_query
 * @param  string  $cause 'CUSTOMER_ID > $customer_id'
 * @param  integer $limit
 * @return string  SQL
 */
function customers_who_need_passwords_encrypted_query($clause, $limit = 1)
{
  if (empty($clause))
    return NULL;

  $sql = 'SELECT TOP ' . $limit . ' CUSTOMER_ID, LOGIN_PASSWORD FROM CUSTOMERS WHERE ' . $clause
     . ' AND LEN(LOGIN_PASSWORD) < 65 AND LOGIN_PASSWORD IS NOT NULL AND LOGIN_PASSWORD != \'\' AND LOGIN_PASSWORD != \' \' '
     . ' ORDER BY CUSTOMER_ID ASC';

  return $sql;
}

/**
 * customers_clone
 * clones CUSTOMERS row data for one customer_id to another
 * @param  Integer $from_customer_id
 * @param  Integer $to_customer_id
 * @param  Integer $fields_to_copy row columns to copy
 * @return Result
 */
function customers_clone_by_customer_id($from_customer_id, $to_customer_id, $fields_to_copy)
{
  $result = new \Result();

  $sql = "SELECT TOP 1 " . implode(',', $fields_to_copy) . " FROM CUSTOMERS WITH (NOLOCK) WHERE CUSTOMER_ID = $from_customer_id";
  $from_row = find_first($sql);

  if (!$from_row)
  {
    $result->add_error("CUSTOMERS row doesn't not exist for CUSTOMER_ID $from_customer_id");
    return $result;
  }

  $i = 0;
  $values = "";
  foreach ($fields_to_copy as $field)
  {
    if (empty($from_row->$field))
      continue;

    if ($i) $values .= ',';
    $i++;

    $values .= "$field = '" . $from_row->$field . "'";
  }

  $sql = "UPDATE CUSTOMERS SET $values WHERE CUSTOMER_ID = $to_customer_id";

  if (! run_sql_and_check($sql))
  {
    $result->add_error("Could not update CUSTOMERS for CUSTOMER_ID $from_customer_id");
    return $result;
  }

  $result->succeed();
  return $result;
}

/**
 * get_customer_contact_phone
 * @param  integer $customer_id
 * @return string
 */
function get_customer_contact_phone($customer_id)
{
  $query = sprintf(
    'SELECT CONTACT_PHONE FROM HTT_CUSTOMER_CONTACT_PHONE
      WHERE CUSTOMER_ID = %d',
    $customer_id
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($query));
  if (count($result) && !empty($result[0]->CONTACT_PHONE)) {
    return $result[0]->CONTACT_PHONE;
  } else {
    return null;
  }
}

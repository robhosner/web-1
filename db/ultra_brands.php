<?php

/**
 * get_brand_from_brand_id
 * @param  Integer $brand_id primary_key of brand row
 * @return Object  row from ULTRA.BRANDS
 */
function get_brand_from_brand_id($brand_id)
{
  $query = sprintf('
    DECLARE @BRAND_ID SMALLINT = %d;
    SELECT * FROM ULTRA.BRANDS WITH (NOLOCK) WHERE BRAND_ID = @BRAND_ID',
    $brand_id
  );
  return find_first($query);
}

/**
 * get_brand_from_short_name
 * @param  String $brand_short_name
 * @return Object row from ULTRA.BRANDS
 */
function get_brand_from_short_name($brand_short_name)
{
  $query = sprintf('
    DECLARE @SHORT_NAME VARCHAR(16) = \'%s\';
    SELECT * FROM ULTRA.BRANDS WITH (NOLOCK) WHERE SHORT_NAME = @SHORT_NAME',
    $brand_short_name
  );
  return find_first($query);
}

/**
 * get_short_name_from_brand_id
 * @param  String $brand_short_name
 * @return Object row from ULTRA.BRANDS
 */
function get_short_name_from_brand_id($brand_id)
{
  $brand = get_brand_from_brand_id($brand_id);
  return ($brand) ? $brand->SHORT_NAME : NULL;
}

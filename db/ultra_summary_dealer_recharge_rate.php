<?php

/*
DB table [ULTRA].[SUMMARY_DEALER_RECHARGE_RATE]
*/

/**
 * get_summary_dealer_recharge_rate_by_dealers
 *
 * @return array
 */
function get_summary_dealer_recharge_rate_by_dealers( $dealers, $start = NULL )
{
  if ( ! is_array( $dealers ) )
    return array();
  
  $start = $start ? "AND ACTIVATION_PERIOD_STARTED = '$start' " : NULL;

  $sql =
   'SELECT ACTIVATION_PERIOD_STARTED AS activation_period_started,
        ACTIVATION_PERIOD_ENDED AS activation_period_ended,
        PERIOD_ONE_ENDED AS period_one_ended,
        SUM( ACTIVATIONS ) sum_activations,
        SUM( REMAINING )   sum_remaining,
        SUM( RECHARGED )   sum_recharged,
        0                  sum_suspended,
        0                  sum_recharge_rate
    FROM   ULTRA.SUMMARY_DEALER_RECHARGE_RATE
    WHERE  DEALER IN ('.implode(' , ',$dealers).') ' . $start .
   'GROUP BY ACTIVATION_PERIOD_STARTED, ACTIVATION_PERIOD_ENDED, PERIOD_ONE_ENDED
    ORDER BY ACTIVATION_PERIOD_STARTED, PERIOD_ONE_ENDED';

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  for ( $i = 0 ; $i < count($data) ; $i++ )
  {
    $data[ $i ]->sum_suspended = $data[ $i ]->sum_activations - ( $data[ $i ]->sum_recharged + $data[ $i ]->sum_remaining ) ;

    if ( $data[ $i ]->sum_activations - $data[ $i ]->sum_remaining )
      $data[ $i ]->sum_recharge_rate = round( ( $data[ $i ]->sum_recharged / ( $data[ $i ]->sum_activations - $data[ $i ]->sum_remaining ) ) , 2 );
    else
      $data[ $i ]->sum_recharge_rate = 0;
  }

  dlog('',"data = %s",$data);

  return $data;
}

/**
 * get_second_month_recharge_rate_by_dealer
 *
 * Not the current month.
 * Only previous two months.
 *
 * @return array
 */
function get_second_month_recharge_rate_by_dealer( $dealer )
{
  if ( empty($dealer) )
    return array();
  
  if ( is_array($dealer) )
  {
    foreach ($dealer as &$item)
      $item = sprintf('%d', $item);
    $dealer = implode(',', $dealer);
  }
  else
    $dealer = sprintf('%d', $dealer);

  $sql =
   'SELECT 
      ACTIVATION_PERIOD_STARTED activation_period_started,
      PERIOD_ONE_ENDED          period_one_ended,
      sum(ACTIVATIONS)          activations,
      sum(RECHARGED)            recharged,
      sum(REMAINING)            remaining,
      0                         recharge_rate,
      CASE 
        WHEN DATEDIFF( mm , ACTIVATION_PERIOD_ENDED , getutcdate() ) = 2 THEN "prior_month" 
        WHEN DATEDIFF( mm , ACTIVATION_PERIOD_ENDED , getutcdate() ) = 3 THEN "two_months_prior" 
        ELSE "current_month" END month
    FROM  ULTRA.SUMMARY_DEALER_RECHARGE_RATE
    WHERE DEALER IN (' . $dealer . ')
    AND   ACTIVATION_PERIOD_ENDED < GETUTCDATE()
    AND   DATEDIFF( mm , ACTIVATION_PERIOD_ENDED , getutcdate() ) < 4
    GROUP BY ACTIVATION_PERIOD_STARTED, PERIOD_ONE_ENDED, ACTIVATION_PERIOD_ENDED
    ORDER BY ACTIVATION_PERIOD_STARTED DESC';

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  dlog('',"data = %s",$data);

  $result = array(
    'current_month'    => new stdClass(),
    'prior_month'      => new stdClass(),
    'two_months_prior' => new stdClass()
  );

  foreach ($data as &$month)
  {
    if ($month->activations - $month->remaining)
      $month->recharge_rate = round(($month->recharged / ($month->activations - $month->remaining)), 2);

    $result[ $month->month ] = $month;

    unset ( $result[ $month->month ]->month );
  }

  return $result;
}

/**
 * fill_second_month_recharge_rate_query
 * @return string query
 */
function fill_second_month_recharge_rate_query()
{
  $dealerPortal = \Ultra\UltraConfig\celluphoneDb();

  return "
delete from ultra.summary_dealer_recharge_rate where suspend_ended = 0;

declare @start_date as datetime = (select coalesce(DATEADD(mm, 1, DATEADD(MONTH, DATEDIFF(MONTH, 0, max(activation_period_started)), 0)), '20121001') as [first_activation_period_started_for_insert] from ultra.summary_dealer_recharge_rate where suspend_ended = 1) --Determines the earliest activation dates that need to be inserted into the table
declare @end_date as datetime = cast(getdate() as date);
                              
--select the max plan expires associated with all subscribers who only have 1 period. This will be used to determine whether a subscriber had the opportunity to have a 2nd Month Recharge or not if no recharge (period = 2) record is present
select customer_id ,cast(max(dbo.utc_to_pt(plan_expires)) as date) as plan_expires 
into #acq_max
from htt_plan_tracker a 
where period = 1 
and not exists (select * from htt_plan_tracker b where a.customer_id = b.customer_id and period > 1) group by customer_id;

--index the #acq_max table
create index idx1_acq_max_cid_pe on #acq_max (customer_id, plan_expires);

--pull all period = 1 records from htt_plan_tracker
select 
		customer_id
		,cos_id --cos_id at activation
		,cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,dbo.utc_to_pt(plan_started))+1,0)) as date) [ME]
		,cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, dbo.utc_to_pt(plan_started)), 0) as date) as Activation_Period_Started
		,cast(DATEADD(MM, DATEDIFF(MM, -1, dbo.utc_to_pt(plan_started)), 0) - 1 as date) as Activation_Period_Ended
		,cast(dbo.utc_to_pt(plan_started) as date) as plan_started
		,row_number() over (partition by customer_id order by plan_started, plan_expires) as rn --will be used later to filter subscriber records to ensure 1 record per sub from htt_plan_tracker is pulled into the results
into #acq
from [HTT_PLAN_TRACKER] 
where dbo.utc_to_pt(plan_started) >= @start_date and dbo.utc_to_pt(plan_started) < @end_date 
and period = 1 
and plan_updated is null --should grab first plan tracker record for a sub (if a plan change occurred during the billing cycle, this would pick up the record before the plan changes);

--index the #acq table
create index idx1_acq_cid on #acq (customer_id);

--pull all subs that had a 2nd Month Recharge (a record in htt_plan_tracker with a period = 2)
with second_recharge as 
(
	select customer_id from [HTT_PLAN_TRACKER] where period = 2 group by customer_id
)

--pull a list of dealers, their parents, and the masteragent associated with the parent
,dealers_master as
(
	select 
			tds.dealersiteid --child dealer
			,tds2.dealersiteid as parentdealersiteid --parent dealer
			,tds2.masterid --masteragent id associated with parent dealer
			,replace(replace(replace(m2.businessname, char(9), ''), CHAR(13), ''), CHAR(10), '') as MasterAgent --masteragent name associated with parent dealer
	from $dealerPortal..tblDealerSite tds
	inner join $dealerPortal..tblDealerSite tds2 on tds2.dealersiteid = tds.parentdealersiteid
	inner join $dealerPortal..tblMaster m on m.MasterID = tds2.MasterID --grabs the subdistributor associated with the parentdealer record which will ultimately be used to grab the masteragent for the parent dealer
	inner join $dealerPortal..tblMaster m2 on m2.MasterID = m.ParentMasterId --grabs the masteragent associated with the parent dealer record
	where m2.[BusinessName] not in ('NULL','Global Mobile Konnections, LLC','Ultra Mobile Master Agent','Ultra Promotions','Ultra Giveaways','Ultra TV Master Agent') --exclude doors that rollup to the preceding masteragents
)

--customer level summary
,customer_summary as 
(
	select a.me as acq_month
			,a.plan_started
			,a.cos_id
			,a.customer_id
			,case when r.customer_id is not null then 1 else 0 end as Recharge --subs who have rechraged as of when the query is run
			,case when acq_max.plan_expires >= @end_date and r.customer_id is null then 1 else 0 end as Remaining --subs who have had not had an opportunity to recharge as of when the query is run
			,case when acq_max.plan_expires < @end_date and r.customer_id is null then 1 else 0 end as [No Recharge] --subs who have had an opportunity to recharge, but haven't as of when the query is run
			,ds.parentdealersiteid
			,ds.masteragent
			,a.Activation_Period_Started
			,a.Activation_Period_Ended
	from #acq a 
	left join second_recharge r on a.customer_id = r.customer_id
	left join htt_customers_overlay_ultra hcou on a.customer_id = hcou.customer_id
	left join #acq_max acq_max on a.customer_id = acq_max.customer_id
	join ultra.htt_activation_history hal on hal.iccid_full = hcou.activation_iccid_full and hal.final_state ='Completed' and hal.activation_type is not null
	join dealers_master ds on hal.dealer = ds.dealersiteid --used to grab parent dealer record and the masteragent associated with the parent dealer
	where a.rn = 1 --ensures that each subscriber is pulled into the results once even if multiple records for the same sub exist in htt_plan_tracker
	group by a.me
			,a.plan_started
			,a.cos_id
			,a.customer_id
			,case when r.customer_id is not null then 1 else 0 end
			,case when acq_max.plan_expires >= @end_date and r.customer_id is null then 1 else 0 end
			,case when acq_max.plan_expires < @end_date and r.customer_id is null then 1 else 0 end
			,ds.parentdealersiteid
			,ds.masteragent
			,a.Activation_Period_Started
			,a.Activation_Period_Ended
)
--dealer level summary
,dealer_summary as 
(
	select a.acq_month
			,a.cos_id
			,a.parentdealersiteid as dealer_id
			,a.Activation_Period_Started
			,a.Activation_Period_Ended
			,count(distinct customer_id) as Activations
			,coalesce(count(distinct case when [No Recharge] = 1 then customer_id else null end), 0) as No_Recharge
			,coalesce(count(distinct case when recharge = 1 then customer_id else null end), 0) as Recharged
			,coalesce(count(distinct case when Remaining = 1 then customer_id else null end), 0) as Remaining
	from customer_summary a
	group by 
			a.acq_month
			,a.cos_id
			,a.parentdealersiteid
			,a.Activation_Period_Started
			,a.Activation_Period_Ended
)

--insert final dealer results into ultra.summary_dealer_recharge_rate
insert into ultra.summary_dealer_recharge_rate
select Dealer_id
		,Activation_Period_Started
		,Activation_Period_Ended
		,Activations
		,Remaining
		,Recharged
		,case when datediff(dd, activation_period_ended, @end_date) >= 31 then 1 else 0 end as Period_One_Ended
		,case when datediff(dd, activation_period_ended, @end_date) >= 61 then 1 else 0 end as Suspend_Ended
		,coalesce(Recharged/nullif((Activations-Remaining)*1.0,0),0) as Recharge_Rate
		,cos_id
from dealer_summary
order by 2,1,10;

--clean up
drop table #acq_max;
drop table #acq;
";
}


/**
 * get_summary_dealer_recharge_rate_by_month
 * return dealer and children activation statistics for the given month
 * @see SMR-13
 * @param array of dealer IDs
 * @param start ACTIVATION_PERIOD_START date in MM/DD/YYYY format
 * @return array or rechange plans
 */
function get_summary_dealer_recharge_rate_by_month($dealers, $start)
{
  // verify parameters
  if ( ! is_array($dealers) || ! count($dealers) || empty($start))
  {
    dlog('', 'ERROR: invalid parameters %s', func_get_args());
    return array();
  }

  // prepare SQL
  $dealers = implode(',', $dealers);
  $sql = "SELECT
      ACTIVATION_PERIOD_STARTED AS period_started,
      ACTIVATION_PERIOD_ENDED AS period_ended,
      PERIOD_ONE_ENDED AS period_one_ended,
      sum(ACTIVATIONS) AS activations,
      sum(RECHARGED) AS recharged,
      sum(REMAINING) AS remaining,
      0 AS suspended,
      0 AS recharge_rate,
      COS_ID AS plan_name
    FROM ULTRA.SUMMARY_DEALER_RECHARGE_RATE
    WHERE DEALER IN ($dealers) AND ACTIVATION_PERIOD_STARTED = '$start'
    GROUP BY ACTIVATION_PERIOD_STARTED, ACTIVATION_PERIOD_ENDED, PERIOD_ONE_ENDED, COS_ID
    ORDER BY COS_ID";

  // get results and post-process them
  $activations = mssql_fetch_all_objects(logged_mssql_query($sql));
  
  $planIdentifiers = [
    'L' => 'Ultra',
    'S' => 'Ultra',
    'D' => 'Ultra',
    'UV' => 'Univision',
  ];
  
  $planNamesList = [];
  $duplicatePlansIndexes = [];

  // process each plan and sum up to Combined
  for ($i = 0; $i < count($activations); $i++)
  {
    $activations[$i]->suspended = $activations[$i]->activations - $activations[$i]->recharged - $activations[$i]->remaining;

    if ($activations[$i]->activations - $activations[$i]->remaining)
    {
      $activations[$i]->recharge_rate = round(($activations[$i]->recharged / ($activations[$i]->activations - $activations[$i]->remaining)), 2);
    }
    
    $activations[$i]->plan_name = get_plan_from_cos_id($activations[$i]->plan_name);

    // split the plan identifier e.g., "L" from the plan cost "29"
    list($identifier, $amount) = preg_split('#(?<=[a-zA-Z])(?=\d)#', $activations[$i]->plan_name);

    if (array_key_exists($identifier, $planIdentifiers))
    {
      $planName = $planIdentifiers[$identifier] . " $" . $amount;
      $activations[$i]->plan_name = $planName;

      // if $planName already exists in $planNamesList then combined their amounts
      if (array_key_exists($planName, $planNamesList))
      {
        $activations[$i]->activations += $activations[$planNamesList[$planName]]->activations;
        $activations[$i]->recharged += $activations[$planNamesList[$planName]]->recharged;
        $activations[$i]->suspended += $activations[$planNamesList[$planName]]->suspended;
        $activations[$i]->remaining += $activations[$planNamesList[$planName]]->remaining;
        $activations[$i]->recharge_rate += $activations[$planNamesList[$planName]]->recharge_rate;

        // add index to array to be deleted afterwards.
        $duplicatePlansIndexes[] = $planNamesList[$planName];
      }
      else
      {
        $planNamesList[$activations[$i]->plan_name] = $i;
      }
    }
    else
    {
      logWarn('The plan prefix ' . $identifier . ' was not found.');
    }
  }

  // remove all duplicates from $activations
  for ($i = 0; $i < count($duplicatePlansIndexes); $i++)
  {
    unset($activations[$duplicatePlansIndexes[$i]]);
  }

  usort($activations, function($a, $b) {
    return strcmp($a->plan_name, $b->plan_name);
  });

  return $activations;
}

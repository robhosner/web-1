<?php

/**
 * update_cc_transactions_from_htt_billing_history
 * 
 * cross reference CC_TRANSACTIONS.TRANSACTION_ID to HTT_BILLING_HISTORY.TRANSACTION_ID
 * @param  array (cc_transactions_id, transaction_id)
 * @return boolean result of SQL UPDATE
 */
function update_cc_transactions_from_htt_billing_history( $params )
{
  $sql = updateUltraCCTransactionsPROC([
    'transaction_id'     => $params['billing_transaction_id'],
    'cc_transactions_id' => $params['cc_transactions_id']
  ]);

  return run_sql_and_check( $sql );
}

/**
 * update_cc_transactions
 * $params: cc_transactions_id
 *          status
 *          err_description
 *          transaction_id
 *          cc_transactions_id_parent
 *          merchant_transaction_id
 *          result
 *          completed
 *          
 * @param  array $params
 * @return boolean result of SQL UPDATE
 */
function update_cc_transactions( $params )
{
  // 'cc_transactions_id' must be defined
  if ( !isset($params['cc_transactions_id']) || !$params['cc_transactions_id'] )
  {
    dlog('',"Error: cc_transactions_id not provided");
    return FALSE;
  }

  $sql = updateUltraCCTransactionsPROC($params);

  return run_sql_and_check( $sql );
}

/**
 * add_to_ultra_cc_transactions
 *
 * TRANSACTION_ID is initially NULL because we insert a row into HTT_BILLING_HISTORY after inserting into ULTRA.CC_TRANSACTIONS
 * MERCHANT_TRANSACTION_ID is initially NULL, we obtain this after the gateway/merchant API call
 * STATUS is initially 'INITIATED'
 * RESULT is initially NULL
 * CREATED_DATE_TIME is GETUTCDATE()
 * COMPLETED_DATE_TIME is initially NULL
 *
 * $params: see ultra_cc_transactions_insert_query
 *          + cc_transactions_id_parent
 *
 * @param  array $params
 * @return boolean
 */
function add_to_ultra_cc_transactions( $params )
{
  dlog('', "(%s)", func_get_args());

  return run_sql_and_check( addToUltraCCTransactionsPROC( $params ) );
}

/**
 * ultra_cc_transactions_insert_query
 * $params: cc_holder_tokens_id
 *          customer_id
 *          charge_amount
 *          type
 *          cc_transactions_id_parent
 *          description
 *          gateway
 *          
 * @param  array $params
 * @return string $sql
 */
function ultra_cc_transactions_insert_query( $params )
{
  if (!isset($params['cc_holder_tokens_id']) || !$params['cc_holder_tokens_id'])
  {
    $params['cc_holder_tokens_id'] = sprintf("(
      SELECT TOP 1
        t.CC_HOLDER_TOKENS_ID
      FROM
        ULTRA.CC_HOLDER_TOKENS t
      JOIN
        ULTRA.CC_HOLDERS       h
      ON
        t.CC_HOLDERS_ID = h.CC_HOLDERS_ID
      WHERE
        h.CUSTOMER_ID = %d
      AND
        t.ENABLED     = 1
      AND
        h.ENABLED     = 1
      ORDER BY t.CC_HOLDER_TOKENS_ID DESC
      )",  
        $params['customer_id']
    );
  }

  if (!isset($params['completed_date_time']))
  {
    $params['completed_date_time'] = 'NULL';
  }
  else
  {
    if (strtolower($params['completed_date_time']) == 'now')
      $params['completed_date_time'] = 'GETUTCDATE()';
  }

  dlog('', "(%s)", func_get_args());

  $sql = sprintf("
INSERT INTO ULTRA.CC_TRANSACTIONS
(
  CC_HOLDER_TOKENS_ID,
  CUSTOMER_ID,
  CHARGE_AMOUNT,
  TYPE,
  DESCRIPTION,
  CC_TRANSACTIONS_ID_PARENT,
  COMPLETED_DATE_TIME,
  CUSTOMER_IP,
  DETAILS
) 
 VALUES (
  %s,
  %d,
  %s,
  %s,
  %s,
  %s,
  %s,
  '%s',
  %s
)",
    $params['cc_holder_tokens_id'],
    $params['customer_id'],
    $params['charge_amount'],
    mssql_escape_with_zeroes($params['type']),
    mssql_escape_with_zeroes($params['description']),
    $params['cc_transactions_id_parent'],
    $params['completed_date_time'],
    Session::getClientIp(),
    empty($params['details']) ? 'NULL' : mssql_escape_with_zeroes(json_encode($params['details']))
  );

  return $sql;
}

/**
 * get_initated_cc_transaction
 * Get the latest INITIATED cc_transaction for the given cc_holder_tokens_id
 * $params: cc_holder_tokens_id
 *          customer_id
 *
 * @param  array $params
 * @return array initiated cc transaction
 */
function get_initated_cc_transaction( $params )
{
  $rows = ( isset( $params['cc_holder_tokens_id'] ) )
          ?
          get_ultra_cc_transactions(
            array(
              'cc_holder_tokens_id' => $params['cc_holder_tokens_id'],
              'status'              => 'INITIATED'
            )
          )
          :
          getUltraCCTransactionsPROC(
            array(
              'customer_id'         => $params['customer_id'],
              'status'              => 'INITIATED'
            )
          )
          ;

  return ( $rows && is_array( $rows ) && count( $rows ) ) ? $rows[0] : NULL ;
}

/**
 * get_distinct_cc_holder_tokens_id
 *
 * @param  integer $customer_id
 * @param  integer $days_range
 * @param  string $status
 * @param  string $result
 * @return array $distinct_cc_holder_tokens_id
 */
function get_distinct_cc_holder_tokens_id( $customer_id , $days_range=30 , $status='COMPLETE' , $result='APPROVED' )
{
  $distinct_cc_holder_tokens_id = array();

  $data = getUltraCCTransactionsPROC([
    'customer_id' => $customer_id,
    'days_range'  => -$days_range,
    'status'      => $status,
    'result'      => $result,
    'token'       => true
  ]);

  if ( $data && is_array( $data ) && count( $data ) )
  {
    $underscoreObject = new __ ;

    $distinct_cc_holder_tokens_id = $underscoreObject->flatten( $data );
  }

  dlog('',"distinct_cc_holder_tokens_id = %s",$distinct_cc_holder_tokens_id);

  return $distinct_cc_holder_tokens_id;
}

/**
 * get_ultra_cc_transaction_sum
 *
 * @param  integer $customer_id
 * @param  integer $days_range
 * @param  string $status
 * @param  string $result
 * @return integer $ultra_cc_transaction_count
 */
function get_ultra_cc_transaction_sum( $customer_id , $days_range , $status='COMPLETE' , $result ='APPROVED' )
{
  $data = getUltraCCTransactionsPROC([
    'customer_id' => $customer_id,
    'days_range'  => -$days_range,
    'status'      => $status,
    'result'      => $result,
    'amount'      => 1
  ]);

  return ( ! empty($data[0])) ? $data[0]->ULTRA_CC_TRANSACTION_SUM : 0;
}


/**
 * get_ultra_cc_transaction_and_token
 *
 * Given a $cc_transactions_id, returns ULTRA.CC_TRANSACTIONS and ULTRA.CC_HOLDER_TOKENS data
 *
 * @param  integer $cc_transactions_id
 * @return object $ultra_cc_transaction_and_token
 */
function get_ultra_cc_transaction_and_token( $cc_transactions_id )
{
  $query = sprintf('SELECT
    t.CC_TRANSACTIONS_ID, t.CC_HOLDER_TOKENS_ID, t.CUSTOMER_ID, t.TYPE, t.CHARGE_AMOUNT, t.DESCRIPTION, CAST(t.DETAILS AS TEXT) AS DETAILS,
    h.CC_HOLDERS_ID, h.GATEWAY, h.MERCHANT_ACCOUNT, h.TOKEN, h.ENABLED,
    ch.EXPIRES_DATE, ch.BIN, ch.LAST_FOUR, p.CC_PROCESSORS_ID, p.PROCESSOR_NAME
    FROM ULTRA.CC_TRANSACTIONS t WITH (NOLOCK)
    JOIN ULTRA.CC_HOLDER_TOKENS h WITH (NOLOCK) ON h.CC_HOLDER_TOKENS_ID = t.CC_HOLDER_TOKENS_ID
    JOIN ULTRA.CC_HOLDERS ch ON ch.CC_HOLDERS_ID = h.CC_HOLDERS_ID
    JOIN ULTRA.CC_PROCESSORS p ON p.CC_PROCESSORS_ID = h.CC_PROCESSORS_ID
    WHERE t.CC_TRANSACTIONS_ID = %d',
    $cc_transactions_id
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($query));
  return empty($result[0]->CC_TRANSACTIONS_ID) ? NULL : $result[0];
}


/**
 * get_ultra_cc_transaction
 * $params: see get_ultra_cc_transactions
 *
 * @param  array $params
 * @return object
 */
function get_ultra_cc_transaction( $params )
{
  $rows = get_ultra_cc_transactions( $params );

  return ( $rows && count( $rows ) ) ? $rows[0] : NULL ;
}

/**
 * get_ultra_cc_transactions
 * $params: order_by
 * 
 * @param  array $params
 * @return object[]
 */
function get_ultra_cc_transactions( $params )
{
  $rows = array();

  $order_by = ' CREATED_DATE_TIME DESC ';

  if ( isset($params['order_by']) )
  {
    $order_by = $params['order_by'];

    unset( $params['order_by'] );
  }

  $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.CC_TRANSACTIONS', NULL, NULL, $params).
         ' ORDER BY '.$order_by;

  // sanity check
  if ( !$sql )
    return $rows;

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
    $rows = $query_result;

  return $rows;
}

/**
 * get_ultra_cc_timed_out_transactions
 *
 * Returns CC transactions which timed out in the last 24 hours, but older than 15 minutes
 *
 * @return object[] $transactions
 */
function get_ultra_cc_timed_out_transactions()
{
  $transactions = array();

  $sql = "SELECT * FROM ULTRA.CC_TRANSACTIONS WHERE
    TRANSACTION_ID IS NULL
    AND STATUS = 'COMPLETE'
    AND RESULT != 'FAILED'
    AND MERCHANT_TRANSACTION_ID != 'always_succeed'
    AND CREATED_DATE_TIME < DATEADD(mi, -15, GETDATE())
    AND CREATED_DATE_TIME > DATEADD(dd, -1, GETDATE())";

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ($data && is_array($data) && count($data))
    $transactions = $data;

  return $transactions;
}

function getUltraCCTransactionsPROC($params)
{
  $cols = [];

  foreach ($params as $key => $val)
  {
    switch ($key)
    {
      case 'customer_id':
        $cols[] = "@CUSTOMER_ID = $val";
        break;
      case 'result':
        $cols[] = "@RESULT = '$val'";
        break;
      case 'status':
        $cols[] = "@STATUS = '$val'";
        break;
      case 'token':
        $cols[] = "@TOKEN = 1";
        break;
      case 'days_range':
        $cols[] = "@DAYS = $val";
        break;
      case 'amount':
        if ($val) $cols[] = "@AMOUNT = 1";
        break;
    }
  }

  $sql = "EXEC [ULTRA].[Get_CC_Transactions] " . implode(', ', $cols);

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

function addToUltraCCTransactionsPROC($params)
{
  $sql = sprintf("
    EXEC [ULTRA].[Insert_CC_Transactions]
      @CUSTOMER_ID   = %d,
      @CHARGE_AMOUNT = %s,
      @TYPE          = '%s',
      @DESCRIPTION   = '%s',
      @CUSTOMER_IP   = '%s',
      @DETAILS       = '%s'
  ",
    $params['customer_id'],
    $params['charge_amount'],
    $params['type'],
    $params['description'],
    Session::getClientIp(),
    json_encode($params['details'])
  );

  return $sql;
}

function updateUltraCCTransactionsPROC($params)
{
  $cols = [];
  foreach ($params as $key => $val)
  {
    switch ($key)
    {
      case 'cc_transactions_id':
        $cols[] = "@CC_TRANSACTIONS_ID = $val";
        break;
      case 'err_description':
        $cols[] = "@ERR_DESCRIPTION = '$val'";
        break;
      case 'merchant_transaction_id':
        $cols[] = "@MERCHANT_TRANSACTION_ID = '$val'";
        break;
      case 'transaction_id':
        $cols[] = "@TRANSACTION_ID = '$val'";
        break;
      case 'result':
        $cols[] = "@RESULT = '$val'";
        break;
      case 'status':
        $cols[] = "@STATUS = '$val'";
        break;
    }
  }

  $sql = "EXEC [ULTRA].[Update_CC_Transactions] " . implode(', ', $cols);

  return $sql;
}

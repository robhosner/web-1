<?php

/**
 * log_data_event 
 * @param Array (String 'action', Interger 'customer_id, String 'soc', Integer 'value')
 * @return Boolean TRUE on success, FALSE on failure
 */
function log_data_event($params)
{
  $sql = sprintf(
    'DECLARE @CUSTOMER BIGINT = %d;
    INSERT INTO DBO.HTT_DATA_EVENT_LOG
      ([ACTION], [CUSTOMER_ID], [SOC], [VALUE], [HTT_PLAN_TRACKER_ID])
      VALUES
      (%s, @CUSTOMER, %s, %d, (SELECT MAX(HTT_PLAN_TRACKER_ID) FROM DBO.HTT_PLAN_TRACKER WITH (NOLOCK) WHERE CUSTOMER_ID = @CUSTOMER))',
    $params['customer_id'],
    mssql_escape_with_zeroes($params['action']),
    mssql_escape_with_zeroes($params['soc']),
    empty($params['value']) ? 0 : $params['value']);

  $return = run_sql_and_check_result($sql);

  return $return->is_success();
}

/**
 * get_data_event_log 
 * @param  array $params
 * @return object[]
 */
function get_data_event_log($params) {

  $sql = sprintf(
    "SELECT *
     FROM   HTT_DATA_EVENT_LOG
     WHERE  CUSTOMER_ID = %d
     AND    DATEDIFF(dd, EVENT_DATE, GETUTCDATE()) < %d
     ORDER BY EVENT_DATE ASC",
    $params['customer_id'],
    $params['days_ago']
  );

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * get_latest_data_event_log 
 * @param  integer $customer_id
 * @return array || NULL
 */
function get_latest_data_event_log( $customer_id )
{
  $sql = sprintf(
    "SELECT TOP 1 * , DATEDIFF(ss, '1970-01-01', EVENT_DATE ) event_date_epoch
     FROM   HTT_DATA_EVENT_LOG
     WHERE  CUSTOMER_ID = %d
     ORDER BY EVENT_DATE DESC",
    $customer_id
  );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array( $data ) && count( $data ) )
    return $data[0];

  return NULL;  
}

?>

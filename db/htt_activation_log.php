<?php
include_once ('lib/util-common.php');

// php component to access DB table htt_activation_log #

// synopsis

// cho htt_activation_log_insert_query(
// array(
// 'iccid' => '800000000000000001',#'890126084210358574',
// 'customer_id' => '5',
// 'masteragent' => '4',
// 'distributor' => '3',
// 'dealer' => '2',
// 'user_id' => '1',
// 'date' => 'getutcdate()',
// 'activated_by' => 'test by'
// )
// ."\n";

// cho htt_activation_log_select_query(
// array(
// 'customer_id' => '5'
// )
// ."\n";

/**
 * customer_activation_start_date 
 * @param  integer $customer_id
 * @return string $customer_activation_start_date
 */
function customer_activation_start_date($customer_id)
{
  $customer_activation_start_date = '';
  
  $htt_activation_log_select_query = htt_activation_log_select_query ( array (
      'customer_id' => $customer_id,
      'nolock'      => TRUE
  ) );
  
  $htt_activation_log_data = mssql_fetch_all_objects ( logged_mssql_query ( $htt_activation_log_select_query ) );
  
  if ($htt_activation_log_data && count ( $htt_activation_log_data ))
  {
    $customer_activation_start_date = $htt_activation_log_data [0]->ACTIVATED_DATE;
  }
  
  return $customer_activation_start_date;
}


/**
 * htt_activation_log_select_query 
 *
 * $params: customer_id
 * @param  array $params
 * @return string SQL
 */
function htt_activation_log_select_query($params)
{
  dlog ("", "htt_activation_log_select_query = " . $params ['customer_id'] );

  $top_clause = get_sql_top_clause ( $params );

  $nolock = ( empty( $params['nolock'] ) || ! $params['nolock'] ) ? '' : 'WITH (NOLOCK) ' ;

  return sprintf(
   "SELECT $top_clause *
    FROM   HTT_ACTIVATION_LOG $nolock
    WHERE  ACTIVATED_CUSTOMER_ID = %d
    ORDER BY ACTIVATED_DATE DESC",
    $params ['customer_id']
  );
}


/**
 * get_eligible_demo_line_dealers
 * return a list of dealers eligible for demo accounts based on their type of number of monthly activations
 * @see DEMO-7
 */
function get_eligible_demo_line_dealers($list = array(), $mode)
{
  $between = 'GETUTCDATE()';

  if ($mode == 'month')
    $between = "dbo.pt_to_utc(DateAdd(Month, (DateDiff(Month, 0, GetDate())-1), 0)) AND dbo.pt_to_utc(DateAdd(Month, DateDiff(Month, 0, GetDate()), 0))";

  else if ($mode == 'daily')
    $between = "dbo.pt_to_utc(DateAdd(Month, DateDiff(Month, 0, GetDate()), 0)) AND GETUTCDATE()";

  $celluphone = \Ultra\UltraConfig\celluphoneDb();
  $sql = "SELECT d.DealerSiteID as id, d.Dealercd as code, l.ACTIVATIONS as activations
    FROM $celluphone.DBO.tblDealerSite d WITH (NOLOCK)
    LEFT JOIN
      (SELECT DEALER, COUNT(DEALER) AS ACTIVATIONS FROM ULTRA.HTT_ACTIVATION_HISTORY WITH (NOLOCK)
      WHERE PLAN_STARTED_DATE_TIME BETWEEN $between
      AND FINAL_STATE = '" . FINAL_STATE_COMPLETE . "'
      GROUP BY DEALER) AS l
    ON d.DealerSiteID = l.DEALER
    WHERE (d.Dealercd like 'EPP%' OR l.ACTIVATIONS > 9)"; // DEMO-7: 10 or more activations required for eligibility

  // if dealer list is given then limit query to those dealers
  if (is_array($list) && count($list))
  {
    $dealers = NULL;
    foreach ($list as $dealer)
      $dealers .= ($dealers ? ',' : NULL) . sprintf("'%s'", $dealer);
    $sql .= " AND d.Dealercd IN ($dealers)";
  }

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}


/**
 * htt_activation_log_select_by_iccid_full_query
 * 
 * @param  integer $iccid_full -- 19 digit ICCID "full" for Sim - 'ICCID_FULL', 
 * @return string
 */
function htt_activation_log_select_by_iccid_full_query($iccid_full)
{
  $dealerPortal = \Ultra\UltraConfig\celluphoneDb();

  return sprintf("
SELECT ma.[masterid]                                AS master_id,
       ma.[businessname]                            AS master_name,
       ma.[mastercd]                                AS master_code,
       di.[masterid]                                AS dist_id,
       di.[businessname]                            AS dist_name,
       di.[mastercd]                                AS dist_code,
       ds.[dealersiteid]                            AS dealer_id,
       ds.[businessname]                            AS dealer_name,
       ds.[dealercd]                                AS dealer_code,
       Datediff(ss, '1970-01-01', s.activated_date) AS activation_epoch
FROM   htt_activation_log s with (NOLOCK)
       LEFT JOIN $dealerPortal..[tblmaster] ma with (NOLOCK)
              ON s.activated_masteragent = ma.[masterid]
       LEFT JOIN $dealerPortal..[tblmaster] di with (NOLOCK)
              ON s.activated_distributor = di.[masterid]
       LEFT JOIN $dealerPortal..[tbldealersite] ds with (NOLOCK)
              ON s.activated_dealer = ds.[dealersiteid] 
WHERE s.[ICCID_FULL]='%s'", $iccid_full );
}

/**
 * htt_activation_log_insert_query
 * $params: iccid
 *          customer_id
 *          masteragent
 *          distributor
 *          dealer
 *          user_id
 *          date
 *          activated_by
 *          promised_amount
 *          initial_cos_id
 *          initial_bolt_ons
 *         
 * @param  array $params
 * @return string SQL
 */
function htt_activation_log_insert_query($params)
{
  dlog ( '', "htt_activation_log_insert_query params = " . json_encode ( $params ) );
  
  $iccid_full = luhnenize ( $params ['iccid'] );
  
  dlog ( '', "htt_activation_log_insert_query iccid full = $iccid_full" );
  
  $query = sprintf ( "
IF NOT EXISTS (SELECT * FROM HTT_ACTIVATION_LOG WHERE ICCID_NUMBER = %s)
BEGIN
INSERT INTO HTT_ACTIVATION_LOG
(
  ICCID_NUMBER,
  ACTIVATED_CUSTOMER_ID,
  ACTIVATED_MASTERAGENT,
  ACTIVATED_DISTRIBUTOR,
  ACTIVATED_DEALER,
  ACTIVATED_USERID,
  ACTIVATED_DATE,
  ACTIVATED_BY,
  ICCID_FULL,
  PROMISED_AMOUNT,
  INITIAL_COS_ID,
  INITIAL_BOLTONS
)
VALUES
(
  %s,
  %d,
  %d,
  %d,
  %d,
  %d,
  %s,
  %s,
  %s,
  %d,
  %d,
  %s
)
END",
  mssql_escape_with_zeroes ( $params ['iccid'] ),
  mssql_escape_with_zeroes ( $params ['iccid'] ),
  $params ['customer_id'],
  $params ['masteragent'],
  $params ['distributor'],
  $params ['dealer'],
  $params ['user_id'],
  $params ['date'],
  mssql_escape ( $params ['activated_by'] ),
  mssql_escape_with_zeroes ( $iccid_full ),
  $params['promised_amount'],
  $params['initial_cos_id'],
  mssql_escape_with_zeroes($params['initial_bolt_ons']));

  return $query;
}

?>

<?php

// ULTRA_ACTIVATION_DETAIL replaces HTT_ACTIVATION_HISTORY, see MVNO-2955
require_once 'ultra_activation_details.php';

/*
PHP module to access DB table ULTRA.HTT_ACTIVATION_HISTORY
*/

/**
 * flush_ultra_activation_history_cache_by_customer_id
 *
 * @param  integer $customer_id
 * @param  object Redis
 * @return boolean
 */
function flush_ultra_activation_history_cache_by_customer_id( $customer_id , $redis=NULL )
{
  if ( !$customer_id )
    return FALSE;

  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis();

  $dealer = \Ultra\Lib\DB\Getter\getScalar('ULTRA.HTT_ACTIVATION_HISTORY.CUSTOMER_ID', $customer_id, 'DEALER', NULL, NULL, $redis);

  if ( $dealer )
    $redis->set( 'ultra/api/dealer_by_customer_id/'.$customer_id.'/'.$dealer , TRUE , 10800 );

  return flush_ultra_activation_history_cache_by_dealer( $dealer , $redis );
}

/**
 * flush_ultra_activation_history_cache_by_dealer
 *
 * @param  string $dealer
 * @param  object $redis Redis
 * @return boolean
 */
function flush_ultra_activation_history_cache_by_dealer( $dealer , $redis=NULL )
{
  if ( !$dealer )
    return FALSE;

  if ( is_null($redis) )
    $redis = new \Ultra\Lib\Util\Redis();

  $redis_key = 'ultra/activation/history/'.$dealer;

  $redis->del( $redis_key );

  $redis_key = 'ultra/activation/history/'.$dealer.'/LAST_UPDATED';

  $redis->del( $redis_key );

  $redis_key = 'ultra/activation/history/'.$dealer.'/CREATION';

  $redis->del( $redis_key );

  return TRUE;
}

/**
 * get_activation_history_by_customer_id
 * @param  integer $customer_id
 * @return object
 */
function get_activation_history_by_customer_id( $customer_id )
{
  $activation_history = NULL;

  $result = find_ultra_activation_history(
    array(
      'customer_id' => $customer_id
    )
  );

  if ( $result->is_success() && count($result->data_array) )
    $activation_history = $result->data_array[0];

  return $activation_history;
}

/**
 * find_ultra_activation_history
 * $params: epoch_from
 *          epoch_to
 *          dealer
 *          date_selected
 *
 * @param  array $params
 * @param  object $redis Redis
 * @return object Result
 */
function find_ultra_activation_history( $params , $redis=NULL )
{
  dlog('', '(%s)', func_get_args());

  // if the query involves the latest 300 seconds, do not go to the DB, use Redis cache
  $should_cache = ! !
      ( isset($params['epoch_from'])
     && isset($params['epoch_to'])
     && isset($params['dealer'])
     && $params['epoch_from']
     && $params['epoch_to']
     && $params['dealer']
     && ( $params['epoch_to'] - $params['epoch_from'] < 330 )
  );

  if ( $should_cache )
  {
    dlog('','epoch_from = '.$params['epoch_from'].' ; epoch_to = '.$params['epoch_to']);

    // check Redis cache

    $redis_key = 'ultra/activation/history/'.$params['dealer'];

    if ( isset($params['date_selected']) && $params['date_selected'] )
      $redis_key .= '/'.$params['date_selected'];

    if ( is_null($redis) )
      $redis = new \Ultra\Lib\Util\Redis();

    $data = $redis->get( $redis_key );

    if ( $data )
      $data = json_decode($data);

    if ( $data )
      return make_ok_Result( $data );
  }

  $sql = make_find_ultra_activation_details( $params );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) && count($data) )
  {

    // -548 days == 1.5 years
    $defaultDate = strtotime("now -1 year");
    $minAllowedDays = round(strtotime("now -548 days") / 60 / 60 / 24);

    // convert plan_started from days to seconds
    foreach ($data as $key => $value)
    {
      if (isset($data[$key]->plan_started))
      {
        try
        {
          if ($data[$key]->plan_started < $minAllowedDays)
          {
            $data[$key]->plan_started = $defaultDate;
          }
          else
          {
            $data[$key]->plan_started = $data[$key]->plan_started * 60 * 60 * 24;
          }
        }
        catch(Exception $e)
        {
          $data[$key]->plan_started = $defaultDate;
        }
      }
    }

    if ( $should_cache )
    {
      // store in Redis cache

      $redis_key = 'ultra/activation/history/'.$params['dealer'];

      if ( isset($params['date_selected']) && $params['date_selected'] )
        $redis_key .= '/'.$params['date_selected'];

      if ( is_null($redis) )
        $redis = new \Ultra\Lib\Util\Redis();

      $redis->set( $redis_key , json_encode( $data ) , 3600 ); // 30 minutes
    }

    return make_ok_Result( $data );
  }

  return make_error_Result('No data found');
}

/**
 * get_ultra_activation_history
 *
 * @param  integer $customer_id
 * @return object
 */
function get_ultra_activation_history( $customer_id )
{
  $activation_history = NULL;

  $sql = sprintf("
    SELECT
      FUNDING_SOURCE,
      FUNDING_AMOUNT,
      FINAL_STATE,
      CASE WHEN h.LAST_UPDATED_DATE IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', h.LAST_UPDATED_DATE ) END history_last_updated
    FROM
      ULTRA.HTT_ACTIVATION_HISTORY h
    WHERE
      h.CUSTOMER_ID = %d",
    $customer_id
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
    $activation_history = $query_result[0];

  return $activation_history;
}

/**
 * log_new_activation_in_activation_history
 *
 * @param  string $transition_uuid
 * @param  integer $customer_id
 * @return array
 */
function log_new_activation_in_activation_history( $transition_uuid , $customer_id )
{
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id'     => $customer_id,
      'activation_type' => 'NEW',
      'transition_uuid' => $transition_uuid
    )
  );

  $success = is_mssql_successful(logged_mssql_query($sql));

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  return ( $success ) ? $result = array( 'success' => TRUE , 'errors' => array() )
                      : $result = array( 'success' => FALSE, 'errors' => array('DB error') );
}

/**
 * log_port_transition_in_activation_history
 *
 * @param  string $transition_uuid
 * @param  integer $customer_id
 * @return array
 */
function log_port_transition_in_activation_history( $transition_uuid , $customer_id )
{
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id'     => $customer_id,
      'activation_type' => 'PORT',
      'transition_uuid' => $transition_uuid
    )
  );

  $success = is_mssql_successful(logged_mssql_query($sql));

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  return ( $success ) ? $result = array( 'success' => TRUE , 'errors' => array() )
                      : $result = array( 'success' => FALSE, 'errors' => array('DB error') );
}

/**
 * log_intraport_transition_in_activation_history
 *
 * @param  string $transition_uuid
 * @param  integer $customer_id
 * @return array
 */
function log_intraport_transition_in_activation_history( $transition_uuid , $customer_id )
{
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id'     => $customer_id,
      'activation_type' => 'INTRA-PORT',
      'transition_uuid' => $transition_uuid
    )
  );

  $success = is_mssql_successful(logged_mssql_query($sql));

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  return ( $success ) ? $result = array( 'success' => TRUE , 'errors' => array() )
                      : $result = array( 'success' => FALSE, 'errors' => array('DB error') );
}

/**
 * log_transition_in_activation_history
 *
 * @param  string $transition_uuid
 * @param  integer $customer_id
 * @return array
 */
function log_transition_in_activation_history( $transition_uuid , $customer_id )
{
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id'     => $customer_id,
      'transition_uuid' => $transition_uuid
    )
  );

  $success = is_mssql_successful(logged_mssql_query($sql));

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  return ( $success ) ? $result = array( 'success' => TRUE , 'errors' => array() )
                      : $result = array( 'success' => FALSE, 'errors' => array('DB error') );
}

/**
 * log_cancellation_transition_in_activation_history
 *
 * @param  string $transition_uuid
 * @param  integer $customer_id
 * @return array
 */
function log_cancellation_transition_in_activation_history( $transition_uuid , $customer_id )
{
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id'         => $customer_id,
      'transition_uuid'     => $transition_uuid,
      'state_not_completed' => TRUE
    )
  );

  $success = is_mssql_successful(logged_mssql_query($sql));

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  return ( $success ) ? $result = array( 'success' => TRUE , 'errors' => array() )
                      : $result = array( 'success' => FALSE, 'errors' => array('DB error') );
}

/**
 * log_plan_started_in_activation_history
 *
 * @param  integer $customer_id
 * @return array
 */
function log_plan_started_in_activation_history( $customer_id )
{
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id'            => $customer_id,
      'plan_started_date_time' => 'GETUTCDATE()'
    )
  );

  $success = is_mssql_successful(logged_mssql_query($sql));

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  return ( $success ) ? $result = array( 'success' => TRUE , 'errors' => array() )
                      : $result = array( 'success' => FALSE, 'errors' => array('DB error') );
}

/**
 * log_funding_in_activation_history
 *
 * @param  integer $customer_id
 * @param  string $funding_source
 * @param  decimal $funding_amount
 * @return boolean
 */
function log_funding_in_activation_history( $customer_id , $funding_source , $funding_amount )
{
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id'    => $customer_id,
      'funding_source' => $funding_source,
      'funding_amount' => $funding_amount
    )
  );

  $success = is_mssql_successful(logged_mssql_query($sql));

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  return $success;
}

/**
 * log_port_iccid_in_activation_history
 *
 * @param  integer $customer_id
 * @param  string $iccid
 * @return array
 */
function log_port_iccid_in_activation_history( $customer_id , $iccid )
{
  return log_new_iccid_in_activation_history( $customer_id , $iccid , 'PORT' );
}

/**
 * log_intraport_iccid_in_activation_history
 *
 * @param  integer $customer_id
 * @param  string $iccid
 * @return array
 */
function log_intraport_iccid_in_activation_history( $customer_id , $iccid )
{
  return log_new_iccid_in_activation_history( $customer_id , $iccid , 'INTRA-PORT' );
}

/**
 * log_new_iccid_in_activation_history
 *
 * Updates
 * - ULTRA.HTT_ACTIVATION_HISTORY
 * - HTT_CUSTOMERS_OVERLAY_ULTRA.MVNE
 * - HTT_ULTRA_MSISDN.MVNE
 *
 * @param  integer $customer_id
 * @param  string $iccid
 * @param  string $activation_type
 * @return array
 */
function log_new_iccid_in_activation_history( $customer_id , $iccid , $activation_type='NEW' )
{
  $result = array( 'success' => TRUE, 'errors' => array() );

  if ( !$customer_id )
    return array( 'success' => FALSE, 'errors' => array('log_port_iccid_in_activation_history invoked with no customer_id') );

  if ( !$iccid )
    return array( 'success' => FALSE, 'errors' => array('log_port_iccid_in_activation_history invoked with no iccid') );

  // get iccid info
  $sql = htt_inventory_sim_select_query(
    array(
      'iccid_full' => luhnenize($iccid)
    )
  );

  $iccid_info = mssql_fetch_all_objects ( logged_mssql_query ( $sql ) );

  if ( ! $iccid_info || ! is_array($iccid_info) || !count($iccid_info) )
    return array( 'success' => FALSE, 'errors' => array('no data found for iccid '.$iccid) );

  // update ULTRA.HTT_ACTIVATION_HISTORY
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id'     => $customer_id,
      'masteragent'     => $iccid_info[0]->INVENTORY_MASTERAGENT,
      'distributor'     => $iccid_info[0]->INVENTORY_DISTRIBUTOR,
      'activation_type' => $activation_type,
      'iccid_full'      => $iccid_info[0]->ICCID_FULL
    )
  );

  $result['success'] = ! ! run_sql_and_check($sql);

  if ( ! $result['success'] )
    $result['errors'][] = 'Could not update ULTRA.HTT_ACTIVATION_HISTORY';

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  // update HTT_CUSTOMERS_OVERLAY_ULTRA
  $sql = htt_customers_overlay_ultra_update_query(
    array(
      'customer_id' => $customer_id,
      'mvne'        => $iccid_info[0]->MVNE
    )
  );

  if ( ! run_sql_and_check($sql) )
  {
    $result['success'] = FALSE;

    $result['errors'][] = 'Could not update HTT_CUSTOMERS_OVERLAY_ULTRA';
  }

  if ( $iccid_info[0]->MVNE == '2' )
  {
    $sql = htt_customers_overlay_ultra_select_query(
      array(
        'select_fields' => array('customer_id','current_mobile_number'),
        'sql_limit'     => 1,
        'customer_id'   => $customer_id
      )
    );

    $customers_overlay_result = \Ultra\Lib\DB\fetch_objects($sql);

    if (!( $customers_overlay_result && is_array($customers_overlay_result) && count($customers_overlay_result) ))
      return $result;

    // update HTT_ULTRA_MSISDN
    $sql = htt_ultra_msisdn_update_query(
      array(
        'msisdn'  => $customers_overlay_result[0]->current_mobile_number,
        'mvne'    => '2'
      )
    );

    if ( ! is_mssql_successful(logged_mssql_query($sql)) )
    {
      $result['success'] = FALSE;

      $result['errors'][] = 'Could not update HTT_ULTRA_MSISDN';
    }

    // fix redis for MVNE cache
    \Ultra\Lib\DB\Getter\setScalarMVNE( '2' , $customers_overlay_result[0]->current_mobile_number , $iccid , $customer_id );
  }

  return $result;
}

/**
 * log_datetime_activation_history
 *
 * @param  integer $customer_id
 * @return boolean
 */
function log_datetime_activation_history( $customer_id )
{
  $sql = ultra_activation_history_update_query(
    array(
      'customer_id'    => $customer_id
    )
  );

  flush_ultra_activation_history_cache_by_customer_id( $customer_id );

  return is_mssql_successful(logged_mssql_query($sql));
}

/**
 * add_to_ultra_activation_history
 *
 * $params: see ultra_activation_history_insert_query
 *
 * @param  array $params
 * @return boolean
 */
function add_to_ultra_activation_history( $params , $redis=NULL )
{
  $ultra_activation_history_insert_query = ultra_activation_history_insert_query( $params );

  $success = is_mssql_successful(logged_mssql_query($ultra_activation_history_insert_query));

  if ( $success && isset($params['dealer']) && $params['dealer'] && isset($params['customer_id']) && $params['customer_id'] )
  {
    $redis = empty( $redis ) ? new \Ultra\Lib\Util\Redis() : $redis ;

    $redis->set( 'ultra/api/dealer_by_customer_id/'.$params['customer_id'].'/'.$params['dealer'] , TRUE , 10800 );
    $redis->set( 'ultra/getter/ULTRA.HTT_ACTIVATION_HISTORY.CUSTOMER_ID/'.$params['customer_id'].'/DEALER' , $params['dealer'] , 10800 );
  }

  return $success;
}

/**
 * get_report_activation_activity
 *
 * The activation activity report provides a listing of activations performed.
 *
 * @param  array $params
 * @return object of class \Result
 */
function get_report_activation_activity( $params )
{
  dlog('', '(%s)', func_get_args());

  $masteragent = $params['masteragent'] ?: 'NULL';
  $distributor = $params['distributor'] ?: 'NULL';
  $start_date = date('Y-m-d', strtotime($params['date_from']));
  $end_date = date('Y-m-d', strtotime($params['date_to']));

  if ($masters = find_credential('ultra/master_agents/alternative'))
  {
    $exclusion = str_replace(' ', ',', $masters);
  }

  $sql = "EXEC [dbo].[Get_Activation_History] @START_DATE = '$start_date', @END_DATE = '$end_date', @MASTERAGENT = ".$masteragent.", @DISTRIBUTOR = ".$distributor.", @EXCLUSION = ".($exclusion ? "'$exclusion'" : 'NULL');

  if ( isset($params['masteragent']) )
    $where[] = sprintf( " a.MASTERAGENT = %d " , $params['masteragent'] );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ($data && is_array($data))
  {
    // MVNO-2335: convert dates into 'MM/DD/YY HH:MM:SS AP' format
    foreach ($data as &$row)
    {
      if ($row->funding_date)
        $row->funding_date = date('m/d/y h:i:s A', strtotime($row->funding_date));
      if ($row->created_date)
        $row->created_date = date('m/d/y h:i:s A', strtotime($row->created_date));
    }

    return make_ok_Result($data);
  }

  return make_error_Result('No data found');
}

/**
 * combine_report_activation_activity
 *
 * Return a combination of the two reports
 *
 * @param  object $result2014 Result
 * @param  object $result2015 Result
 * @return object of class \Result
 */
function combine_report_activation_activity( $result2014 , $result2015 )
{
  if ( $result2014->is_failure() )
    return $result2015;

  if ( $result2015->is_failure() )
    return $result2014;

  return make_ok_Result(
    array_merge( $result2014->data_array , $result2015->data_array )
  );
}

/**
 * get_report_activation_activity_2015
 *
 * Activation activity report after Nov 1, 2014
 *
 * @param  array $params
 * @param  string $db
 * @return object of class \Result
 */
function get_report_activation_activity_2015( $params , $db )
{
  // PROD-900 - convert param dates to "long" version
  $params['date_from']  = date('Y-m-d 00:00:01', strtotime($params['date_from']));
  $params['date_to']    = date('Y-m-d 23:59:59', strtotime($params['date_to']));

  $where = array(
    ' CASE WHEN PLAN_STARTED_DATE_TIME IS NULL THEN CREATION_DATE ELSE PLAN_STARTED_DATE_TIME END' .
    " BETWEEN '{$params['date_from']}' AND '{$params['date_to']}' ",

    " s.PRODUCT_TYPE = 'PURPLE' ",

    " CASE WHEN PLAN_STARTED_DATE_TIME IS NULL THEN CREATION_DATE ELSE PLAN_STARTED_DATE_TIME END >= '2014-11-01 00:00:00.000' "
  );

  // PROD-1721: alternative master agents
  if ($masters = find_credential('ultra/master_agents/alternative'))
    $where[] = 's.INVENTORY_MASTERAGENT NOT IN (' .  str_replace(' ', ',', $masters) . ')';
  else
    logError('Failed to get alternative master agent list');

  if ( isset($params['masteragent']) )
    $where[] = sprintf( " a.MASTERAGENT = %d " , $params['masteragent'] );

  if ( isset($params['distributor']) )
    $where[] = sprintf( " a.DISTRIBUTOR = %d " , $params['distributor'] );

  $sql = "
    SELECT
      ".customer_plan_name_attribute('a').",
      a.MSISDN                 msisdn,
      a.ICCID_FULL             iccid,
      a.DEALER,
      a.PLAN_STARTED_DATE_TIME funding_date,
      a.CREATION_DATE          created_date,
      s.PRODUCT_TYPE           product_type,
      a.INVENTORY_MASTERAGENT,
      a.INVENTORY_DISTRIBUTOR,
      s.INVENTORY_DEALER,
      m.BusinessName master_agent_name,
      d.BusinessName distrbutor_name,
      i.Dealercd retailer_inventory,
      r.Dealercd retailer_activation,
      s.BRAND_ID
    FROM ULTRA.ACTIVATION_DETAILS  a WITH (NOLOCK)
    JOIN HTT_INVENTORY_SIM         s WITH (NOLOCK)
      ON s.ICCID_FULL = a.ICCID_FULL
    LEFT JOIN $db..tblmaster       m WITH (NOLOCK)
      ON m.MasterID = a.MASTERAGENT
    LEFT JOIN $db..tblmaster       d WITH (NOLOCK)
      ON d.MasterID = a.DISTRIBUTOR
    LEFT JOIN $db..tblDealerSite   i WITH (NOLOCK)
      ON i.DealerSiteID = s.INVENTORY_DEALER
    LEFT JOIN $db..tblDealerSite   r WITH (NOLOCK)
      ON r.DealerSiteID = a.DEALER
    WHERE " . implode(" AND ", $where)."
    ORDER BY CREATION_DATE DESC";

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) )
  {
    // MVNO-2335: convert dates into 'MM/DD/YY HH:MM:SS AP' format
    foreach ($data as &$row)
    {
      if ($row->funding_date)
        $row->funding_date = date('m/d/y h:i:s A', strtotime($row->funding_date));
      if ($row->created_date)
        $row->created_date = date('m/d/y h:i:s A', strtotime($row->created_date));
    }

    return make_ok_Result( $data );
  }

  return make_error_Result('No data found');
}

/**
 * get_report_activation_activity_2014
 *
 * Activation activity report prior to Nov 1, 2014
 *
 * @param  array $params
 * @param  string $db
 * @return object of class \Result
 */
function get_report_activation_activity_2014( $params , $db )
{
  // PROD-900 - convert param dates to "long" version
  $params['date_from']  = date('Y-m-d 00:00:01', strtotime($params['date_from']));
  $params['date_to']    = date('Y-m-d 23:59:59', strtotime($params['date_to']));

  // PROD-1721: alternative master agents
  if ($masters = find_credential('ultra/master_agents/alternative'))
    $alternative = 'OR s.INVENTORY_MASTERAGENT IN (' .  str_replace(' ', ',', $masters) . ')';
  else
  {
    logError('Failed to get alternative master agent list');
    $alternative = NULL;
  }

  $where = array(
    // MVNO-2353: use CREATION_DATE when PLAN_STARTED_DATE_TIME is NULL
    ' CASE WHEN PLAN_STARTED_DATE_TIME IS NULL THEN CREATION_DATE ELSE PLAN_STARTED_DATE_TIME END' .
    " BETWEEN '{$params['date_from']}' AND '{$params['date_to']}' ",

    // MVNO-2935: new condition for date prior to Nov 1, 2014
    " ( ( CASE WHEN PLAN_STARTED_DATE_TIME IS NULL THEN CREATION_DATE ELSE PLAN_STARTED_DATE_TIME END < '2014-11-01 00:00:00.000' )
    OR s.PRODUCT_TYPE = 'ORANGE'
    OR s.INVENTORY_MASTERAGENT = 688 $alternative) "
  );

  if ( isset($params['masteragent']) )
    $where[] = sprintf( " s.INVENTORY_MASTERAGENT = %d " , $params['masteragent'] );

  if ( isset($params['distributor']) )
    $where[] = sprintf( " s.INVENTORY_DISTRIBUTOR = %d " , $params['distributor'] );

  $sql = "
    SELECT
      ".customer_plan_name_attribute('h').",
      h.MSISDN                 msisdn,
      h.ICCID_FULL             iccid,
      h.DEALER,
      h.PLAN_STARTED_DATE_TIME funding_date,
      h.CREATION_DATE          created_date,
      s.PRODUCT_TYPE           product_type,
      s.INVENTORY_MASTERAGENT,
      s.INVENTORY_DISTRIBUTOR,
      s.INVENTORY_DEALER,
      m.BusinessName master_agent_name,
      d.BusinessName distrbutor_name,
      i.Dealercd retailer_inventory,
      r.Dealercd retailer_activation,
      s.BRAND_ID
    FROM ULTRA.HTT_ACTIVATION_HISTORY h WITH (NOLOCK)
    JOIN HTT_INVENTORY_SIM            s WITH (NOLOCK)
      ON s.ICCID_FULL = h.ICCID_FULL
    LEFT JOIN $db..tblmaster       m WITH (NOLOCK)
      ON m.MasterID = s.INVENTORY_MASTERAGENT
    LEFT JOIN $db..tblmaster       d WITH (NOLOCK)
      ON d.MasterID = s.INVENTORY_DISTRIBUTOR
    LEFT JOIN $db..tblDealerSite   i WITH (NOLOCK)
      ON i.DealerSiteID = s.INVENTORY_DEALER
    LEFT JOIN $db..tblDealerSite   r WITH (NOLOCK)
      ON r.DealerSiteID = h.DEALER
    WHERE " . implode(" AND ", $where)."
    ORDER BY CREATION_DATE DESC";

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) )
  {
    // MVNO-2335: convert dates into 'MM/DD/YY HH:MM:SS AP' format
    foreach ($data as &$row)
    {
      if ($row->funding_date)
        $row->funding_date = date('m/d/y h:i:s A', strtotime($row->funding_date));
      if ($row->created_date)
        $row->created_date = date('m/d/y h:i:s A', strtotime($row->created_date));
    }

    return make_ok_Result( $data );
  }

  return make_error_Result('No data found');
}

/**
 * ultra_activation_history_insert_query
 * $params: customer_id
 *          masteragent
 *          iccid_full
 *          msisdn
 *          cos_id
 *          distributor
 *          dealer
 *          userid
 *          final_state
 *          activation_type
 *          funding_source
 *          funding_amount
 *          last_attempted_transition
 *          promised_amount
 *
 * @param  array $params
 * @return string
 */
function ultra_activation_history_insert_query( $params )
{
  $customer_id = sprintf("%d",$params['customer_id']);

  if ( ! isset( $params['masteragent'] ) )
  {
    dlog('',"No masteragent given!");
    $params['masteragent'] = '0';
  }

  $masteragent = sprintf("%d",$params['masteragent']);

  $iccid_full  = isset($params['iccid_full'])  ? sprintf("%s",mssql_escape_with_zeroes($params['iccid_full'])) : 'NULL' ;

  $msisdn      = isset($params['msisdn'])      ? sprintf("%s",mssql_escape_with_zeroes($params['msisdn']))     : 'NULL' ;

  $cos_id      = isset($params['cos_id'])      ? sprintf("%d",$params['cos_id'])                               : 'NULL' ;

  $distributor = isset($params['distributor']) ? sprintf("%d",$params['distributor'])                          : 'NULL' ;

  $dealer      = isset($params['dealer'])      ? sprintf("%d",$params['dealer'])                               : 'NULL' ;

  $userid      = isset($params['userid'])      ? sprintf("%d",$params['userid'])                               : 'NULL' ;

  $final_state     = isset($params['final_state'])     ? sprintf("%s",mssql_escape_with_zeroes($params['final_state']))     : 'NULL' ;

  $activation_type = isset($params['activation_type']) ? sprintf("%s",mssql_escape_with_zeroes($params['activation_type'])) : 'NULL' ;

  $funding_source  = isset($params['funding_source'])  ? sprintf("%s",mssql_escape_with_zeroes($params['funding_source']))  : 'NULL' ;

  $funding_amount  = isset($params['funding_amount'])  ? sprintf("%d",$params['funding_amount'])                            : 'NULL' ;

  $last_attempted_transition = isset($params['last_attempted_transition'])  ? sprintf("%s",mssql_escape_with_zeroes($params['last_attempted_transition'])) : 'NULL' ;

  $promised_amount = isset($params['promised_amount']) ? sprintf('%d', $params['promised_amount']) : 'NULL' ;

  // associate customer with dealer
  if ( isset($params['dealer']) && $params['dealer'] )
  {
    $redis = new \Ultra\Lib\Util\Redis();

    $redis->set( 'ultra/api/dealer_by_customer_id/'.$params['customer_id'].'/'.$params['dealer'] , TRUE , 10800 );
    $redis->set( 'ultra/getter/ULTRA.HTT_ACTIVATION_HISTORY.CUSTOMER_ID/'.$params['customer_id'].'/DEALER' , $params['dealer'] , 10800 );
  }

  return "INSERT INTO ULTRA.HTT_ACTIVATION_HISTORY
   (
 CUSTOMER_ID,
 MASTERAGENT,
 ICCID_FULL,
 MSISDN,
 COS_ID,
 DISTRIBUTOR,
 DEALER,
 USERID,
 CREATION_DATE,
 ACTIVATION_TYPE,
 FUNDING_SOURCE,
 FUNDING_AMOUNT,
 LAST_UPDATED_DATE,
 LAST_ATTEMPTED_TRANSITION,
 FINAL_STATE,
 PROMISED_AMOUNT
   ) VALUES (
 $customer_id,
 $masteragent,
 $iccid_full,
 $msisdn,
 $cos_id,
 $distributor,
 $dealer,
 $userid,
 GETUTCDATE(),
 $activation_type,
 $funding_source,
 $funding_amount,
 GETUTCDATE(),
 $last_attempted_transition,
 $final_state,
 $promised_amount
  )";
}

/**
 * ultra_activation_history_update_query
 *
 * $params: customer_id
 *          masteragent
 *          iccid_full
 *          msisdn
 *          cos_id
 *          distributor
 *          dealer
 *          userid
 *          activation_type
 *          activation_transition
 *          final_state
 *          plan_started_date_time
 *          funding_source
 *          funding_amount
 *          last_attempted_transition
 *          transition_uuid
 *          state_not_completed
 *
 * @param  array $params
 * @return string
 */
function ultra_activation_history_update_query( $params )
{
  $customer_id = sprintf("%d",$params['customer_id']);

  $set_clause  = array(" LAST_UPDATED_DATE = GETUTCDATE() ");

  if ( isset($params['masteragent']) )
    $set_clause[] = sprintf( " MASTERAGENT = %d " , $params['masteragent'] );

  if ( isset($params['iccid_full']) )
    $set_clause[] = sprintf( " ICCID_FULL  = %s " , mssql_escape_with_zeroes($params['iccid_full']) );

  if ( isset($params['msisdn']) )
    $set_clause[] = sprintf( " MSISDN      = %s " , mssql_escape_with_zeroes($params['msisdn']) );

  if ( isset($params['cos_id']) )
    $set_clause[] = sprintf( " COS_ID      = %d " , $params['cos_id'] );

  if ( isset($params['distributor']) )
    $set_clause[] = sprintf( " DISTRIBUTOR = %d " , $params['distributor'] );

  if ( isset($params['dealer']) )
    $set_clause[] = sprintf( " DEALER      = %d " , $params['dealer'] );

  if ( isset($params['userid']) )
    $set_clause[] = sprintf( " USERID      = %d " , $params['userid'] );

  if ( isset($params['activation_type']) )
    $set_clause[] = sprintf( " ACTIVATION_TYPE = %s " , mssql_escape_with_zeroes($params['activation_type']) );

  if ( isset($params['activation_transition']) )
    $set_clause[] = sprintf( " ACTIVATION_TRANSITION = %s " , mssql_escape_with_zeroes($params['activation_transition']) );

  if ( isset($params['final_state']) )
    $set_clause[] = sprintf( " FINAL_STATE = %s " , mssql_escape_with_zeroes($params['final_state']) );

  if ( isset($params['plan_started_date_time']) )
    $set_clause[] = " PLAN_STARTED_DATE_TIME = GETUTCDATE() ";

  if ( isset($params['funding_source']) )
    $set_clause[] = sprintf( " FUNDING_SOURCE = %s " , mssql_escape_with_zeroes($params['funding_source']) );

  if ( isset($params['funding_amount']) )
    $set_clause[] = sprintf( " FUNDING_AMOUNT = %d " , $params['funding_amount'] );

  if ( isset($params['last_attempted_transition']) )
    $set_clause[] = sprintf( " LAST_ATTEMPTED_TRANSITION = %s " , mssql_escape_with_zeroes($params['last_attempted_transition']) );
  elseif ( isset($params['transition_uuid']) )
    $set_clause[] = sprintf( " LAST_ATTEMPTED_TRANSITION = ( SELECT TRANSITION_LABEL FROM HTT_TRANSITION_LOG WHERE TRANSITION_UUID = %s ) " , mssql_escape_with_zeroes($params['transition_uuid']) );

  $additional_where = '';

  if ( isset($params['state_not_completed']) && $params['state_not_completed'] )
    $additional_where .= " AND FINAL_STATE != '" . FINAL_STATE_COMPLETE . "'";

  return
    " UPDATE ULTRA.HTT_ACTIVATION_HISTORY SET ".
    implode(',',$set_clause).
    " WHERE  CUSTOMER_ID = $customer_id ".$additional_where;
}

/**
 * get_ultra_dealer_customers_by_plan
 *
 * return dealer's customer count by plan
 *
 * @see MVNO-2411: add count from dealer's children as well
 * @param  integer $dealer
 * @return array array(plan_state => count)
 */
function get_ultra_dealer_customers_by_plan($dealer)
{
  // result is an array(plan_state => count)
  $result = array('Active' => 0, 'Cancelled' => 0, 'Neutral' => 0, 'Pre-Funded' => 0, 'Port-In Denied' => 0, 'Port-In Requested' => 0, 'Provisioned' => 0, 'Suspended' => 0);

  $where = make_where_clause_for_dealer_and_children($dealer);

  if ( empty( $where ) )
  {
    dlog('',"No dealer found");

    return $result;
  }

  $sql = 'SELECT count(u.CUSTOMER_ID) customers, u.PLAN_STATE state ' .
    'FROM HTT_CUSTOMERS_OVERLAY_ULTRA u ' .
    'JOIN ULTRA.HTT_ACTIVATION_HISTORY h on u.CUSTOMER_ID = h.CUSTOMER_ID ' .
    ($where ? "WHERE h.DEALER in ($where) " : NULL) .
    'GROUP BY U.PLAN_STATE';
  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  // pack result into array(plan_state => count) which already contains possible missing states
  foreach ($data as $row)
    $result[$row->state] = $row->customers;

  return $result;
}

/**
 * get_ultra_dealer_activations_by_plan
 *
 * return dealer's recent activations count by plan
 *
 * @param  integer $dealer
 * @return integer dealer customer count
 */
function get_ultra_dealer_activations_by_plan($dealer)
{
  // result is an array(plan => count)
  $result = array();
  foreach (array_merge(get_ultra_plans_short(), array('today')) as $short_name)
    $result[$short_name] = 0;

  $where = make_where_clause_for_dealer_and_children($dealer);

  if ( empty( $where ) )
  {
    dlog('',"No dealer found");

    return $result;
  }

  // get monthly activations
  \Ultra\Lib\DB\dealer_portal_connect();
  // get monthly activations
  $sql = sprintf("EXEC dbo.get_ultra_dealer_activations_by_cos_id %d", $dealer);
  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  // pack result into array(plan => count) which already contains possible missing plans
  $result['intraports'] = 0;
  foreach ($data as $row) {
    $result[get_plan_from_cos_id($row->cos_id)] = $row->activations;
    $result['intraports'] += $row->intraports;
  }

  teldata_change_db();
  // also get today's activations without breakdown by plan
  $sql = 'SELECT count(u.CUSTOMER_ID) customers FROM HTT_CUSTOMERS_OVERLAY_ULTRA u ' .
    'JOIN ULTRA.HTT_ACTIVATION_HISTORY h ON u.CUSTOMER_ID = h.CUSTOMER_ID '.
    "WHERE u.PLAN_STATE = 'Active' " .
    'AND h.PLAN_STARTED_DATE_TIME >= dbo.PT_TO_UTC(DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)) ' .
    ($where ? "AND h.DEALER in ($where) " : NULL);
  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  // add to result
  if ( ! empty($data) )
    $result['today'] = $data[0]->customers;

  return $result;
}

/**
 * make_where_clause_for_dealer_and_children
 *
 * create a WHERE clause for dealer and dealer's children (if found) for use with WHERE DEALER IN (...) query
 * dealers children are defined in celluphone DB tblDealerSite.ParentDealerSiteID
 *
 * @param  integer $dealer
 * @return string
 */
function make_where_clause_for_dealer_and_children($dealer)
{
  // administrator: all dealers
  if (empty($dealer))
    return NULL;

  $redis_key = 'ultra/celluphone/dealersiteid/dealer_and_children/'.$dealer;
  $redis     = new \Ultra\Lib\Util\Redis;

  // verify if the data has been cached
  $dealer_and_children = $redis->get( $redis_key );

  if ( ! empty( $dealer_and_children ) )
    return $dealer_and_children;

  // the data has not been cached, proceed with the query
  $celluphoneDB = \Ultra\UltraConfig\celluphoneDb();

  $data = mssql_fetch_all_rows(
    logged_mssql_query(
      sprintf(
        "SELECT DealerSiteID FROM $celluphoneDB..tblDealerSite WHERE DealerSiteID = %d OR ParentDealerSiteID = %d",
        $dealer,
        $dealer
      )
    )
  );

  if ( ! empty( $data ) && is_array( $data ) && count( $data ) )
  {
    $underscoreObject = new __ ;

    // comma-separated list of DealerSiteID's
    $data = implode( ',' , $underscoreObject->flatten( $data ) );

    // cache the data for 4 hours
    $redis->set( $redis_key , $data , 14400 );

    return $data;
  }

  // we cound not find any data
  return NULL;
}

/**
 * get_ultra_dealer_activations_by_month
 *
 * get activation information back x $months
 *
 * @param  integer $dealer
 * @param  integer $months
 * @return Array of objects
 */
function get_ultra_dealer_activations_by_month($dealer, $months = 2)
{
  $celluphone = \Ultra\UltraConfig\celluphoneDb();
  $sql = "
SELECT DATEADD(m, DATEDIFF(m, 0, LAST_UPDATED_DATE), 0) as month_start,
       COUNT(DEALER) AS activations
FROM   ULTRA.HTT_ACTIVATION_HISTORY WITH (NOLOCK)
WHERE  LAST_UPDATED_DATE BETWEEN dbo.pt_to_utc(DateAdd(Month, (DateDiff(Month, 0, GetDate()) - $months), 0)) AND dbo.pt_to_utc(GETDATE())
AND    DEALER = $dealer
AND    FINAL_STATE = '" . FINAL_STATE_COMPLETE . "'
GROUP BY DATEADD(m, DATEDIFF(m, 0, LAST_UPDATED_DATE), 0)";

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

function get_ultra_dealer_activations_by_month_stored_proc($dealer, $months = 2)
{
  $sql = sprintf("
    EXEC dbo.get_ultra_dealer_activations_by_month
      @dealer_id   = %d,
      @months      = %s
  ",
    $dealer,
    $months
  );

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

function get_ultra_dealer_customers_by_plan_stored_proc()
{

}

function get_ultra_dealer_activations_by_plan_stored_proc()
{

}
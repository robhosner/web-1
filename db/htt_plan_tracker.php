<?php

include_once('cosid_constants.php');

# php component to access DB table HTT_PLAN_TRACKER

#echo htt_plan_tracker_select_query(
#  array(
#    'customer_id'       => 10,
#  )
#)."\n";

#echo htt_plan_tracker_insert_query(
#  array(
#    'customer_id'  => 10,
#    'cos_id'       => 111,
#    'plan_started' => 'getutcdate()',
#    'plan_expires' => 'getutcdate()',
#    'period'       => 1,
#    'plan_updated' => 'getutcdate()'
#  )
#)."\n";

/**
 * htt_plan_tracker_insert_query
 * $params: plan_started
 *          plan_expires
 *          plan_updated
 *          period
 *          customer_id
 *          cos_id
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_plan_tracker_insert_query($params)
{
  if ( ! empty( $params['plan_started_sql'] ) )
    // it should not be included in single quotes
    $params['plan_started'] = $params['plan_started_sql'];
  elseif ( ( $params['plan_started'] != 'getutcdate()' ) && ( $params['plan_started'] != 'NULL' ) )
    $params['plan_started'] = "'".$params['plan_started']."'";

  if ( ( $params['plan_expires'] != 'getutcdate()' ) && ( $params['plan_expires'] != 'NULL' ) )
    $params['plan_expires'] = "'".$params['plan_expires']."'";

  if ( ( $params['plan_updated'] != 'getutcdate()' ) && ( $params['plan_updated'] != 'NULL' ) )
    $params['plan_updated'] = "'".$params['plan_updated']."'";

  if ( $params['period'] == 'increment' )
    $params['period'] = sprintf(
      "( SELECT coalesce( MAX(period)+1 , 1 ) FROM HTT_PLAN_TRACKER WHERE customer_id = %d )",
      $params['customer_id']
    );

  return sprintf(
    "INSERT INTO HTT_PLAN_TRACKER
     (
     customer_id,
     cos_id,
     plan_started,
     plan_expires,
     period,
     plan_updated,
     MONTH
     )
     VALUES
     (
     %d,
     %d,
     %s,
     %s,
     %s,
     %s,
     %d
     )
    ",
    $params['customer_id'],
    $params['cos_id'],
    $params['plan_started'],
    $params['plan_expires'],
    $params['period'],
    $params['plan_updated'],
    $params['month']
  );
}

/**
 * htt_plan_tracker_select_query 
 *
 * $params: customer_id
 * @param  array $params
 * @return string SQL
 */
function htt_plan_tracker_select_query($params)
{
  $htt_plan_tracker_select_query = sprintf(
    "SELECT *
     FROM   HTT_PLAN_TRACKER WITH (NOLOCK)
     WHERE  customer_id = %d
    ",
    $params['customer_id']
  );

  return $htt_plan_tracker_select_query;
}

/**
 * func_htt_plan_tracker_first_activation 
 *
 * @param  integer $customer_id
 * @param  integer $cos_id
 * @return array (success=>boolean, errors=>[])
 */
function func_htt_plan_tracker_first_activation($customer_id,$cos_id=NULL)
{
  $return = array('success'=>FALSE,'errors'=>array());

  $customer_data = mssql_fetch_all_objects(
    logged_mssql_query(
      sprintf(
      "SELECT u.plan_started , u.plan_expires , u.CUSTOMER_ID , a.COS_ID
       FROM   HTT_CUSTOMERS_OVERLAY_ULTRA u
       JOIN   ACCOUNTS                    a
       ON     a.CUSTOMER_ID = u.CUSTOMER_ID
       WHERE  a.CUSTOMER_ID = %d " , $customer_id
      )
    )
  );

  if ( $customer_data && count($customer_data) )
  {
    $result = htt_plan_tracker_first_activation(
      array(
        'customer' => $customer_data[0],
        'cos_id'   => $cos_id
      )
    );

    $return = $result->to_array();
  }
  else
    $return['errors'][] = "Customer not found";

  return $return;
}

/**
 * htt_plan_tracker_first_activation
 * $params: customer
 *          cos_id
 *          
 * Insert in data all as one would imagine, with period=1 and plan_updated = NULL
 *
 * @param  array $params
 * @return object Result
 */
function htt_plan_tracker_first_activation($params)
{
  $cos_id =
    ( isset($params['cos_id']) && $params['cos_id'] )
    ?
    $params['cos_id']
    :
    $params['customer']->COS_ID
    ;

  $htt_plan_tracker_insert_query = htt_plan_tracker_insert_query(
    array(
      'customer_id'  => $params['customer']->CUSTOMER_ID,
      'cos_id'       => $cos_id,
      'plan_started' => $params['customer']->plan_started,
      'plan_expires' => $params['customer']->plan_expires,
      'period'       => 1,
      'plan_updated' => 'NULL',
      'month'        => 1
    )
  );

  return run_sql_and_check_result($htt_plan_tracker_insert_query);
}

/**
 * func_htt_plan_tracker_plan_renewal 
 *
 * @param  integer $customer_id
 * @return array (success=>boolean, errors=>[])
 */
function func_htt_plan_tracker_plan_renewal($customer_id, $target_cos_id = NULL)
{
  $return = array('success'=>FALSE,'errors'=>array());

  $customer = get_customer_from_customer_id($customer_id);

  if ( $customer )
  {
    $month = 1;

    if (\Ultra\Lib\Util\validateMintBrandId($customer->BRAND_ID))
    {
      $mintMultiMonth = ultra_multi_month_overlay_from_customer_id($customer->CUSTOMER_ID);
      $month = $mintMultiMonth->UTILIZED_MONTHS;
    }
    else if (is_multi_month_plan($target_cos_id))
    {
      if ($multiMonth = multi_month_info($customer->CUSTOMER_ID))
        $month = $multiMonth['months_total'] - $multiMonth['months_left'];
    }

    $result = htt_plan_tracker_plan_renewal( array(
      'customer'      => $customer,
      'target_cos_id' => $target_cos_id,
      'month'         => $month
    ));

    $return = $result->to_array();
  }
  else
  {
    $return['errors'][] = "Customer not found";
  }

  return $return;
}

/**
 * htt_plan_tracker_plan_renewal 
 * $params: customer
 * @param  array $params
 * @return object Result 
 */
function htt_plan_tracker_plan_renewal($params)
{
  // Insert in data all as one would imagine, with period=max+1 and plan_updated = NULL. This includes a new cos_id if the customer did a plan change future.

  $result = new Result();

  $cos_id = NULL;

  if (isset($params['target_cos_id']) && $params['target_cos_id'])
  {
    $cos_id = $params['target_cos_id'];
  }
  else
  {
    $cos_id =
      ( $params['customer']->MONTHLY_RENEWAL_TARGET )
      ?
      get_cos_id_from_plan( $params['customer']->MONTHLY_RENEWAL_TARGET )
      :
      $params['customer']->COS_ID
      ;
  }

  $htt_plan_tracker_insert_query = htt_plan_tracker_insert_query(
    array(
      'customer_id'  => $params['customer']->CUSTOMER_ID,
      'cos_id'       => $cos_id,
      'plan_started' => $params['customer']->plan_started,
      'plan_expires' => $params['customer']->plan_expires,
      'period'       => 'increment',
      'plan_updated' => 'NULL',
      'month'        => $params['month']
    )
  );

  return run_sql_and_check_result($htt_plan_tracker_insert_query);
}

/**
 * func_htt_plan_tracker_plan_change 
 * @param  integer $customer_id
 * @param  integer $target_cos_id
 * @return array (success=>boolean, errors=>[])
 */
function func_htt_plan_tracker_plan_change($customer_id,$target_cos_id)
{
  $return = array('success'=>FALSE,'errors'=>array());

  $customer = get_customer_from_customer_id($customer_id,$target_cos_id);

  if ( $customer )
  {
    $result = htt_plan_tracker_plan_change( array('customer'=>$customer , 'target_cos_id'=>$target_cos_id) );

    $return = $result->to_array();
  }
  else
  {
    $return['errors'][] = "Customer not found";
  }

  return $return;
}

/**
 * htt_plan_tracker_plan_change 
 * $params: target_cos_id
 *          customer
 *          
 * @param  array $params
 * @return object Result
 */
function htt_plan_tracker_plan_change($params)
{
  // Insert in the new cos_id, re-using the current plan_started and plan_expires, with the SAME period (not max+1) - and plan_updated being getutcdate()

  $result = new Result();

  $sql = sprintf(
    "INSERT INTO HTT_PLAN_TRACKER
     (
     customer_id,
     cos_id,
     plan_started,
     plan_expires,
     period,
     plan_updated,
     MONTH
     )
     SELECT
       customer_id,
       %d,
       plan_started,
       plan_expires,
       period,
       getutcdate(),
       MONTH
     FROM  HTT_PLAN_TRACKER
     WHERE customer_id = %d 
     AND   period      = ( SELECT isnull( MAX(period) , 1 ) FROM HTT_PLAN_TRACKER WHERE customer_id = %d )
     ",
     $params['target_cos_id'],
     $params['customer']->CUSTOMER_ID,
     $params['customer']->CUSTOMER_ID
  );

  return run_sql_and_check_result($sql);
}

/**
 * htt_plan_tracker_plan_cancel 
 * Empty function
 * 
 * @param  array $params
 */
function htt_plan_tracker_plan_cancel($params)
{
  // Customer is cancelled - anything to insert?
}

/**
 * get_last_plan_tracker
 * returns latest entry from htt_plan_tracker for provided customer_id
 *
 * @param  Integer $customer_id
 * @return Object  $latest_plan
 */
function get_last_plan_tracker($customer_id)
{
  $sql = sprintf(
    "SELECT TOP 1 * FROM HTT_PLAN_TRACKER WITH (NOLOCK) WHERE CUSTOMER_ID = %d ORDER BY HTT_PLAN_TRACKER_ID DESC",
    $customer_id
  );

  $result = mssql_fetch_all_objects(logged_mssql_query($sql));
  return (count($result)) ? $result[0] : NULL;
}

?>

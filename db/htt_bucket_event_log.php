<?php

/**
 * log_bucket_event
 * params: action
 *         customer_id
 *         soc
 *         
 * @param  array $params
 * @return boolean
 */
function log_bucket_event($params) {

  $sql = sprintf(
    "INSERT INTO HTT_BUCKET_EVENT_LOG (
      ACTION,
      CUSTOMER_ID,
      SOC
    ) VALUES (
      %s,
      %d,
      %s
    )",
    mssql_escape_with_zeroes($params['action']),
    $params['customer_id'],
    mssql_escape_with_zeroes($params['soc'])
  );

  $return = run_sql_and_check_result($sql);

  return $return->is_success();
}

/**
 * log_bucket_event_by_msisdn
 * $params: msisdn
 *          customer_id
 * 
 * @param  array $params
 * @return object Result
 */
function log_bucket_event_by_msisdn($params) {

  $result = NULL;

  $customers = get_ultra_customers_from_msisdn($params['msisdn'],array('CUSTOMER_ID'));

  if ( $customers && is_array($customers) && count($customers) ) {

    if ( count($customers) == 1 ) {

      $params['customer_id'] = $customers[0]->CUSTOMER_ID;

      $result = log_bucket_event($params);

    } else {

      dlog('',"Error: multiple ultra customers for msisdn ".$params['msisdn']);

    }

  }

  return $result;
}

/**
 * get_bucket_event_log
 * $params: customer_id
 *          days_ago
 *         
 * @param  array $params
 * @return object[]
 */
function get_bucket_event_log($params) {

  $sql = sprintf(
    "SELECT *
     FROM   HTT_BUCKET_EVENT_LOG
     WHERE  CUSTOMER_ID = %d
     AND    DATEDIFF(dd, EVENT_DATE, GETUTCDATE()) < %d
     ORDER BY EVENT_DATE ASC",
    $params['customer_id'],
    $params['days_ago']
  );

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

?>

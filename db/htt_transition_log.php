<?php


include_once('db.php');
require_once('Ultra/Lib/Util/Redis.php');
require_once('Ultra/Lib/Util/Redis/Action.php');
require_once('Ultra/Lib/Util/Redis/Transition.php');

/*

php component to access DB table HTT_TRANSITION_LOG #

synopsis

echo htt_transition_log_select_query(
  array(
    'transition_uuid'    => '{44EFBC07-A635-00EC-09FF-91603675F247}',
    'NOT#CLOSED'         => TRUE,
    'NOT#ABORTED'        => TRUE
  )
);

echo htt_transition_log_select_query(
  array(
    'sql_limit' => 1,
    'status'      => 'OPEN',
    'customer_id' => 123,
    'order_by'    => 'ORDER BY t.CREATED ASC'
  )
);

echo htt_transition_log_insert_query(
  array(
    'customer_id'      => 123,
    'transition_uuid'  => '123456abcd',
    'status'           => 'CLOSED',
    'created'          => 'getutcdate()',
    'closed'           => 'getutcdate()',
    'from_cos_id'      => '123',
    'from_plan_state'  => 'ACTIVE',
    'to_cos_id'        => '321',
    'to_plan_state'    => 'DELETED',
    'context'          => 'test context'
  )
);

echo get_all_customer_state_transitions( 1915 );

echo htt_transition_log_select_query(
  array(
    'is_stuck' => TRUE
  )
);

*/

/**
 * htt_transition_log_open_count
 *
 * How many OPEN transitions are left to resolve?
 *
 * @return integer
 */
function htt_transition_log_open_count()
{
  $redis = new \Ultra\Lib\Util\Redis;

  $redis_open_count = $redis->get('ultra/transitions/open/count');
  if (!is_null($redis_open_count))
  {
    dlog('',"redis_open_count = $redis_open_count");
    return $redis_open_count;
  }

  $open_count = 0;

  $query = "SELECT COUNT(*)
    FROM  dbo.HTT_TRANSITION_LOG with (nolock)
    WHERE STATUS = 'OPEN'";

  $query_result = mssql_fetch_all_rows(logged_mssql_query($query));

  if ( $query_result && is_array($query_result) && is_array($query_result[0]) )
    $open_count = $query_result[0][0];

  dlog('',"open_count = $open_count");

  $redis->set('ultra/transitions/open/count', $open_count, 60);

  return $open_count;
}

/**
 * htt_transition_log_last_closed
 *
 * Given a $customer_id, returns the SQL query to retrieve his/her lastest closed transition
 *
 * @return string
 */
function htt_transition_log_last_closed($customer_id)
{
  $query = sprintf("
    DECLARE @CUSTOMER_ID BIGINT = %d;
    SELECT TOP(1)
    t.TRANSITION_UUID,
    t.TRANSITION_LABEL
    FROM  dbo.HTT_TRANSITION_LOG t
    WHERE t.CUSTOMER_ID = @CUSTOMER_ID
    AND   t.STATUS = 'CLOSED'
    ORDER BY t.CLOSED DESC
  ",$customer_id
  );

  return $query;
}

/**
 * htt_transition_log_activation_request_id
 *
 * Given a $customer_id, returns the SQL query to retrieve the TRANSITION_UUID of his/her activation
 *
 * @return string
 */
function htt_transition_log_activation_request_id($customer_id)
{
  $query = sprintf("
    DECLARE @CUSTOMER_ID BIGINT = %d;
    SELECT 
    t.TRANSITION_UUID
    FROM  dbo.HTT_TRANSITION_LOG t
    JOIN  dbo.HTT_ACTION_LOG a
    ON    t.TRANSITION_UUID = a.TRANSITION_UUID
    WHERE t.CUSTOMER_ID = @CUSTOMER_ID
    AND   a.ACTION_NAME IN ( 'mvneActivateSIM',  'mvneProvisionSIM' , 'mvneRequestPortIn' ) 
    ORDER BY t.CLOSED DESC
  ",$customer_id
  );

  return $query;
}

/**
 * get_actions_and_transitions
 *
 * Returns actions and transitions according to $params settings
 *
 * @return array
 */
function get_actions_and_transitions($params)
{
  $htt_transition_log_select_query = htt_transition_log_select_query($params);

  return mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_select_query));
}

/**
 * get_stuck_transitions
 *
 * @return array keyed by TRANSITION_UUID
 */
function get_stuck_transitions()
{
  $stuck_transitions = array();

  $htt_transition_log_select_query = htt_transition_log_select_query(
    array(
      'is_stuck' => TRUE,
      'order_by' => 'ORDER BY t.TRANSITION_UUID , ACTION_SEQ'
    )
  );

  $transitions = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_select_query));

  if ( $transitions && is_array($transitions) && count($transitions) )
  {
    dlog('',"transitions = %s",$transitions);

    foreach( $transitions as $transition )
    {
      if ( ! isset( $stuck_transitions[ $transition->TRANSITION_UUID ] ) )
      {
        $stuck_transitions[ $transition->TRANSITION_UUID ]['customer_id'] = $transition->CUSTOMER_ID;
        $stuck_transitions[ $transition->TRANSITION_UUID ]['created']     = $transition->CREATED;
        $stuck_transitions[ $transition->TRANSITION_UUID ]['from_state']  = $transition->FROM_PLAN_STATE;
        $stuck_transitions[ $transition->TRANSITION_UUID ]['to_state']    = $transition->TO_PLAN_STATE;
        $stuck_transitions[ $transition->TRANSITION_UUID ]['environment'] = $transition->HTT_ENVIRONMENT;
      }
    }
  }

  dlog('',"stuck_transitions = %s",$stuck_transitions);

  return $stuck_transitions;
}

/**
 * get_transition_by_uuid
 *
 * Given a TRANSITION_UUID, returns HTT_TRANSITION_LOG data
 *
 * @return stdObject $transition
 */
function get_transition_by_uuid( $transition_uuid )
{
  $transition = NULL;

  $query = sprintf("
  SELECT
    CUSTOMER_ID,
    TRANSITION_UUID,
    STATUS,
    CREATED,
    CLOSED,
    FROM_COS_ID,
    FROM_PLAN_STATE,
    TO_COS_ID,
    TO_PLAN_STATE,
    CONVERT(varchar(max), CONTEXT) CONTEXT,
    HTT_ENVIRONMENT,
    TRANSITION_LABEL,
    PRIORITY
  FROM  dbo.HTT_TRANSITION_LOG
  WHERE TRANSITION_UUID = %s",
  mssql_escape_with_zeroes( $transition_uuid )
  );

  $transition_data = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( $transition_data && is_array( $transition_data ) && count( $transition_data ) )
    $transition = $transition_data[0];

  return $transition;
}

/**
 * htt_transition_log_select_query
 * Generic function to compose a HTT_TRANSITION_LOG SELECT query.
 * $params: label
 *          transition_uuid
 *          customer_id
 *          NOT#CLOSED
 *          NOT#ABORTED
 *          status
 *          action_status
 *          created_mm_dd_yyyy
 *          created_dd_mm_yyyy
 *          real_customer
 *          from_plan_state
 *          to_plan_state
 *          is_stuck
 *          order_by
 *
 * @param  array $params
 * @return string SQL
 */
function htt_transition_log_select_query($params)
{
  $top_clause = get_sql_top_clause($params);

  $where = array();

  // a transition is considered stuck if it's OPEN for more than $is_stuck_minutes minutes
  $is_stuck_minutes = 30;

  if ( isset( $params["label"] ) )
  {
    if ( is_array( $params["label"] ) )
      $where[] = " t.TRANSITION_LABEL IN ( '" . implode("','",$params["label"]) . "' ) ";
    else
      $where[] = sprintf(" t.TRANSITION_LABEL = %s " , mssql_escape($params["label"]) );
  }

  if ( isset( $params["transition_uuid"] ) )
    $where[] = sprintf(" t.TRANSITION_UUID = %s " , mssql_escape($params["transition_uuid"]) );

  if ( isset( $params["customer_id"] ) && $params["customer_id"] )
    $where[] = " t.CUSTOMER_ID = ".$params["customer_id"];

  if ( ( isset( $params["NOT#CLOSED"] ) ) && $params["NOT#CLOSED"] )
    $where[] = " t.STATUS != 'CLOSED' ";

  if ( ( isset( $params["NOT#ABORTED"] ) ) && $params["NOT#ABORTED"] )
    $where[] = " t.STATUS != 'ABORTED' ";

  if ( isset( $params["status"] ) )
    $where[] = sprintf(" t.STATUS = %s " , mssql_escape($params["status"]) );

  if ( isset( $params["action_status"] ) )
    $where[] = sprintf(" a.STATUS = %s " , mssql_escape($params["action_status"]) );

  if ( isset($params['created_mm_dd_yyyy']) )
    $where[] = " convert(varchar, t.created, 110) = '".$params['created_mm_dd_yyyy']."' ";

  if ( isset($params['created_dd_mm_yyyy']) )
    $where[] = " convert(varchar, t.created, 105) = '".$params['created_dd_mm_yyyy']."' ";

  if ( isset($params['real_customer']) && $params['real_customer'] )
    $where[] = " t.CUSTOMER_ID > 0 ";

  if ( isset($params['from_plan_state']) )
    $where[] = sprintf(" t.FROM_PLAN_STATE = %s " , mssql_escape($params['from_plan_state']) );

  if ( isset($params['to_plan_state']) )
    $where[] = sprintf(" t.TO_PLAN_STATE = %s " , mssql_escape($params['to_plan_state']) );

  if ( isset($params['is_stuck']) && $params['is_stuck'] )
    $where[] = " t.STATUS = 'OPEN' AND DATEDIFF( mi , t.CREATED , GETUTCDATE() ) > $is_stuck_minutes ";

  $where_clause = ' WHERE '.implode(" AND ", $where);

  $order_by_clause = '';

  if ( isset( $params['order_by'] ) )
    $order_by_clause = $params['order_by'];

  return "SELECT $top_clause

     t.TRANSITION_UUID,
     t.STATUS,
     t.CUSTOMER_ID,
     t.FROM_COS_ID,
     t.FROM_PLAN_STATE,
     t.TO_COS_ID,
     t.TO_PLAN_STATE,
     t.TRANSITION_LABEL,
     t.CREATED,
     CONVERT(varchar(max), t.CONTEXT) CONTEXT,
     t.HTT_ENVIRONMENT,
     t.PRIORITY,
     DATEDIFF(ss, '1970-01-01', t.created) AS created_epoch,
     DATEDIFF(ss, '1970-01-01', t.closed) AS closed_epoch,

     CASE
      WHEN t.CLOSED IS NULL THEN 0
      ELSE 1
     END as is_closed_or_aborted,

     a.STATUS as ACTION_STATUS,
     a.ACTION_UUID,
     a.ACTION_RESULT,
     a.ACTION_NAME

     FROM dbo.HTT_TRANSITION_LOG t with (nolock)
     LEFT JOIN dbo.HTT_ACTION_LOG a with (nolock) ON t.TRANSITION_UUID = a.TRANSITION_UUID

     $where_clause
     $order_by_clause ";
}

/**
 * htt_transition_log_insert_query
 * Generic function to compose a HTT_TRANSITION_LOG INSERT query.
 * $params: customer_id
 *          transition_uuid
 *          status
 *          created
 *          closed
 *          from_cos_id
 *          from_plan_state
 *          to_cos_id
 *          to_plan_state
 *          context
 *
 * @param  array $params
 * @return string
 */
function htt_transition_log_insert_query($params)
{
  $query = sprintf(
    "
    DECLARE @CUSTOMER_ID     BIGINT        = %d;
    DECLARE @TRANSITION_UUID VARCHAR(128)  = %s;
    DECLARE @STATUS          VARCHAR(128)  = %s;
    DECLARE @FROM_COS_ID     BIGINT        = %d;
    DECLARE @FROM_PLAN_STATE VARCHAR(50)   = %s;
    DECLARE @TO_COS_ID       BIGINT        = %d;
    DECLARE @TO_PLAN_STATE   VARCHAR(50)   = %s;
    DECLARE @HTT_ENVIRONMENT VARCHAR(50)   = %s;
    DECLARE @CONTEXT         VARCHAR(1024) = '%s';

    INSERT INTO HTT_TRANSITION_LOG
(
  CUSTOMER_ID,
  TRANSITION_UUID,
  STATUS,
  CREATED,
  CLOSED,
  FROM_COS_ID,
  FROM_PLAN_STATE,
  TO_COS_ID,
  TO_PLAN_STATE,
  HTT_ENVIRONMENT,
  CONTEXT
)
VALUES
(
  @CUSTOMER_ID,
  @TRANSITION_UUID,
  @STATUS,
  GETUTCDATE(),
  GETUTCDATE(),
  @FROM_COS_ID,
  @FROM_PLAN_STATE,
  @TO_COS_ID,
  @TO_PLAN_STATE,
  @HTT_ENVIRONMENT,
  @CONTEXT
)
",
  $params['customer_id'],
  mssql_escape_with_zeroes( $params['transition_uuid'] ),
  mssql_escape_with_zeroes( $params['status'] ),
  $params['from_cos_id'],
  mssql_escape_with_zeroes( $params['from_plan_state'] ),
  $params['to_cos_id'],
  mssql_escape_with_zeroes( $params['to_plan_state'] ),
  mssql_escape_with_zeroes( get_htt_env() ),
  $params['context']
);

  return $query;
}

/**
 * get_aborted_state_transitions
 * 
 * Given a date range, returns all aborted transitions during that period
 * 
 * @param  string $created_mm_dd_yyyy
 * @param  string $from_plan_state
 * @param  string $to_plan_state
 * @param  integer $customer_id
 * @return array (errors,state_transitions,warnings)
 */
function get_aborted_state_transitions( $created_mm_dd_yyyy , $from_plan_state , $to_plan_state , $customer_id=NULL )
{
  // given a date, returns all aborted transitions

  $result = array( 'errors' => array() , 'state_transitions' => array() , 'warnings' => array() );

  $htt_transition_log_select_query = htt_transition_log_select_query(
    array(
      'customer_id'        => $customer_id,
      'status'             => 'ABORTED',
      'action_status'      => 'ABORTED',
      'from_plan_state'    => $from_plan_state,
      'to_plan_state'      => $to_plan_state,
      'created_mm_dd_yyyy' => $created_mm_dd_yyyy,
      'real_customer'      => TRUE,
      'order_by'           => 'ORDER BY t.CREATED ASC'
    )
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_select_query));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
    $result['state_transitions'] = $query_result;
  else
    $result['warnings'][] = "No data found.";

  return $result;
}

/**
 * get_all_customer_state_transitions
 *
 * Given a customer ID, returns all his/her state transitions
 *
 * @param  integer $customer_id
 * @return array (errors, state_transitions, warnings)
 */
function get_all_customer_state_transitions( $customer_id, $null_plan_state = FALSE )
{
  $result = array( 'errors' => array() , 'state_transitions' => array() , 'warnings' => array() );

  $where_clause = sprintf("WHERE t.customer_id = %d", $customer_id);
  if ( ! $null_plan_state)
    $where_clause .= ' AND ( FROM_PLAN_STATE IS NOT NULL OR TO_PLAN_STATE IS NOT NULL ) ';

  $select_attributes = "
  t.TRANSITION_UUID,
  t.STATUS,
  t.FROM_PLAN_STATE,
  t.TO_PLAN_STATE,
  t.TRANSITION_LABEL,
  dbo.UTC_TO_PT( t.CREATED ) CREATED_PST,
  CASE WHEN t.CLOSED IS NOT NULL THEN dbo.UTC_TO_PT( t.CLOSED  ) ELSE NULL END CLOSED_PST,
  t.CREATED,
  t.CLOSED,
  t.TO_COS_ID,
  a.ACTION_RESULT ";

  $sql = "
SELECT * FROM
( (
SELECT $select_attributes
FROM
  dbo.HTT_TRANSITION_LOG t with (nolock)
LEFT OUTER JOIN
  dbo.HTT_ACTION_LOG a with (nolock)
ON ( a.TRANSITION_UUID = t.TRANSITION_UUID AND a.ACTION_RESULT LIKE '%\"success\":false%' )
$where_clause
)
UNION
(
SELECT $select_attributes
FROM
  dbo.HTT_TRANSITION_ARCHIVE t with (nolock)
LEFT OUTER JOIN
  dbo.HTT_ACTION_ARCHIVE a with (nolock)
ON ( a.TRANSITION_UUID = t.TRANSITION_UUID AND a.ACTION_RESULT LIKE '%\"success\":false%' )
$where_clause
) ) HTT_TRANSITION_UNION ORDER BY CREATED DESC
";

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
    $result['state_transitions'] = $query_result;
  else
    $result['warnings'][] = "No data found.";

  return $result;
}

/**
 * get_portin_transitions_query
 *
 * Given a $customer_id, returns a SELECT query to retrieve his/her portin transitions
 *
 * @param  integer $customer_id
 * @return string SQL
 */
function get_portin_transitions_query($customer_id)
{
  $action_clause = "htt_transition_log.transition_uuid = a.transition_uuid AND
(a.action_type = 'async_command' AND a.action_name LIKE '%%PortIn')";

  $query = sprintf("SELECT *,
                           DATEDIFF(ss, '1970-01-01',  created) as created_epoch,
                           (SELECT TOP 1 action_name FROM dbo.HTT_ACTION_LOG a
                            WHERE $action_clause) as action
FROM dbo.HTT_TRANSITION_LOG
WHERE customer_id = %d AND
(SELECT count(action_uuid) FROM dbo.HTT_ACTION_LOG a
 WHERE $action_clause) > 0
ORDER BY created",
                   $customer_id);

  return $query;
}

/**
 * archive_transitions_query
 *
 * SQL query to archive transitions
 *   HTT_ACTION_PARAMETER_LOG rows are copied to HTT_ACTION_PARAMETER_ARCHIVE
 *   HTT_ACTION_LOG           rows are copied to HTT_ACTION_ARCHIVE
 *   HTT_TRANSITION_LOG       rows are copied to HTT_TRANSITION_ARCHIVE
 *
 * @return string SQL
 */
function archive_transitions_query()
{
  return "
BEGIN TRANSACTION ArchiveTransitions
BEGIN TRY

IF OBJECT_ID('tempdb..#TRANS_TO_ARCHIVE', 'U') IS NOT NULL
DROP TABLE #TRANS_TO_ARCHIVE

IF OBJECT_ID('tempdb..#ACTS_TO_ARCHIVE', 'U') IS NOT NULL
DROP TABLE #ACTS_TO_ARCHIVE

--holy crap this is great syntax
--http://sqlblog.com/blogs/merrill_aldrich/archive/2011/08/17/handy-trick-move-rows-in-one-statement.aspx
select TOP 2500 [TRANSITION_UUID] into #TRANS_TO_ARCHIVE from [HTT_TRANSITION_LOG] WHERE [CREATED] < dateadd(dd,-7,getutcdate()) and STATUS in ('ABORTED','CLOSED')
select [ACTION_UUID] into #ACTS_TO_ARCHIVE from [HTT_ACTION_LOG] WHERE [TRANSITION_UUID] in ( select * from #TRANS_TO_ARCHIVE )

select count(*) from #TRANS_TO_ARCHIVE
select count(*) from #ACTS_TO_ARCHIVE

DELETE HTT_ACTION_PARAMETER_LOG
          OUTPUT
          DELETED.ACTION_UUID, DELETED.PARAM, DELETED.VAL
          INTO HTT_ACTION_PARAMETER_ARCHIVE
          WHERE ACTION_UUID  IN ( select * from #ACTS_TO_ARCHIVE )

DELETE HTT_ACTION_LOG
          OUTPUT
          DELETED.ACTION_UUID, DELETED.TRANSITION_UUID, DELETED.STATUS, DELETED.CREATED, DELETED.PENDING_SINCE, DELETED.CLOSED, DELETED.ACTION_SEQ, DELETED.ACTION_TYPE, DELETED.ACTION_NAME, DELETED.ACTION_RESULT, DELETED.ACTION_SQL
          INTO dbo.HTT_ACTION_ARCHIVE
          WHERE ACTION_UUID  IN ( select * from #ACTS_TO_ARCHIVE )

DELETE HTT_TRANSITION_LOG
          OUTPUT
          DELETED.CUSTOMER_ID, DELETED.TRANSITION_UUID, DELETED.STATUS, DELETED.CREATED, DELETED.CLOSED, DELETED.FROM_COS_ID, DELETED.FROM_PLAN_STATE, DELETED.TO_COS_ID, DELETED.TO_PLAN_STATE, DELETED.CONTEXT, DELETED.HTT_ENVIRONMENT, DELETED.TRANSITION_LABEL
          INTO dbo.HTT_TRANSITION_ARCHIVE
          WHERE TRANSITION_UUID  IN ( select * from #TRANS_TO_ARCHIVE )


END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION ArchiveTransitions
END CATCH
COMMIT TRANSACTION ArchiveTransitions

-- db maintainance should not be done by applications

-- ALTER INDEX ALL ON HTT_TRANSITION_LOG  REORGANIZE ;
-- ALTER INDEX ALL ON HTT_ACTION_PARAMETER_LOG  REORGANIZE ;
-- ALTER INDEX ALL ON HTT_ACTION_LOG  REORGANIZE ;

-- ALTER INDEX ALL ON HTT_TRANSITION_ARCHIVE  REBUILD ;
-- ALTER INDEX ALL ON HTT_ACTION_RESULT_ARCHIVE  REBUILD ;
-- ALTER INDEX ALL ON HTT_ACTION_PARAMETER_ARCHIVE  REBUILD ;
-- ALTER INDEX ALL ON HTT_ACTION_ARCHIVE  REBUILD ;
";
}

/**
 * open_transactions_reaper
 *
 * Returns a query to clean up ancient transitions
 *
 * @param  string $mode
 * @param  string $max_age
 * @return string SQL
 */
function open_transactions_reaper( $mode , $max_age )
{
  $clause = " STATUS = 'OPEN'
       AND    DATEDIFF(ss, COALESCE(CREATED, GETUTCDATE()), GETUTCDATE()) > $max_age";

  $finder = "
      SELECT TRANSITION_LABEL, TRANSITION_UUID, CUSTOMER_ID, CREATED
      FROM   dbo.HTT_TRANSITION_LOG
      WHERE $clause";

  foreach (mssql_fetch_all_objects(logged_mssql_query($finder)) as $transition)
  {
    dlog('',
           "$mode: Found OPEN transition %s %s %s to be aborted, created %s (%d seconds)",
           $transition->TRANSITION_UUID,
           $transition->CUSTOMER_ID,
           $transition->TRANSITION_LABEL,
           $transition->CREATED,
           $max_age);
  }

  return "UPDATE HTT_TRANSITION_LOG SET STATUS = 'ABORTED', CLOSED = GETUTCDATE() WHERE $clause";
}

/**
 * log_transition
 *
 * Save data to DB table HTT_TRANSITION_LOG
 *
 * @param  string $transition_uuid
 * @param  array $context
 * @param  integer $from_cos_id
 * @param  string $from_plan_state
 * @param  integer $to_cos_id
 * @param  string $to_plan_state
 * @param  string $trans_label
 * @param  integer $priority
 * @return boolean
 */
function log_transition($transition_uuid, $context,
                        $from_cos_id, $from_plan_state,
                        $to_cos_id, $to_plan_state,
                        $trans_label,
                        $priority=6,
                        $env=NULL
)
{
  // reserve this transition immediately in order to avoid a race condition
  $success = reserve_transition_uuid_by_pid( $transition_uuid );

  if ( ! $success )
    return $success;

  if (empty($env))
    $env = get_htt_env();

  $q = sprintf("
    DECLARE @CUSTOMER_ID      BIGINT        = %d;
    DECLARE @TRANSITION_UUID  VARCHAR(128)  = '%s';
    DECLARE @FROM_COS_ID      BIGINT        = %d;
    DECLARE @FROM_PLAN_STATE  VARCHAR(50)   = %s;
    DECLARE @TO_COS_ID        BIGINT        = %d;
    DECLARE @TO_PLAN_STATE    VARCHAR(50)   = %s;
    DECLARE @CONTEXT          VARCHAR(1024) = %s;
    DECLARE @HTT_ENVIRONMENT  VARCHAR(25)   = '%s';
    DECLARE @TRANSITION_LABEL VARCHAR(60)   = '%s';
    DECLARE @PRIORITY         TINYINT       = %d;

    INSERT INTO htt_transition_log
    (
      CUSTOMER_ID,
      TRANSITION_UUID,
      FROM_COS_ID,
      FROM_PLAN_STATE,
      TO_COS_ID,
      TO_PLAN_STATE,
      CONTEXT,
      HTT_ENVIRONMENT,
      TRANSITION_LABEL,
      PRIORITY
    )
    VALUES 
    (
      @CUSTOMER_ID,
      @TRANSITION_UUID,
      @FROM_COS_ID,
      @FROM_PLAN_STATE,
      @TO_COS_ID,
      @TO_PLAN_STATE,
      @CONTEXT,
      @HTT_ENVIRONMENT,
      @TRANSITION_LABEL,
      @PRIORITY
    );",
    $context['customer_id'],
    $transition_uuid,
    $from_cos_id,
    mssql_quote_or_null($from_plan_state),
    $to_cos_id,
    mssql_quote_or_null($to_plan_state),
    mssql_escape(enclose_JSON($context)),
    $env,
    $trans_label,
    $priority
  );

  $success = run_sql_and_check($q);

  if ( $success )
  {
    $redis_transition = new \Ultra\Lib\Util\Redis\Transition();

    // add new transition object in Redis
    $redis_transition->addObject(
      $transition_uuid,
      $priority,
      $context['customer_id'],
      $from_cos_id,
      $from_plan_state,
      $to_cos_id,
      $to_plan_state,
      $trans_label,
      get_utc_date(),
      \Ultra\UltraConfig\getTransitionDelayTime($trans_label),
      $env
    );
  }

  return $success;
}

/**
 * get_next_transition_from_redis
 *
 * Provides the next OPEN transition to be resolved, accessing Redis data structures
 *
 * @param  array $environments
 * @return object $next_transition
 */
function get_next_transition_from_redis( $environments )
{
  dlog('', '(%s)', func_get_args());

  $next_transition = NULL;

  if ( ! is_array( $environments ) )
    $environments = array( $environments );

  try
  {
    if ( ! count( $environments ) )
      throw new Exception("No environments given");

    $redis = new \Ultra\Lib\Util\Redis;

    $redis_transition = new \Ultra\Lib\Util\Redis\Transition( $redis );

    $priority = '0'; # [0..9]

    // loop through priorities
    while( ( ! $next_transition ) && ( $priority < 10 ) )
    {
      // loop through environments
      foreach( $environments as $environment )
      {
        if ( ! $next_transition )
        {
          // get transition sorted set (by time) identified by $environment and $priority
          $transition_list = $redis_transition->getListByEnvironmentAndPriority( $environment , $priority );

          if ( $transition_list && is_array($transition_list) && count($transition_list) )
          {
            dlog('',"transition_list = %s",$transition_list);

            // get the first element from the current transition list
            $transition_uuid = array_shift($transition_list);

            // loop through transition sorted set
            while( ( ! ( $next_transition ) ) && $transition_uuid )
            {
              // get transition from DB - TODO: we could access the data directly from Redis!
              if ( ! is_transition_uuid_reserved_by_pid( $transition_uuid ) )
                $next_transition = get_transition_by_uuid( $transition_uuid );

              if ( $next_transition
                && ( $next_transition->STATUS == 'OPEN' )
                && ( reserve_transition_uuid_by_pid( $transition_uuid, NULL, $next_transition->CUSTOMER_ID ) )
              )
              {
                // TODO: we should check if another transition is already running for the same customer and discard this one if necessary
              }
              else
              {
                // if STATUS != 'OPEN', we should remove it from Redis
                if ( $next_transition && is_object($next_transition) && ( $next_transition->STATUS != 'OPEN' ) )
                {
                  $redis_action = new \Ultra\Lib\Util\Redis\Action( $redis );
                  remove_transition_from_redis( $transition_uuid , $environment , $priority , $redis_action , $redis_transition , $redis );
                }

                // either $next_transition is not OPEN or it has been already reserved
                $next_transition = NULL;
              }

              // get the next element from the current transition list
              $transition_uuid = array_shift($transition_list);
            }
          }
        }

      } // loop through environments

      $priority++;

    } // loop through priorities
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());
  }

  dlog('', 'returning : %s', $next_transition);

  return $next_transition;
}

/**
 * get_next_transition
 *
 * Provides the next transition to be resolved by the current process
 *
 * @param  integer $customer_id
 * @param  array $environments
 * @return object $next_transition
 */
function get_next_transition($customer_id=NULL, $environments=NULL)
{
  $fields = array('status' => 'OPEN');

  if (NULL == $environments)
  {
    if (get_htt_env())
      $fields['HTT_ENVIRONMENT'] = get_htt_env();
    else
      // this is a potentially dangerous situation in which transitions saved in an environment are being retrieved from an "unauthorized" environment
      dlog('', "please set HTT_ENV!!!  Returning all transitions for now.");
  }
  else
    $fields['HTT_ENVIRONMENT'] = $environments;

  if ( NULL != $customer_id )
    $fields['customer_id'] = $customer_id;

  $next_transition = NULL;

  if ( isset( $fields['customer_id'] ) )
  {
    /* select the next transition for the same customer with status OPEN */
    /* we wait 2 seconds from their creation because of a race condition with HTT_ACTION_PARAMETER_LOG insertions */

    $next_transition = find_first(sprintf("SELECT TOP 1 
      CUSTOMER_ID,
      TRANSITION_UUID,
      STATUS,
      CREATED,
      CLOSED,
      FROM_COS_ID,
      FROM_PLAN_STATE,
      TO_COS_ID,
      TO_PLAN_STATE,
      CONVERT(varchar(max),CONTEXT) as CONTEXT,
      HTT_ENVIRONMENT,
      TRANSITION_LABEL,
      PRIORITY
    FROM HTT_TRANSITION_LOG with (nolock)
    WHERE CUSTOMER_ID = %d
    AND HTT_ENVIRONMENT = '%s'
    AND status = 'OPEN'
    AND DATEDIFF(ss, htt_transition_log.created, GETUTCDATE()) > 1
    ORDER BY PRIORITY, CREATED",
    $fields['customer_id'],
    $fields['HTT_ENVIRONMENT']));
  }
  else
  {
    $next_transition = get_next_transition_from_redis( $fields['HTT_ENVIRONMENT'] );

    // we wait 1 second because of a race condition with HTT_ACTION_PARAMETER_LOG insertions
    sleep(1);
  }

  // Attempt to reserve this Transition UUID
  if ( $next_transition && $next_transition->TRANSITION_UUID )
  {
    $success = reserve_transition_uuid_by_pid( $next_transition->TRANSITION_UUID, NULL, $next_transition->CUSTOMER_ID );

    if ( $success )
      dlog('',"process id %d reserved Transition UUID %s",getmypid(),$next_transition->TRANSITION_UUID);
    else
      dlog('',"process id %d could not reserve Transition UUID %s",getmypid(),$next_transition->TRANSITION_UUID);

    if ( ! $success )
      $next_transition = NULL;
  }


  return $next_transition;
}

/**
 * get_customer_id_from_transition_uuid
 *
 * Given a $transition_uuid, returns the related CUSTOMER_ID
 *
 * @param  string $transition_uuid
 * @return integer $customer_id
 */
function get_customer_id_from_transition_uuid($transition_uuid)
{
  $customer_id = NULL;

  $transition = get_transition_by_uuid( $transition_uuid );

  if ( $transition && is_object($transition) )
    $customer_id = $transition->CUSTOMER_ID;

  return $customer_id;
}

/**
 * count_customer_open_transitions
 *
 * How many OPEN transitions are left for $customer_id ?
 *
 * @param  integer $customer_id
 * @return integer $count
 */
function count_customer_open_transitions( $customer_id )
{
  $count = 0;

  $sql = sprintf("
    DECLARE @CUSTOMER_ID BIGINT = %d;

    SELECT count(*)
    FROM   dbo.HTT_TRANSITION_LOG t
    WHERE  t.customer_id = @CUSTOMER_ID
    AND    t.STATUS = 'OPEN'",
    $customer_id
  );

  dlog('',$sql);

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result )
  {
    $count = $query_result[0][0];
  }

  dlog('',"count = $count");

  return $count;
}

/**
 * close_transition
 *
 * Closes a transition
 *
 * @param  string $transition_uuid
 * @param  object $redis Redis
 * @param  string $redis_transition
 * @param  integer $customer_id
 * @return boolean
 */
function close_transition($transition_uuid,$redis=NULL,$redis_transition=NULL,$customer_id=NULL)
{
  $success = transition_set_status( $transition_uuid , 'CLOSED' , $redis , $customer_id );

  if ( $success )
  {
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $transition_uuid );

    if ( is_null($redis) )
      $redis = new \Ultra\Lib\Util\Redis;

    if ( is_null($redis_transition) )
      $redis_transition = new \Ultra\Lib\Util\Redis\Transition( $redis );

    // destroy transition in Redis
    $redis_transition->deleteObject( $t_uuid );
  }

  return $success;
}

/**
 * abort_transition
 *
 * Aborts a transition
 *
 * @param  string $transition_uuid
 * @param  object $redis Reds
 * @param  string $redis_transition
 * @param  integer $customer_id
 * @return boolean
 */
function abort_transition($transition_uuid,$redis=NULL,$redis_transition=NULL,$customer_id=NULL)
{
  $success = transition_set_status( $transition_uuid , 'ABORTED' , $redis , $customer_id );

  if ( $success )
  {
    $t_uuid = preg_replace( '/[\{\}]/' , '' , $transition_uuid );

    if ( is_null($redis) )
      $redis = new \Ultra\Lib\Util\Redis;

    if ( is_null($redis_transition) )
      $redis_transition = new \Ultra\Lib\Util\Redis\Transition( $redis );

    // destroy transition in Redis
    $redis_transition->deleteObject( $t_uuid );
  }

  return $success;
}

/**
 * transition_set_status
 *
 * Assign the value of HTT_TRANSITION_LOG.STATUS
 *
 * @param  string $transition_uuid
 * @param  string $status
 * @param  object $redis Redis
 * @param  integer $customer_id
 * @return boolean
 */
function transition_set_status( $transition_uuid , $status , $redis=NULL , $customer_id )
{
  $query = sprintf("
    DECLARE @STATUS          VARCHAR(128) = '%s';
    DECLARE @TRANSITION_UUID VARCHAR(128) = %s;

    UPDATE HTT_TRANSITION_LOG
    SET    STATUS          = @STATUS,
           CLOSED          = GETUTCDATE()
    WHERE  TRANSITION_UUID = @TRANSITION_UUID",
    $status,
    mssql_escape_with_zeroes($transition_uuid)
  );

  $success = run_sql_and_check($query);

  if ( $success )
  {
    if ( is_null($redis) || !is_object($redis) )
      $redis = new \Ultra\Lib\Util\Redis;

    $t_uuid = preg_replace( '/[\{\}]/' , '' , $transition_uuid );

    // update transition in Redis
    $redis->hset( 'ultra/transition/'.$t_uuid , 'status' , $status );

    if ( $customer_id )
      $redis->set( 'ultra/transition/by_customer_id/'.$customer_id.'/status' , $status , 60 * 60 * 24 );
  }

  return $success;
}

/**
 * abort_action_and_transition
 *
 * Aborts an action and its transition
 *
 * @param  string $transition_uuid
 * @param  string $action_uuid
 * @param  string $action_result
 * @param  integer $customer_id
 * @return NULL
 */
function abort_action_and_transition($transition_uuid, $action_uuid, $action_result=NULL, $customer_id=NULL)
{
  abort_action($action_uuid, $action_result);
  abort_transition($transition_uuid,$customer_id);
}

/**
 * redo_transition
 *
 * Re-do redoable transitions.
 * It will skip CLOSED actions; it will re-OPEN ABORTED or RUNNING actions.
 *
 * @param  string $transition_uuid
 * @return boolean
 */
function redo_transition($transition_uuid)
{
  dlog('',"$transition_uuid");

  $success = FALSE;

  try
  {
    $query = sprintf("
      UPDATE HTT_ACTION_LOG
      SET    STATUS = 'OPEN'
      WHERE  STATUS != 'CLOSED'
      AND    TRANSITION_UUID = %s",
      mssql_escape_with_zeroes($transition_uuid)
    );

    if ( ! run_sql_and_check($query) )
      throw new Exception("UPDATE HTT_ACTION_LOG failed!");

    $query = sprintf("
      UPDATE HTT_TRANSITION_LOG
      SET    STATUS = 'OPEN'
      WHERE  STATUS = 'ABORTED'
      AND    TRANSITION_UUID = %s",
      mssql_escape_with_zeroes($transition_uuid)
    );

    if ( ! run_sql_and_check($query) )
      throw new Exception("UPDATE HTT_TRANSITION_LOG failed!");

    // handle Redis data structures

    $redis            = new \Ultra\Lib\Util\Redis;
    $redis_transition = new \Ultra\Lib\Util\Redis\Transition( $redis );
    $redis_action     = new \Ultra\Lib\Util\Redis\Action( $redis );

    $transition_object = $redis_transition->getObject( $transition_uuid );

    if ( ( ! $transition_object ) || !count($transition_object) )
    {
      $transition = get_transition_by_uuid( $transition_uuid );

      if ( !$transition )
        throw new Exception("ERROR: transition $transition_uuid not found");

      // add new transition object in Redis
      $redis_transition->addObject(
        $transition->TRANSITION_UUID,
        $transition->PRIORITY,
        $transition->CUSTOMER_ID,
        $transition->FROM_COS_ID,
        $transition->FROM_PLAN_STATE,
        $transition->TO_COS_ID,
        $transition->TO_PLAN_STATE,
        $transition->TRANSITION_LABEL,
        $transition->CREATED
      );

      $redis->set( 'ultra/transition/by_customer_id/'.$transition->CUSTOMER_ID.'/status' , 'OPEN' , 60 * 60 * 24 );
    }

    // add actions to Redis if needed

    // get data from HTT_ACTION_LOG
    $sql = htt_action_log_select_query(
      array(
        "transition_uuid" => $transition_uuid
      )
    );

    $actions_data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( ! ( $actions_data && is_array( $actions_data ) && count( $actions_data ) ) )
      throw new Exception("ERROR: actions not found for transition $transition_uuid");

    // get action UUIDs from Redis
    $listByTransition = $redis_action->getListByTransition( $transition_uuid );

    // loop through HTT_ACTION_LOG rows
    foreach( $actions_data as $action_data )
    {
      $a_uuid = preg_replace( '/[\{\}]/' , '' , $action_data->ACTION_UUID );

      if ( ! in_array( $a_uuid , $listByTransition ) && ( $action_data->STATUS == 'OPEN' ) )
      {
        // Add action to Redis only if OPEN
        $redis_action->addObject( $a_uuid , $transition_uuid , $action_data->ACTION_TYPE , $action_data->ACTION_NAME , 'OPEN' , $action_data->ACTION_SEQ );
      }
      else
      {
        if ( $action_data->STATUS == 'OPEN' )
        {
          $redis->hmset( 'ultra/action/'.$a_uuid , array( 'action_seq' => $action_data->ACTION_SEQ , 'status' => 'OPEN' ) );
        }
        else
        {
          // we have to remove the Action Object from Redis
          $redis_action->deleteObject( $action_data->ACTION_UUID );
        }
      }
    }

    $success = TRUE;
  }
  catch(\Exception $e)
  {
    dlog('', $e->getMessage());
  }

  return $success;
}

/**
 * get_open_activation_transition_uuid_from_iccid
 *
 * Given an Orange ICCID, we want to retrieve the TRANSITION_UUID of the open 'Orange Sim Activation' transition, if any
 *
 * @param  string $iccid_full
 * @return string or NULL
 */
function get_open_activation_transition_uuid_from_iccid( $iccid_full )
{
  $transition_uuid = NULL;

  $sql = sprintf(
    " SELECT TRANSITION_UUID
      FROM   dbo.HTT_CUSTOMERS_OVERLAY_ULTRA o
      JOIN   dbo.HTT_TRANSITION_LOG t
      ON     o.CUSTOMER_ID = t.CUSTOMER_ID
      WHERE  t.STATUS = 'OPEN'
      AND    o.CURRENT_ICCID_FULL = '%s'
    ",$iccid_full
  )." AND    t.TRANSITION_LABEL LIKE 'Orange Sim Activation %' ";

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
  {
    $transition_uuid = $query_result[0][0];
  }

  return $transition_uuid;
}

/**
 * get_activation_request_id 
 * @param  integer $customer_id
 * @return string $activation_request_id
 */
function get_activation_request_id($customer_id)
{
  $activation_request_id = '';

  $htt_transition_log_query = htt_transition_log_activation_request_id($customer_id);

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_transition_log_query));

  if ( ( is_array($query_result) ) && count($query_result) > 0 )
  {
    $activation_request_id = $query_result[0]->TRANSITION_UUID;
  }

  return $activation_request_id;
}

/**
 * htt_transition_log_cos_id_from_customer_id
 * @param  integer $customer_id
 * @return object
 */
function htt_transition_log_to_cos_id_from_customer_id($customer_id)
{
  $sql = sprintf("SELECT a.to_cos_id FROM dbo.htt_transition_log a     WHERE a.to_cos_id > 1 AND a.customer_id = %d
            UNION SELECT b.to_cos_id FROM dbo.htt_transition_archive b WHERE b.to_cos_id > 1 AND b.customer_id = %d",
            $customer_id, $customer_id);

  return find_first($sql);
}

?>

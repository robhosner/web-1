<?php

# echo htt_accounts_acquisition_insert_query('1',array())."\n";

/**
 * htt_accounts_acquisition_insert_query 
 * $utm: utmgclid
 *       utmscr
 *       utmcmd
 *       utmctr
 *       utmcct
 *       utmccn
 *       
 * @param  string $customer
 * @param  array $utm
 * @return string SQL
 */
function htt_accounts_acquisition_insert_query($customer,$utm)
{
  return sprintf("IF NOT EXISTS (SELECT * FROM [dbo].[htt_accounts_acquisition] WHERE ACCOUNT = '%s') BEGIN
  INSERT INTO [dbo].[htt_accounts_acquisition]
   ([ACCOUNT], [gclid], [source], [medium], [keyword], [content], [campaign],
    [acq_source])
   VALUES('%s', %s, %s, %s, %s, %s, %s, 'api')
END",
                 $customer,
                 $customer,
                 array_key_exists('utmgclid', $utm) ? mssql_escape($utm['utmgclid']) : "''",
                 array_key_exists('utmcsr', $utm) ? mssql_escape($utm['utmcsr']) : "''",
                 array_key_exists('utmcmd', $utm) ? mssql_escape($utm['utmcmd']) : "''",
                 array_key_exists('utmctr', $utm) ? mssql_escape($utm['utmctr']) : "''",
                 array_key_exists('utmcct', $utm) ? mssql_escape($utm['utmcct']) : "''",
                 array_key_exists('utmccn', $utm) ? mssql_escape($utm['utmccn']) : "''"
  );
}

?>

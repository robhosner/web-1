<?php

/**
 * PHP module to acces ULTRA.ACTIVATION_LOG_OVERRIDE table
 * @see MVNO-2932
 * @param  string $iccid
 * @return objects[] activation log override data
 */

function get_activation_log_override($iccid)
{
  if (empty($iccid) || strlen($iccid) != 19)
  {
    dlog('', 'ERROR: invalid parameter iccid');
    return NULL;
  }

  $celluphone  = \Ultra\UltraConfig\celluphoneDb();

  // schema weirdness: ULTRA.ACTIVATION_LOG_OVERRIDE.STORE is actually DEALER ID
  $sql = sprintf('SELECT
    l.ACTIVATION_ICCID, l.TYPE, l.NEW_MASTERAGENT, l.NEW_DISTRIBUTOR, l.NEW_STORE, l.NEW_USERID, l.CREATED_BY, l.CREATED_DATE_TIME, l.NOTES,
    m.MasterCd AS new_masteragent_code, s.Dealercd AS new_dealer_code, d.MasterCd AS new_distributor_code,
    m.BusinessName AS new_masteragent_name, s.BusinessName AS new_dealer_name, d.BusinessName AS new_distributor_name 
    FROM ULTRA.ACTIVATION_LOG_OVERRIDE l WITH (NOLOCK)
    LEFT JOIN ' . $celluphone . '..[tblMaster] m WITH (NOLOCK) ON l.NEW_MASTERAGENT = m.MasterID
    LEFT JOIN ' . $celluphone . '..[tblDealerSite] s WITH (NOLOCK) on l.NEW_STORE = s.DealerSiteID
    LEFT JOIN ' . $celluphone . '..[tblMaster] d WITH (NOLOCK) ON l.NEW_DISTRIBUTOR = d.MasterID
    WHERE l.ACTIVATION_ICCID = %s', mssql_escape_with_zeroes($iccid));
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}


/**
 * save_activation_log_override
 * update or insert ULTRA.ACTIVATION_LOG_OVERRIDE record by ICCID
 * @param  string $iccid
 * @param  integer $type
 * @param  integer $new_masteragent
 * @param  integer $new_distributor
 * @param  integer $new_store
 * @param  integer $new_userid
 * @param  string $created_by
 * @param  string $created_date_time
 * @param  string $notes
 * @return boolean TRUE on success of FALSE on failer
 */
function save_activation_log_override($iccid, $type, $new_masteragent, $new_distributor, $new_store, $new_userid, $created_by, $created_date_time, $notes)
{
  if (empty($iccid) || empty($type) || empty($created_by) || empty($created_date_time) || empty($notes))
  {
    dlog('', 'ERROR: invalid parameters');
    return FALSE;
  }
  dlog('', '%s', func_get_args());

  $table = 'ULTRA.ACTIVATION_LOG_OVERRIDE';
  $values = array(
    'TYPE'              => $type,
    'NEW_MASTERAGENT'   => $new_masteragent,
    'NEW_DISTRIBUTOR'   => $new_distributor,
    'NEW_STORE'         => $new_store,
    'NEW_USERID'        => $new_userid,
    'CREATED_BY'        => $created_by,
    'CREATED_DATE_TIME' => $created_date_time,
    'NOTES'             => $notes);
  $clause = array('ACTIVATION_ICCID' => $iccid);
  $select = \Ultra\Lib\DB\makeSelectQuery($table, NULL, 'NULL', $clause);
  $update = \Ultra\Lib\DB\makeUpdateQuery($table, $values, $clause);
  $insert = \Ultra\Lib\DB\makeInsertQuery($table, array_merge($clause, $values));

  return run_sql_and_check("IF EXISTS ($select) $update ELSE $insert");
}

/**
 * undo_activation_log_override
 * removes a record from ULTRA.ACTIVATION_LOG_OVERRIDE record by ICCID
 * @param  string $iccid
 * @return boolean TRUE on success of FALSE on failure
 */
function undo_activation_log_override($iccid)
{
  if (empty($iccid))
  {
    dlog('', 'ERROR: invalid parameters');
    return FALSE;
  }
  dlog('', '%s', func_get_args());

  $sql = \Ultra\Lib\DB\makeDeleteQuery('ULTRA.ACTIVATION_LOG_OVERRIDE', array('ACTIVATION_ICCID' => $iccid));

  return run_sql_and_check($sql);
}
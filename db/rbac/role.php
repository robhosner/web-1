<?php

/**
 * get_activity_by_role
 *
 * Given a Role, returns all allowed activities for that role.
 * Joins ROLE with ROLE_ACTIVITY, ACTIVITY, ACTIVITY_TYPE
 *
 * @param  integer $role_id
 * @param  string $role_name
 * @param  boolean $is_active
 * @return array
 */
function get_activity_by_role( $role_id=NULL , $role_name=NULL , $is_active=NULL )
{
  $activity = array();

  if ( !$role_id && !$role_name )
    return $activity;

  $where = array();

  if ( $role_id )
    $where[] = sprintf(" ROLE_ID = %d ",$role_id);

  if ( $role_name )
    $where[] = sprintf(" ROLE_NAME = %s ",mssql_escape_with_zeroes($role_name));

  if ( !is_null($is_active) )
    $where[] = sprintf(" ROLE_ACTIVE_FLAG = %d ",$is_active);

  $sql = "
    SELECT
      a.ACTIVITY_ID,
      a.ACTIVITY_NAME,
      at.ACTIVITY_TYPE_NAME
    FROM
      DP_RBAC.ROLE r
    JOIN
      DP_RBAC.ROLE_ACTIVITY ra
    ON
      r.ROLE_ID = ra.ROLE_ID
    JOIN
      DP_RBAC.ACTIVITY a
    ON
      a.ACTIVITY_ID = ra.ACTIVITY_ID
    JOIN
      DP_RBAC.ACTIVITY_TYPE at
    ON
      a.ACTIVITY_TYPE_ID = at.ACTIVITY_TYPE_ID
    WHERE
      ".implode(" AND ", $where);

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) )
    $activity = $data;

  return $activity;
}

/**
 * get_api_by_role
 *
 * Given a Role, returns all allowed apis for that role.
 * Joins ROLE with ROLE_ACTIVITY, ACTIVITY , API_ACTIVITY, API, ACTIVITY_TYPE and optionally PLAN_STATE
 *
 * @param  integer $role_id
 * @param  string $role_name
 * @param  string $api_name
 * @param  boolean $is_active
 * @param  boolean $plan_state also return plan_state_name for each action (this JOIN will create duplicate action rows)
 * @return object[] $api_list
 */
function get_api_by_role( $role_id=NULL , $role_name=NULL , $api_name=NULL , $is_active=NULL, $plan_state = FALSE )
{
  $api_list = array();

  if ( !$role_id && !$role_name )
    return $api_list;

  if (! $role_id)
    $role_id = \Ultra\Lib\DB\Getter\getScalar('ROLE_NAME', $role_name, 'ROLE_ID');

  // attempt to get values from cache for the most common types of queries
  if (! $plan_state && ! $is_active)
  {
    $redis = new \Ultra\Lib\Util\Redis;
    $key = "ultra/api_by_role/$role_id" . ($api_name ? "/$api_name" : NULL);
    if ($list = $redis->get($key))
      return json_decode($list);
  }

  $where = array();
  if ( $role_id )
    $where[] = sprintf(" r.ROLE_ID = %d ",$role_id);
  elseif ( $role_name )
    $where[] = sprintf(" r.ROLE_NAME = %s ",mssql_escape_with_zeroes($role_name));

  if ( !is_null($is_active) )
  {
    $where[] = sprintf(" r.ROLE_ACTIVE_FLAG = %d ",$is_active);
    $where[] = sprintf(" p.API_ACTIVE_FLAG  = %d ",$is_active);
    $where[] = sprintf(" aa.ACTIVE_FLAG     = %d ",$is_active);
  }

  if ( !is_null($api_name ) )
    $where[] = sprintf(" p.API_NAME = %s ",mssql_escape_with_zeroes($api_name));

  $sql = '
    SELECT
      a.ACTIVITY_ID,
      a.ACTIVITY_NAME,
      at.ACTIVITY_TYPE_NAME,
      p.API_ID,
      p.API_NAME,
      p.API_DESC,
      p.API_ACTIVE_FLAG' .
      ($plan_state ? ', s.PLAN_STATE_NAME ' : ' ') .
    'FROM
      DP_RBAC.ROLE r
    JOIN
      DP_RBAC.ROLE_ACTIVITY ra
    ON
      r.ROLE_ID = ra.ROLE_ID
    JOIN
      DP_RBAC.ACTIVITY a
    ON
      a.ACTIVITY_ID = ra.ACTIVITY_ID
    JOIN
      DP_RBAC.API_ACTIVITY aa
    ON
      a.ACTIVITY_ID = aa.ACTIVITY_ID
    JOIN
      DP_RBAC.API p
    ON
      aa.API_ID = p.API_ID
    JOIN
      DP_RBAC.ACTIVITY_TYPE at
    ON
      a.ACTIVITY_TYPE_ID = at.ACTIVITY_TYPE_ID ' .
    ($plan_state ?
      'LEFT JOIN DP_RBAC.PLAN_STATE_ACTIVITY psa ON a.ACTIVITY_ID = psa.ACTIVITY_ID
      LEFT JOIN DP_RBAC.PLAN_STATE s on psa.PLAN_STATE_ID = s.PLAN_STATE_ID '
      : '') .
    'WHERE ' . implode(' AND ', $where).
    ' ORDER BY a.ACTIVITY_NAME, p.API_NAME ';

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) )
  {
    $api_list = $data;

    // save the most common type of queries to cache
    if (! $plan_state && ! $is_active)
      $redis->set($key, json_encode($data), 60*60*24);
  }

  return $api_list;
}

/**
  * get_role_by_role_name
  *
  * return one row of DP_RBAC.ROLE by DP_RBAC.ROLE.ROLE_NAME
  *
  * @param  string $role_name
  * @return object || NULL
  */
function get_role_by_role_name($role_name)
{
  $sql = \Ultra\Lib\DB\makeSelectQuery('DP_RBAC.ROLE', NULL, NULL, array('ROLE_NAME' => $role_name));
  return $sql ? find_first($sql) : NULL;
}

?>

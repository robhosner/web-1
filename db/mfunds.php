<?php

// mfunds
// 'nuff said

/**
 * make_find_children_query_mfunds
 * @param  integer $v customer id
 * @return string SQL
 */
function make_find_children_query_mfunds($v)
{
  /* note that here and elsewhere mssql_escape will corrupt a
   * non-numeric customer ID because it will pop an escaped string
   * inside quotes, which is *exactly* what we want */
  return sprintf("SELECT mfunds_person_id AS child FROM htt_mfunds_applications
WHERE parent_mfunds_person_id IN (
  SELECT mfunds_person_id
  FROM htt_mfunds_applications
  WHERE customer_id = %d) AND parent_mfunds_person_id IS NOT NULL AND mfunds_person_id IS NOT NULL",
                 $v);
}
/**
 * make_find_status_customer_query_mfunds
 * @param  integer  $v customer id
 * @param  boolean $child
 * @return string SQL
 */
function make_find_status_customer_query_mfunds($v, $child=FALSE)
{
  /* note that here and elsewhere mssql_escape will corrupt a
   * non-numeric customer ID because it will pop an escaped string
   * inside quotes, which is *exactly* what we want */
  return sprintf("SELECT *,
         CASE
          WHEN application_lifecycle = 20 THEN 'Address submitted'
          WHEN application_lifecycle = 30 THEN 'KYC Uploaded'
          WHEN application_lifecycle = 40 THEN 'Pending Application'
          WHEN application_lifecycle = 50 THEN 'Virtual Card Approved'
          WHEN application_lifecycle = 60 THEN 'Physical Card Issued'
          WHEN application_lifecycle = 70 THEN 'Declined'
          WHEN application_lifecycle = 80 THEN 'Inactive'
          ELSE 'Account created'
         END AS lifecycle_label
FROM htt_mfunds_applications WHERE customer_id = %d AND %s",
                 $v,
                 ($child ? 'parent_mfunds_person_id IS NOT NULL' : 'parent_mfunds_person_id IS NULL'));
}

/**
 * make_find_status_customer_nullchild_query_mfunds
 * @param  integer $v customer id
 * @return string SQL
 */
function make_find_status_customer_nullchild_query_mfunds($v)
{
  /* note that here and elsewhere mssql_escape will corrupt a
   * non-numeric customer ID because it will pop an escaped string
   * inside quotes, which is *exactly* what we want */
  return sprintf("SELECT *,
         CASE
          WHEN application_lifecycle = 20 THEN 'Address submitted'
          WHEN application_lifecycle = 30 THEN 'KYC Uploaded'
          WHEN application_lifecycle = 40 THEN 'Pending Application'
          WHEN application_lifecycle = 50 THEN 'Virtual Card Approved'
          WHEN application_lifecycle = 60 THEN 'Physical Card Issued'
          WHEN application_lifecycle = 70 THEN 'Declined'
          WHEN application_lifecycle = 80 THEN 'Inactive'
          ELSE 'Account created'
         END AS lifecycle_label
FROM htt_mfunds_applications
WHERE parent_mfunds_person_id IN (
        SELECT mfunds_person_id
        FROM htt_mfunds_applications
        WHERE customer_id = %d AND mfunds_person_id IS NOT NULL
      )
AND mfunds_person_id IS NULL ORDER BY application_id",
                 $v, $v);
}

/**
 * make_find_status_person_id_query_mfunds
 * @param  integer $v customer id
 * @return string SQL
 */
function make_find_status_person_id_query_mfunds($v)
{
  /* note that here and elsewhere mssql_escape will corrupt a
   * non-numeric customer ID because it will pop an escaped string
   * inside quotes, which is *exactly* what we want */
  return sprintf("SELECT *,
         CASE
          WHEN application_lifecycle = 20 THEN 'Address submitted'
          WHEN application_lifecycle = 30 THEN 'KYC Uploaded'
          WHEN application_lifecycle = 40 THEN 'Pending Application'
          WHEN application_lifecycle = 50 THEN 'Virtual Card Approved'
          WHEN application_lifecycle = 60 THEN 'Physical Card Issued'
          WHEN application_lifecycle = 70 THEN 'Declined'
          WHEN application_lifecycle = 80 THEN 'Inactive'
          ELSE 'Account created'
         END AS lifecycle_label
FROM htt_mfunds_applications WHERE mfunds_person_id = %s",
                 mssql_escape_with_zeroes($v));
}

/**
 * mfunds_reviewable_query
 * @return string SQL
 */
function mfunds_reviewable_query()
{
  return "
SELECT
 mfunds_person_id,
 parent_mfunds_person_id,
 last_four,
 status,
 application_id,
 DATEDIFF(ss, '1970-01-01',  application_date_time) as application_date_time_epoch,
 DATEDIFF(ss, '1970-01-01',  approval_date_time) as approval_date_time_epoch,
 cs_last_activity,
 first_name,
 middle_initial,
 last_name,
 address1,
 address2,
 city,
 state_region,
 postal_code,
 country,
 home_phone,
 work_phone,
 e_mail,
 date_of_birth,
 country_of_birth,
 province_of_birth,
 preferred_language,
 alternate_id_number,
 alternate_id_type,
 social_security,
 decline_cause,
 notes
 FROM htt_mfunds_applications
WHERE application_lifecycle = 40 AND card_type = 'mFunds'";
}

/**
 * htt_mfunds_applications_insert_query
 * @param  string $parent
 * @param  integer $customer_id
 * @return string SQL
 */
function htt_mfunds_applications_insert_query( $parent , $customer_id )
{
  return sprintf("
INSERT INTO htt_mfunds_applications
(
  customer_id, mfunds_person_id, parent_mfunds_person_id, last_four, card_type,
  status, application_lifecycle,
  application_date_time, approval_date_time, cs_last_activity,
  first_name, middle_initial, last_name, address1, address2, city,
  state_region, postal_code, country, home_phone, work_phone, e_mail,
  date_of_birth,
  agent_code, alternate_id_number, alternate_id_type, social_security,
  load_amount, subprogram_id, package_id, image_file_path, decline_cause,
  notes
)
VALUES ( %d,   -- customer ID
         NULL, -- person id
         $parent, -- parent
         NULL, -- last 4
         NULL, -- card type
         NULL, -- status
         NULL, -- no lifecycle status
         GETUTCDATE(), NULL, NULL,

         -- first, middle, last, address1+2, city
         '', NULL, '', '', NULL, '',
         -- state, postal, country, home+work phone, e-mail
         '', '', 'USA', '', '', '',

         '', -- DOB

          -- agent, alt id+type, SSN
         NULL, '', '', '',

         -- amount, subid, package, image, decline cause
         0, NULL, NULL, NULL, NULL,

         ''    -- notes
); ",
  $customer_id);
}

?>

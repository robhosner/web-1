<?php

/*

php component to access DB table ULTRA.CUSTOMER_OPTIONS

IMPORTANT: attributes should be added to ULTRA.CUSTOMER_OPTION_ATTRIBUTES due to referential constraint

See http://wiki.hometowntelecom.com:8090/display/SPEC/Customer+Options+Modifier

*/

define('CUSTOMER_OPTIONS_REDIS_KEY', 'ultra/api/customer_options/cache/');

require_once 'classes/CustomerOptions.php';

/**
 * get_customers_promotional_sim_count_by_status
 * 
 * Returns the count of customers associated with the promo ID $ultra_promotional_plans_id
 * $status_clause is the SQL clause applied to HTT_CUSTOMERS_OVERLAY_ULTRA.PLAN_STATE
 *
 * @param  integer $ultra_promotional_plans_id
 * @param  string $status_clause
 * @return array (ready_customers_promotional_sim_count, error)
 */
function get_customers_promotional_sim_count_by_status( $ultra_promotional_plans_id , $status_clause )
{
  $ready_customers_promotional_sim_count = '';
  $error                                 = '';

  $sql = sprintf("
    SELECT COUNT(*)
    FROM   ULTRA.CUSTOMER_OPTIONS o
    JOIN   HTT_CUSTOMERS_OVERLAY_ULTRA u
    ON     o.CUSTOMER_ID      = u.CUSTOMER_ID
    WHERE  o.OPTION_ATTRIBUTE = 'PROMO_ASSIGNED'
    AND    o.OPTION_VALUE     = %d
    AND    u.PLAN_STATE       $status_clause
  ",$ultra_promotional_plans_id);

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result))
  {
    $ready_customers_promotional_sim_count = $query_result[0][0];
  }
  else
  {
    $error = "DB error: could not query ULTRA.CUSTOMER_OPTIONS";
  }

  return array( $ready_customers_promotional_sim_count , $error );
}

/**
 * get_active_customers_promotional_sim_count
 * 
 * Returns the count of Active customers associated with the promo ID $ultra_promotional_plans_id
 *
 * @param  integer $ultra_promotional_plans_id
 * @return array (ready_customers_promotional_sim_count, error)
 */
function get_active_customers_promotional_sim_count( $ultra_promotional_plans_id )
{
  return get_customers_promotional_sim_count_by_status( $ultra_promotional_plans_id , " = 'Active' " );
}

/**
 * get_ready_customers_promotional_sim_count
 * 
 * Returns the count of non-Neutral customers associated with the promo ID $ultra_promotional_plans_id
 *
 * @param  integer $ultra_promotional_plans_id
 * @return array (ready_customers_promotional_sim_count, error)
 */
function get_ready_customers_promotional_sim_count( $ultra_promotional_plans_id )
{
  return get_customers_promotional_sim_count_by_status( $ultra_promotional_plans_id , " != 'Neutral' " );
}

/**
 * remove_customer_options
 *
 * @param  integer $customer_id
 * @param  array $attributes OPTION_ATTRIBUTE to remove
 * @return object of class \Result
 */
function remove_customer_options( $customer_id , $attributes )
{
  dlog('', "customer_id = $customer_id , attributes = %s",$attributes);

  $underscoreObject = new \__ ;

  $list = $underscoreObject->map( $attributes , function($attribute) { return mssql_escape_with_zeroes($attribute); } );

  $list = implode(',',$list);

  $sql = sprintf(
    " DELETE FROM ULTRA.CUSTOMER_OPTIONS
      WHERE  CUSTOMER_ID = %d
      AND    OPTION_ATTRIBUTE IN ( $list ) ",
    $customer_id
  );

  $result = run_sql_and_check_result($sql);

  if ($result->is_success())
    \CustomerOptions::delCache($customer_id);

  return $result;
}

/**
 * update_customer_options
 *
 * @param  integer $customer_id
 * @param  array kva array $attribute => $value
 * @return object of class \Result
 */
function update_customer_options( $customer_id , $pairs )
{
  foreach( $pairs as $attribute => $value )
  {
    $result = update_customer_option( $customer_id , $attribute , $value );

    if ( $result->is_failure() )
      return make_error_Result('DB error');
  }

  return make_ok_Result();
}

/**
 * update_customer_option
 *
 * @param  integer $customer_id
 * @param  string $attribute
 * @param  string $value
 * @return object of class \Result
 */
function update_customer_option( $customer_id , $attribute , $value )
{
  dlog('', "customer_id = $customer_id , attribute = $attribute , value = $value");

  $sql = sprintf(
    " UPDATE ULTRA.CUSTOMER_OPTIONS
      SET
      OPTION_VALUE       = %s,
      LAST_MOD_TIMESTAMP = GETUTCDATE()
      WHERE
      OPTION_ATTRIBUTE = %s
      AND
      CUSTOMER_ID      = %d ",
    mssql_escape_with_zeroes( $value ),
    mssql_escape_with_zeroes( $attribute ),
    $customer_id
  );

  $result = run_sql_and_check_result($sql);

  if ($result->is_success())
    \CustomerOptions::delCache($customer_id);

  return $result;
}

/**
 * ultra_customer_options_insert
 *
 * Add a new row into ULTRA.CUSTOMER_OPTIONS
 *
 * @param  string $attribute
 * @param  string $value
 * @param  integer $customer_id
 * @return boolean
 */
function ultra_customer_options_insert( $attribute , $value , $customer_id ) 
{
  dlog('', "customer_id = $customer_id , attribute = $attribute , value = $value");

  $sql = sprintf(
    " INSERT INTO ULTRA.CUSTOMER_OPTIONS
    (
      CUSTOMER_ID,
      OPTION_ATTRIBUTE,
      OPTION_VALUE
    ) VALUES (
      %d,
      %s,
      %s
    )",
    $customer_id,
    mssql_escape_with_zeroes( $attribute ),
    mssql_escape_with_zeroes( $value )
  );

  $check = ! ! run_sql_and_check( $sql );

  if ($check)
    \CustomerOptions::delCache($customer_id);

  return $check;
}

/**
 * compute_bolt_ons_configuration
 *
 * @param  array $current_configuration
 * @param  array $new_configuration
 * @return array
 */
function compute_bolt_ons_configuration( $current_configuration , $new_configuration )
{
  $to_add    = array();
  $to_remove = array();
  $to_update = array();

  foreach( $new_configuration as $new_id => $new_value )
  {
    if ( isset( $current_configuration[ $new_id ] ) )
    {
      // we do already have this ID

      if ( $current_configuration[ $new_id ] != $new_configuration[ $new_id ] )
        $to_update[ $new_id ] = $new_value;
    }
    else
      // we don't have this ID
      $to_add[ $new_id ] = $new_value;
  }

  foreach( $current_configuration as $old_id => $old_value )
    if ( ! isset( $new_configuration[ $old_id ] ) )
      // we don't want this ID anymore
      $to_remove[ $old_id ] = $old_value;

  dlog('',"to_add    = %s",$to_add);
  dlog('',"to_remove = %s",$to_remove);
  dlog('',"to_update = %s",$to_update);

  return array( $to_add , $to_remove , $to_update );
}

/**
 * update_bolt_ons_values_to_customer_options
 *
 * Adjust Recurring Bolt On configurations for the given customer
 *
 * @param  integer $customer_id
 * @param  array $current_configuration
 * @param  array $new_configuration
 * @param  string $source
 * @param  string $transaction_id
 * @param  string $htt_plan_tracker_id
 * @return object of class \Result
 */
function update_bolt_ons_values_to_customer_options( $customer_id , $current_configuration , $new_configuration , $source , $transaction_id=NULL , $htt_plan_tracker_id=NULL )
{
  list( $to_add , $to_remove , $to_update ) = compute_bolt_ons_configuration( $current_configuration , $new_configuration );

  if ( ! start_mssql_transaction() )
    return make_error_Result('DB error');

  if ( count( $to_add ) )
  {
    $result = add_bolt_ons_to_customer_options(
      $customer_id,
      $to_add
    );

    if ( $result->is_failure() )
    {
      rollback_mssql_transaction();
      return make_error_Result('DB error');
    }

/*
    foreach( $to_add as $attribute => $value )
    {
      dlog('',"attribute = $attribute ; value = $value");

      $info = \Ultra\UltraConfig\getBoltOnInfoByOption( $attribute , $value );

      dlog('',"attribute = $attribute ; value = $value ; info = %s",$info);

      $result = trackBoltOn( $customer_id , $source , 'ADDED' , $info['sku'] , $info['product'] , 'MONTHLY' , $transaction_id , $htt_plan_tracker_id );

      if ( $result->is_failure() )
      {
        rollback_mssql_transaction();
        return make_error_Result('DB error');
      }
    }
*/
  }

  if ( count( $to_remove ) )
  {
    $result = remove_customer_options(
      $customer_id,
      array_keys( $to_remove )
    );

    if ( $result->is_failure() )
    {
      rollback_mssql_transaction();
      return make_error_Result('DB error');
    }

/*
    foreach( $to_remove as $attribute => $value )
    {
      dlog('',"attribute = $attribute ; value = $value");

      $info = \Ultra\UltraConfig\getBoltOnInfoByOption( $attribute , $value );

      dlog('',"attribute = $attribute ; value = $value ; info = %s",$info);

      $result = trackBoltOn( $customer_id , $source , 'REMOVED' , $info['sku'] , $info['product'] , 'MONTHLY' , $transaction_id , $htt_plan_tracker_id );

      if ( $result->is_failure() )
      {
        rollback_mssql_transaction();
        return make_error_Result('DB error');
      }
    }
*/
  }

  if ( count( $to_update ) )
  {
    $result = update_customer_options(
      $customer_id,
      $to_update
    );

    if ( $result->is_failure() )
    {
      rollback_mssql_transaction();
      return make_error_Result('DB error');
    }

/*
    foreach( $to_update as $attribute => $value )
    {
      dlog('',"attribute = $attribute ; value = $value");

      $info = \Ultra\UltraConfig\getBoltOnInfoByOption( $attribute , $value );

      dlog('',"attribute = $attribute ; value = $value ; info = %s",$info);

      $result = trackBoltOn( $customer_id , $source , 'UPDATED' , $info['sku'] , $info['product'] , 'MONTHLY' , $transaction_id , $htt_plan_tracker_id );

      if ( $result->is_failure() )
      {
        rollback_mssql_transaction();
        return make_error_Result('DB error');
      }
    }
*/
  }

  if ( ! commit_mssql_transaction() )
    return make_error_Result('DB error');

  return make_ok_Result();
}

/**
 * get_bolt_ons_from_customer_options
 *
 * Get all Bolt Ons assigned to a customer, but not their value
 *
 * @param  integer $customer_id
 * @return array
 */
function get_bolt_ons_from_customer_options( $customer_id )
{
  $attributes = \Ultra\UltraConfig\getBoltOnsOptionAttributes();

  $ultra_customer_options = get_ultra_customer_options_by_customer_id( $customer_id );

  $underscoreObject = new \__ ;

  return $underscoreObject->intersection( $attributes , $ultra_customer_options );
}

/**
 * get_bolt_ons_info_from_customer_options
 *
 * Get all Bolt Ons assigned to a customer, including all related info
 *
 * @param  integer $customer_id
 * @return array
 */
function get_bolt_ons_info_from_customer_options( $customer_id )
{
  $ultra_customer_options = get_ultra_customer_options_by_customer_id( $customer_id );

  $customer = get_customer_from_customer_id($customer_id, ['u.BRAND_ID']);
  $brand    = \Ultra\UltraConfig\getShortNameFromBrandId($customer->BRAND_ID);

  $products = [
    'MINT'      => ['MDATA'],
    'ULTRA'     => ['DATA','IDDCA','VOICE','GLOBE','SHAREDILD','SHAREDDATA'],
    'UNIVISION' => ['DATA','IDDCA','VOICE']
  ];

  dlog('',"ultra_customer_options = %s", $ultra_customer_options);

  $bolt_ons = array();

  $bolt_ons_definitions = \Ultra\UltraConfig\getBoltOnsMapping();

  foreach( $bolt_ons_definitions as $id => $data )
    for ( $i = 0 ; $i < count($ultra_customer_options) ; $i = $i + 2 )
    {
      $attribute = $ultra_customer_options[ $i ] ;
      $value     = $ultra_customer_options[ $i+1 ] ;

      if (
        $attribute == $data['option_attribute']
        && $value  == $data['cost']
        && in_array($data['product'], $products[$brand])
      )
      {
        $bolt_on = $data;
        $bolt_on['id'] = $id;
        $bolt_ons[] = $bolt_on;
      }
    }

  return $bolt_ons;
}

/**
 * get_bolt_ons_values_from_customer_options
 *
 * Get all Bolt Ons assigned to a customer, including their value
 *
 * @param  integer $customer_id
 * @return array
 */
function get_bolt_ons_values_from_customer_options( $customer_id )
{
  $attributes = \Ultra\UltraConfig\getBoltOnsOptionAttributes();

  $ultra_customer_options = get_ultra_customer_options_by_customer_id( $customer_id );

  $bolt_ons = array();

  for ( $i = 0 ; $i < count($ultra_customer_options) ; $i++ )
    if ( in_array( $ultra_customer_options[$i] , $attributes ) )
      $bolt_ons[ $ultra_customer_options[$i] ] = $ultra_customer_options[$i+1] ;

  return $bolt_ons;
}

/**
 * add_bolt_ons_to_customer_options
 *
 * Add the given $bolt_ons to the table ULTRA.CUSTOMER_OPTIONS
 *
 * @param  integer $customer_id
 * @param  array $bolt_ons kva
 * @return object of class \Result
 */
function add_bolt_ons_to_customer_options( $customer_id , $bolt_ons )
{
  dlog('', "customer_id = $customer_id , bolt_ons = %s",$bolt_ons);

  if ( ! $bolt_ons || ! is_array( $bolt_ons ) )
    return make_error_Result('Invalid bolt_ons argument');

  if ( ! count( $bolt_ons ) )
    return make_ok_Result( array() , NULL , 'Nothing to do' );

  $success = TRUE;

  while ( list( $attribute , $value ) = each($bolt_ons) )
    if ( $success )
      $success = ultra_customer_options_insert( $attribute , $value , $customer_id );

  if ( $success )
    return make_ok_Result();

  return make_error_Result( 'DB error - we could not insert data into ULTRA.CUSTOMER_OPTIONS' );
}

/**
 * get_ultra_customer_options_by_customer_id_assoc
 *
 * Returns a list as assoc array (OPTION_ATTRIBUTE => OPTION_VALUE)
 *
 * @param  integer $customer_id
 * @return array $ultra_customer_options
 */
function get_ultra_customer_options_by_customer_id_assoc($customer_id)
{
  $options = array();

  \CustomerOptions::delCache($customer_id);

  $sql = 'SELECT OPTION_ATTRIBUTE, OPTION_VALUE FROM ULTRA.CUSTOMER_OPTIONS with (nolock) WHERE CUSTOMER_ID = %d';
  $res = mssql_fetch_all_objects(logged_mssql_query(sprintf($sql, $customer_id)));

  if ($res && is_array($res) && count($res))
  {
    foreach ($res as $option)
      $options[$option->OPTION_ATTRIBUTE] = $option->OPTION_VALUE;
  }

  return $options;
}

function set_marketing_options_by_array($customer_id, $options, $toRemove)
{
  remove_customer_options_fuzzy($customer_id, $toRemove);
  set_ultra_customer_options_by_array($customer_id, $options);
}

function remove_customer_options_fuzzy($customer_id, $toRemove)
{
  foreach ($toRemove as $attribute)
  {
    $sql = "DELETE FROM ULTRA.CUSTOMER_OPTIONS 
            WHERE OPTION_ATTRIBUTE LIKE '%$attribute'
            AND CUSTOMER_ID = $customer_id";

    logged_mssql_query($sql);
  }
}

function set_ultra_customer_options_by_array($customer_id, $options)
{
  try
  {
    $currOpt  = get_ultra_customer_options_by_customer_id_assoc($customer_id);
    $currAttr = [];
    foreach ($currOpt as $attr => $val)
      $currAttr[] = $attr;

    $inserts = [];

    foreach ($options as $attr => $val)
    {
      if (in_array($attr, $currAttr))
        update_customer_options($customer_id, [$attr => $val]);
      else
        $inserts[] = "($customer_id, '$attr', '$val')";
    }

    if ($inserts)
    {
      $sqlInsert  = ' INSERT INTO ULTRA.CUSTOMER_OPTIONS ';
      $sqlInsert .= ' (CUSTOMER_ID, OPTION_ATTRIBUTE, OPTION_VALUE) VALUES ';
      $sqlInsert .= implode(',', $inserts);
      logged_mssql_query($sqlInsert);
    }

    return true;
  }
  catch (\Exception $e)
  {
    return false;
  }

  // return logged_mssql_query($sqla . $sqlb . $sqlc);
}

/**
 * get_ultra_customer_options_by_customer_id
 *
 * Returns a flatten list of couples (OPTION_ATTRIBUTE, OPTION_VALUE)
 *
 * @param  integer $customer_id
 * @param  string $option_attribute
 * @return array $ultra_customer_options
 */
function get_ultra_customer_options_by_customer_id( $customer_id , $option_attribute=NULL )
{
  $ultra_customer_options = array();

  $sql = sprintf(
    " SELECT
        OPTION_ATTRIBUTE,
        OPTION_VALUE
      FROM
        ULTRA.CUSTOMER_OPTIONS WITH (NOLOCK)
      WHERE
        CUSTOMER_ID = %d",
    $customer_id);

  if ( $option_attribute )
    $sql .= sprintf( " AND OPTION_ATTRIBUTE = %s " , mssql_escape_with_zeroes($option_attribute) );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
  {
    $underscoreObject = new \__ ;

    $ultra_customer_options = $underscoreObject->flatten( $query_result );
  }

  return $ultra_customer_options;
}

/**
 * delay_provisioned_customer_cancellation
 *
 * Returns TRUE if
 * - the customer is in Provisioned State
 * - ( plan_expires + BILLING.CANCEL_DELAY_PROVISION days ) is in the future
 *
 * @param  array $customer
 * @param  string $plan_state
 * @return boolean
 */
function delay_provisioned_customer_cancellation($customer,$plan_state)
{
  $delay = FALSE;

  if ( $plan_state == 'Provisioned' )
  {
    $ultra_customer_options = get_ultra_customer_options_by_customer_id( $customer->CUSTOMER_ID , 'BILLING.CANCEL_DELAY_PROVISION' );

    if ( $ultra_customer_options && is_array($ultra_customer_options) && count($ultra_customer_options) )
    {
      dlog('',"%s",$ultra_customer_options);

      // $ultra_customer_options[1] contains how many days we should we delay cancellation for customers in Provisioned State

      if ( $ultra_customer_options[1] && ( ( $ultra_customer_options[1] + $customer->days_before_expire ) > 0 ) )
        $delay = TRUE;
    }
  }

  dlog('',"returning ".( ( $delay ) ? 'TRUE' : 'FALSE' ) );

  return $delay;
}

/**
 * set_brand_uv_subplan
 *
 * @param  integer $customer_id
 * @param  string  $sub_plan ( A or B )
 * @param  integer $number_of_months_since_import
 * @return object of class \Result
 */
function set_brand_uv_subplan( $customer_id , $sub_plan , $number_of_months_since_import )
{
  if ( ! $sub_plan)
    return make_error_Result('sub_plan cannot be null');

  if ( $number_of_months_since_import === null)
    return make_error_Result('number_of_months_since_import cannot be null');

  $success = ultra_customer_options_insert( 'BRAND.UV.SUBPLAN.'.strtoupper( $sub_plan ) , $number_of_months_since_import , $customer_id );

  return $success ? make_ok_Result() : make_error_Result('DB error') ;
}

/**
 * update_brand_uv_subplan
 *
 * @param  integer $customer_id
 * @param  string  $sub_plan ( A or B )
 * @param  integer $number_of_months_since_import
 * @return object of class \Result
 */
function update_brand_uv_subplan( $customer_id , $sub_plan , $number_of_months_since_import )
{
  if ( ! $sub_plan)
    return make_error_Result('sub_plan cannot be null');

  if ( $number_of_months_since_import === null)
    return make_error_Result('number_of_months_since_import cannot be null');

  $success = update_customer_option($customer_id, 'BRAND.UV.SUBPLAN.'.strtoupper( $sub_plan ), $number_of_months_since_import);

  return $success ? make_ok_Result() : make_error_Result('DB error') ;
}

/**
 * get_brand_uv_subplan
 *
 * returns letter of UV subplan or NULL if none
 *
 * @param  integer $customer_id
 * @return char || NULL subplan type ex. B
 */
function get_brand_uv_subplan($customer_id)
{
  $sql    = "SELECT TOP 1 * FROM ULTRA.CUSTOMER_OPTIONS with (nolock)
    WHERE OPTION_ATTRIBUTE LIKE 'BRAND.UV.SUBPLAN%'
    AND CUSTOMER_ID = $customer_id";
  $option = find_first($sql);

  return ($option) ? substr($option->OPTION_ATTRIBUTE, -1) : NULL;
}

/**
 * enroll_migration_extension_plan
 *
 * Enable MEP
 *
 * @param  integer $customer_id
 * @param  string  $mep_start_date ( MEP start date )
 * @return object of class \Result
 */
function enroll_migration_extension_plan( $customer_id , $mep_start_date )
{
  $success = ultra_customer_options_insert( MIGRATION_EXTENSION_PLAN_START_DATE , $mep_start_date , $customer_id );

  return $success ? make_ok_Result() : make_error_Result('DB error') ;
}

/**
 * enroll_mrc_house_account
 *
 * Enable house account
 *
 * @param  integer $customer_id
 * @param  string  $value ( usually the customer name )
 * @return object of class \Result
 */
function enroll_mrc_house_account( $customer_id , $value )
{
  $success = ultra_customer_options_insert( MRC_HOUSE_ACCOUNT , $value , $customer_id );

  return $success ? make_ok_Result() : make_error_Result('DB error') ;
}

/**
 * enroll_billing_mrc_promo_plan
 *
 * Enrolls a customer to a Promo Plan
 *
 * @param  integer $customer_id
 * @param  string $plan
 * @param  integer $ultra_customer_options
 * @param  integer $months
 * @return object of class \Result
 */
function enroll_billing_mrc_promo_plan( $customer_id , $plan , $ultra_promotional_plans_id , $months=12 )
{
  $value = implode(',',array( $plan , $ultra_promotional_plans_id , $months ));

  $success = ultra_customer_options_insert( BILLING_OPTION_PROMO_PLAN , $value , $customer_id );

  return $success ? make_ok_Result() : make_error_Result('DB error') ;
}

/**
 * billing_mrc_promo_plan_info
 *
 * Retrieves customer's info relative to a Promo Plan
 *
 * @param  integer $customer_id
 * @return array or NULL
 */
function billing_mrc_promo_plan_info( $customer_id )
{
  $ultra_customer_options = get_ultra_customer_options_by_customer_id( $customer_id , 'BILLING.MRC_PROMO_PLAN' );

  return format_billing_mrc_promo_plan_info( $ultra_customer_options );
}

/**
 * format_billing_mrc_promo_plan_info
 *
 * @param  array $ultra_customer_options
 * @return array
 */
function format_billing_mrc_promo_plan_info( $ultra_customer_options )
{
  if ( ! $ultra_customer_options || ! is_array($ultra_customer_options) || ! count($ultra_customer_options) )
    return NULL;

  $values = explode(',',$ultra_customer_options[1]);

  return array(
    'plan'                       => $values[0],
    'ultra_promotional_plans_id' => $values[1],
    'months'                     => $values[2]
  );
}

/**
 * decrement_billing_mrc_promo_plan
 *
 * Decrement the promo plan value by one month
 *
 * @param  integer $customer_id
 * @return object of class \Result
 */
function decrement_billing_mrc_promo_plan( $customer_id )
{
  $ultra_customer_options = billing_mrc_promo_plan_info( $customer_id );

  if ( ! $ultra_customer_options || ! is_array($ultra_customer_options) )
    return make_error_Result('Customer Id '.$customer_id.' is not associated with a mrc_promo_plan');

  if ( empty( $ultra_customer_options['plan'] ) )
    return make_error_Result('plan is missing');

  if ( empty( $ultra_customer_options['ultra_promotional_plans_id'] ) )
    return make_error_Result('ultra_promotional_plans_id is missing');

  if ( is_null( $ultra_customer_options['months'] ) || ( $ultra_customer_options['months'] == '' ) )
    return make_error_Result('months is missing');

  if ( $ultra_customer_options['months'] == 0 )
    return make_error_Result('months extinguished');

  $value = implode(',',array( $ultra_customer_options['plan'] , $ultra_customer_options['ultra_promotional_plans_id'] , $ultra_customer_options['months'] - 1 ));

  return update_customer_option( $customer_id , 'BILLING.MRC_PROMO_PLAN' , $value );
}

/**
 * opt_out_voice_preference
 *
 * Customer opts out from voice promotions
 *
 * @param  integer $customer_id
 * @return object of class \Result
 */
function opt_out_voice_preference($customer_id)
{
  $sql = sprintf(
    "IF NOT EXISTS ( SELECT CUSTOMER_ID FROM ULTRA.CUSTOMER_OPTIONS WHERE CUSTOMER_ID = %d AND OPTION_ATTRIBUTE = 'PREFERENCES.OPT_OUT.VOICE')
    
    BEGIN
    INSERT INTO ULTRA.CUSTOMER_OPTIONS
    (
      CUSTOMER_ID,
      OPTION_ATTRIBUTE,
      OPTION_VALUE,
      LAST_MOD_TIMESTAMP,
      LAST_MOD_USERNAME
    )
    VALUES (%d, 'PREFERENCES.OPT_OUT.VOICE', '1', GETUTCDATE(), %s)
    END",
    $customer_id,
    $customer_id,
    "'PHP'"
  );

  $result = run_sql_and_check_result($sql);

  if ($result->is_success())
    \CustomerOptions::delCache($customer_id);

  return $result;
}

/**
 * opt_in_sms_preference
 *
 * Customer opts in from voice promotions
 *
 * @param  integer $customer_id
 * @return object of class \Result
 */
function opt_in_voice_preference($customer_id)
{
  return remove_customer_options($customer_id, array("PREFERENCES.OPT_OUT.VOICE"));
}

/**
 * get_voice_preference
 *
 * Returns TRUE if Customer did not opt out from voice promotions
 *
 * @param  integer $customer_id
 * @return boolean
 */
function get_voice_preference($customer_id)
{
  $ultra_customer_options = get_ultra_customer_options_by_customer_id( $customer_id );

  dlog('',"ultra_customer_options = %s",$ultra_customer_options);

  return ! ! in_array( 'PREFERENCES.OPT_OUT.VOICE' , $ultra_customer_options );
}

/**
 * enroll_bogo_month
 * add BOGO promotion option for $customer_id with $amount as VALUE
 *
 * @param  integer $customer_id
 * @param  integer $plan_cost
 * @param  string  $option
 * @return boolean
 */
function enroll_bogo_month($customer_id, $plan_cost, $option)
{
  return ultra_customer_options_insert($option, $plan_cost, $customer_id);
}

/**
 * redeemed_bogo_month
 * remove BILLING.MRC_BOGO_MONTH option for $customer_id
 *
 * @param  integer $customer_id
 * @return Result object
 */
function redeemed_bogo_month($customer_id)
{
  return remove_customer_options($customer_id, \Ultra\UltraConfig\getValidBOGOTypes());
}

/**
 * bogo_month_info
 * return if the $customer_id has the BILLING.MRC_BOGO_MONTH option
 *
 * @param  integner $customer_id
 * @return array bogo info
 */
function bogo_month_info($customer_id)
{
  $ultra_customer_options = get_ultra_customer_options_by_customer_id($customer_id);

  dlog('',"ultra_customer_options = %s", $ultra_customer_options);

  $index = false;
  $valid_options = \Ultra\UltraConfig\getValidBOGOTypes();
  for ($i = 0; $i < count($ultra_customer_options); $i++)
  {
    if (in_array($ultra_customer_options[$i], $valid_options))
      $index = $i;
  }

  if ($index !== false)
  {
    return array(
      $ultra_customer_options[$index],
      $ultra_customer_options[$index + 1]
    );
  }
  else
    return array();
}

/**
 * bogo_second_month_expiring_customer_ids
 *
 * gets all bogo customer ids in the second month
 */
function bogo_second_month_expiring_customer_ids($inDays = 2)
{
  $valid_options = \Ultra\UltraConfig\getValidBOGOTypes();

  $sql = 'SELECT co.CUSTOMER_ID FROM ULTRA.CUSTOMER_OPTIONS co WITH (NOLOCK)
  INNER JOIN HTT_CUSTOMERS_OVERLAY_ULTRA hcou WITH (NOLOCK)
    ON hcou.CUSTOMER_ID = co.CUSTOMER_ID
    AND convert(varchar, hcou.plan_expires, 105) = \'%s\'
  WHERE co.OPTION_ATTRIBUTE IN (';

  for ($i = 0; $i < count($valid_options); $i++)
  {
    if ($i) $sql .= ',';
    $sql .= "'{$valid_options[$i]}'";
  }

  $sql = sprintf($sql . ')', date("d-m-20y", mktime(0, 0, 0, date("m"), date("d") + $inDays, date("Y"))));

  $result = mssql_fetch_all_objects(logged_mssql_query($sql));

  $output = array();
  foreach ($result as $row)
    $output[] = $row->CUSTOMER_ID;

  return $output;
}

/**
 * enroll_promo_7_11
 * add BILLING.MRC_7_11 option for $customer_id with $amount as VALUE
 *
 * @param  integer $customer_id
 * @param  integer $amount in dollars
 * @return boolean
 */
function enroll_promo_7_11($customer_id, $amount)
{
  return ultra_customer_options_insert(BILLING_OPTION_ATTRIBUTE_7_11, $amount, $customer_id);
}

/**
 * redeemed_promo_7_11
 * remove BILLING.MRC_7_11 option for $customer_id
 *
 * @param  integer $customer_id
 * @return Result object
 */
function redeemed_promo_7_11($customer_id)
{
  return remove_customer_options($customer_id, array(BILLING_OPTION_ATTRIBUTE_7_11));
}

/**
 * promo_7_11_info
 * return if the $customer_id has the BILLING.MRC_7_11 option
 *
 * @param  integer $customer_id
 * @return boolean
 */
function promo_7_11_info($customer_id)
{
  $ultra_customer_options = get_ultra_customer_options_by_customer_id($customer_id);

  dlog('',"ultra_customer_options = %s", $ultra_customer_options);

  return ! ! in_array(BILLING_OPTION_ATTRIBUTE_7_11 , $ultra_customer_options);
}

/**
 * enroll_multi_month
 * add MULTI_MONTH customer option
 *
 * @param  Integer $customer_id
 * @param  Integer $months
 * @return Boolean
 */
function enroll_multi_month($customer_id, $ultra_plan, $months)
{
  return ultra_customer_options_insert(BILLING_OPTION_MULTI_MONTH, "$ultra_plan,$months," . ($months - 1), $customer_id);
}

/**
 * deduct_multi_month
 * deduct one month from MULTI_MONTH customer option
 *
 * @param  Integer $customer_id
 * @param  Integer $months
 * @return Boolean
 */
function deduct_multi_month($customer_id)
{
  $values = multi_month_info($customer_id);

  return update_customer_options($customer_id,
    array( BILLING_OPTION_MULTI_MONTH => $values['ultra_plan'] . ',' . $values['months_total'] . ',' . ($values['months_left'] - 1) )
  );
}

/**
 * remove_multi_month
 * remove MULTI_MONTH customer option
 *
 * @param  Integer $customer_id
 * @return Boolean
 */
function remove_multi_month($customer_id)
{
  return (remove_customer_options($customer_id, array(BILLING_OPTION_MULTI_MONTH))) ? make_ok_Result() : make_error_Result();
}

/**
 * multi_month_info
 * @param Integer customer ID
 * @param Array all current customer options as retrieved from get_ultra_customer_options_by_customer_id($customer_id)
 * @return Array ((string) ultra_plan, (integer) months_total, (integer) months_left) or FALSE
 */
function multi_month_info($customer_id, $all_options = NULL)
{
  if (is_array($all_options))
  {
    $index = array_search(BILLING_OPTION_MULTI_MONTH, $all_options);
    $option = $index === FALSE ? array() : array(BILLING_OPTION_MULTI_MONTH, $all_options[$index + 1]);
  }
  else
    $option = get_ultra_customer_options_by_customer_id($customer_id, BILLING_OPTION_MULTI_MONTH);

  if (count($option) !== 2)
    return FALSE;

  $values = explode(',', $option[1]);
  return array('ultra_plan' => $values[0], 'months_total' => $values[1], 'months_left' => $values[2]);
}

/**
 * set_acc_address_e911
 * add ACC.ADDRESS_E911 option for $customer_id
 *
 * @param  integer $customer_id
 * @param  varchar $address
 * @return boolean
 */
function set_acc_address_e911($customer_id, $address)
{
  if (get_acc_address_e911($customer_id))
  {
    $result = update_customer_options($customer_id, array( ACC_OPTION_ADDRESS_E911 => $address));
    return ! ! $result->is_success();
  }
  else
    return ultra_customer_options_insert(ACC_OPTION_ADDRESS_E911, $address, $customer_id);
}

/**
 * remove_acc_address_e911
 * remove ACC.ADDRESS_E911 option for $customer_id
 *
 * @param  integer $customer_id
 * @return Result object
 */
function remove_acc_address_e911($customer_id)
{
  return remove_customer_options($customer_id, array(ACC_OPTION_ADDRESS_E911));
}

/**
 * get_acc_address_e911
 * return ACC.ADDRESS_E911 values for $customer_id
 *
 * @param  integner $customer_id
 * @return array ACC.ADDRESS_E911 info
 */
function get_acc_address_e911($customer_id)
{
  $ultra_customer_options = get_ultra_customer_options_by_customer_id($customer_id);

  dlog('',"ultra_customer_options = %s", $ultra_customer_options);

  $index = array_search(ACC_OPTION_ADDRESS_E911, $ultra_customer_options);
  return ($index !== false) ? $ultra_customer_options[$index + 1] : NULL;
}

function customer_options_get_throttle_speed($customer_id)
{
  $sql = "SELECT TOP 1 OPTION_ATTRIBUTE, OPTION_VALUE FROM ULTRA.CUSTOMER_OPTIONS 
          WHERE CUSTOMER_ID = %d AND OPTION_ATTRIBUTE LIKE 'LTE_SELECT%%'";
  return find_first(sprintf($sql, $customer_id));
}

function customer_options_update_throttle_speed($customer_id, $new_attribute, $new_value )
{
  $sql = sprintf("UPDATE ULTRA.CUSTOMER_OPTIONS SET 
                    OPTION_ATTRIBUTE   = '%s', OPTION_VALUE = '%s',
                    LAST_MOD_TIMESTAMP = GETUTCDATE()
                  WHERE OPTION_ATTRIBUTE LIKE 'LTE_SELECT%%' AND CUSTOMER_ID = %d",
    $new_attribute,
    $new_value,
    $customer_id
  );

  $check = run_sql_and_check($sql);

  if ($check)
    \CustomerOptions::delCache($customer_id);

  return $check;
}

function customer_options_insert_throttle_speed($customer_id, $option_attribute, $option_value)
{
  $sql = sprintf("INSERT INTO ULTRA.CUSTOMER_OPTIONS (CUSTOMER_ID, OPTION_ATTRIBUTE, OPTION_VALUE)
                  VALUES (%d, '%s', '%s')",
    $customer_id,
    $option_attribute,
    $option_value
  );

  $check = run_sql_and_check($sql);

  if ($check)
    \CustomerOptions::delCache($customer_id);

  return $check;
}

function set_wifi_calling_flag($customer_id, $value)
{
  if (get_wifi_calling_flag($customer_id)) {
    return (bool) run_sql_and_check(
      sprintf(
        "UPDATE ULTRA.CUSTOMER_OPTIONS SET
          OPTION_VALUE       = %d,
          LAST_MOD_TIMESTAMP = GETUTCDATE()
        WHERE OPTION_ATTRIBUTE = '%s' AND CUSTOMER_ID = %d",
        $value,
        ACC_WIFI_CALLING_ENABLED,
        $customer_id
      )
    );
  } else {
    return (bool) run_sql_and_check(
      sprintf(
        "INSERT INTO ULTRA.CUSTOMER_OPTIONS (CUSTOMER_ID, OPTION_ATTRIBUTE, OPTION_VALUE) VALUES (%d, '%s', %d)",
        $customer_id,
        ACC_WIFI_CALLING_ENABLED,
        $value
      )
    );
  }
}

function get_wifi_calling_flag($customer_id)
{
  $sql = "SELECT OPTION_VALUE FROM ULTRA.CUSTOMER_OPTIONS 
          WHERE CUSTOMER_ID = %d AND OPTION_ATTRIBUTE = '" . ACC_WIFI_CALLING_ENABLED . "'";
  return find_first(sprintf($sql, $customer_id));
}

function remove_wifi_calling_flag($customer_id)
{
  return remove_customer_options($customer_id, [ACC_WIFI_CALLING_ENABLED]);
}

function set_wifi_calling_channel_flag($customer_id, $attribute, $isAdd=true)
{
  $sql = sprintf(
    "IF EXISTS (SELECT CUSTOMER_ID FROM ULTRA.CUSTOMER_OPTIONS WHERE CUSTOMER_ID = %d AND OPTION_ATTRIBUTE LIKE 'WIFI_CALLING.%%')
      UPDATE ULTRA.CUSTOMER_OPTIONS SET 
        OPTION_ATTRIBUTE   = %s,
        OPTION_VALUE       = '%s',
        LAST_MOD_TIMESTAMP = GETUTCDATE()
      WHERE CUSTOMER_ID = %d AND OPTION_ATTRIBUTE LIKE 'WIFI_CALLING.%%'
    ELSE
      INSERT INTO ULTRA.CUSTOMER_OPTIONS (CUSTOMER_ID, OPTION_ATTRIBUTE, OPTION_VALUE) VALUES (%d, %s, '%s')",
    $customer_id,
    mssql_escape_with_zeroes($attribute),
    $isAdd ? 'Add' : 'Remove',
    $customer_id,
    $customer_id,
    mssql_escape_with_zeroes($attribute),
    $isAdd ? 'Add' : 'Remove'
  );

  $result = run_sql_and_check_result($sql);

  if ($result->is_success())
    \CustomerOptions::delCache($customer_id);

  return $result->is_success();
}
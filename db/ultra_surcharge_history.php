<?php

/**
 * add_rows_to_ultra_surcharge_history
 *
 * Add a row into ULTRA.SURCHARGE_HISTORY, in this case the TRANSACTION_ID fields is obtained with an INSERT SELECT
 * $taxes_and_fees_data: sales_tax
 *                       sales_tax_rule
 *                       sales_tax_percentage
 *                       recovery_fee
 *                       recovery_fee_rule
 *                       recovery_fee_basis
 * $params: customer_id
 *          entry_type
 *          reference
 *          location
 *
 * @param  array taxes_and_fees_data
 * @param  array $params
 * @return boolean
 */
function add_rows_to_ultra_surcharge_history( $taxes_and_fees_data , $params )
{
  $success = TRUE;

  if ( $taxes_and_fees_data['mts_tax'] )
  {
    $success = add_to_ultra_surcharge_history(
      array(
        'surcharge_type'    => 'MTS TAX',
        'customer_id'       => $params['customer_id'],
        'entry_type'        => $params['entry_type'],
        'reference'         => isset($params['reference']) ? $params['reference'] : NULL,
        'transaction_id'    => isset($params['transaction_id']) ? $params['transaction_id'] : NULL,
        'amount'            => ( $taxes_and_fees_data['mts_tax'] / 100 ),
        'rule'              => $taxes_and_fees_data['sales_tax_rule'],
        'basis'             => $taxes_and_fees_data['sales_tax_percentage'].'%',
        'location'          => $params['location'],
        'status'            => 'COMPLETE'
      )
    );
  }

  if ( $taxes_and_fees_data['sales_tax'] )
    $success = add_to_ultra_surcharge_history(
      array(
        'surcharge_type'    => 'SALES TAX',
        'customer_id'       => $params['customer_id'],
        'entry_type'        => $params['entry_type'],
        'reference'         => isset($params['reference']) ? $params['reference'] : NULL,
        'transaction_id'    => isset($params['transaction_id']) ? $params['transaction_id'] : NULL,
        'amount'            => ( $taxes_and_fees_data['sales_tax'] / 100 ),
        'rule'              => $taxes_and_fees_data['sales_tax_rule'],
        'basis'             => $taxes_and_fees_data['sales_tax_percentage'].'%',
        'location'          => $params['location'],
        'status'            => 'COMPLETE'
      )
    );

  if ( $taxes_and_fees_data['recovery_fee'] && $success )
    $success = add_to_ultra_surcharge_history(
      array(
        'surcharge_type'    => 'RECOVERY FEE',
        'customer_id'       => $params['customer_id'],
        'entry_type'        => $params['entry_type'],
        'reference'         => isset($params['reference']) ? $params['reference'] : NULL,
        'transaction_id'    => isset($params['transaction_id']) ? $params['transaction_id'] : NULL,
        'amount'            => ( $taxes_and_fees_data['recovery_fee'] / 100 ),
        'rule'              => $taxes_and_fees_data['recovery_fee_rule'],
        'basis'             => $taxes_and_fees_data['recovery_fee_basis'],
        'location'          => $params['location'],
        'status'            => 'COMPLETE'
      )
    );

  return $success;
}

/**
 * add_to_ultra_surcharge_history
 *
 * Add a row into ULTRA.SURCHARGE_HISTORY
 *
 * @return boolean
 */
function add_to_ultra_surcharge_history( $params )
{
  dlog('', '(%s)', func_get_args());

  $sql = isset( $params['transaction_id'] )
         ?
         ultra_surcharge_history_insert_query( $params )
         :
         ultra_surcharge_history_insert_query_by_billing_transition( $params )
         ;

  return run_sql_and_check( $sql );
}

function ultra_surcharge_history_insert_query_by_billing_transition( $params )
{
  return sprintf("
  INSERT INTO ULTRA.SURCHARGE_HISTORY
(
  [SURCHARGE_TYPE],
  [TRANSACTION_ID],
  [AMOUNT],
  [RULE],
  [BASIS],
  [LOCATION],
  [STATUS]
)
  SELECT TOP 1
    %s,
    h.TRANSACTION_ID,
    %s,
    %s,
    %s,
    %s,
    %s
  FROM
    HTT_BILLING_HISTORY h
  WHERE
    h.CUSTOMER_ID = %d
  AND
    h.ENTRY_TYPE  = %s
  AND
    h.REFERENCE   = %s
  ORDER BY h.TRANSACTION_ID DESC
",
  mssql_escape_with_zeroes($params['surcharge_type']),
  $params['amount'],
  mssql_escape_with_zeroes($params['rule']),
  mssql_escape_with_zeroes($params['basis']),
  mssql_escape_with_zeroes($params['location']),
  mssql_escape_with_zeroes($params['status']),
  $params['customer_id'],
  mssql_escape_with_zeroes($params['entry_type']),
  mssql_escape_with_zeroes($params['reference'])
  );
}

/**
 * ultra_surcharge_history_insert_query
 *
 * ULTRA.SURCHARGE_HISTORY INSERT query
 *
 * @return string
 */
function ultra_surcharge_history_insert_query( $params )
{
  return sprintf("
  INSERT INTO ULTRA.SURCHARGE_HISTORY
(
  [SURCHARGE_TYPE],
  [TRANSACTION_ID],
  [AMOUNT],
  [RULE],
  [BASIS],
  [LOCATION],
  [STATUS]
)
  VALUES
(
  %s,
  %d,
  %s,
  %s,
  %s,
  %s,
  %s
)",
  mssql_escape_with_zeroes($params['surcharge_type']),
  $params['transaction_id'],
  $params['amount'],
  mssql_escape_with_zeroes($params['rule']),
  mssql_escape_with_zeroes($params['basis']),
  mssql_escape_with_zeroes($params['location']),
  mssql_escape_with_zeroes($params['status'])
  );
}

?>

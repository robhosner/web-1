<?php

include_once('db.php');

/*
php component to access DB table HTT_CANCELLATION_REASONS

Synopsis:

$sql = htt_cancellation_reasons_insert_query(
  array(
    'customer_id'   => 33,
    'reason'        => 'test',
    'type'          => 'VOID',
    'portout_ocn'   => 1, # PORTOUT, VOID, EXPIRED, CANCELLED
    'portout_carrier_name'       => 'Sprint',
    'portout_ocn_request_date'   => 'getutcdate()',
    'cos_id' => '12345',
    'msisdn' => '1234512345',
    'iccid'  => '1234512345123451234',
    'status' => 'test1',
    'agent'  => 'test_agent1',
    'ever_active' => 0
  )
);
*/

/**
 * is_cancellation_port_out 
 * @param  integer $customer_id
 * @return boolean
 */
function is_cancellation_port_out( $customer_id )
{
  $is_cancellation_port_out = FALSE;

  $sql = htt_cancellation_reasons_select_query(
    array(
      'customer_id'   => $customer_id,
      'select_fields' => array (
        'AGENT'
      )
    )
  );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  $is_cancellation_port_out = ! ! ( $data && is_array($data) && count($data) && ( $data[0]->AGENT == 'Port Out Import Tool' ) );

  dlog('',"is_cancellation_port_out = %s",($is_cancellation_port_out?'true':'false'));

  return $is_cancellation_port_out;
}

/**
 * htt_cancellation_reasons_insert_query 
 * $params: portout_ocn
 *          portout_carrier_name
 *          portout_ocn_request_date
 *          portout_spid
 *          carrier_id
 *          deactivation_date
 *          carrier_name
 *          msisdn
 *          iccid
 *          cos_id
 *          status
 *          agent
 *          ever_active
 *          customer_id
 *          reason
 *          type
 * 
 * @param  array $params
 * @return string SQL
 */
function htt_cancellation_reasons_insert_query($params)
{
  dlog('', '(%s)', func_get_args());

  $portout_ocn              = ( isset($params['portout_ocn']) )              ? $params['portout_ocn']                                    : '0' ;
  $portout_carrier_name     = ( isset($params['portout_carrier_name']) )     ? mssql_escape_with_zeroes($params['portout_carrier_name']) : 'NULL' ;
  $portout_ocn_request_date = ( isset($params['portout_ocn_request_date']) ) ? $params['portout_ocn_request_date']                       : 'NULL' ;
  $portout_spid             = ( isset($params['portout_spid']) )             ? $params['portout_spid']                                   : 'NULL' ;
  $carrier_id               = 'NULL';

  if ( isset( $params['carrier_id'] ) && ( $params['carrier_id'] != '' ) )
    $carrier_id = mssql_escape_with_zeroes($params['carrier_id']);

  if ( ! isset( $params['deactivation_date'] ) )
    $params['deactivation_date'] = 'GETUTCDATE()';

  if ( ! isset( $params['carrier_name'] ) )
    $params['carrier_name'] = '';

  if ( ! isset( $params['msisdn'] ) )
    $params['msisdn'] = '';

  if ( ! isset( $params['iccid'] ) )
    $params['iccid'] = '';

  if ( ! isset( $params['cos_id'] ) )
    $params['cos_id'] = 0;

  if ( ! is_numeric($params['cos_id']))
    $params['cos_id'] = 0;

  $params['status'] = ( isset( $params['status'] ) && $params['status'] )
                      ?
                      $params['status']
                      :
                      'Neutral'
                      ;

  $params['agent'] = ( isset( $params['agent'] ) && $params['agent'] )
                     ?
                     $params['agent']
                     :
                     'no_agent_given'
                     ;

  $params['ever_active'] = ( isset( $params['ever_active'] ) && is_int($params['ever_active']) )
                         ?
                         $params['ever_active']
                         :
                         0
                         ;

  return sprintf(
    "IF NOT EXISTS ( SELECT CUSTOMER_ID FROM HTT_CANCELLATION_REASONS WHERE CUSTOMER_ID = %d )

     BEGIN

     INSERT INTO HTT_CANCELLATION_REASONS
     (
     CUSTOMER_ID,
     REASON,
     TYPE,
     PORTOUT_OCN,
     PORTOUT_CARRIER_NAME,
     PORTOUT_OCN_REQUEST_DATE,
     PORTOUT_SPID,
     DEACTIVATION_DATE,
     DEACTIVATION_CARRIER_NAME,
     DEACTIVATION_CARRIER_ID,
     MSISDN,
     ICCID,
     COS_ID,
     STATUS,
     EVER_ACTIVE,
     AGENT
     )
     VALUES
     (
     %d,
     %s,
     %s,
     %d,
     %s,
     %s,
     %s,
     %s,
     %s,
     %s,
     %s,
     %s,
     %d,
     %s,
     %d,
     %s
     )

    END",

    $params['customer_id'],
    $params['customer_id'],
    mssql_escape_with_zeroes($params['reason']),
    mssql_escape_with_zeroes($params['type']),
    $portout_ocn,
    $portout_carrier_name,
    $portout_ocn_request_date,
    mssql_escape_with_zeroes($portout_spid),
    $params['deactivation_date'],
    mssql_escape_with_zeroes($params['carrier_name']),
    $carrier_id,
    mssql_escape_with_zeroes($params['msisdn']),
    mssql_escape_with_zeroes($params['iccid']),
    $params['cos_id'],
    mssql_escape_with_zeroes($params['status']),
    $params['ever_active'],
    mssql_escape_with_zeroes($params['agent'])
  );
}

/**
 * htt_cancellation_reasons_add
 * $params: see htt_cancellation_reasons_insert_query
 * @param  array $params
 * @return object Result
 */
function htt_cancellation_reasons_add($params)
{
  $htt_cancellation_reasons_insert_query = htt_cancellation_reasons_insert_query($params);

  return run_sql_and_check_result($htt_cancellation_reasons_insert_query);
}

/**
 * get_cancelled_date_epoch 
 * @param  integer $customer_id
 * @return string $cancelled_date_epoch
 */
function get_cancelled_date_epoch( $customer_id )
{
  $cancelled_date_epoch = '';

  $sql = htt_cancellation_reasons_select_query(
    array(
      'customer_id'   => $customer_id,
      'select_fields' => array (
        'DATEDIFF(ss, \'1970-01-01\', dbo.UTC_TO_PT( DEACTIVATION_DATE ) ) cancelled_date_epoch'
      )
    )
  );

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) && count($data) )
    $cancelled_date_epoch = $data[0]->cancelled_date_epoch;

  return $cancelled_date_epoch;
}

/**
 * htt_cancellation_reasons_select_query
 *
 * $params: customer_id
 *          select_fields
 * @param  array $params
 * @return string SQL
 */
function htt_cancellation_reasons_select_query($params)
{
  $select_fields = '*';

  $top_clause = get_sql_top_clause($params);

  $where = array(
    sprintf(" CUSTOMER_ID = %d ",$params['customer_id'] )
  );

  if ( isset($params['select_fields']) )
  {
    $select_fields =
      ( is_array( $params['select_fields'] ) )
      ?
      implode(',',$params['select_fields'])
      :
      $params['select_fields']
      ;
  }

  $where_clause =
    ( count($where) )
    ?
    " WHERE ".implode(" AND ", $where)
    :
    ''
    ;

  $query =
    "SELECT $top_clause $select_fields
     FROM   HTT_CANCELLATION_REASONS WITH (NOLOCK)
     $where_clause";

  return $query;
}

/**
 * get_cancellation_customer_ids_by_msisdn
 *
 * Returns all customer IDs associated with the given MSISDN in HTT_CANCELLATION_REASONS
 *
 * @param  string $msisdn
 * @return array $customer_ids
 */
function get_cancellation_customer_ids_by_msisdn($msisdn)
{
  $customer_ids = array();

  $sql = "EXEC [ULTRA].[Get_Cancelled_Customer_ID] @MSISDN = '$msisdn'";

  $data = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $data && is_array($data) )
  {
    $underscoreObject = new __ ;

    $customer_ids = $underscoreObject->flatten( $data );
  }

  return $customer_ids;
}

/**
 * get_cancellation_by_customer_id
 *
 * return customer's cancellation record from HTT_CANCELLATION_REASONS
 * @param int customer ID
 * @return object table row
 */
function get_cancellation_by_customer_id($customer_id)
{
  if ( ! $customer_id)
    return NULL;

  $sql = sprintf('SELECT TOP 1 * FROM HTT_CANCELLATION_REASONS WITH (NOLOCK) WHERE CUSTOMER_ID = %d ORDER BY DEACTIVATION_DATE DESC',
    $customer_id);
  $data = mssql_fetch_all_objects(logged_mssql_query($sql));
  if ($data && is_array($data) && count($data))
    return $data[0];
  else
    return NULL;
}

/**
 * get_cancellation_by_customer_id
 *
 * return customer's cancellation records from HTT_CANCELLATION_REASONS
 * @param int customer ID
 * @return array rows
 */
function get_cancellations_by_customer_id($customer_id)
{
  if ( ! $customer_id)
    return NULL;

  $sql = sprintf('SELECT TOP 100 * FROM HTT_CANCELLATION_REASONS WITH (NOLOCK) WHERE CUSTOMER_ID = %d ORDER BY DEACTIVATION_DATE DESC',
    $customer_id);
  $data = mssql_fetch_all_objects(logged_mssql_query($sql));
  
  return (count($data)) ? $data : array();
}

/**
 * get_cancellation_by_msisdn_or_iccid
 *
 * @return array rows
 */
function get_cancellation_by_msisdn_or_iccid($params)
{
  $msisdn = !isset($params['msisdn']) ? NULL : $params['msisdn'];
  $iccid  = !isset($params['iccid'])  ? NULL : $params['iccid'];

  $where = null; 

  if (is_null($msisdn) && is_null($iccid))
    return array();

  $where = array();

  $sql = "SELECT REASON, TYPE, DEACTIVATION_DATE, MSISDN, ICCID, STATUS, AGENT
            FROM HTT_CANCELLATION_REASONS WHERE";

  if ($msisdn) $sql .= " MSISDN = '$msisdn'";
  if ($iccid)
  {
    if ($msisdn) $sql .= ' OR';
    $sql .= " ICCID = '$iccid'";
  }

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}


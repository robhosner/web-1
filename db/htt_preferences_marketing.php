<?php

# php component to access DB table HTT_PREFERENCES_MARKETING

# synopsis

#echo htt_preferences_marketing_insert_query(
#  array(
#    "customer_id"  => 123
#  )
#)."\n\n";

#$r = get_marketing_settings(
#  array(
#    "customer_id"  => 1001
#  )
#);

#$errors = set_marketing_settings(
#  array(
#    "customer_id"            => 1001,
#    'marketing_sms_option'   => 1,
#    'marketing_email_option' => 0
#  )
#);

/**
 * set_marketing_settings
 * $params: marketing_email_option
 *          marketing_sms_option
 *          customer_id
 * @param  array $params
 * @return array $errors
 */
function set_marketing_settings( $params )
{
  $errors = array();

  $result = get_marketing_settings( $params );

  if ( count($result['errors']) )
  {
    $errors = $result['errors'];
  }
  else
  {
    $update_params = array(
      array(
        'param_name' => '@customer_id',
        'variable'   => $params['customer_id'],
        'type'       => SQLINT4,
        'is_output'  => false,
        'is_null'    => false,
        'max_length' => 16
      )
    );

    if (isset($params['marketing_email_option']))
    {
      $update_params[] = array(
        'param_name' => '@marketing_email_option',
        'variable'   => $params['marketing_email_option'],
        'type'       => SQLBIT,
        'is_output'  => false,
        'is_null'    => false,
        'max_length' => 1
      );
    }

    if (isset($params['marketing_sms_option']))
    {
      $update_params[] = array(
        'param_name' => '@marketing_sms_option',
        'variable'   => $params['marketing_sms_option'],
        'type'       => SQLBIT,
        'is_output'  => false,
        'is_null'    => false,
        'max_length' => 1
      );
    }

    if ( \Ultra\Lib\DB\run_stored_procedure('[ultra].[Preference_Marketing_Options_Update]', $update_params) !== null)
    {
      $errors[] = "DB error";
    }
  }

  return $errors;
}

/**
 * get_marketing_settings
 * $params: see htt_preferences_marketing_select_query
 * @param  array $params
 * @return array (errors[], marketing_settings[])
 */
function get_marketing_settings( $params )
{
  $result = array( 'errors' => array() , 'marketing_settings' => array() );

  $htt_preferences_marketing_select_query = htt_preferences_marketing_select_query( $params );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_preferences_marketing_select_query));

  if ( $query_result && ( is_array($query_result) ) && count($query_result) > 0 )
  {
    $result['marketing_settings']['last_updated']           = $query_result[0]->last_updated;
    $result['marketing_settings']['marketing_email_option'] = $query_result[0]->marketing_email_option;
    $result['marketing_settings']['marketing_sms_option']   = $query_result[0]->marketing_sms_option;
  }
  else
  {
    $htt_preferences_marketing_insert_query = htt_preferences_marketing_insert_query( $params );

    if ( is_mssql_successful(logged_mssql_query($htt_preferences_marketing_insert_query)) )
    {
      $query_result = mssql_fetch_all_objects(logged_mssql_query($htt_preferences_marketing_select_query));

      if ( $query_result && ( is_array($query_result) ) && count($query_result) > 0 )
      {
        $result['marketing_settings']['last_updated']           = $query_result[0]->last_updated;
        $result['marketing_settings']['marketing_email_option'] = $query_result[0]->marketing_email_option;
        $result['marketing_settings']['marketing_sms_option']   = $query_result[0]->marketing_sms_option;
      }
      else
      { $result['errors'][] = "DB error"; }
    }
    else
    { $result['errors'][] = "DB error"; }
  }

  return $result;
}

/**
 * htt_preferences_marketing_select_query 
 * $params: customer_id
 * @param  array $params
 * @return string SQL
 */
function htt_preferences_marketing_select_query( $params )
{
  $query = sprintf("
    SELECT *
    FROM   HTT_PREFERENCES_MARKETING
    WHERE  customer_id = %d",
    $params['customer_id']
  );

  return $query;
}

/**
 * htt_preferences_marketing_insert_query 
 * $params: customer_id
 * @param  array $params
 * @return string SQL
 */
function htt_preferences_marketing_insert_query( $params )
{
  $query = sprintf("
    IF NOT EXISTS (SELECT customer_id FROM HTT_PREFERENCES_MARKETING WHERE customer_id = %d)

    BEGIN

    INSERT INTO HTT_PREFERENCES_MARKETING
    (customer_id)
    VALUES
    ( %d )

    END",
    $params['customer_id'],
    $params['customer_id']
  );

  return $query;
}

/**
 * validate_marketing_settings
 *
 * @param  array $marketing_settings - array of value/value pairs (no keys)
 * @return array $errors - array of error messages or empty array on success
 */
function validate_marketing_settings($marketing_settings)
{
  $errors = array();

  if ( count($marketing_settings) % 2 != 0 )
    $errors[] = "ERR_API_INVALID_ARGUMENTS: marketing_settings is invalid (length)";
  else
  {
    $allowed_marketing_settings = array(
      'marketing_email_option',
      'marketing_sms_option'
    );

    foreach( $marketing_settings as $id => $string )
    {
      if ( ( $id % 2 ) && ( $string != '0' ) && ( $string != '1') )
        $errors[] = "ERR_API_INVALID_ARGUMENTS: marketing_settings is invalid (values should be 0 or 1)";
      else if ( ! ( $id % 2 ) && ! ( in_array( $string , $allowed_marketing_settings ) ) )
        $errors[] = "ERR_API_INVALID_ARGUMENTS: marketing_settings is invalid (field mismatch)";
    }
  }

  return $errors;
}

/**
 * get_sms_preference_by_customer_id
 *
 * Verifies if marketing_sms_option is present ( the customer opted in SMS promo )
 * Returns TRUE if present.
 *
 * @param  integer $customer_id
 * @return boolean
 */
function get_sms_preference_by_customer_id($customer_id)
{
  $result = get_marketing_settings( array( 'customer_id' => $customer_id ) );

  dlog('',"result = %s",$result);

  if ( ( count($result['errors']) == 0 )
    && ! empty( $result['marketing_settings']['marketing_sms_option'] )
  )
    return ! ! $result['marketing_settings']['marketing_sms_option'];

  // HTT_PREFERENCES_MARKETING row is missing for $customer_id
  return FALSE;
}

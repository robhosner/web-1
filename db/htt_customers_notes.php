<?php

/*

HTT_CUSTOMERS_NOTES

echo htt_customers_notes_insert_query(
  array(
    'customer_id'  => 123456,
    'type'         => 'a',
    'source'       => 'b',
    'user'         => 'c',
    'contents'     => 'd'
  )
)."\n\n";

*/

/**
 * htt_customers_notes_insert_query 
 * $params: customer_id
 *          type
 *          source
 *          user
 *          contents
 *         
 * @param  array $params
 * @return string SQL
 */
function htt_customers_notes_insert_query($params)
{
  $query = sprintf("
INSERT INTO HTT_CUSTOMERS_NOTES
(
  CUSTOMER_ID,
  TYPE,
  SOURCE,
  [USER],
  CONTENTS
)
  VALUES
(
  %d,
  %s,
  %s,
  %s,
  %s
)",
    $params['customer_id'],
    mssql_escape_with_zeroes($params['type']),
    mssql_escape_with_zeroes($params['source']),
    mssql_escape_with_zeroes($params['user']),
    mssql_escape_with_zeroes($params['contents'])
  );

  return $query;
}

?>

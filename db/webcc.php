<?php

include_once('db.php');

# php component to access DB table webcc #

# synopsis

#class test_customer_data
#{
#  public $COS_ID      = 1010;
#  public $CUSTOMER_ID = 19;
#  public $FIRST_NAME  = 'Adam';
#  public $LAST_NAME   = 'Smith';
#  public $CUSTOMER    = 19;
#  public $COMPANY     = 'Umbrella Corp.';
#  public $ADDRESS1    = '1 York Ave';
#  public $ADDRESS2    = 'Apt 1A';
#  public $CITY        = 'New York';
#  public $STATE_REGION = 'NY';
#  public $COUNTRY      = 'USA';
#  public $POSTAL_CODE  = '10128';
#  public $LOCAL_PHONE  = '9009230000';
#  public $E_MAIL       = 'umbrella@example.com';
#  public $CC_NUMBER    = '1234';
#  public $CC_EXP_DATE  = '';
#  public $ACCOUNT_GROUP_ID = '1';
#  public $CCV = '1234';
#}
#
#$customer = new test_customer_data;
#
#echo webcc_insert_query(
#  array(
#    'customer' => $customer,
#    'session' => 's',
#    'charge_amount' => '10',
#    'credit_amount' => '10',
#    'status' => 'WPENDING',
#    'charge_description' => 'test charge',
#    'charge_detail' => 'test detail',
#    'upgrade_seconds_gain' => 10,
#    'date' => date('Y-m-d H:i:s'),
#    'time' => date('12/30/1899 H:i:s')
#  )
#)."\n";

/**
 * webcc_insert_query
 * $params: customer
 *          bypass_billing_info
 *          session
 *          date
 *          time
 *          charge_amount
 *          credit_amount
 *          status
 *          charge_description
 *          charge_detail
 *          upgrade_seconds_gain
 *          
 * @param  array $params
 * @return string query
 */
function webcc_insert_query($params)
{
  dlog('',"THIS CODE IS OBSOLETE AND SHOULD BE REMOVED");

  $customer = $params["customer"];

  $billing_info = array();

  if ( isset($params["bypass_billing_info"]) && is_array($params["bypass_billing_info"]) )
  {
    dlog('',"billing_info bypassed");

    # if "bypass_billing_info" is provided,
    # it must contain
    # - 'cc_number'
    # - 'cc_exp_date'
    # - 'ccv'
    # it may contain
    # - 'first_name'
    # - 'last_name'
    # - 'address1'
    # - 'address2'
    # - 'postal_code'

    $billing_info = $params["bypass_billing_info"];

    $billing_info_optional = array(
      'first_name',
      'last_name',
      'address1',
      'address2',
      'postal_code'
    );

    $customer_array = get_object_vars( $customer );

    foreach( $billing_info_optional as $optional_field )
    {
      if ( ! isset($billing_info[ $optional_field ]) )
      { $billing_info[ $optional_field ] = $customer_array[ strtoupper( $optional_field ) ]; }
    }
  }
  else
  {
    $billing_info = array(
      'cc_number'   => $customer->CC_NUMBER,
      'cc_exp_date' => $customer->CC_EXP_DATE,
      'ccv'         => $customer->CCV,
      'first_name'  => $customer->FIRST_NAME,
      'last_name'   => $customer->LAST_NAME,
      'address1'    => $customer->ADDRESS1,
      'address2'    => $customer->ADDRESS2,
      'postal_code' => $customer->POSTAL_CODE
    );
  }

  $query = sprintf(
    "INSERT INTO WebCC
     (
     SessionId, date, time, Account, First_Name, Last_Name, Company, Address, Address2, City, State, Country, ZipCode, Telephone, Email, CreditCard, CreditExpiry, Charge_Amount, Credit_Amount, Status, Account_Group_Id, COS_ID, CCV, Description, Detail, Packaged_Balance1
     )
     VALUES
     (".
     "%s,". //session
     "'%s',". // date
     "'%s',". // time
     "'%s',". // customer
     "%s,". // first
     "%s,". // last
     "%s,". // company
     "%s,". // address1
     "%s,". // address2
     "%s,". // city
     "%s,". // state_region
     "%s,". // country
     "'%s',". // postal_code
     "'%s',". // local_phone
     "'%s',". // e_mail
     "'%s', -- cc_number\n". // cc_number
     "'%s',". // cc_exp_date
     "%.2f,%.2f," . // charge and credit amounts
     "'%s',". // status
     "%s,%s,'%s',". // account_group_id, cos_id, CCV
     "%s," . // charge_description
     "%s, -1*%d)", // detail and packaged_balance1

     mssql_escape($params['session']),
     $params['date'],
     $params['time'],
     $customer->CUSTOMER,
     mssql_escape($billing_info['first_name']),
     mssql_escape($billing_info['last_name']),
     mssql_escape($customer->COMPANY),
     mssql_escape($billing_info['address1']),
     mssql_escape($billing_info['address2']),
     mssql_escape($customer->CITY),
     mssql_escape($customer->STATE_REGION),
     mssql_escape($customer->COUNTRY),
     $billing_info['postal_code'],
     $customer->LOCAL_PHONE,
     $customer->E_MAIL,
     $billing_info['cc_number'],
     $billing_info['cc_exp_date'],
     $params['charge_amount'], /* charge amount in dollars */
     $params['credit_amount'], /* credit amount */
     $params['status'],
     $customer->ACCOUNT_GROUP_ID,
     $customer->COS_ID,
     $billing_info['ccv'],
     mssql_escape($params['charge_description']),
     mssql_escape($params['charge_detail']),
     $params['upgrade_seconds_gain']
  );

  return $query;
}

/**
 * webcc_select_query
 * $params: time
 *          date
 *          session
 *          
 * @param  array $params
 * @return string $query
 */
function webcc_select_query($params)
{
  $time    = $params['time'];
  $date    = $params['date'];
  $session = $params['session'];

  $query = sprintf(
    "SELECT transactionid, STATUS, RESULT
     FROM   WebCC
     WHERE  SessionId = %s
     AND    date = '%s'
     AND    time = '%s'",
    mssql_escape_with_zeroes($session),
    $date,
    $time
  );

  return $query;
}

/**
 * webcc_rerun_select_query
 * @return string $query
 */
function webcc_rerun_select_query()
{
  return "SELECT * FROM webcc
WHERE date = DATEADD(dd, DATEDIFF(dd, 0, getdate()), 0) AND
      ( result LIKE 'SGS-005000%' OR
        result LIKE 'SGS-000100%' OR
        result LIKE 'SGS-005003%' )
AND status = 'REJECTED'";
}

/**
 * webcc_rerun_update_query
 * @return string $query
 */
function webcc_rerun_update_query()
{
  return "UPDATE webcc
SET status ='PENDING'
WHERE date = DATEADD(dd, DATEDIFF(dd, 0, getdate()), 0) AND
      ( result LIKE 'SGS-005000%' OR
        result LIKE 'SGS-000100%' OR
        result LIKE 'SGS-005003%' )
AND status = 'REJECTED'";
}

/**
 * webcc_distinct_cc_count_by_period
 * @param  integer $customer_id
 * @param  integer $days_range
 * @return integer $distinct_cc_count_by_period
 */
function webcc_distinct_cc_count_by_period($customer_id,$days_range)
{
  $distinct_cc_count_by_period = 0;

  $query = sprintf("
    SELECT COUNT( DISTINCT CREDITCARD ) distinct_cc_count
    FROM   WEBCC w
    JOIN   ACCOUNTS a
    ON     a.ACCOUNT = w.ACCOUNT
    WHERE  a.CUSTOMER_ID = %d
    AND    w.DATE > DATEADD( dd , - %d , GETUTCDATE() )
    AND    w.STATUS = 'APPROVED'
  ",$customer_id
   ,$days_range
  );

  $webcc_data = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( $webcc_data && is_array($webcc_data) && count($webcc_data) )
  {
    $distinct_cc_count_by_period = $webcc_data[0]->distinct_cc_count;
  }

  return $distinct_cc_count_by_period;
}

/**
 * webcc_cc_count_by_period
 * @param  integer $customer_id
 * @param  integer $days_range 
 * @param  string  $status
 * @return integer $rejected_cc_count_by_period
 */
function webcc_cc_count_by_period($customer_id,$days_range,$status)
{
  $rejected_cc_count_by_period = 0;

  $query = sprintf("
    SELECT count(distinct [CREDITCARD]) AS rejected_cc_count
    FROM   WEBCC w
    JOIN   ACCOUNTS a
    ON     a.ACCOUNT = w.ACCOUNT
    WHERE  a.CUSTOMER_ID = %d
    AND    w.DATE > DATEADD( dd , - %d , GETUTCDATE() )
    AND    w.STATUS = %s
    AND    w.RESULT != 'CCV - No Credit Card Specified'
  ",$customer_id
   ,$days_range
   ,mssql_escape_with_zeroes($status)
  );

  $webcc_data = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( $webcc_data && is_array($webcc_data) && count($webcc_data) )
  {
    $rejected_cc_count_by_period = $webcc_data[0]->rejected_cc_count;
  }

  return $rejected_cc_count_by_period;
}

/*
function webcc_amount_by_period($customer_id,$days_range)
{
  $amount = 0;

  $query = sprintf("
    SELECT ISNULL(SUM(w.CHARGE_AMOUNT),0) AS total_charges
    FROM   WEBCC w
    JOIN   ACCOUNTS a
    ON     a.ACCOUNT = w.ACCOUNT
    WHERE  a.CUSTOMER_ID = %d
    AND    w.DATE > DATEADD( dd , - %d , GETUTCDATE() )
    AND    w.STATUS = 'APPROVED'
  ",$customer_id
   ,$days_range
  );

  $webcc_data = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( $webcc_data && is_array($webcc_data) && count($webcc_data) )
  {
    $amount = $webcc_data[0]->total_charges;
  }

  return $amount;
}
*/

?>

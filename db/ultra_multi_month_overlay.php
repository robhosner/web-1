<?php

function ultra_multi_month_overlay_from_customer_id($customer_id)
{
  $sql = sprintf('SELECT TOP 1 * FROM ULTRA.MULTI_MONTH_OVERLAY with (nolock) WHERE CUSTOMER_ID = %d', $customer_id);
  $obj = find_first($sql);
  if ($obj)
    $obj->MONTHS_REMAINING = $obj->TOTAL_MONTHS - $obj->UTILIZED_MONTHS;
  return $obj;
}

function ultra_multi_month_overlay_insert($customer_id, $cycle_expires, $total_months)
{
  $sql = sprintf("
    INSERT INTO ULTRA.MULTI_MONTH_OVERLAY
    (CUSTOMER_ID, CYCLE_STARTED, CYCLE_EXPIRES, TOTAL_MONTHS, UTILIZED_MONTHS)
    VALUES
    (%d, GETUTCDATE(), dbo.PT_TO_UTC('%s'), %d, 1)
  ",
    $customer_id,
    $cycle_expires,
    $total_months
  );

  return run_sql_and_check($sql);
}

function ultra_multi_month_overlay_set($customer_id, $cycle_expires, $total_months)
{
  $sql = sprintf("
    EXEC [ULTRA].[UpdateMultiMonthOverlay]
      @CUSTOMER_ID = %d,
      @CYCLE_EXPIRES = '%s',
      @TOTAL_MONTHS = %d",
    $customer_id,
    $cycle_expires,
    $total_months
  );

  return run_sql_and_check($sql);
}

function ultra_multi_month_overlay_delete($customer_id)
{
  $sql = sprintf("
    DELETE FROM ULTRA.MULTI_MONTH_OVERLAY
    WHERE CUSTOMER_ID = %d
  ",
    $customer_id
  );

  return run_sql_and_check($sql);
}

function ultra_multi_month_overlay_update($customer_id, $params)
{
  if (empty($params))
    return FALSE;

  $set = [];
  foreach ($params as $key => $val)
    $set[] = "$key = $val";

  $set = implode(' , ', $set);

  $sql = sprintf("
    UPDATE ULTRA.MULTI_MONTH_OVERLAY
    SET $set
    WHERE CUSTOMER_ID = %d
  ",
    $customer_id
  );

  return run_sql_and_check($sql);
}
<?php

/**
 * getUltraSettings
 * Retrieve one or all settings
 * 
 * @param  string $token
 * @return mixed $ultraSettings
 */
function getUltraSettings( $token )
{
  $ultraSettings = NULL;

  $query = ( is_null( $token ) )
    ?
    " SELECT * FROM ULTRA_SETTINGS "
    :
    sprintf(
      " SELECT * FROM ULTRA_SETTINGS WHERE NAME = %s ",
      mssql_escape_with_zeroes( $token )
    )
    ;
  $query .= ' ORDER BY NAME';

  $ultraSettingsData = mssql_fetch_all_objects(logged_mssql_query($query));

  if ( is_null( $token ) )
  {
    $ultraSettings = array();

    foreach( $ultraSettingsData as $row )
    {
      $row->DEFAULT = find_credential( $row->NAME );

      if ( $row->DEFAULT == '1' )
        $row->DEFAULT = 'TRUE';

      $ultraSettings[] = $row;
    }

    return $ultraSettings;
  }

  if ( $ultraSettingsData && is_array($ultraSettingsData) && count($ultraSettingsData) )
    $ultraSettings = $ultraSettingsData[0];

  return $ultraSettings;
}

/**
 * assignUltraSettings
 * Adds or modifies a setting named $token with $value and $ttlSeconds
 * 
 * @param  string $token
 * @param  string $value
 * @param  integer $ttlSeconds
 * @return boolean result of query
 */
function assignUltraSettings( $token , $value , $ttlSeconds )
{
  $query = sprintf("
IF EXISTS (SELECT NAME FROM ULTRA_SETTINGS WHERE NAME = %s )
UPDATE  ULTRA_SETTINGS
  SET   VALUE       = %s,
        TTL_SECONDS = %d
  WHERE NAME        = %s
ELSE
  INSERT INTO ULTRA_SETTINGS ( NAME , VALUE , TTL_SECONDS )
  VALUES ( %s , %s , %d )
  ",
  mssql_escape_with_zeroes( $token ),
  mssql_escape_with_zeroes( $value ),
  $ttlSeconds,
  mssql_escape_with_zeroes( $token ),
  mssql_escape_with_zeroes( $token ),
  mssql_escape_with_zeroes( $value ),
  $ttlSeconds
  );

  return is_mssql_successful(logged_mssql_query($query));
}

?>

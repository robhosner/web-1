<?php

/*
htt_auditlog_insert(
  array(
'request_id' => 'test_raf',
'type'       => 'test_raf',
'event'      => 'test_raf',
'status'     => 'test_raf',
'site'       => 0,
'customer'   => 0,
'net'        => 'test_raf',
'source'     => 'test_raf',
'dest'       => 'test_raf',
'channel'      => 'test_raf',
'dollar_cost'  => 0,
'channel_type' => 'test_raf'
  )
);
*/

/**
 * htt_auditlog_insert
 * $params: request_id
 *          type
 *          event
 *          status
 *          site
 *          customer
 *          net
 *          source
 *          dest
 *          channel
 *          dollar_cost
 *          channel_type
 *         
 * @param  array $params
 * @return boolean
 */
function htt_auditlog_insert($params)
{
  $q = sprintf("INSERT INTO htt_auditlog (
  [EVENT_UUID],
  [EVENT_MODULE],
  [EVENT_NAME],
  [EVENT_RESULT],
  [SITE_ID],
  [CUSTOMER_ID],
  [SCOPE_NET],
  [SCOPE_SOURCE],
  [SCOPE_DEST],
  [SCOPE_CHANNEL],
  [SCOPE_DOLLAR_COST],
  [SCOPE_CHANNEL_TYPE]
) VALUES (
  '%s', '%s', '%s', '%s',
  '%s', %s, '%s', '%s', '%s',
  '%s', %s, '%s')",
  $params['request_id'], $params['type'], $params['event'], $params['status'],
  $params['site'], $params['customer'], $params['net'],
  $params['source'], $params['dest'],
  $params['channel'], $params['dollar_cost'], $params['channel_type']);

  return is_mssql_successful(logged_mssql_query($q));
}

?>

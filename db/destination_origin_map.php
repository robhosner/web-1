<?php

include_once('db.php');
include_once('db/htt_customers_overlay_ultra.php');

# php component to access DB table DESTINATION_ORIGIN_MAP

# synopsis

#echo destination_origin_map_insert_query(
#  array(
#    'destination' => '11001001000'
#  )
#)."\n\n";

#echo destination_origin_map_update_query(
#  array(
#    'destination_from' => '11001001000',
#    'destination_to'   => '11001001001'
#  )
#)."\n\n";

#echo destination_origin_map_select_query(
#  array(
#    'destination' => '11001001000'
#  )
#)."\n\n";

/**
 * verify_destination_origin_map 
 * @param  integer $customer_id
 * @param  string $current_mobile_number
 * @return array destination_origin_map_data
 */
function verify_destination_origin_map($customer_id,$current_mobile_number)
{
  $destination_origin_map_data = array();

  $destination_origin_map_prefixes = destination_origin_map_prefixes();

  foreach( $destination_origin_map_prefixes as $prefix )
  {
    $sql = destination_origin_map_select_query(
      array(
        'destination' => $prefix . $current_mobile_number
      )
    );

    $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

    $destination_origin_map_data[ $prefix . $current_mobile_number ] = ! ! ( $query_result && is_array($query_result) && count($query_result) );
  }

  return $destination_origin_map_data;
}

/**
 * destination_origin_map_select_query
 * $params: destination
 * 
 * @param  array $params
 * @return string SQL
 */
function destination_origin_map_select_query($params)
{
  $query = sprintf(
    " SELECT
        DESTINATION,
        ACCOUNT,
        ORIGIN,
        DESTINATION_ORIGIN_MAP_ID
      FROM  DESTINATION_ORIGIN_MAP
      WHERE DESTINATION = %s ",
    mssql_escape_with_zeroes($params['destination'])
  );

  return $query;
}

/**
 * destination_origin_map_delete_query
 * $params: destination
 * 
 * @param  array $params
 * @return string SQL
 */
function destination_origin_map_delete_query($params)
{
  $query =  sprintf(
    "DELETE FROM DESTINATION_ORIGIN_MAP
     WHERE  DESTINATION = %s",
    mssql_escape_with_zeroes($params['destination'])
  );

  return $query;
}

/**
 * destination_origin_map_update_query 
 * $params: destination_to
 *         destination_from
 * @param  array $params
 * @return string SQL
 */
function destination_origin_map_update_query($params)
{
  $query =  sprintf(
    "UPDATE DESTINATION_ORIGIN_MAP
     SET    DESTINATION = %s
     WHERE  DESTINATION = %s
    ",
    mssql_escape_with_zeroes($params['destination_to']),
    mssql_escape_with_zeroes($params['destination_from'])
  );

  return $query;
}

/**
 * destination_origin_map_prefixes 
 * @return array
 */
function destination_origin_map_prefixes()
{
  return array(
    '1' , '' , '011' ,'00111' , '01' , '0' , '001'
  );
}

/**
 * destination_origin_map_cancellation_by_customer_id 
 * @param  integer $customer_id
 * @return array (success=>boolean, errors=>[])
 */
function destination_origin_map_cancellation_by_customer_id($customer_id)
{
  $return = array('success'=>FALSE,'errors'=>array());

  $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query(
    array(
      'select_fields'    => array("current_mobile_number"),
      'customer_id'      => $customer_id
    )
  );

  $ultra_customer_data = mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));

  if ( $ultra_customer_data && is_array($ultra_customer_data) && count($ultra_customer_data) )
  {
    $customer = $ultra_customer_data[0];

    if ( destination_origin_map_cancellation($customer) )
    {
      $return['success'] = TRUE;
    }
    else
    {
      $return['errors'][] = "ERR_API_INTERNAL: DB error";
    }
  }
  else
  {
    $return['errors'][] = "ERR_API_INTERNAL: Customer not found ($customer_id)";
  }

  return $return;
}

/**
 * destination_origin_map_cancellation 
 * @param  object $customer
 * @return boolean $check
 */
function destination_origin_map_cancellation($customer)
{
  $destination_origin_map_prefixes = destination_origin_map_prefixes();

  $check = TRUE;

  foreach( $destination_origin_map_prefixes as $prefix )
  {
    $destination_origin_map_delete_query = destination_origin_map_delete_query(
      array( 'destination' => $prefix . $customer->current_mobile_number )
    );

    dlog('',$destination_origin_map_delete_query);

    $check = $check && run_sql_and_check($destination_origin_map_delete_query);
  }

  return $check;
}

/**
 * destination_origin_map_msisdn_insert 
 * @param  string $msisdn
 * @return boolean $check
 */
function destination_origin_map_msisdn_insert($msisdn)
{
  $destination_origin_map_prefixes = destination_origin_map_prefixes();

  $check = TRUE;

  foreach( $destination_origin_map_prefixes as $prefix )
  {
    $destination_origin_map_insert_query = destination_origin_map_insert_query(
      array( 'destination' => $prefix . $msisdn )
    );

    dlog('',$destination_origin_map_insert_query);

    $check = $check && run_sql_and_check($destination_origin_map_insert_query);
  }

  return $check;
}

/**
 * destination_origin_map_insert_after_activation 
 * @param  object $customer
 * @return boolean
 */
function destination_origin_map_insert_after_activation($customer)
{
  $destination_origin_map_prefixes = destination_origin_map_prefixes();

  $check = TRUE;

  foreach( $destination_origin_map_prefixes as $prefix )
  {
    $destination_origin_map_insert_query = destination_origin_map_insert_query(
      array( 'destination' => $prefix . $customer->current_mobile_number )
    );

    dlog('',$destination_origin_map_insert_query);

    $check = $check && run_sql_and_check($destination_origin_map_insert_query);
  }

  return $check;
}

/**
 * destination_origin_map_insert_query
 * $params: destination
 * 
 * @param  array $params
 * @return string SQL
 */
function destination_origin_map_insert_query($params)
{
  $params['account'] = 'CALL_ME_FREE';
  $params['origin']  = '7001';

  $query = sprintf( "
IF NOT EXISTS (SELECT * FROM DESTINATION_ORIGIN_MAP where DESTINATION = %s )
BEGIN
INSERT INTO DESTINATION_ORIGIN_MAP
(
  DESTINATION,
  ACCOUNT,
  ORIGIN
)
VALUES
(
  %s,
  %s,
  %s
)
END
",
  mssql_escape_with_zeroes($params['destination']),
  mssql_escape_with_zeroes($params['destination']),
  mssql_escape_with_zeroes($params['account']),
  mssql_escape_with_zeroes($params['origin'])
  );

  return $query;
}

/**
 * return an array of queries rather than a single
 * @see MVNO-2180
 * @return string SQL
 */
function destination_origin_map_fixup_query()
{
  $destination_origin_map_prefixes = destination_origin_map_prefixes();

  $query = array();

  foreach( $destination_origin_map_prefixes as $prefix )
  {
    $query[] = sprintf("
      insert into DESTINATION_ORIGIN_MAP (DESTINATION,ACCOUNT,ORIGIN)
      select distinct '%s'+[MSISDN],'CALL_ME_FREE',7001 from [HTT_ULTRA_MSISDN] where len(msisdn)=10 and MSISDN_ACTIVATED =1 and '%s'+[MSISDN] not in 
      ( select destination from [DESTINATION_ORIGIN_MAP] )
      ;",
      $prefix,
      $prefix
    );
  }

  return $query;
}

?>

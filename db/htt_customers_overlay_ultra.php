<?php

include_once('db.php');
include_once('cosid_constants.php');
require_once ('lib/util-common.php');

# php component to access DB table htt_customers_overlay_ultra #

/*

Synopsis:

$query_params = array( 'sql_limit' => 10 );
echo customers_suspended_for_cancellation_query( $query_params )."\n\n";

echo htt_customers_overlay_ultra_update_query(
  array(
    'customer_id'      => 50,
    'applied_data_soc' => 'test socs',
    'preferred_language' => 'IT'
  )
)."\n\n";

echo htt_customers_overlay_ultra_update_query(
  array(
    'customer_id'            => 123,
    'monthly_renewal_target' => 'NULL'
  )
)."\n\n";

echo htt_customers_overlay_ultra_update_query(
  array(
    'monthly_renewal_target' => 'L29',
    'customer_id'            => 123
  )
)."\n\n";

echo htt_customers_overlay_ultra_update_query(
  array(
    'balance_to_add' => 10,
    'customer_id' => 123
  )
)."\n\n";

echo htt_customers_overlay_ultra_update_query(
  array(
    'stored_value' => 0,
    'customer_id' => 123
  )
)."\n\n";

echo htt_customers_overlay_ultra_update_query(
  array(
    'easypay_activated' => 1,
    'customer_id'       => 123
  )
)."\n\n";

echo htt_customers_overlay_ultra_update_query(
  array(
    'customer_id'           => 123456,
    'current_iccid_full'    => '2345678901234567890',
    'activation_iccid_full' => '2345678901234567890'
  )
)."\n\n";

echo htt_customers_overlay_ultra_update_query(
  array(
    'customer_id'           => 123456,
    'current_mobile_number' => '456'
  )
)."\n\n";

echo htt_customers_overlay_ultra_insert_query(
  array(
    'customer_id'           => 123456,
    'stored_value'          => 29.9,
    'preferred_language'    => 'ES',
    'notes'                 => 'added for testing',
    'current_mobile_number' => '1001001000',
    'current_iccid'         => '123456789012345678',
    'current_iccid_full'    => '1234567890123456789',
    'applied_data_soc'      => 'test soc',
    'applied_data_soc_date' => 'getutcdate()',
    'easypay_activated'     => 0,
    'plan_state'            => 'test plan state',
    'plan_started'          => 'getutcdate()',
    'plan_expires'          => 'getutcdate()',
    'monthly_cc_renewal'    => 0
  )
)."\n\n";

echo htt_customers_overlay_ultra_select_query(
  array(
    'customer_id'           => 123456,
    'current_mobile_number' => '1001001000',
  )
)."\n\n";

echo htt_customers_overlay_ultra_select_query(
  array(
    'select_fields' => array('customer_id'),
    'sql_limit'  => 1,
    'expired'    => TRUE,
    'plan_state' => 'Active'
  )
)."\n\n";

echo htt_customers_overlay_ultra_select_query(
  array(
    'select_fields' => array('current_iccid','ACTIVATION_ICCID','CURRENT_ICCID_FULL','ACTIVATION_ICCID_FULL','CUSTOMER_ID'),
    'any_iccid'     => '890126084210224238'
  )
)."\n\n";

$customer = get_ultra_customer_from_customer_id(50,array(
'CUSTOMER_ID', 'current_mobile_number', 'current_iccid', 'applied_data_soc', 'applied_data_soc_date', 'easypay_activated', 'stored_value', 'plan_state', 'preferred_language', 'plan_started', 'plan_expires', 'monthly_cc_renewal', 'tos_accepted', 'MONTHLY_RENEWAL_TARGET', 'CANCEL_REQUESTED', 'CANCELLED', 'ACTIVATION_ICCID', 'DATA_RECHARGES_LEFT', 'CUSTOMER_SOURCE', 'CURRENT_ICCID_FULL', 'ACTIVATION_ICCID_FULL'
));
*/

/**
 * ultra_customer_match_by_sim
 *
 * @param  string $iccid
 * @return boolean TRUE if $iccid is found in any of the htt_customers_overlay_ultra ICCID fields
 */
function ultra_customer_match_by_sim( $iccid )
{
  $customer_match_by_sim = FALSE;

  $sql = htt_customers_overlay_ultra_select_query(
    array(
      'select_fields' => array('CURRENT_ICCID','ACTIVATION_ICCID','CURRENT_ICCID_FULL','ACTIVATION_ICCID_FULL','CUSTOMER_ID'),
      'any_iccid'     => $iccid
    )
  );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  $customer_match_by_sim = ! ! ( $query_result && is_array($query_result) && count($query_result) );

  if ( $customer_match_by_sim )
    dlog('',"%s",$query_result[0]);

  return $customer_match_by_sim;
}

/**
 * htt_customers_overlay_ultra_insert_query
 * $params: notes
 *          preferred_language
 *          current_mobile_number
 *          current_iccid
 *          current_iccid_full
 *          applied_data_soc
 *          applied_data_soc_date
 *          tos_accepted
 *          customer_source
 *          easypay_activated
 *          stored_value
 *          monthly_cc_renewal
 *          mvne
 *          brand_id
 *          customer_id
 *          plan_state
 *          plan_started
 *          plan_expires
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_customers_overlay_ultra_insert_query($params)
{
  dlog('',$params);

  $applied_data_soc      = 'NULL';
  $applied_data_soc_date = 'NULL';

  if ( ! isset($params["notes"]) )
    $params["notes"] = '';

  if ( ! isset($params["preferred_language"]) )
    $params["preferred_language"] = 'EN';

  if ( isset($params["current_mobile_number"]) )
    $params["current_mobile_number"] = mssql_escape_with_zeroes($params["current_mobile_number"]);
  else
    $params["current_mobile_number"] = "''";

  if ( isset($params["current_iccid"]) )
    $params["current_iccid"] = "'".$params["current_iccid"]."'";
  else
    $params["current_iccid"] = "''";

  if ( isset($params["current_iccid_full"]) )
    $params["current_iccid_full"] = "'".$params["current_iccid_full"]."'";
  else
    $params["current_iccid_full"] = "''";

  if ( isset($params["applied_data_soc"]) )
    $applied_data_soc = mssql_escape($params["applied_data_soc"]);

  if ( isset($params["applied_data_soc_date"]) )
    $applied_data_soc_date = $params["applied_data_soc_date"];

  if ( ! isset($params["tos_accepted"]) || ! $params["tos_accepted"] )
    $params["tos_accepted"] = 'NULL';
  else
    $params["tos_accepted"] = 'getutcdate()';

  if ( ! isset($params["customer_source"]) || ! $params["customer_source"])
    $params["customer_source"] = 'NULL';
  else
    $params["customer_source"] = mssql_escape_with_zeroes($params["customer_source"]);

  if ( ! isset($params["easypay_activated"]) )
    $params["easypay_activated"] = 0;

  if ( ! isset($params["stored_value"]) )
    $params["stored_value"] = 0;

  if ( ! isset($params["monthly_cc_renewal"]) )
    $params["monthly_cc_renewal"] = 0;

  if ( ! isset($params["mvne"]) )
    $params["mvne"] = '1';

  $query = sprintf(
    "INSERT INTO HTT_CUSTOMERS_OVERLAY_ULTRA
(
  customer_id,
  preferred_language,
  notes,
  current_mobile_number,
  CURRENT_ICCID_FULL,
  current_iccid,
  applied_data_soc,
  applied_data_soc_date,
  easypay_activated,
  stored_value,
  plan_state,
  plan_started,
  plan_expires,
  monthly_cc_renewal,
  tos_accepted,
  CUSTOMER_SOURCE,
  MVNE,
  BRAND_ID
)
VALUES
(
  %d,
  %s,
  '%s',
  %s,
  %s,
  %s,
  %s,
  %s,
  %d,
  %d,
  %s,
  %s,
  %s,
  %d,
  %s,
  %s,
  %s,
  %d
)",
    $params['customer_id'],
    mssql_escape($params["preferred_language"]),
    $params["notes"],
    $params["current_mobile_number"],
    $params["current_iccid_full"],
    $params["current_iccid"],
    $applied_data_soc,
    $applied_data_soc_date,
    $params["easypay_activated"],
    $params["stored_value"],
    mssql_escape($params["plan_state"]),
    $params["plan_started"],
    $params["plan_expires"],
    $params["monthly_cc_renewal"],
    $params["tos_accepted"],
    $params["customer_source"],
    mssql_escape($params["mvne"]),
    $params['brand_id']
  );

  return $query;
}

/**
 * htt_customers_overlay_ultra_update_query 
 * $params: balance_to_add
 *          tos_accepted
 *          preferred_language
 *          current_mobile_number
 *          mvne
 *          notes
 *          current_iccid
 *          current_iccid_full
 *          applied_data_soc
 *          applied_data_soc_date
 *          iccid_activated
 *          activation_iccid
 *          activation_iccid_full
 *          monthly_renewal_target
 *          stored_value
 *          easypay_activated
 *          monthly_cc_renewal
 *          customer_id
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_customers_overlay_ultra_update_query($params)
{
  dlog('',"htt_customers_overlay_ultra_update_query invoked with params = %s",$params);

  $set_clause = '';

  $additional_where_clause = '';

  $comma = '';

  # TODO: rename this parameter :-/
  if ( isset($params['balance_to_add']) )
  {
    $set_clause .= $comma . sprintf(
      " stored_value = stored_value + %s ",
      $params['balance_to_add']
    );

    $comma = ',';
  }

  if ( isset($params['tos_accepted']) )
  {
    $value = ($params['tos_accepted']) ? 'getutcdate()' : 'NULL' ;
    $set_clause .= $comma . " tos_accepted = $value ";
    $comma = ',';
  }

  if ( isset($params['preferred_language']) && $params['preferred_language'] )
  {
    $set_clause .= $comma . sprintf(
      " preferred_language = %s ",
      mssql_escape_with_zeroes($params['preferred_language'])
    );

    $comma = ',';
  }

  if ( isset($params['current_mobile_number']) && ( is_numeric($params['current_mobile_number']) || ( $params['current_mobile_number'] === "NULL" ) ) )
  {
    if ( $params['current_mobile_number'] === "NULL" )
      $set_clause .= $comma . " current_mobile_number = NULL";
    else
      $set_clause .= $comma . " current_mobile_number = '".$params['current_mobile_number']."'";

    $comma = ',';
  }

  if ( isset($params['mvne']) && $params['mvne'] )
  {
    $set_clause .= $comma . sprintf(" mvne = %s ",mssql_escape_with_zeroes($params['mvne']));
    $comma = ',';
  }

  if ( isset($params['notes']) )
  {
    $set_clause .= $comma . " notes = '".$params['notes']."'";
    $comma = ',';
  }

  if ( isset($params['iccid_current']) )
  {
    $params['current_iccid_full'] = luhnenize( $params['iccid_current'] );
    $params['current_iccid']      = substr( $params['current_iccid_full'] , 0 , -1 );
  }

  if ( isset($params['current_iccid']) )
  {
    $set_clause .= $comma . " current_iccid = '".$params['current_iccid']."'";
    $comma = ',';
  }

  if ( isset($params['current_iccid_full']) )
  {
    $set_clause .= $comma . " current_iccid_full = '".$params['current_iccid_full']."'";
    $comma = ',';
  }

  if ( isset($params['applied_data_soc']) )
  {
    $set_clause .= $comma . " applied_data_soc = '".$params['applied_data_soc']."'";
    $comma = ',';
  }

  if ( isset($params['applied_data_soc_date']) )
  {
    $set_clause .= $comma . " applied_data_soc_date = ".$params['applied_data_soc_date'];
    $comma = ',';
  }

  if ( isset($params['plan_state']) && $params['plan_state'] )
  {
    $set_clause .= $comma . " plan_state = '".$params['plan_state']."'";
    $comma = ',';
  }

  if ( ! empty($params['plan_started']) )
  {
    $set_clause .= $comma . " plan_started = ".$params['plan_started'];
    $comma = ',';
  }

  if ( ! empty($params['plan_expires']) )
  {
    $set_clause .= $comma . " plan_expires = ".$params['plan_expires'];
    $comma = ',';
  }

  if ( ! empty($params['gross_add_date']) )
  {
    $set_clause .= $comma . " gross_add_date = ".$params['gross_add_date'];
    $comma = ',';
  }

  if ( isset($params['iccid_activated']) && $params['iccid_activated'] )
  {
    $params['activation_iccid_full'] = luhnenize( $params['iccid_activated'] );
    $params['activation_iccid']      = substr( $params['activation_iccid_full'] , 0 , -1 );
  }

  if ( isset($params['activation_iccid_full']) && $params['activation_iccid_full'] )
  {
    $set_clause .= $comma . " ACTIVATION_ICCID_FULL = '".$params['activation_iccid_full']."'";
    $comma = ',';

    $additional_where_clause .= ' AND ACTIVATION_ICCID_FULL IS NULL '; # it should be set only once
  }

  if ( isset($params['monthly_renewal_target']) )
  {
    if ( $params['monthly_renewal_target'] === 'NULL' )
    {
      $set_clause .= $comma .
        " MONTHLY_RENEWAL_TARGET           = NULL,".
        " MONTHLY_RENEWAL_TARGET_REQUESTED = NULL ";
    }
    else
    {
      $set_clause .= $comma .
        " MONTHLY_RENEWAL_TARGET           = '".$params['monthly_renewal_target']."',".
        " MONTHLY_RENEWAL_TARGET_REQUESTED = getutcdate() ";
    }

    $comma = ',';
  }

  if ( isset($params['activation_iccid']) && $params['activation_iccid'] )
  {
    $set_clause .= $comma . " ACTIVATION_ICCID = '".$params['activation_iccid']."'";
    $comma = ',';

    $additional_where_clause .= ' AND ACTIVATION_ICCID IS NULL '; # it should be set only once
  }

  if ( isset($params['stored_value']) && preg_match('/^[\d\.]+$/', $params['stored_value']) )
  {
    $set_clause .= $comma . " STORED_VALUE = ".$params['stored_value'].' ';
    $comma = ',';
  }

  $numeric_params = array(
    'easypay_activated' , 'monthly_cc_renewal'
  );

  foreach( $numeric_params as $nparam )
    if ( isset($params[ $nparam ]) )
    {
      $set_clause .= $comma .
        sprintf(
          " $nparam = %d ",
          $params[ $nparam ]
        );

      $comma = ',';
    }

  $query = sprintf(
  "UPDATE htt_customers_overlay_ultra
   SET    $set_clause
   WHERE  CUSTOMER_ID = %d
   $additional_where_clause
   ",
   $params['customer_id']
  );

  return $query;
}

/**
 * get_latest_customer_plan_from_transition 
 * returns the latest TO_COS_ID / plan
 * $params: see get_ultra_customer_most_recent_state_transition
 * 
 * @param  array $params
 * @return string $ultra_customer_plan
 */
function get_latest_customer_plan_from_transition($params)
{
  $ultra_customer_plan = '';

  $latest_transition = get_ultra_customer_most_recent_state_transition($params);

  if ( $latest_transition && is_array($latest_transition) && count($latest_transition) )
  {
    $ultra_customer_plan = get_plan_name_from_short_name( get_plan_from_cos_id( $latest_transition[0]->TO_COS_ID ) );
  }

  return $ultra_customer_plan;
}

/**
 * get_latest_customer_state_from_transition 
 * returns the latest TO_PLAN_STATE value of the most recent OPEN or CLOSED state transition for the given customer
 * $params: see get_ultra_customer_most_recent_state_transition
 * 
 * @param  array $params
 * @return string $ultra_customer_state
 */
function get_latest_customer_state_from_transition($params)
{
  $ultra_customer_state = '';

  $latest_transition = get_ultra_customer_most_recent_state_transition($params);

  if ( $latest_transition && is_array($latest_transition) && count($latest_transition) )
  {
    $ultra_customer_state = $latest_transition[0]->TO_PLAN_STATE;
  }

  return $ultra_customer_state;
}

/**
 * get_ultra_customer_most_recent_state_transition
 * $params: msisdn
 *          customer_id
 *          
 * @param  array $params
 * @return object $latest_transition
 */
function get_ultra_customer_most_recent_state_transition($params)
{
  $customer_clause = '';

  if ( isset( $params['msisdn'] ) )
    $customer_clause = sprintf(" o.current_mobile_number = %s ",mssql_escape_with_zeroes($params['msisdn']));

  elseif ( isset( $params['customer_id'] ) )
    $customer_clause = sprintf(" o.CUSTOMER_ID = %d ",$params['customer_id']);

  else
    return '';

  $sql = sprintf("
    SELECT TOP 1 t.*
    FROM   HTT_TRANSITION_LOG t
    JOIN   HTT_CUSTOMERS_OVERLAY_ULTRA o
    ON     o.CUSTOMER_ID = t.CUSTOMER_ID
    WHERE  t.TO_PLAN_STATE IS NOT NULL
    AND    t.TO_PLAN_STATE != ''
    AND    t.STATUS in ('OPEN','CLOSED')
    AND    %s
    ORDER BY t.CREATED DESC
    ",
    $customer_clause
  );

  $latest_transition = mssql_fetch_all_objects(logged_mssql_query($sql));

  return $latest_transition;
}

/**
 * get_ultra_customer_state_from_msisdn 
 * @param  string $msisdn
 * @return string $ultra_customer_state
 */
function get_ultra_customer_state_from_msisdn($msisdn)
{
  $ultra_customer_state = '';

  if ( (strlen($msisdn) > 10) )
  {
    $msisdn = substr($msisdn, 1);
  }

  $customers = get_ultra_customers_from_msisdn($msisdn,array('customer_id','plan_state'));

  dlog('',"%s",$customers);

  if ( $customers && is_array($customers) && count($customers) )
  {
    if ( count($customers) == 1 )
    {
      $ultra_customer_state = $customers[0]->plan_state;
    }
    else
    {
      dlog('',"Error: multiple ultra customers for msisdn $msisdn");
    }
  }

  return $ultra_customer_state;
}

/**
 * get_ultra_customers_from_iccid 
 * @param  string $iccid
 * @param  array $select_fields
 * @return object[]
 */
function get_ultra_customers_from_iccid($iccid,$select_fields)
{
  $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query(
    array(
      'select_fields'         => $select_fields,
      'iccid'                 => $iccid
    )
  );

  return mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));
}

/**
 * get_ultra_customer_from_iccid
 * @param  string $iccid
 * @param  array $select_fields
 * @return object
 */
function get_ultra_customer_from_iccid($iccid,$select_fields)
{
  $customers = get_ultra_customers_from_iccid($iccid,$select_fields);
  return ($customers && count($customers)) ? $customers[0] : NULL;
}

/**
 * get_ultra_customers_from_msisdn 
 * @param  string $msisdn
 * @param  array $select_fields
 * @return object[]
 */
function get_ultra_customers_from_msisdn($msisdn, $select_fields)
{
  if ( (strlen($msisdn) == 11) )
  {
    $msisdn = substr($msisdn, 1);
  }

  $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query(
    array(
      'select_fields'         => $select_fields,
      'current_mobile_number' => $msisdn
    )
  );

  return mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));
}


/**
 * get_ultra_customer_from_msisdn
 *
 * @param  $missdn
 * @return object NULL || single row of htt_customers_overlay_ultra by MSISDN (aka current_mobile_number)
 *
 */
function get_ultra_customer_from_msisdn($msisdn)
{
  if (empty($msisdn))
    return NULL;

  $query = htt_customers_overlay_ultra_select_query(array('current_mobile_number' => normalize_msisdn($msisdn), 'nolock' => TRUE));
  $result = mssql_fetch_all_objects(logged_mssql_query($query));

  if (is_array($result) && count($result))
    return $result[0];
  else
    return NULL;
}


/**
 * get_ultra_customer_from_customer_id 
 * @param  integer $customer_id
 * @param  array $select_fields
 * @return object $customer || NULL
 */
function get_ultra_customer_from_customer_id($customer_id, $select_fields = NULL)
{
  $customer = NULL;

  $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query(
    array(
      'select_fields' => $select_fields,
      'customer_id'  => $customer_id
    )
  );

  $ultra_customer_data = mssql_fetch_all_objects(logged_mssql_query($htt_customers_overlay_ultra_select_query));

  if ( $ultra_customer_data && is_array($ultra_customer_data) && count($ultra_customer_data) )
  {
    $customer = $ultra_customer_data[0];
  }

  return $customer;
}

/**
 * htt_customers_overlay_ultra_select_query
 * Queries only HTT_CUSTOMERS_OVERLAY_ULTRA
 * $params: select_fields
 *          any_iccid
 *
 * @param  array $params
 * @return string SQL
 */
function htt_customers_overlay_ultra_select_query($params)
{
  $select_fields = '*';

  $top_clause = get_sql_top_clause($params);

  $where = array();

  if ( isset($params['select_fields']) )
  {
    // format days before expire
    $days_before_expire = '';
    $index = array_search('days_before_expire', $params['select_fields']);

    if ($index !== FALSE)
    {
      unset($params['select_fields'][$index]);
      $days_before_expire .= 'DATEDIFF(Day, getutcdate() , plan_expires) days_before_expire';
      if (count($params['select_fields'])) $days_before_expire = ', ' . $days_before_expire;
    }

    $index = array_search('latest_plan_date', $params['select_fields']);
    if ( $index !== FALSE )
    {
      $params['select_fields'][$index] = 'DATEADD( dd , -1 , plan_expires ) latest_plan_date';
    }

    $select_fields  = implode(',',$params['select_fields']);
    $select_fields .= $days_before_expire;
  }

  if ( isset($params['any_iccid']) )
  {
    # fix ICCID
    $iccid_19 = $params['any_iccid'];
    $iccid_18 = $params['any_iccid'];

    if (strlen($params['any_iccid']) == 18)
    { $iccid_19 = luhnenize($iccid_18);   }
    else
    { $iccid_18 = substr($iccid_19,0,-1); }

    $where[] = sprintf(
    " ( CURRENT_ICCID         = %s OR
        CURRENT_ICCID_FULL    = %s OR
        ACTIVATION_ICCID      = %s OR
        ACTIVATION_ICCID_FULL = %s ) ",
      mssql_escape_with_zeroes($iccid_18),
      mssql_escape_with_zeroes($iccid_19),
      mssql_escape_with_zeroes($iccid_18),
      mssql_escape_with_zeroes($iccid_19)
    );
  }

  if ( isset($params['iccid']) )
  {
    $where[] = ( strlen($params['iccid']) == 18 )
               ?
               sprintf( "CURRENT_ICCID      = %s " , mssql_escape_with_zeroes($params['iccid']) )
               :
               sprintf( "CURRENT_ICCID_FULL = %s " , mssql_escape_with_zeroes($params['iccid']) )
               ;
  }

  if ( isset($params['customer_id']) )
  {
    $where[] = "customer_id = ".$params['customer_id'];
  }

  if ( isset($params['customer_id_list']) && is_array($params['customer_id_list']) && count($params['customer_id_list']) )
  {
    $where[] = "customer_id in (".implode(",",$params['customer_id_list']).") ";
  }

  if ( isset($params['plan_state']) )
  {
    $where[] = sprintf(
      "plan_state = %s ",
      mssql_escape_with_zeroes($params['plan_state'])
    );
  }

  if ( isset($params['current_mobile_number']) )
  {
    $where[] = sprintf(
      "current_mobile_number = %s ",
      mssql_escape_with_zeroes($params['current_mobile_number'])
    );
  }

  if ( isset($params['expired']) && ( $params['expired'] === TRUE ) )
  {
    $where[] = " plan_expires IS NOT NULL AND plan_expires <= getutcdate() ";
  }

  if ( isset($params['plan_expires_dd_mm_yyyy']) )
  {
    $where[] = " convert(varchar, plan_expires, 105) = '".$params['plan_expires_dd_mm_yyyy']."' ";
  }

  if ( isset($params["monthly_cc_renewal"]) && ( $params["monthly_cc_renewal"] != '' ) )
  {
    $where[] = " monthly_cc_renewal = ".$params["monthly_cc_renewal"];
  }

  $where_clause = ' WHERE '.implode(" AND ", $where);

  $nolock = empty($params['nolock']) ? NULL : 'WITH (NOLOCK)';

  $query =
    "SELECT $top_clause $select_fields
     FROM htt_customers_overlay_ultra $nolock $where_clause";

  return $query;
}

/**
 * get_customer_from_cancelled_msisdn 
 * @param  string $msisdn
 * @return object $customer || FALSE
 */
function get_customer_from_cancelled_msisdn($msisdn)
{
  # $msisdn is HTT_CANCELLATION_REASONS.msisdn

  $sql = make_find_ultra_customer_query_from_cancelled_msisdn($msisdn);

  $customer = find_customer($sql);

  return $customer;
}

/**
 * get_customer_from_msisdn 
 * @param  string $msisdn
 * @param  array $select_attributes
 * @return object $customer || FALSE
 */
function get_customer_from_msisdn($msisdn,$select_attributes=NULL)
{
  # $msisdn is HTT_CUSTOMERS_OVERLAY_ULTRA.current_mobile_number

  $find_ultra_customer_query_from_msisdn = make_find_ultra_customer_query_from_msisdn($msisdn,$select_attributes);

  $customer = find_customer($find_ultra_customer_query_from_msisdn);

  return $customer;
}

/**
 * get_customers_from_email 
 * @param  string $email
 * @return object[]
 */
function get_customers_from_email($email)
{
  $find_ultra_customer_query_from_email = make_find_ultra_customer_query_from_email($email);

  dlog("", "get_customers_from_email invoked with $email");

  return mssql_fetch_all_objects(logged_mssql_query($find_ultra_customer_query_from_email));
}

/**
 * get_customers_from_name 
 * @param  string $name
 * @return object[]
 */
function get_customers_from_name($name)
{
  $find_ultra_customer_query_from_name = make_find_ultra_customer_query_from_name($name);

  dlog("", "get_customer_from_name invoked with $name");

  return mssql_fetch_all_objects(logged_mssql_query($find_ultra_customer_query_from_name));
}

/**
 * get_customer_from_customer 
 * @param  string $customer
 * @return object $customer
 */
function get_customer_from_customer($customer)
{
  $find_ultra_customer_query_from_customer = make_find_ultra_customer_query_from_customer($customer);

  dlog("","get_customer_from_customer invoked with $customer");

  $customer = find_customer($find_ultra_customer_query_from_customer);

  return $customer;
}

/**
 * get_customer_from_iccid 
 * @param  string $iccid
 * @param  array $select_attributes
 * @return object $customer || FALSE
 */
function get_customer_from_iccid($iccid,$select_attributes=NULL)
{
  $find_ultra_customer_query_from_iccid = make_find_ultra_customer_query_from_iccid($iccid,$select_attributes);

  dlog('',"get_customer_from_iccid invoked with $iccid");

  $customer = find_customer($find_ultra_customer_query_from_iccid);

  return $customer;
}

/**
 * get_customer_from_actcode 
 * @param  string $actcode
 * @return object $customer || FALSE
 */
function get_customer_from_actcode($actcode)
{
  $find_ultra_customer_query_from_actcode = make_find_ultra_customer_query_from_actcode($actcode);

  dlog('',"get_customer_from_actcode invoked with $actcode");

  $customer = find_customer($find_ultra_customer_query_from_actcode);

  return $customer;
}

/**
 * get_customer_from_customer_id
 *
 * @deprecated Do not use this function. Use get_ultra_customer_from_customer_id instead.
 *
 * @param  integer $customer_id
 * @param  array $select_fields
 * @return object $customer || FALSE
 */
function get_customer_from_customer_id($customer_id,$select_fields=NULL)
{
  $find_ultra_customer_query_from_customer_id = make_find_ultra_customer_query_from_customer_id($customer_id,$select_fields);

  dlog('',"get_customer_from_customer_id invoked with $customer_id");

  $customer = find_customer($find_ultra_customer_query_from_customer_id);

  return $customer;
}

function getCustomerFromCustomerIdForMRCProc($customer_id)
{
  $sql = "EXEC [ULTRA].[Get_Customer_Info] @CUSTOMER_ID = $customer_id";

  if ( ! $customer = find_first($sql))
    return null;

  foreach ($customer as $key => $val)
  {
    $lowKey = strtolower($key);
    $customer->$lowKey = $val;
  }

  return $customer;
}

/**
 * get_customer_from_login 
 * @param  string $login
 * @return object $customer || FALSE
 */
function get_customer_from_login($login)
{
  $find_ultra_customer_query_from_login = make_find_ultra_customer_query_from_login($login);

  dlog('',"get_customer_from_login invoked with $login");

  $customer = find_customer($find_ultra_customer_query_from_login);

  return $customer;
}

/**
 * func_flush_stored_value 
 * @param  integer $customer_id
 * @return array (errors=>[], success=>boolean)
 */
function func_flush_stored_value($customer_id)
{
  $return = array( 'errors' => array() , 'success' => FALSE );

  $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
    array(
      'customer_id'  => $customer_id,
      'stored_value' => 0
    )
  );

  $check = run_sql_and_check($htt_customers_overlay_ultra_update_query);

  if ( $check )
  {
    $return['success'] = TRUE;
  }
  else
  {
    $return['errors'][] = "ERR_API_INTERNAL: DB error.";
  }

  return $return;
}

/**
 * func_reset_monthly_renewal_target 
 * sets the monthly renewal target to $monthly_renewal_target for $customer_id
 * @param  integer $customer_id
 * @param  string  $monthly_renewal_target, defaults to 'NULL'
 * @return array (errors=>[], success=>boolean)
 */
function func_reset_monthly_renewal_target($customer_id, $monthly_renewal_target = 'NULL')
{
  $return = array( 'errors' => array() , 'success' => FALSE );

  $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
    array(
      'customer_id'            => $customer_id,
      'monthly_renewal_target' => $monthly_renewal_target
    )
  );

  $check = run_sql_and_check($htt_customers_overlay_ultra_update_query);

  if ( $check )
  {
    $return['success'] = TRUE;
  }
  else
  {
    $return['errors'][] = "Error while updating htt_customers_overlay_ultra.";
  }

  return $return;
}

/**
 * trackDataSOC 
 * @param  integer $customer_id
 * @param  string $data_soc
 * @return object Result
 */
function trackDataSOC($customer_id,$data_soc)
{
  if (!isset($customer_id))
    return make_error_Result('ERR_API_INVALID_ARGUMENTS: missing parameter customer_id');

  if (!isset($data_soc))
    return make_error_Result('ERR_API_INVALID_ARGUMENTS: missing parameter data_soc');

  // Whenever a data SOC is applied to a customer, we should update htt_customers_overlay_ultra.applied_data_soc and applied_data_soc_date

  $htt_customers_overlay_ultra_update_query = htt_customers_overlay_ultra_update_query(
    array(
      "customer_id"           => $customer_id,
      "applied_data_soc_date" => 'getutcdate()',
      "applied_data_soc"      => $data_soc
    )
  );

  return run_sql_and_check_result($htt_customers_overlay_ultra_update_query);
}

/*
function all_customers_for_cancellation_query( $query_params )
{
  $top_clause = get_sql_top_clause($query_params);

  return "WITH ALL_CUSTOMERS_FOR_CANCELLATION ( customer_id , plan_state ) as (".
    customers_suspended_for_cancellation_query( $query_params ).
    " UNION " .
    customers_neutral_for_cancellation_query( $query_params ).
    " UNION " .
    customers_port_requested_for_cancellation_query( $query_params ).
    " UNION " .
    customers_provisioned_for_cancellation_query( $query_params ).
    " UNION " .
    customers_port_denied_for_cancellation_query( $query_params ).
    ") SELECT $top_clause customer_id , plan_state FROM ALL_CUSTOMERS_FOR_CANCELLATION ORDER BY customer_id ASC";
}

function customers_provisioned_for_cancellation_query( $query_params )
{
  $query_params['status']   = 'Provisioned';
  $query_params['days_ago'] = \Ultra\UltraConfig\monthly_charge_cancellation_delay( $query_params['status'] );

  return customers_for_cancellation_query( $query_params );
}

function customers_suspended_for_cancellation_query( $query_params )
{
  $query_params['status']   = 'Suspended';
  $query_params['days_ago'] = \Ultra\UltraConfig\monthly_charge_cancellation_delay( $query_params['status'] );

  return customers_for_cancellation_query( $query_params );
}

function customers_neutral_for_cancellation_query( $query_params )
{
  $query_params['status']   = 'Neutral';
  $query_params['days_ago'] = \Ultra\UltraConfig\monthly_charge_cancellation_delay( $query_params['status'] );

  return customers_for_cancellation_query( $query_params );
}

function customers_port_requested_for_cancellation_query( $query_params )
{
  $query_params['status']   = 'Port-In Requested';
  $query_params['days_ago'] = \Ultra\UltraConfig\monthly_charge_cancellation_delay( $query_params['status'] );

  return customers_for_cancellation_query( $query_params );
}

function customers_port_denied_for_cancellation_query( $query_params )
{
  $query_params['status']   = 'Port-In Denied';
  $query_params['days_ago'] = \Ultra\UltraConfig\monthly_charge_cancellation_delay( $query_params['status'] );

  return customers_for_cancellation_query( $query_params );
}
*/

/**
 * customers_promo_unused_for_cancellation_query 
 * @param  array $query_params
 * @return string SQL
 */
function customers_promo_unused_for_cancellation_query( $query_params )
{
  $top_clause = get_sql_top_clause($query_params);

  $customers_clause =
    ( isset($query_params['customer_id_list']) && is_array($query_params['customer_id_list']) && count($query_params['customer_id_list']) )
    ?
    " AND u.customer_id in (".implode(",",$query_params['customer_id_list']).") "
    :
    '';

  return sprintf("
    SELECT $top_clause u.customer_id , u.plan_state
    FROM   htt_customers_overlay_ultra u
    JOIN   ULTRA.CUSTOMER_OPTIONS      o on o.CUSTOMER_ID  = u.CUSTOMER_ID
    JOIN   ULTRA.PROMOTIONAL_PLANS     p on o.OPTION_VALUE = p.ULTRA_PROMOTIONAL_PLANS_ID
    WHERE  u.plan_state       = '%s'
    AND    o.OPTION_ATTRIBUTE = 'PROMO_ASSIGNED'
    AND    p.FORCE_CANCEL     = 1
    AND    p.PROMO_STATUS     = 'READY'
    $customers_clause
    ORDER BY u.customer_id ASC",
    $query_params['status']
  );
}

/**
 * get_customers_for_retry
 * @param  array $query_params
 * @return string SQL
 */
function get_customer_ids_for_retry( $query_params = array() )
{
  dlog('', "(%s)", func_get_args());

  $result = array(
    'errors'               => array(),
    'customer_id_list'     => array()
  );

  $customers_clause =
    ( isset($query_params['customer_id_list']) && is_array($query_params['customer_id_list']) && count($query_params['customer_id_list']) )
    ? " AND CUSTOMER_ID in (".implode(",",$query_params['customer_id_list']).") "
    : '';

  $sql = sprintf("SELECT chargelog.CUSTOMER_ID FROM HTT_MONTHLY_SERVICE_CHARGE_LOG as chargelog WITH (NOLOCK) 
    INNER JOIN HTT_CUSTOMERS_OVERLAY_ULTRA as overlay WITH (NOLOCK) ON chargelog.CUSTOMER_ID = overlay.CUSTOMER_ID
    WHERE chargelog.STATUS = 'RETRY_CC' AND overlay.plan_state = '%s'
    $customers_clause
    ORDER BY chargelog.CUSTOMER_ID ASC",
    STATE_SUSPENDED
  );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) )
  {
    foreach($query_result as $data)
      $result['customer_id_list'][] = $data[0];

    if ( count($result['customer_id_list']) > 1 )
      shuffle( $result['customer_id_list'] );
  }

  return $result;
}

/**
 * customers_for_cancellation_query 
 * @param  array $query_params
 * @return string SQL
 */
function customers_for_cancellation_query( $query_params )
{
  if ( $query_params['status'] == 'Promo Unused' )
    return customers_promo_unused_for_cancellation_query( $query_params );

  $top_clause = get_sql_top_clause($query_params);

  $customers_clause =
    ( isset($query_params['customer_id_list']) && is_array($query_params['customer_id_list']) && count($query_params['customer_id_list']) )
    ?
    " AND u.customer_id in (".implode(",",$query_params['customer_id_list']).") "
    :
    '';

  // get all customers which
  // - are in $query_params['status'] status
  // - have a transition to $query_params['status'] $days_ago old
  // - have not another successful (closed) transition since $days_ago days
  return sprintf("
    DECLARE @BEGINDATE datetime
    SET     @BEGINDATE = DATEADD( d , - %d , getutcdate() )

    SELECT $top_clause u.customer_id , u.plan_state
    FROM   htt_customers_overlay_ultra u WITH (NOLOCK)
    JOIN   ACCOUNTS a WITH (NOLOCK)
    ON     a.customer_id  = u.customer_id
    WHERE  u.plan_state    = '%s'
    AND    a.CREATION_DATE_TIME < @BEGINDATE
    AND    LEN(u.current_mobile_number) = 10
    AND    LEN(u.CURRENT_ICCID_FULL) = 19
    AND    a.customer_id NOT IN (
      SELECT ha.customer_id
      FROM   htt_transition_archive ha WITH (NOLOCK)
      WHERE  ha.STATUS = 'CLOSED'
      AND    ha.CLOSED > @BEGINDATE
    )
    AND    a.customer_id NOT IN (
      SELECT ht.customer_id
      FROM   htt_transition_log ht WITH (NOLOCK)
      WHERE  ht.STATUS = 'CLOSED'
      AND    ht.CLOSED > @BEGINDATE
    ) $customers_clause
    ORDER BY u.customer_id ASC",
    $query_params['days_ago'],
    $query_params['status']
  );
}

/**
 * customers_for_monthly_charge_query 
 * $query_params: see htt_customers_overlay_ultra_select_query
 * @param  array $query_params
 * @return string SQL
 */
function customers_for_monthly_charge_query( $query_params )
{
  $htt_customers_overlay_ultra_select_query = htt_customers_overlay_ultra_select_query( $query_params );

  # add clause for being processed in the last month
  $htt_customers_overlay_ultra_select_query .=
    " AND customer_id NOT IN
        (
        SELECT CUSTOMER_ID
        FROM   HTT_MONTHLY_SERVICE_CHARGE_LOG
        WHERE  (
               STATUS = 'RESERVED' OR
               STATUS = 'COMPLETE' OR
               ( STATUS = 'ERROR' AND NEXT_ATTEMPT_DATETIME IS NULL) OR
               ( STATUS = 'ERROR' AND NEXT_ATTEMPT_DATETIME IS NOT NULL AND NEXT_ATTEMPT_DATETIME > getutcdate() )
               )
        AND    getutcdate() <= DATEADD(d, 15, CREATED_DATETIME)
        )
    ";

  return $htt_customers_overlay_ultra_select_query;
}

/**
 * can_process_monthly_charge_customer 
 * @param  integer $customer_id
 * @return boolean $can_process_monthly_charge_customer
 */
function can_process_monthly_charge_customer( $customer_id )
{
  # we need to check if the customer is still Active and there currently are no active state changes

  $can_process_monthly_charge_customer = TRUE;

  $sql = sprintf("
    DECLARE @customer INT = %d;
    SELECT count(*)
    FROM   htt_customers_overlay_ultra u
    WHERE  u.customer_id = @customer
    AND    ( u.plan_state != 'Active' OR u.plan_expires > getutcdate() )

    UNION ALL

    SELECT count(*)
    FROM   htt_transition_log t
    WHERE  t.customer_id = @customer
    AND    t.STATUS = 'OPEN'

    UNION ALL

    SELECT count(*)
    FROM   HTT_MONTHLY_SERVICE_CHARGE_LOG l
    WHERE  l.customer_id = @customer
    AND    l.STATUS NOT IN ('ERROR', 'TODO')
    AND    DATEDIFF( dd , l.CREATED_DATETIME , getutcdate() ) = 0 -- today'",
    $customer_id
  );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  $reason = NULL;
  if ($query_result)
  {
    if ($query_result[0][0])
      $reason = 'plan state';
    if ($query_result[1][0])
      $reason = 'transition';
    if ($query_result[2][0])
      $reason = 'service charge log status';

    if ($reason)
      $can_process_monthly_charge_customer = FALSE;
  }
  else
    logError('can_process_monthly_charge_customer query error');

  logInfo("customer ID $customer_id: " . ($can_process_monthly_charge_customer ? 'can' : "cannot due to $reason"));

  return $can_process_monthly_charge_customer;
}

/**
 * func_accept_terms_of_service 
 * @param  integer $customer_id
 * @return boolean
 */
function func_accept_terms_of_service( $customer_id )
{
  $sql = htt_customers_overlay_ultra_update_query(
    array(
      'customer_id'  => $customer_id,
      'tos_accepted' => '1'
    )
  );

  return run_sql_and_check($sql);
}

/**
 * port_in_requested_customer_finder 
 * @return string SQL
 */
function port_in_requested_customer_finder()
{
  return "SELECT *
          from   htt_customers_overlay_ultra h,
                 htt_portin_log p
          WHERE  h.customer_id = p.customer_id
          AND    h.plan_state = 'Port-In Requested'";
}

/**
 * getPreferredLanguageFromMSISDN 
 * 
 * retrieve htt_customers_overlay_ultra.preferred_language
 * default is EN - ignore errors
 * 
 * @param  string $msisdn
 * @return string $preferred_language
 */
function getPreferredLanguageFromMSISDN( $msisdn )
{
  $preferred_language = 'EN';

  try
  {
    if ( is_null($msisdn) || ( ! $msisdn ) )
      throw new Exception("No MSISDN given");

    $msisdn_10 = $msisdn;

    if ( (strlen($msisdn) != 10) )
    {
      $msisdn_10 = substr($msisdn_10, 1);
    }

    $customers = get_ultra_customers_from_msisdn($msisdn,array('preferred_language'));

    dlog('',"%s",$customers);

    if ( $customers && is_array($customers) && count($customers) && $customers[0]->preferred_language )
    {
      $preferred_language = $customers[0]->preferred_language;
    }
  }
  catch(Exception $e)
  {
    dlog('', $e->getMessage());
  }

  return $preferred_language;
}

/**
 * get_ultra_customers_from_multiple
 * get ultra customers from multiple fields using SELECT ... [field] OR [field] query
 * $params: see htt_customers_overlay_ultra_select_query
 * 
 * @param  array $params
 * @return object[]
 */
function get_ultra_customers_from_multiple($params)
{
  // prepare SELECT query as usual but replace AND with OR
  $query = htt_customers_overlay_ultra_select_query($params);
  $query = str_replace(' AND ', ' OR ', $query);

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

/**
 * get_old_portin_customers
 * return a list of customers in 'Port-In Requested' or 'Port-In Denied' states older than $days
 * 
 * @param  integer days old
 * @param  integer MVNE
 * @return object[]
 */
function get_old_portin_customers($days, $mvne = NULL)
{
  $mvne = $mvne ? "AND c.MVNE = '$mvne' " : NULL;

  $sql = 'SELECT * FROM HTT_CUSTOMERS_OVERLAY_ULTRA c ' .
    'JOIN ACCOUNTS a ON c.CUSTOMER_ID = a.CUSTOMER_ID ' .
    "WHERE a.CREATION_DATE_TIME < DATEADD(DAY, -$days, GETUTCDATE()) $mvne" .
    "AND c.PLAN_STATE IN ('Port-In Requested', 'Port-In Denied')";
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

/**
 * htt_customers_overlay_ultra_clone
 * clones htt_customers_overlay_ultra row data for one customer_id to another
 * @param  Integer $from_customer_id
 * @param  Integer $to_customer_id
 * @param  Integer $fields_to_copy row columns to copy
 * @return Result
 */
function htt_customers_overlay_ultra_clone_by_customer_id($from_customer_id, $to_customer_id, $fields_to_copy)
{
  $result = new \Result();

  $sql = "SELECT TOP 1 " . implode(',', $fields_to_copy) . " FROM htt_customers_overlay_ultra WITH (NOLOCK) WHERE CUSTOMER_ID = $from_customer_id";
  $from_row = find_first($sql);

  if (!$from_row)
  {
    $result->add_error("htt_customers_overlay_ultra row doesn't not exist for CUSTOMER_ID $from_customer_id");
    return $result;
  }

  $i = 0;
  $values = "";
  foreach ($fields_to_copy as $field)
  {
    if (empty($from_row->$field))
      continue;

    if ($i) $values .= ',';
    $i++;

    $values .= "$field = '" . $from_row->$field . "'";
  }

  $sql = "UPDATE htt_customers_overlay_ultra SET $values WHERE CUSTOMER_ID = $to_customer_id";

  if (! run_sql_and_check($sql))
  {
    $result->add_error("Could not update htt_customers_overlay_ultra for CUSTOMER_ID $from_customer_id");
    return $result;
  }

  $result->succeed();
  return $result;
}

/**
 * get_ultra_customers_by_msisdn_or_iccids
 * returns customers with matching msisdn OR iccids
 * 
 * @param  string $msisdn
 * @param  array  $iccids
 * @return object[]
 */
function get_ultra_customers_by_msisdn_or_iccids($msisdn, array $iccids)
{
  $params = sprintf('DECLARE @MSISDN VARCHAR(16) = %s;', mssql_escape_with_zeroes($msisdn));
  $where = 'WHERE current_mobile_number = @MSISDN';
  for ( $i=0; $i < count( $iccids ); $i++ )
  {
    $params .= sprintf('DECLARE @ICCID_%d VARCHAR(32) = %s;', $i, mssql_escape_with_zeroes(luhnenize($iccids[$i])));
    $where .= sprintf(' OR CURRENT_ICCID_FULL = @ICCID_%d', $i);
  }

  $query = $params . 'SELECT * FROM htt_customers_overlay_ultra with (nolock) ' . $where;

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

<?php

/**
 * get_ultra_incomm_inventory_sim_by_customer_id
 *
 * @param  string $iccid
 * @return array row || empty array
 */
function get_ultra_incomm_inventory_sim_by_customer_id($customer_id)
{
  $ultra_incomm_inventory_sim = array();

  $sql = \Ultra\Lib\DB\makeSelectQuery(
    'ULTRA.INCOMM_INVENTORY_SIM',
    1,
    '*',
    array('CUSTOMER_ID' => $customer_id),
    NULL,
    NULL,
    TRUE
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
    $ultra_incomm_inventory_sim = $query_result[0];

  return $ultra_incomm_inventory_sim;
}

/**
 * ultra_incomm_inventory_sim_insert
 *
 * @return string SQL
 */
function ultra_incomm_inventory_sim_insert($params)
{
  $sql = sprintf("INSERT INTO ULTRA.INCOMM_INVENTORY_SIM (
    ICCID,
    TYPE,
    CREATED_DATE,
    CREATED_BY,
    EXPIRES_DATE,
    STORED_VALUE,
    START_DATE
  ) VALUES (
    '%s',
    '%s',
    GETUTCDATE(),
    '%s',
    %s,
    %d,
    %s
  )",
  $params['iccid'],
  $params['type'],
  $params['created_by'],
  $params['expires_date'],
  $params['stored_value'],
  $params['start_date']);

  return $sql;
}

/**
 * ultra_incomm_inventory_sim_update
 *
 * @return string SQL
 */
function ultra_incomm_inventory_sim_update_sql($params)
{
  if ($params['redemption_date'] == 'now')
    $params['redemption_date'] = 'GETUTCDATE()';
  else
    $params['redemption_date'] = mssql_escape_with_zeroes($params['redemption_date']);

  $sql = sprintf("UPDATE ULTRA.INCOMM_INVENTORY_SIM
    SET CUSTOMER_ID = %d, REDEMPTION_DATE = %s
    WHERE ICCID = %s",
  $params['customer_id'],
  $params['redemption_date'],
  mssql_escape_with_zeroes($params['iccid']));

  return $sql;
}

/**
 * ultra_incomm_inventory_sim_update_customer_id_sql
 *
 * @param  iccid
 * @param  customer_id
 * @return string SQL
 */
function ultra_incomm_inventory_sim_update_customer_id_sql($iccid, $customer_id, $plan_cost)
{
  return sprintf("UPDATE ULTRA.INCOMM_INVENTORY_SIM
    SET CUSTOMER_ID = %d, STORED_VALUE = %d
    WHERE ICCID = %s", $customer_id, $plan_cost, mssql_escape_with_zeroes($iccid));
}

/**
 * ultra_incomm_inventory_sim_insert
 *
 * @param  int $hours // GETUTCDATE() - hours, how far back to select
 * @return string SQL
 */
function select_recently_activated_sims($hours)
{
  $sql = sprintf("SELECT ICCID, 
      HTT_ACTIVATION_LOG.ACTIVATED_CUSTOMER_ID, 
      HTT_ACTIVATION_LOG.PROMISED_AMOUNT 
    FROM ULTRA.INCOMM_INVENTORY_SIM AS UIIS with (nolock)
    INNER JOIN HTT_ACTIVATION_LOG with (nolock) ON
      UIIS.ICCID = HTT_ACTIVATION_LOG.ICCID_FULL AND
      UIIS.CUSTOMER_ID IS NULL AND
      UIIS.TYPE = '7_11' AND
      UIIS.EXPIRES_DATE > GETUTCDATE() AND
      HTT_ACTIVATION_LOG.ACTIVATED_DATE > DATEADD(hh, -%d, GETUTCDATE())",
    $hours);

  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

?>

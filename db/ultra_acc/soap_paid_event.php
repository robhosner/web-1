<?php

/**
 * get_soap_paid_event_by_soap_log_id
 *
 * Retrieves a row from SOAP_PAID_EVENT given a SOAP_LOG_ID
 *
 * @return array
 */
function get_soap_paid_event_by_soap_log_id( $soap_log_id=NULL )
{
  if (empty( $soap_log_id ) )
  {
    \logError('get_soap_paid_event invoked with empty SOAP_LOG_ID');
    return [];
  }

  $data = mssql_fetch_all_objects(
    logged_mssql_query(
      \Ultra\Lib\DB\makeSelectQuery(
        'SOAP_PAID_EVENT',
        NULL,
        array( 'SOAP_LOG_ID' , 'MAKEITSO_QUEUE_ID' , 'SOC_NAME' ),
        array( 'SOAP_LOG_ID' => $soap_log_id )
      )
    )
  );

  if ( ! empty( $data ) && is_array( $data ) )
    return $data[0];

  return [];
}

/**
 * get_soap_paid_event_by_makeitso_queue_id
 *
 * Retrieves a row from SOAP_PAID_EVENT given a MAKEITSO_QUEUE_ID
 *
 * @return array
 */
function get_soap_paid_event_by_makeitso_queue_id( $makeitso_queue_id=NULL )
{
  if (empty( $makeitso_queue_id ) )
  {
    \logError('get_soap_paid_event_by_makeitso_queue_id invoked with empty MAKEITSO_QUEUE_ID');
    return [];
  }

  $data = mssql_fetch_all_objects(
    logged_mssql_query(
      \Ultra\Lib\DB\makeSelectQuery(
        'SOAP_PAID_EVENT',
        NULL,
        array( 'SOAP_LOG_ID' , 'MAKEITSO_QUEUE_ID' , 'SOC_NAME' ),
        array( 'MAKEITSO_QUEUE_ID' => $makeitso_queue_id )
      )
    )
  );

  if ( ! empty( $data ) && is_array( $data ) )
    return $data[0];

  return [];
}

/**
 * get_successful_paid_event_by_makeitso_queue_id
 *
 * Retrieves successful SOAP_LOG and SOAP_PAID_EVENT data given a MAKEITSO_QUEUE_ID
 *
 * @return array
 */
function get_successful_paid_event_by_makeitso_queue_id( $makeitso_queue_id=NULL )
{
  if (empty( $makeitso_queue_id ) )
  {
    \logError('get_successful_paid_event_by_makeitso_queue_id invoked with empty MAKEITSO_QUEUE_ID');
    return [];
  }

  $sql = sprintf(
    "SELECT s.SOAP_LOG_ID
     FROM   SOAP_LOG        s
     JOIN   SOAP_PAID_EVENT e
     ON     s.SOAP_LOG_ID = e.SOAP_LOG_ID
     WHERE  s.RESULTCODE        = '%d'
     AND    e.MAKEITSO_QUEUE_ID = %d",
    ACC_SUCCESS_RESULTCODE,
    $makeitso_queue_id
  );

  return find_first($sql);
}

/**
 * exists_successful_paid_event_by_makeitso_queue_id
 *
 * Returns TRUE if there exists a successful SOAP_PAID_EVENT associated to a given MAKEITSO_QUEUE_ID
 *
 * @return array
 */
function exists_successful_paid_event_by_makeitso_queue_id( $makeitso_queue_id=NULL )
{
  $event = get_successful_paid_event_by_makeitso_queue_id( $makeitso_queue_id );

  return ! ! ( is_object($event) && ! empty( $event->SOAP_LOG_ID ) );
}

/**
 * inserts a row into soap_paid_event
 */
function insert_soap_paid_event($soap_log_id, $soc_name = null, $makeitso_queue_id = null)
{
  $fields = [
    'SOAP_LOG_ID'       => $soap_log_id,
    'SOC_NAME'          => $soc_name,
    'MAKEITSO_QUEUE_ID' => $makeitso_queue_id
  ];

  $cols = [];
  $vals = [];

  foreach ($fields as $key => $val)
  {
    if ($val)
    {
      $cols[] = $key;
      $vals[] = "'$val'";
    }
  }

  $cols = implode(',', $cols);
  $vals = implode(',', $vals);

  $sql = "INSERT INTO SOAP_PAID_EVENT ($cols) VALUES ($vals)";

  return run_sql_and_check($sql);
}
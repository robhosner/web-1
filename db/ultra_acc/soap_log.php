<?php

// PHP component for ULTRA_ACC..SOAP_LOG queries

/**
 * get_max_soap_log_id
 *
 * Returns MAX(SOAP_LOG_ID)
 *
 * @return integer
 */
function get_max_soap_log_id()
{
  $sql = "SELECT MAX(SOAP_LOG_ID) MAX_SOAP_LOG_ID FROM SOAP_LOG with (nolock)";

  $data = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( ! empty( $data )
    && is_array( $data )
    && ! empty( $data[0] )
    && ! empty( $data[0][0] )
  )
    return $data[0][0];

  return NULL;
}

/**
 * get_soap_log_aggregated
 *
 * @param  string $iccid
 * @param  string $msisdn
 * @param  array $dates
 * @return object[] $soap_log_data
 */
function get_soap_log_aggregated( $iccid , $msisdn, $dates )
{
  $soap_log_data = array();

  if ( !$iccid && !$msisdn )
  {
    dlog('',"invalid input");
    return $soap_log_data;
  }

  $where_iccid = $iccid ? sprintf("ICCID = %s", mssql_escape_with_zeroes(luhnenize($iccid))) : NULL;
  $where_msisdn = $msisdn ? sprintf("MSISDN = %s", mssql_escape_with_zeroes(luhnenize($msisdn))) : NULL;
  $where_clause = $iccid && $msisdn ? "($where_iccid OR $where_msisdn)" : $where_iccid . $where_msisdn;

  // add date range if given
  if (is_array($dates) && count($dates) == 2)
    $where_clause .= " AND SOAP_DATE BETWEEN '{$dates[0]}' AND '{$dates[1]}'";
  else // use default range
    $where_clause .= ' AND SOAP_DATE > dateadd(dd, -15, getutcdate())';

  $table = 'SOAP_LOG';

  $sql = "with arrange as
    (select
    min(soap_date) as began,
    command,
    tag,
    sum(case when type_id=1 then 1 else 0 end) as sync_request,
    min(case when type_id=1 then soap_date else null end) as sync_request_time,
    sum(case when type_id=2 then 1 else 0 end) as sync_response,
    min(case when type_id=2 then soap_date else null end) as sync_response_time,
    sum(case when type_id=3 then 1 else 0 end) as async_request,
    min(case when type_id=3 then soap_date else null end) as async_request_time,
    sum(case when type_id=4 then 1 else 0 end) as async_response,
    expect_sync = case
    when command in ('QuerySubscriber', 'UpdateWholesalePlan', 'CheckBalance', 'GetNetworkDetails', 'PortInEligibility', 'CancelDeviceLocation', 'GetNGPList', 'SendSMS') then 1
    when command in ('RenewPlan', 'ChangeSIM', 'ReactivateSubscriber', 'DeactivateSubscriber', 'ChangeMSISDN', 'SuspendSubscriber', 'RestoreSubscriber', 'ActivateSubscriber') then 1
    else 0 end,
    expect_async = case
    when command in ('NotificationReceived', 'PortOutDeactivation', 'ThrottlingAlert', 'PortOutRequest', 'ProvisioningStatus') then 1
    when command in ('RenewPlan', 'ChangeSIM', 'ReactivateSubscriber', 'DeactivateSubscriber', 'ChangeMSISDN', 'SuspendSubscriber', 'RestoreSubscriber', 'ActivateSubscriber') then 1
    else 0 end, max(RESULTCODE) as RESULTCODE
    from $table WITH (NOLOCK)
    where $where_clause
    and (tag is not null or COMMAND='ThrottlingAlert') --debug
    group by command,tag),
    
    replaceprovision as (
    select began,command,tag,row_number() OVER (PARTITION BY tag ORDER BY began asc) rn
    from arrange where command != 'ProvisioningStatus'),
    
    substitute as (
    select began,
    case when command = 'ProvisioningStatus' then (select command from replaceprovision q where q.tag=a.tag)
    else command end as command,tag,
    sync_request,sync_response,async_request,async_response,expect_sync,expect_async,
    case when command = 'ProvisioningStatus' then 1
    else 0 end as correlation,sync_request_time,sync_response_time,async_request_time,RESULTCODE
     from arrange a
    )
    select
    s.began,
    case
    when s.RESULTCODE=100 then 'SUCCESS'
    when s.RESULTCODE is null and s.expect_sync = 0 then null
    when s.RESULTCODE!=100 then 'FAILED'
    else null end as status, s.RESULTCODE,
    
    s.command,s.tag,
    case
    when s.sync_request = 1 and s.expect_sync = 1 then 'OK'
    when s.sync_request > 1 and s.expect_sync = 1 then 'OVERLAP'
    when s.sync_request = 0 and s.expect_sync = 1 then 'MISSING'
    when s.expect_sync = 0 then null
    end as sync_request,
    case
    when s.sync_response = 1 and s.expect_sync = 1 then 'OK'
    when s.sync_response > 1 and s.expect_sync = 1 then 'OVERLAP'
    when s.sync_response = 0 and s.expect_sync = 1 then 'MISSING'
    when s.expect_sync = 0 then null
    end as sync_response,
    case
    -- handle provision status join
    when s.expect_sync =1 and s.expect_async = 1 and s.RESULTCODE != 100 then 'FAILED'
    when s.expect_sync =1 and s.expect_async = 1 and r.async_request = 1 then 'OK'
    when s.expect_sync =1 and s.expect_async = 1 and r.async_request > 1 then 'OVERLAP'
    when s.expect_sync =1 and s.expect_async = 1 and r.async_request = 0 then 'MISSING'
    -- otherwise handle async only
    when s.async_request = 1 and s.expect_async = 1 then 'OK'
    when s.async_request > 1 and s.expect_async = 1 then 'OVERLAP'
    when s.async_request = 0 and s.expect_async = 1 then 'MISSING'
    when s.expect_async = 0 then null end as async_request,
    case when (s.sync_request_time is not null and s.sync_response_time is not null)
     then datediff(ss,s.sync_request_time,s.sync_response_time)
     else null end as delay_sync_complete,
    case when (s.sync_response_time is not null and r.async_request_time is not null)
     then datediff(ss,s.sync_response_time,r.async_request_time)
     else null end as delay_async_complete,
    case when (s.sync_request_time is not null and r.async_request_time is not null)
     then datediff(ss,s.sync_request_time,r.async_request_time)
     else null end as delay_e2e_completed,
    s.sync_request_time,s.sync_response_time,r.async_request_time
    
    from (select * from substitute s where s.correlation=0) s
    left join (select * from substitute s where s.correlation=1) r on (s.tag=r.tag)
    order by began";

  $soap_log_data = array();

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) )
    $soap_log_data = $data;

  return $soap_log_data;
}

/**
 * get_soap_log
 *
 * Reads from ULTRA_ACC..SOAP_LOG
 *
 * @param array $params SQL column field names => values
 * @param boolean $archive search in ARCHIVE table
 * @return object[] $soap_log_data
 */
function get_soap_log( $params, $archive = FALSE )
{
  $soap_log_data = array();

  $where = array();

  $top_clause = isset($params['sql_limit']) ? get_sql_top_clause($params) : NULL;

  if ( isset($params['type_id']) )
    $where[] = sprintf(" TYPE_ID    = %d ",$params['type_id']);

  if ( isset($params['msisdn']) )
    $where_msisdn = ( is_array( $params['msisdn'] ) )
               ?
               ' MSISDN IN ('.implode(',',array_map("mssql_escape_with_zeroes", $params['msisdn'])).')'
               :
               sprintf(" MSISDN     = %s ",mssql_escape_with_zeroes($params['msisdn']))
               ;

  if ( isset($params['iccid']) )
    $where_iccid = ( is_array( $params['iccid'] ) )
               ?
               ' ICCID IN ('.implode(',',array_map("mssql_escape_with_zeroes", $params['iccid'])).')'
               :      
               sprintf(" ICCID     = %s ",mssql_escape_with_zeroes($params['iccid']))
               ;     

  // AMDOCS-365: using both ICCID AND MSISDN results in too strict SQL query; use OR instead
  if (! empty($where_msisdn))
    if (! empty($where_iccid))
      $where[] = "($where_msisdn OR $where_iccid)";
    else
      $where[] = $where_msisdn;
  elseif (! empty($where_iccid))
    $where[] = $where_iccid;

  if ( isset($params['command']) )
    $where[] = ( is_array( $params['command'] ) ) 
               ?
               ' COMMAND IN ('.implode(',',array_map("mssql_escape_with_zeroes", $params['command'])).')'
               :
               sprintf(" COMMAND = %s ",mssql_escape_with_zeroes($params['command']))
               ;

  if ( isset($params['session_id']) )
    $where[] = sprintf(" SESSION_ID = %s ",mssql_escape_with_zeroes($params['session_id']));

  if (array_key_exists('tag', $params))
    $where[] = $params['tag'] ? sprintf(" TAG        = %s ",mssql_escape_with_zeroes($params['tag'])) : 'TAG IS NULL';

  if ( isset($params['fromtimestamp']) )
    $where[] = sprintf(" SOAP_DATE >= dateadd(S, %s, '1970-01-01')",$params['fromtimestamp']);

  // range of dates
  if (isset($params['soap_date']) && is_array($params['soap_date']) && count($params['soap_date']))
      $where[] = sprintf("SOAP_DATE BETWEEN '%s' AND '%s'", $params['soap_date'][0], $params['soap_date'][1]);

  if ( ! empty($params['soap_log_id']))
  {
    $where[] = ( is_array($params['soap_log_id']) )
               ?
               "SOAP_LOG_ID IN (".implode(",",$params['soap_log_id']).")"
               :
               sprintf("SOAP_LOG_ID = %d", $params['soap_log_id']);
  }

  // sanity check
  if ( ! count($where))
    return $soap_log_data;

  // determine source table
  $table = $archive ? 'ULTRA_ACC..SOAP_LOG_ARCHIVE' : 'ULTRA_ACC..SOAP_LOG' ;

  $order = empty($params['order']) ? NULL : " ORDER BY {$params['order']}";

  $sql = "SELECT $top_clause
      SOAP_LOG_ID,
      SOAP_DATE,
      TYPE_ID,
      MSISDN,
      ICCID,
      TAG,
      SESSION_ID,
      COMMAND,
      ENV,
      RESULTCODE,
      CONVERT(varchar(max), DATA_XML ) xml
    FROM $table
    WHERE " . implode(" AND ", $where) .
    $order;

  $data = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $data && is_array($data) )
    $soap_log_data = $data;

  return $soap_log_data;
}

/**
 * soap_log_insert_query
 *
 * Query used to insert rows into soap_log or develop_soap_log
 * $params: data_xml
 *          soap_date
 *          type_id
 *          msisdn
 *          iccid
 *          tag
 *          session_id
 *          command
 *          env
 *          result_code
 * 
 * @param array $params
 * @return string
 * @author bwalters@ultra.me
 */
function soap_log_insert_query(array $params)
{
  // escape some symbols from XML
  $clean_chars = array(';' => '&#59;', "'" => '&#39;', '&' => '&amp;');
  foreach ($clean_chars as $key => $value)
    $params['data_xml'] = str_replace($key, $value, $params['data_xml']);
  
  // convert unicode characters to Java Escape (\u\d\d\d\d)
  $params['data_xml'] = convertUnicodeToJavaEscape( $params['data_xml'] );
  $params['data_xml'] = str_replace('&amp&#59;', 'and', $params['data_xml']);

  $table = 'SOAP_LOG';

  $query = sprintf(
  "INSERT INTO %s (SOAP_DATE, DATA_XML, TYPE_ID, MSISDN, ICCID, TAG, SESSION_ID, COMMAND, ENV, RESULTCODE) " .
  "VALUES ('%s', '%s', %d, %s, %s, %s, %s, %s, %s, %s)",
    $table,
    $params['soap_date'],
    $params['data_xml'],
    $params['type_id'],
    mssql_escape_with_zeroes($params['msisdn']),
    (empty($params['iccid']) ? 'NULL' : mssql_escape_with_zeroes($params['iccid'])),
    mssql_escape_with_zeroes($params['tag']),
    mssql_escape_with_zeroes($params['session_id']),
    mssql_escape_with_zeroes($params['command']),
    mssql_escape_with_zeroes($params['env']),
    (empty($params['result_code']) ? 'NULL' : sprintf('%d', $params['result_code']))
  );

  return $query;
}

/**
 * soap_log_insert_query
 *
 * a query to select soap_log entries that may be candidates
 * for propagation to alternate endpoints i.e. QA,DEV
 * 
 * @param  Integer $startAfter         ROW ID to start SELECT from
 * @param  Integer $typeId             TYPE_ID of SOAP_LOGs to SELECT
 * @param  Integer $newerThanInMinutes SELECT rows newer than GETDATE() - $newThanInMinutes
 * @return Array   of rows from SOAP_LOG
 */
function soap_log_select_for_propagation($startAfter = 0, $typeId = 3, $newerThanInMinutes = 5)
{
  $table = 'SOAP_LOG';

  $query = sprintf("
    DECLARE @START_AFTER BIGINT = %d;
    DECLARE @TYPE_ID INT = %d;
    DECLARE @NEWER_THAN_IN_MINUTES INT = %d;

    SELECT TOP 1000
      SOAP_LOG_ID,
      SOAP_DATE,
      CAST(DATA_XML AS varchar(max)) AS DATA_XML,
      TYPE_ID,
      MSISDN,
      ICCID,
      TAG,
      SESSION_ID,
      COMMAND,
      ENV,
      RESULTCODE
    FROM $table WITH (NOLOCK)
    WHERE SOAP_DATE > DATEADD(mi, -@NEWER_THAN_IN_MINUTES, GETUTCDATE())
    AND TYPE_ID = @TYPE_ID
    AND SOAP_LOG_ID > @START_AFTER",
    $startAfter,
    $typeId,
    $newerThanInMinutes
  );

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

/*
SELECT TOP 1 SOAP_LOG_ID, SOAP_DATE, TYPE_ID, MSISDN, ICCID, TAG, SESSION_ID, COMMAND, ENV, RESULTCODE, CONVERT(varchar(max), DATA_XML ) xml FROM SOAP_LOG
*/

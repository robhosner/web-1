<?php

/**
 * log_incomm_transaction
 * record inComm PIN transaction into ULTRA.INCOMM_TRANSACTIONS
 *
 * @param  integer $customer_id
 * @param  string $reference
 * @param  array $transaction
 * @return boolean
 */
function log_incomm_transaction($customer_id, $reference, $transaction)
{
  // convert inComm PT date & time to UTC datetime
  try
  {
    $date = new DateTime("{$transaction['Date']} {$transaction['Time']}");
  }
  catch(Exception $e)
  {
    dlog('', "WARNING: failed to convert date {$transaction['Date']} time {$transaction['Time']}");
    $date = new DateTime();
  }
  $date->setTimezone(new DateTimeZone('UTC'));
  $datetime = date('M d Y h:i:sA');

  $sql = sprintf("
    INSERT INTO ULTRA.INCOMM_TRANSACTIONS (
      [REFERENCE],
      [TRANSACTION_ID],
      [SOURCE],
      [CATEGORY],
      [CREATED],
      [CODE],
      [MESSAGE],
      [VALUE],
      [BALANCE],
      [SERIAL],
      [UPC],
      [MERCHANT],
      [STORE])
    SELECT TOP 1 
      %d,
      TRANSACTION_ID,
      %d,
      %s,
      '%s',
      %d,
      %s,
      %s,
      %s,
      %d,
      %d,
      %s,
      %d
    FROM HTT_BILLING_HISTORY
      WHERE CUSTOMER_ID = %d AND SOURCE = 'INCOMM' AND REFERENCE = %s
      ORDER BY TRANSACTION_ID DESC",
    $transaction['RespRefNum'],
    $transaction['SrcRefNum'],
    mssql_escape_with_zeroes($transaction['RespAction']),
    $datetime,
    $transaction['RespCode'],
    mssql_escape_with_zeroes($transaction['RespMsg']),
    $transaction['FaceValue'],
    $transaction['Balance'],
    $transaction['SerialNumber'],
    $transaction['UPC'],
    mssql_escape_with_zeroes($transaction['MerchantName']),
    $transaction['StoreID'],
    $customer_id,
    mssql_escape_with_zeroes($reference));

  return run_sql_and_check($sql);
}

?>

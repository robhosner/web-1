<?php

include_once('db.php');
include_once('lib/util-common.php');
include_once('classes/Result.php'); // Used for returning status
include_once('Ultra/Lib/Util/Redis/Brand.php');

# php component to access DB table htt_inventory_sim #

# synopsis

/*
htt_inventory_sim_update_range(
  array(
    'inventory_masteragent' => 16,
    'inventory_distributor' => 27,
    'inventory_dealer'      => 38,
    'iccid_full_from'       => 8000000000000000011,
    'iccid_full_to'         => 8000000000000000022
  )
);

echo htt_inventory_sim_select_query(
  array(
    "customer_id" => 5336
  )
)."\n\n";

echo htt_inventory_sim_select_query(
  array(
    "iccid_full"   => '1234567890123456789'
    "iccid_number" => '890126084210225639'
  )
)."\n\n";

echo htt_inventory_sim_select_query(
  array(
    "iccid_numbers" => array( '890126084210464030' , '890126084210464031')
    "iccid_numbers" => array( '890126084210225639' , '890126084210225638' )
    "iccid_numbers_full" => array( '8901260842102256386' , '8901260842102256394' )
    "iccid_full_range" => array( '8901260842102256386' , '8901260842102256394' )
  )
)."\n\n";

echo htt_inventory_sim_select_query(
  array(
    "iccid_numbers" => array( '890126084210225639' , '890126084210225638' ),
   "ShipSIMMaster" => 1
  )
)."\n\n";

echo $htt_inventory_sim_update_query = htt_inventory_sim_update_query(
  array(
    'batch_sim_set'         => array("123456789012345678"),
    'inventory_status'      => 'test_status',
    'inventory_masteragent' => 'test_masteragent',
    'sim_activated'         => 1,
    'customer_id'           => 123,
    'last_changed_date'     => 'getutcdate()',
    'last_changed_by'       => 'user',
    'last_transition_uuid'  => 'xyz123'
  )
)."\n\n";

$iccid_18 = '800000800000800011';
echo htt_inventory_sim_insert_query(
  array(
    "iccid_number" => $iccid_18,
    "imsi"         => 'xyz',
    "inventory_status" => 'AT_FOUNDRY',
    "last_changed_by"  => 'Raf testing MVNO-1601',
    "created_by"       => 'Raf testing MVNO-1601',
    "iccid_batch_id"   => 'TESTbatch',
    "iccid_full"       => luhnenize($iccid_18),
    "expires_date"     => 'NULL',
    "pin1"  => '1234',
    "puk1"  => '13994182',
    "pin2"  => '5678',
    "puk2"  => '38120550',
    "other" => 'ABCDEFGH'
  )
)."\n\n";
*/

/**
 * htt_inventory_sim_join_ultra_celluphone_channel_query
 * 
 * Joins HTT_INVENTORY_SIM with DEALER_PORTAL tables
 * 
 * @param string $iccid
 * @return string $query -- Formatted query ready for execution
 */
function htt_inventory_sim_join_ultra_celluphone_channel_query($iccid)
{
  $celluphone  = \Ultra\UltraConfig\celluphoneDb();
  $unboundQuery = "select
    s.MVNE,
    s.SIM_HOT,
    s.PRODUCT_TYPE,
    s.PIN1,
    s.PUK1,
    s.PIN2,
    s.PUK2,
    s.STORED_VALUE,
    s.BRAND_ID,
    b.ICCID_BATCH_ID,
    b.SKU,
    b.EXPIRES_DATE    as SIMBATCH_EXPIRES_DATE,
    ds.[DealerSiteID] as dealer_id,
    ds.[Dealercd]     as dealer_code,
    ds.[BusinessName] as dealer_name,
    di.[MasterID]     as dist_id,
    di.[MasterCd]     as dist_code,
    di.[BusinessName] as dist_name,
    ma.[MasterID]     as master_id,
    ma.[MasterCd]     as master_code,
    ma.[BusinessName] as master_name
    from HTT_INVENTORY_SIM s
    join HTT_INVENTORY_SIMBATCHES b           on s.ICCID_BATCH_ID = b.ICCID_BATCH_ID
    left join $celluphone..[tblMaster] ma     on s.[INVENTORY_MASTERAGENT]=ma.[MasterID]
    left join $celluphone..[tblMaster] di     on s.[INVENTORY_DISTRIBUTOR]=di.[MasterID]
    left join $celluphone..[tblDealerSite] ds on s.[INVENTORY_DEALER]=ds.[DealerSiteID]
    where s.[ICCID_FULL]='%s';";
	
  return sprintf( $unboundQuery, mssql_escape( $iccid ) );
}

/**
 * htt_inventory_sim_insert_query
 * 
 * Query used to insert rows into htt_inventory_sim
 * $params: iccid_number
 *          imsi
 *          inventory_status
 *          last_changed_by
 *          created_by
 *          iccid_batch_id
 *          iccid_full
 *          expires_date
 *          pin1
 *          puk1
 *          pin2
 *          puk2
 *          other
 *
 *    optional paraameters
 *          sim_activated
 *          inventory_masteragent
 *          inventory_distributor
 *          inventory_dealer
 *          customer_id
 *          last_changed_date
 *          created_by_date
 *          reservation_time
 *          globally_used
 *          product_type
 *          stored_value
 *          sim_hot
 *          mvne
 *          offer_id
 *          brand_id
 * 
 * @param array $params
 * @return string -- Formatted query ready for execution
 */
function htt_inventory_sim_insert_query($params)
{
  // handle optional parameters
  $optionalFields = array();
  $optionalValues = array();

  // optional parameters with default values
  $params['sim_activated'] = array_key_exists('sim_activated', $params) && $params['sim_activated'] ? 1 : 0;
  $params['last_changed_date'] = !empty($params['last_changed_date']) ? "'" . $params['last_changed_date'] . "'" : 'getutcdate()';
  $params['created_by_date'] = !empty($params['created_by_date']) ? "'" . $params['created_by_date'] . "'" : 'getutcdate()';

  $optionalStrings = array('product_type');
  foreach ($optionalStrings as $field)
  {
    if (array_key_exists($field, $params))
    {
      $optionalFields[] = strtoupper($field);
      $optionalValues[] = mssql_escape_with_zeroes($params[$field]);
    }
  }

  $optionalInts = array(
    'inventory_masteragent', 'inventory_distributor', 'inventory_dealer', 'customer_id', 'globally_used', 'stored_value', 'sim_hot',
    'mvne', 'offer_id', 'brand_id'
  );
  foreach ($optionalInts as $field)
  {
    if (array_key_exists($field, $params) && $params[$field] !== null)
    {
      $optionalFields[] = strtoupper($field);
      $optionalValues[] = intval($params[$field]);
    }
  }

  $optionalDates = array('reservation_time');
  foreach ($optionalDates as $field)
  {
    if (array_key_exists($field, $params) && $params[$field] !=  null)
    {
      $optionalFields[] = strtoupper($field);
      $optionalValues[] = "'" . $params[$field] . "'";
    }
  }

  $optionalFieldsStr = count($optionalFields) ? ',' . implode(',', $optionalFields) : null;
  $optionalValuesStr = count($optionalValues) ? ',' . implode(',', $optionalValues) : null;

  $query = sprintf(
"INSERT INTO htt_inventory_sim
 (
ICCID_NUMBER,
IMSI,
SIM_ACTIVATED,
INVENTORY_STATUS,
LAST_CHANGED_DATE,
LAST_CHANGED_BY,
CREATED_BY_DATE,
CREATED_BY,
ICCID_BATCH_ID,
ICCID_FULL,
EXPIRES_DATE,
PIN1,
PUK1,
PIN2,
PUK2,
OTHER
%s
 )
 VALUES
 (
  %s,
  %s,
  %d,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s,
  %s
  %s
 )",
    $optionalFieldsStr,
    mssql_escape($params["iccid_number"]),
    mssql_escape($params["imsi"]),
    $params['sim_activated'],
    mssql_escape($params["inventory_status"]),
    $params['last_changed_date'],
    mssql_escape($params["last_changed_by"]),
    $params['created_by_date'],
    mssql_escape($params["created_by"]),
    mssql_escape($params["iccid_batch_id"]),
    mssql_escape($params["iccid_full"]),
    $params["expires_date"],
    mssql_escape($params["pin1"]),
    mssql_escape($params["puk1"]),
    mssql_escape($params["pin2"]),
    mssql_escape($params["puk2"]),
    mssql_escape($params["other"]),
    $optionalValuesStr
  );

  return $query;
}

/**
 * htt_inventory_sim_update_range
 * $params: iccid_full_from
 *          iccid_full_to
 *          inventory_masteragent
 *          inventory_distributor
 *          inventory_dealer
 * 
 * @param array $params
 * @return boolean -- success or failure
 */
function htt_inventory_sim_update_range($params)
{
  $iccid_from = luhnenize($params['iccid_full_from']);
  $iccid_to   = luhnenize($params['iccid_full_to']);

  // sanity check: 50k max range
  if ( ( $iccid_to - $iccid_from ) > 50000 )
  {
    dlog('',"max range is 50000");
    return FALSE;
  }

  // sanity check: range not empty
  if ( $iccid_from > $iccid_to )
  {
    dlog('',"range is empty");
    return FALSE;
  }

  $sql = sprintf("
    UPDATE HTT_INVENTORY_SIM
    SET    INVENTORY_MASTERAGENT = %d,
           INVENTORY_DISTRIBUTOR = %d,
           INVENTORY_DEALER      = %d
    WHERE  ICCID_FULL BETWEEN %s AND %s ",
    $params['inventory_masteragent'],
    $params['inventory_distributor'],
    $params['inventory_dealer'],
    mssql_escape_with_zeroes($iccid_from),
    mssql_escape_with_zeroes($iccid_to)
  );

  return is_mssql_successful(logged_mssql_query($sql));
}

/**
 * htt_inventory_sim_resurrect_iccid
 *
 * @param  string $iccid
 * @param  string $user
 * @return boolean
 */
function htt_inventory_sim_resurrect_iccid( $iccid , $user=NULL )
{
  if ( ! $user )
    $user = __FUNCTION__;

  $sql = sprintf(
" UPDATE HTT_INVENTORY_SIM
  SET    GLOBALLY_USED     = 0,
         RESERVATION_TIME  = NULL,
         CUSTOMER_ID       = NULL,
         SIM_ACTIVATED     = 0,
         LAST_CHANGED_DATE = GETUTCDATE(),
         LAST_CHANGED_BY   = %s
  WHERE  ICCID_FULL        = %s
  AND    MVNE              = '2'
", mssql_escape_with_zeroes($user)
 , mssql_escape_with_zeroes(luhnenize($iccid)));

  return is_mssql_successful(logged_mssql_query($sql));
}

/**
 * htt_inventory_sim_update_query
 * 
 * Query used to update htt_inventory_sim rows
 * $params: inventory_status
 *          last_changed_by
 *          last_transition_uuid
 *          sim_activated
 *          globally_used
 *          inventory_masteragent
 *          customer_id
 *          mvne
 *          last_changed_date
 *          batch_sim_set
 * 
 * @param array $params
 * @return string -- Formatted query ready for execution
 */
function htt_inventory_sim_update_query($params)
{
  $set_clause_array = array();

  $string_params = array(
    "inventory_status",
    "last_changed_by",
    "last_transition_uuid"
  );

  $digit_params = array(
    "sim_activated",
    "globally_used",
    "inventory_masteragent",
    "customer_id",
    "mvne"
  );

  foreach( $string_params as $string_param )
    if ( isset($params[ $string_param ]) )
      $set_clause_array[] = sprintf(" $string_param = %s " , mssql_escape_with_zeroes($params[ $string_param ]));

  foreach( $digit_params as $digit_param )
    if ( isset($params[ $digit_param ]) )
      $set_clause_array[] = sprintf(" $digit_param = %d " , $params[ $digit_param ]);

  if ( isset($params["last_changed_date"]) )
    $set_clause_array[] = sprintf("last_changed_date = %s " , $params["last_changed_date"] );

  $set_clause = ' SET '.implode(" , ",$set_clause_array);

  $where_clause =
    ( strlen($params["batch_sim_set"][0]) == 18 )
    ?
    " WHERE ICCID_NUMBER IN ('".implode("','", $params["batch_sim_set"])."') "
    :
    " WHERE ICCID_FULL   IN ('".implode("','", $params["batch_sim_set"])."') "
    ;

  $query =
    "UPDATE HTT_INVENTORY_SIM
     $set_clause
     $where_clause
    ";

  return $query;
}

/**
 * htt_inventory_sim_count_query
 * 
 * Returns the count of how many SIMS we have in the range [ $iccid_start - $iccid_end ]
 * 
 * @param string $iccid_start
 * @param string $iccid_end
 * @param  boolean $is_active appends WHERE clause ' AND SIM_ACTIVATED = 1 '
 * @return integer $htt_inventory_sim_count
 */
function htt_inventory_sim_count_by_range( $iccid_start , $iccid_end , $is_active=FALSE )
{
  $htt_inventory_sim_count = '';
  $error                   = '';
  $where_clause            = '';
  
  if ( $is_active )
    $where_clause .= ' AND SIM_ACTIVATED = 1 ';
  
  $sql = sprintf("
    SELECT COUNT(*)
    FROM   HTT_INVENTORY_SIM s
    WHERE  ICCID_FULL >= %s
    AND    ICCID_FULL <= %s
    $where_clause
  ",
    mssql_escape_with_zeroes(luhnenize($iccid_start)),
    mssql_escape_with_zeroes(luhnenize($iccid_end))
  );
  
  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));
  
  if ( $query_result && is_array($query_result))
    $htt_inventory_sim_count = $query_result[0][0];
  else
    $error = "DB error: could not query HTT_INVENTORY_SIM";
  
  return array($htt_inventory_sim_count,$error);
}

/**
 * get_htt_inventory_sims_by_range
 * 
 * Returns an array of htt_inventory_sim data
 * Max output size is 10000
 */
function get_htt_inventory_sims_by_range( $iccid_start , $iccid_end, $nolock = FALSE )
{
  $sql = htt_inventory_sim_select_query(
    array(
      "sql_limit"        => 10000,
      "iccid_full_range" => array( luhnenize($iccid_start) , luhnenize($iccid_end) ),
      $nolock
    )
  );

  return mssql_fetch_all_objects ( logged_mssql_query ( $sql ) );
}

/**
 * set_master_agent_iccid_range_hotorcold
 *
 * @param int $iccid_start
 * @param int $iccid_end
 * @param int $masterAgentId 
 * @param int $hotOrCold
 * @throws Exception Invalid_Argument_Error, if $hotOrCold is not a numeric one or numeric zero
 */
function set_master_agent_iccid_range_hotorcold( $iccid_start , $iccid_end, $masterAgentId, $hotOrCold )
{
    dlog('', $hotOrCold );

    if($hotOrCold < 0 || $hotOrCold > 1  )
    {
      throw new Exception('Invalid_Argument_Error');
    }
    $query = sprintf("update HTT_INVENTORY_SIM set SIM_HOT = %d where ICCID_FULL >= %d and ICCID_FULL <= %d and INVENTORY_MASTERAGENT = %d",
      $hotOrCold,
      mssql_escape($iccid_start), 
      mssql_escape($iccid_end), 
      mssql_escape($masterAgentId));
    
    return mssql_fetch_all_objects ( logged_mssql_query ( $query ) );
}

/**
 * The ICCID numbers are inclusive in this function.
 *
 * @param int $iccid_start
 * @param int $iccid_end
 * @param int $masterAgentId
 * @param string $productType -- Typically 'ORANGE' or 'PURPLE'
 * @return multitype:object
 */
function get_htt_inventory_sims_by_range_for_masterAgentId( $iccid_start , $iccid_end, $masterAgentId, $productType )
{
    dlog('', $productType);
    
    $whereClause = sprintf("  where s.ICCID_FULL >= '%d' and s.ICCID_FULL <= '%d' and s.PRODUCT_TYPE = '%s' and s.INVENTORY_MASTERAGENT = %d ",
      mssql_escape($iccid_start), 
      mssql_escape($iccid_end), 
      $productType,
      mssql_escape($masterAgentId));
    
    $sqlQuery =
    "SELECT TOP 500 * FROM 
( (
SELECT 
LTRIM(RTRIM(s.ICCID_NUMBER)) as ICCID_NUMBER,
LTRIM(RTRIM(s.ICCID_FULL)) as ICCID_FULL,
c.plan_state as PLAN_STATE,
s.CUSTOMER_ID as CUSTOMER_ID,
s.INVENTORY_MASTERAGENT as INVENTORY_MASTERAGENT,
s.SIM_HOT as SIM_HOT,
s.SIM_ACTIVATED as SIM_ACTIVATED,
s.STORED_VALUE as STORED_VALUE
FROM htt_inventory_sim s
  inner join htt_customers_overlay_ultra c on c.customer_id = s.CUSTOMER_ID
   $whereClause
)

UNION 

(
SELECT 
LTRIM(RTRIM(s.ICCID_NUMBER)) as ICCID_NUMBER,
LTRIM(RTRIM(s.ICCID_FULL)) as ICCID_FULL,
case when SIM_ACTIVATED = 0 then ''
     when SIM_ACTIVATED = 1 then ''
end as SIM_ACTIVATED,
s.CUSTOMER_ID as CUSTOMER_ID,
s.INVENTORY_MASTERAGENT as INVENTORY_MASTERAGENT,
s.SIM_HOT as SIM_HOT,
s.SIM_ACTIVATED as SIM_ACTIVATED,
s.STORED_VALUE as STORED_VALUE
FROM htt_inventory_sim s
$whereClause
        and ICCID_NUMBER not in (
          SELECT 
            LTRIM(RTRIM(s.ICCID_NUMBER)) as ICCID_NUMBER
              FROM htt_inventory_sim s
                inner join htt_customers_overlay_ultra c on c.customer_id = s.CUSTOMER_ID
                  $whereClause
        )
)  ) ORANGE_SIM_MASTERAGENT ORDER by ICCID_NUMBER";
    
  return mssql_fetch_all_objects ( logged_mssql_query ( $sqlQuery ) );
}

/**
 * get_htt_inventory_sim_from_wildcarded_iccid
 *
 * @param  string $wildcarded_iccid
 * @return array
 */
function get_htt_inventory_sim_from_wildcarded_iccid( $wildcarded_iccid )
{
  $wildcarded_iccid = preg_replace('/[\:\;\'\"\s]+/', '', $wildcarded_iccid);

  $sql = " SELECT * FROM HTT_INVENTORY_SIM WHERE ICCID_FULL LIKE '".$wildcarded_iccid."'";

  return mssql_fetch_all_objects ( logged_mssql_query ( $sql ) );
}

/**
 * get_htt_inventory_sim_from_imsi
 *
 * @param  string $imsi
 * @return array
 */
function get_htt_inventory_sim_from_imsi( $imsi )
{
  $sql = sprintf(
    " SELECT * FROM HTT_INVENTORY_SIM WHERE IMSI = %s ",
    mssql_escape_with_zeroes($imsi)
  );

  $ICCIDs = mssql_fetch_all_objects ( logged_mssql_query ( $sql ) );
  return get_multi_month_duration($ICCIDs);
}

/**
 * htt_inventory_sim_select_query
 * 
 * Query used to select rows from htt_inventory_sim
 * $params: actcode
 *          customer_id
 *          iccid
 *          iccid_number
 *          iccid_full
 *          iccid_numbers
 *          iccid_numbers_full
 *          iccid_full_range
 *          NOT#inventory_status
 *          ShipSIMMaster
 *          MasterDealerID
 * 
 * @param array $params
 * @return string -- Formatted query ready for execution
 */
function htt_inventory_sim_select_query($params, $nolock = FALSE)
{
  $top_clause = get_sql_top_clause($params);

  $where_clause = '';

  if ( isset($params['actcode']) && $params['actcode'] )
  {
    $where_clause .= sprintf(
      " ACT_CODE = %s ",
      mssql_escape_with_zeroes($params["actcode"])
    );
  }

  if ( isset($params['customer_id']) && $params['customer_id'] )
  {
    $where_clause .= sprintf(
      " CUSTOMER_ID = %d ",
      $params['customer_id']
    );
  }

  if ( isset($params['iccid']) )
  {
    if ( strlen($params['iccid']) == 18 )
      $params['iccid_number'] = $params['iccid'];

    if ( strlen($params['iccid']) == 19 )
      $params['iccid_full'] = $params['iccid'];
  }

  if ( isset($params['iccid_number']) )
  {
    $where_clause .= sprintf(
      " iccid_number = %s",
      mssql_escape_with_zeroes($params["iccid_number"])
    );
  }
  else if ( isset($params['iccid_full']) )
  {
    $where_clause .= sprintf(
      " ICCID_FULL = %s ",
      mssql_escape_with_zeroes($params["iccid_full"])
    );
  }

  if ( isset($params['iccid_numbers']) )
  {
    $iccid_numbers_array = array();

    foreach( $params['iccid_numbers'] as $iccid_number )
    {
      $iccid_numbers_array[] = mssql_escape($iccid_number);
    }

    $iccid_numbers = implode("' , '", $iccid_numbers_array);

    $low  = min($iccid_numbers_array);
    $high = max($iccid_numbers_array);

    $where_clause .= " iccid_number in ('$iccid_numbers') and iccid_number between '$low' and '$high'";
  }

  if ( isset($params['iccid_numbers_full']) )
  {
    $iccid_numbers_array = array();

    foreach( $params['iccid_numbers_full'] as $iccid_number )
    {
      $iccid_numbers_array[] = mssql_escape($iccid_number);
    }

    $iccid_numbers = implode("' , '", $iccid_numbers_array);

    $low  = min($iccid_numbers_array);
    $high = max($iccid_numbers_array);

    $where_clause .= " ICCID_FULL in ('$iccid_numbers') and ICCID_FULL between '$low' and '$high'";
  }

  if ( isset($params['iccid_full_range']) )
  {
    $low  = min($params['iccid_full_range']);
    $high = max($params['iccid_full_range']);

    $where_clause .= sprintf(
      " ICCID_FULL BETWEEN %s AND %s ",
      mssql_escape_with_zeroes($low),
      mssql_escape_with_zeroes($high)
    );
  }

  if ( isset($params['NOT#inventory_status']) )
  {
    $where_clause .= ' AND '.sprintf(
      "inventory_status != %s",
      mssql_escape($params['NOT#inventory_status'])
    );
  }

  if ( isset($params['ShipSIMMaster']) )
  {
    # ShipSIMMaster condition fails if:
    # status = AT_CELLUPHONE, or they have a status = SHIPPED_MASTER with a LAST_CHANGED_DATE within an hour of now

    $where_clause .= 
      ' AND inventory_status != "AT_CELLUPHONE" '.
      ' AND ( inventory_status != "SHIPPED_MASTER" OR ( inventory_status = "SHIPPED_MASTER" AND DATEDIFF(mi, LAST_CHANGED_DATE, GETUTCDATE()) > 60 ) ) ';
  }
  

  if ( isset($params['MasterDealerID']) )
  {  
    $masterAgentID = mssql_escape( $params['MasterDealerID'] );
    $where_clause .= sprintf(' AND INVENTORY_MASTERAGENT = %d;', $masterAgentID );
  }

  // check if WHERE clause is present: we refuse to return the entire table
  if (! strlen($where_clause))
    return NULL;
  $where_clause = ' WHERE ' . $where_clause;

  $withNoLock = ($nolock) ? ' WITH (nolock) ' : '';

  $query =
    "SELECT $top_clause
      LTRIM(RTRIM(ICCID_NUMBER)) ICCID_NUMBER,
      IMSI,
      SIM_ACTIVATED,
      INVENTORY_MASTERAGENT,
      INVENTORY_DISTRIBUTOR,
      INVENTORY_DEALER,
      CUSTOMER_ID,
      INVENTORY_STATUS,
      LAST_CHANGED_DATE,
      LAST_CHANGED_BY,
      CREATED_BY_DATE,
      CREATED_BY,
      ICCID_BATCH_ID,
      LTRIM(RTRIM(ICCID_FULL)) ICCID_FULL,
      EXPIRES_DATE,
      LAST_TRANSITION_UUID,
      PIN1,
      PUK1,
      PIN2,
      PUK2,
      OTHER,
      RESERVATION_TIME,
      GLOBALLY_USED,
      ACT_CODE,
      PRODUCT_TYPE,
      STORED_VALUE,
      SIM_HOT,
      MVNE,
      CASE WHEN ( EXPIRES_DATE IS NOT NULL AND EXPIRES_DATE <= GETUTCDATE() )
           THEN 1
           ELSE 0
      END is_expired,
      OFFER_ID,
      BRAND_ID
    FROM htt_inventory_sim $withNoLock
    $where_clause
    ORDER by reservation_time"; // NOTE: NULL reservation times come first!

  return $query;
}

/**
 * globally_used_update_query
 * 
 * @return string SQL
 */
function globally_used_update_query()
{
  return "update HTT_INVENTORY_SIM set [GLOBALLY_USED]=1 where [SIM_ACTIVATED]=1 and GLOBALLY_USED=0;
  UPDATE a SET a.[GLOBALLY_USED]=1
  FROM HTT_INVENTORY_SIM AS a
  INNER JOIN ULTRA_DEVELOP_TEL..[HTT_INVENTORY_SIM] AS b
  ON  a.[ICCID_NUMBER]=b.[ICCID_NUMBER]
  WHERE b.[SIM_ACTIVATED]=1 and a.[GLOBALLY_USED]=0";
}

/**
 * get_htt_inventory_sim_from_actcode
 *
 * Provides HTT_INVENTORY_SIM data, given an ACTCODE
 *
 * @param  string $actcode     -- 11 digits ACTCODE
 * @return object[]
 */
function get_htt_inventory_sim_from_actcode( $actcode )
{
  $query = htt_inventory_sim_select_query(array('actcode' => $actcode));
  $ICCIDs = mssql_fetch_all_objects ( logged_mssql_query ( $query ) );
  return get_multi_month_duration($ICCIDs);
}

/**
 * get_htt_inventory_sim_from_iccid
 *
 * get one table row for the given ICCID
 *
 * @param  string $iccid
 * @return array || NULL
 */
function get_htt_inventory_sim_from_iccid($iccid)
{
  if (empty($iccid))
    return NULL;

  $query = htt_inventory_sim_select_query(array("iccid" => $iccid));
  $data  = mssql_fetch_all_objects(logged_mssql_query($query));
  $data = get_multi_month_duration($data);
  return ( is_array($data) && count($data) ) ? $data[0] : NULL;
}


/**
 * get_product_type_from_iccid
 *
 * Provides HTT_INVENTORY_SIM.PRODUCT_TYPE, given an ICCID
 *
 * @param  string $iccid -- 18 or 19 digits ICCID
 * @return string $product_type
 */
function get_product_type_from_iccid( $iccid )
{
  $product_type = '';

  if ( ! $iccid )
    return $product_type;

  $query = htt_inventory_sim_select_query(
    array(
      "iccid" => $iccid
    )
  );

  $data = mssql_fetch_all_objects ( logged_mssql_query ( $query ) );

  if ( $data && is_array($data) && count($data) && $data[0]->PRODUCT_TYPE )
  {
    $product_type = $data[0]->PRODUCT_TYPE;
  }

  return $product_type;
}

/**
 * get_act_code_from_iccid
 *
 * Provides HTT_INVENTORY_SIM.ACT_CODE, given an ICCID
 *
 * @param  string $iccid -- 18 or 19 digits ACTIVATION ICCID
 * @return string $act_code
 */
function get_act_code_from_iccid( $iccid )
{
  $act_code = '';

  $query = htt_inventory_sim_select_query(
    array(
      "iccid" => $iccid
    )
  );

  $data = mssql_fetch_all_objects ( logged_mssql_query ( $query ) );

  if ( $data && is_array($data) && count($data) && $data[0]->ACT_CODE )
  {
    $act_code = $data[0]->ACT_CODE;
  }

  return $act_code;
}

/**
 * validate_sim_product_type
 * 
 * The function queries the table HTT_INVENTORY_SIM by $iccid and check the PRODUCT_TYPE value.
 * The result will be successful if PRODUCT_TYPE is equal to the $product_type given in input.
 * The result will be a failure otherwise.
 *
 * @param  string $product_type
 * @param  string $iccid -- 18 or 19 digits ICCID
 * @return object $result Result
 */
function validate_sim_product_type($product_type, 
                                   $iccid )
{
  dlog('',"product_type = $product_type ; iccid = $iccid");

  // object to return to caller on the disposition of the call
  $functionDisposition = new Result();

  // Make sure we have a valid 18 or 19 character length ICCID
  if ( strlen($iccid) == 18 || strlen($iccid) == 19 )
  {
    $iccid = luhnenize($iccid);

    $query = sprintf( "SELECT product_type FROM HTT_INVENTORY_SIM WHERE ICCID_FULL = %s" , mssql_escape_with_zeroes($iccid) );

    $rowSet = mssql_fetch_all_objects ( logged_mssql_query ( $query ) );

    if ( ( ! $rowSet ) || ( ! is_array($rowSet) ) )
    {
      $msg = sprintf("ERR_API_INTERNAL: DB error");
      $functionDisposition->add_error($msg);
    }
    // Not there, report an error, bad ICCID or just not in the database
    elseif ( ! count($rowSet) )
    {
      $msg = sprintf("ERR_API_INVALID_ARGUMENTS: ICCID not found, ICCID=%s.", $iccid );
      $functionDisposition->add_error($msg);
    }
    else
    {
      // We got back some data, from our query, so lets check to see if the database product type matches our 
      // validation request.
      if( strcasecmp( $product_type, $rowSet[0]->product_type ) == 0 )
      {
        // All good, ICCID found a row, and the product type matched, Success!
        $functionDisposition->succeed();
      }
      else
      {
        // Product types do not match
        dlog('',"ERR_API_INVALID_ARGUMENTS: Product types do not match. Database Value=%s <> Product Specified=%s for ICCID=%s.",$rowSet[0]->product_type , $product_type, $iccid);
        $msg = "ERR_API_INVALID_ARGUMENTS: Incorrect ICCID Product type";
        $functionDisposition->add_error($msg);
      }      
    }
  }
  else
  {
    $functionDisposition->add_error("ERR_API_INVALID_ARGUMENTS: ICCID is not 18 or 19 characters in length.");
  }

  return $functionDisposition;
}

/**
 * htt_inventory_sim_get_iccid_user_status 
 * @param  string $ICCID
 * @return object $result Result
 */
function htt_inventory_sim_get_iccid_user_status($ICCID)
{
  $result = new Result();

  // check for 19 digits ICCID
  $htt_inventory_sim_select_query = htt_inventory_sim_select_query( array( "iccid_full" => $ICCID ) );

  $htt_inventory_sim_data = \Ultra\Lib\DB\fetch_objects($htt_inventory_sim_select_query);

  if ( ! ( $htt_inventory_sim_data && is_array($htt_inventory_sim_data) && count($htt_inventory_sim_data) ) )
    $result->add_error('ICCID not found.');
  else
  {
    $result->add_data_array(array('status' => get_ICCID_user_status($ICCID)));
    $result->succeed();
  }

  return $result;
}


/**
 * get_multi_month_duration
 * looked multi month duration for given ICCIDs and add as property
 * @param Array of Objects as retrieved from HTT_INVENTORY_SIM
 * @return Array of Objects with added property DURATION
 */
function get_multi_month_duration($ICCIDs)
{
  foreach ($ICCIDs as $ICCID)
  {
    // get and cache actual duration if SIM has offer set
    if ( ! empty($ICCID->OFFER_ID))
    {
      $key = 'multi_month/duration/' . $ICCID->OFFER_ID;
      if ( ! $ICCID->DURATION = $duration = \Ultra\Lib\Util\redis_read($key))
      {
        if ( ! $sql = \Ultra\Lib\DB\makeSelectQuery('ULTRA.INVENTORY_SIM_OFFERS', 1, 'DURATION', array('OFFER_ID' => $ICCID->OFFER_ID)))
          dlog('', 'ERROR: failed to generate SQL');
        else
        {
          $offers = mssql_fetch_all_objects(logged_mssql_query($sql));
          if (count($offers) && ! empty($offers[0]->DURATION))
          {
            $ICCID->DURATION = $offers[0]->DURATION;
            \Ultra\Lib\Util\redis_write($key , $offers[0]->DURATION, TTL_ONE_DAY);
          }
        }
      }
    }

    // pre-MRP behavior and error default: duration is 1 month
    if (empty($ICCID->DURATION))
      $ICCID->DURATION = 1;
  }

  return $ICCIDs;
}

/**
 * is_multi_month_sim
 * @param  String  $iccid
 * @return Boolean
 */
function is_multi_month_sim($iccid)
{
  $sim = get_htt_inventory_sim_from_iccid($iccid);

  if ($sim)
  {
    if ( ! empty($sim->STORED_VALUE) && $sim->DURATION > 1 )
      return TRUE;
  }

  return FALSE;
}

/**
 * get_brand_id_from_iccid
 *
 * Given and ICCID, returns the BRAND_ID associated to it
 *
 * @param  String  $iccid_full
 * @return Integer $brand_id
 */
function get_brand_id_from_iccid($iccid_full)
{
  if ( empty($iccid_full) )
    return NULL;

  $iccid_full = luhnenize($iccid_full);

  $brand_id = \Ultra\Lib\Util\Redis\getBrandSIM( $iccid_full );

  if ( empty($brand_id) )
  {
    $query = sprintf('
      DECLARE @ICCID_FULL VARCHAR(19) = \'%s\';
      SELECT BRAND_ID FROM HTT_INVENTORY_SIM WITH (NOLOCK)
      WHERE ICCID_FULL = @ICCID_FULL',
      $iccid_full
    );

    $iccid = find_first($query);
    if ($iccid && ! empty($iccid->BRAND_ID))
      $brand_id = $iccid->BRAND_ID;
  }

  if ( ! empty($brand_id) )
    \Ultra\Lib\Util\Redis\setBrandSIM( $iccid_full , $brand_id );

  return $brand_id;
}

/**
 * get_brand_id_from_msisdn
 *
 * Given a MSISDN, returns the BRAND_ID associated to it
 *
 * @param  String  $msisdn
 * @return Integer $brand_id
 */
function get_brand_id_from_msisdn($msisdn)
{
  if ( empty($msisdn) )
    return NULL;

  $brand_id = \Ultra\Lib\Util\Redis\getBrandMSISDN($msisdn);

  if ( empty($brand_id) )
  {
    $query = sprintf('
      SELECT BRAND_ID
      FROM   HTT_CUSTOMERS_OVERLAY_ULTRA
      WHERE  CURRENT_MOBILE_NUMBER = %s ',
      mssql_escape_with_zeroes($msisdn)
    );

    $data = find_first($query);
    if ($data && ! empty($data->BRAND_ID))
      $brand_id = $data->BRAND_ID;
  }

  if ( ! empty($brand_id) )
    \Ultra\Lib\Util\Redis\setBrandMSISDN( $msisdn , $brand_id );

  return $brand_id;
}


<?php

# php component to access DB table accounts #

# synopsis

# payments::funcAddBalance
# echo accounts_update_query( array('add_to_balance'=>'100','customer_id'=>'123','last_recharge_date_time'=>'now') )."\n\n";

# payments::funcSweepStoredValue
# echo accounts_update_query( array('add_to_balance'=>'100','customer_id'=>'123') )."\n\n";

# payments::funcAddILDMinutes
# echo accounts_update_query( array('add_to_package_balance1'=>'10','customer_id'=>'123') )."\n\n";

# echo accounts_update_query( array('cos_id' => 98274,'customer_id'=>'123') )."\n\n";

# $customer = get_account_from_customer_id(552,array('CUSTOMER_ID','BALANCE'));

# $customer = get_account_from_customer_id( 1915 , array('COS_ID') );

/**
 * get_customer_plan 
 * @param  integer $customer_id
 * @return array (cos_id, plan) || NULL
 */
function get_customer_plan($customer_id)
{
  $plan_data = NULL;

  $sql = accounts_select_query(
    array(
      'select_fields' => array('cos_id'),
      'customer_id'   => $customer_id
    )
  );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
    $plan_data = array(
      'cos_id' => $query_result[0][0],
      'plan'   => get_plan_from_cos_id( $query_result[0][0] )
    );

  return $plan_data;
}

/**
 * get_balance_by_customer_id
 *
 * Given a customer id, return the customer's balance
 *
 * @param  integer $customer_id
 * @return integer $balance || NULL
 */
function get_balance_by_customer_id($customer_id)
{
  $sql = accounts_select_query(
    array(
      'select_fields' => array('balance'),
      'customer_id'   => $customer_id
    )
  );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
    return $query_result[0][0];

  return NULL;
}

/**
 * accounts_update_query
 * $params: subtract_from_balance
 *          add_to_balance
 *          last_recharge_date_time
 *          add_to_package_balance1
 *          packaged_balance1
 *          cos_id
 *          ultra_zero_reset
 *          zero_used_minutes
 *          customer_id
 * 
 * @param  array $params
 * @return string SQL
 */
function accounts_update_query($params)
{
  $set_clause_array = array();

  if ( isset($params["subtract_from_balance"]) )
  {
    $set_clause_array[] = sprintf(
      "balance = balance - %s",
      $params["subtract_from_balance"]
    );
  }

  if ( isset($params["add_to_balance"]) )
  {
    $set_clause_array[] = sprintf(
      "balance = balance + %s",
      $params["add_to_balance"]
    );
  }

  if (
    ( isset($params["last_recharge_date_time"]) )
    &&
    ( $params["last_recharge_date_time"] == 'now' )
  )
  {
    $set_clause_array[] = "last_recharge_date_time = getdate()";
  }

  if ( isset($params["add_to_package_balance1"]) )
  {
    $set_clause_array[] = sprintf(
      "packaged_balance1 = packaged_balance1 + ( %s * 100 * 60 )",
      $params["add_to_package_balance1"]
    );
  }

  if ( isset($params["add_to_package_balance2"]) )
  {
    $set_clause_array[] = sprintf(
      "packaged_balance2 = packaged_balance2 + ( %s * 100 * 60 )",
      $params["add_to_package_balance2"]
    );
  }

  if ( array_key_exists( 'packaged_balance1', $params) )
  {
    $set_clause_array[] = sprintf(
      "packaged_balance1 = %d",
      $params["packaged_balance1"]
    );
  }

  if ( array_key_exists('packaged_balance3', $params) )
  {
    $set_clause_array[] = sprintf(
      "packaged_balance3 = %d",
      $params["packaged_balance3"]
    );
  }

  if ( isset($params["cos_id"]) )
  {
    $set_clause_array[] = 'cos_id = '.$params["cos_id"];
  }

  if ( isset($params["ultra_zero_reset"]) && $params["ultra_zero_reset"] )
  {
    $set_clause_array[] = ' PERIOD_MINUTES_TO_DATE_BILLED = ' . $params['zero_used_minutes'];
    $set_clause_array[] = ' PERIOD_MINUTES_TO_DATE_ACTUAL = ' . $params['zero_used_minutes'];
    $set_clause_array[] = " PERIOD_CALLS_TO_DATE          = 0 ";
    $set_clause_array[] = " PERIOD_LAST_RESET_DATE_TIME   = GETUTCDATE() ";
  }

  $set_clause = implode(" , ", $set_clause_array);

  $query = sprintf(
    "UPDATE accounts
     SET $set_clause
     WHERE CUSTOMER_ID = %d",
    $params['customer_id']
  );

  return $query;
}

/**
 * func_reset_zero_minutes 
 * @param  integer $customer_id
 * @param  integer $cos_id
 * @return array (customer_id, cos_id)
 */
function func_reset_zero_minutes($customer_id, $cos_id)
{
  $result = array( 'success' => FALSE , 'errors' => array());

  $plan_name = get_plan_name_from_short_name(get_plan_from_cos_id($cos_id));

  $zero_used_minutes = \Ultra\UltraConfig\zeroUsedMinutes($plan_name);
  if (! $zero_used_minutes) // if not L19
    $zero_used_minutes = 0;

  $query = accounts_update_query(
    array(
      'customer_id'       => $customer_id,
      'ultra_zero_reset'  => TRUE,
      'zero_used_minutes' => $zero_used_minutes));

  if ( run_sql_and_check($query) )
    $result['success'] = TRUE;
  else
    $result['errors'][] = 'Cannot update ACCOUNTS in func_reset_zero_minutes';

  return $result;
}

/**
 * func_set_minutes
 *
 * Amounts are in cents
 *
 * @param  integer $customer_id
 * @param  integer $intl_minutes
 * @param  integer $unlimited_intl_minutes
 * @return array (errors=>[], success=>boolean)
 */
function func_set_minutes($customer_id,$intl_minutes,$unlimited_intl_minutes)
{
  $return = array( 'errors' => array() , 'success' => FALSE );

  if ($subplan = get_brand_uv_subplan($customer_id))
  {
    \logit("CUSTOMER_ID $customer_id in subplan $subplan");

    if ($account = get_account_from_customer_id($customer_id, array('COS_ID')))
    {
      \logit("old values intl_minutes $intl_minutes , unlimited_intl_minutes $unlimited_intl_minutes");

      if ($minutes = \Ultra\UltraConfig\getMinutesBySubplan(get_plan_from_cos_id($account->COS_ID), $subplan))
      {
        $intl_minutes           = $minutes[0];
        $unlimited_intl_minutes = $minutes[1];
      }

      \logit("new values intl_minutes $intl_minutes , unlimited_intl_minutes $unlimited_intl_minutes");
    }
    else
      \logError("DB error on ACCOUNTS for CUSTOMER_ID $customer_id");
  }

  if ( empty( $intl_minutes ) )
    $intl_minutes = 0;

  if ( empty( $unlimited_intl_minutes ) )
    $unlimited_intl_minutes = 0;

  $sql = sprintf("
          UPDATE accounts
          SET    packaged_balance1 = %d,
                 packaged_balance2 = %d
          WHERE  customer_id       = %d",
    ($intl_minutes*600),
    ($unlimited_intl_minutes*600),
    $customer_id
  );

  $check = run_sql_and_check($sql);

  if ( $check )
    $return['success'] = TRUE;
  else
    $return['errors'][] = "Error while updating htt_customers_overlay_ultra.";

  return $return;
}

/**
 * accounts_select_query
 * $params: select_fields
 *          account_id
 *          customer_id
 *          customer_id_list
 * 
 * @param  array $params
 * @return string SQL
 */
function accounts_select_query($params)
{
  $select_fields = '*';

  $top_clause = get_sql_top_clause($params);

  $where = array();

  if ( isset($params['select_fields']) )
  {
    $select_fields = implode(',',$params['select_fields']);
  }

  if ( isset($params['account_id']) )
  {
    $where[] = sprintf("account_id = %s",mssql_escape($params['account_id']));
  }

  if ( isset($params['customer_id']) )
  {
    $where[] = "customer_id = ".$params['customer_id'];
  }

  if ( isset($params['customer_id_list']) && is_array($params['customer_id_list']) && count($params['customer_id_list']) )
  {
    $where[] = "customer_id in (".implode(",",$params['customer_id_list']).") ";
  }

  $where_clause = ' WHERE '.implode(" AND ", $where);

  $query =
    "SELECT $top_clause $select_fields
     FROM accounts $where_clause";

  return $query;
}

/**
 * get_account_from_customer_id 
 * @param  integer $customer_id
 * @param  array $select_fields
 * @return object $customer
 */
function get_account_from_customer_id($customer_id, $select_fields = NULL)
{
  $customer = NULL;

  $accounts_select_query = accounts_select_query(
    array(
      'select_fields' => $select_fields,
      'customer_id'  => $customer_id
    )
  );

  $account_data = mssql_fetch_all_objects(logged_mssql_query($accounts_select_query));

  if ( $account_data && is_array($account_data) && count($account_data) )
  {
    $customer = $account_data[0];
  }

  return $customer;
}

/**
 * accounts_zero_credit_limit_select_query 
 * @return string SQL
 */
function accounts_zero_credit_limit_select_query()
{
  return "SELECT account
FROM accounts a, parent_cos pc
WHERE a.cos_id = pc.cos_id
AND a." . account_enabled_clause(TRUE) . " AND a.balance = 0 AND a.credit_limit > 0 AND pc.site='IndiaLD'
AND a.cos_id NOT IN ('".COSID_QUARTER_CALLS."','".COSID_QUARTER_CALLS_T."','".COSID_PAYG_MINUTES."','".COSID_PAYG_MINUTES_T."','".COSID_1_5_CALLING_CARD."','".COSID_1_5_CALLING_CARD_T."')";
}

/**
 * accounts_zero_credit_limit_update_query 
 * @return string SQL
 */
function accounts_zero_credit_limit_update_query()
{
  return "UPDATE accounts
SET credit_limit = 0
WHERE account IN (".accounts_zero_credit_limit_select_query().")";
}

/**
 * internal_func_midcycle_packaged_balance_check
 *
 * Change Plan Mid-Cycle L49 to L59 (change plan not during monthly renewal)
 * If packaged_balance1 >300000 ($5) then the value needs to be set to 300000
 *
 * @param  integer $customer_id
 * @return array (errors=>[], success=>boolean)
 */
function internal_func_midcycle_packaged_balance_check($customer_id)
{
  $return = array( 'errors' => array() , 'success' => TRUE );

  $sql = accounts_select_query(
    array(
      'select_fields' => array('PACKAGED_BALANCE1'),
      'customer_id'   => $customer_id
    )
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));
 
  // If packaged_balance1 > 300000 ($5) then the value needs to be set to 300000
  if ( $query_result && is_array($query_result) && count($query_result) && ( $query_result[0]->PACKAGED_BALANCE1 > 300000 ) )
  {
    dlog('',"query_result = %s",$query_result);

    $sql = accounts_update_query(
      array(
        'packaged_balance1' => 300000,
        'customer_id'       => $customer_id
      )
    );

    if ( ! run_sql_and_check($sql) )
      dlog('',"PACKAGED_BALANCE1 update failed");
  }

  return $return;
}


/**
 * account_enabled_clause
 * return SQL SELECT clause for ACCOUNTS.ENABLED or ACCOUNTS.ACCOUNT_STATUS_TYPE depending on TSIP environment version
 * @see PROD-2332
 * @param Boolean TRUE for account enabled, FALSE for disabled
 * @return String SQL clause for ACCOUNTS table
 */
function account_enabled_clause($enabled)
{
  if (\Ultra\UltraConfig\getTSIPDBVersion() == '3.12')
    return $enabled ? 'ACCOUNT_STATUS_TYPE != ' . ACCOUNT_STATUS_TYPE_DEACTIVATED : 'ACCOUNT_STATUS_TYPE = ' . ACCOUNT_STATUS_TYPE_DEACTIVATED;
  else
    return $enabled ? 'ENABLED = 1' : 'ENABLED = 0';
}


/**
 * account_enabled_assignment
 * return SQL SET assignment for ACCOUNTS.ENABLED or ACCOUNTS.ACCOUNT_STATUS_TYPE depending on TSIP environment version
 * @see PROD-2332
 * @param Boolean TRUE to enable account, FALSE to disable (TSIP v3.11); Integer account status type (TSIP v3.12)
 * @return String part of SQL SET statement for ACCOUNTS table
 */
function account_enabled_assignment($value)
{
  // TSIP v3.12
  if (\Ultra\UltraConfig\getTSIPDBVersion() == '3.12')
  {
    // compensate for allowed (but incorrect) legacy usage
    if (is_bool($value))
      $value = $value ? ACCOUNT_STATUS_TYPE_ACTIVE : ACCOUNT_STATUS_TYPE_DEACTIVATED;

    // allowed account status values defined in cosid_constants.php
    $allowed = array(
      ACCOUNT_STATUS_TYPE_PROVISIONED,
      ACCOUNT_STATUS_TYPE_IN_TRANSIT,
      ACCOUNT_STATUS_TYPE_IN_STOCK,
      ACCOUNT_STATUS_TYPE_ACTIVE,
      ACCOUNT_STATUS_TYPE_ACTIVATED,
      ACCOUNT_STATUS_TYPE_EXPIRED,
      ACCOUNT_STATUS_TYPE_SUSPENDED,
      ACCOUNT_STATUS_TYPE_DEACTIVATED);

    // check input
    if ( ! in_array($value, $allowed))
      return logError("invalid ACCOUNT_STATUS_TYPE value '$value'");

    return "ACCOUNT_STATUS_TYPE = $value";
  }

  // TSIP legacy
  else
    return $value ? 'ENABLED = 1' : 'ENABLED = 0';
}

/**
 * accounts_clone
 * clones ACCOUNTS row data for one customer_id to another
 * @param  Integer $from_customer_id
 * @param  Integer $to_customer_id
 * @param  Integer $fields_to_copy row columns to copy
 * @return Result
 */
function accounts_clone_by_customer_id($from_customer_id, $to_customer_id, $fields_to_copy)
{
  $result = new \Result();

  $sql = "SELECT TOP 1 " . implode(',', $fields_to_copy) . " FROM ACCOUNTS WITH (NOLOCK) WHERE CUSTOMER_ID = $from_customer_id";
  $from_row = find_first($sql);

  if (!$from_row)
  {
    $result->add_error("ACCOUNTS row doesn't not exist for CUSTOMER_ID $from_customer_id");
    return $result;
  }

  $i = 0;
  $values = "";
  foreach ($fields_to_copy as $field)
  {
    if (empty($from_row->$field))
      continue;

    if ($i) $values .= ',';
    $i++;

    $values .= "$field = '" . $from_row->$field . "'";
  }

  $sql = "UPDATE ACCOUNTS SET $values WHERE CUSTOMER_ID = $to_customer_id";

  if (! run_sql_and_check($sql))
  {
    $result->add_error("Could not update ACCOUNTS for CUSTOMER_ID $from_customer_id");
    return $result;
  }

  $result->succeed();
  return $result;
}

/**
 * accounts_clear_balance
 * set BALANCE col in ACCOUNTS to 0
 * @param  Integer $customer_id
 * @return Result
 */
function accounts_clear_balance($customer_id)
{
  $result = array( 'errors' => array(), 'success' => TRUE );

  $sql = sprintf('UPDATE ACCOUNTS SET BALANCE = 0 WHERE CUSTOMER_ID = %d', $customer_id);

  if ( run_sql_and_check($sql) )
    $result['success'] = TRUE;
  else
    $result['errors'][] = 'Cannot update ACCOUNTS in accounts_clear_balance';

  return $result;
}

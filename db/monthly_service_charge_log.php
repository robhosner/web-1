<?php

# php component to access DB table HTT_MONTHLY_SERVICE_CHARGE_LOG

/**
 * fix_monthly_charge_zombies description
 * delete HTT_MONTHLY_SERVICE_CHARGE_LOG rows in RESERVED status for too log ( 1 hour )
 */
function fix_monthly_charge_zombies()
{
  $sql =
    " DELETE FROM HTT_MONTHLY_SERVICE_CHARGE_LOG
      WHERE  STATUS = 'RESERVED'
      AND    CREATED_DATETIME <=  DATEADD( ss , - 60*60 , GETUTCDATE() ) ";

  if ( ! run_sql_and_check($sql) )
    dlog('',"fix_monthly_charge_zombies failed");
}

/**
 * monthly_service_charge_log_lock
 * attempt to lock a customer
 * 
 * @param  integer $customer_id
 * @return array $result
 */
function monthly_service_charge_log_lock($customer_id)
{
  $result = array(
    'lock_id'       => FALSE,
    'attempt_count' => 0
  );

  $max_attempt = 3;

  // check if we should retry this customer or attempt a new lock

  $sql = sprintf("
    SELECT ATTEMPT_COUNT,
           DATEDIFF( mi , NEXT_ATTEMPT_DATETIME , getutcdate() ) minutes_ago,
           NEXT_ATTEMPT_DATETIME,
           STATUS
    FROM   HTT_MONTHLY_SERVICE_CHARGE_LOG l
    WHERE  CUSTOMER_ID = %d
    AND    (STATUS = 'ERROR' OR STATUS = 'RETRY_CC' OR STATUS = 'TODO')
    AND    DATEDIFF( dd , l.CREATED_DATETIME , getutcdate() ) < 2 -- yesterday or today
    AND    NEXT_ATTEMPT_DATETIME IS NOT NULL
    AND    ATTEMPT_COUNT < $max_attempt ",
    $customer_id
  );

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && ( count($query_result) == 1 ) && $query_result[0]->minutes_ago > 0)
  {
    // we should retry this customer

    $result['lock_id']       = monthly_service_charge_log_relock($customer_id);
    $result['attempt_count'] = $query_result[0]->ATTEMPT_COUNT + 1;
    $result['status']        = $query_result[0]->STATUS;
  }
  elseif ( $query_result && is_array($query_result) && ( count($query_result) == 1 ) )
  {
    // we should NOT retry this customer yet, since NEXT_ATTEMPT_DATETIME is in the future

    dlog('',"We should NOT retry this customer yet, since NEXT_ATTEMPT_DATETIME is in the future : ".$query_result[0]->NEXT_ATTEMPT_DATETIME);
  }
  elseif ( $query_result && is_array($query_result) && ( count($query_result) > 1 ) )
  {
    // HTT_MONTHLY_SERVICE_CHARGE_LOG consistency error

    dlog('',"HTT_MONTHLY_SERVICE_CHARGE_LOG is inconsistent for customer id $customer_id");
  }
  else
  {
    // we should attempt a new lock

    $result['lock_id']       = monthly_service_charge_log_new_lock($customer_id);
    $result['attempt_count'] = 1;
  }

  return $result;
}

/**
 * monthly_service_charge_log_relock
 * attempt a retry lock
 * 
 * @param  integer $customer_id
 * @return boolean $locked
 */
function monthly_service_charge_log_relock($customer_id)
{
  if ( ! $customer_id ) { return FALSE; }

  $locked = FALSE;

  // lock attempt

  $sql = sprintf("
    UPDATE HTT_MONTHLY_SERVICE_CHARGE_LOG
    SET    STATUS = 'RESERVED',
           PROCESS_ID = %d
    WHERE  CUSTOMER_ID = %d
    AND    (STATUS = 'ERROR' OR STATUS = 'RETRY_CC' OR STATUS = 'TODO')
    AND    NEXT_ATTEMPT_DATETIME IS NOT NULL
    ",
    getmypid(),
    $customer_id
  );

  if ( is_mssql_successful(logged_mssql_query($sql)) )
  {
    // confirm lock
    $locked = monthly_service_charge_confirm_lock($customer_id,TRUE);
  }
  else { dlog('',"Error after $sql"); }

  return $locked;
}

/**
 * monthly_service_charge_log_confirm_lock_query
 * @param  integer $customer_id
 * @return string SQL
 */
function monthly_service_charge_log_confirm_lock_query($customer_id)
{
  $sql = sprintf("
    SELECT PROCESS_ID, HTT_MONTHLY_SERVICE_CHARGE_LOG_ID
    FROM   HTT_MONTHLY_SERVICE_CHARGE_LOG with (nolock)
    WHERE  CUSTOMER_ID = %d
    AND    STATUS = 'RESERVED' ",
    $customer_id
  );

  return $sql;
}

/**
 * monthly_service_charge_log_clean_lock_query
 * @param  integer $customer_id
 * @return string SQL
 */
function monthly_service_charge_log_clean_lock_query($customer_id)
{
  $sql = sprintf("
    DELETE FROM HTT_MONTHLY_SERVICE_CHARGE_LOG
    WHERE  CUSTOMER_ID = %d
    AND    STATUS      = 'RESERVED'
    AND    PROCESS_ID  = %d ",
    $customer_id,
    getmypid()
  );

  return $sql;
}

/**
 * monthly_service_charge_log_new_lock
 * attempt to lock a customer
 * 
 * @param  integer $customer_id
 * @return boolean $locked
 */
function monthly_service_charge_log_new_lock($customer_id)
{
  if ( ! $customer_id ) { return FALSE; }

  $locked = FALSE;

  // lock attempt

  $sql = sprintf("
    INSERT INTO HTT_MONTHLY_SERVICE_CHARGE_LOG
    (
      ATTEMPT_COUNT,
      PROCESS_ID,
      CUSTOMER_ID
    ) VALUES (
      0,
      %d,
      %d
    )
    ",
    getmypid(),
    $customer_id
  );

  if ( is_mssql_successful(logged_mssql_query($sql)) )
  {
    // confirm lock
    $locked = monthly_service_charge_confirm_lock($customer_id);
  }
  else { dlog('',"Error after $sql"); }

  return $locked;
}

/**
 * monthly_service_charge_confirm_lock
 * 
 * @param  integer  $customer_id
 * @param  boolean $relock
 * @return boolean $locked
 */
function monthly_service_charge_confirm_lock($customer_id,$relock=FALSE)
{
  $locked = FALSE;

  $sql = monthly_service_charge_log_confirm_lock_query($customer_id);

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && ( count($query_result) == 1 ) )
  {
    if ( $query_result[0][0] === getmypid() )
    {
      $locked = $query_result[0][1];
    }
  }
  else { dlog('',"Lock failed in monthly_service_charge_confirm_lock for customer id $customer_id"); }

  if ( ( ! $locked ) && ( ! $relock ) )
  {
    $sql = monthly_service_charge_log_clean_lock_query($customer_id);

    if ( ! is_mssql_successful(logged_mssql_query($sql)) )
    { dlog('',"Error after $sql"); }
  }

  return $locked;
}

/**
 * monthly_service_charge_log_update
 * @param  integer $customer_id
 * @param  string $status
 * @param  string $next_attempt_datetime
 */
function monthly_service_charge_log_update($customer_id,$status,$next_attempt_datetime)
{
  $sql = sprintf("
    UPDATE HTT_MONTHLY_SERVICE_CHARGE_LOG
    SET    STATUS                = '%s',
           NEXT_ATTEMPT_DATETIME = %s,
           ATTEMPT_COUNT         = ATTEMPT_COUNT+1
    WHERE  CUSTOMER_ID = %d
    AND    PROCESS_ID  = %d
    ",
    $status,
    $next_attempt_datetime,
    $customer_id,
    getmypid()
  );

  if ( ! is_mssql_successful(logged_mssql_query($sql)) )
  { dlog('',"Error after $sql"); }
}

/**
 * monthly_service_charge_log_fatal_errors_list
 * @param  integer $customer_id
 * @param  string[] $errors
 */
function monthly_service_charge_log_fatal_errors_list($customer_id,$errors)
{
  foreach( $errors as $i => $error )
  {
    $sql = sprintf("
      INSERT INTO HTT_MONTHLY_SERVICE_CHARGE_LOG_ERRORS
      (
        HTT_MONTHLY_SERVICE_CHARGE_LOG_ID,
        ERROR_DATETIME,
        ERROR
      )
      SELECT TOP(1)
        l.HTT_MONTHLY_SERVICE_CHARGE_LOG_ID,
        getutcdate(),
        %s
      FROM   HTT_MONTHLY_SERVICE_CHARGE_LOG l
      WHERE  l.CUSTOMER_ID = %d
      AND    l.PROCESS_ID  = %d
      ORDER BY l.HTT_MONTHLY_SERVICE_CHARGE_LOG_ID DESC
      ",
      mssql_escape_with_zeroes($error),
      $customer_id,
      getmypid()
    );

    if ( is_mssql_successful(logged_mssql_query($sql)) )
    { sleep(1); }
    else
    { dlog('',"Error after $sql"); }
  }
}

/**
 * monthly_service_charge_log_transient_error
 * @param  integer $customer_id
 * @param  integer $attempt_count
 * @param  string[] $errors
 */
function monthly_service_charge_log_transient_error($customer_id,$attempt_count,$errors)
{
  // retry at 3 minutes, 6 hours, 9 hours, and then give up. At 6/9 hours they are going to be in suspend, so those retries will be as part of step 6.

  if ($errors[0] == 'retry' || in_array($errors[0], \Ultra\UltraConfig\getInsufficientFundsErrorCodes()))
  {
    switch ($attempt_count)
    {
      case 1:
      case 2:
        $dateTime = new DateTime('tomorrow 9AM');
        $next_attempt_datetime = "'" . $dateTime->format(MSSQL_DATE_FORMAT) . "'";

        $status = 'RETRY_CC';
        break;
      default:
        $next_attempt_datetime = "NULL";
        $status = 'FAILED_CC';
        break;
    }
  }
  else
  {
    $minutes = ( $attempt_count == 1 ) ? 3 :
             ( ( $attempt_count == 2 ) ? 60*6 : 60*9 ) ;

    $next_attempt_datetime = "DATEADD(n,$minutes,CREATED_DATETIME)";

    $status = 'ERROR';
  }

  dlog('',"monthly_service_charge_log_transient_error, ATTEMPT_COUNT: $attempt_count");

  monthly_service_charge_log_update($customer_id,$status,$next_attempt_datetime);

  if ( isset( $errors ) && is_array($errors) && count($errors) )
  {
    // save errors in HTT_MONTHLY_SERVICE_CHARGE_LOG_ERRORS
    monthly_service_charge_log_fatal_errors_list($customer_id,$errors);
  }
}

/**
 * monthly_service_charge_log_fatal_error
 * @param  integer $customer_id
 * @param  string[] $errors
 */
function monthly_service_charge_log_fatal_error($customer_id,$errors)
{
  monthly_service_charge_log_update($customer_id,'ERROR','NULL');

  if ( isset( $errors ) && is_array($errors) && count($errors) )
  {
    // save errors in HTT_MONTHLY_SERVICE_CHARGE_LOG_ERRORS
    monthly_service_charge_log_fatal_errors_list($customer_id,$errors);
  }
}

/**
 * monthly_service_charge_log_release
 * @param  integer $customer_id
 */
function monthly_service_charge_log_release($customer_id)
{
  monthly_service_charge_log_update($customer_id, 'COMPLETE', 'NULL');
}

/**
 * monthly_service_charge_log_stats
 * @param  string $date_mm_dd_yyyy
 * @return object Result
 */
function monthly_service_charge_log_stats($date_mm_dd_yyyy)
{
  $result = new Result();

  $result->data_array['monthly_service_charge_log_stats'] = array();

  $monthly_service_charge_log_stats = array();

  /* first report by STATUS */

  $sql = sprintf("
    SELECT STATUS, COUNT(*)
    FROM   HTT_MONTHLY_SERVICE_CHARGE_LOG
    WHERE  CONVERT(VARCHAR(10), CREATED_DATETIME,110) = '%s'
    GROUP BY STATUS",
    $date_mm_dd_yyyy
  );

  $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

  if ( $query_result && is_array($query_result) && count($query_result) )
  {
    $result->succeed();

    foreach($query_result as $i => $row)
    { $result->data_array['monthly_service_charge_log_stats']['count_by_status'][$row[0]] = $row[1]; }
  }
  else
  {
    $result = make_error_Result("No data found");
  }

  if ( $result->is_success() && isset($result->data_array['monthly_service_charge_log_stats']['count_by_status']['ERROR']) )
  {
    /* get ERROR details */

    $sql = sprintf("
      SELECT l.customer_id, e.ERROR_DATETIME, e.ERROR, u.plan_state, u.current_mobile_number, u.stored_value, a.BALANCE, a.PACKAGED_BALANCE1, convert(varchar(10), u.plan_expires,110) exp
      FROM   HTT_MONTHLY_SERVICE_CHARGE_LOG l
      JOIN   HTT_MONTHLY_SERVICE_CHARGE_LOG_ERRORS e
      ON     l.HTT_MONTHLY_SERVICE_CHARGE_LOG_ID = e.HTT_MONTHLY_SERVICE_CHARGE_LOG_ID
      JOIN   htt_customers_overlay_ultra u
      ON     l.customer_id = u.customer_id
      JOIN   accounts a
      ON     l.customer_id = a.customer_id
      WHERE  CONVERT(VARCHAR(10), l.CREATED_DATETIME,110) = '%s'
      ",
      $date_mm_dd_yyyy
    );

    $query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

    if ( $query_result && is_array($query_result) && count($query_result) )
    {
      foreach($query_result as $i => $row)
      {
        $result->data_array['monthly_service_charge_log_stats']['errors'][ $row[0] ][] = array( substr($row[1],0,20) , $row[2] );
        $result->data_array['monthly_service_charge_log_stats']['customer'][ $row[0] ] = array( $row[3] , $row[4] , $row[5] , $row[6] , $row[7] , $row[8] );
      }
    }
    else
    {
      $result->add_warning("HTT_MONTHLY_SERVICE_CHARGE_LOG_ERRORS data found");
    }
  }

  return $result;
}


/**
 * add_pending_renewal_subscriber
 * add subscriber with status TODO
 * @see API-212
 * @param Object MRC group
 * @param Object subscriber
 * @return Boolean TRUE on success, FALSE on failure
 */
function add_pending_renewal_subscriber($group, $subscriber)
{
  // validate parameters
  if (empty($subscriber->CUSTOMER_ID) || ! is_numeric($subscriber->CUSTOMER_ID) || empty($group))
  {
    logError('invalid parameters: ' . json_encode(func_get_args()));
    return FALSE;
  }

  // missing time zone defaults to Pacific
  if (empty($subscriber->TZ))
    $subscriber->TZ = date('T');

  // compute subscribers's start time as UTC
  if ( ! $start = date_to_datetime("tomorrow {$group->EARLIEST_LOCAL_HOUR}:00 {$subscriber->TZ}", FALSE, TRUE))
  {
    // failover to 7AM Pacific which is possible if time zone is not recognized
    logError('failed to compute subscriber start time: ' . json_encode(func_get_args()));
    if ( ! $start = date_to_datetime('tomorrow 7:00 ' . date('T'), FALSE, TRUE))
    {
      logError('failed to compute default start time');
      return FALSE;
    }
  }

  // prepare INSERT WHERE NOT EXISTS query
  $sql = sprintf(
    'DECLARE @customer INT = %d;
    DECLARE @start DATETIME = \'%s\';
    DECLARE @status VARCHAR(15) = \'TODO\';
    DECLARE @group INT = %d;
    INSERT INTO HTT_MONTHLY_SERVICE_CHARGE_LOG
    (CUSTOMER_ID, CREATED_DATETIME, NEXT_ATTEMPT_DATETIME, ATTEMPT_COUNT, PROCESS_ID, STATUS, MRC_GROUP_ID)
    SELECT
    @customer, GETUTCDATE(), @start, 0, 0, @status, @group
    WHERE NOT EXISTS
    (SELECT CUSTOMER_ID FROM HTT_MONTHLY_SERVICE_CHARGE_LOG WHERE CUSTOMER_ID = @customer AND STATUS = @status)',
    $subscriber->CUSTOMER_ID, $start, $group->MRC_GROUP_ID);

  // execute
  return run_sql_and_check($sql);
}


/**
 * load_group_subscribers
 * retrieve a list of subscribers associated with the given MRC group in status TODO
 * WARNING: max column name is 30 chars!
 * @param Integer group ID
 * @param Integer max number
 * @return Array of Objects
 */
function load_group_subscribers($group, $count = NULL)
{
  $top = $count ? sprintf('@Top = %d,', $count) : null;
  $sql = sprintf(
   "EXEC [ULTRA].[GetMonthlyServiceChargeLog]
      $top
      @Group = %d,
      @Status = 'TODO'",
    $group);
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}


/**
 * monthly_service_charge_log_update_status
 * update status of a single row by primary key
 * @param Integer HTT_MONTHLY_SERVICE_CHARGE_LOG_ID
 * @param String STATUS
 * @param Bollean increment ATTEMPT_COUNT
 * @return Boolean TRUE on success, FALSE on failure
 */
function monthly_service_charge_log_update_status($key, $status, $increment)
{
  $count = $increment ? 'ATTEMPT_COUNT = ATTEMPT_COUNT + 1,' : NULL;

  $sql = sprintf(
    'DECLARE @row INT = %d;
    DECLARE @status VARCHAR(15) = %s;
    DECLARE @pid INT = %d;
    UPDATE HTT_MONTHLY_SERVICE_CHARGE_LOG SET %s STATUS = @status, PROCESS_ID = @pid WHERE HTT_MONTHLY_SERVICE_CHARGE_LOG_ID = @row',
    $key, mssql_escape_with_zeroes($status), getmypid(), $count);
  return run_sql_and_check($sql);
}


/**
 * get_monthly_service_charge_log_init_stats
 * execute and return MRC groups initialization statistics
 * @see PROD-2248
 * @return Array of Objects
 */
function get_monthly_service_charge_log_init_stats()
{
  // statis query, therefore no need to parameterize
  $sql = "SELECT g.MRC_GROUP_ID, g.EARLIEST_LOCAL_HOUR, r.TZ, count(*) AS TOTAL
    FROM ULTRA.MRC_GROUPS g WITH (NOLOCK)
    JOIN HTT_MONTHLY_SERVICE_CHARGE_LOG l WITH (NOLOCK) ON g.MRC_GROUP_ID=l.MRC_GROUP_ID
    JOIN CUSTOMERS c WITH (NOLOCK) ON l.CUSTOMER_ID=c.CUSTOMER_ID
    LEFT JOIN HTT_LOOKUP_REFERENCE_ZIP r WITH (NOLOCK) ON c.POSTAL_CODE=r.zip
    WHERE l.STATUS='TODO'
    GROUP BY g.MRC_GROUP_ID, g.EARLIEST_LOCAL_HOUR, r.TZ
    ORDER BY CASE
      WHEN r.TZ='EST' THEN 1
      WHEN r.TZ='CST' THEN 2
      WHEN r.TZ='MST' THEN 3
      WHEN r.TZ='PST' THEN 4
      WHEN r.TZ='PST-6' THEN 5
      ELSE 6 END,
    g.MRC_GROUP_ID";
  return mssql_fetch_all_objects(logged_mssql_query($sql));
}

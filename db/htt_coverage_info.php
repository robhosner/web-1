<?php

include_once('db.php');

# php component to access DB table htt_coverage_info #

# synopsis

#echo htt_coverage_info_select_query( array('zip_code' => 12345) )."\n";

/**
 * htt_coverage_info_select_query
 * $params: zip_code
 * 
 * @param  array $params
 * @return string SQL
 */
function htt_coverage_info_select_query($params)
{
  $where = array();

  if ( isset($params['zip_code']) )
  {
    $where[] = "zipcode = '".mssql_escape($params["zip_code"])."'";
  }

  $where_clause = "";

  if ( count($where) > 0 )
  {
    $where_clause = ' WHERE '.implode(" AND ", $where);
  }

  $query = "
 SELECT *,

 CASE
   WHEN RF_GREAT_POP =  1     THEN 9
   WHEN RF_GREAT_POP >= 0.6   THEN 8
   WHEN RF_GREAT_POP >= 0.2   THEN 7
   WHEN RF_GOOD_POP  >= 0.75  THEN 6
   WHEN RF_GOOD_POP  >= 0.5   THEN 5
   WHEN RF_GOOD_POP  >= 0.2   THEN 4
   WHEN RF_FAIR_POP  >= 0.6   THEN 3
   WHEN RF_FAIR_POP  >= 0.2   THEN 2
   WHEN RF_FAIR_POP  >  0     THEN 1
   ELSE 0
 END as quality

 FROM htt_coverage_info $where_clause";

  return $query;
}

/**
 * htt_coverage_info_get_coverage_data 
 * @param  string $zipcode
 * @return object[]
 */
function htt_coverage_info_get_coverage_data($zipcode)
{
  $query = htt_coverage_info_select_query(array('zip_code' => $zipcode));

  return mssql_fetch_all_objects(logged_mssql_query($query));
}

/**
 * validateZipCodesCoverage
 *
 * Zip code coverage validation
 *
 * @param  array $zipcodes
 * @return boolean
 */
function validateZipCodesCoverage( $zipcodes )
{
  $success = TRUE;

  foreach( $zipcodes as $zipcode )
  {
    $sql = htt_coverage_info_select_query( array('zip_code' => $zipcode ) );

    $coverage_data = mssql_fetch_all_objects(logged_mssql_query($sql));

    if ( ! ( $coverage_data && is_array($coverage_data) && count($coverage_data) ) )
      $success = FALSE;
  }

  return $success;
}

?>

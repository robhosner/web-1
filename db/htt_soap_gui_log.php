<?php

/**
 * htt_soap_gui_log_insert 
 * $params: command
 *          param_name_1
 *          param_name_2
 *          param_value_1
 *          param_value_2
 *          auth_user
 *          
 * @param  array $params
 * @return boolean
 */
function htt_soap_gui_log_insert($params)
{
  $sql = sprintf(
    "INSERT INTO htt_soap_gui_log
     (
       COMMAND,
       PARAM_NAME_1,
       PARAM_NAME_2,
       PARAM_VALUE_1,
       PARAM_VALUE_2,
       AUTH_USER
     ) VALUES (
       %s,
       %s,
       %s,
       %s,
       %s,
       %s
     )",
     mssql_escape_with_zeroes($params['command']),
     mssql_escape_with_zeroes($params['param_name_1']),
     mssql_escape_with_zeroes($params['param_name_2']),
     mssql_escape_with_zeroes($params['param_value_1']),
     mssql_escape_with_zeroes($params['param_value_2']),
     mssql_escape_with_zeroes($params['auth_user'])
  );

  return is_mssql_successful(logged_mssql_query($sql));
}

?>

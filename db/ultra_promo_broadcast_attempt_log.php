<?php

// [ULTRA].[PROMO_BROADCAST_ATTEMPT_LOG]

/**
 * add_to_ultra_promo_broadcast_attempt_log
 * $params: customer_id
 *          promo_shortcode
 *          result
 *          
 * @param array $params
 * @return boolean result of INSERT query
 */
function add_to_ultra_promo_broadcast_attempt_log( $params )
{
  if ( empty( $params['customer_id'] ) )
    return make_error_Result('customer_id missing');

  if ( empty( $params['promo_shortcode'] ) )
    return make_error_Result('promo_shortcode missing');

  if ( empty( $params['result'] ) )
    return make_error_Result('result missing');

  $sql = sprintf("
INSERT INTO ULTRA.PROMO_BROADCAST_ATTEMPT_LOG
(
CUSTOMER_ID,
PROMO_SHORTCODE,
RESULT
)
VALUES
(
%d,
%s,
%s
) ",
    $params['customer_id'],
    mssql_escape_with_zeroes($params['promo_shortcode']),
    mssql_escape_with_zeroes($params['result'])
  );

  return run_sql_and_check_result($sql);
}

/**
 * get_ultra_promo_broadcast_attempt_log_success_count
 *
 * @param  string $shortcode
 * @return Array [RESULT => count,...]
 */
function get_ultra_promo_broadcast_attempt_log_success_count($shortcode)
{
  $sql = sprintf('SELECT RESULT, COUNT(*) as count FROM ULTRA.PROMO_BROADCAST_ATTEMPT_LOG with (nolock)
    WHERE PROMO_SHORTCODE = %s
    GROUP BY RESULT',
    mssql_escape_with_zeroes($shortcode)
  );

  $log = mssql_fetch_all_objects(logged_mssql_query($sql));

  $output = array();

  foreach ($log as $entry)
    $output[$entry->RESULT] = $entry->count;

  return $output;
}

?>

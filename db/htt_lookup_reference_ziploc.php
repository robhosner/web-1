<?php

include_once('db.php');

# php component to access DB table htt_lookup_reference_ziploc #

# synopsis

#echo htt_lookup_reference_ziploc_select_query_by_coordinates(
#  array(
#    "max_entries" => 3,
#    "latitude"    => 86.51557,
#    "longitude"   => 33.584132
#  )
#)."\n\n";

# echo htt_lookup_reference_ziploc_select_query_by_zip_code('12345')."\n\n";

/**
 * htt_lookup_reference_ziploc_select_query_by_zip_code 
 * @param  string $zip_code
 * @return string SQL
 */
function htt_lookup_reference_ziploc_select_query_by_zip_code($zip_code)
{
  $sql = sprintf(
    "SELECT ZIP_CODE, LATITUDE, LONGITUDE
     FROM   HTT_LOOKUP_REFERENCE_ZIPLOC
     WHERE  ZIP_CODE = %s",
     mssql_escape_with_zeroes($zip_code)
  );

  return $sql;
}

/**
 * htt_lookup_reference_ziploc_select_query_by_coordinates
 * $params: latitude
 *          longitude
 *          max_entries
 *          
 * @param  array $params
 * @return string SQL
 */
function htt_lookup_reference_ziploc_select_query_by_coordinates($params)
{
  $where = array(
    # location should not be empty
    " LOCATION.STDistance(@g) IS NOT NULL "
  );

  $where_clause = ' WHERE '.implode(" AND ", $where);

  $sql = sprintf(
"
DECLARE @g geography = 'POINT( %s %s )';

SELECT    TOP(%s)
          ZIP_CODE, LATITUDE, LONGITUDE
FROM      HTT_LOOKUP_REFERENCE_ZIPLOC
$where_clause
ORDER BY  LOCATION.STDistance(@g)
",
    $params["latitude"],
    $params["longitude"],
    $params["max_entries"]
  );

  return $sql;
}

?>

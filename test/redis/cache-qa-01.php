<?php

require_once 'db.php';
require_once 'Ultra/UltraConfig.php';

$port = '6379';
$host = 'cache-qa-01.hometowntelecom.com';

    try
    {
      $redis = new \Redis();

      # Redis connection
      if ( ! $redis->connect( $host , $port ) )
        throw new \Exception("redis connection attempt failed ( host = $host , port = $port )");

      # Redis authentication
      if ( ! $redis->auth( \Ultra\UltraConfig\redisPassword() ) )
        throw new \Exception("redis authentication attempt failed");

      $value = time();

      echo "test value = $value\n";

      # test set
      $redis->set('test/cache-qa-01',$value,5);

      echo "test get before sleep - ".$redis->get('test/cache-qa-01')."\n";

      sleep(6);

      echo "test get after sleep - ".$redis->get('test/cache-qa-01')."\n";
    }
    catch(\Exception $e)
    {
      echo $e->getMessage()."\n";
    }

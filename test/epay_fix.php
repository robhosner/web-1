<?php

require_once 'db.php';
require_once 'Ultra/Lib/Billing/functions.php';
require_once 'Ultra/Lib/Util/Redis/Billing.php';

teldata_change_db();

$sql = "select UUID from HTT_BILLING_ACTIONS where DATEDIFF( mi , CREATED , getutcdate() ) > 30 and status = 'OPEN'";

$query_result = mssql_fetch_all_rows(logged_mssql_query($sql));

foreach($query_result as $row)
{
  $redisBilling = new \Ultra\Lib\Util\Redis\Billing();

  $r = \Ultra\Lib\Billing\billingTask( $redisBilling , $row[0] );

  print_r($r);
}


<?php

require_once 'db.php';

$param_types = array(
  'active_days'         => '30',
  'amount'              => '2.50',
  'balance'             => '99.99',
  'wallet_balance'      => '99.99',
  'stored_value'        => '99.99',
  'old_plan_amount'     => '2.50',
  'value'               => '2.50',
  'bolt_on'             => 'UpData',
  'bolt_ons'            => 'UpIntl and UpData',
  'current_plan_amount' => '29',
  'data_MB'             => '2048',
  'usage'               => '2048',
  'zero_minutes'        => '9999',
  'remaining'           => '2048',
  'gizmo_amount'        => '$10.00',
  'minutes'             => '50',
  'msisdn'              => '1001001000',
  'new_plan_amount'     => '19',
  'personal'            => 'Personal',
  'plan_amount_upgrade' => '25',
  'plan_amount'         => '10.00',
  'plan_name'           => 'Ultra $29',
  'reload_amount'       => '$10.00',
  'renewal_date'        => 'Aug 30 2014',
  'date'                => 'Aug 30 2014',
  'temp_password'       => 'btxhco97',
  'url'                 => 'https://local.m.ultra.me/#/login/%7BC200EF3C-1168-65D4-AFFC-CC9120256FF8%7D 75',
  'activations'         => '4',
  'login_token'         => 'TeStLoGiNtOkEn',
  'added_amount'        => '789',
  'owed_amount'         => '987'
);

teldata_change_db();

$customer = get_customer_from_customer_id(18759);
$customer->preferred_language = 'EN'; // 'ES'; // 'ZH';

$sent_messages = array();

$templates = \Ultra\Messaging\Templates\getAllTemplates();

foreach ($templates as $template => $text)
{
  $params = array();

  $message_type = explode('__', $template);
  $params['message_type'] = $message_type[0];

  if (in_array($params['message_type'], $sent_messages))
    continue;

  foreach ($param_types as $type => $val)
  {
    if (strpos($text, '__params__' . $type) !== FALSE)
    {
      $params[$type] = $val;
    }
  }

  $sent_messages[] = $params['message_type'];

  $params['customer'] = $customer;

  echo "[{$params['message_type']}] Continue? ";
  $handle = fopen ("php://stdin","r");
  $line   = fgets ($handle);
  if (trim($line) != 'y')
  {
    echo "ABORTING!\n";
    exit;
  }

  funcSendExemptCustomerSMS($params);
}

print_r($sent_messages);

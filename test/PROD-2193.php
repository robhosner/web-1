<?php

require_once 'db.php';
require_once 'Ultra/Lib/DB/Cache.php';

/*
  PROD-2193
  clean up invalid MRC usage values
  bugs
    we are still writing MRC usage for activations
    we do not write usage when suspending via mvneinternal__NetworkSuspend (CID 1437788)
*/

define('MODE', 'PROD'); // PROD | DEV
if (MODE == 'PROD')
{
  $data_host = 'db.coresite';
  $data_db = 'ULTRA_DATA';
  $acc_host = 'sandy';
  $acc_db = 'ULTRA_ACC';
  $prefix = '';
}
elseif (MODE == 'DEV')
{
  $data_host = 'db-a.coresite';
  $data_db = 'ULTRA_DEVELOP_TEL';
  $acc_host = 'sandy';
  $acc_db = 'ULTRA_ACC';
  $prefix = 'DEVELOP_';
}
else
  die('invalid mode ' . MODE);



// connect to production DB (use Cache class to minimize DB impact since we generate a large number of queries)
$prod = new Ultra\Lib\DB\Cache(NULL, $data_host, $data_db);
if ($error = $prod->getError())
  die("$error\n");
logInfo("using DB $data_db on host $data_host");

// connect to ULTRA_ACC (use Cache class to minimize DB impact since we generate a large number of queries)
$acc = new Ultra\Lib\DB\Cache(NULL, $acc_host, $acc_db);
if ($error = $acc->getError())
  die("$error\n");
logInfo("using DB $acc_db on host $acc_host");

// walk sequentially through all MRC_USAGE rows, validate against HTT_PLAN_TRACKER and correct if neccessary
for ($i = 1; $i < 2000000; $i++)
{
  if ( ! $usage = $acc->selectRow("{$prefix}MRC_USAGE", 'MRC_USAGE_ID', $i, -1, FALSE))
  {
    // sequence gap
    logInfo("no MRC_USAGE for ID $i");
    continue;
  }
  if ( ! $period = $prod->selectRow('HTT_PLAN_TRACKER', 'HTT_PLAN_TRACKER_ID', $usage->HTT_PLAN_TRACKER_ID, -1, FALSE))
  {
    logError("data integrity problem: missing HTT_PLAN_TRACKER $i");
    continue;
  }
  echo "processing MRC_USAGE_ID {$usage->MRC_USAGE_ID}, HTT_PLAN_TRACKER_ID {$period->HTT_PLAN_TRACKER_ID}: ";

  if ($err = convertRowData($period, $usage))
  {
    logError("$err\n");
    echo "ERROR: $err\n";
    continue;
  }

  // validate customer ID
  if ($usage->CUSTOMER_ID != $period->CUSTOMER_ID)
    echo "ERROR: customer ID does not match\n";

  // validate usage sample date: must be within 1 day of period expiration date
  elseif (abs($usage->SAMPLE_DATE_EPOCH - $period->PLAN_EXPIRES_EPOCH) > SECONDS_IN_DAY)
    echo handleInvalidSampleDate($period, $usage) . "\n";

  // data are consistent
  else
    echo "OK\n";

  usleep(10000); // reduce DB impact
}


/**
 * removeMrcUsage
 * remove invalid MRC usage records
 * @return Boolean success
 */
function removeMrcUsage($id)
{
  global $acc_host, $acc_db, $prefix;

  if ( ! $sql1 = Ultra\Lib\DB\makeDeleteQuery("{$prefix}MRC_USAGE_VALUES", array('MRC_USAGE_ID' => $id)))
    return logError('cannot prepare DELETE query 1');

  if ( ! $sql2 = Ultra\Lib\DB\makeDeleteQuery("{$prefix}MRC_USAGE", array('MRC_USAGE_ID' => $id)))
    return logError('cannot prepare DELETE query 2');

  $link = connect_to_ultra_db($acc_host, $acc_db);
  if ( ! is_resource($link))
    return logError('cannot connect to ACC DB');

  logInfo($sql1);
  if ( ! $result = @mssql_query($sql1, $link))
    return logError('failed to remove MRC_USAGE_VALUES: ' . mssql_get_last_message());

  logInfo($sql2);
  if ( ! $result = @mssql_query($sql2, $link))
    return logError('failed to remove MRC_USAGE: ' . mssql_get_last_message());

  mssql_close($link);
  return TRUE;
}


/**
 * updateMrcUsage
 * re-assign MRC usage to a different HTT_PLAN_TRACKER period
 * @return Boolean success
 */
function updateMrcUsage($usage, $period)
{
  global $acc_host, $acc_db, $prefix;

  if ( ! $sql = Ultra\Lib\DB\makeUpdateQuery("{$prefix}MRC_USAGE",
    array('HTT_PLAN_TRACKER_ID' => $period->HTT_PLAN_TRACKER_ID),
    array('MRC_USAGE_ID' => $usage->MRC_USAGE_ID)))
    return logError('cannot prepare UPDATE query');

  $link = connect_to_ultra_db($acc_host, $acc_db);
  if ( ! is_resource($link))
    return logError('cannot connect to ACC DB');

  logInfo($sql);
  if ( ! $result = @mssql_query($sql, $link))
    return logError('failed to update MRC_USAGE: ' . mssql_get_last_message());

  mssql_close($link);
  return TRUE;
}


/**
 * convertRowData
 * prepare row data for processing
 * @return String error message
 */
function convertRowData(&$period = NULL, &$usage = NULL)
{
  if ($period)
  {
    if ( ! $period->PLAN_STARTED_EPOCH = date_to_epoch($period->PLAN_STARTED))
      return 'cannot convert PLAN_STARTED to epoch';

    if ( ! $period->PLAN_EXPIRES_EPOCH = date_to_epoch($period->PLAN_EXPIRES))
      return 'cannot convert PLAN_EXPIRES to epoch';
  }

  if ($usage)
  {
    if ( ! $usage->SAMPLE_DATE_EPOCH = date_to_epoch($usage->SAMPLE_DATE))
      return 'cannot convert SAMPLE_DATE to epoch';
  }

  return NULL;
}


/**
 * handleInvalidSampleDate
 * handle case when usage sample date is not during period expirationd date
 * adjust PLAN_TRACKER_ID if previos period is availble, remove sample date for first activation period (bug)
 * @return String message what was done
 */
function handleInvalidSampleDate($period, $usage)
{
  global $data_host, $data_db, $prefix;

  logInfo("handling invalid SAMPLE_DATE {$usage->SAMPLE_DATE}");

  // remove usage sampled during activation: these values are useless
  if ($period->PERIOD == 1)
    return (removeMrcUsage($usage->MRC_USAGE_ID) ? 'removed' : 'failed to remove') . ' activation sample';

  // attemp to locate a previous period
  if ( ! $sql = Ultra\Lib\DB\makeSelectQuery('HTT_PLAN_TRACKER', 1, NULL,
    array(
      'CUSTOMER_ID'         => $period->CUSTOMER_ID,
      'HTT_PLAN_TRACKER_ID' => "lt_{$period->HTT_PLAN_TRACKER_ID}"),
    NULL, 'HTT_PLAN_TRACKER_ID DESC', TRUE))
    return $message . 'failed to prepare SELECT query';

  if ( ! $link = connect_to_ultra_db($data_host, $data_db))
    return 'cannot connect to production DB';

  logInfo($sql);
  if ( ! $result = @mssql_query($sql, $link))
    return 'query failed: ' . mssql_get_last_message();

  $data = mssql_fetch_all_objects($result, MSSQL_FLAG_CAPITALIZE);
  mssql_close($link);
  if (empty($data))
    return 'no previous period found';
  $previous = $data[0];
  logInfo('found previous period: ' . print_r($previous, TRUE));

  // validate previous period start date
  if ($err = convertRowData($previous))
    return $err;

  // if previous plan start date does not match then sub was suspended between plans and therefore data is useless
  if (abs($previous->PLAN_EXPIRES_EPOCH - $usage->SAMPLE_DATE_EPOCH) > SECONDS_IN_DAY)
    return removeMrcUsage($usage->MRC_USAGE_ID) ? 'removed' : 'failed to remove';

  // update to previous perioud
  return updateMrcUsage($usage, $previous) ? 'updated' : 'update failed';
}


/**
 * ultra_data_connect
 * connect to DB $database on host $host
 * @return Resource DB connection or NULL on failure
 */
function connect_to_ultra_db($host, $database)
{
  if ( ! $username = \Ultra\Lib\DB\user())
    return logError('environment is missing database username');

  if ( ! $password = \Ultra\Lib\DB\pwd())
    return logError('environment is missing database password');

  if ( ! $link = @mssql_pconnect($host, $username, $password))
    return logError("failed to connect to host $host as username $username");

  if ( ! @mssql_select_db($database, $link))
    return logError("username $username cannot access database $database on host $host ($message)");

  logInfo("using database $database on host $host");  
  return $link;
}



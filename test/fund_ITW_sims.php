<?php

/*
a) select all customers from CID that are in provisioned with no state changes and queue them up
  CID = select * from [HTT_CUSTOMERS]
  where [customer_id] in
  (select [CUSTOMER_ID] from [HTT_INVENTORY_SIM] where [INVENTORY_MASTERAGENT]=69 and INVENTORY_DISTRIBUTOR=301 and [INVENTORY_DEALER]=10323)

b) Call AddCourtesyCash with the appropriate params

c) halt when we go above 40 active transitions, until we have less than 15 active transitions.
*/

include_once 'db.php';
include_once 'test/api/test_api_include.php';

teldata_change_db(); // DB connection

main();

function main()
{
  delay_due_to_transitions();

  $sql = "
SELECT TOP 10 o.plan_state,
              s.iccid_number,
              s.customer_id
FROM   htt_inventory_sim s
       JOIN htt_customers_overlay_ultra o
         ON o.customer_id = s.customer_id
 WHERE  s.inventory_masteragent     = 69
       AND s.inventory_distributor = 301
       AND s.inventory_dealer      = 10323
       AND o.plan_state            = 'Provisioned'";

  $customers = mssql_fetch_all_objects(logged_mssql_query($sql));

  $customer  = extract_customer($customers);

  if ( $customer )
  {
    dlog('',"customer = %s",$customer);
    fund_ITW_customer($customer);
  }
  else
  {
    dlog('',"no customer to be processed");
  }
}

function fund_ITW_customer($customer)
{
  // check customer's latest transitions: we need to find exactly one transition from Neutral to Provisioned

  $result = get_all_customer_state_transitions($customer->customer_id);

  if ( count($result['errors']) )
  {
    dlog('',"Errors after get_all_customer_state_transitions : %s",$result['errors']);
  }
  elseif ( isset($result['state_transitions']) && is_array($result['state_transitions']) && count($result['state_transitions']) )
  {
    if ( count($result['state_transitions']) == 1 )
    {
      if ( ( $result['state_transitions'][0]->FROM_PLAN_STATE == 'Neutral' ) && ( $result['state_transitions'][0]->TO_PLAN_STATE == 'Provisioned' ) )
      {
        fund_with_AddCourtesyCash($customer);
      }
      else
      {
        dlog('',"Last transitions for this customer is not from Neutral to Provisioned");
      }
    }
    else
    {
      dlog('',"Too many transitions found for this customer");
    }
  }
  else
  {
    dlog('',"No transitions from Neutral to Provisioned found for this customer");
  }
}

function fund_with_AddCourtesyCash($customer)
{
  dlog('',"fund_with_AddCourtesyCash invoked with customer_id = ".$customer->customer_id);

  // invoke customercare__AddCourtesyCash with $59

  $apache_username = 'ultra_rainmaker_develop_test';
  $apache_password = 'vanilla-yahtzee-fantastic';

  $curl_options = array(
    CURLOPT_USERPWD  => "$apache_username:$apache_password",
    CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
  );

  $params = array(
    'reason'                => 'fund_ITW_sims',
    'agent_name'            => 'fund_ITW_sims',
    'customercare_event_id' => time(),
    'amount'                => 5900, # in cents
    'customer_id'           => 0 # $customer->customer_id
  );

  $url = 'https://live-api.ultra.me/pr/rainmaker/1/ultra/api/customercare__AddCourtesyCash';

  $json_result = curl_post($url,$params,$curl_options,NULL,120);

  echo "\n\n$json_result\n\n";

  print_r(json_decode($json_result));
}

function extract_customer($customers)
{
  // get 1 random customer from $customers

  $customer = NULL;

  if ( $customers && is_array($customers) && count($customers) )
  {
    $customer = $customers[ array_rand( $customers ) ];
  }

  return $customer;
}

function delay_due_to_transitions()
{
  # If the amount of OPEN transitions is more than 40, we will sleep for 30 seconds.
  # We'll wait until the count will be less than 15

  $max_limit = 40;

  $htt_transition_log_open_count = htt_transition_log_open_count();

  while ( $htt_transition_log_open_count > $max_limit )
  {
    dlog('',"Sleeping for 30 seconds since we have $htt_transition_log_open_count open transitions (limit = $max_limit).");

    sleep(30);

    $max_limit = 15;

    $htt_transition_log_open_count = htt_transition_log_open_count();
  }
}

?>

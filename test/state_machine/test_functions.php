<?php


date_default_timezone_set("America/Los_Angeles");


include_once('lib/state_machine/functions.php');
include_once('Ultra/Lib/MiddleWare/Adapter/Control.php');


# Usage:

# php test/state_machine/test_functions.php get_last_transition_name_by_customer_id $CUSTOMER_ID
# php test/state_machine/test_functions.php internal_func_delayed_provisioning_suspend $CUSTOMER_ID
# php test/state_machine/test_functions.php internal_func_get_state $CUSTOMER_ID
# php test/state_machine/test_functions.php internal_func_check_transition
# php test/state_machine/test_functions.php internal_func_get_all_pending_transitions
# php test/state_machine/test_functions.php internal_func_get_pending_transitions
# php test/state_machine/test_functions.php internal_func_resolve_pending_transitions
# php test/state_machine/test_functions.php close_action $ACTION_UUID $STATUS $ACTION_RESULT
# php test/state_machine/test_functions.php abort_action $ACTION_UUID $ACTION_RESULT
# php test/state_machine/test_functions.php abort_transition $HTT_TRANSITION_UUID
# php test/state_machine/test_functions.php abort_action_and_transition $HTT_TRANSITION_UUID $ACTION_UUID $ACTION_RESULT
# php test/state_machine/test_functions.php redo_activation_action $CUSTOMER_ID $HTT_TRANSITION_UUID $ACTION_UUID
# php test/state_machine/test_functions.php redo_transition $HTT_TRANSITION_UUID
# php test/state_machine/test_functions.php undo_cancel_account $CUSTOMER_ID
# php test/state_machine/test_functions.php prefunded_to_portinreq       $CUSTOMER_ID
# php test/state_machine/test_functions.php prefunded_to_active          $CUSTOMER_ID
# php test/state_machine/test_functions.php neutral_to_provisioned       $CUSTOMER_ID $PLAN
# php test/state_machine/test_functions.php neutral_to_port_in_requested $CUSTOMER_ID $MSISDN $ICCID $ACCOUNT_NUMBER $ACCOUNT_PASSWORD $ZIPCODE $PLAN
# php test/state_machine/test_functions.php portinreq_to_provisioned     $CUSTOMER_ID
# php test/state_machine/test_functions.php cancelled_to_portinreq_to_provisioned $CUSTOMER_ID $MSISDN $ICCID
# php test/state_machine/test_functions.php portin_denied_to_provisioned $CUSTOMER_ID
# php test/state_machine/test_functions.php neutral_to_port_suceeded     $CUSTOMER_ID
# php test/state_machine/test_functions.php transition_customer_state    $CUSTOMER_ID $TRANSITION_NAME
# php test/state_machine/test_functions.php adjust_cancelled_customer    $CUSTOMER_ID $MSISDN $ICCID $PLAN_STATE
# php test/state_machine/test_functions.php callbackChangeSIM            $ICCID $CUSTOMER_ID
# php test/state_machine/test_functions.php internal_func_delayed_ensure_provisioning_or_activation
# php test/state_machine/test_functions.php internal_func_delayed_ensure_suspend $CUSTOMER_ID

abstract class AbstractTestStrategy
{
  abstract function test();
}

class Test_callbackChangeSIM extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $iccid          = $argv[2];
    $customer_id    = $argv[3];

    $r = callbackChangeSIM($iccid,$customer_id);
    print_r($r);
  }
}

// customer cancelled - we need to add the missing info
// Example :
// %> php test/state_machine/test_functions.php adjust_cancelled_customer 846514 2138588730 8901260962111065634 'Port-In Requested'
class Test_adjust_cancelled_customer extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    if ( ! $argv[2] || ! is_numeric( $argv[2] ) )
      die('invalid or missing customer_id');

    if ( ! $argv[3] || ! is_numeric( $argv[3] ) )
      die('invalid or missing MSISDN');

    if ( ! $argv[4] || ! is_numeric( $argv[4] ) )
      die('invalid or missing ICCID');

    if ( ! $argv[5] )
      die('missing plan_state');

    $customer_id     = $argv[2];
    $msisdn          = $argv[3];
    $iccid_19        = luhnenize( $argv[4] );
    $iccid_18        = substr($iccid_19,0,-1);
    $plan_state      = $argv[5];

    $sql = sprintf("
UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA
SET    CURRENT_MOBILE_NUMBER = '%s',
       CURRENT_ICCID         = '%s',
       CURRENT_ICCID_FULL    = '%s',
       PLAN_STATE            = '%s'
WHERE  CUSTOMER_ID           = %d",
      $msisdn,
      $iccid_18,
      $iccid_19,
      $plan_state,
      $customer_id
    );

    echo "$sql\n";

    $check = run_sql_and_check($sql);

   if ( $check )
   { echo "SUCCESS\n;"; }
   else
   { echo "ERROR\n;"; }

    exit;
  }
}


class Test_transition_customer_state extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id     = $argv[2];
    $transition_name = $argv[3];

    $customer = get_customer_from_customer_id($customer_id);

    $r = transition_customer_state($customer,$transition_name);

    print_r($r);

    echo "\n\n";
  }
}


class Test_close_action extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $action_uuid     = $argv[2];
    $status          = $argv[3];
    $action_result   = $argv[4];

    $r = close_action($action_uuid, $status, $action_result);

    print_r($r);

    echo "\n\n";
  }
}


class Test_abort_action extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $action_uuid     = $argv[2];
    $action_result   = $argv[3];

    $r = abort_action($action_uuid, $action_result);

    print_r($r);

    echo "\n\n";
  }
}


class Test_redo_transition extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = redo_transition( $argv[2] );

    print_r($r);

    echo "\n\n";
  }
}


class Test_undo_cancel_account extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = undo_cancel_account( array( 'customer_id' => $argv[2] ) );

    print_r($r);

    echo "\n\n";
  }
}


class Test_abort_transition extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $transition_uuid = $argv[2];

    $r = abort_transition($transition_uuid);

    print_r($r);

    echo "\n\n";
  }
}


class Test_abort_action_and_transition extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $transition_uuid = $argv[2];
    $action_uuid     = $argv[3];
    $action_result   = $argv[4];

    $r = abort_action_and_transition($transition_uuid, $action_uuid, $action_result);

    print_r($r);

    echo "\n\n";
  }
}


class Test_redo_activation_action extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = redo_activation_action($argv[2], $argv[3], $argv[4]);

    dlog('',"%s",$r);

    echo "\n\n";
  }
}



class Test_get_last_transition_name_by_customer_id extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $last_transition_name = get_last_transition_name_by_customer_id( $argv[2] );

    echo "last_transition_name = $last_transition_name\n\n";
  }
}


class Test_internal_func_delayed_provisioning_suspend extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = internal_func_delayed_provisioning_suspend( $argv[2] );

    print_r($r);

    echo "\n\n";
  }
}


class Test_neutral_to_port_in_requested extends AbstractTestStrategy
{
/*
php test/state_machine/test_functions.php neutral_to_port_in_requested 2405 1001001000 1001001000100100100 11122 33344 12345 L59
*/
  function test()
  {
    global $argv;

    $customer_id = $argv[2];
    $plan        = $argv[8];

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $customer_id
        )
      )
    );

    print_r($r);

    echo "\n\n";

    if ( $r['state'] != 'Neutral' )
      die('customer state is not Neutral');

    // performs State Transition [ Neutral ] => [ Port-In Requested ]

    $context = array(
      'customer_id'              => $customer_id,
      'plan'                     => $plan,
      'port_in_msisdn'           => $argv[3],
      'port_in_iccid'            => $argv[4],
      'port_in_account_number'   => $argv[5],
      'port_in_account_password' => $argv[6],
      'port_in_zipcode'          => $argv[7]
    );

    $dry_run         = FALSE;
    $resolve_now     = FALSE;
    $transition_name = "Request Port ".get_plan_name_from_short_name($plan);

    $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, 1);

    print_r($result_status);

    echo "\n\n";
  }
}


class Test_portin_denied_to_provisioned extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $argv[2]
        )
      )
    );

    print_r($r);

    echo "\n\n";

    if ( $r['state'] != 'Port-In Denied' )
      die('customer state is not Port-In Denied');

    echo "\n\n";

    $sql = "update htt_customers_overlay_ultra set plan_state = 'Port-In Requested' where customer_id = ".$argv[2];

    if ( ! run_sql_and_check( $sql ) )
      die('update failed');

    $context = array(
      'customer_id'   => $argv[2]
    );

    $change = change_state($context, TRUE, 'Port Provisioned', 'take transition', FALSE, 1);

    print_r($change);

    echo "\n\n";
  }
}


class Test_cancelled_to_portinreq_to_provisioned extends AbstractTestStrategy
{
  function test()
  {
    // PROD-1011 : Cancelled State with Network Setting

    global $argv;

    if ( empty($argv[2]) )
      die('customer id missing');

    if ( empty($argv[3]) )
      die('msisdn missing');

    if ( empty($argv[4]) )
      die('iccid missing');

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $argv[2]
        )
      )
    );

    print_r($r);

    echo "\n\n";

    if ( $r['state'] != 'Cancelled' )
      die('customer state is not Cancelled');

    $customer_id = $argv[2];
    $msisdn      = $argv[3];
    $iccid       = $argv[4];

    $iccid_19 = $iccid;
    $iccid_18 = $iccid;

    if (strlen($iccid) == 18)
    { $iccid_19 = luhnenize($iccid_18);   }
    else
    { $iccid_18 = substr($iccid_19,0,-1); }

    $sql = "select a.to_cos_id from htt_transition_log     a where a.to_cos_id > 1 and a.customer_id = $customer_id
            union
            select b.to_cos_id from htt_transition_archive b where b.to_cos_id > 1 and b.customer_id = $customer_id";

    echo "$sql\n";

    $result = mssql_query($sql);

    $data = mssql_fetch_all_rows($result);

    if ( ! $data || ! is_array($data) || ! is_array($data[0]) )
      die('no cos_id found');

    $cos_id = $data[0][0];

    echo "customer_id = $customer_id ; msisdn = $msisdn ; iccid_18 = $iccid_18 ; iccid_19 = $iccid_19 ; cos_id = $cos_id\n";

    $sql = sprintf("update accounts set cos_id = %d where customer_id = %d",$cos_id,$customer_id);

    echo "$sql\n";

    if ( ! run_sql_and_check($sql) )
      die("SQL failed");

    $q = sprintf("update htt_customers_overlay_ultra set
current_iccid         = '%s',
current_mobile_number = '%s', plan_state = 'Port-In Requested',
CURRENT_ICCID_FULL    = '%s' where customer_id = %d",
    $iccid_18,$msisdn,$iccid_19,$customer_id
    );

    echo "$q\n";

    if ( ! run_sql_and_check($q) )
      die("SQL failed");

    sleep(1);

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $argv[2]
        )
      )
    );

    print_r($r['state']);

    echo "\n\n";

    if ( $r['state'] != 'Port-In Requested' )
      die('customer state is not Port-In Requested');

    $context = array(
      'customer_id'   => $argv[2]
    );

    $change = change_state($context, TRUE, 'Port Provisioned', 'take transition', FALSE, 1);

    print_r($change);

    echo "\n\n";
  }
}

class Test_portinreq_to_provisioned extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $argv[2]
        )
      )
    );

    print_r($r);

    echo "\n\n";

    if ( $r['state'] != 'Port-In Requested' )
      die('customer state is not Port-In Requested');

    $context = array(
      'customer_id'   => $argv[2]
    );

    $change = change_state($context, TRUE, 'Port Provisioned', 'take transition', FALSE, 1);

    print_r($change);

    echo "\n\n";
  }
}


class Test_neutral_to_port_suceeded extends AbstractTestStrategy
{
/*
 - Customer is in 'Neutral' state
 - there is an aborted state transition Neutral -> Port-In Requested
 - Customer's ICCID is active on the network
*/
  function test()
  {
    global $argv;

    // check the state

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $argv[2]
        )
      )
    );

    print_r($r);

    echo "\n\n";

    if ( $r['state'] != 'Neutral' )
      die('customer state is not Neutral');

    // load the customer

    $customer = get_customer_from_customer_id($argv[2]);

    if ( ! $customer )
      die("No customer found");

    // check the aborted state transition

    $query = htt_transition_log_select_query(
      array(
        'customer_id'        => $argv[2],
        'status'             => 'ABORTED',
        'action_status'      => 'ABORTED',
        'from_plan_state'    => 'Neutral',
        'to_plan_state'      => 'Port-In Requested',
        'real_customer'      => TRUE,
        'order_by'           => 'ORDER BY t.CREATED ASC'
      )
    );

    $aborted_transition = mssql_fetch_all_objects(logged_mssql_query($query));

    if (!( ( is_array($aborted_transition) ) && count($aborted_transition) ))
      die("No aborted transition found");

    $aborted_transition = $aborted_transition[0];

    print_r($aborted_transition);

    if ( $aborted_transition->ACTION_NAME != 'mvneRequestPortIn' )
      die("ACTION_NAME must be mvneRequestPortIn");

    // check the status of the ICCID on the network

    $mwControl = new \Ultra\Lib\MiddleWare\Adapter\Control;

    $data = array(
      'actionUUID' => time(),
      'iccid'      => $customer->CURRENT_ICCID_FULL,
      'msisdn'     => $customer->current_mobile_number
    );

    print_r( $data );

    $result = $mwControl->mwQuerySubscriber( $data );

    print_r($result);

    if ( $result->is_success() && isset($result->data_array['body']) )
    {
      echo "\nSubscriberStatus : ".$result->data_array['body']->SubscriberStatus."\n";

      if ( property_exists( $result->data_array['body'] , 'SIM' ) )
        echo "\nSIM : ".$result->data_array['body']->SIM."\n\n";

      if ( property_exists( $result->data_array['body'] , 'CurrentAsyncService' ) )
        die("CurrentAsyncService : ".$result->data_array['body']->CurrentAsyncService." !!!!");

      if ( $result->data_array['body']->SubscriberStatus != 'Active' )
        die("SubscriberStatus is not Active");

      $q = "update htt_customers_overlay_ultra set plan_state = 'Port-In Requested' where customer_id = ".$aborted_transition->CUSTOMER_ID;

      $check = run_sql_and_check($q);
      if ( $check ) { echo "SUCCESS\n"; } else { die("ERROR\n"); }

      $q = "update accounts set cos_id = ".$aborted_transition->TO_COS_ID." where customer_id = ".$aborted_transition->CUSTOMER_ID;

      $check = run_sql_and_check($q);
      if ( $check ) { echo "SUCCESS\n"; } else { die("ERROR\n"); }

      $context = array(
        'customer_id'   => $aborted_transition->CUSTOMER_ID
      );

      $change = change_state($context, TRUE, 'Port Provisioned', 'take transition', FALSE, 1);

      print_r($change);
    }
    else
      die("mwQuerySubscriber failed");

    exit;
  }
}


class Test_neutral_to_provisioned extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $argv[2]
        )
      )
    );

    print_r($r);

    echo "\n\n";

    if ( $r['state'] != 'Neutral' )
      die('customer state is not Neutral');

    $max_path_depth   = 1;
    $transition_name  = 'Provision '.$argv[3];
    $dry_run          = FALSE;
    $resolve_now      = TRUE;

    $context = array(
      'transition_id' => '',
      'customer_id'   => $argv[2]
    );


    $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    print_r($result_status);

    echo "\n\n";
  }
}


class Test_prefunded_to_active extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $argv[2]
        )
      )
    );

    print_r($r);

    echo "\n\n";

    if ( $r['state'] != 'Pre-Funded' )
      die('customer state is not Pre-Funded');

    $max_path_depth   = 1;
    $transition_name  = 'Activate Shipped SIM';
    $dry_run          = FALSE;
    $resolve_now      = TRUE;

    $context = array(
      'transition_id' => '',
      'customer_id'   => $argv[2]
    );


    $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    print_r($result_status);

    echo "\n\n";
  }
}


class Test_prefunded_to_portinreq extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $argv[2]
        )
      )
    );

    print_r($r);

    echo "\n\n";

    if ( $r['state'] != 'Pre-Funded' )
      die('customer state is not Pre-Funded');

    $max_path_depth   = 1;
    $transition_name  = 'SIM Received Request Port';
    $dry_run          = FALSE;
    $resolve_now      = TRUE;

    $context = array(
      'transition_id' => '',
      'customer_id'   => $argv[2]
    );

    $result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

    print_r($result_status);

    echo "\n\n";
  }
}


class Test_internal_func_get_state extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = internal_func_get_state(
      array(
        'states'            => NULL,
        'context'           => array(
          'customer_id'       => $argv[2]
        )
        #'given_plan'        => 1,
        #'given_state_name'  => 1
      )
    );

    echo "internal_func_get_state end\n";

    print_r($r);

    echo "\n\n";
  }
}


class Test_internal_func_resolve_pending_transitions extends AbstractTestStrategy
{
  function test()
  {
    $customer_id = NULL;

    $r = internal_func_resolve_pending_transitions( $customer_id );

    print_r($r);
  }
}


class Test_internal_func_check_transition extends AbstractTestStrategy
{
  function test()
  {
    $r = internal_func_check_transition(
      array(
        'transition_uuid' => '{ADB006D8-698C-8766-6A7E-3DE857A54703}'
      )
    );

    print_r($r);
  }
}


class Test_internal_func_get_all_pending_transitions extends AbstractTestStrategy
{
  function test()
  {
    $r = internal_func_get_all_pending_transitions();

    print_r($r);
  }
}


class Test_internal_func_get_pending_transitions extends AbstractTestStrategy
{
  function test()
  {
    $r = internal_func_get_pending_transitions(
      array(
        'customer_id' => 123
      )
    );

    print_r($r);
  }
}


class Test_internal_func_delayed_ensure_provisioning_or_activation extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = internal_func_delayed_ensure_provisioning_or_activation(18773);
    print_r($r);
  }

}


class Test_internal_func_delayed_ensure_suspend extends AbstractTestStrategy
{
  function test()
  {
    global $argv;
    
    if (empty($argv[2]) || ! is_numeric($argv[2]))
      echo "ERROR: missing or invalid parameter CUSTOMER_ID\n";
    else
    {
      $r = internal_func_delayed_ensure_suspend($argv[2]);
      print_r($r);
     }
  }
}


# perform test #


teldata_change_db(); // connect to the DB

$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


?>

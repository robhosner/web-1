<?php


// Utility I found very useful to re-do redoable transitions.
// It will skip CLOSED actions; it will re-OPEN ABORTED actions.

// Usage: php php test/state_machine/redo_transition.php $TRANSITION_UUID

include_once('db.php');
require_once('db/htt_transition_log.php');
require_once('Ultra/Lib/Util/Redis.php');
require_once('Ultra/Lib/Util/Redis/Transition.php');
require_once('Ultra/Lib/Util/Redis/Action.php');


teldata_change_db(); // connect to DB

$transition_uuid = "{".$argv[1]."}";

echo "transition_uuid = $transition_uuid\n";

$success = redo_transition ($transition_uuid);

?>

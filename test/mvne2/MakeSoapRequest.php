<?php

// constants
define('WSDL', 'runners/amdocs/TMobile_MVNO_3_15_development.wsdl');


// this will disable WSDL caching: only needed if you change WSDL file when testing
// ini_set('soap.wsdl_cache_ttl', 0);

// create UserDataObj object
$UserDataObj = new StdClass();
$UserDataObj->senderId = '15555555555';
$UserDataObj->timeStamp = date('c'); // important: ISO8601 format

// create NotificationReceived object
$NotificationReceived = new StdClass();
$NotificationReceived->UserData = $UserDataObj;
$NotificationReceived->MSISDN = '2122121122';
$NotificationReceived->serviceGrade = 'SERVICE';
$NotificationReceived->shortCode = 'CODE';
$NotificationReceived->messageType = 'TTYPE';
$NotificationReceived->smsText = 'Hello, World!';

try
{
  // create SOAP client instance
  $client = new SoapClient(WSDL);

  // this will describe the instance's interface
  // var_dump($client->__getFunctions());
  // var_dump($client->__getTypes());

  // call instance's function
  $Response = $client->NotificationReceived($NotificationReceived);

  // output result
  var_dump($Response);
}
catch(Exception $e)
{
  var_dump($e);
}

?>



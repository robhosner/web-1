<?php

require_once 'db.php';
require_once 'lib/transitions.php';

/*
MVNE consistency check

htt_customers_overlay_ultra.MVNE
htt_inventory_sim.MVNE
htt_ultra_msisdn.MVNE
*/

$id = $argv[1];

echo "$id\n";

if ( ! is_numeric($id) )
  die('numeric input needed');

teldata_change_db();

if ( strlen($id) > 17 )
{
  // ICCID

  $customer = get_customer_from_iccid($id);

  if ( !$customer )
    die('no customer found for this ICCID');

  echo 'customer_id = '.$customer->CUSTOMER_ID."\n";

  echo 'Customer MVNE = '.$customer->MVNE."\n";

  $sql = htt_inventory_sim_select_query( array( "iccid_full" => luhnenize($id) ) );

  $htt_inventory_sim_data = \Ultra\Lib\DB\fetch_objects($sql);

  if ( $htt_inventory_sim_data && is_array($htt_inventory_sim_data) && count($htt_inventory_sim_data) )
    echo 'ICCID    MVNE = '.$htt_inventory_sim_data[0]->MVNE."\n";
  else
    echo "ICCID    MVNE not found\n";

  if ( $customer->current_mobile_number )
  {
    $msisdn_data = get_ultra_msisdn_from_msisdn( $customer->current_mobile_number );

    if ( $msisdn_data )
      echo 'MSISDN   MVNE = '.$msisdn_data->MVNE."\n";
    else
      echo "MSISDN   MVNE not found\n";
  }
}
elseif ( strlen($id) > 9 )
{
  // MSISDN

  $msisdn_data = get_ultra_msisdn_from_msisdn( $id );

  if ( $msisdn_data )
    echo 'MSISDN   MVNE = '.$msisdn_data->MVNE."\n";
  else
    echo "MSISDN   MVNE not found\n";

  if ( $msisdn_data->CUSTOMER_ID )
  {
    $customer = get_customer_from_customer_id( $msisdn_data->CUSTOMER_ID );

    if ( !$customer )
      die('no customer found for this ICCID');

    echo 'customer_id = '.$customer->CUSTOMER_ID."\n";

    echo 'Customer MVNE = '.$customer->MVNE."\n";

    $sql = htt_inventory_sim_select_query( array( "iccid_full" => $customer->CURRENT_ICCID_FULL ) );

    $htt_inventory_sim_data = \Ultra\Lib\DB\fetch_objects($sql);

    if ( $htt_inventory_sim_data && is_array($htt_inventory_sim_data) && count($htt_inventory_sim_data) )
      echo 'ICCID    MVNE = '.$htt_inventory_sim_data[0]->MVNE."\n";
    else
      echo "ICCID    MVNE not found\n";
  }
}
else
{
  // CUSTOMER ID

  $customer = get_customer_from_customer_id( $id );

  if ( !$customer )
    die('no customer found for this ICCID');

  echo 'customer_id = '.$customer->CUSTOMER_ID."\n";

  echo 'Customer MVNE = '.$customer->MVNE."\n";

  $sql = htt_inventory_sim_select_query( array( "iccid_full" => $customer->CURRENT_ICCID_FULL ) );

  $htt_inventory_sim_data = \Ultra\Lib\DB\fetch_objects($sql);

  if ( $htt_inventory_sim_data && is_array($htt_inventory_sim_data) && count($htt_inventory_sim_data) )
    echo 'ICCID    MVNE = '.$htt_inventory_sim_data[0]->MVNE."\n";
  else
    echo "ICCID    MVNE not found\n";

  if ( $customer->current_mobile_number )
  {
    $msisdn_data = get_ultra_msisdn_from_msisdn( $customer->current_mobile_number );

    if ( $msisdn_data )
      echo 'MSISDN   MVNE = '.$msisdn_data->MVNE."\n";
    else
      echo "MSISDN   MVNE not found\n";
  }
}

/*

select u.CUSTOMER_ID,
       u.MVNE CUSTOMER_MVNE,
       i.MVNE SIM_MVNE,
       m.MVNE MSISDN_MVNE
from            HTT_CUSTOMERS_OVERLAY_ULTRA u
left outer join HTT_ULTRA_MSISDN            m
on              m.CUSTOMER_ID = u.CUSTOMER_ID
left outer join HTT_INVENTORY_SIM           i
on              i.ICCID_FULL = u.CURRENT_ICCID_FULL
WHERE
( u.MVNE != i.MVNE )
OR
( u.MVNE != m.MVNE )
OR
( i.MVNE != m.MVNE )



select u.CUSTOMER_ID, u.MVNE CUSTOMER_MVNE, i.MVNE SIM_MVNE, m.MVNE MSISDN_MVNE from            HTT_CUSTOMERS_OVERLAY_ULTRA u left outer join HTT_ULTRA_MSISDN            m on              m.CUSTOMER_ID = u.CUSTOMER_ID left outer join HTT_INVENTORY_SIM           i on              i.ICCID_FULL = u.CURRENT_ICCID_FULL WHERE ( u.MVNE != i.MVNE ) OR ( u.MVNE != m.MVNE ) OR ( i.MVNE != m.MVNE )
*/

?>

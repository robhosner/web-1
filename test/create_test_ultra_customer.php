<?php

include_once('lib/provisioning/functions.php');


# Example:
# php test/create_test_ultra_customer.php 'fname' 'lname' 98274 'Active' 2900 1001001000


if (
  ( ! isset( $argv[1] ) ) // first_name
  ||
  ( ! isset( $argv[2] ) ) // last_name
  ||
  ( $argv[1] == '' )
  ||
  ( $argv[2] == '' )
)
{
  die("You must provide first and last name as arguments.\n");
}

teldata_change_db(); // connect to the DB

$stored_value          = 0;
$cos_id                = 98274;
$plan_state            = 'test';
$current_mobile_number = 1001001000;

if ( isset( $argv[3] ) && $argv[3] ) // cos_id
{
  $cos_id = $argv[3];
}

if ( isset( $argv[4] ) && $argv[4] ) // plan_state
{
  $plan_state = $argv[4];
}

if ( isset( $argv[5] ) && $argv[5] ) // stored_value
{
  $stored_value = $argv[5];
}

if ( isset( $argv[6] ) && $argv[6] ) // current_mobile_number
{
  $current_mobile_number = $argv[6];
}

check_unique_msisdn( $current_mobile_number );

$params = array(
  'cos_id'      => $cos_id,
  'first_name'  => $argv[1],
  'last_name'   => $argv[2],

  'company'        => 'company 1',
  'address1'       => 'add 1',
  'address2'       => 'add 2',
  'city'           => 'New York',
  'state_region'   => 'NY',
  'postal_code'    => '12345',
  'country'        => 'USA',
  'local_phone'    => '1001001000',
  'fax'            => '1001001001',
  'e_mail'         => 'eb@example.com',
  'login_name'     => time(),
  'login_password' => 'swordfish',

  "preferred_language"    => 'EN',
  "notes"                 => 'test',
  "current_mobile_number" => $current_mobile_number,
  "current_iccid"         => '123456789012345678',
  "easypay_activated"     => 0,
  "stored_value"          => $stored_value,
  "plan_state"            => $plan_state,
  "plan_started"          => 'getutcdate()',
  "plan_expires"          => 'getutcdate()',
  "monthly_cc_renewal"    => 1
);

$r = create_ultra_customer($params);

print_r($r);


function check_unique_msisdn($msisdn)
{
  $sql = "select count(*) c from HTT_CUSTOMERS_OVERLAY_ULTRA where CURRENT_MOBILE_NUMBER = '$msisdn'";

  $query_result = mssql_fetch_all_objects(logged_mssql_query($sql));

  if ( ( is_array($query_result) ) && count($query_result) )
  {
    if ( $query_result[0]->c > 0 )
    {
      die("MSISDN $msisdn already in DB\n\n");
    }

    $test_iccid = $query_result[0]->iccid_full;
  }
  else
  {
    die("fatal DB error\n\n");
  }
}

?>

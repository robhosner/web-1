<?php


include_once('classes/Messenger.php');

date_default_timezone_set("America/Los_Angeles");

teldata_change_db();


# Usage:
# php test/db/test_htt_messaging_queue.php sms_internal_message $CUSTOMER_ID
# php test/db/test_htt_messaging_queue.php sms_internal_message_type $CUSTOMER_ID $TEMPLATE_NAME
# php test/db/test_htt_messaging_queue.php email_postageapp_template_name $CUSTOMER_ID
# php test/db/test_htt_messaging_queue.php sms_external_message $CUSTOMER_ID
# php test/db/test_htt_messaging_queue.php sms_external_message_type $CUSTOMER_ID


abstract class AbstractTestStrategy
{
  abstract function test($customer);
}


class Test_sms_internal_message extends AbstractTestStrategy
{
  function test($customer)
  {
    $r = htt_messaging_queue_add(
      array(
        'customer_id'                 => $customer->CUSTOMER_ID,
        'reason'                      => 'Test_sms_internal_message',
        'expected_delivery_datetime'  => 'getutcdate()',
        'next_attempt_datetime'       => 'getutcdate()',
        'type'                        => 'SMS_INTERNAL',
        'messaging_queue_params'      => array(
          'message' => 'Test_sms_internal_message',
          'plan_name' => 'Ultra $29',
          'plan_amount' => '29'
        )
      )
    );

    print_r($r);
  }
}


class Test_sms_internal_message_type extends AbstractTestStrategy
{
  function test($customer)
  {
    global $argv;

    $message_type = $argv[3];

    $params = array(
      'customer_id'                 => $customer->CUSTOMER_ID,
      'reason'                      => 'Test_sms_internal_message_type',
      'expected_delivery_datetime'  => 'getutcdate()',
      'next_attempt_datetime'       => 'getutcdate()',
      'type'                        => 'SMS_INTERNAL',
      'messaging_queue_params'      => array(
        'message_type'  => $message_type
      )
    );

    switch( $message_type )
    {
      case 'activate_provision':
        $params['messaging_queue_params']['plan_amount']  = '29';
        $params['messaging_queue_params']['plan_name']    = 'Ultra $29';
      break;
      case 'activate_planinfo':
        $params['messaging_queue_params']['plan_name']    = 'Ultra $29';
        $params['messaging_queue_params']['renewal_date'] = 'Jul 1st';
      break;
      case 'balanceadd_recharge':
        $params['messaging_queue_params']['renewal_date'] = 'Jul 1st';
        $params['messaging_queue_params']['reload_amount']= '$29.00';
      break;
      case 'balanceadd_reload':
        $params['messaging_queue_params']['gizmo_amount'] = '$10';
      break;
      case 'temp_password':
        $params['messaging_queue_params']['temp_password'] = '123456';
      break;
      case 'monthly_renewal':
      case 'balanceadd_resume':
        $params['messaging_queue_params']['renewal_date'] = 'Jul 1st';
      break;
      case 'plan_close_to_expiration':
      case 'unsuspend_activation':
      case 'renewal_plan_change':
        $params['messaging_queue_params']['plan_amount']  = '29';
        $params['messaging_queue_params']['renewal_date'] = 'Jul 1st';
      break;
      case 'plan_expires_today':
      case 'plan_suspended_now':
      case 'plan_suspended_two_days_ago':
        $params['messaging_queue_params']['plan_amount']  = '29';
      break;
      case 'pin_apply':
        $params['messaging_queue_params']['amount']       = '33.00';
      break;
      case 'activation':
      case 'activate_portin':
      case 'activate_newphone':
        $params['messaging_queue_params']['msisdn']       = '1001001000';
      break;

      // no params in those templates
      case 'activate_help';
      case 'activate_datahelp';
      case 'port_resolution_required':
      case 'auto_monthly_cc_renewal':
      case 'reset_voicemail_password':
      case 'porting':
      break;

      default:
        die("ERROR: message_type $message_type not known");
      break;
    }

    $r = htt_messaging_queue_add( $params );

    print_r($r);
  }
}


class Test_email_postageapp_template_name extends AbstractTestStrategy
{
  function test($customer)
  {
    $r = htt_messaging_queue_add(
      array(
        'customer_id'                 => $customer->CUSTOMER_ID,
        'reason'                      => 'test email',
        'expected_delivery_datetime'  => 'getutcdate()',
        'next_attempt_datetime'       => 'getutcdate()',
        'type'                        => 'EMAIL_POSTAGEAPP',
        'messaging_queue_params'      => array(
          'template_name' => 'ultra-auto-recharge-on',
          'email'         => $customer->E_MAIL,
          'first_name'    => $customer->FIRST_NAME,
          'last_name'     => $customer->LAST_NAME
        )
      )
    );

    print_r($r);
  }
}


class Test_sms_external_message extends AbstractTestStrategy
{
  function test($customer)
  {
    $r = htt_messaging_queue_add(
      array(
        'customer_id'                 => $customer->CUSTOMER_ID,
        'reason'                      => 'Test_sms_external_message',
        'expected_delivery_datetime'  => 'getutcdate()',
        'next_attempt_datetime'       => 'getutcdate()',
        'type'                        => 'SMS_EXTERNAL',
        'messaging_queue_params'      => array(
          'message'     => 'Test_sms_external_message'
        )
      )
    );

    print_r($r);
  }
}


class Test_sms_external_message_type extends AbstractTestStrategy
{
  function test($customer)
  {
    $r = htt_messaging_queue_add(
      array(
        'customer_id'                 => $customer->CUSTOMER_ID,
        'reason'                      => 'Test_sms_external_message',
        'expected_delivery_datetime'  => 'getutcdate()',
        'next_attempt_datetime'       => 'getutcdate()',
        'type'                        => 'SMS_EXTERNAL',
        'messaging_queue_params'      => array(
          'message_type' => 'port_resolution_required'
        )
      )
    );

    print_r($r);
  }
}


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$customer_id = $argv[2];

$customer = get_customer_from_customer_id($customer_id);

$testObject->test($customer);


?>

<?php

require_once 'db.php';
require_once 'db/ultra_intl_fixed.php';

teldata_change_db();

$input = $argv[1];

echo "input = $input\n";

if ( strlen($input) < 10 )
  add_customer_id_to_ultra_intl_fixed( $input );
else
  add_msisdn_to_ultra_intl_fixed( $input );


?>

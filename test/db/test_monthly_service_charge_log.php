<?php


include_once('db.php');
include_once('db/monthly_service_charge_log.php');

/*
php test/db/test_monthly_service_charge_log.php remove_htt_monthly_service_charge_log $HTT_MONTHLY_SERVICE_CHARGE_LOG_ID
*/

teldata_change_db(); // connect to DB


class Test_remove_htt_monthly_service_charge_log
{
  function test()
  {
    global $argv;

    $id = $argv[2];

    $sql = sprintf(
      "delete from HTT_MONTHLY_SERVICE_CHARGE_LOG_ERRORS where HTT_MONTHLY_SERVICE_CHARGE_LOG_ID = %d",
      $id
    );

    if ( ! run_sql_and_check($sql) )
      die("ERROR");

    $sql = sprintf(
      "delete from HTT_MONTHLY_SERVICE_CHARGE_LOG where HTT_MONTHLY_SERVICE_CHARGE_LOG_ID = %d",
      $id
    );

    if ( ! run_sql_and_check($sql) )
      die("ERROR");
  }
}


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


?>

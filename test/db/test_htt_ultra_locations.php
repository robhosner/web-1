<?php

// test for perform_htt_ultra_locations_temp_import

include_once('db/htt_ultra_locations.php');


teldata_change_db();
#$errors = TEST_perform_htt_ultra_locations_import();
$errors = perform_htt_ultra_locations_import();
#print_r($errors);
exit;


// mimic function perform_htt_ultra_locations_import
function TEST_perform_htt_ultra_locations_import()
{
  $errors = array();

  if ( TEST_perform_htt_ultra_locations_temp_import() )
  {
    list( $errors , $duplicate_location_ids ) = perform_htt_ultra_locations_temp_cleanup($errors);

    if ( ! count($errors) )
    {
      $errors = perform_htt_ultra_locations_temp_switch($errors,$duplicate_location_ids);
    }
  }
  else
  {
    $errors[]= "perform_htt_ultra_locations_temp_import error";
  }

  return $errors;
}

// mimic function perform_htt_ultra_locations_temp_import
function TEST_perform_htt_ultra_locations_temp_import()
{
  $sql = set_mssql_options().
         htt_ultra_locations_create_temp_table();

  dlog('',$sql);

  $check = run_sql_and_check_result($sql);

  if ( ! $check ) { die('TEST_perform_htt_ultra_locations_temp_import ERROR'); }

  $i = 1;

  while ($i <= 500)
  {
    $sql = TEST_htt_ultra_locations_temp_import($i);

    dlog('',$sql);

    $check = run_sql_and_check_result($sql);

    if ( ! $check ) { die('TEST_htt_ultra_locations_temp_import ERROR'); }

    $i++;
  }

  return $check;
}

function TEST_htt_ultra_locations_temp_import($i)
{
  $j = $i;

  if ( $j == 10 ) { $j = 9; } // TEST for duplicate address

  return '
INSERT INTO #TEMP_HTT_ULTRA_LOCATIONS (
 LOCATION_ID,
 RETAILER_NAME,
 ADDRESS1,
 ADDRESS2,
 CITY,
 STATE,
 ZIPCODE,
 RETAILER_CONTACT,
 RETAILER_PHONE_NUMBER,
 RETAILER_FAX_NUMBER,
 RETAILER_EMAIL_ADDRESS,
 LOCATION,
 LOCATION_TYPE,
 ACTIVE,
 EXPIRES_DATE,
 LOCATION_SOURCE,
 SOURCE_ID
) VALUES (
 '.($i+100).',
 \'SONTEK MOBILE\',
 \''.($j+100).' EAST RIDGE ROAD\',
 NULL,
 \'ROCHESTER\',
 \'NY\',
 \'14621\',
 \'MEHMET TURK\',
 \'5853604'.($i+100).'\',
 NULL,
 NULL,
 NULL,
 \'DEALER\',
 1,
 NULL,
 \'PORTAL_DEALER\',
 \'1138\'
);
  ';
}

?>

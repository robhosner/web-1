<?php

# php test/db/test_htt_billing_actions.php htt_billing_actions_select_query $STATUS $CUSTOMER_ID $TRANSACTION_ID
# php test/db/test_htt_billing_actions.php htt_billing_actions_update_query $STATUS_FROM $STATUS_TO $CUSTOMER_ID $TRANSACTION_ID $AMOUNT
# php test/db/test_htt_billing_actions.php htt_billing_actions_insert_query $CUSTOMER_ID $REQUEST_ID $STATUS $ATTEMPT_LOG $TRANSACTION_ID $PRODUCT_ID $SUBPRODUCT_ID $NEXT_ATTEMPT $AMOUNT $SKU

include_once 'db.php';
include_once 'db/htt_billing_actions.php';

function test_htt_billing_actions_select_query()
{
  $params = array();
  $params['status']         = (isset($argv[2])) ? $argv[2] : 'OPEN';
  $params['customer_id']    = (isset($argv[3])) ? $argv[3] : 3600;
  $params['transaction_id'] = (isset($argv[4])) ? $argv[4] : 't';

  echo htt_billing_actions_select_query($params) . "\n";
}

function test_htt_billing_actions_update_query()
{
  $params = array();
  $params['status_from']    = (isset($argv[2])) ? $argv[2] : 'FROM';
  $params['status_to']      = (isset($argv[3])) ? $argv[3] : 'TO';
  $params['customer_id']    = (isset($argv[4])) ? $argv[4] : '3600';
  $params['transaction_id'] = (isset($argv[5])) ? $argv[5] : 't';
  $params['amount']         = (isset($argv[6])) ? $argv[6] : 4400;

  echo htt_billing_actions_update_query($params) . "\n";
}

function test_htt_billing_actions_insert_query()
{
  $params = array();
  $params['customer_id']    = (isset($argv[2])) ? $argv[2] : 3600;
  $params['request_id']     = (isset($argv[3])) ? $argv[3] : 'r';
  $params['status']         = (isset($argv[4])) ? $argv[4] : 'test';
  $params['attempt_log']    = (isset($argv[5])) ? $argv[5] : 'test';
  $params['transaction_id'] = (isset($argv[6])) ? $argv[6] : 1;
  $params['product_id']     = (isset($argv[7])) ? $argv[7] : 1;
  $params['subproduct_id']  = (isset($argv[8])) ? $argv[8] : 1;
  $params['next_attempt']   = (isset($argv[9])) ? $argv[9] : 'dateadd(ss, 120, getutcdate())';
  $params['amount']         = (isset($argv[10])) ? $argv[10] : 1000;
  $params['provider_sku']   = (isset($argv[11])) ? $argv[11] : '890123456789';

  echo htt_billing_actions_insert_query($params) . "\n";
}

$test = $argv[1];

if (empty($argv[1]))
{
  echo "ERROR: no test name given\n";
  return;
}

$test = 'test_' . $argv[1];
if (!is_callable($test))
{
  echo "ERROR: function '$test' does not exist\n";
  return;
}

teldata_change_db();
$test();
echo "\n";

?>

<?php

require_once 'db.php';
require_once 'db/ultra_user_error_messages.php';


teldata_change_db();


$languages = array('IT','EN','ES','ZH');

foreach ( $languages as $language )
{
  $errors = get_ultra_user_error_messages($language);

  print_r($errors);
}


<?php

/*

php test/db/test_customer_misc.php test_get_customer_time_zone $CUSTOMER_ID
php test/db/test_customer_misc.php is_dev_customer
php test/db/test_customer_misc.php is_dev_iccid
php test/db/test_customer_misc.php test_get_customer_from_msisdn $MSISDN
php test/db/test_customer_misc.php test_get_customer_from_customer_id $CUSTOMER_ID
php test/db/test_customer_misc.php test_make_customer_where_clause_from_conditions
php test/db/test_customer_misc.php test_make_find_ultra_customers_from_details
php test/db/test_customer_misc.php test_make_signup_query $CUSTOMER_NUMBER

*/

require_once('db.php');
require_once('lib/transitions.php');

function test_make_customer_where_clause_from_conditions()
{
  $break = "\n\n *** \n\n";

  $conds = array('customer_stored_value_gte' => '_renewal_cost_');
  $query = make_customer_where_clause_from_conditions($conds);
  echo $query . $break;

  $conds = array('customer_stored_value_lt' => '_renewal_cost_');
  $query = make_customer_where_clause_from_conditions($conds);
  echo $query . $break;

  $conds = array('customer_balance_gte' => '_renewal_cost_');
  $query = make_customer_where_clause_from_conditions($conds);
  echo $query . $break;

  $conds = array('customer_balance_lt' => '_renewal_cost_');
  $query = make_customer_where_clause_from_conditions($conds);
  echo $query . $break;
}

function test_get_customer_from_customer_id()
{
  global $argv;

  if (empty($argv[2]))
    echo "ERROR: no CUSTOMER_ID given\n";

  $customer = get_customer_from_customer_id($argv[2]);

  print_r($customer);
}

/**
 * make_find_ultra_customers_from_plan_and_date_range
 */
function test_make_find_ultra_customers_from_plan_and_date_range()
{
  $ultra_states = get_all_states();
  print_r($ultra_states);

  $date_from = '2014-11-29';
  $date_to   = '2015-01-29';

  // loop through plan states
  foreach( $ultra_states as $plan_state )
  {
    $sql = make_find_ultra_customers_from_plan_and_date_range(
      array(
        'plan_state' => $plan_state,
        'date_from'  => $date_from,
        'date_to'    => $date_to,
        'dealer'     => 7038
      )
    );

    print_r($sql);

    echo PHP_EOL; echo PHP_EOL;

    $result = mssql_query($sql);

    $data = mssql_fetch_all_objects($result);

    print_r($data);

    echo PHP_EOL; echo PHP_EOL;
  }
}


/**
 * make_find_ultra_customers_from_details
 */
function test_make_find_ultra_customers_from_details()
{
  $sql = make_find_ultra_customers_from_details(
    array(
      'first_name' => 'sean',
      'dealer'     => 7038
    )
  );
  print_r($sql);
  echo PHP_EOL; echo PHP_EOL;
  $result = mssql_query($sql);
  $data = mssql_fetch_all_objects($result);
  print_r($data);
  echo PHP_EOL; echo PHP_EOL;

  $sql = make_find_ultra_customers_from_details(
    array(
      'last_name' => 'Ultrasmith'
    )
  );
  print_r($sql);
  echo PHP_EOL; echo PHP_EOL;
  $result = mssql_query($sql);
  $data = mssql_fetch_all_objects($result);
  print_r($data);
  echo PHP_EOL; echo PHP_EOL;

  $sql = make_find_ultra_customers_from_details(
    array(
      'email' => 'sean@ultra.me'
    )
  );
  print_r($sql);
  echo PHP_EOL; echo PHP_EOL;
  $result = mssql_query($sql);
  $data = mssql_fetch_all_objects($result);
  print_r($data);
  echo PHP_EOL; echo PHP_EOL;

}


/**
 * get_customer_from_msisdn
 */
function test_get_customer_from_msisdn()
{
  global $argv;

  if (empty($argv[2]))
    echo "ERROR: no msisdn given\n";

  $customer = get_customer_from_msisdn($argv[2]);

  print_r($customer);
}


/**
 * test_is_dev_customer
 */
function test_is_dev_customer()
{
  // positive control
  if ( is_dev_customer(3647) )
    echo "DEV\n";
  else
    echo "not-DEV\n";

  // negative control
  if ( is_dev_customer(18671) )
    echo "DEV\n";
  else
    echo "not-DEV\n";

  if ( is_dev_customer(NULL) )
    echo "DEV\n";
  else
    echo "not-DEV\n";
}


/**
 * test_is_dev_iccid
 */
function test_is_dev_iccid()
{
  // positive control
  if ( is_dev_iccid('8901260962113607409') )
    echo "DEV\n";
  else
    echo "not-DEV\n";

  // negative control
  if ( is_dev_iccid('8901260842116130932') )
    echo "DEV\n";
  else
    echo "not-DEV\n";

  if ( is_dev_iccid(NULL) )
    echo "DEV\n";
  else
    echo "not-DEV\n";
}


/**
 * test_find_ultra_customers_from_plan_and_date_range
 */
function test_find_ultra_customers_from_plan_and_date_range()
{
  // child dealer 7036 has 2 active subs
  $params = array(
    'dealer'        => 7036,
    'plan_state'    => 'Active',
    'date_from'     => '1 may 2014',
    'date_to'       => '5 june 2014',
    'select_attributes' => array(
      "'' cancelled_date",
      "CASE WHEN CREATION_DATE_TIME IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', CREATION_DATE_TIME ) END created_date",
      "CASE WHEN PLAN_EXPIRES       IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', PLAN_EXPIRES       ) END service_expires_date",
      "CASE WHEN port_status_date   IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', port_status_date   ) END port_status_date",
      "CASE WHEN plan_started       IS NULL THEN '' ELSE DATEDIFF(ss, '1970-01-01', plan_started       ) END plan_started",
      'u.CUSTOMER_ID         customer_id',
      'CURRENT_MOBILE_NUMBER msisdn',
      'CURRENT_ICCID_FULL    iccid',
      'E_MAIL                email',
      'first_name',
      'last_name',
      'a.cos_id',
      'plan_state',
      'balance',
      'stored_value',
      'monthly_cc_renewal',
      'p.port_status',
      'p.port_query_status'));
  $result = find_ultra_customers_from_plan_and_date_range($params);
  echo 'Found ' . count($result->get_data_array()) . " records\n";
  // print_r($result->get_data_array());

  // parent dealer has 2 active subs but children have 4 more
  $params['dealer'] = 7038;
  $result = find_ultra_customers_from_plan_and_date_range($params);
  echo 'Found ' . count($result->get_data_array()) . " records\n";

}


/**
 * test_find_ultra_customers_from_details
 */
function test_find_ultra_customers_from_details()
{

  // child dealer 7036 has 4 subs with this last name
  $params = array(
    'dealer'      => 7036,
    'last_name'   => 'Enright');
  $result = find_ultra_customers_from_details($params);
  echo 'Found ' . count($result->get_data_array()) . " records\n";

  // parent dealer 7038 has also 4 subs but children have 6 more
  $params['dealer'] = 7038;
  $result = find_ultra_customers_from_details($params);
  echo 'Found ' . count($result->get_data_array()) . " records\n";

}


/**
 * test_get_customer_time_zone
 */
function test_get_customer_time_zone()
{ global $argv;

  if (empty($argv[2]))
    echo "ERROR: no customer given\n";
  else
  {
    $customer_id = $argv[2];
    $hours = get_customer_time_zone($customer_id);
    echo "Customer: $customer_id, UTC difference: $hours hrs\n";
  }
}


/**
 * test_get_expiring_subscribers_from_dealer
 */
function test_get_expiring_subscribers_from_dealer()
{
  global $argv;

  $dealer = $argv[2];
  $days_since_suspended = $argv[3];
  $days_until_expire = $argv[4];
  $subscribers = get_expiring_subscribers_from_dealer($dealer, $days_since_suspended, $days_until_expire);
  print_r($subscribers);
}


/**
 * test_make_signup_query
 */
function test_make_signup_query()
{
  global $argv;

  if (empty($argv[2]))
    echo "ERROR: missing argument customer number\n";
  else
    echo make_signup_query($argv[2]);
}


if (empty($argv[1]))
{
  echo "ERROR: no test name given\n";
  return;
}

$test = $argv[1];
if ( ! is_callable($test))
{
  echo "ERROR: function '$test' does not exist\n";
  return;
}

// run the test
teldata_change_db();
echo "$test\n";
$test();
echo "\n";

?>

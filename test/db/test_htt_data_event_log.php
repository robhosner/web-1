<?php

require_once 'db.php';
require_once 'db/htt_data_event_log.php';

teldata_change_db();

$params = array(
  'customer_id'   => 19380,
  'action'        => 'Event 30',
  'soc'           => 'Base Data Usage',
  'value'         =>  123456);
$result = log_data_event($params);
echo $result ? 'SUCCESS' : 'FAILURE';
echo "\n";

$r = get_latest_data_event_log($params['customer_id']);
print_r( $r );

?>

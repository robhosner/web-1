<?php

require_once 'db.php';


/*
  http://php.net/manual/en/book.pdo.php
  http://php.net/manual/en/class.pdo.php  
  http://php.net/manual/en/ref.pdo-dblib.php

  Usage
    // e.g. 1: explicit database connection
    if (! pdoConnect(PDO_ULTRA_DATA)) // sets default database connection for subsequent calls
      throw new exception('Failed to connect to database');
    $data = pdoSelect('SELECT ...'); // no database connection specified because we have default

    // e.g. 2: background database connection
    $data = pdoSelect('SELECT ...', PDO_ULTRA_DATA);
  
    // e.g. 2: working with multiple databases
    $customer = pdoSelect('SELECT ...', PDO_ULTRA_DATA);
    $soap_log = pdoSelect('SELECT ...', PDO_ULTRA_ACC);


  TODO:
    check TRANSACTION state when switching databases
    configure /etc/freetds.conf
*/

use \PDO;


// Ultra databases
define('PDO_ULTRA_DATA', 1);
define('PDO_ULTRA_ACC', 2);

// globals
$gPdoConnections = array(PDO_ULTRA_DATA => NULL, PDO_ULTRA_ACC => NULL); // array of database connections
$gPdoCurrent = NULL; // current connection index
$gPdoDebug = TRUE; // enable heavy development debugging


$start = microtime(TRUE);
echo "START: " . memory_get_usage() . "\n";

for ($i = 0; $i < 4; $i++)
{
  // connect
  if (! pdoConnect(PDO_ULTRA_DATA, FALSE))
    return NULL;

  $accounts = pdoSelect('SELECT ACCOUNT_ID, CUSTOMER_ID, COS_ID FROM ACCOUNTS WHERE CUSTOMER_ID IS NOT NULL');
  echo "fetched " . count($accounts) . " accounts \n";

  $customers = pdoSelect('SELECT * FROM HTT_CUSTOMERS_OVERLAY_ULTRA');
  echo "fetched " . count($customers) . " customers \n";

  foreach ($customers as $customer)
  {
    $pref = pdoSelect("SELECT * FROM HTT_PREFERENCES_MARKETING WHERE CUSTOMER_ID = {$customer->CUSTOMER_ID}");
    pdoBeginTransaction();
    if (count($pref))
    {
      if (rand() % 2)
      {
        echo "pref update customer {$customer->CUSTOMER_ID} \n";
        $result = pdoExecute('UPDATE HTT_PREFERENCES_MARKETING SET MARKETING_EMAIL_OPTION = ' .
          ($pref[0]->MARKETING_EMAIL_OPTION ? 0 : 1) . ' WHERE CUSTOMER_ID = ' . $customer->CUSTOMER_ID);
      }
      else
      {
        echo "pref delete customer {$customer->CUSTOMER_ID} \n";
        $result = pdoExecute('DELETE FROM HTT_PREFERENCES_MARKETING WHERE CUSTOMER_ID = ' . $customer->CUSTOMER_ID);
      }
    }
    else
    {
      echo "pref insert customer {$customer->CUSTOMER_ID} \n";
      $result = pdoExecute("INSERT INTO HTT_PREFERENCES_MARKETING (CUSTOMER_ID, LAST_UPDATED, MARKETING_EMAIL_OPTION, MARKETING_SMS_OPTION)
        VALUES ({$customer->CUSTOMER_ID}, getutcdate(), 0, 0)");
    }
    pdoCommitTransaction();
    $soap = pdoSelect("select * from COMMAND_INVOCATIONS with (nolock) where CUSTOMER_ID = {$customer->CUSTOMER_ID}", PDO_ULTRA_ACC);
    echo "result: " . count($soap) . " rows \n";
  }

  $result = pdoSelect("select top 10 SOAP_LOG_ID from SOAP_LOG with (nolock) order by SOAP_LOG_ID", PDO_ULTRA_ACC);
  echo "result: " . count($result) . "rows \n";

  $result = pdoExecute("UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET NOTES='Test Update Note' WHERE PLAN_STATE='Cancelled'");
  echo sprintf('update notes 1: %s', print_r($result, TRUE));

  $msisdns = pdoSelect('SELECT MSISDN, CUSTOMER_ID, LAST_TRANSITION_UUID, MVNE, * FROM HTT_ULTRA_MSISDN');
  echo "fetched " . count($msisdns) . " MSISDNs \n";

  $result = pdoExecute("UPDATE HTT_CUSTOMERS_OVERLAY_ULTRA SET NOTES='Update Test Note' WHERE PLAN_STATE='Cancelled'");
  echo sprintf('update notes 2: %s', print_r($result, TRUE));

  $locations = pdoSelect('SELECT TOP 10000 * FROM HTT_ULTRA_LOCATIONS');
  echo "fetched " . count($locations) . " locations \n";

}

echo "END: " . memory_get_usage() . ", time: " . (microtime(TRUE) - $start) . "\n";


/**
 * pdoConnect
 * connect to environment-defined database using PDO driver dblib
 * @param database definition
 * @param boolean TRUE: enable (heavy) debugging, FALSE: disable debugging, NULL: use current value
 * @param boolean current: make this the default connection for subsequent queries
 * @return boolean TRUE on success, FALSE on failure
 */
function pdoConnect($definition, $debug = NULL, $default = TRUE)
{
  global $gPdoConnections;
  global $gPdoCurrent;
  global $gPdoDebug;
  $success = FALSE;

  if ($debug)
    $gPdoDebug = $debug;

  if ($gPdoDebug)
    dlog('', '%s', func_get_args());

  try
  {
    // check parameter
    if (empty($definition) || ! in_array($definition, array(PDO_ULTRA_DATA, PDO_ULTRA_ACC)))
      throw new Exception("invalid database definition '$definition'");

    // check if connection already exists
    if ($gPdoConnections[$definition])
    {
      if ($gPdoDebug)
        dlog('', "re-using existing connection $definition");

      if ($default)
        $gPdoCurrent = $definition;

      return TRUE;
    }

    // fetch connection parameters
    switch ($definition)
    {
      case PDO_ULTRA_DATA:
        $host = pdoGetCredential('host');
        $port = pdoGetCredential('port');
        $database = pdoGetCredential('database');
        $username = pdoGetCredential('username');
        $password = pdoGetCredential('password');
        break;

      case PDO_ULTRA_ACC:
        $host = 'sandy.hometowntelecom.com';
        $port = pdoGetCredential('port');
        $database = 'ULTRA_ACC';
        $username = pdoGetCredential('username');
        $password = pdoGetCredential('password');
        break;

      default:
        throw new Exception("uknown database definition '$definition'");
    }

    // verify connection parameters
    if ( ! $host || ! $port || ! $database || ! $username || ! $password)
      throw new Exception('missing one or more connection parameters');

    // compose full DSN
    $dsn = "dblib:dbname=$database;host=$host:$port";
    if ($gPdoDebug)
      dlog('', "DSN: $dsn, username: $username, password: $password");

    // connect
    $options  = array(
      PDO::ATTR_CASE    => PDO::CASE_UPPER, // normalize schema case
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION); // throw exceptions on all DB errors
    $connection = new PDO($dsn, $username, $password, $options); // connect
    if ($gPdoDebug)
      dlog('', 'successfully connected to database definition ' . $definition);

    // save connection to global
    $gPdoConnections[$definition] = $connection;

    // set as default connection for future PDO calls
    if ($default)
      $gPdoCurrent = $definition;

    $success = TRUE;
  }
  catch(\Exception $e)
  {
    dlog('', 'EXCEPTION: ' . $e->getMessage());
  }

  return $success;
}


/**
 * pdoGetCredential
 * determine database credential from user environment
 * @param string credential name
 * @return string credential or NULL if not found
 */
function pdoGetCredential($name)
{
  global $gPdoDebug;

  if ($gPdoDebug)
    dlog('', '%s', func_get_args());

  $enviro = array(
    'host'      => 'OVERRIDE_DB_HOST',
    'port'      => 'OVERRIDE_DB_PORT',
    'database'  => 'OVERRIDE_DB_NAME',
    'username'  => 'OVERRIDE_DB_USER',
    'password'  => 'OVERRIDE_DB_PWD');

  $config = array(
    'host'      => 'db/host',
    'port'      => 'db/port',
    'database'  => 'db/name',
    'username'  => 'db/user',
    'password'  => '');

  // determine credential value
  if (! $value = getenv($enviro[$name]))
    if (! $value = \Ultra\UltraConfig\find_config($config[$name]))
    {
      dlog('', "FATAL: cannot determine credential $name");
      return NULL;
    }

  if ($gPdoDebug)
    dlog('', "return '$value'");

  return $value;
}


/**
 * pdoInterpretCode
 * given MSSQL error code return a meaningfull error message
 * @param int error code
 * @return string message
 */
function pdoInterpretCode($code)
{
  global $gPdoDebug;

  switch ($code)
  {
    case 102:
    case 156:
    case 319:   $message = 'syntax error'; break;
    case 208:   $message = 'invalid object name'; break;
    case 2627:  $message = 'constraint violation: cannot insert duplicate key'; break;
    case 4004:  $message = 'ntext is not supported by this driver (CAST AS TEXT)'; break;
    default:    $message = "undefined error message for code $code";
  }

  if ($gPdoDebug)
    dlog('', 'code %s -> %s', func_get_args(), $message);

  return $message;
}


/**
 * pdoSelect
 * execute a SELECT SQL statement on current or given database connection and return its results
 * @param string SQL statement
 * @param database definition (current database connection will be used if omitted)
 * @return array of objects of table rows or NULL on failure
 */
function pdoSelect($sql, $definition = NULL)
{
  global $gPdoDebug;

  if ($gPdoDebug)
    dlog('', '%s', func_get_args());

  try
  {
    // get database connection
    if (! $connection = pdoGetConnection($definition))
      throw new Exception('missing database connection');

    // execute SELECT and fetch result as an array of objects
    $statement = $connection->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_OBJ);

    if ($gPdoDebug)
      dlog('', 'fetchAll returned ' . count($result) . ' rows');
  }
  catch (\Exception $e)
  {
    $message = empty($e->errorInfo[1]) ? $e->getMessage() : pdoInterpretCode($e->errorInfo[1]);
    dlog('', "EXCEPTION '$message' on '$sql'");
    $result = NULL;
  }

  return $result;
}


/**
 * pdoExecute
 * execute a SELECT SQL statement on current or given database connection and return error code
 * @param string SQL statement
 * @param database definition (current database connection will be used if omitted)
 * @return array of objects of table rows or NULL on failure
 */
function pdoExecute($sql, $definition = NULL)
{
  global $gPdoDebug;
  $result = NULL;

  if ($gPdoDebug)
    dlog('', '%s', func_get_args());

  try
  {
    // get database connection
    if (! $connection = pdoGetConnection($definition))
      throw new Exception('missing database connection');

    // execute statement and get error code
    $statement = $connection->prepare($sql);
    $statement->execute();
    $result = (int)$statement->errorCode();

    if ($gPdoDebug)
      dlog('', "execute returned $result code");
  }
  catch (\Exception $e)
  {
    $result = $e->errorInfo[1];
    $message = empty($result) ? $e->getMessage() : pdoInterpretCode($result);
    dlog('', "EXCEPTION '$message' on '$sql'");
  }

  return $result;
}


/**
 * pdoGetConnection
 * connection manager for most PDO calls to manage existing connections
 * @param database definition or NULL for current database connection
 * @return object database connection for subsequent operations
 */
function pdoGetConnection($definition = NULL)
{
  global $gPdoConnections;
  global $gPdoCurrent;
  global $gPdoDebug;
  $result = NULL;

  if ($gPdoDebug)
    dlog('', '%s', func_get_args());

  try
  {
    // check connection for the given database definition
    if ($definition)
    {
      if (empty($gPdoConnections[$definition])) // create connection if none yet
      {
        if ($gPdoDebug)
          dlog('', "attempting to established a connection for definition $definition");

        if ( ! pdoConnect($definition, NULL, FALSE))
          throw new Exception('failed query due to missing database connection');
      }
      else
      {
        if ($gPdoDebug)
          dlog('', "using existing connection $definition");
      } 
    }
    else // not definition given: wants to use the current connection
    {
      if ( ! $gPdoCurrent)
        throw new Exception('no current database connection exists');

      if ($gPdoDebug)
        dlog('', "using current connection $gPdoCurrent");
      $definition = $gPdoCurrent;
    }

    $result = $gPdoConnections[$definition];
  }
  catch (\Exception $e)
  {
    $message = empty($e->errorInfo[1]) ? $e->getMessage() : pdoInterpretCode($e->errorInfo[1]);
    dlog('', "EXCEPTION '$message' on '$sql'");
  }

  return $result;
}


/**
 * pdoBeginTransaction
 * start transaction on the default or specific database connection
 * @param database definition
 * @return boolean TRUE on success, FALSE on failure
 */
function pdoBeginTransaction($definition = NULL)
{
  global $gPdoDebug;
  $result = FALSE;

  if ($gPdoDebug)
    dlog('', '%s', func_get_args());

  try
  {
    if (! $connection = pdoGetConnection($definition))
      return $result;
    $result = $connection->beginTransaction();
  }
  catch (\Exception $e)
  {
    $message = empty($e->errorInfo[1]) ? $e->getMessage() : pdoInterpretCode($e->errorInfo[1]);
    dlog('', "EXCEPTION '$message'");
  }

  return $result;
}


/**
 * pdoCommitTransaction
 * end transaction on the default or specific database connection
 * @param database definition
 * @return boolean TRUE on success, FALSE on failure
 */
function pdoCommitTransaction($definition = NULL)
{
  global $gPdoDebug;
  $result = FALSE;

  if ($gPdoDebug)
    dlog('', '%s', func_get_args());

  try
  {
    if (! $connection = pdoGetConnection($definition))
      return $result;
    $result = $connection->commit();
  }
  catch (\Exception $e)
  {
    $message = empty($e->errorInfo[1]) ? $e->getMessage() : pdoInterpretCode($e->errorInfo[1]);
    dlog('', "EXCEPTION '$message'");
  }

  return $result;
}

?>

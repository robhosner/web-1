<?php

/*
TODO: Use https://github.com/fzaninotto/Faker
*/

function generateTestData()
{
  return [
    'order_id'    => generateOrderId(),
    'ICCID'       => generateICCID(),
    'first_name'  => generateStringName(),
    'last_name'   => generateStringName(),
    'email'       => generateEmail(),
    'address1'    => generateStringName(),
    'address2'    => generateStringName(),
    'city'        => generateStringName(),
    'state'       => 'CA',
    'postal_code' => generateZip(),
    'token'       => generateStringName(),
    'bin'         => generateDigits(6),
    'last_four'   => generateDigits(4),
    'expires_date'       => generateDigits(4),
    'gateway'            => generateStringName(),
    'merchant_account'   => 1,
    'avs_validation'     => 'Y',
    'cvv_validation'     => 'Y',
    'auto_enroll'        => 1,
    'customer_ip'        => '111.111.111.111',
    'preferred_language' => 'EN'
  ];

}


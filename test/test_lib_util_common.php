<?php


# Usage:
# php test/test_lib_util_common.php get_utc_date_formatted_for_mssql
# php test/test_lib_util_common.php strvalArrayValues
# php test/test_lib_util_common.php map_array_keys
# php test/test_lib_util_common.php get_all_http_headers
# php test/test_lib_util_common.php json_decode_nice [$JSON] [$ASSOC]
# php test/test_lib_util_common.php indent__JSON [$JSON]
# php test/test_lib_util_common.php enclose_JSON [$JSON]
# php test/test_lib_util_common.php disclose_JSON [$JSON]
# php test/test_lib_util_common.php dlog_allowed_regexp
# php test/test_lib_util_common.php getReferringIPAddress
# php test/test_lib_util_common.php dlog
# php test/test_lib_util_common.php get_mcrypt_encrypt_key
# php test/test_lib_util_common.php perform_mcrypt_encryption $STRING
# php test/test_lib_util_common.php perform_mcrypt_decryption $ENCRYPTED
# php test/test_lib_util_common.php getNewTransitionUUID
# php test/test_lib_util_common.php getNewActionUUID
# php test/test_lib_util_common.php create_guid
# php test/test_lib_util_common.php luhn_checksum $NUMBER
# php test/test_lib_util_common.php luhn_checksum_check $NUMBER
# php test/test_lib_util_common.php luhn_append $NUMBER
# php test/test_lib_util_common.php curry
# php test/test_lib_util_common.php canonify $STRING
# php test/test_lib_util_common.php eval_dangerous $CODE $VARS
# php test/test_lib_util_common.php debug_json_decode
# php test/test_lib_util_common.php mssql_escape_raw $DATA
# php test/test_lib_util_common.php mssql_escape $DATA
# php test/test_lib_util_common.php mssql_escape_with_zeroes $DATA
# php test/test_lib_util_common.php mssql_escape_or_null $DATA
# php test/test_lib_util_common.php clause_mssql_escape_or_null $DATA
# php test/test_lib_util_common.php get_htt_env
# php test/test_lib_util_common.php var_log
# php test/test_lib_util_common.php stack_log
# php test/test_lib_util_common.php caption_amount $AMOUNT
# php test/test_lib_util_common.php get_date_from_full_date $DATE
# php test/test_lib_util_common.php fix_transition_uuid_format $REQUEST_ID
# php test/test_lib_util_common.php get_customer_from_zsession $CUSTOMER_ID
# php test/test_lib_util_common.php validate_date_dd_mm_yyyy $DATE
# php test/test_lib_util_common.php validate_port_account_number $NUMBER
# php test/test_lib_util_common.php validate_username_characters $USERNAME
# php test/test_lib_util_common.php validate_unique_username $USERNAME
# php test/test_lib_util_common.php validate_unique_e_mail $EMAIL
# php test/test_lib_util_common.php validate_last_shipped_days_ago_min $CUSTOMER_ID $LAST_DAYS
# php test/test_lib_util_common.php validate_sim_shipments_max $CUSTOMER_ID $MAX
# php test/test_lib_util_common.php validate_first_last_name $NAME
# php test/test_lib_util_common.php get_customer_state $CUSTOMER_ID // translates to CUSTOMER
# php test/test_lib_util_common.php shipping_cost
# php test/test_lib_util_common.php sim_cost
# php test/test_lib_util_common.php get_customer_source $MASTERAGENT $STORE $USERID
# php test/test_lib_util_common.php get_voice_recharge_by_plan
# php test/test_lib_util_common.php get_plan_cost_by_cos_id
# php test/test_lib_util_common.php redis_get_command_count_by_hour $CMD
# php test/test_lib_util_common.php redis_increment_command_count_by_hour $CMD
# php test/test_lib_util_common.php set_data_recharge_notification_delay $CUSTOMER_ID
# php test/test_lib_util_common.php get_data_recharge_notification_delay $CUSTOMER_ID
# php test/test_lib_util_common.php set_data_recharge_semaphore $CUSTOMER_ID
# php test/test_lib_util_common.php get_data_recharge_semaphore $CUSTOMER_ID
# php test/test_lib_util_common.php set_voice_recharge_semaphore $CUSTOMER_ID
# php test/test_lib_util_common.php get_voice_recharge_semaphore $CUSTOMER_ID
# php test/test_lib_util_common.php set_bolt_on_semaphore $CUSTOMER_ID $TYPE
# php test/test_lib_util_common.php get_bolt_on_semaphore $CUSTOMER_ID $TYPE
# php test/test_lib_util_common.php is_porting_disabled
# php test/test_lib_util_common.php customer_has_credit_card $CUSTOMER_ID
# php test/test_lib_util_common.php languageIETF $LANGUAGE
# php test/test_lib_util_common.php func_validate_orange_sim $CUSTOMER_ID
# php test/test_lib_util_common.php validateUSZipcode $ZIPCODE
# php test/test_lib_util_common.php extractZipcodesFromString $ZIPCODES
# php test/test_lib_util_common.php get_reference_source
# php test/test_lib_util_common.php get_utc_date
# php test/test_lib_util_common.php search_data_structure
# php test/test_lib_util_common.php date_from_pst_to_utc $DATE
# php test/test_lib_util_common.php date_from_utc_to_pst $DATE
# php test/test_lib_util_common.php int_positive_or_zero $INT
# php test/test_lib_util_common.php compute_imei $IMEI
# php test/test_lib_util_common.php Test_normalize_msisdn_11 $MSISDN
# php test/test_lib_util_common.php Test_normalize_msisdn_10 $MSISDN
# php test/test_lib_util_common.php date_to_datetime $DATE
# php test/test_lib_util_common.php tmo_dummymsisdn $MSISDN || $IMSI || $ICCID
# php test/test_lib_util_common.php date_sql_to_usa $DATE
# php test/test_lib_util_common.php time_zone_mapping
# php test/test_lib_util_common.php has_non_ascii_characters $STRING
# php test/test_lib_util_common.php beautify_address
# php test/test_lib_util_common.php validate_email_address
# php test/test_lib_util_common.php overridden_cc_dupe_check $customer_cvv $customer_cc_n $customer_cc_exp $last_name
# php test/test_lib_util_common.php fix_renewal_date
# php test/test_lib_util_common.php dlog_allowed
# php test/test_lib_util_common.php amount_cents_to_dollars_string
# php test/test_lib_util_common.php cleanse_credit_card_string
# php test/test_lib_util_common.php deunicodify_characters
# php test/test_lib_util_common.php archive_messaging_queue_query
# php test/test_lib_util_common.php luhnenize $ICCID
# php test/test_lib_util_common.php validate_act_code $ACTCODE
# php test/test_lib_util_common.php adjust_act_code   $ACTCODE
# php test/test_lib_util_common.php remove_duplicates_by_field
# php test/test_lib_util_common.php date_from_tz_to_tz
# php test/test_lib_util_common.php timestamp_to_next_daytime
# php test/test_lib_util_common.php date_to_next_daytime
# php test/test_lib_util_common.php timestamp_to_date
# php test/test_lib_util_common.php getTimeStamp
# php test/test_lib_util_common.php find_data_recharge  $PLAN $DATA_SOC_ID
# php test/test_lib_util_common.php find_voice_recharge $PLAN $VOICE_SOC_ID
# php test/test_lib_util_common.php validate_ICCID      $FLAG $ICCID
# php test/test_lib_util_common.php paginate_array
# php test/test_lib_util_common.php compute_delivery_daytime $timestamp $utc_offset $priority
# php test/test_lib_util_common.php get_data_recharge_by_plan
# php test/test_lib_util_common.php beautify_4g_lte_string $STRING [TRUE:FALSE]
# php test/test_lib_util_common.php convert_value_to_boolean
# php test/test_lib_util_common.php ultraErrorHandler
# php test/test_lib_util_common.php get_plan_costs_by_customer_id
# php test/test_lib_util_common.php get_bolt_ons_by_customer_id $CUSTOMER_ID
# php test/test_lib_util_common.php get_bolt_ons_costs_by_customer_id
# php test/test_lib_util_common.php find_domain_url
# php test/test_lib_util_common.php date_to_epoch "$DATE" [$TIMEZONE]


include_once('db.php');
include_once('lib/util-common.php');
include_once('test/api/test_api_include.php');

use Ultra\Lib\Util\Redis as RedisUtils;

date_default_timezone_set("America/Los_Angeles");


teldata_change_db(); // connect to the DB

$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);



abstract class AbstractTestStrategy
{
  abstract function test();
}

class Test_timeUTC extends AbstractTestStrategy
{
  function test()
  {
    echo \timeUTC();
    echo PHP_EOL;
  }
}

class Test_json_decode_nice extends AbstractTestStrategy
{
  function test()
  {
    $json = '
  {
    "parameters": {
        "name": "n",
        "value": 1
    },
    "returns": [
        "common-returns.json"
    ]
  }
  ';

    print_r(json_decode_nice($json, FALSE));
  }
}

class Test_indent__JSON extends AbstractTestStrategy
{
  function test()
  {
    $json = '
  {
    "parameters": {
        "name": "n",
        "value": 1
    },
    "returns": [
        "common-returns.json"
    ]
  }
  ';

    print_r(indent__JSON($json));
  }
}

class Test_enclose_JSON extends AbstractTestStrategy
{
  function test()
  {
    $json = '
  {
    "parameters": {
        "name": "n",
        "value": 1
    },
    "returns": [
        "common-returns.json"
    ]
  }
  ';

    print_r(enclose_JSON($json));
  }
}

class Test_disclose_JSON extends AbstractTestStrategy
{
  function test() {
    $enc = '[
  {
    "parameters": {
        "name": "n",
        "value": 1
    },
    "returns": [
        "common-returns.json"
    ]
  }
  ]';

    print_r(disclose_JSON($enc));
  }
}

class Test_dlog_allowed_regexp extends AbstractTestStrategy
{
  function test()
  {
    echo dlog_allowed_regexp() . "\n";
  }
}

class Test_getReferringIPAddress extends AbstractTestStrategy
{
  function test()
  {
    echo getReferringIPAddress() . "\n";
  }
}

class Test_dlog extends AbstractTestStrategy
{
  function test()
  {
    dlog('', "%s", 'Dlog test');
  }
}

class Test_get_mcrypt_encrypt_key extends AbstractTestStrategy
{
  function test()
  {
    echo get_mcrypt_encrypt_key() . "\n";
  }
}

class Test_perform_mcrypt_encryption extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $string = (isset($argv[2])) ? $argv[2] : 'Test encrypt string';
    echo perform_mcrypt_encryption($string) . "\n";
  }
}

class Test_perform_mcrypt_decryption extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $encrypted = (isset($argv[2])) ? $argv[2] : perform_mcrypt_encryption('Test encrypt string');

    echo perform_mcrypt_decryption($encrypted) . "\n";
  }
}

class Test_getNewTransitionUUID extends AbstractTestStrategy
{
  function test()
  {
    echo getNewTransitionUUID() . "\n";
  }
}

class Test_getNewActionUUID extends AbstractTestStrategy
{
  function test()
  {
    echo getNewActionUUID() . "\n";
  }
}

class Test_getNewUUID extends AbstractTestStrategy
{
  function test()
  {
    echo getNewUUID('PFIX') . "\n";

    echo getNewUUID('PFIX',FALSE,FALSE) . "\n";
  }
}

class Test_create_guid extends AbstractTestStrategy
{
  function test()
  {
    echo create_guid() . "\n";
  }
}

class Test_luhn_checksum extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $number = (isset($argv[2])) ? $argv[2] : 890126084210225427;

    echo luhn_checksum($number) . "\n";
  }
}

class Test_luhn_checksum_check extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $toTest = (isset($argv[2])) ? $argv[2] : '4111111111111111';
    
    if (luhn_checksum_check($toTest))
      echo "SUCCESS on $toTest\n";
    else
      echo "FAILURE on $toTest\n";
  }
}

class Test_luhn_append extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $toTest = (isset($argv[2])) ? $argv[2] : '4111111111111111';

    echo "$toTest results in : " . luhn_append($toTest) . "\n";
  }
}

class Test_curry extends AbstractTestStrategy
{
  function test()
  {
    $add20 = curry(function($a, $b){return $a + $b;}, 20);
    echo $add20(5) . "\n"; #25
  }
}

class Test_canonify extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $toCanonify = (isset($argv[2])) ? $argv[2] : '[1234]AB[56]C';

    echo canonify($toCanonify) . "\n";
  }
}

class Test_eval_dangerous extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $code = (isset($argv[2])) ? $argv[2] : 'return $test_color;';
    $vars = (isset($argv[3])) ? $argv[3] : array('test_color' => 'red');

    echo eval_dangerous($code, $vars) . "\n";
  }
}

class Test_debug_json_decode extends AbstractTestStrategy
{
  function test()
  {
    $json = '
  {
    "parameters": {
        "name": "n",
        "value": 1
    },
    "returns": [ 
        "common-returns.json"
    ]
  ';

    json_decode($json);
    echo debug_json_decode() . "\n";

    $json .= '}';
    json_decode($json);
    echo debug_json_decode() . "\n";
  }
}

class Test_mssql_escape_raw extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $data = (isset($argv[2])) ? $argv[2] : "'test string";

    echo mssql_escape_raw($data) . "\n";
  }
}

class Test_mssql_escape extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $data = (isset($argv[2])) ? $argv[2] : 'data';

    echo mssql_escape($data) . "\n";
  }
}

class Test_mssql_escape_with_zeroes extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $data = (isset($argv[2])) ? $argv[2] : 'data';

    echo mssql_escape_with_zeroes($data) . "\n";
  }
}

class Test_mssql_escape_or_null extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $data = (isset($argv[2])) ? $argv[2] : 'data';

    echo mssql_escape_or_null($data) . "\n";
    echo mssql_escape_or_null(NULL) . "\n";
  }
}

class Test_clause_mssql_escape_or_null extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $data = (isset($argv[2])) ? $argv[2] : 'data';

    echo clause_mssql_escape_or_null($data) . "\n";
    echo clause_mssql_escape_or_null(NULL) . "\n";
  }
}

class Test_get_htt_env extends AbstractTestStrategy
{
  function test()
  {
    echo get_htt_env() . "\n";
  }
}

class Test_var_log extends AbstractTestStrategy
{
  function test()
  {
    var_log(array('data' => 'test'));
  }
}

class Test_stack_log extends AbstractTestStrategy
{
  function test()
  {
    stack_log();
  }
}

class Test_caption_amount extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $amount = (isset($argv[2])) ? $argv[2] : 400;
    echo "$amount results in : " . caption_amount($amount) . "\n";
  }
}

class Test_get_date_from_full_date extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $date = (isset($argv[2])) ? $argv[2] : 'Oct 2 2012 08:39:38:233PM';

    echo get_date_from_full_date($date) . "\n";
  }
}

class Test_fix_transition_uuid_format extends AbstractTestStrategy
{
  function test() 
  {
    global $argv;

    $uuid = (isset($argv[2])) ? $argv[2] : '8D8BE06C-1C86-1AA0-9DA7-F67C941C83FA';

    echo fix_transition_uuid_format($uuid) . "\n";
  }
}

class Test_get_customer_from_zsession extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;

    $zsession = make_zsession_from_customer_id($customer_id);

    print_r(get_customer_from_zsession($zsession));
    echo "\n";
  }
}

class Test_validate_date_dd_mm_yyyy extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $date = (isset($argv[2])) ? $argv[2] : '02-07-2014';

    if (!validate_date_dd_mm_yyyy($date))
      echo "FAILURE on $date" . "\n";
    else
      echo "SUCCESS on $date" . "\n";
  }
}

class Test_validate_port_account_number extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $portAccountNum = (isset($argv[2])) ? $argv[2] : 'ABC123456789';

    if (!validate_port_account_number($portAccountNum))
      echo "FAILURE on $portAccountNum" . "\n";
    else
      echo "SUCCESS on $portAccountNum" . "\n";
  }
}

class Test_validate_username_characters extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $username = (isset($argv[2])) ? $argv[2] : 'user?name';

    // invalid
    print_r(validate_username_characters($username));
    echo "\n";
  }
}

class Test_validate_unique_username extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $username = (isset($argv[2])) ? $argv[2] : 'GOGOUSERNAME1234';

    print_r(validate_unique_username($username));
    echo "\n";
  }
}

class Test_validate_unique_e_mail extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $email = (isset($argv[2])) ? $argv[2] : 'GOGOUSERNAME1234@ultra.me';

    print_r(validate_unique_e_mail($email));
  }
}

class Test_validate_last_shipped_days_ago_min extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;
    $last_days = (isset($argv[3])) ? $argv[3] : 5;

    $s = (validate_last_shipped_days_ago_min($customer_id, $last_days)) ? 'TRUE' : 'FALSE';
    echo "$s on customer_id,last_days : $customer_id,$last_days\n";
  }
}

class Test_validate_sim_shipments_max extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;
    $max = (isset($argv[3])) ? $argv[3] : 5;

    $s = (validate_last_shipped_days_ago_min($customer_id, $max)) ? 'TRUE' : 'FALSE';
    echo "$s on customer_id,max : $customer_id,$max\n";
  }
}

class Test_validate_first_last_name extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $name = (isset($argv[2])) ? $argv[2] : 'James Steinmetz';

    if (validate_first_last_name($name))
      echo "SUCCESS on $name" . "\n";
    else
      echo "FAILURE ON $name" . "\n";
  }
}

class Test_get_customer_state extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;
    $customer = get_customer_from_customer_id($customer_id);

    print_r(get_customer_state($customer));
    echo "\n";
  }
}

class Test_shipping_cost extends AbstractTestStrategy
{
  function test()
  {
    echo shipping_cost() . "\n";
  }
}

class Test_sim_cost extends AbstractTestStrategy
{
  function test()
  {
    echo sim_cost() . "\n";
  }
}

class Test_get_customer_activation_source extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $masteragent = (isset($argv[2])) ? $argv[2] : 54;
    $store = (isset($argv[3])) ? $argv[3] : 28;
    $userid = (isset($argv[4])) ? $argv[4] : 67;

    $source = get_customer_source($masteragent, $store, $userid);

    echo "$masteragent,$store,$userid : $source\n";
  }
}

class Test_get_voice_recharge_by_plan extends AbstractTestStrategy
{
  function test()
  {
    $l = array('L19','L29','L39','L49','L59');

    foreach( $l as $p )
    {
      $x = get_voice_recharge_by_plan($p);
      echo "\n\n$p\n";
      print_r($x);
    }

    echo "\n";
  }
}

class Test_get_plan_cost_by_cos_id extends AbstractTestStrategy
{
  function test()
  {
    $l = array('L19','L29','L39','L49','L59');

    foreach( $l as $p )
    {
      $x = get_plan_cost_by_cos_id($p);
      echo "\n\n$p\n";
      print_r($x);
    }

    echo "\n";
  }
}

class Test_redis_get_command_count_by_hour extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $redis = new \Ultra\Lib\Util\Redis();

    $cmd = $argv[2];
    echo "Using $cmd : " . RedisUtils\redis_get_command_count_by_hour($redis, $cmd) . "\n";
  }
}

class Test_redis_increment_command_count_by_hour extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $redis = new \Ultra\Lib\Util\Redis();

    $cmd = $argv[2];
    RedisUtils\redis_increment_command_count_by_hour($redis, $cmd);
  }
}

class Test_set_data_recharge_notification_delay extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;
    
    $redis = new \Ultra\Lib\Util\Redis();
    set_data_recharge_notification_delay($redis, $customer_id);
  }
}

class Test_get_data_recharge_notification_delay extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;
    
    $redis = new \Ultra\Lib\Util\Redis();
    echo get_data_recharge_notification_delay($redis, $customer_id) . "\n";
  }
}

class Test_set_data_recharge_semaphore extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;

    $redis = new \Ultra\Lib\Util\Redis();
    set_data_recharge_semaphore($redis, $customer_id);
  }
}

class Test_get_data_recharge_semaphore extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;

    $redis = new \Ultra\Lib\Util\Redis();
    echo get_data_recharge_semaphore($redis, $customer_id) . "\n";
  }
}

class Test_set_voice_recharge_semaphore extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;

    $redis = new \Ultra\Lib\Util\Redis();
    set_voice_recharge_semaphore($redis, $customer_id);
  }
}

class Test_get_voice_recharge_semaphore extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;

    $redis = new \Ultra\Lib\Util\Redis();
    echo get_voice_recharge_semaphore($redis, $customer_id) . "\n";
  }
}

class Test_set_bolt_on_semaphore extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;
    $type = empty($argv[3]) ? 'DATA' : $argv[3];

    $redis = new \Ultra\Lib\Util\Redis();
    set_bolt_on_semaphore($redis, $customer_id, $type);
  }
}

class Test_get_bolt_on_semaphore extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;
    $type = empty($argv[3]) ? 'DATA' : $argv[3];

    $redis = new \Ultra\Lib\Util\Redis();
    echo get_bolt_on_semaphore($redis, $customer_id, $type) . "\n";
  }
}

class Test_is_porting_disabled extends AbstractTestStrategy
{
  function test()
  {
    print_r(is_porting_disabled());
    echo "\n";
  }
}

class Test_customer_has_credit_card extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;

    if ( customer_has_credit_card( get_customer_from_customer_id($customer_id)) )
      echo "CUSTOMER $customer_id has credit card/s";
    else
      echo "CUSTOMER $customer_id has no credit cards";

    echo "\n";

    if ( customer_has_credit_card( $customer_id ) )
      echo "CUSTOMER $customer_id has credit card/s";
    else
      echo "CUSTOMER $customer_id has no credit cards";
  }
}

class Test_languageIETF extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $lang = (isset($argv[2])) ? $argv[2] : 'en';

    echo languageIETF($lang) . "\n";
  }
}

class Test_func_validate_orange_sim extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $customer_id = (isset($argv[2])) ? $argv[2] : 3585;

    print_r(func_validate_orange_sim($customer_id));
    echo "\n";
  }
}

class Test_validateUSZipcode extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $zipcode = (isset($argv[2])) ? $argv[2] : '92683';

    if (validateUSZipcode($zipcode))
      echo "SUCCESS with $zip";
    else
      echo "FAILURE with $zip";

    echo "\n";
  }
}

class Test_extractZipcodesFromString extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $zipcodes = (isset($argv[2])) ? $argv[2] : $zipcodes;

    print_r(extractZipcodesFromString($zipcodes));
  }
}

class Test_get_reference_source extends AbstractTestStrategy
{
  function test()
  {
    foreach (array('WEBCC','EPAY','PIN','VOID','MANUAL','ORANGE_SIM','PROMO','EPAYPQ','') as $key)
    {
      // 0,1,2,4,5,6,7,8,3
      echo get_reference_source($key) . "\n";
    }
  }
}

class Test_get_utc_date extends AbstractTestStrategy
{
  function test()
  {
    echo get_utc_date() . "\n";
  }
}

class Test_search_data_structure extends AbstractTestStrategy
{
  function test()
  {
    $ds = array('test_field' => 'test_value');
    echo search_data_structure('test_field', $ds) . "\n"; 
  }
}

class Test_date_from_pst_to_utc extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $date = (isset($argv[2])) ? $argv[2] : get_utc_date();
    echo "$date to : " . date_from_pst_to_utc($date) . "\n";
  }
}

class Test_date_from_utc_to_pst extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $date = (isset($argv[2])) ? $argv[2] : get_utc_date();
    echo "$date to : " . date_from_utc_to_pst($date) . "\n";
  }
}

class Test_int_positive_or_zero extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $int = (isset($argv[2])) ? int_positive_or_zero($int) : -1;

    echo "$int to : " . int_positive_or_zero($int) . "\n";
  }
}

class Test_compute_imei extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $imei = (isset($argv[2])) ? $argv[2] : '3559720503124605';

    echo "$imei to : " . compute_imei('3559720503124605') . "\n";
  }
}

class Test_normalize_msisdn_11 extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $number = (isset($argv[2])) ? $argv[2] : '7145559999';

    echo "$number to : " . normalize_msisdn_11($number) . "\n";
  }
}

class Test_normalize_msisdn_10 extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $number = (isset($argv[2])) ? $argv[2] : '17145559999';

    echo "$number to : " . normalize_msisdn_10($number) . "\n";
  }
}

class Test_date_to_datetime extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $date = (isset($argv[2])) ? $argv[2] : '2001-02-29';
    echo "$date to : " . date_to_datetime($date) . "\n";
  }
}

class Test_tmo_dummymsisdn extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $iccid = (isset($argv[2])) ? $argv[2] : '890126084210201011';

    print_r(tmo_dummymsisdn($iccid));
    echo "\n";
  }
}

class Test_date_sql_to_usa extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $date = (isset($argv[2])) ? $argv[2] : 'Apr 4 2014 03:02:44:387PM';

    echo "$date to : " . date_sql_to_usa($date) . "\n";
  }
}

class Test_time_zone_mapping extends AbstractTestStrategy
{
  function test()
  {
    print_r(time_zone_mapping());
    echo "\n";
  }
}

class Test_find_domain_url extends AbstractTestStrategy
{
  function test()
  {
    echo "find_site_url   = ".find_site_url()."\n";
    echo "find_domain_url = ".find_domain_url()."\n";
  }
}

class Test_getTimeStamp extends AbstractTestStrategy
{
  function test()
  {
    echo getTimeStamp()."\n";
  }
}

class Test_beautify_4g_lte_string extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $string = beautify_4g_lte_string( $argv[2], $argv[3] == 'TRUE' );

    echo "$string\n";
  }
}


class Test_get_plan_costs_by_customer_id extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $costs = get_plan_costs_by_customer_id( $argv[2] );

    print_r($costs);
  }
}


class Test_get_bolt_ons_by_customer_id extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $costs = get_bolt_ons_by_customer_id( $argv[2] );

    print_r($costs);
  }
}


class Test_get_bolt_ons_costs_by_customer_id extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $costs = get_bolt_ons_costs_by_customer_id( $argv[2] );

    echo "Cost = $costs\n";
  }
}


class Test_get_data_recharge_by_plan extends AbstractTestStrategy
{
  function test()
  {
    $l = array('L19','L29','L39','L49','L59');

    foreach( $l as $p )
    {
      $x = get_data_recharge_by_plan($p);
      echo "\n\n$p\n";
      print_r($x);
    }

    echo "\n";
  }
}


class Test_paginate_array extends AbstractTestStrategy
{
  function test()
  {
    $l = array('a',2,3,4,5,6,7,8,9,'x','y');

    $x = paginate_array( $l , 2 );

    print_r($x);

    $x = paginate_array( $l , 3 );

    print_r($x);
  
    $x = paginate_array( $l , 4 );

    print_r($x);
  }
}


class Test_validate_ICCID extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $flag  = $argv[2];
    $ICCID = $argv[3];

    teldata_change_db();

    $r = validate_ICCID($ICCID, $flag);
    echo 'RESULT: ' . ($r ? 'TRUE' : 'FALSE') . "\n";

    $sim = get_htt_inventory_sim_from_iccid($ICCID);
    $r = validate_ICCID($sim, $flag);
    echo 'RESULT: ' . ($r ? 'TRUE' : 'FALSE') . "\n";

  }
}


class Test_find_voice_recharge extends AbstractTestStrategy
{
/*
php test/test_lib_util_common.php find_voice_recharge L19 50_100

    [voice_soc_id] => 50_100
    [voice_minutes] => 50
    [cost] => 100
*/
  function test()
  {
    global $argv;

    $plan         = $argv[2];
    $voice_soc_id = $argv[3];

    $r = find_voice_recharge( $plan , $voice_soc_id );

    print_r($r);
  }
}


class Test_find_data_recharge extends AbstractTestStrategy
{
  /*
  "cred[ultra_develop_tel_api_accounts][plans/Ultra/NINETEEN/data_recharge]"     string => "PayGo_50_250,PayGo_250_500,PayGo_500_1000";
  "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/data_recharge]"  string => "PayGo_50_250,PayGo_250_500,PayGo_500_1000";
  "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/data_recharge]"  string => "Bucketed_500_1000";
  "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/data_recharge]"   string => "Bucketed_500_1000";
  "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/data_recharge]"   string => "Bucketed_250_500_256kb";
  */

/*
php test/test_lib_util_common.php find_data_recharge L19 PayGo_250_500
php test/test_lib_util_common.php find_data_recharge L19 PayGo_500_1000
php test/test_lib_util_common.php find_data_recharge L29 PayGo_500_1000
php test/test_lib_util_common.php find_data_recharge L59 Bucketed_250_500_256kb
php test/test_lib_util_common.php find_data_recharge L39 Bucketed_500_1000
*/
  function test()
  {
    global $argv;

    $plan        = $argv[2];
    $data_soc_id = $argv[3];

    $r = find_data_recharge( $plan , $data_soc_id );

    print_r($r);
  }
}


class Test_timestamp_to_date
{
  function test()
  {
    $t = time();

    $date = timestamp_to_date( $t );

    $columns = mssql_fetch_all_rows(logged_mssql_query("SELECT GETUTCDATE()"));

    print_r($columns);

    echo "Server timestamp (PST): $t\n";

    echo "Date PST : $date\n";

    echo "Date UTC : ".date_from_pst_to_utc( $date )."\n"; // this format can be used ro insert or update

    echo "Date UTC from DB : ".$columns[0][0]."\n";
  }
}

class Test_date_from_tz_to_tz
{
  function test()
  {
    $date = "11-11-2013";

    echo date_from_pst_to_utc( $date )."\n";

    echo date_from_utc_to_pst( $date )."\n";
  }
}

class Test_date_to_next_daytime
{
  function test()
  {
    $test_times = array(
      1384879211
    );

    foreach (range(1, 23) as $number)
    {
      $test_times[] = $test_times[ count($test_times) -1 ] - 60*60 ;
    }

    print_r($test_times);

    foreach ( $test_times as $timestamp )
    {
      $d = new DateTime( date("Y-m-d H:i:s", $timestamp) );
      echo "input  = $timestamp    ".$d->format( "Y-m-d H:i:s" )."\n";

      $next = date_to_next_daytime( $timestamp );
      echo "output = $next\n\n";
    }
  }
}

class Test_timestamp_to_next_daytime
{
  function test()
  {
    $test_times = array(
      1384879211
    );

    foreach (range(1, 23) as $number)
    {
      $test_times[] = $test_times[ count($test_times) -1 ] - 60*60 ;
    }

    print_r($test_times);

    foreach ( $test_times as $timestamp )
    {
      $d = new DateTime( date("Y-m-d H:i:s", $timestamp) );
      echo "input  = $timestamp    ".$d->format( "Y-m-d H:i:s" )."\n";

      $t = timestamp_to_next_daytime( $timestamp );
      $d = new DateTime( date("Y-m-d H:i:s", $t) );
      echo "output = $t    ".$d->format( "Y-m-d H:i:s" )."\n\n";
    }
  }
}

class Test_remove_duplicates_by_field
{
  function test()
  {
    $string = '[{"name":"last_name","type":"a"},{"name":"last_name","type":"a"},{"name":"last_name","type":"f"},{"name":"last_name","type":"e"},{"name":"last_name","type":"d"}]';

    $data = json_decode( $string );

    print_r($data);

    $field  = 'type';

    $data = remove_duplicates_by_field( $data , $field );

    print_r($data);
  }
}


class Test_beautify_address
{
  function test()
  {
    $test_strings = array(
'301  EAST  RIDGE  ROAD',
'301 EAST RIDGE ROAD',
'282 AMHEARST',
'733 BUTTERNUT ST',
'13274 VAN NUYS BLVD',
'10515 western ave 4',
'13372 Van Nuys Blvd',
'3956 beverly blvd',
'4160 beverly bl ',
'4075 Beverly Blvd ',
'3707 3rd St   ',
'585 glenoaks bl',
'2407 Cesar E Chavez Ave',
'4126 S broadway',
'585 glenoaks bl',
'230 e winston st #41',
'999 n waterman ave',
'993 w valley bl #106',
'21508 sherman way',
'16776 Valley Blvd  ',
'1155 s klein ave',
'Aaa Computer Repair Center 1',
'AAA Computer Repair Center 1',
'aaa Computer Repair Center 1',
'NYC Computer Repair Center 1',
'AHS Wireless Shack',
'A&Z Wireless',
'A-Z Wireless',
'Jim Wireless',
'Joe Wireless',
'Ulf Wireless',
'Mr Wireless',
'Cs Technology',
'MR Wireless',
'CS Technology'
    );

    foreach($test_strings as $i => $string)
    {
      echo " $string\n";
      echo "|".beautify_address($string)."|\n\n";
    }
  }
}


class Test_validate_email_address
{
  function test()
  {
    $emails = array(
"null@null.de",
"null@null.com",
"dclo@us.ibm.com",
"abc\\@def@example.com",
"abc\\\\@example.com",
"Fred\\ Bloggs@example.com",
"Joe.\\\\Blow@example.com",
"\"Abc@def\"@example.com",
"\"Fred Bloggs\"@example.com",
"customer/department=shipping@example.com",
"\$A12345@example.com",
"!def!xyz%abc@example.com",
"_somename@example.com",
"user+mailbox@example.com",
"peter.piper@example.com",
"Doug\\ \\\"Ace\\\"\\ Lovell@example.com",
"\"Doug \\\"Ace\\\" L.\"@example.com",

// All of these should fail

"abc@def@example.com",
"abc\\\\@def@example.com",
"abc\\@example.com",
"@example.com",
"doug@",
"\"qu@example.com",
"ote\"@example.com",
".dot@example.com",
"dot.@example.com",
"two..dot@example.com",
"\"Doug \"Ace\" L.\"@example.com",
"Doug\\ \\\"Ace\\\"\\ L\\.@example.com",
"hello world@example.com",
"gatsby@f.sc.ot.t.f.i.tzg.era.l.d.",
"null@null",
"null@null.q",
"null@.null",
"null@null."
    );

    foreach($emails as $i => $email)
    {
      echo "$email\n";
      if ( validate_email_address($email) )
      {
        echo "yes\n";
      }
      else
      {
        echo "NOT\n";
      }
    }
  }
}


class Test_overridden_cc_dupe_check
{
  function test()
  {
    global $argv;

    $customer_cvv = $argv[2];
    $customer_cc_n = $argv[3];
    $customer_cc_exp = $argv[4];
    $last_name = $argv[5];

echo "customer_cvv = $customer_cvv\n";
echo "customer_cc_n = $customer_cc_n\n";
echo "customer_cc_exp = $customer_cc_exp\n";
echo "last_name = $last_name\n";

    if ( overridden_cc_dupe_check($customer_cvv,$customer_cc_n,$customer_cc_exp,$last_name) )
    { echo "overridden\n"; }
    else
    { echo "NOT overridden\n"; }
  }
}


class Test_fix_renewal_date
{
  function test()
  {
    $test_strings = array(
      'Dec 01, 2012',
      'Dec 02, 2012',
      'Dec 03, 2012',
      'Dec 04, 2012',
      'Dec 08, 2012',
      'Dec 11, 2012',
      'Dec 12, 2012',
      'Dec 13, 2012',
      'Dec 14, 2012',
      'Dec 21, 2012',
      'Dec 22, 2012',
      'Dec 23, 2012',
      'Dec 24, 2012',
      'Dec 31, 2012'
    );

    foreach($test_strings as $i => $string)
    {
      echo "$string\n";
      echo fix_renewal_date($string)."\n";
    }
  }
}


class Test_dlog_allowed
{
  function test()
  {
    if ( dlog_allowed(NULL   ) ) { echo "1\n"; }
    if ( dlog_allowed(''     ) ) { echo "2\n"; }
    if ( dlog_allowed('test1') ) { echo "3\n"; }
    if ( dlog_allowed('test2') ) { echo "4\n"; }
  }
}


class Test_amount_cents_to_dollars_string
{
  function test()
  {
    $test_strings = array('12300', '12345', '1234', '123', '12', '1');

    foreach($test_strings as $i => $string)
    {
      echo "Before: $string\n";
      echo "After:  ".amount_cents_to_dollars_string($string)."\n\n";
    }
  }
}


class Test_adjust_act_code
{
  function test()
  {
    global $argv;

    $act_code = $argv[2];

    echo "act_code = $act_code\n";

    if ( strlen($act_code) < 10 )
      die("too short\n");

    if ( strlen($act_code) > 11 )
      die("too long\n");

    if ( strlen($act_code) == 11 )
    {
      if ( validate_act_code( $act_code ) )
        echo "$act_code valid\n";
      else
        echo "$act_code NOT valid\n";
    }
    else
    {
      $check_digit = luhn_checksum($act_code * 10);
      $check_digit = $check_digit == 0 ? $check_digit : (10 - $check_digit);
      echo "act_code = $act_code".$check_digit."\n";
    }
  }
}


class Test_validate_act_code
{
  function test()
  {
    global $argv;

    echo $argv[2].' => '.validate_act_code( $argv[2] )."\n";
  }
}


class Test_luhnenize
{
  function test()
  {
    global $argv;

    echo $argv[2].' => '.luhnenize( $argv[2] )."\n";
  }
}


class Test_deunicodify_characters
{
  function test()
  {
    $string = 'xàyé ÌÒşöäÿŖŖŖĺĺĺ';

    echo "$string\n";

    $string = deunicodify_characters($string);

    echo "$string\n";
  }
}


class Test_cleanse_credit_card_string
{
  function test()
  {
    $test_strings = array(
'"format":"jsonp","partner":"portal","command":"portal__SetCustomerFields","callback":"l_xSignup.process_info","bath":"rest","version":"1","zsession":"9214A3F4-CD1E-8BE1-A2AE-D19AD2655F0A","cc_address1":"PO Box 111","cc_address2":" ","cc_city":"Irvine","cc_state_or_region":"CA","cc_postal_code":"92623","cc_country":"US","account_number_phone":"8887775555","cc_name":"Q Ultrasmith","account_cc_exp":"1213","account_cc_cvv":"798","address1":"PO Box 111","address2":" ","city":"Irvine","state_or_region":"CA","postal_code":"92623","country":"US","account_cc_number":"6011580159853550","_":"1380062294584"',
'"LOGIN_PASSWORD":" ","CC_NUMBER":"!CFF20866A79BBA393BEB118B6344C05B5736","CC_EXP_DATE":"1015"',
'"ICCID":"8901260842103112661","loadPayment":"CCARD","targetPlan":"L29","pinsToApply":"","creditCard":"4342570991836033","creditExpiry":"0915"',
'xxxx xxxx xxxx 0000',
'CC=xxxx xxxxxx x1008,Expiry=1016',
'"account_cc_cvv":"208","account_cc_number":"4435121312860456"}',
'customercare__SetCustomerCreditCard, arguments ["","5932","0313","208","4435121312860456",null]',
'\'CC_NUMBER\'",{"validated":{"ok":true,"max_length":20,"v_type":[0,20,"CC_NUMBER","cc_and_pin_saver",""],"field_name":"account_cc_number","field_value":"4117733944873783","validation_msg":""}},{"all_validated":true},{"sql":["account_cc_exp",true]},{"sql":["account_cc_cvv",true]},{"sql":["account_cc_number",true]}]}',
'[Thu May 29 13:43:24 2014] [error] [client 70.39.130.103] {UA-F621F20D3B6C259F-FD6E10FA56B40610} [6003] Ultra\\Lib\\MiddleWare\\ACC\\ControlCommandBase::interactWithChannel(): inboundReplyMessage = {"body":{"ResultCode":"100","IMSI":0123456789123137,"ResultMsg":"Success","IMEI":012345678918806,"NAPFeatureList":{"NAPFeature":[{"FeatureName":"MVNO","FeatureDescription":{}},{"FeatureName":"WBASTHR80","FeatureDescription":{}}',
'[Thu May 29 13:43:24 2014] [error] [client 70.39.130.103] {UA-F621F20D3B6C259F-FD6E10FA56B40610} [6003] Ultra\\Lib\\MiddleWare\\ACC\\ControlCommandBase::interactWithChannel(): inboundReplyMessage = {"body":{"ResultCode":"100","IMSI":012345678912313,"ResultMsg":"Success","IMEI":01234567891880,"NAPFeatureList":{"NAPFeature":[{"FeatureName":"MVNO","FeatureDescription":{}},{"FeatureName":"WBASTHR80","FeatureDescription":{}}',
'[Thu May 29 13:43:24 2014] [error] [client 70.39.130.103] {UA-F621F20D3B6C259F-FD6E10FA56B40610} [6003] Ultra\\Lib\\MiddleWare\\ACC\\ControlCommandBase::interactWithChannel(): inboundReplyMessage = {"body":{"ResultCode":"100","IMSI":01234567891231371,"ResultMsg":"Success","IMEI":0123456789188061,"NAPFeatureList":{"NAPFeature":[{"FeatureName":"MVNO","FeatureDescription":{}},{"FeatureName":"WBASTHR80","FeatureDescription":{}}',
'"IMSI":"4435121312860456","IMEI":"4342570991836033"',
'"IMSA":"4435121312860456","IMEA":"4342570991836033"'
    );

    foreach($test_strings as $i => $string)
    {
      echo "Before: $string\n";
      echo "After:  ". cleanse_credit_card_string($string)."\n\n";
    }
  }
}


class Test_archive_messaging_queue_query
{
  function test()
  {
    $query = archive_messaging_queue_query();

    echo "$query\n";

    $check = is_mssql_successful(logged_mssql_query($query));

    if ( $check )
    {
      echo "OUTCOME : success\n";
    }
    else
    {
      echo "OUTCOME : failure\n";
    }
  }
}


class Test_map_array_keys
{
  function test()
  {
    $x = array( 'a' , 'b' , 'c' );
    $z = array( 'a3' , 'b2' , 'c1' );

    $r = map_array_keys( $x , $z );

    print_r($r);

/*
(
    [a] => a3
    [b] => b2
    [c] => c1
)
*/
  }
}


class Test_map_object_keys_array
{
  function test()
  {
    $values = array( 'a' , 'b' , 'c' );
    $z = array( 'a3' , 'b2' , 'c1' );

    $r = map_object_keys_array(
      $z,
      array( $values , $values )
    );

    print_r($r);

/*
Array
(
    [0] => stdClass Object
        (
            [a3] => a
            [b2] => b
            [c1] => c
        )
    [1] => stdClass Object
        (
            [a3] => a
            [b2] => b
            [c1] => c
        )
)
*/
  }
}


class Test_remove_empty_values
{
  function test()
  {
    $values = array( 'a' , 'b' , ' ' , '' , 'c' );

    $r = remove_empty_values( $values );

    print_r($r);
  }
}


class Test_map_object_keys
{
  function test()
  {
    $x = array( 'a' , 'b' , 'c' );
    $z = array( 'a3' , 'b2' , 'c1' );

    $r = map_object_keys( $x , $z );

    print_r($r);

/*
stdClass Object
(
    [a] => a3
    [b] => b2
    [c] => c1
)
*/
  }
}


class Test_compute_delivery_daytime
{

  function test()
  {
    global $argv;

    $result = compute_delivery_daytime($argv[2], $argv[3], $argv[4]);

    print_r($result);
  }
}


class Test_convert_value_to_boolean
{
  function test()
  {
    foreach (array('TRUE', 'true', 'FALSE', "\tfalse ", '0', '1', '', NULL, 'five', array()) as $value)
    {
      $result = convert_value_to_boolean($value);
      echo "$value -> " . (gettype($result) === 'boolean' ? ($result ? 'TRUE' : 'FALSE') : 'NULL') . "\n";
    }
  }
}


class Test_has_non_ascii_characters
{
  function test()
  {
    global $argv;

    if ( has_non_ascii_characters( $argv[2] ) ) { echo "YES\n"; } else { echo "NO\n"; }
  }
}


class Test_get_all_http_headers
{
  function test()
  {
    print_r(get_all_http_headers());
  }
}


class Test_ultraErrorHandler
{
  function test()
  {
    // test undefined variable
    $result = $value + 1;

    // test SQL syntax error
    $sql = "select top * from dp_rbac.activity";
    $result = run_sql_and_check($sql);
    
    // test SQL statement error
    $sql = "insert into dp_rbac.activity values (1, 'Activate', 2, getutcdate(), 'test')";
    $result = run_sql_and_check($sql);
  }
}


class Test_strvalArrayValues
{
  function test()
  {
    global $argv;

    $obj = new stdClass();

    $obj->test = 'zonk';

    $a = array(
      'q1' => 'q1',
      'q2' => 'q1',
      'q3' => 1,
      'q4' => 03,
      'q5' => '02',
      'lol' => array(
        'z' => $obj,
        'a1' => 'q1',
        'a2' => 'q1',
        'a3' => 1,
        'a4' => 03,
        'a5' => '02',
        'afk' => array(
          'b1' => 'q1',
          'b2' => 'q1',
          'b3' => 1,
          'b4' => 03,
          'b5' => '02'
        )
      )
    );

    $a = \strvalArrayValues( $a );

    print_r($a);

    $a = \strvalArrayValues( NULL );

    print_r($a);
  }
}

class Test_get_utc_date_formatted_for_mssql
{
  function test()
  {
    global $argv;

    $a = \get_utc_date_formatted_for_mssql();

    echo "$a\n";
  }
}

class Test_date_to_epoch
{
  function test()
  {
    global $argv;
    $date = $argv[2];
    $tz = empty($argv[3]) ? NULL : $argv[3];
    $result = date_to_epoch($argv[2], $tz);
    echo "date: $date, time zone: $tz -> epoch $result \n";
  }
}

class Test_toKeyValString
{
  function test()
  {
    $obj = new stdClass();
    $obj->o_a = 1;
    $obj->o_b = 2;

    $arr = array(
      'a' => 1,
      'b' => 2,
      'obj' => $obj
    );

    $obj->ob_c = $arr;

    print_r( \toKeyValString($obj) );

    $obj = new stdClass();
    $obj->e = 1;
    $obj->f = 2;
    $arr = array('a', 'b', 'c', 'd', $obj);
    echo "\r\n";

    print_r( \toKeyValString($arr) );

    $t = '[{"w":1,"wtimeout":10000},{"un":"*1428702618*","tt":"login"},{"un":"*1428702618*","sa":"AJKFF8tob03X.8J6Sw67x6.HshgYEQ==","ud":"552c3818ea82d7.48364956","tt":"login"},{"upsert":true}]';
    $arr = json_decode($t, true);
    echo "\r\n";
    print_r( \toKeyValString($arr) );
  }
}

class Test_log
{
  function test()
  {
    $message = 'testdataforlog';
    dlog('',"message = %s",$message);
    logFatal( $message );
    logError( $message );
    logInfo( $message );
    logDebug( $message );
    logTrace( $message );
  }
}

# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


/*
$date = '2014-03-18T07:21:00.000-07:00';

echo "$date\n";

echo date_to_datetime($date, TRUE, FALSE);
echo "\n";
echo date_to_datetime($date, TRUE, TRUE);
echo "\n";
echo date_to_datetime($date, FALSE, FALSE);
echo "\n";
echo date_to_datetime($date, FALSE, TRUE);
echo "\n\n";

$date = '2014-03-18T07:21:00.000';

echo "$date\n";

echo date_to_datetime($date, TRUE, FALSE);
echo "\n";
echo date_to_datetime($date, TRUE, TRUE);
echo "\n";
echo date_to_datetime($date, FALSE, FALSE);
echo "\n";
echo date_to_datetime($date, FALSE, TRUE);
echo "\n";
*/

?>

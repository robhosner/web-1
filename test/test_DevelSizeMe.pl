use strict;
use warnings;

use Devel::SizeMe qw(perl_size heap_size);

print STDOUT 'perl_size = '.perl_size()."\n";
print STDOUT 'heap_size = '.heap_size()."\n";

__END__


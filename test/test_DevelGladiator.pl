use strict;
use warnings;

use Data::Dumper;
use Devel::Gladiator qw(walk_arena arena_ref_counts arena_table);

my $all         = walk_arena();
my $arena_table = arena_table();

print STDOUT Dumper($all);
print STDOUT Dumper($arena_table);

__END__


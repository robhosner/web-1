<?php


# php test/test_dlog.php
# php test/test_dlog.php > /tmp/A
# php test/test_dlog.php 2> /tmp/B


require_once 'db.php';

// STDERR
dlog('_ALWAYS_LOG_',"test number 1");

// STDERR
dlog('dummy',"test number 2");

include ((getenv('HTT_CONFIGROOT') . '/e.php'));

if (array_key_exists('logger/log4php', $e_config))
{
  // STDOUT
  echo "logger/log4php is defined - ".$e_config['logger/log4php']."\n";
}
else
  // STDOUT
  echo "logger/log4php is NOT defined\n";

// STDERR
dlog('',"test number 3");

$e_config['logger/log4php'] = 1;

if (array_key_exists('logger/log4php', $e_config))
{
  // STDOUT
  echo "logger/log4php is defined - ".$e_config['logger/log4php']."\n";
}
else
  // STDOUT
  echo "logger/log4php is NOT defined\n";

// STDERR
dlog('',"test number 4");

$e_config['logger/log4php'] = 0;

if (array_key_exists('logger/log4php', $e_config))
{
  // STDOUT
  echo "logger/log4php is defined - ".$e_config['logger/log4php']."\n";
}
else
  // STDOUT
  echo "logger/log4php is NOT defined\n";

// STDERR
dlog('',"test number 5");

?>

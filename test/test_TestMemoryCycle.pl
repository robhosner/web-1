use strict;
use warnings;

use Data::Dumper;
use Test::Memory::Cycle;
use Ultra::Lib::MQ::ControlChannel;


# instantiate Ultra::Lib::MQ::ControlChannel
my $mq_cc = Ultra::Lib::MQ::ControlChannel->new();

memory_cycle_ok( $mq_cc );

#done_testing();

__END__


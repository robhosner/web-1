<?php


date_default_timezone_set("America/Los_Angeles");


include_once('db.php');
include_once('lib/inventory/functions.php');
include_once('lib/inventory/functions_shipwire.php');


# Usage:

# php test/inventory/test_functions.php get_htt_inventory_sim_from_wildcarded_iccid
# php test/inventory/test_functions.php get_sim_info_from_fake_msisdn $FAKE_MSISDN
# php test/inventory/test_functions.php batch_update_htt_inventory_sim
# php test/inventory/test_functions.php api_FulfillmentServices
# php test/inventory/test_functions.php api_TrackingServices
# php test/inventory/test_functions.php api_InventoryServices
# php test/inventory/test_functions.php api_call_shipwire
# php test/inventory/test_functions.php build_xml_FulfillmentServices
# php test/inventory/test_functions.php resurrect_iccid $ICCID


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_get_sim_info_from_fake_msisdn
{
  function test()
  {
    global $argv;

    $r = get_sim_info_from_fake_msisdn( $argv[2] );

    print_r($r);
  }
}


class Test_get_htt_inventory_sim_from_wildcarded_iccid
{
  function test()
  {
    $wildcarded_iccid = '890126084[0-9][0-9]1733198[0-9]';

    $r = get_htt_inventory_sim_from_wildcarded_iccid( $wildcarded_iccid );

    print_r($r);
  }
}


class Test_api_InventoryServices
{
  function test()
  {
    $r = api_InventoryServices( array() );

    print_r($r);
  }
}


class Test_api_TrackingServices
{
  function test()
  {
    $r = api_TrackingServices(
      array(
        #'bookmark' => 1 # OK
        # 'shipwire_id' => '1344723285-470860-1' # OK
        # 'shipwire_id' => '1348584820-410929-1' # test-2
        # 'shipwire_id' => '1348586264-141988-1' # test-3
        # 'shipwire_id' => '1348587158-221023-1' # test-4
        # 'order_no' => 132 # OK
        'order_no' => '1348584820-410929-1'
      )
    );

    print_r($r);
  }
}


class Test_api_FulfillmentServices
{
  function test()
  {
    $r = api_FulfillmentServices( test_data_FulfillmentServices() );

    print_r($r);
  }
}


class Test_resurrect_iccid
{
  function test()
  {
    global $argv;

    $r = resurrect_iccid( $argv[2] );

    print_r( $r );

    exit;
  }
}


class Test_api_call_shipwire
{
  function test()
  {
    $xml_content = build_xml_FulfillmentServices( array() );

    $params = array(
      'url' => 'https://api.shipwire.com/exec/FulfillmentServices.php',
      'api_xml_name' => 'OrderListXML',
      'xml_content'  => $xml_content
    );

    $r = api_call_shipwire($params);

    print_r($r);
  }
}


class Test_build_xml_FulfillmentServices
{
  function test()
  {
    $test_data = test_data_FulfillmentServices();

    $xml_FulfillmentServices = build_xml_FulfillmentServices( $test_data );

    echo "$xml_FulfillmentServices\n";

    $test_data['orders'][0]['address_1'] = "12345678901234567890123456789012345678901";
    $test_data['orders'][0]['address_2'] = "12345678901234567890123456789012345678901";

    $xml_FulfillmentServices = build_xml_FulfillmentServices( $test_data );

    echo "$xml_FulfillmentServices\n";
  }
}


class Test_batch_update_htt_inventory_sim
{
  function test()
  {
    $test_params = array(
      'startICCID' => 1,
      'endICCID'   => 2
    );

    $errors = batch_update_htt_inventory_sim(
      array(
        'startICCID'       => $test_params['startICCID'],
        'endICCID'         => $test_params['endICCID'],
        'inventory_status' => 'TEST'
      )
    );

    test_output( array( 'errors' => $errors ) );

    $errors = batch_update_htt_inventory_sim(
      array(
        'startICCID'            => $test_params['startICCID'],
        'endICCID'              => $test_params['endICCID'],
        'inventory_masteragent' => 'TEST'
      )
    );

    test_output( array( 'errors' => $errors ) );
  }
}


function test_output($result)
{
  if ( count($result['errors']) )
  {
    echo "Test got errors:\n";
    print_r($result);
  }
  else
  {
    echo "Test OK\n";
  }
}


function test_data_FulfillmentServices()
{
#    return
#    array(
#      'referer' => 'test_referer_4',
#      'orders'  => array(
#        array(
#          'order_id'  => 'Test-4',
##          'warehouse' => 'Philadelphia',
#          'full_name' => 'BA Telecom',
#          'address_1' => '33 Nassau Ave',
#          'address_2' => 'Suite 37',
#          'city'      => 'Brooklyn',
#          'state'     => 'NY',
#          'country'   => 'US United States',
#          'zip_code'  => '11222',
#          'phone'     => '',
#          'email'     => 'chris@ultra.me',
#          'items'     => array(
#            array( 'code' => 'ULTRA-KIT1-STD' , 'quantity' => 1 )
#          )
#        )
#      )
#    );

  return
    array(
      'referer' => 'test_referer',
      'orders'  => array(
        array(
          'order_id'  => '123test456',
          'warehouse' => 'test_warehouse',
          'full_name' => 'Alan Turing',
          'address_1' => '341 Nassau Ave',
          'address_2' => 'ste 1234567890',
          'city'      => 'Nowhere',
          'state'     => 'NY',
          'country'   => 'US United States',
          'zip_code'  => '12345',
          'phone'     => '555.444.3210',
          'email'     => '12345@example.com',
          'items'     => array(
            array( 'code' => '1Afoo' , 'quantity' => 123 ),
            array( 'code' => '2Bbar' , 'quantity' => 456 )
          )
        )
      )
    );
}


$test_params = array(
  'startICCID' => 1,
  'endICCID'   => 2
);


# perform test #


teldata_change_db(); // DB connection

$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


?>

<?php

/*

HTT_ACTIVATION_LOG
->
ULTRA.HTT_ACTIVATION_HISTORY


 select top 1 * from ULTRA.HTT_ACTIVATION_HISTORY;

CUSTOMER_ID:                        2604.000000
ICCID_FULL:                8901260842102267078
MSISDN:                    NULL
COS_ID:                            98277.000000
MASTERAGENT:                           0.000000
DISTRIBUTOR:               NULL
DEALER:                                0.000000
USERID:                    NULL
CREATION_DATE:             Nov 14 2013 07:01:05:757PM
ACTIVATION_TYPE:           NULL
ACTIVATION_TRANSITION:     NULL
FINAL_STATE:               NULL
PLAN_STARTED_DATE_TIME:    NULL
FUNDING_SOURCE:            111
FUNDING_AMOUNT:                      222.000000
LAST_UPDATED_DATE:         Nov 14 2013 08:18:16:607PM
LAST_ATTEMPTED_TRANSITION: NULL
 

 select top 1 * from HTT_ACTIVATION_LOG;

ICCID_NUMBER:          800000000000000001
ACTIVATED_CUSTOMER_ID:          1234.000000
ACTIVATED_MASTERAGENT:            12.000000
ACTIVATED_DISTRIBUTOR:            11.000000
ACTIVATED_DEALER:                 10.000000
ACTIVATED_USERID:                 90.000000
ACTIVATED_DATE:        Oct 14 2012 08:34:23:600PM
ACTIVATED_BY:          80
ICCID_FULL:            NULL

*/

include_once 'db.php';

teldata_change_db();

$insert = "IF NOT EXISTS ( SELECT CUSTOMER_ID FROM ULTRA.HTT_ACTIVATION_HISTORY WHERE CUSTOMER_ID = ___ID___ ) BEGIN
INSERT INTO ULTRA.HTT_ACTIVATION_HISTORY ( CUSTOMER_ID , ICCID_FULL , MSISDN , COS_ID , MASTERAGENT , DISTRIBUTOR , DEALER , USERID , CREATION_DATE , FINAL_STATE , LAST_UPDATED_DATE ) VALUES (";

$sql_htt_activation_log = 
" SELECT 
         ICCID_NUMBER             iccid,
         ACTIVATED_CUSTOMER_ID    customer_id,
         ACTIVATED_MASTERAGENT    masteragent,
         ACTIVATED_DISTRIBUTOR    distributor,
         ACTIVATED_DEALER         dealer,
         ACTIVATED_USERID         userid,
         ACTIVATED_DATE           creation_date,
         ICCID_FULL               iccid_full,
         u.current_mobile_number,
         u.ACTIVATION_ICCID,
         u.ACTIVATION_ICCID_FULL,
         u.PLAN_STATE,
         a.COS_ID                 cos_id,
         r.MSISDN                 deleted_msisdn,
         r.ICCID                  deleted_iccid,
         r.COS_ID                 deleted_cos_id
  FROM   HTT_ACTIVATION_LOG l
  LEFT OUTER JOIN ACCOUNTS a
  ON     l.ACTIVATED_CUSTOMER_ID = a.CUSTOMER_ID
  LEFT OUTER JOIN HTT_CUSTOMERS_OVERLAY_ULTRA u
  ON     l.ACTIVATED_CUSTOMER_ID = u.CUSTOMER_ID
  LEFT OUTER JOIN HTT_CANCELLATION_REASONS r
  ON     l.ACTIVATED_CUSTOMER_ID = r.CUSTOMER_ID
";

$data = mssql_fetch_all_objects(logged_mssql_query($sql_htt_activation_log));

foreach( $data as $row )
{
  insert_row( $insert , $row );
}

function insert_row( $insert , $row )
{
  $iccid_full = $row->iccid_full;

  if ( ! $iccid_full ) $iccid_full = $row->iccid;
  if ( ! $iccid_full ) $iccid_full = $row->ACTIVATION_ICCID_FULL;
  if ( ! $iccid_full ) $iccid_full = $row->ACTIVATION_ICCID;

  if ( $iccid_full )
  { $iccid_full = "'".luhnenize($iccid_full)."'"; }
  else
  { $iccid_full = "NULL"; }

  $msisdn = $row->deleted_msisdn;

  if ( $row->current_mobile_number ) $msisdn = $row->current_mobile_number;
  if ( ( ! $msisdn ) || ( $msisdn = '' ) || ( $msisdn = ' ' ) ) $msisdn = "NULL";

  $cos_id = $row->deleted_cos_id;

  if ( ! $cos_id ) $cos_id = $row->cos_id; 
  if ( ! $cos_id ) $cos_id = 0;

  $masteragent = 0;
  $distributor = 0;
  $dealer      = 0;
  $userid      = 0;

  if ( $row->masteragent ) $masteragent = $row->masteragent;
  if ( $row->distributor ) $distributor = $row->distributor;
  if ( $row->dealer      ) $dealer      = $row->dealer; 
  if ( $row->userid      ) $userid      = $row->userid; 

  $creation_date = 'getutcdate()';

  if ( $row->creation_date )
    $creation_date = "'".$row->creation_date."'";

  $plan_state = $row->PLAN_STATE;

  if ( $row->deleted_msisdn || $row->deleted_iccid || $row->deleted_cos_id || ( $row->PLAN_STATE == 'Cancelled' ) )
    $plan_state = 'Cancelled';

  if ( ( $row->PLAN_STATE == 'Suspended' ) || ( $row->PLAN_STATE == 'Active' ) )
    $plan_state = 'Active';

  if ( ! $plan_state ) $plan_state = 'Cancelled';

  $sql_insert = $insert;

  $sql_insert = str_replace(' ___ID___ ', $row->customer_id, $sql_insert);

  $sql_insert .=
        $row->customer_id .
    ','.$iccid_full .
    ','.$msisdn .
    ','.$cos_id .
    ','.$masteragent .
    ','.$distributor .
    ','.$dealer .
    ','.$userid .
    ','.$creation_date .
    ',"'.$plan_state .
    '",'.$creation_date .
    ") END";

  echo "$sql_insert\n";

  if ( !run_sql_and_check($sql_insert) ) echo "SQL failed\n\n";
}

?>

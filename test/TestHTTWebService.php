<?php
  //soap settings
  //$wsdlBase = 'http://indiald/'; // Development
  $wsdlBase = 'http://demo.mfundsllc.com/'; // Staging
  //$wsdlBase = 'https://backoffice.mfundsllc.com/'; // Production
  $partnerPassword = "foo";
  
  /*** Step 1 - Test with an existing card ***/
  
  //Build soap client
  $soapCardOrder = new SoapClient($wsdlBase . 'partnerwebservice/httwebservice.asmx?WSDL');
  //$soapCardOrder = new SoapClient('http://demo.mfundsllc.com/partnerwebservice/httwebservice.asmx?WSDL');
  
  //Test patner password
  $params = array();
  $params['partnerPassword'] = $partnerPassword;
  $result = $soapCardOrder->TestPartnerPassword($params);
  print_r($result);
  
  //Get card details
  $params = array();
  $params['partnerPassword'] = $partnerPassword;
  $params['personIdentifier'] = "1077045858";
  $params['last4'] = "4887";
  $result = $soapCardOrder->CardDetails($params);
  print_r($result);

  //Get Account Balance
  $params = array();
  $params['partnerPassword'] = $partnerPassword;
  $params['personIdentifier'] = "1077045858";
  $params['last4'] = "4887";
  $result = $soapCardOrder->AccountBalance($params);
  print_r($result);

  //Load Card
  $params = array();
  $params['partnerPassword'] = $partnerPassword;
  $params['personIdentifier'] = "1077045858";
  $params['last4'] = "4887";
  $params['amount'] = "1.07";
  $params['description'] = "Simulate GreenDot load";
  $params['clientRefId'] = uniqid('LoadAccountTest-', true);
  //$result = $soapCardOrder->LoadAccountHTT($params);
  //print_r($result);

  //Get Account Balance
  $params = array();
  $params['partnerPassword'] = $partnerPassword;
  $params['personIdentifier'] = "1077045858";
  $params['last4'] = "4887";
  //$result = $soapCardOrder->AccountBalance($params);
  //print_r($result);

  /*** Step 2 - Test creating a virtual card, that a person will Load from GreenDot MoneyPak ***/
  //Get Order Card
  //NOTE: This can fail if IDV (Identity Verification) fails for the person.
  //The card order will be set in the pending state and Last4 will not be returned.
  $params = array();
  $params['partnerPassword'] = $partnerPassword;
  $params['clientRefId'] = uniqid('OrderCardTest-', true);
  $params['FirstName'] = "HTT";
  $params['MidInitial'] = "";
  $params['LastName'] = "IndiaLD";
  $params['Address1'] = "9121 W. Russell Rd.";
  $params['Address2'] = "Ste 208";
  $params['City'] = "Las Vegas";
  $params['State'] = "NV";
  $params['Zip'] = "89148";
  $params['HomePhone'] = "7022166880";
  $params['WorkPhone'] = "";
  $params['Email'] = "doug.meli@mfundsglobal.com";
  $params['DateOfBirth'] = "01/01/1980";
  $params['DriversLicenseState'] = "";
  $params['DriversLicense'] = "";
  $params['SocialSecurity'] = "555555555";
  $params['ClientId'] = "175700";
  $params['SubProgramId'] = "529572";
  $params['PackageId'] = "184997"; // Virtual
  $params['Amount'] = "0";
  $params['CompanyId'] = "17898";
  $params['EmployeeId'] = "IndiaLD-Username";
  $result = $soapCardOrder->OrderCardHTT($params);
  print_r($result);
  
  $personId = $result->OrderCardHTTResult->CardIdentifier;
  $last4 = $result->OrderCardHTTResult->Last4; 
 
  //Get card details
  $params = array();
  $params['partnerPassword'] = "foo";
  $params['personIdentifier'] = $personId;
  $params['last4'] = $last4;
  $result = $soapCardOrder->CardDetails($params);
  print_r($result);

  //Enable to simulate GreenDot load
  //Load Card
  $params = array();
  $params['partnerPassword'] = "foo";
  $params['personIdentifier'] = $personId;
  $params['last4'] = "4887";
  $params['amount'] = "1.07";
  $params['description'] = "Simulate GreenDot load";
  $params['clientRefId'] = uniqid('LoadAccountTest-', true);
  //$result = $soapCardOrder->LoadAccountHTT($params);
  //print_r($result);

  /*** Step 3 - User comes back to order physical card. Create their mFunds user account  ***/

  //Convert the Card from Physical to Virtual
  //All the money from virtual card will be transferred to the
  //physical card. Virtual card will be closed once the new
  //physical card is received and activated by the user.
  $params = array();
  $params['partnerPassword'] = "foo";
  $params['personIdentifier'] = $personId;
  $params['last4'] = $last4;
  $params['newSubProgramId'] = "529572"; // Pass same value as that for the virtual card
  $params['newPackageId'] = "184765"; // Personalized physical package
  $params['clientRefId'] = uniqid('ConvertCardTest-', true);
  $result = $soapCardOrder->ConvertCardHTT($params);
  print_r($result);
  
  $personId = $result->ConvertCardHTTResult->CardIdentifier;
  $last4 = $result->ConvertCardHTTResult->Last4; 
  
  //Get card details for the physical card
  $params = array();
  $params['partnerPassword'] = "foo";
  $params['personIdentifier'] = $personId;
  $params['last4'] = $last4;
  $result = $soapCardOrder->CardDetails($params);
  print_r($result);
  
  $username = 'HTT_'.$personId.'_'.$last4; // need to have a unique user id
  
  // Create the account for this user using mobile number as 0
  // This is done so that that call to create the user does not fail
  // because the mobile number is already in use with another card.
  // We will update the mobile number with another web service call
  $params = array();
  $params['partnerPassword'] = "foo";
  $params['personIdentifier'] = $personId;
  $params['last4'] = $last4;
  $params['username'] = $username;
  $params['password'] = 'password'; // replace with some secure password. User will not use this. 
                                    // This is used between HTT web site and mFunds Web service.
                                    // This will be hashed before being stored in mFunds DB.
  $params['mdn'] = 0;
  $params['mobilePin'] = $last4;
  $params['clientRefId'] = uniqid('CreateMobileWalletTest-', true);
  $result = $soapCardOrder->CreateMobileWallet($params);
  print_r($result);
  
  // NOTE: After this step you can login as the user created above on the following portal
  // http://indiald.mfundsmobile.com/portaldev using the username and password created above.
  // You will have to type the actual URLs of the pages (that will be iFramed).
  // The menu navigation will be implemented by the HTT portals.
  
  /*** Step 4 - User might wanto link their mobile number to the card for SMS support and mobile to mobile trasnfer  ***/
  
  // This API will be used by HTT web site to ask their users to register their Mobile number
  // in case they want to use the SMS features or transfer money using mobile number. 
  // Otherwise, this is optional.
  $dummy_mobile_number = $personId%10000000000; // take the last 10 digits only
 
  $params = array();
  $params['partnerPassword'] = "foo";
  $params['personIdentifier'] = $personId;
  $params['last4'] = $last4;
  $params['username'] = $username;
  $params['mdn'] = $dummy_mobile_number;
  $result = $soapCardOrder->ChangeMobileNumber($params);
  print_r($result);

  /*** Step 5 - HTT website will need to check the card activation status before they can allow single sign on  ***/

  // This API will be used by HTT web site to check the status of the
  // physical card. If the card is not Active (status=2), the user will
  // be sent to the page with information to activate the card.
  // If the card is active, they will be redirected to a page where
  // they will be logged on the the cardholder portal hoster inside an iFrame.
  $params = array();
  $params['partnerPassword'] = "foo";
  $params['personIdentifier'] = $personId;
  $params['last4'] = $last4;
  $result = $soapCardOrder->CardStatus($params);
  print_r($result);

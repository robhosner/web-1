<?php


# Usage:
# php test/api/test_ultrainfo.php InternationalCallingRates
# php test/api/test_ultrainfo.php SMSCountryList
# php test/api/test_ultrainfo.php GetLocations


include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');


$apache_username = 'dougmeli';
$apache_password = 'Flora';


$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$base_url = find_site_url().'/pr/api_public/1/ultra/api/';
#$base_url = 'https://rgalli4-dev.uvnv.com/pr/api_public/1/ultra/api/';


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_ultrainfo__SMSCountryList extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'ultrainfo__SMSCountryList';

    $params = array(
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_ultrainfo__InternationalCallingRates extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'ultrainfo__InternationalCallingRates';

    $params = array(
      'product_name' => 'UVCARD'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_ultrainfo__GetLocations extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'ultrainfo__GetLocations';

    $params = array(
      "latitude"      => $argv[2],
      "longitude"     => $argv[3],
      "radius"        => $argv[4],
      "max_entries"   => 3,
      "location_type" => $argv[5]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


# perform test #


$testClass = 'Test_ultrainfo__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


/*

php test/api/test_ultrainfo.php GetLocations +40.05 -74.40 1000 ALL

*/

?>

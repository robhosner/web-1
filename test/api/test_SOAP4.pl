#!/usr/bin/perl


use warnings;
use strict;


use Data::Dumper;
use XML::Compile::WSDL11;
use XML::Compile::SOAP11;
use XML::Compile::SOAP::Trace;
use XML::Compile::Transport::SOAPHTTP;


# Load and compile all the WSDL files.
my $wsdl = XML::Compile::WSDL11->new("runners/soapy/MVNOWSAPI-20121012/MVNOWSAPIService.wsdl");
$wsdl->importDefinitions("runners/soapy/MVNOWSAPI-20121012/xsd0.xsd");
$wsdl->importDefinitions("runners/soapy/MVNOWSAPI-20121012/xsd1.xsd");
$wsdl->importDefinitions("runners/soapy/MVNOWSAPI-20121012/xsd2.xsd");
$wsdl->importDefinitions("runners/soapy/MVNOWSAPI-20121012/xsd3.xsd");
$wsdl->compileCalls;

my $command = 'ModifyBalance';

my $params =
{
  'tag' => 'raf_'.time,
  'msisdn' => '15189864359',
  'container' => 'Promotional Cash',
  'isAbsolute' => 1
};

my ($answer, $trace);

  eval
  {
    ($answer,$trace) = $wsdl->call( $command => { parameters => $params } );
  };

  if ( $@ )
  {
    die $@;
  }
  elsif ( ! $trace )
  {
    die("No trace available after $command SOAP call");
  }
  else
  {
    print "\nSOAP request:\n";
    print $trace->request()->content();

    print "\nSOAP response:\n";
    print $trace->response()->content();

    # returns SOAP request
    # $return->{ 'soapRequest' } = $this->prettyPrintXML( $trace->request()->content() );
    # # returns SOAP response
    #$return->{ 'soapResponse' } = $this->prettyPrintXML( $trace->response()->content() );
    #$return->{ 'outcome' } = $trace->response()->status_line();
  }

print "\n";

__END__


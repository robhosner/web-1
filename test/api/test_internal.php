<?php


# Usage:

# php test/api/test_internal.php ReassignUltraICCID  $OLD_ICCID  $NEW_ICCID  $MSISDN $TXT
# php test/api/test_internal.php ReassignUltraMSISDN $OLD_MSISDN $NEW_MSISDN $ICCID  $TXT
# php test/api/test_internal.php GetAllAbortedTransitions $DATE
# php test/api/test_internal.php RecoverFailedTransition $TRANSITION_UUID
# php test/api/test_internal.php GetCustomerData
# php test/api/test_internal.php SubmitChargeAmount         $ID $AMOUNT $REASON
# php test/api/test_internal.php WalletNotificationHandler
# php test/api/test_internal.php QuerySIMByICCID $ICCID
# php test/api/test_internal.php GetAllCustomerStateTransitions
# php test/api/test_internal.php IsEligiblePortIn $ID $MSISDN
# php test/api/test_internal.php QueryPortIn $CUSTOMER_ID
# php test/api/test_internal.php QueryMSISDN $MSISDN
# php test/api/test_internal.php CheckTransition $ID
# php test/api/test_internal.php CheckAction $ID
# php test/api/test_internal.php CancelPort $ID $MSISDN
# php test/api/test_internal.php Test                       $ID
# php test/api/test_internal.php TakeTransition             $ID $RESOLVE_NOW $TRANSITION_NAME $DRY_RUN
# php test/api/test_internal.php CancelFraudCustomer        $MSISDN $ID $REASON

# php test/api/test_internal.php Crone                       $MODE
# php test/api/test_internal.php States
# php test/api/test_internal.php ChangeState                 $CUSTOMER_ID $RESOLVE_NOW $PLAN $STATE_NAME $DRY_RUN $MAX_PATH_DEPTH
# php test/api/test_internal.php UpdateActionResult          $STATES $ACTION_ID $RESULT $RESOLVE_NOW
# php test/api/test_internal.php AppendAction                $CUSTOMER_ID $TRANSITION_ID $TRANSITION_CONTEXT $ACTION_TYPE $ACTION_NAME $ACTION_TRANSACTION $ACTION_SEQ $ACTION_PARAMS $ACTION_FPARAMS $PENDING
# php test/api/test_internal.php GetState                    $CUSTOMER_ID
# php test/api/test_internal.php GetActionsByTransition      $TRANSITION_UUID
# php test/api/test_internal.php GetAllPendingTransitions
# php test/api/test_internal.php GetPendingTransitions       $CUSTOMER_ID
# php test/api/test_internal.php ResolvePendingTransitions   $CUSTOMER_ID
# php test/api/test_internal.php QueryNetworkSettingsByICCID $MSISDN
# php test/api/test_internal.php MonthlyServiceChargeStats   $STATS_DATE
# php test/api/test_internal.php TestVersion2

include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');

$apache_username = 'dougmeli';
$apache_password = 'Flora';


$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$base_url = find_site_url().'/pr/internal/1/ultra/api/';

abstract class AbstractTestStrategy
{
  abstract function test();

  function postAndPrint($url,$params) {
     global $curl_options;

     $json_result = curl_post($url,$params,$curl_options,NULL,360);
     echo "\n\n$json_result\n\n";
     print_r($json_result);
  }
}

class Test_internal__Test extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__Test';

    // tests customer/ultra exists
    $params = array(
      'customer_id' => 3488
    );
    $this->postAndPrint($url, $params);

    // tests string too short
    $params = array(
      'test_string' => 'A'
    );
    $this->postAndPrint($url,$params);

    // tests string too long
    $params = array(
      'test_string' => 'AAAAAAAAAAAA'
    );
    $this->postAndPrint($url,$params);

    // valid string
    $params = array(
      'test_string' => 'AAAAAA'
    );
    $this->postAndPrint($url,$params);

    // test invalid match
    $params = array(
      'test_matches' => 'C'
    );
    $this->postAndPrint($url,$params);

    // test valid match
    $params = array(
      'test_matches' => 'A'
    );
    $this->postAndPrint($url,$params);

    // test format invalid (for JSON)
    $params = array(
      'test_format' => 'invalid'
    );
    $this->postAndPrint($url,$params);

    // test format valid (for JSON)
    $params = array(
      'test_format' => json_encode(array('test' => 1))
    );
    $this->postAndPrint($url,$params);

    // test invalid date format
    $params = array(
      'test_date_format' => 'invalid'
    );
    $this->postAndPrint($url,$params);

    // test valid date format
    $params = array(
      'test_date_format' => '07-02-2014'
    );
    $this->postAndPrint($url,$params);

    // test transition exists
    $params = array(
      'test_transitions_exists' => $argv[2] // '{TX-7208D8F68FF55C2B-61BE6827307CA097}'
    );
    # $this->postAndPrint($url,$params);
  }
}

class Test_internal__CancelFraudCustomer extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__CancelFraudCustomer';

    $params = array(
      'customer_id'     => $argv[2],
      'msisdn'          => $argv[3],
      'reason'          => $argv[4]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,360);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}

class Test_internal__TakeTransition extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__TakeTransition';

    $params = array(
      'customer_id'     => $argv[2],
      'resolve_now'     => $argv[3],
      'transition_name' => $argv[4],
      'dry_run'         => $argv[5]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,360);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__RecoverFailedTransition extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__RecoverFailedTransition';

    $params = array(
      'transition_uuid' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__CheckTransition extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__CheckTransition';

    $params = array(
      'transition_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__GetCustomerData extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__GetCustomerData';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__CheckAction extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__CheckAction';

    $params = array(
      'action_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__IsEligiblePortIn extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__IsEligiblePortIn';

    $params = array(
      'customer_id' => $argv[2],
      'msisdn'      => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__QueryPortIn extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__QueryPortIn';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__QueryMSISDN extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__QueryMSISDN';

    $params = array(
      'msisdn' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__GetAllCustomerStateTransitions extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__GetAllCustomerStateTransitions';

    $params = array(
      'customer_id' => 19257
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__GetAllAbortedTransitions extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__GetAllAbortedTransitions';

    $params = array(
      'transition_date' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__WalletNotificationHandler extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'internal__WalletNotificationHandler';

    $params = array(
      'msisdn'       => '1001001000',
      'balance_type' => 'Promotional Cash',
      'threshold'    => '11.11',
      'new_balance'  => '22.22',
      'message'      => 'test'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_internal__SubmitChargeAmount extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__SubmitChargeAmount';

    $params = array(
      'customer_id' => $argv[2],
      'amount'      => $argv[3],
      'reason'      => $argv[4],
      'reference'   => time(),
      'partner_tag' => 'Test_internal__SubmitChargeAmount'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}

class Test_internal__Crone extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__Crone';

    $params = array(
      'mode' => $argv[2]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__States extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__States';

    $params = array();

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__ChangeState extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__ChangeState';

    $params = array(
      'customer_id' => $argv[2],
      'resolve_now' => $argv[3],
      'plan' => $argv[4],
      'state_name' => $argv[5],
      'dry_run' => $argv[6],
      'max_path_depth' => $argv[7]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__AppendAction extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__AppendAction';

    $params = array(
      'customer_id' => $argv[2],
      'transition_id' => $argv[3],
      'transition_context' => $argv[4],
      'action_type' => $argv[5],
      'action_name' => $argv[6],
    );

    if (isset($argv[7]))
	$params['action_transaction'] = $argv[6];

    if (isset($argv[8]))
       $params['action_seq'] = $argv[7];

    if (isset($argv[9]))
       $params['action_params'] = $argv[8];

    if (isset($argv[10]))
       $params['action_fparams'] = $argv[9];

    if (isset($argv[11]))
       $params['pending'] = $argv[10];

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__GetState extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__GetState';

    $params = array(
      'customer_id' => $argv[2]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__GetActionsByTransition extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__GetActionsByTransition';

    $params = array(
      'transition_uuid' => $argv[2]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__GetAllPendingTransitions extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__GetAllPendingTransitions';

    $params = array();

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__GetPendingTransitions extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__GetPendingTransitions';

    $params = array(
      'customer_id' => $argv[2]
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__ResolvePendingTransitions extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__ResolvePendingTransitions';

    $params = array(
      'customer_id' => $argv[2]
    );

    if (isset($argv[3]))
      $params['environments'] = $argv[3];

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__MonthlyServiceChargeStats extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'internal__MonthlyServiceChargeStats';

    $params = array(
       'stats_date' => $argv[2] // format mm-dd-yyyy
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

class Test_internal__TestVersion2 extends AbstractTestStrategy {
  function test() {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'internal__TestVersion2';

    $params = array(
      'input1' => 1,
      'input2' => 'A', // match A or B
    );

    $this->postAndPrint($url,$params,$curl_options);
  }
}

# perform test #


$testClass = 'Test_internal__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


/*
Examples:

php test/api/test_internal.php SubmitChargeAmount 2215 2900 ULTRA_PLAN_RECHARGE

php test/api/test_internal.php TakeTransition 2132 1 "Change Plan Mid-Cycle L29 to L39" 1

*/

?>

<?php


# Usage:


# php test/api/test_customercare.php SearchCustomers $MSISDN $ICCID $NAME $EMAIL $ACTCODE
# php test/api/test_customercare.php CustomerInfoByID $CUSTOMER_ID  TEST OK
# php test/api/test_customercare.php CheckInternationalDirectDialSettings $CUSTOMER_ID
# php test/api/test_customercare.php ShowInternationalDirectDialErrors    
# php test/api/test_customercare.php FlushStoredBalance              TEST OK
# php test/api/test_customercare.php SetCustomerCreditCard           TEST OK
# php test/api/test_customercare.php ChargeCreditCard                OK: Active; To test: Suspended->Active; To test other states
# php test/api/test_customercare.php ChargePinCard $CUSTOMER_ID $MSISDN $STORE_ID $USER_ID $DESTINATION $PIN [ $PIN2 $PIN3 $PIN4 $PIN5 $PIN6 ]
# php test/api/test_customercare.php CourtesyMinutes                 		TEST OK
# php test/api/test_customercare.php AddCourtesyMinutes $AMOUNT      		TEST OK
# php test/api/test_customercare.php CourtesyCash                    		TEST OK
# php test/api/test_customercare.php AddCourtesyCash $AMOUNT $CUSTOMER_ID	TEST OK
# php test/api/test_customercare.php LoginAsCustomer                 		TEST OK
# php test/api/test_customercare.php ReplaceSIMCard $CUSTOMER_ID $OLD_ICCID $NEW_ICCID
# php test/api/test_customercare.php ChangePhoneNumber $REASON $AGENT $EVENT_ID $CUSTOMER_ID $OLD_MSISDN $ZIPCODE
# php test/api/test_customercare.php CustomerInfo $MSISDN            TEST OK
# php test/api/test_customercare.php ReshipSIM                       TEST OK
# php test/api/test_customercare.php DealerLoginAsCustomer           
# php test/api/test_customercare.php ShipwireOrderDetails            OK
# php test/api/test_customercare.php GetCustomerShipwireOrders       OK
# php test/api/test_customercare.php ResetPasswordSMS $CUSTOMER_ID   OK
# php test/api/test_customercare.php TransitionProvisionedToActive   OK
# php test/api/test_customercare.php VoidCourtesyCash                OK
# php test/api/test_customercare.php RemoveCash $AMOUNT              TEST OK
# php test/api/test_customercare.php RemoveMinutes $AMOUNT           TEST OK
# php test/api/test_customercare.php JumpstartToActive $CUSTOMER_ID
# php test/api/test_customercare.php AspiderCancelPortIn $CUSTOMER_ID
# php test/api/test_customercare.php CustomerDirectSMS $AGENT $CUSTOMER_ID $MESSAGE
# php test/api/test_customercare.php AddCourtesyStoredValue 		TEST OK
# php test/api/test_customercare.php EnsureSOCs $MSISDN
# php test/api/test_customercare.php ValidateMSISDN $MSISDN
# php test/api/test_customercare.php ApplyDataRecharge $MSISDN $DATA_SOC
# php test/api/test_customercare.php CheckDataRechargeByCustomerId $CUSTOMER_ID
# php test/api/test_customercare.php ApplyVoiceRecharge $MSISDN
# php test/api/test_customercare.php CheckVoiceRechargeByCustomerId $CUSTOMER_ID
# php test/api/test_customercare.php GetRechargeHistory $CUSTOMER_ID
# php test/api/test_customercare.php GetCustomerCycleHistory
# php test/api/test_customercare.php CancelDeviceLocation $CUSTOMER_ID
# php test/api/test_customercare.php ReplaceSIMCardByMSISDN $MSISDN $NEW_ICCID $STORE $USER
# php test/api/test_customercare.php GetMVNEDetails $ICCID $MSISDN
# php test/api/test_customercare.php mvneQueryPortIn    $CUSTOMER_ID
# php test/api/test_customercare.php mvneCancelPortIn   $CUSTOMER_ID
# php test/api/test_customercare.php mvneCancelLocation $CUSTOMER_ID
# php test/api/test_customercare.php ValidateSIMOrigin $ICCID


include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');


$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$partner = 'customercare';
// $partner = 'celluphone';

$base_url = find_site_url().'/pr/'.$partner.'/1/ultra/api/';


$tests_outcome = array(
 'ok' => 0,
 'ko' => 0
);


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_customercare__mvneQueryPortIn extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__mvneQueryPortIn';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__mvneCancelPortIn extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__mvneCancelPortIn';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__mvneCancelLocation extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__mvneCancelLocation';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__GetCustomerCycleHistory extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__GetCustomerCycleHistory';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__CancelDeviceLocation extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__CancelDeviceLocation';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__ReplaceSIMCardByMSISDN extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ReplaceSIMCardByMSISDN';

    $base_params = array(
      'msisdn'    => $argv[2],
      'new_ICCID' => $argv[3],
      'store_id'  => $argv[4],
      'user_id'   => $argv[5]
    );

    $json_result = curl_post($url,$base_params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__CheckVoiceRechargeByCustomerId extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__CheckVoiceRechargeByCustomerId';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__GetRechargeHistory extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__GetRechargeHistory';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__ApplyVoiceRecharge extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ApplyVoiceRecharge';

    $params = array(
      'msisdn'       => $argv[2],
      'voice_soc_id' => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__ApplyDataRecharge extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ApplyDataRecharge';

    $params = array(
      'msisdn'         => $argv[2],
      'data_soc_id'    => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__TransitionProvisionedToActive extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__TransitionProvisionedToActive';

    $params = array(
      'customer_id' => 1465
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__CheckDataRechargeByCustomerId extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__CheckDataRechargeByCustomerId';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__ValidateMSISDN extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ValidateMSISDN';

    $params = array(
      'msisdn' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__AddCourtesyStoredValue extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__AddCourtesyStoredValue';

    $params = array(
      'reason' => 'tozzi fan',
      'agent_name' => 'agent_name',
      'amount' => 2400,
      'customercare_event_id' => 11,
      'customer_id' => 18942
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__EnsureSOCs extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__EnsureSOCs';

    $params = array(
      'msisdn' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__SearchCustomers extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__SearchCustomers';

    $params = array(
      'msisdn'  => empty($argv[2]) ? NULL : $argv[2],
      'iccid'   => empty($argv[3]) ? NULL : $argv[3],
      'name'    => empty($argv[4]) ? NULL : $argv[4],
      'email'   => empty($argv[5]) ? NULL : $argv[5],
      'actcode' => empty($argv[6]) ? NULL : $argv[6]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__CustomerInfoByID extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $base_url = str_replace('customercare', 'celluphone', $base_url);
    $url = $base_url . 'customercare__CustomerInfoByID';

    $params = array(
      'customer_id' => $argv[2],
      'mode' => 'OCS_BALANCE'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__CheckInternationalDirectDialSettings extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__CheckInternationalDirectDialSettings';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__ShowInternationalDirectDialErrors extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ShowInternationalDirectDialErrors';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__FlushStoredBalance extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__FlushStoredBalance';

    $params = array(
      'reason'      => 'testing',
      'customer_id' => $argv[2],
      'agent_name'  => 'Raffaello',
      'customercare_event_id' => time()
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__CustomerDirectSMS extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__CustomerDirectSMS';

    $params = array(
      'agent'       => $argv[2],
      'customer_id' => $argv[3],
      'message'     => $argv[4]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__AspiderCancelPortIn extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__AspiderCancelPortIn';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__JumpstartToActive extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__JumpstartToActive';

    $params = array(
      'customer_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__RemoveMinutes extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__RemoveMinutes';

    $params = array(
      'reason'         => 'testing',
      'agent_name'     => 'Kenny',
      'customer_id'    => 552,
      'customercare_event_id' => '987654',
      'amount'                => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,300);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__RemoveCash extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__RemoveCash';

    $params = array(
      'reason'         => 'testing',
      'agent_name'     => 'Kenny',
      'customer_id'    => 552,
      'customercare_event_id' => '9876',
      'amount'                => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__VoidCourtesyCash extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'customercare__VoidCourtesyCash';

    $params = array(
      'reason'         => 'testing',
      'agent_name'     => '77777',
      'transaction_id' => 1443,
      'customer_id'    => 552,
      'amount'         => 5500
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,350);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__GetCustomerShipwireOrders extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'customercare__GetCustomerShipwireOrders';

    $base_params = array(
      'customer_id' => 552
    );

    $expected_values_array = array(
    );

    test_success($url,$base_params,$expected_values_array);
  }
}


class Test_customercare__DealerLoginAsCustomer extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'customercare__DealerLoginAsCustomer';

    $base_params = array(
      'customer_id' => 1125,
      'agent_id'    => 33,#123,
      'user_id'     => 456
    );

    $expected_values_array = array(
    );

    test_success($url,$base_params,$expected_values_array);
  }
}


class Test_customercare__SetCustomerCreditCard extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'customercare__SetCustomerCreditCard';

    # curl -i 'https://rgalli2-dev.uvnv.com/pr/customercare/1/ultra/api/customercare__SetCustomerCreditCard' -d 'customer_id=31&account_cc_exp=0313&account_cc_cvv=1234&account_cc_number=5454871000080964' -u dougmeli:Flora

    $base_params = array(
      'customer_id'       => 18666,
      'account_cc_exp'    => '1216',#'0413',
      'account_cc_cvv'    => '110',#'1235',
      'account_cc_number' => '4067129148899676',#'6011580159853550',#'5454871000080964'
      'account_zipcode'   => '92618'
    );

    $json_result = curl_post($url,$base_params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__ShipwireOrderDetails extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'customercare__ShipwireOrderDetails';

    $params = array(
      'order_id' => '1348584820-410929-1'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,10);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__ChargeCreditCard extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'customercare__ChargeCreditCard';

    $base_params = array(
      'customer_id'       => 31,
      'reason'            => 'testing',
      'amount'            => 200
    );

    $json_result = curl_post($url,$base_params,$curl_options,NULL,30);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__ChargePinCard extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ChargePinCard';

    $base_params = array(
      'customer_id' => $argv[2],
      'msisdn'      => $argv[3],
      'store_id'    => $argv[4],
      'user_id'     => $argv[5],
      'destination' => $argv[6],
      'pin'         => $argv[7]
    );

    if ( isset($argv[8]) )
    { $base_params['pin2'] = $argv[8]; }

    if ( isset($argv[9]) )
    { $base_params['pin3'] = $argv[9]; }

    if ( isset($argv[10]) )
    { $base_params['pin4'] = $argv[10]; }

    if ( isset($argv[11]) )
    { $base_params['pin5'] = $argv[11]; }

    if ( isset($argv[12]) )
    { $base_params['pin6'] = $argv[12]; }

    print_r($base_params);

    $json_result = curl_post($url,$base_params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__AddCourtesyMinutes extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__AddCourtesyMinutes';

    $base_params = array(
      'reason'                => 'testing',
      'agent_name'            => '191919',
      'customercare_event_id' => '919191',
      'customer_id'           => 552,
      'amount'                => $argv[2]
    );

    $expected_values_array = array(
    );

    test_success($url,$base_params,$expected_values_array);

  }
}


class Test_customercare__CourtesyMinutes extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__CourtesyMinutes';

    $base_params = array(
      'reason'                => 'testing',
      'agent_name'            => '191919',
      'customercare_event_id' => '919191',
      'customer_id'           => 552,
      'amount'                => $argv[2]
    );

    $expected_values_array = array(
    );

    test_success($url,$base_params,$expected_values_array);

  }
}


class Test_customercare__AddCourtesyCash extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__AddCourtesyCash';

    $base_params = array(
      'reason'                => 'testing',
      'agent_name'            => '77777',
      'customercare_event_id' => '88888',
      'customer_id'           => $argv[3],
      'amount'                => $argv[2]
    );

    $json_result = curl_post($url,$base_params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__CourtesyCash extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__CourtesyCash';

    $base_params = array(
      'reason'                => 'testing',
      'agent_name'            => '77777',
      'customercare_event_id' => '88888',
      'customer_id'           => $argv[3],
      'amount'                => $argv[2]
    );

    $expected_values_array = array(
    );

    test_success($url,$base_params,$expected_values_array,240);
  }
}


class Test_customercare__LoginAsCustomer extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'customercare__LoginAsCustomer';

    $base_params = array(
      'customer_id'   => 31,
      'account_login' => '1347193206'
    );

    $expected_values_array = array(
    );

    test_success($url,$base_params,$expected_values_array);
  }
}


class Test_customercare__ReplaceSIMCard extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ReplaceSIMCard';

    $base_params = array(
      'reason'                => 'test',
      'agent_name'            => '919191',
      'customercare_event_id' => '191919',
      'customer_id'           => $argv[2],
      'old_ICCID'             => $argv[3],
      'new_ICCID'             => $argv[4]
    );

    $json_result = curl_post($url,$base_params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__ChangePhoneNumber extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ChangePhoneNumber';

    $base_params = array(
      'reason'                => $argv[2],
      'agent_name'            => $argv[3],
      'customercare_event_id' => $argv[4],
      'customer_id'           => $argv[5],
      'old_phone_number'      => $argv[6],
      'zipcode'               => $argv[7]
    );

    $json_result = curl_post($url,$base_params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_customercare__CustomerInfo extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__CustomerInfo';

    $base_params = array(
      'phone_number' => $argv[2],
      'mode' => 'OCS_BALANCE'
    );

    $json_result = curl_post($url,$base_params,$curl_options,NULL,240);
    
    echo "\n\n$json_result\n\n";
    
    print_r(json_decode($json_result));
  }
}


class Test_customercare__ReshipSIM extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'customercare__ReshipSIM';

    $base_params = array(
      'reason'                => 'Technical defect',
      'customer_id'           => 594,
      'agent_name'            => 'Billy',
      'customercare_event_id' => '12345'
    );

    $expected_values_array = array(
    );

    test_success($url,$base_params,$expected_values_array);
  }
}


class Test_customercare__ResetPasswordSMS extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ResetPasswordSMS';

    $base_params = array(
      'customer_id' => $argv[2]
    );

    $expected_values_array = array(
    );

    test_success($url,$base_params,$expected_values_array);
  }
}


class Test_customercare__GetMVNEDetails extends AbstractTestStrategy
{
  // set $partner = 'customercare' above

  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__GetMVNEDetails';

    $params = array(
      'ICCID' => empty($argv[2]) ? NULL : $argv[2],
      'MSISDN' => empty($argv[3]) ? NULL : $argv[3],
      'action_uuid' => empty($argv[4]) ? NULL : $argv[4]);

    $json_result = curl_post($url, $params, $curl_options, NULL, 350);
    echo "\n\n$json_result\n\n";
  }
}


class Test_customercare__ValidateSIMOrigin extends AbstractTestStrategy
{
  // set $partner = 'customercare' above

  function test()
  {
    global $curl_options;
    global $base_url;
    global $argv;

    $url = $base_url . 'customercare__ValidateSIMOrigin';

    $params = array(
      'iccid' => empty($argv[2]) ? NULL : $argv[2]
    );

    $json_result = curl_post($url, $params, $curl_options, NULL, 350);
    echo "\n\n$json_result\n\n";
    print_r(json_decode($json_result));
  }
}


# perform test #


$testClass = 'Test_customercare__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


/*

Examples:
php test/api/test_customercare.php ApplyVoiceRecharge 3108666903 50_100

php test/api/test_customercare.php ChargePinCard 31 3108666903 32 33 WALLET 400000004000000048 400000004000000047
*/

?>

<?php


# Usage:
# %> php test/api/test_ePay.php externalpayments__CheckMobileBalance
# %> php test/api/test_ePay.php externalpayments__CancelRealtimeReload


include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');


date_default_timezone_set("America/Los_Angeles");


$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$tests_outcome = array(
 'ok' => 0,
 'ko' => 0
);


$base_url = find_site_url().'/pr/ePay/1/ultra/api/';


abstract class AbstractTestStrategy
{
  abstract function test();
}




class Test_externalpayments__CancelRealtimeReload extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'externalpayments__CancelRealtimeReload';

    $base_params = array(
      'phone_number' => 1000000030,
      'load_amount' => 2900, # TODO: test invalid amount for cos_id
      'request_epoch' => time(),
      'cancel_type' => 'VOID',
      'provider_trans_id' => '123456789'
    );

    $expected_values_array = array(
      'success',
      'errors',
      'warnings',
      'ultra_trans_id',
      'partner_tag',
      'ultra_trans_epoch',
      'request',
      'ultra_payment_trans_id',
      'phone_number',
      'customer_balance',
      'provider_trans_id'
    );

    test_success($url,$base_params,$expected_values_array);

    test_missing_rest($url,$base_params,'cancel_type');
    test_missing_rest($url,$base_params,'phone_number');
    test_missing_rest($url,$base_params,'load_amount');

    test_wrong($url,$base_params,'request_epoch','');
    test_wrong($url,$base_params,'cancel_type','');
    test_wrong($url,$base_params,'load_amount','');
    test_wrong($url,$base_params,'phone_number','');

    $base_params['partner_tag'] = 'test_partner_tag';

    test_expect_returned($url,$base_params,'partner_tag','test_partner_tag');

    test_success($url,$base_params,$expected_values_array);

    $base_params["cancel_type"] = 'REFUND';
    test_success($url,$base_params,$expected_values_array);

    # "min_strlen":3
    test_wrong($url,$base_params,"provider_trans_id",11);

    test_wrong($url,$base_params,"ultra_payment_trans_id",11);

    test_always($url,'succeed',1);
    test_always($url,'fail',1);
  }
}


class Test_externalpayments__CheckMobileBalance extends AbstractTestStrategy
{
  # function externalpayments__CheckMobileBalance($partner_tag, $phone_number, $request_epoch, $store_zipcode, $store_zipcode_extra4, $store_id, $clerk_id, $terminal_id)

  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'externalpayments__CheckMobileBalance';

    $base_params = array(
      'phone_number' => 1000000030,
      'store_zipcode' => 4,
      'terminal_id' => 8,
      'request_epoch' => time(),
      'store_zipcode' => 10128,
    );

    $expected_values_array = array(
      'success',
      'errors',
      'warnings',
      'ultra_trans_id',
      'partner_tag',
      'ultra_trans_epoch',
      'request',
      'phone_number',
      'customer_balance',
      'customer_active',
      'plan_name',
      'service_expires_epoch'
    );

    test_success($url,$base_params,$expected_values_array);

    test_missing_rest($url,$base_params,'phone_number');
    test_missing_rest($url,$base_params,'store_zipcode');
    test_missing_rest($url,$base_params,'terminal_id');

    test_wrong($url,$base_params,'request_epoch','');
    test_wrong($url,$base_params,'store_zipcode','');
    test_wrong($url,$base_params,'phone_number','');
    test_wrong($url,$base_params,'store_zipcode_extra4','54321');
    test_wrong($url,$base_params,'store_zipcode_extra4','321');

    $base_params['partner_tag'] = 'test_partner_tag';

    test_expect_returned($url,$base_params,'partner_tag','test_partner_tag');

    test_always($url,'succeed',1);
    test_always($url,'fail',1);
  }
}


function test_expect_returned($url,$params,$field,$value)
{
  # expects a $field to be returned in the response

  $test_name = 'test_expect_returned ' . $field;

  global $curl_options;

  $result = curl_post($url,$params,$curl_options);

  $result_decoded = json_decode($result);

  $result_array = get_object_vars( $result_decoded );

  echo "\nExpected:$value\n";
  echo "\nReturned:".$result_array["partner_tag"]."\n\n";

  if ( $result_array["partner_tag"] == $value )
  {
    record_test_ok($test_name);
  }
  else
  {
    record_test_ko($test_name);
  }
}


# perform test(s) #


$class_ext = $argv[1];
if ( ! $class_ext ) { $class_ext = $_GET['class']; }

$testClass = 'Test_'.$class_ext;

echo "\n$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


# display tests outcome #


print_r($tests_outcome);


?>

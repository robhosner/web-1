<?php


# Usage:
# php test/api/test_middleware.php PollAccNotification
# php test/api/test_middleware.php PollUltraControl
# php test/api/test_middleware.php PollUltraNotification


include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');


$apache_username = 'dougmeli';
$apache_password = 'Flora';


$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$base_url = find_site_url().'/pr/middleware/1/ultra/api/middleware__';


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_middleware__PollAspSOAPInbound extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'PollAspSOAPInbound';

    $params = array(
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_middleware__PollAccNotification extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'PollAccNotification';

    $params = array(
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_middleware__PollUltraControl extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'PollUltraControl';

    $params = array();

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_middleware__PollUltraNotification extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'PollUltraNotification';

    $params = array();

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


# perform test #


$testClass = 'Test_middleware__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


?>

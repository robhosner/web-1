<?php


# Usage:
# php test/api/test_provisioning.php checkZipCode
# php test/api/test_provisioning.php checkSIMCard $ICCID
# php test/api/test_provisioning.php checkPINCard $PIN
# php test/api/test_provisioning.php checkPINCards $PIN
# php test/api/test_provisioning.php requestProvisionOrangeCustomerAsync
# php test/api/test_provisioning.php requestProvisionNewCustomerAsync
# php test/api/test_provisioning.php requestProvisionNewCustomerAsync_ITW $ICCID
# php test/api/test_provisioning.php verifyProvisionNewCustomerAsync $REQUEST_ID
# php test/api/test_provisioning.php requestProvisionPortedCustomerAsync $numberToPort $portAccountNumber $portAccountPassword
# php test/api/test_provisioning.php verifyProvisionPortedCustomerAsync $REQUEST_ID
# php test/api/test_provisioning.php requestActivateShippedNewCustomerAsync
# php test/api/test_provisioning.php verifyActivateShippedNewCustomerAsync $REQUEST_ID
# php test/api/test_provisioning.php requestResubmitProvisionPortedCustomerAsync $numberToPort $portAccountNumber $portAccountPassword
# php test/api/test_provisioning.php ProvisionBasicOrangeCustomerAsync $actcode
# php test/api/test_provisioning.php ProvisionBasicNewCustomerAsync
# php test/api/test_provisioning.php requestPortFundedCustomerAsync $CUSTOMER_ID $MSISDN $ICCID $portAccountNumber $portAccountPassword
# php test/api/test_provisioning.php CancelRequestedPort $ICCID $numberToPort
# php test/api/test_provisioning.php GetPortingState $MSISDN
# php test/api/test_provisioning.php CheckOrangeActCode $actCode
# php test/api/test_provisioning.php make_zsession_from_customer_id $CUSTOMER_ID
# php test/api/test_provisioning.php test_login_for_zsession $CUSTOMER_ID


# $argv[1] test name
# $argv[2] tester name
# $argv[3] (NONE|CCARD|PINS)


include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('db.php');
include_once('test/api/test_api_include.php');


date_default_timezone_set("America/Los_Angeles");


$apache_username = 'dougmeli';
$apache_password = 'Flora';

$partner = "provisioning";

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$base_url = find_site_url().'/pr/';


$tests_outcome = array(
 'ok' => 0,
 'ko' => 0
);


abstract class AbstractTestStrategy
{
  abstract function test();
}


class Test_provisioning__CheckOrangeActCode extends AbstractTestStrategy
{
    function test()
    {
        global $curl_options;
        global $base_url;
        global $partner;
        global $argv;

        #$partner = 'rainmaker';
        $partner = 'api_public';

        $url = $base_url . $partner . '/1/ultra/api/provisioning__CheckOrangeActCode';

        $actcode = $argv[2];
        $mode    = ( isset($argv[3]) ? $argv[3]: null);

        $params = array(
                'actcode'  => $actcode,
                'mode'     => $mode
        );

        $json_result = curl_post($url,$params,$curl_options,NULL,60);

        echo "\n\n$json_result\n\n";

        print_r(json_decode($json_result));
  }
}


class Test_provisioning__GetPortingState extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $partner;
    global $argv;

    $partner = 'celluphone';

    $url = $base_url . $partner . '/1/ultra/api/provisioning__GetPortingState';

    $params = array(
      'msisdn' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__ProvisionBasicOrangeCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $partner;
    global $argv;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__ProvisionBasicOrangeCustomerAsync';

    $lang = (isset($argv[3])) ? $argv[3] : 'EN';
    $zip  = (isset($argv[4])) ? $argv[4] : '12345';

    $params = array(
      'actcode' => $argv[2],
      'preferred_language' => $lang,
      'customerZip' => $zip
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);
    echo "\n\n$json_result\n\n";
    print_r(json_decode($json_result));
  }
}

class Test_provisioning__ProvisionBasicNewCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; global $partner;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__ProvisionBasicNewCustomerAsync';

#    $test_iccid = get_test_iccid();

    $params = array(
      'ICCID'       => '8901260842107734353',
      'targetPlan'  => 'L24',
      'customerZip' => '12345'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__requestPortFundedCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $partner;
    global $argv;

$partner = 'rainmaker';
#$partner = 'api_public';

    $url = $base_url . $partner . '/1/ultra/api/provisioning__requestPortFundedCustomerAsync';

    $customer_id         = $argv[2];
    $MSISDN              = $argv[3];
    $ICCID               = $argv[4];
    $portAccountNumber   = $argv[5];
    $portAccountPassword = $argv[6];

    $params = array(
      'ICCID'               => $ICCID, #'1901260842102256303',
      'zsession'            => make_zsession_from_customer_id( $customer_id ),
      'numberToPort'        => $MSISDN,
      'portAccountNumber'   => $portAccountNumber,
      'portAccountPassword' => $portAccountPassword,
      'customerZip'         => '12345'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__make_zsession_from_customer_id extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $r = make_zsession_from_customer_id( $argv[2] );

    print_r($r);

    exit;
  }
}


class Test_provisioning__test_login_for_zsession extends AbstractTestStrategy
{
  function test()
  {
    global $argv;
    global $curl_options;

    teldata_change_db();

    $customer = get_customer_from_customer_id( $argv[2] );

    print_r($customer);

    $r = test_login_for_zsession( $curl_options , $customer->LOGIN_NAME , $customer->LOGIN_PASSWORD );

    print_r($r);

    exit;
  }
}


class Test_provisioning__requestActivateShippedNewCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; global $partner;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__requestActivateShippedNewCustomerAsync';

    $test_iccid = get_test_iccid();

    $params = array(
      'ICCID'       => $test_iccid,
      'zsession'    => test_login_for_zsession( $curl_options )
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__verifyActivateShippedNewCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; global $partner;
    global $argv;

    $partner = 'celluphone';

    $url = $base_url . $partner . '/1/ultra/api/provisioning__verifyActivateShippedNewCustomerAsync';

    $params = array(
      'request_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__requestProvisionPortedCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $partner;
    global $argv;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__requestProvisionPortedCustomerAsync';

    $numberToPort        = $argv[2];
    $portAccountNumber   = $argv[3];
    $portAccountPassword = $argv[4];

    if ( ! $numberToPort ) { die("No numberToPort"); }

    teldata_change_db();

    #test_iccid = get_test_iccid();
    $test_iccid = '8901260842116121261';

    if ( ! $test_iccid ) { die("No ICCID to test"); }

    $base_params = array(
      'ICCID'               => $test_iccid,
      'loadPayment'         => 'PINS',
      #'loadPayment'        => 'CCARD',
      'pinsToApply'        => '7264592830',
      'targetPlan'          => 'L24',
      'numberToPort'        => $numberToPort,
#'numberToPort'        => '9172390705', '9172390659', '9174235208', '3472435064',
      'portAccountNumber'   => $portAccountNumber,
      'portAccountPassword' => $portAccountPassword,
      'customerZip'            => '12345',
      'activation_masteragent' => '52',
      'activation_distributor' => 42,
      'activation_agent'       => '32',
      'activation_store'       => 22,
      'activation_userid'      => '12',
      'bolt_ons'               => array() // array('VOICE_500_5')
    );

    $json_result = curl_post($url,$base_params,$curl_options,NULL,120);
    echo "\n\n$json_result\n\n";
return;

    $expected_values_array = array(
      'success',
      'request_id',
      'partner_tag',
      'request',
      'ultra_trans_epoch'
    );

    $cc_params = array(
      #'creditCard' => '5454871000080964',
      'creditCard' => '1234567890123456',
      'creditExpiry' => '0815',
      'creditCVV' => '1234',
      #'creditAutoRecharge' => '1',
      'creditAutoRecharge' => FALSE,
      'creditAmount' => '3333',
      'customerAddressOne' => 'add 1 '.time(),
#      'customerAddressTwo' => 'add 2 '.time(),
      'customerCity' => 'New York',
      'customerState' => 'NY',
#      "customerEMail" => 'abcdefg@yahoo.com'
    );

    test_success($url,$base_params,$expected_values_array,240);
exit;

    $base_params['loadPayment'] = 'CCARD';

    $base_params = array_merge( $base_params , $cc_params );

    #test_success($url,$base_params,$expected_values_array);

    foreach ( $cc_params as $cc_param => $value )
    {
      test_missing_rest($url,$base_params, $cc_param);
    }

    test_wrong($url,$base_params,'creditAmount','1111');


  }
}


class Test_provisioning__verifyProvisionPortedCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $argv;
    global $curl_options;
    global $base_url; global $partner;

    $partner = 'celluphone';

    $url = $base_url . $partner . '/1/ultra/api/provisioning__verifyProvisionPortedCustomerAsync';

    $params = array(
      'request_epoch' => time(),
      'request_id'    => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__requestProvisionOrangeCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; global $partner;
    global $argv;

    $partner = 'celluphone';
    $url = $base_url . $partner . '/1/ultra/api/provisioning__requestProvisionOrangeCustomerAsync';

    $params = array(
      'actcode'            => $argv[2],
      'customerZip'        => $argv[3],
      'activation_channel' => $argv[4],
      'activation_info'    => $argv[5],
      'dealer_code'        => empty($argv[6]) ? NULL : $argv[6]);
    
    $json_result = curl_post($url,$params,$curl_options,NULL,120);
        
    echo "\n\n$json_result\n\n";
        
    print_r(json_decode($json_result));
  }
}


class Test_provisioning__requestProvisionNewCustomerAsync_ITW extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; global $partner;
    global $argv;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__requestProvisionNewCustomerAsync';

    $params = array(
      'ICCID'                  => $argv[2],
      'targetPlan'             => 'L49',
      'loadPayment'            => 'NONE',
      "customerFirstName"      => "ITW Promotional",
      "activation_distributor" => "301",
      "activation_agent"       => "10323",
      "activation_userid"      => "11863",
      "loadPayment"            => "PINS",
      'pinsToApply'            => '7264592833',
      "activation_masteragent" => "69",
      "customerEMail"          => "vtarasov@ultra.me",
      "customerZip"            => "48229",
      "customerLastName"       => "ITW Promotional"
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,120);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__requestProvisionNewCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; global $partner;
    global $argv;

    teldata_change_db();

    $url = $base_url . $partner . '/1/ultra/api/provisioning__requestProvisionNewCustomerAsync';

    if (empty($argv[2]))
      $test_iccid = '8901260842116121261';
    else
      $test_iccid = $argv[2];

    if ( ! $test_iccid ) { die("No ICCID to test"); }

    $base_params = array(
      'ICCID' =>
      $test_iccid,
      'loadPayment' => 'NONE',
#      'loadPayment' => 'CCARD',
#      'loadPayment' => 'PINS',
#      'loadPayment' => $argv[3],
      'targetPlan' => 'L24',
      #'pinsToApply' => '12345678901234567890,98765432101234567890,10000000000000000001',
      #'pinsToApply' => '12345678901234567890,98765432101234567890', # TESTED OK
      #'pinsToApply' => '98765432101234567890',
      'pinsToApply' => '',
#'400000004000000639,400000004000000638',
#'400000004000000642,400000004000000641,400000004000000640',
#'400000004000000643',
#'400000004000000644',
#'400000004000000645',
#'400000004000000646',
#'400000004000000647',
#'400000004000000648',
#'400000004000000649',
      'customerZip' => '12345',
      'activation_masteragent' => '55',
      'activation_distributor' => '44',
      'activation_agent' => '33',
      'activation_store' => '22',
      'activation_userid' => '989898',
      #'testing' => 1
      'bolt_ons' => array() // array('VOICE_500_5')
    );

    $cc_params = array(
      'creditCard' => '5454871000080964',
      #'creditCard' => '1234567890123456',
      'creditExpiry' => '0815',
      'creditCVV' => '1234',
      #'creditAutoRecharge' => '1',
      'creditAutoRecharge' => FALSE,
      'creditAmount' => '5555',
      'customerAddressOne' => 'add 1 '.time(),
#      'customerAddressTwo' => 'add 2 '.time(),
      'customerCity' => 'New York',
      'customerState' => 'NY',
#      "customerEMail" => 'abcdefg@yahoo.com'
    );

    $expected_values_array = array(
      'success',
      'request_id',
      'partner_tag',
      'request',
      'ultra_trans_epoch'
    );

    if ( $base_params['loadPayment'] == 'CCARD' )
    {
      $base_params = array_merge( $base_params , $cc_params );
    }

    test_success($url,$base_params,$expected_values_array);
exit;

    foreach ( $base_params as $base_param => $value )
    {
      test_missing_rest($url,$base_params, $base_param);
    }

    $base_params['loadPayment'] = 'CCARD';

    $base_params = array_merge( $base_params , $cc_params );

    #test_success($url,$base_params,$expected_values_array);

    foreach ( $cc_params as $cc_param => $value )
    {
      test_missing_rest($url,$base_params, $cc_param);
    }

    test_wrong($url,$base_params,'creditAmount','1111');

# TODO: test_missing_rest for $cc_params

  }
}


class Test_provisioning__verifyProvisionNewCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; global $partner;
    global $argv;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__verifyProvisionNewCustomerAsync';

    $params = array(
      'request_epoch' => time(),
      'request_id'    => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__checkSIMCard extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; global $partner;
    global $argv;

    $url = $base_url . 'api_public/1/ultra/api/provisioning__checkSIMCard';

    $params = array(
      'request_epoch' => time(),
      'ICCID'         => $argv[2]
      #'ICCID'         => 8000000000000000011,
      #'ICCID'         => '8901260842102253880'
    );

    echo "\n\n$url\n\n";

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__checkZipCode extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; global $partner;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__checkZipCode';

    $params = array(
      'request_epoch' => time(),
      'zip_code'      => 12345
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__requestResubmitProvisionPortedCustomerAsync extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $partner;
    global $argv;

    $partner = 'celluphone';

    $url = $base_url . $partner . '/1/ultra/api/provisioning__requestResubmitProvisionPortedCustomerAsync';

    #$numberToPort = '2124704574';
    $numberToPort        = $argv[2];
    $portAccountNumber   = $argv[3];
    $portAccountPassword = $argv[4];

    if ( ! $numberToPort ) { die("No numberToPort"); }

    #$test_iccid = get_test_iccid();

    $test_iccid = '8901260842102260396';

    if ( ! $test_iccid ) { die("No ICCID to test"); }

    $params = array(
      'ICCID'                  => $test_iccid,
      'numberToPort'           => $numberToPort,
      'portAccountNumber'      => $portAccountNumber,
      'portAccountPassword'    => $portAccountPassword,
      'portAccountZipcode'     => '12345',
      'activation_masteragent' => '55',
      'activation_distributor' => '44',
      'activation_agent'       => '33',
      'activation_store'       => '22',
      'activation_userid'      => '11'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__CancelRequestedPort extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url; 
    global $partner;
    global $argv;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__CancelRequestedPort';

    $params = array(
      'ICCID'        => $argv[2],
      'numberToPort' => $argv[3]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__checkPINCards extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $partner;
    global $argv;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__checkPINCards';

    $params = array(
      'pin' => $argv[2]
    );

    foreach(range(3,7) as $n)
    {
      if ( isset($argv[$n]) )
      { $params['pin'.($n-1)] = $argv[$n]; }
    }

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_provisioning__checkPINCard extends AbstractTestStrategy
{
  function test()
  {
    global $curl_options;
    global $base_url;
    global $partner;
    global $argv;

    $url = $base_url . $partner . '/1/ultra/api/provisioning__checkPINCard';

    $params = array(
      'pin' => $argv[2]
#'188646081071819442'
#'123456789012345678'
#'890126084210225555'
#'800000000000000003'
#'300000003000000600'
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


function test_login_for_zsession( $curl_options , $login , $password )
{
  $url = find_site_url() . '/pr/portal/1/ultra/api/portal__Login';

  echo "url = $url\n";

  $params = array(
    'account_login'    => $login,
    'account_password' => $password
  );

  $json_result = curl_post($url,$params,$curl_options,NULL,240);

  $json_decoded = json_decode($json_result);

  dlog('',"zsession = ".$json_decoded->zsession);

  return $json_decoded->zsession;
}


# perform test #


$testClass = 'Test_provisioning__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


# display tests outcome #


print_r($tests_outcome);


/*

php test/api/test_provisioning.php requestProvisionOrangeCustomerAsync 66666666665 12345 channel info

 */

?>

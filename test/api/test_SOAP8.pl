#!/usr/bin/perl

use warnings;
use strict;

use Data::Dumper;
use Crypt::SSLeay;
use LWP::UserAgent 6;
use XML::Compile::SOAP 2.38;
use XML::Compile::SOAP11;
use XML::Compile::WSDL11;
use XML::Compile::SOAP::Trace;
use XML::Compile::Transport::SOAPHTTP;
use Log::Report mode => 'DEBUG';

my $ssl_cert_file = '/etc/httpd/certs/acc/ULTRA.pem';

print "ssl_cert_file = $ssl_cert_file\n";

my $wsdl_file = shift;

print "wsdl_file = $wsdl_file\n";

my $end_point = 'https://acc.mvne2.t-mobile.com/acc/inbound.aspx';

print "end_point = $end_point\n";

$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = "0";
$ENV{HTTPS_DEBUG}                  = 1;
$ENV{HTTPS_CERT_FILE}              = $ssl_cert_file;
$ENV{HTTPS_KEY_FILE}               = $ssl_cert_file;

my $ssl_opts = {
    SSL_cert_file => $ssl_cert_file,
    SSL_key_file  => $ssl_cert_file,
    SSL_passwd_cb => sub { return "ultra1"; },
    SSL_version   => 'SSLv3'
};

# Load and compile all the WSDL files.
my $wsdl = XML::Compile::WSDL11->new( $wsdl_file );

my $command1 = 'GetNetworkDetails';
my $command2 = 'GetNGPList';

my $ua1 = LWP::UserAgent->new;
my $ua2 = LWP::UserAgent->new;

$ua1->default_headers->header( 'SOAPAction' => 'http://www.sigvalue.com/acc/'.$command1 );
$ua2->default_headers->header( 'SOAPAction' => 'http://www.sigvalue.com/acc/'.$command2 );

$ua1->ssl_opts( %$ssl_opts );
$ua2->ssl_opts( %$ssl_opts );

my $trans1 = XML::Compile::Transport::SOAPHTTP->new( user_agent => $ua1, address => $end_point );
my $trans2 = XML::Compile::Transport::SOAPHTTP->new( user_agent => $ua2, address => $end_point );

=head4
my $command = 'GetNGPList';

my $params =
{
  'zipCode' => '77583',
  'UserData' => {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2013-11-18T11:33:14',
  }
};

my $command = 'QuerySubscriber';

my $params =
{
  'UserData' => {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2013-11-18T11:33:14',
  },
  'MSISDN' => '1001001000'
};

=cut

my $op1 = $wsdl->operation( $command1 );
$wsdl->compileCall( $op1 , transport => $trans1 );

my $op2 = $wsdl->operation( $command2 );
$wsdl->compileCall( $op2 , transport => $trans2 );

my $params1 =
{
  'UserData' => {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2013-12-04T12:00:00',
  },
  'MSISDN' => '4058878653',
  'ICCID'  => '8901260842107741804',
};

my $params2 =
{
  'zipCode' => '77583',
  'UserData' => {
    'channelId' => 'ULTRA',
    'senderId'  => 'MVNEACC',
    'timeStamp' => '2013-11-18T11:33:14',
  }
};

my ($answer, $trace);

eval
{
  ($answer,$trace) = $wsdl->call( $command1 => { parameters => $params1 } );
};

if ( $@ )
{
  die $@;
}
elsif ( ! $trace )
{
  die("No trace available after $command1 SOAP call");
}
else
{
  print Dumper( $trace->request() );
  print "\n\n\n\n";

  print "\nSOAP request:\n";
  print $trace->request()->content();

  print "\nSOAP response:\n";
  print Dumper($trace->response());
}

print "\n";
print "=======================================================================================\n";
print "\n";


eval
{
  ($answer,$trace) = $wsdl->call( $command2 => { parameters => $params2 } );
};

if ( $@ )
{
  die $@;
}
elsif ( ! $trace )
{
  die("No trace available after $command2 SOAP call");
}
else
{
  print Dumper( $trace->request() );
  print "\n\n\n\n";

  print "\nSOAP request:\n";
  print $trace->request()->content();

  print "\nSOAP response:\n";
  print Dumper($trace->response());
}

print "\n";

__END__


<?php

# Usage:
# %> php test/api/test_tcetra.php tcetra__CheckMobileBalance

include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');

date_default_timezone_set("America/Los_Angeles");

$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$tests_outcome = array(
 'ok' => 0,
 'ko' => 0
);

$partner = 'tcetra';
$base_url = find_site_url().'/pr/'.$partner.'/1/ultra/api/';

abstract class AbstractTestStrategy
{
  abstract function test();
}

class Test_tcetra__CheckMobileBalance extends AbstractTestStrategy
{
  # function externalpayments__CheckMobileBalance($partner_tag, $phone_number, $request_epoch, $store_zipcode, $store_zipcode_extra4, $store_id, $clerk_id, $terminal_id)

  function test()
  {
    global $curl_options;
    global $base_url;

    $url = $base_url . 'externalpayments__CheckMobileBalance';

    $phoneNumber = (isset($argv[2])) ? $argv[2] : 1000000030;
    $terminalId = (isset($argv[4])) ? $argv[4] : 8;
    $storeZip = (isset($argv[3])) ? $argv[3] : 10128;

    $params = array(
      'phone_number' => $phoneNumber,
      'terminal_id' => $terminalId,
      'request_epoch' => time(),
      'store_zipcode' => $storeZip
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,60);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));	
  }
}


function test_expect_returned($url,$params,$field,$value)
{
  # expects a $field to be returned in the response

  $test_name = 'test_expect_returned ' . $field;

  global $curl_options;

  $result = curl_post($url,$params,$curl_options);

  $result_decoded = json_decode($result);

  $result_array = get_object_vars( $result_decoded );

  echo "\nExpected:$value\n";
  echo "\nReturned:".$result_array["partner_tag"]."\n\n";

  if ( $result_array["partner_tag"] == $value )
  {
    record_test_ok($test_name);
  }
  else
  {
    record_test_ko($test_name);
  }
}


# perform test(s) #

$class_ext = $argv[1];
if ( ! $class_ext ) { $class_ext = $_GET['class']; }

$testClass = 'Test_'.$partner.'__'.$class_ext;

echo "\n$testClass\n\n";

$testObject = new $testClass();

$testObject->test();

?>

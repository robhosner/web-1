<?php

# Usage:

# php test/api/test_dealerportal.php ValidateCelluphoneSession     $CELLUPHONE_SESSION_ID
# php test/api/test_dealerportal.php SearchActivationHistoryDealer $TOKEN $FROM $TO
# php test/api/test_dealerportal.php ReportActivationActivity      $TOKEN $FROM $TO


include_once((getenv('HTT_CONFIGROOT') . '/e.php'));
include_once('web.php');
include_once('test/api/test_api_include.php');


$apache_username = 'dougmeli';
$apache_password = 'Flora';

$curl_options = array(
  CURLOPT_USERPWD  => "$apache_username:$apache_password",
  CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
);


$base_url = find_site_url().'/pr/dealerportal/2/ultra/api/dealerportal__';


abstract class AbstractTestStrategy
{
  abstract function test( $curl_options , $base_url , $argv );
}


class Test_dealerportal__ReportActivationActivity extends AbstractTestStrategy
{
  function test( $curl_options , $base_url , $argv )
  {
    $url = $base_url . 'ReportActivationActivity';

    $params = array(
      'security_token' => $argv[2],
      'date_from'      => $argv[3],
      'date_to'        => $argv[4]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_dealerportal__SearchActivationHistoryDealer extends AbstractTestStrategy
{
  function test( $curl_options , $base_url , $argv )
  {
    $url = $base_url . 'SearchActivationHistoryDealer';

    $params = array(
      'security_token' => $argv[2],
      'epoch_from'     => $argv[3],
      'epoch_to'       => $argv[4]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


class Test_dealerportal__ValidateCelluphoneSession extends AbstractTestStrategy
{
  function test( $curl_options , $base_url , $argv )
  {
    $url = $base_url . 'ValidateCelluphoneSession';

    $params = array(
      'celluphone_session_id' => $argv[2]
    );

    $json_result = curl_post($url,$params,$curl_options,NULL,240);

    echo "\n\n$json_result\n\n";

    print_r(json_decode($json_result));
  }
}


# perform test #


$testClass = 'Test_dealerportal__'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test( $curl_options , $base_url , $argv );


/*
Examples:

php test/api/test_dealerportal.php SearchActivationHistoryDealer ONEdoesNOTsimplyOVERRIDE 1390220605 1390220605
php test/api/test_dealerportal.php ReportActivationActivity ONEdoesNOTsimplyOVERRIDE 11-11-2013 11-11-2013
*/

?>

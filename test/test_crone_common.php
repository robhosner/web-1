<?php


include_once('lib/crone-common.php');
include_once('test/AbstractTestStrategy.php');


# php test/test_crone_common.php port_in_requested_customer_crone
# php test/test_crone_common.php retrieve_port_in_denied_list
# php test/test_crone_common.php should_skip_query_portin
# php test/test_crone_common.php adjust_assurance_value_by_customer_state $MSISDN
# php test/test_crone_common.php redo_aborted_transitions
# php test/test_crone_common.php perform_mw_queues_housekeeping


teldata_change_db(); // connect to the DB


class TestPortInData
{
  public $PORT_STATUS        = 'TEST';
  public $customer_id        = '123456';
  public $plan_state         = 'Neutral';
  public $CURRENT_ICCID_FULL = '123456';
  public $MSISDN             = '1002003456';
}

class Test_redo_aborted_transitions extends AbstractTestStrategy
{
  function test()
  {
    redo_aborted_transitions();
  }
}

class Test_perform_mw_queues_housekeeping extends AbstractTestStrategy
{
  function test()
  {
    perform_mw_queues_housekeeping();
  }
}

class Test_adjust_assurance_value_by_customer_state extends AbstractTestStrategy
{
  function test()
  {
    global $argv;

    $msisdn = $argv[2];

    $test_assurance_values = array('Deactivate','Reactivate','Suspend','Activate','Resume');

    foreach( $test_assurance_values as $assurance_value )
    {
      $new_assurance_value = adjust_assurance_value_by_customer_state( $assurance_value , $msisdn );

      echo "$msisdn ; $assurance_value => $new_assurance_value\n";
    }
  }
}

class Test_retrieve_port_in_denied_list extends AbstractTestStrategy
{
  function test()
  {
    $test_inflight = new Result;

    $inflight = array(
      '[2001]' => $test_inflight
    );

    $inflight['[2001]']->fail();

    $denied = retrieve_port_in_denied_list( $inflight );

    print_r($denied); # should be empty

    echo "\n";

    $inflight['[2001]']->succeed();

    $denied = retrieve_port_in_denied_list( $inflight );

    print_r($denied); # should be empty

    echo "\n";

    $test_inflight->data_array['portin_denied'] = TRUE;

    $denied = retrieve_port_in_denied_list( $inflight );

    print_r($denied); # should not be empty

    echo "\n";
  }
}

class Test_should_skip_query_portin extends AbstractTestStrategy
{
  function test()
  {
    $port_in_data = new TestPortInData;

    $r = should_skip_query_portin( array() , 'TEST' , $port_in_data );

    print_r( $r );
  }
}

class Test_port_in_requested_customer_crone extends AbstractTestStrategy
{
  function test()
  {
    $mode = 'test';

    $finder = port_in_requested_customer_finder();

    $port_in_data = mssql_fetch_all_objects(logged_mssql_query($finder));

    $test_port_in_data = $port_in_data[0];

    $test_port_status_list = test_port_status_list();

    foreach($test_port_status_list as $test_port_status)
    {
      # list customer ids for incomplete (aka. inflight) ports
      $inflight = array();

      # force this test status
      $test_port_in_data->PORT_STATUS = $test_port_status;

      # fake MSISDN
      $test_port_in_data->current_mobile_number = '1002003456';
      $test_port_in_data->MSISDN                = '1002003456';

      dlog('',"Customer id = ".$test_port_in_data->customer_id);
      dlog('',"MSISDN      = ".$test_port_in_data->current_mobile_number);
      dlog('',"PORT_STATUS = ".$test_port_in_data->PORT_STATUS);

      $inflight = port_in_requested_customer_crone( $inflight , $test_port_in_data , $mode );

      print_r($inflight);
      echo "\n\n\n";
    }
  }
}

function test_port_status_list()
{
  return array(
    'COMPLETE',
    'CANCELED',
    'CANCELLED',
    'FAILED TO PORT',
    'IN PROGRESS',
    'DELAY',
    'CONCURRENCE',
    'CONFIRMED',
    'RESOLUTION REQUIRED',
    'initialized',
    'submitted',
    'thinking about it',
    '',
  );
}


# perform test(s) #


$class_ext = $argv[1];

$testClass = 'Test_'.$class_ext;

echo "\n$testClass\n\n";

$testObject = new $testClass();

$testObject->test();


?>

<?php

function exitOnFailureResult( \Result $result )
{
  if ( $result->is_failure() )
  {
    echo 'ERRORS' . PHP_EOL;
    print_r($result->get_errors());
    exit;
  }
}

function randomMSISDN()
{
  $randomMSISDN = '';

  for ( $i = 0 ; $i < 10 ; $i++ )
    $randomMSISDN .= mt_rand( 0 , 9 );

  return $randomMSISDN;
}

function randomICCID()
{ 
  $randomICCID = '';
  
  for ( $i = 0 ; $i < 19 ; $i++ )
    $randomICCID .= mt_rand( 0 , 9 );
  
  return $randomICCID;
}   

function randomIMSI()
{ 
  $randomIMSI = '';

  for ( $i = 0 ; $i < 16 ; $i++ )
    $randomIMSI .= mt_rand( 0 , 9 );

  return $randomIMSI;
}   


<?php


date_default_timezone_set("America/Los_Angeles");


include_once('db.php');
include_once('lib/payments/functions.php');


teldata_change_db(); // connect to the DB


# Usage:

# php test/payments/test_functions.php addBoltOnImmediate			$CUSTOMER_ID $BALANCE
# php test/payments/test_functions.php addBoltOnsRecurring			$CUSTOMER_ID $BALANCE
# php test/payments/test_functions.php validateImmediateBoltOn    $cos_id
# php test/payments/test_functions.php validateRecurringBoltOn    $cos_id
# php test/payments/test_functions.php process_recurring_bolt_ons $CUSTOMER_ID
# php test/payments/test_functions.php compute_subscriber_owed_amount_current_plan $CUSTOMER_ID
# php test/payments/test_functions.php func_add_funds_by_tokenized_cc $CUSTOMER_ID 
# php test/payments/test_functions.php func_add_balance $CUSTOMER_ID $SUBPRODUCT_ID	TEST OK
# php test/payments/test_functions.php func_add_ild_minutes $CUSTOMER_ID $AMOUNT        TEST OK
# php test/payments/test_functions.php func_add_stored_value                 		TEST OK
# php test/payments/test_functions.php func_sweep_stored_value               		TEST OK
# php test/payments/test_functions.php func_courtesy_add_ild_minutes         		TEST OK
# php test/payments/test_functions.php func_courtesy_add_balance             		TEST OK
# php test/payments/test_functions.php func_validate_pin_cards $PIN_1 $PIN_2 ... $PIN_N TEST OK
# php test/payments/test_functions.php func_apply_pin_cards $CUSTOMER_ID $DESTINATION $PIN_1 $PIN_2 ... $PIN_N
# php test/payments/test_functions.php func_add_balance_by_credit_card $CUSTOMER_ID $AMOUNT_DOLLARS TEST OK
# php test/payments/test_functions.php func_add_stored_value_by_credit_card  		TEST OK
# php test/payments/test_functions.php func_update_stored_credit_card        		TEST OK
# php test/payments/test_functions.php func_spend_from_balance               		TEST OK
# php test/payments/test_functions.php func_spend_from_balance_explicit         TEST OK
# php test/payments/test_functions.php func_sweep_stored_value_explicit      		TEST OK
# php test/payments/test_functions.php parse_webcc_rejection_errors          		TEST OK
# php test/payments/test_functions.php func_apply_balance_to_stored_value		TEST OK
# php test/payments/test_functions.php func_courtesy_add_stored_value			TEST OK
# php test/payments/test_functions.php func_sweep_balance_to_stored_value			TEST OK
# php test/payments/test_functions.php get_sales_tax $CUSTOMER_ID $AMOUNT
# php test/payments/test_functions.php calculate_taxes_fees $CUSTOMER_ID $AMOUNT
# php test/payments/test_functions.php func_calculate_taxes_fees $ZIP $CUSTOMER_ID $ACCOUNT $AMOUNT_IN_CENTS $TRANSACTION_TYPE
# php test/payments/test_functions.php func_add_balance_by_tokenized_cc      $CUSTOMER_ID $RECURRING_FLAG
# php test/payments/test_functions.php func_add_stored_value_by_tokenized_cc $CUSTOMER_ID $RECURRING_FLAG
# php test/payments/test_functions.php get_recovery_fee $AMOUNT
# php test/payments/test_functions.php verifyAmountDestination $CUSTOMER_ID $DESTINATION $AMOUNT
# php test/payments/test_functions.php verifyProviderSku $PROVIDER $SKU
# php test/payments/test_functions.php verifyProviderSkuAmount $PROVIDER $SKU $AMOUNT
# php test/payments/test_functions.php funcBogoEnrollment $CUSTOMER_ID $ICCID $COS_ID
# php test/payments/test_functions.php funcMultiMonthEnrollment $CUSTOMER_ID $ICCID
# php test/payments/test_functions.php validateFactoredCommission [$CUSTOMER_ID] [$ICCID]
# php test/payments/test_functions.php refundWebPosCustomer $CUSTOMER_ID
# php test/payments/test_functions.php online_auto_enroll $CUSTOMER_ID $iccid

# argument: $CUSTOMER_ID


abstract class AbstractTestStrategy
{
  abstract function test($test_params);
}


class Test_func_spend_from_balance
{
  function test($test_params)
  {
    $result = func_spend_from_balance(
      array(
        'customer_id' => '31',
        'amount'    => 10.25,
        'reason'    => 'test_reason',
        'detail'    => 'd',
        'reference' => 'ref test',
        'source'    => 'src test',
        'reference_source' => 'rs'
      )
    );

    test_output($result);
  }
}

class Test_func_spend_from_balance_explicit extends AbstractTestStrategy
{
  function test($test_params) 
  {
    $customer_id = 18670;
    $amount = 10.25;
    $reason = 'test_reason';
    $detail = 'd';
    $reference = 'ref test';
    $source = 'src test';
    $reference_source = 'rs';

    $result = func_spend_from_balance_explicit($customer_id);
    print_r($result);

    $result = func_spend_from_balance_explicit($customer_id, $amount);
    print_r($result);

    $result = func_spend_from_balance_explicit($customer_id, $amount, $reason);
    print_r($result);

    $result = func_spend_from_balance_explicit($customer_id, $amount, $reason, $detail);
    print_r($result);

    $result = func_spend_from_balance_explicit($customer_id, $amount, $reason, $detail, $reference);
    print_r($result);

    $result = func_spend_from_balance_explicit($customer_id, $amount, $reason, $detail, $reference, $source);
    print_r($result);

    $result = func_spend_from_balance_explicit($customer_id, $amount, $reason, $detail, $reference, $source, $reference_source);
    print_r($result);
  }
}


class Test_compute_subscriber_owed_amount_current_plan
{
  function test($test_params)
  {
    global $argv;

    $result = compute_subscriber_owed_amount_current_plan(
      get_customer_from_customer_id( $argv[2] )
    );

    print_r($result);

    exit;
  }
}


class Test_addBoltOnImmediate
{
  function test($test_params)
  {
    global $argv;

    $result = \Ultra\Lib\BoltOn\addBoltOnImmediate( $argv[2] , $argv[3] );

    print_r($result);

    exit;
  }
}


class Test_addBoltOnsRecurring
{
  function test($test_params)
  {
    global $argv;

    $result = \Ultra\Lib\BoltOn\addBoltOnsRecurring( $argv[2] , $argv[3] );

    print_r($result);

    exit;
  }
}


class Test_validateImmediateBoltOn
{
  function test($test_params)
  {
    global $argv;

    // test array
    $bolt_ons = array(
      'IDDCA_6.25_5',
      'DATA_500_5'
    );

    $result = \Ultra\Lib\BoltOn\validateImmediateBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    // test string
    $bolt_ons = 'IDDCA_6.25_5';

    $result = \Ultra\Lib\BoltOn\validateImmediateBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    // test voice bolt on
    $bolt_ons = 'VOICE_500_5';
    $result = \Ultra\Lib\BoltOn\validateImmediateBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    // invalid bolt on id: should fail
    $bolt_ons = array(
      'IDDCA_6_5',
      'DATA_500_5'
    );

    $result = \Ultra\Lib\BoltOn\validateImmediateBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    echo PHP_EOL;

    // empty
    $bolt_ons = array('');
    $result = \Ultra\Lib\BoltOn\validateImmediateBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    $bolt_ons = array(NULL);
    $result = \Ultra\Lib\BoltOn\validateImmediateBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    $bolto_ons = array("", 'IDDCA_6.25_5');
    $result = \Ultra\Lib\BoltOn\validateImmediateBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;
  }
}

class Test_validateRecurringBoltOn
{
  function test($test_params)
  {
    global $argv;

    // test array

    $bolt_ons = array(
      'IDDCA_6.25_5',
      'DATA_500_5'
    );

    $result = \Ultra\Lib\BoltOn\validateRecurringBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    // test string

    $bolt_ons = 'IDDCA_6.25_5';

    // should fail

    $bolt_ons = array(
      'IDDCA_6_5',
      'DATA_500_5'
    );

    $result = \Ultra\Lib\BoltOn\validateRecurringBoltOn($argv[2], $bolt_ons);
    if ( ! $result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    echo PHP_EOL;

    // empty

    $bolt_ons = array('');
    $result = \Ultra\Lib\BoltOn\validateRecurringBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    $bolt_ons = array(NULL);
    $result = \Ultra\Lib\BoltOn\validateRecurringBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;

    $bolto_ons = array('IDDCA_6_5', '');
    $result = \Ultra\Lib\BoltOn\validateRecurringBoltOn($argv[2], $bolt_ons);
    if ($result) echo 'SUCCESS' . PHP_EOL;
    else         echo 'FAILURE' . PHP_EOL;
  }
}


class Test_process_recurring_bolt_ons
{
  function test($test_params)
  {
    global $argv;

    $result = process_recurring_bolt_ons( array( 'customer_id' => $argv[2] ) );

    print_r($result);

    exit;
  }
}


class Test_func_add_funds_by_tokenized_cc
{
  function test($test_params)
  {
    global $argv;

    $params = array(
      'customer'           => get_customer_from_customer_id( $argv[2] ),
      'charge_amount'      => 10,
      'description'        => 'test',
      'session'            => time(),
      'reason'             => 'testing',
      'fund_destination'   => 'stored_value',
      'detail'             => 'test func_add_funds_by_tokenized_cc',
      'transaction_type'   => 'RECHARGE',
      'include_taxes_fees' => 1
    );

    if (isset($argv[3]))
    {
      $params['transaction_type']   = $argv[3];
      $params['include_taxes_fees'] = 1;
    }

    $result = func_add_funds_by_tokenized_cc( $params );

    print_r($result);

    exit;
  }
}


class Test_get_sales_tax
{
  function test($test_params)
  {
    global $argv;

    $result = get_sales_tax( $argv[3] , $argv[2] );

    print_r($result);

    exit;
  }
}


class Test_get_recovery_fee
{
  function test($test_params)
  {
    global $argv;

    list( $recovery_fee , $recovery_fee_rule , $recovery_fee_basis ) = get_recovery_fee( $argv[2] );

    echo " $recovery_fee , $recovery_fee_rule , $recovery_fee_basis \n";

    exit;
  }
}


class Test_calculate_taxes_fees
{
  function test($test_params)
  {
    global $argv;

    $result = calculate_taxes_fees( $argv[3] , $argv[2] );

    print_r($result);

    exit;
  }
}


class Test_func_calculate_taxes_fees
{
  function test($test_params)
  {
    global $argv;

    $result = func_calculate_taxes_fees( $argv[2], $argv[3], $argv[4], $argv[5], $argv[6] );

    print_r($result);

    exit;
  }
}


class Test_parse_webcc_rejection_errors
{
  function test($test_params)
  {
    $rejection_errors = array(
'RESULT: CCV - No Credit Card Specified',
'RESULT: SGS-000001: D:DeclinedNNNM:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedNNNN:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedNYZM:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedNYZN:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedXXRN:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedXXSM:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedXXUM:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedXXUN:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedXXUP:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedXXUX:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedYNAN:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedYYYM:;AVS=;CVV2=',
'RESULT: SGS-000001: D:DeclinedYYYN:;AVS=;CVV2=',
'RESULT: SGS-000002: R:Referral (call voice cente',
'RESULT: SGS-002000: D:DeclinedNNNM:;AVS=;CVV2=',
'RESULT: SGS-002000: D:DeclinedNYZM:;AVS=;CVV2=',
'RESULT: SGS-002000: D:DeclinedXXUM:;AVS=;CVV2=',
'RESULT: SGS-002000: D:DeclinedYNAM:;AVS=;CVV2=',
'RESULT: SGS-002000: D:DeclinedYYYM:;AVS=;CVV2=',
'RESULT: SGS-002304: Credit card is expired.;AVS=',
'RESULT: SGS-005005: Duplicate transaction.;AVS=;',
'RESULT: SGS-020005: Error (Merchant config file ', # SGS-020005: Error (Merchant config file is missing, empty or cannot be read).
'RESULT: SGS-020008: Invalid Merchant ID.;AVS=;CV',
'SGS-020006: Please contact merchant services.',
'SGS-020003: Invalid XML',
'SGS-020003: Invalid XML - invalid tag installments',
'SGS-002200: There was a gateway configuration error',
'SGS-002300: No credit card expiration year provided',
'SGS-002301: Charge total must be the sum of subtotal, tax, vallue added tax, and shipping.',
'SGS-002303: Invalid credit card number.',
'SGS-002000: The server encountered an error: Unsupported credit card type.',
'SGS-002000: The server encountered an error: General Processor Error.',
'SGS-005999: There was an unknown error in the database',
'SGS-005003: The order already exists in the database.',
'SGS-005002: The merchant is not setup to support the requested service.',
'SGS-005002: No approved authorization found',
'SGS-005002: No transaction to void found',
'SGS-005000: The server encountered a database error.',
'SGS-011202: Not authorized to run a Credit Transaction.',
'SGS-020011: Creditcard or check information is required.',
'SGS-020013: Unexpected AuthService Response.'
    );

    foreach($rejection_errors as $n => $rejection_error)
    {
      $parsed_result = parse_webcc_rejection_errors($rejection_error);
      echo "\n$rejection_error\n".$parsed_result['rejection_errors'][0]."\n";
      echo ( $parsed_result['transient'] ) ? '' : 'not ' ;
      echo "transient\n";
    }

  }
}


class Test_func_add_balance
{
  function test($test_params)
  {
    global $argv;

    $subproduct_id = ( isset($argv[3]) ? $argv[3] : FALSE );

    $result = func_add_balance(
      array(
        'customer'    => $test_params['customer'],
        'amount'      => 10.25,
        'reason'      => 'test',
        'source'      => 'EPAY', # or WEBCC
        'reference'   => 'r'.time(),
        'pay_quicker' => $subproduct_id
      )
    );

    test_output($result);
  }
}


class Test_func_add_ild_minutes
{
  function test($test_params)
  {
    global $argv;

    $customer = get_customer_from_customer_id( $argv[2] );

    $charge_amount = $argv[3];

    $result = func_add_ild_minutes(
      array(
        'customer'  => $customer,
        'amount'    => ( $charge_amount / 100 ),
        'reason'    => 'portal__ApplyCallCredit',
        'source'    => 'PORTAL',
        'reference' => 'test_'.time(),
        'balance_change' => ( $charge_amount / 100 ),
        'bonus'     => .25
      )
    );

    print_r( $result );

    exit;
  }
}


class Test_func_add_stored_value
{
  function test($test_params)
  {
    $result = func_add_stored_value(
      array(
        'customer'  => $test_params['customer'],
        'amount'    => 14.15,
        'reason'    => 'test_reason3',
        'source'    => 'EPAY', # or WEBCC
        'reference' => 'test_reference3'
      )
    );

    test_output($result);
  }
}


class Test_func_courtesy_add_stored_value
{
  function test($test_params)
  {
    $result = func_courtesy_add_stored_value(
      array(
        'amount'           => 20,
        'reference'        => 'reference',
        'reason'           => 'reason',
        'customer'         => $test_params['customer'],
        'detail'           => 'test_func_courtesy_add_stored_value',
        'terminal_id'      => 22
      )
    );

    print_r($result);
  }
}


class Test_func_apply_balance_to_stored_value
{
  function test($test_params)
  {
    $result = func_apply_balance_to_stored_value(
      array(
        'amount'           => 11,
        'reason'           => 'reason',
        'reference'        => 'RECHARGE',
        'source'           => 'SPEND',
        'detail'           => 'reason',
        'customer'         => $test_params['customer'],
        'reference_source' => 'PHPAPI'
      )
    );

    print_r($result);
  }
}


class Test_func_sweep_stored_value_explicit
{
  function test($test_params)
  {
    $result = func_sweep_stored_value_explicit(31,2,3);

    test_output($result);
  }
}


class Test_func_sweep_stored_value
{
  function test($test_params)
  {
    $result = func_sweep_stored_value(
      array(
        'customer'  => $test_params['customer'],
        'source'    => 'test source',
        'reference' => 'test'
      )
    );

    test_output($result);
  }
}

class Test_func_sweep_balance_to_stored_value
{
  function test($test_params)
  {
    $result = func_sweep_balance_to_stored_value(array(
      'customer' => $test_params['customer'],
      'source'    => 'TEST',
      'reference' => 'VYT',
      'store_zipcode' => 11111,
      'store_id'      => 'TEST',
      'clerk_id'      => 'VYT',
      'sweep_value' => '10'));

    test_output($result);
  }
}


class Test_func_courtesy_add_ild_minutes
{
  function test($test_params)
  {
    $result = func_courtesy_add_ild_minutes(
      array(
        'customer'  => $test_params['customer'],
        'reason'    => 'test_reason4',
        'reference' => 'test_reference4',
        'amount'    => 16.5
      )
    );

    test_output($result);
  }
}


class Test_func_courtesy_add_balance
{
  function test($test_params)
  {
    $result = func_courtesy_add_balance(
      array(
        'customer'  => $test_params['customer'],
        'reason'    => 'test_reason5',
        'reference' => 'test_reference5',
        'amount'    => 18.85
      )
    );

    test_output($result);
  }
}


class Test_func_validate_pin_cards
{
  function test($test_params)
  {
    global $argv;

    $pin_list = $argv;

    unset($pin_list[0]);
    unset($pin_list[1]);

    $result = func_validate_pin_cards(
      array(
        "pin_list" => $pin_list
      )
    );

    print_r($result);
  }
}


class Test_func_apply_pin_cards
{
  function test($test_params)
  {
    global $argv;

    $pin_list = $argv;

    unset($pin_list[0]);
    unset($pin_list[1]);
    unset($pin_list[2]);
    unset($pin_list[3]);

    $params = array(
      'customer'    => $test_params['customer'],
      'destination' => $argv[3],
      'reason'      => 'test_reason6',
      'reference'   => 'test_reference6',
      'source'      => 'test source',
      'pin_list'    => $pin_list
    );

    print_r($params);

    $result = func_apply_pin_cards( $params );

    print_r($result);
  }
}


class Test_func_add_stored_value_by_tokenized_cc
{
/*
php test/payments/test_functions.php func_add_stored_value_by_tokenized_cc 32

php Ultra/Lib/CC/Queue_test.php processNext
*/
  function test($test_params)
  {
    global $argv;

    $recurring = ( isset($argv[3]) ? $argv[3] : FALSE );

    $result = func_add_stored_value_by_tokenized_cc(
      array(
        'customer'      => $test_params['customer'],
        'charge_amount' => 1.00,
        'description'   => 'test',
        'session'       => time(),
        'reason'        => 'TEST',
        'recurring_payment' => $recurring
      )
    );

    print_r($result);

    exit;
  }
}


class Test_func_add_balance_by_tokenized_cc
{
/*
php test/payments/test_functions.php func_add_balance_by_tokenized_cc 32

php Ultra/Lib/CC/Queue_test.php processNext
*/
  function test($test_params)
  {
    global $argv;

    $recurring = ( isset($argv[3]) ? $argv[3] : FALSE );

    $result = func_add_balance_by_tokenized_cc(
      array(
        'customer'      => $test_params['customer'],
        'charge_amount' => 1.00,
        'description'   => 'test',
        'session'       => time(),
        'reason'        => 'TEST',
        'recurring_payment' => $recurring
      )
    );

    print_r($result);

    exit;
  }
}


class Test_func_add_balance_by_credit_card
{
  function test($test_params)
  {
    global $argv;

    print_r($test_params['customer']);

    $result = func_add_balance_by_credit_card(
      array(
        'customer'  => $test_params['customer'],
        'reason'    => 'test_reason_cc',
        'amount'    => $argv[3],
        'session'   => 'test session',
        'upgrade_seconds_gain' => 0,
/*
        'bypass_billing_info'  => array(
          'cc_number'   => '123',
          'cc_exp_date' => '456',
          'ccv'         => '789',
          'first_name'  => 'Chris',
          'last_name'   => 'Furly'
        )
*/
      )
    );

    print_r($result);
  }
}


class Test_func_add_stored_value_by_credit_card
{
  function test($test_params)
  {
    $result = func_add_stored_value_by_credit_card(
      array(
        'customer'  => $test_params['customer'],
        'reason'    => 'test_reason6',
        'amount'    => 3,
        'session'   => 'test session',
        'upgrade_seconds_gain' => 0,
/*
        'bypass_billing_info'  => array(
          'cc_number'   => '123',
          'cc_exp_date' => '456',
          'ccv'         => '789',
          'first_name'  => 'Chris',
          'last_name'   => 'Furly'
        )
*/
      )
    );

    print_r($result);

    test_output($result);
  }
}


class Test_func_update_stored_credit_card
{
  function test($test_params)
  {
    $result = func_update_stored_credit_card(
      array(
        'customer'                => $test_params['customer'],
        'account_cc_exp'          => '1212',
        'account_cc_cvv'          => '1234',
        'account_cc_number'       => '5454871000080964',
        'account_address1'        => 'add 1 test '.time(),
        'account_address2'        => 'add 2 test '.time(),
        'account_city'            => 'New York',
        'account_state_or_region' => 'NY',
        'account_postal_code'     => '12345',
        'account_country'         => 'US',
        'account_number_phone'    => '1001001000',
        'account_number_email'    => time().'@example.com'
      )
    );

    test_output($result);
  }
}


class Test_verifyAmountDestination
{
  function test($test_params)
  {
    global $argv;
    $result = verifyAmountDestination($test_params['customer'], $argv[3], $argv[4]);
    echo 'RESULT: ' . print_r($result, TRUE) . "\n";
    exit;
  }
}


class Test_verifyProviderSkuAmount
{
  function test($test_params)
  {
    global $argv;
    $provider = $argv[2];
    $sku = $argv[3];
    $amount = $argv[4];
    $result = verifyProviderSkuAmount($provider, $sku, $amount);
    echo "PROVIDER: $provider, SKU: $sku, AMOUNT: $amount, RESULT: $result\n";
    exit;
  }
}


class Test_verifyProviderSku
{
  function test($test_params)
  {
    global $argv;
    $provider = $argv[2];
    $sku = $argv[3];
    $result = verifyProviderSku($provider, $sku) ? 'YES' : 'NO';
    echo "PROVIDER: $provider, SKU: $sku, KNOWN: $result\n";
    exit;
  }
}


class Test_funcBogoEnrollment
{
  function test($test_params)
  {
    global $argv;
    $customer_id = $argv[2];
    $iccid = $argv[3];
    $cos_id = $argv[4];
    $result = funcBogoEnrollment($customer_id, $iccid, $cos_id);
    echo "funcBogoEnrollment(CUSTOMER_ID: $customer_id, ICCID: $iccid, CUS_ID: $cos_id) ->\n";
    print_r($result);
    exit;
  }
}


class Test_funcMultiMonthEnrollment
{
  function test($test_params)
  {
    global $argv;

    $customer_id = $argv[2];
    $iccid       = $argv[3];

    $result = funcMultiMonthEnrollment(NULL, $iccid);
    print_r($result);

    $result = funcMultiMonthEnrollment($customer_id, NULL);
    print_r($result);

    $result = funcMultiMonthEnrollment($customer_id, $iccid);

    echo "funcMultiMonthEnrollment(CUSTOMER_ID: $customer_id, ICCID: $iccid) ->\n";
    print_r($result);
    
    exit;
  }
}


class Test_validateFactoredCommission
{
  function test($test_params)
  {
    global $argv;
    $customer_id = empty($argv[2]) ? NULL : $argv[2];
    $iccid = empty($argv[3]) ? NULL : $argv[3];

    $result = validateFactoredCommission($customer_id, $iccid);
    echo "RESULT: " . print_r($result, TRUE) . PHP_EOL;
  }
}


class Test_refundWebPosCustomer
{
  function test()
  {
    global $argv;
    $customer_id = $argv[2];

    $result = refundWebPosCustomer($customer_id);
    print_r($result);
  }
}

class Test_online_auto_enroll
{
  function test()
  {
    global $argv;
    $customer_id = $argv[2];
    $iccid       = $argv[3];

    $result = online_auto_enroll($customer_id, $iccid);
    print_r($result);
  }
}


function test_output($result)
{
  if ( count($result['errors']) || ! $result['success'] )
  {
    echo "Test got errors:\n";
    print_r($result);
  }
  else
  {
    echo "Test OK\n";
  }
}


# db connection #


# create test data #


class test_customer_data
{
  public $COS_ID      = 98279;
  public $CUSTOMER_ID = 2062;
  public $CUSTOMER    = '199463754543';
  public $ACCOUNT_ID  = '12344321';
  public $ACCOUNT     = '199463754543';
  public $FIRST_NAME  = 'MJSHILKJMIRUZZLOKAIQ';
  public $LAST_NAME   = 'MJSHILKJMIRUZZLOKAIQ';
  public $COMPANY     = '';
  public $ADDRESS1    = '160 Franklin Street';
  public $ADDRESS2    = '';
  public $CITY        = 'New York';
  public $STATE_REGION  = 'NY';
  public $COUNTRY       = 'USA';
  public $POSTAL_CODE   = '10100';
  public $LOCAL_PHONE   = '1000000025';
  public $E_MAIL        = 'test@ultra.me';
  public $CC_NUMBER     = '1234';
  public $CC_EXP_DATE   = '';
  public $ACCOUNT_GROUP_ID  = '1234';
  public $CCV               = '1234';
}


if ( isset($argv[2]) && $argv[2] )
{
  $test_params['customer'] = get_customer_from_customer_id($argv[2]);
}
else
{
  $test_params['customer'] = new test_customer_data;
}


// print_r($test_params['customer']);


# perform test #


$testClass = 'Test_'.$argv[1];

print "$testClass\n\n";

$testObject = new $testClass();

$testObject->test( $test_params );


/*
Examples:

php test/payments/test_functions.php func_apply_pin_cards 31 MONTHLY 400000004000000043
*/

?>

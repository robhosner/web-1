<?php

function findFiles($dir = '.', $pattern = '/./')
{
  $prefix = $dir . '/';
  $dir = dir($dir);

  while (false !== ($file = $dir->read()))
  {
    if ($file === '.' || $file === '..')
      continue;

    $file = $prefix . $file;

    if (is_dir($file))
      findFiles($file, $pattern);

    if (preg_match($pattern, $file))
      find_errors($file);
  }
}

function find_errors($filename)
{
  $error_lines = array();

  $file = file($filename);
  for ($i = 0; $i < count($file); $i++)
  {
    $line = $file[$i];
    $filtered = filter_var(
      $line,
      FILTER_SANITIZE_STRING,

        FILTER_FLAG_NO_ENCODE_QUOTES | 
        FILTER_FLAG_STRIP_LOW | 
        FILTER_FLAG_ENCODE_LOW | 
        FILTER_FLAG_ENCODE_AMP
    );

    if (strlen($line) != strlen($filtered) + 1 
      && strpos($filtered, '#38') == FALSE
      // && strpos($filtered, '#10') == FALSE
      // && strpos($filtered, '#9') == FALSE
      // && strpos($line, '?') == FALSE
    )
    {
      if (strpos($line, '<') !== FALSE && strpos($filtered, '<') == FALSE)
      {}
      else
      {
        $error_line = array();
        $error_line[] = "$i," . strlen($line)     . " : " . $line;
        $error_line[] = "$i," . strlen($filtered) . " : " . $filtered;

        $error_lines[] = $error_line;
      }
    }
  }

  if (count($error_lines))
  {
    echo $filename . PHP_EOL;
    for ($j = 0; $j < strlen($filename); $j++)
      echo '*';
    echo PHP_EOL;

    for ($k = 0; $k < count($error_lines); $k++)
    {
      echo $error_lines[$k][0];
      echo $error_lines[$k][1];
      echo PHP_EOL.PHP_EOL;
    }
  }
}

findFiles('.', '/.php/');
// find_errors('./Ultra/Messaging/Templates.php');

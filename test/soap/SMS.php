<?php

/**
 * Useful for constructing XML or sending SMS messages.
 *
 *  Usage: $messenger = new SMS();
 *  $messenger->send('9499032013', 'Testing');
 *
 * @throws Exception
 * @author bwalters@ultra.me
 */
class SMS {

  protected $wsdl               = 'runners/amdocs/TMobile_MVNE_4_5_prod_proxy.wsdl';
  protected $location           = 'https://acc-prod.proxy.ultra.me:8084/acc/inbound.aspx';
  protected $action             = 'http://www.sigvalue.com/acc/SendSMS';
  protected $cert_location      = '/etc/httpd/certs/acc/ULTRA.pem';
  protected $senderId           = 'MVNEACC';
  protected $channelId          = 'ULTRA';
  protected $timeStamp          = false;
  protected $MSISDN             = false;
  protected $qualityOfService   = 801;
  protected $preferredLanguage  = 'en';

  private   $debug_mode         = false;
  private   $client             = false;
  private   $result             = false;
  private $acceptable_params    = array(
    'debug_mode',
    'cert',
    'senderId',
    'channelId',
    'qualityOfService',
    'MSISDN',
    'preferredLanguage'
  );

  public function __construct($params = false)
  {
    if ($params && is_array($params)) {
      foreach ($params as $param => $value) {
        if (in_array($param, $this->acceptable_params)) {
          $this->{$param} = $value;
        }
      }
    }
  }

  public function send($MSISDN = false, $message = false, $preferredLanguage = false)
  {
    if (!$MSISDN || !$message) {
      throw new Exception('Missing MSISDN or message');
    }

    $this->MSISDN = $MSISDN;

    if ($preferredLanguage) {
      $this->preferredLanguage = $preferredLanguage;
    }

    $this->message = $message;

    $now              = time();
    $this->timeStamp  = date('Y-m-d', $now) . 'T' . date('H:s:i', $now);
    $this->payload    = $this->getXML();

    return $this->sendSMS();
  }

  private function getXML()
  {
    $soapEnvelope =
    '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:acc="http://www.sigvalue.com/acc">
       <soapenv:Header/>
       <soapenv:Body>
          <acc:SendSMS>
             <acc:UserData>
                <acc:senderId>' . $this->senderId . '</acc:senderId>
                <acc:channelId>' . $this->channelId . '</acc:channelId>
                <acc:timeStamp>' . $this->timeStamp . '</acc:timeStamp>
             </acc:UserData>
             <acc:qualityOfService>' . $this->qualityOfService . '</acc:qualityOfService>
             <acc:preferredLanguage>' . $this->preferredLanguage . '</acc:preferredLanguage>
             <acc:smsText>' . $this->message . '</acc:smsText> <!-- Mandatory -->
             <acc:SubscriberSMSList>
                <!--Zero or more repetitions:-->
                <acc:SubscriberSMS>
                   <acc:MSISDN>' . $this->MSISDN . '</acc:MSISDN>
                   <!--Optional:-->
                   <acc:Language>' . $this->preferredLanguage . '</acc:Language>
                   <!--Optional:-->
                   <acc:Text>' . $this->message . '</acc:Text>
                </acc:SubscriberSMS>
             </acc:SubscriberSMSList>
          </acc:SendSMS>
       </soapenv:Body>
    </soapenv:Envelope>';

    return $soapEnvelope;
  }

  private function sendSMS()
  {
    try {
      $this->client = new SoapClient(
        // $this->wsdl,
        null,
        array(
            "location"      => $this->location,
            "uri"           => $this->action,
            "trace"         => 1,
            "exceptions"    => true,
            "local_cert"    => $this->cert_location,
            'passphrase'    => 'mvne1',
            "uri"           => "urn:xmethods-delayed-quotes",
            "style"         => SOAP_RPC,
            "use"           => SOAP_ENCODED,
            "soap_version"  => SOAP_1_2 ,
        )
      );

      $this->result = $this->client->__doRequest($this->payload, $this->location, $this->action, 1);

      if ($this->debug_mode) {
        $this->debug();
      }
    } catch( SoapException $e ) {
      echo $e->getMessage();
      return false;
    }

    return $this->result;
  }

  /**
   * Prints debug information about the soap client, request and response.
   */
  private function debug() {
    echo "========== CLIENT =========" . PHP_EOL;
    print_r($this->client);
    echo PHP_EOL;

    echo "========= PAYLOAD =========" . PHP_EOL;
    print_r($this->payload);
    echo PHP_EOL;

    echo "====== REQUEST HEADERS =====" . PHP_EOL;
    var_dump($this->client->__getLastRequestHeaders());
    echo PHP_EOL;

    echo "========= REQUEST ==========" . PHP_EOL;
    var_dump($this->client->__getLastRequest());
    echo PHP_EOL;

    echo "====== RESPONSE HEADERS =====" . PHP_EOL;
    var_dump($this->client->__getLastResponseHeaders());
    echo PHP_EOL;

    echo "========= RESPONSE =========" . PHP_EOL;
    var_dump($this->client->__getLastResponse());
    echo PHP_EOL;

    echo "========= RESULT =========" . PHP_EOL;
    print_r($this->result);
    echo PHP_EOL;
  }
}

$messenger = new SMS(array('debug_mode' => true));
#$messenger = new SMS();
$messages = array(
  'English', // English
  'Español', // Spanish
  '中国普通话 - Mandarin Chinese', // Mandarin
  'Руссиа - Russian', // Russian
  'Chào mừng bạn đến siêu di động . Số điện thoại của bạn là', // vietnamese
);

foreach ($messages as $message) {
  echo "{$message} - " . (($messenger->send('9499032013', $message)) ? 'success' : 'failure') . PHP_EOL;
}

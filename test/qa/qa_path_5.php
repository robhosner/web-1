<?php

/*
Test [ Neutral ] => [ Active ] transition
*/

# Usage:
# %> php test/qa/qa_path_5.php $PLAN [EN|ES] $ZIP $ICCID


include_once('classes/Result.php');
include_once('test/api/test_api_include.php');
include_once('test/qa/test_classes.php');
include_once('web.php');


# default params
$plan               = 'L29';
$preferred_language = 'EN';

if ( isset($argv[1]) && $argv[1] )
{
  $allowed_plans = array('L19','L29','L39','L49','L59');

  if ( in_array( $argv[1] , $allowed_plans ) )
    $plan = $argv[1];
  else
    die($argv[1].' is not a valid plan');
}

if ( isset($argv[2]) )
{
  $allowed_languages = array('EN','ES');

  if ( in_array( $argv[2] , $allowed_languages ) )
    $preferred_language = $argv[2];
  else
    die($argv[2].' is not a valid language');
}

teldata_change_db();

$ZIP = 12345;
if ( array_key_exists ( 3 , $argv ) && $argv[3] )
  $ZIP = $argv[3];

$ICCID = '';
if ( array_key_exists ( 4 , $argv ) && $argv[4] )
  $ICCID = $argv[4];
else
  $ICCID = get_test_iccid();

$ICCID = luhnenize($ICCID);

// validate ICCID

if ( strlen($ICCID) != 19 )
  die('We need a 19 char ICCID');

$valid = validate_ICCID($ICCID,1);

if ( ! $valid )
  die('the given ICCID is invalid or already used');

// validate ICCID against htt_customers_overlay_ultra

$overlay_data = get_ultra_customers_from_iccid( $ICCID , array('customer_id') );

if ( $overlay_data && is_array($overlay_data) && count($overlay_data) )
  die('ERR_API_INVALID_ARGUMENTS: the given ICCID is already used');

// skip Activate/Can

// create ULTRA user in DB

$ICCID_18 = substr($ICCID,0,-1);

$params = array(
  'masteragent'           => '99999',
  'userid'                => '99998',
  'distributor'           => '99997',
  'dealer'                => '99996',
  'preferred_language'    => $preferred_language,
  'cos_id'                => get_cos_id_from_plan('STANDBY'),
  'first_name'            => 'testPHP',
  'last_name'             => 'testPHP',
  'postal_code'           => $ZIP,
  'country'               => 'USA',
  'e_mail'                => 'test@null',
  'plan_state'            => 'Neutral',
  'plan_started'          => 'NULL',
  'plan_expires'          => 'NULL',
  'customer_source'       => '0',
  'current_iccid'         => $ICCID_18,
  'current_iccid_full'    => $ICCID
);

$result = create_ultra_customer_db_transaction($params);

$customer = $result['customer'];

if ( ! $customer )
  die('DB error - 00');

$accounts_update_query = accounts_update_query(
  array(
    'add_to_balance'          => 59,
    'customer_id'             => $customer->CUSTOMER_ID,
    'last_recharge_date_time' => 'now'
  )
);

if ( ! is_mssql_successful(logged_mssql_query($accounts_update_query)) )
  die('DB error - 01');

$customer = get_customer_from_customer_id($customer->CUSTOMER_ID);

print_r( $customer );

// transition from Neutral to Active

$activationPlan   = get_plan_name_from_short_name($plan);
$transition_name  = 'Activate '.$activationPlan;
$context          = array( 'customer_id' => $customer->CUSTOMER_ID );
$resolve_now      = TRUE;
$dry_run          = FALSE;
$max_path_depth   = 1;

$result_status = change_state($context, $resolve_now, $transition_name, 'take transition', $dry_run, $max_path_depth);

print_r( $result_status );

/*
Example:
php test/qa/qa_path_5.php L39 ES 12345 1234512345123451234
*/

?>

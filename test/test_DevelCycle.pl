use strict;
use warnings;

use Data::Dumper;
use Devel::Cycle;


use Ultra::Lib::MQ::ControlChannel;
#my $mq_cc = Ultra::Lib::MQ::ControlChannel->new();


my $mq_cc = { 1=>2 , 2=>3 };
$mq_cc->{1} = $mq_cc;

find_cycle($mq_cc,sub{ print 'FOUND CYCLE : '.Dumper( @_ ); });

__END__


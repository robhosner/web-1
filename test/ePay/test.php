<?php

require_once 'db.php';
require_once 'partner-face/ePay.php';

teldata_change_db();

$zipcode = '12345';

$boltOns = NULL;

$provider_name = NULL;

$sku = NULL;

$load_amount = NULL;

$plan_name = NULL;

$subproduct_id = NULL;

$brand = 'ULTRA';


// load a brand 1 ICCID
$res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 1 and CUSTOMER_ID is not null');
$ref_iccid = $res->ICCID_FULL;

echo "iccid = $ref_iccid\n";
echo "brand = $brand\n";

$error = secondaryValidation($ref_iccid, $zipcode, $boltOns, $provider_name, $sku, $load_amount, $plan_name, $subproduct_id, $brand);
echo "error = $error\n\n\n";


// load a brand 2 ICCID
$res = find_first('select top 1 ICCID_FULL from htt_inventory_sim where BRAND_ID = 2 and CUSTOMER_ID is not null');
$ref_iccid = $res->ICCID_FULL;

echo "iccid = $ref_iccid\n";
echo "brand = $brand\n";

$error = secondaryValidation($ref_iccid, $zipcode, $boltOns, $provider_name, $sku, $load_amount, $plan_name, $subproduct_id, $brand);
echo "error = $error\n";


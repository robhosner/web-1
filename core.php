<?php

// HTT_CONFIGROOT contains the directory where environment configuration files are stored
$e_config    = loadEnvConfig();
$credentials = include_credentials_files($e_config);
$security    = include_security_env($e_config);
$sites       = include_site_parameters( $e_config );

/**
 * loadEnvConfig
 *
 * Load $e_config from environment.json
 *
 * @return array
 */
function loadEnvConfig()
{
  $e_config = [];

  $json_file = getenv('HTT_CONFIGROOT') . "/environment.json";

  if (file_exists($json_file))
  {
    $json_config = file_get_contents( $json_file );

    $e_config = json_decode( $json_config , TRUE );
  }
  else
    error_log("Cannot find environment.json");

  return $e_config;
}

/* DO NOT PUT ANY INCLUDES ABOVE THIS LINE */
require_once 'lib/underscore/underscore.php';
require_once 'classes/Outcome.php';
require_once 'Ultra/Lib/DB/AccountAliases.php';
require_once 'Ultra/Lib/DB/Customer.php';
require_once 'Ultra/Lib/DemoLine.php';
require_once 'lib/util-common.php';
require_once 'Ultra/Lib/DB/MSSQL.php';

// MVNO-2461: set custom error handler to trap MSSQL errors (in production only)
if ( ! \Ultra\UltraConfig\isDevelopmentDB())
  $result = set_error_handler('ultraErrorHandler', E_ALL);

require_once 'vendor/autoload.php';

// Autoload files contained within a specific namespace
spl_autoload_register(function($class) {
  loadClass($class, 'Ultra');
});

spl_autoload_register(function($class) {
  loadClass($class, 'classes');
});

function loadClass($class, $basePrefix) {
  // project-specific namespace prefix
  $prefix = $basePrefix . '\\';
  $baseDir = $basePrefix . '/';

  // does the class use the namespace prefix?
  $len = strlen($prefix);
  if (strncmp($prefix, $class, $len) !== 0)
  {
    // no, move to the next registered autoloader
    return;
  }

  // get the relative class name
  $relativeClass = substr($class, $len);
  $file = $baseDir . str_replace('\\', '/', $relativeClass) . '.php';

  if (file_exists($file))
  {
    require_once $file;
  }
}

/**
 * include_json_credentials
 *
 * Import config tokens from environment file /home/ht/config/$HTT_ENV/$CREDENTIAL/cred.json
 * See PROD-2148
 *
 * @return array
 */
function include_json_credentials( array $credentials , $credential_group )
{
  // load JSON file associated to $credential_group
  $credential_group_file = (getenv('HTT_CONFIGROOT') . "/$credential_group/cred.json");

  if ( file_exists( $credential_group_file ) )
  {
    // load entire JSON
    $credential_group_json = file_get_contents( $credential_group_file );

    if ( empty( $credential_group_json ) )
      error_log( "CONFIGURATION FILE $credential_group IS EMPTY" );
    else
    {
      // decode JSON
      $credential_group_json_data = json_decode( $credential_group_json, TRUE );

      if ( empty( $credential_group_json_data ) || ! is_array( $credential_group_json_data ) )
        error_log( "CONFIGURATION FILE $credential_group IS NOT VALID JSON" );
      else
        $credentials = array_merge( $credentials , $credential_group_json_data );
    }
  }
  else
    error_log( "CONFIGURATION FILE $credential_group NOT FOUND" );

  return $credentials;
}

/**
 * include_credentials_files
 *
 * Include files associated to all credentials groups mentioned in $e_config['credentials']
 *
 * @return array
 */
function include_credentials_files( $e_config )
{ 
  // space delimited list of credentials groups
  $credentials_str = $e_config['credentials'];
  $carray = explode(' ', $credentials_str);
  $credentials = array();

  // loop through credentials groups
  foreach ($carray as $cname)
  {
    // include file associated to credentials group $cname
    $ifile = (getenv('HTT_CONFIGROOT') . "/$cname/cred.php");

    if ( file_exists($ifile) )
    {
      include_once($ifile); // creates local variable $cred_config
      $credentials[$cname] = $cred_config;
    }
    else
      $credentials[$cname] = [];

    // also include JSON credentials (most are redefined from cred.php)
    $json_file = getenv('HTT_CONFIGROOT') . "/$cname/cred.json";

    if (file_exists($json_file))
      $credentials[$cname] = include_json_credentials($credentials[$cname], $cname); // merges values from json file
  }

  // globals
  $json_file = getenv('HTT_CONFIGROOT') . "/globals/cred.json";

  if (file_exists($json_file))
    $credentials['globals'] = include_json_credentials(array(), 'globals'); // merges values from json file

  return $credentials;
}

/**
 * include_security_env
 *
 * Unlike credentials ( $e_config['credentials'] ) and other parameters, we load security parameters explicitly
 * For each environments, we may have the following security tokens:
 *  - ip/allow
 *  - auth/mode
 *
 * @return array
 */
function include_security_env( $e_config )
{
  $senv_str = empty($e_config) ? '' : $e_config['security/env'] ;
  $security = array();
  foreach (array('ip/allow', 'auth/mode') as $sparam)
  {
    $security_file_name = find_config_file('security', $senv_str, $sparam);

    if ( file_exists( $security_file_name ) )
    {
      $c = file_get_contents( $security_file_name );
      $security[$sparam] = explode(' ', trim($c, "\n\r"));
    }
    else
      error_log( "CONFIGURATION FILE $security_file_name NOT FOUND" );
  }

  return $security;
}

/**
 * include_site_parameters
 *
 * Unlike credentials ( $e_config['credentials'] ) and other parameters, we load site parameters explicitly
 * For each domain, we may have the following tokens:
 *  - type
 *  - name
 *  - aliases
 *
 * @return array
 */
function include_site_parameters( $e_config )
{
  $sites_str = empty($e_config) ? '' : $e_config['www/sites'] ;
  $sites = array();
  foreach (explode(' ', $sites_str) as $site)
    foreach (array('type', 'name', 'aliases') as $sparam)
    {
      $parameters_file_name = getenv('HTT_CONFIGROOT') . "/../sites/$site/$sparam";

      if ( file_exists( $parameters_file_name ) )
        $sites[$site][$sparam] = explode(' ', trim(file_get_contents( $parameters_file_name ),"\n\r"));
      else
        error_log( "CONFIGURATION FILE $parameters_file_name NOT FOUND" );
    }

  return $sites;
}

/**
 * find_config_file
 */
function find_config_file($type, $key, $subkey)
{
  return getenv('HTT_CONFIGROOT') . "/../$type/$key/$subkey";
}

/**
 * find_security
 *
 * Get security token value
 */
function find_security($skey)
{
  global $security;

  return ( isset($security[$skey]) ) ? $security[$skey] : NULL ;
}

/**
 * get_sites
 *
 * Use the first site in the list
 */
function get_sites( $sites )
{
  $all_sites = array_keys($sites);

  if (count($all_sites) < 1)
    dlog('', "No sites are defined, something's wrong with out environment configuration");

  return $all_sites;
}

function find_site_param($site, $skey, $sites)
{
  return ( isset($sites[$site][$skey]) ) ? $sites[$site][$skey] : NULL ;
}

function find_domain_url($site=NULL)
{
  $site_url = find_site_url($site);

  $parts = explode('@',$site_url);

  return $parts[1];
}

function find_site_url($site=NULL)
{
  global $sites;

  /* use the first site in the list if it's not given */
  if (NULL == $site)
  {
    $all_sites = get_sites( $sites );
    $site = $all_sites[0];
  }

  if (NULL == $site)
  {
    dlog('', "No site, something's wrong with out environment configuration");
    return NULL;
  }

  $methods = find_site_param($site, 'type', $sites);
  $method  = $methods[0] === 'ssl' ? 'https' : 'http';
  $names   = find_site_param($site, 'name', $sites);

  $auth = find_security('auth/mode');
  $matches = array();

  $cred = '';
  if ($auth && is_array($auth) && count($auth) > 0 &&
      preg_match('/^HTTPAUTH=(.+)=/', $auth[0], $matches))
  {
    $cred = "$matches[1]@";
  }

  return sprintf("%s://%s%s", $method, $cred, $names[0]);
}

function find_credential($skey)
{
  global $credentials;

  // if the credentials aren't there, return NULL
  if ( ! $credentials || ! is_array($credentials) )
    return NULL;

  foreach ($credentials as $cname => $cred)
  {
    if ( ! empty( $cred ) )
      foreach ($cred as $key => $val)
        if ($key === $skey)
          return $val;
  }
}

function set_tz_by_offset($offset)
{
  $offset = $offset*60*60;
  $abbrarray = timezone_abbreviations_list();
  foreach ($abbrarray as $abbr)
  {
    //echo $abbr."<br>";
    foreach ($abbr as $city)
    {
      //echo $city['offset']." $offset<br>";
      if ($city['offset'] == $offset)
      {
        // remember to multiply $offset by -1 if you're getting it from js
        date_default_timezone_set($city['timezone_id']);
        return true;
      }
    }
  }
  date_default_timezone_set("ust");
  return false;
}

/* public domain from http://php.net/manual/en/function.is-array.php */
function isVector($var)
{
  return is_array($var) &&
    count(array_diff_key($var, range(0, count($var) - 1))) == 0;
}

/* public domain from http://php.net/manual/en/function.is-array.php */
function isAssociative($var)
{
  return is_array($var) && !isVector($var);
}


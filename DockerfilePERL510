FROM buildpack-deps
MAINTAINER Peter Martini <PeterCMartini@GMail.com>

ENV HOME=/home/ht/www/rgalli_acc_dev/web
ENV CONFIG=/home/ht/config/rgalli_acc_dev
ENV HTT_CONFIGROOT=$CONFIG
ENV DOCUMENT_ROOT=$HOME

COPY . $HOME

RUN apt-get update \
    && apt-get install -y curl procps \
    && rm -fr /var/lib/apt/lists/* \
    && apt-get install -y git \
    && apt-get update && apt-get install -y apache2 vim jq net-tools \
    && useradd -m apache

RUN mkdir /usr/src/perl
COPY *.patch /usr/src/perl/
WORKDIR /usr/src/perl

RUN curl -SL https://cpan.metacpan.org/authors/id/D/DA/DAPM/perl-5.10.1.tar.bz2 -o perl-5.10.1.tar.bz2 \
    && echo '9385f2c8c2ca8b1dc4a7c31903f1f8dc8f2ba867dc2a9e5c93012ed6b564e826 *perl-5.10.1.tar.bz2' | sha256sum -c - \
    && tar --strip-components=1 -xjf perl-5.10.1.tar.bz2 -C /usr/src/perl \
    && rm perl-5.10.1.tar.bz2 \
    && cat *.patch | patch -p1 -l \
    && ./Configure -Dusethreads -Duse64bitall -Duseshrplib -Dvendorprefix=/usr/local -A ccflags=-fwrapv -des \
    && make -j$(nproc) \
    && make install \
    && cd /usr/src \
    && curl -LO https://raw.githubusercontent.com/miyagawa/cpanminus/master/cpanm \
    && chmod +x cpanm \
    && ./cpanm App::cpanminus \
    && rm -fr ./cpanm /root/.cpanm /usr/src/perl /tmp/* \
    && apt-get install -y unixodbc unixodbc-dev unixodbc-bin libodbc1 odbcinst1debian2 tdsodbc freetds-bin freetds-common freetds-dev libct4 libsybdb5 \
    && mkdir -p /usr/share/freetds/include \
    && ln -s /usr/lib/x86_64-linux-gnu/ /usr/share/freetds/lib \
    && ln -s /usr/include /usr/share/freetds/include/freetds \
    && export SYBASE=/usr/share/freetds \
    && cpanm DBI DBD::Sybase XML::Compile::SOAP::Daemon::AnyDaemon Any::Daemon HTTP::Daemon::SSL XML::LibXML::PrettyPrint XML::Simple Data::UUID JSON::XS Redis Crypt::SSLeay Devel::Size -f \
    && apt-get install -y php php7.0-curl \
    && CONFIG_SERVICE=http://web-qa-08.ultra.local:5030 php $HOME/LoadConfiguration.php universal_api_qa \
    && mv /home/ht/config/universal_api_qa/ $CONFIG

RUN mkdir /etc/apache2/conf-enabled/htt/ && mkdir -p /var/log/htt/ && mkdir -p /etc/httpd/certs && mkdir -p /etc/httpd/certs/acc/ && mkdir /credentials
RUN cat $CONFIG/ultra_develop_tel_api_accounts/cred.json | jq -r '{"mw/db/pfile__retrieved"} | .[]' > /credentials/api_accounts

ADD DockerDependencies/rgalli-acc-dev.ultra.me.conf /etc/apache2/conf-enabled/htt/rgalli-acc-dev.ultra.me.conf
ADD DockerDependencies/envvars /etc/apache2/envvars
ADD DockerDependencies/certs/gd_bundle.crt /etc/httpd/certs/gd_bundle.crt
ADD DockerDependencies/certs/ULTRA.ME.crt /etc/httpd/certs/ultra.me.crt
ADD DockerDependencies/certs/ultra.me.key /etc/httpd/certs/ultra.me.key
ADD DockerDependencies/certs/ULTRA.ME.crt /etc/httpd/certs/acc/ultra.me.crt
ADD DockerDependencies/certs/ultra.me.key /etc/httpd/certs/acc/ultra.me.key
ADD DockerDependencies/certs/acc/ULTRA.pem /etc/httpd/certs/acc/ULTRA.pem

WORKDIR $HOME

CMD ["perl5.10.1","-de0"]
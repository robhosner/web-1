This is a magical, wonderful walkthrough of the `ht.cf` and `webserver.cf` code as of *2013-02-07*.  We'll start from the top, with `ht.cf`.

If you want to do a syntax check of `ht.cf`, look in `cm.git/Makefile` which has a *syntax* target that runs a syntax check.

If you want to actually run `ht.cf`, look in `/etc/cron.d/cfrun` which
is in turn set up by `ht.cf` (so yes, there's a chicken-and-egg issue
here).  The already mentioned `Makefile` has *run* and *debug* targets
so you can run things explicitly and kickstart the process, laying an
egg if you will.

Furthermore, the `Makefile` has targets *first*, *second*, and *third*
to assist in getting a machine set up the first time.  The exact
mechanics of those targets are not important here, but can be easily
reviewed in the `Makefile`.

```
body common control
{
      bundlesequence => { "g", "gogo" };
      inputs => { "sketches/CFEngine/stdlib/cfengine_stdlib.cf", "tzz_stdlib.cf", "sketches/Security/SSH/ssh.cf", "sketches/VCS/vcs_mirror/main.cf", "webserver.cf" };
}

body agent control
{
      agentfacility => "LOG_LOCAL6";
}
```

This preamble sets up `inputs` with some library includes (`cfengine_stdlib.cf` is
part of CFEngine; while `tzz_stdlib.cf` has some simple utilities).
`ssh.cf` and `vcs_mirror/main.cf` are libraries for controlling the
SSH configuration and for mirroring a Git repository locally,
respectively.

The `bundlesequence` is like a top-level `main()` function for CFEngine's `cf-agent` program.  Here we run only two things: `g` (the general variable setup bundle) and `gogo` (the bundle that launches all the others).

Finally, we set the `agentfacility` specifically for *rsyslogd*, so it will use *LOCAL6*.  This is routed appropriately further in `rsyslogd.conf`, which see.

```
bundle common g
{
  classes:
      "managed" or => { "tardis_01", "vault", "web_01", "web_dev_01", "web_qa_01" };
      "webserver" or => { "web_01", "web_dev_01", "web_qa_01" };
      "dev_webserver" or => { "web_dev_01" };
      "qa_webserver" or => { "web_qa_01" };
      "prod_webserver" or => { "web_01" };
      "postfix_satellite" or => { "vault", "webserver" };
      "postfix_hub" or => { "tardis_01" };
      "memcached_home" or => { "webserver", "tardis_01" };
      # the same machine can't serve more than one of
      # (transferto_runner, billing_runner)
      "transferto_prod_runner_home" or => { "tardis_01" };
      "billing_runner_home" or => { "web_dev_01", "web_01" };
      "home_checkout" expression => fileexists("$(checkout)/.git");
      "wordpress_webserver" or => { "web_01", "web_dev_01" };
      "splunk_client" or => { "webserver" };
```

The `g` bundle sets up global variables and classes in CFEngine.  Here
we lay down some ground rules:

* what machines are *managed*?
* who are the *webserver*s?
* what machines are webservers for *dev*, *qa*, and *prod*?
* who are the *postfix_satellite*s and who is the *postfix_hub*?
* where is *memcached_home*? (machines where *memcached* is installed,
  currently defined as *webserver* or `tardis-01`, the last one
  specifically because it hosts the TransferTo runner)
* what machines serve as the *transferto_prod_runner_home*?
* what machines serve as the *billing_runner_home*?
* what machines will be *wordpress_webserver*s?
* what machines will be *splunk_client*s?
* functionally, *home_checkout* is only set if the file or directory
  `$(checkout)/.git` exists, but note that `$(checkout)` is defined
  later.  CFEngine allows this out-of-order reference, although I have
  tried to keep its usage limited to only the places where it made
  sense.

If this seems like we're defining a global systems inventory, we
are... in a way.  It's really rules that are specific to CFEngine, but
the idea is to avoid specialized software and configurations per
_machine_, preferring instead to work in logical terms.

We continue the `g` bundle with global variables.  These variables will be used a lot throughout the configuration, so let's review them:

```
  vars:
    debug::
      "debug_prefix" string => "/tmp/cf-debug";
    !debug::
      "debug_prefix" string => "";
```

First, we define a variable called `debug_prefix` which is either
empty or contains `/tmp/cf-debug`.  The choice is made by the *debug*
class, which is *not* defined above.  It is intended to be given from
the command line (look in the `Makefile` and observe the difference
between the *debug* and *run* targets, which is the `-Ddebug` switch).

You can define behavior specific for other such classes to be given
from the command line.  The most important one you should know is the
*quick* class, which most notably disables package checks and
installations, producing a quicker CFEngine run.  Check
`/etc/cron.d/cfrun` to see the exact schedule for the *quick* class
and for full runs without it.

    !(webserver||transferto_prod_runner_home)::
      "environments" slist => { "cf_null" }; # empty list!

    transferto_prod_runner_home::
      "environments" slist => { "transferto_runner_prod" };

    web_dev_01::
      "environments" slist => {
                                "celluphone_dev",
                                "eapi_dev",
                                "indiald_app_dev",
                                "rainmaker_dev",
                                "rgalli2_dev",
                                "rgalli3_dev",
                                "rgalli_dev",
                                "riz_api",
                                "riz_dev",
                                "sean_dev",
                                "seanapi_dev",
                                "tzz2_dev",
                                "tzz_dev",
                                "ultra_api_dev",
                                "ultra_ui_dev",
                                "ultrame_ee_dev",
                              };

Here we find the variable `environments` which is different on
different machines, and is an *slist* (string list).  First we define
where it's empty, which in CFEngine is indicated by the `cf_null`
string as the only member; here that is on machines that are not
*webserver* or the *transferto_prod_runner_home*.

The strings in these `environments` can be repeated between machines,
so multiple machines could carry the same environment.  Setting up a
web site on that environment will be trickier (because of the vhost)
and hasn't been needed so far, but at least for non-website deployment
the process should be obvious.

Then we set the context to `web_dev_01` (which is only true on the
machine `web-dev-01`) and list the development environments that live
there.  So, when `cf-agent` runs on `web-dev-01`, this is the
`environments` slist it will see.

The context will remain `web_dev_01` until we set it to something else
or a new promise type begins.  Right now we're still doing variables.

      "resolved_environments[rgalli2_dev]" string => "celluphone_dev ultra_api_dev rainmaker_dev seanapi_dev";

      "resolved_environments[seanapi_dev]" string => "seanapi_dev";

We now meet a new kind of variable, the array.
`resolved_environments` on `web-dev-01` is an array with two keys, and
each key has a string value.  The specific purpose of
`resolved_environments` is to set up periodic resolution of state
machine actions from a single entry point, but the array syntax is
worth remembering because we will use it heavily throughout `ht.cf`.

      "crones[UPDATE_DEALER_LOC][site]" string => "rgalli2-dev.uvnv.com";
      "crones[UPDATE_DEALER_LOC][cron_schedule]" string => "*/28 * * * *";

      "crones[CBS_ACT_LOG][site]" string => "rgalli2-dev.uvnv.com";
      "crones[CBS_ACT_LOG][cron_schedule]" string => "*/3 * * * *";

      "crones[CBS_TRANS_LOG][site]" string => "rgalli2-dev.uvnv.com";
      "crones[CBS_TRANS_LOG][cron_schedule]" string => "*/7 * * * *";

      "crones[porting_crone][site]" string => "rgalli2-dev.uvnv.com";
      "crones[porting_crone][cron_schedule]" string => "*/3 5-22 * * *";

      "crones[ensure_crone][site]" string => "rgalli2-dev.uvnv.com";
      "crones[ensure_crone][cron_schedule]" string => "*/5 5-22 * * *";

      "crones[threeci_test][site]" string => "rgalli2-dev.uvnv.com";
      "crones[threeci_test][cron_schedule]" string => "0 7-22 * * *";

      # "crones[reservation_crone][site]" string => "rgalli2-dev.uvnv.com";
      # "crones[reservation_crone][cron_schedule]" string => "* * * * *";

      "crones[reaper_crone][site]" string => "rgalli2-dev.uvnv.com";
      "crones[reaper_crone][cron_schedule]" string => "*/3 * * * *";

      "crones[open_transactions_reaper][site]" string => "rgalli2-dev.uvnv.com";
      "crones[open_transactions_reaper][cron_schedule]" string => "3 * * * *";

      "crones[open_actions_reaper][site]" string => "rgalli2-dev.uvnv.com";
      "crones[open_actions_reaper][cron_schedule]" string => "3 * * * *";

Ah, crones.  A stupid pun on *cron* that you'll have to live with for
some time.  Sorry.

These are inserted in a cron job later.  The `crones` array defines
the name of a crone (the top key), and at the second level has the
`site` and `cron_schedule` keys to define, basically, when the cron
job should run and what web site it should hit.  The actual code these
run is in `lib/crone_common.php` and is just a big switch statement.

If you wanted arguments for the crones (and we all know crones love
arguments... heh heh) you could easily add another second-level key.
So far that wasn't necessary.

    web_01::
      "environments" slist => {
                                "celluphone_prod",
                                "epay_prod",
                                "rainmaker_prod",
                                "rgalli4_dev",
                                "threeci_prod",
                                "tzz3_dev",
                                "glickmancapital_prod",
                                "indiald_prod",
                                "ultra_api_prod_internal",
                                "ultra_api_prod_public",
                                "ultra_ui_prod",
                                "ultrame_ee_prod",
                                "uvnv-wordpress_prod",
                                "uvnv_prod",
                                "ultra_gateway_prod",
                              };

      "resolved_environments[ultra_api_prod_internal]" string => "ultra_api_prod_public celluphone_prod epay_prod rainmaker_prod threeci_prod";

      "crones[UPDATE_DEALER_LOC][site]" string => "live-api.ultra.me";
      "crones[UPDATE_DEALER_LOC][cron_schedule]" string => "*/28 * * * *";

      "crones[CBS_ACT_LOG][site]" string => "live-api.ultra.me";
      "crones[CBS_ACT_LOG][cron_schedule]" string => "*/3 * * * *";

      "crones[CBS_TRANS_LOG][site]" string => "live-api.ultra.me";
      "crones[CBS_TRANS_LOG][cron_schedule]" string => "*/7 * * * *";

      "crones[activation_fixups][site]" string => "live-api.ultra.me";
      "crones[activation_fixups][cron_schedule]" string => "* */4 * * *";

      "crones[porting_crone][site]" string => "live-api.ultra.me";
      "crones[porting_crone][cron_schedule]" string => "*/3 5-22 * * *";

      "crones[ensure_crone][site]" string => "live-api.ultra.me";
      "crones[ensure_crone][cron_schedule]" string => "*/5 5-22 * * *";

      "crones[GLOBALSIM_CRONE][site]" string => "live-api.ultra.me";
      "crones[GLOBALSIM_CRONE][cron_schedule]" string => "*/15 * * * *";

      # "crones[reservation_crone][site]" string => "live-api.ultra.me";
      # "crones[reservation_crone][cron_schedule]" string => "* * * * *";

      "crones[reaper_crone][site]" string => "live-api.ultra.me";
      "crones[reaper_crone][cron_schedule]" string => "*/3 * * * *";

      "crones[open_transactions_reaper][site]" string => "live-api.ultra.me";
      "crones[open_transactions_reaper][cron_schedule]" string => "3 * * * *";

      "crones[open_actions_reaper][site]" string => "live-api.ultra.me";
      "crones[open_actions_reaper][cron_schedule]" string => "3 * * * *";

The same kinds of variables (`environments`, `resolved_environments`,
and `crones`) for the machine `web-01` this time.  Exactly like the
previous data, except in a different context.

    any::
      "mailto"         string => "tzz@hometowntelecom.com";
      "home"           string => "/home/ht";
      "wwwroot"        string => "$(home)/www";
      "configroot"     string => "$(g.home)/config";
      "checkout"       string => "$(home)/cm";
      "cmroot"         string => "$(checkout)";

Back to global (`any`) context.  These variables will be seen by all
machines and are in fact essential.  Learn them well so we won't have
to explain them again later.  They define the very basics of where we
deploy things, how they run, and where configurations are stored.

      "design_center_path" string => "$(home)/design-center";
      "cfsketch_home"  string => "$(design_center_path)/tools/cf-sketch";

      "policy_dir" string => dirname("$(this.promise_filename)");
      "fileroot" string => "$(g.policy_dir)/files";
      "pwfileroot" string => "/etc/httpd/credentials";

      "sshd_config[Protocol]"             string => "2";
      "sshd_config[X11Forwarding]"        string => "yes";
      "sshd_config[UseDNS]"               string => "no";
      "sshd_config[GSSAPIAuthentication]" string => "no";
      "sshd_config[PermitRootLogin]"      string => "yes";

The above are more global settings, this time more CFEngine-specific.
The `$(fileroot)` and `$(pwfileroot)` paths are important; that's
where we'll store configuration templates and password files
respectively.

```
  reports:
    managed.!home_checkout::
      "You don't have the CM HT repository (cm.git) checked out in $(g.checkout).";
}
```

Finally, we close the `g` bundle with a report that prints when the
machine is *managed* and does not have the `cm.git` checkout in
`$(checkout)`.

```
bundle agent gogo
{
  methods:
    managed.home_checkout::
      "gogo_base"      usebundle => base();
      "gogo_environs"  usebundle => environments(@(g.environments));
      "gogo_cfrun"     usebundle => cfrun();
      "gogo_users"     usebundle => users();

      "CF DC" usebundle => cfdc_vcs:mirror("dc_",
                                           "dc_",
                                           "/usr/bin/git",
                                           $(g.design_center_path),
                                           "https://github.com/cfengine/design-center.git",
                                           "master",
                                           "root",
                                           "022");

      "ssh" usebundle => cfdc_sshd:sshd_config("g.sshd_config");
```

The `gogo` bundle is the next thing to run in the global
*bundlesequence*.  It runs a list of methods, which are simply
function calls in practical terms.  The methods above are only run on
*managed* machines with the *home_checkout*.  The `base`,
`environments`, `cfrun`, and `users` bundles will be explained later.
The `mirror` and `sshd_config` bundles are loaded from external
libraries and implement mirroring of the external libraries and
maintenance of the SSH daemon configuration.

    managed.home_checkout.!quick::
      "gogo_packages"  usebundle => packages();

On *managed* machines with the *home_checkout*, when *quick* is not
defined, we install packages.

    managed.home_checkout::
      "gogo_rcfiles"   usebundle => rcfiles();
      "gogo_resolver"  usebundle => resolver();

On *managed* machines with the *home_checkout*, we run `rcfiles` and
`resolver` which will be explained later.

    managed.home_checkout.webserver::
      "gogo_webserver" usebundle => webserver();

On *managed* *webserver* machines with the *home_checkout*, we run the `webserver` bundle which does webserver-y things.

    managed.home_checkout::
      "gogo_etckeeper /etc" usebundle => etckeeper("/etc");
      "gogo_etckeeper configroot" usebundle => etckeeper("$(g.configroot)");

    any::
      "gogo_runner cron.d" usebundle => runner_cron_d();

Finally, again on *managed* machines with the *home_checkout* we snapshot the `/etc` and `$(configroot)` directories with `etckeeper`.

And then, on *any* machine, we install the `/etc/cron.d/runner` cron job.  So the `runner_cron_d` bundle is pretty important to the health of the CFEngine configuration management.

```
bundle agent environments(elist)
{
  vars:
    any::
      "mc_dev_prefix" string => "DEV/";
      "mc_prod_prefix" string => "PROD/";
```

We're in the `environments` bundle now.  It starts with variables for
*any* machine.  The `mc_*_prefix` variables are used by the PHP code
in front of any memcached keys to ensure that production and
development environment, even talking to the same memcached server,
will not see each other's entries.

      "dev_partners" string => "ePay ui internal test inventory portal provisioning celluphone rainmaker customercare api_public interactivecare";

      "web_repo" string => "https://cmviewer:bmb00@source.hometowntelecom.com:8843/scm/WEB/web.git";
      "ultra_repo" string => "https://cmviewer:bmb00@source.hometowntelecom.com:8843/scm/ULTRA/ultra.git";

      "uv_favicon" string => "$(g.fileroot)/uv-favicon.ico";
      "ultra_favicon" string => "$(g.fileroot)/ultra-favicon.ico";

      "ultra_cert_crt" string => "/etc/httpd/certs/ultra.me.crt";
      "ultra_cert_key" string => "/etc/httpd/certs/ultra.me.key";

      "uvnv_cert_crt" string => "/etc/httpd/certs/uvnv.com.crt";
      "uvnv_cert_key" string => "/etc/httpd/certs/uvnv.com.key";

      "htt_cert_crt" string => "/etc/httpd/certs/hometowntelecom.com.crt";
      "htt_cert_key" string => "/etc/httpd/certs/hometowntelecom.com.key";

We define more globals: the list of partners to be enabled for
development environments in `dev_partners`; the URLs (with username
and password, which you may want to change to SSH authentication) to
the `web.git` and `ultra.git` repositories; the favicons for our
websites; the SSL certificate and key locations.  Simple stuff.

     !aspider_gateway::
      "cred[globals][aspider/soapy/wsdl]" string => "runners/soapy/MVNOWSAPI-20121012/MVNOWSAPIServiceLocal.wsdl";
    aspider_gateway::
      "cred[globals][aspider/soapy/wsdl]" string => "runners/soapy/MVNOWSAPI-20121012/MVNOWSAPIService.wsdl";

    any::
On *any* machine, the `services` array gets two entries, which are
positive (must have) and negative (must not have) services for *ALL
PLANS*.  These are in a single string, separated by semicolons.

      "services[TWENTY_NINE/gateway/ensure/1]" string => "Block at 50 MB (PayGo)";
      "services[TWENTY_NINE/gateway/ensure/0]" string => "Throttle at 1 GB (Bucketed);Permanent Throttle 128kbps;Throttle at 2 GB (Bucketed)";

      "services[THIRTY_NINE/gateway/ensure/1]" string => "Permanent Throttle 128kbps";
      "services[THIRTY_NINE/gateway/ensure/0]" string => "Block at 50 MB (PayGo);Throttle at 1 GB (Bucketed);Throttle at 2 GB (Bucketed)";

      "services[FORTY_NINE/gateway/ensure/1]" string => "Throttle at 1 GB (Bucketed)";
      "services[FORTY_NINE/gateway/ensure/0]" string => "Block at 50 MB (PayGo);Permanent Throttle 128kbps;Throttle at 2 GB (Bucketed)";

      "services[FIFTY_NINE/gateway/ensure/1]" string => "Throttle at 2 GB (Bucketed)";
      "services[FIFTY_NINE/gateway/ensure/0]" string => "Block at 50 MB (PayGo);Permanent Throttle 128kbps;Throttle at 1 GB (Bucketed)";

Next, for each plan (from `TWENTY_NINE` to `FIFTY_NINE` for now) we
define the positive and negative services.  This is very important for
the gateway's `Service/Ensure` operation.

      "cred[messaging][sms/silverstreet/url]" string => "http://gateway.silverstreet.nl/send.php";
      "cred[messaging][sms/silverstreet/user]" string => "htt";
      "cred[messaging][sms/silverstreet/password]" string => "jgh85y";

      "cred[messaging][sms/3ci/user]" string => "sFbzHEzcwUsUGttGU0QRaw==";
      "cred[messaging][sms/3ci/password]" string => "RbV6hKO6xJDSL4Biq9CWhFn0NX352rns4k48p9lqjsU=";
      "cred[messaging][sms/3ci/clientid]" string => "2285";
      "cred[messaging][sms/3ci/shortcode]" string => "35047"; #Temporary

      "cred[messaging][numberportability/u]" string => "rizwank";
      "cred[messaging][numberportability/p]" string => "cheeky monkey robot";

We are still working with *any* machine.  We create the `messaging` credentials and add some entries for various messaging needs.

      "cred[tel_data_api_accounts][shipwire/env]" string => "Production";
      "cred[tel_data_api_accounts][ultra/login/site]" string => "api.ultra.me";
      "cred[tel_data_api_accounts][ultra/rlogin/site]" string => "services.ultra.me";

Now we are working with the `tel_data_api_accounts` credentials.
These are for environments that need the `TEL_DATA` database--mostly
legacy stuff.  Here we define second-level keys for the Shipwire
environment name, for the gateway location (a space-separated
HOST:PORT list), and UI-specific pointers to the login and service
sites.

      "cred[tel_data_api_accounts][db/name]"  string => "TEL_DATA";
      "cred[tel_data_api_accounts][db/user]"  string => "api_accounts";
      "cred[tel_data_api_accounts][db/pfile]" string => "$(g.pwfileroot)/api_accounts";
      "cred[tel_data_api_accounts][db/host]" string => "db.hometowntelecom.com";
      "cred[tel_data_api_accounts][db/port]" string => "";

The `db/*` portion of the `tel_data_api_accounts` credentials simply
defines how the datbase can be reached.

      "cred[tel_data_api_accounts][constants/account_group_id]" string => "70";
      "cred[tel_data_api_accounts][constants/signup_cos_id]" string => "94916";
      "cred[tel_data_api_accounts][constants/creation_date]" string => "GETDATE()";

Some constants for the `tel_data_api_accounts` credentials... nothing
exciting, except perhaps the `constants/creation_date` which is
defined to be the local date here, but could be `GETUTCDATE()` or even
something more complex elsewhere.

      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_1000/cos_id]"  string => "49361";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_1000/name]"    string => "IndiaLD USA Monthly 1000";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_1500/cos_id]"  string => "49362";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_1500/name]"    string => "IndiaLD USA Monthly 1500";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_2000/cos_id]"  string => "49363";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_2000/name]"    string => "IndiaLD USA Monthly 2000";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_500/cos_id]"   string => "49360";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_500/name]"     string => "IndiaLD USA Monthly 500";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_250/cos_id]"   string => "88261";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_USA_MONTHLY_250/name]"     string => "IndiaLD USA Monthly 250";

      # "IndiaLD Monthly"
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_1000/cos_id]"      string => "12069";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_1000/name]"        string => "IndiaLD Monthly 1000";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_2000/cos_id]"      string => "12070";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_2000/name]"        string => "IndiaLD Monthly 2000";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_3000/cos_id]"      string => "12071";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_3000/name]"        string => "IndiaLD Monthly 3000";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_500/cos_id]"       string => "16727";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_500/name]"         string => "IndiaLD Monthly 500";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_1500/cos_id]"      string => "16728";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_1500/name]"        string => "IndiaLD Monthly 1500";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_250/cos_id]"       string => "25720";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_250/name]"         string => "IndiaLD Monthly 250";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_5000/cos_id]"      string => "25786";
      "cred[tel_data_api_accounts][plans/ILD/INDIALD_MONTHLY_5000/name]"        string => "IndiaLD Monthly 5000";

      # "India LD Monthly"
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_250/cos_id]"      string => "27373";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_250/name]"        string => "India LD Monthly 250";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_500/cos_id]"      string => "27374";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_500/name]"        string => "India LD Monthly 500";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_1000/cos_id]"     string => "27375";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_1000/name]"       string => "India LD Monthly 1000";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_1500/cos_id]"     string => "27376";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_1500/name]"       string => "India LD Monthly 1500";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_2000/cos_id]"     string => "27377";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_2000/name]"       string => "India LD Monthly 2000";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_3000/cos_id]"     string => "27378";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_3000/name]"       string => "India LD Monthly 3000";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_5000/cos_id]"     string => "29133";
      "cred[tel_data_api_accounts][plans/ILD/INDIA_LD_MONTHLY_5000/name]"       string => "India LD Monthly 5000";

      # not monthly
      "cred[tel_data_api_accounts][plans/ILD/1_5_CALLING_CARD/cos_id]"          string => "29955";
      "cred[tel_data_api_accounts][plans/ILD/1_5_CALLING_CARD/name]"            string => "1.5 Calling Card";
      "cred[tel_data_api_accounts][plans/ILD/1_5_CALLING_CARD_T/cos_id]"        string => "30456";
      "cred[tel_data_api_accounts][plans/ILD/1_5_CALLING_CARD_T/name]"          string => "1.5 Calling Card T";
      "cred[tel_data_api_accounts][plans/ILD/QUARTER_CALLS/cos_id]"             string => "53788";
      "cred[tel_data_api_accounts][plans/ILD/QUARTER_CALLS/name]"               string => "Quarter Calls";
      "cred[tel_data_api_accounts][plans/ILD/QUARTER_CALLS_T/cos_id]"           string => "53789";
      "cred[tel_data_api_accounts][plans/ILD/QUARTER_CALLS_T/name]"             string => "Quarter Calls T";
      "cred[tel_data_api_accounts][plans/ILD/PAYG_MINUTES/cos_id]"              string => "69143";
      "cred[tel_data_api_accounts][plans/ILD/PAYG_MINUTES/name]"                string => "PAYG Minutes";
      "cred[tel_data_api_accounts][plans/ILD/PAYG_MINUTES_T/cos_id]"            string => "69146";
      "cred[tel_data_api_accounts][plans/ILD/PAYG_MINUTES_T/name]"              string => "PAYG Minutes T";
      "cred[tel_data_api_accounts][plans/ILD/UV_CARD/cos_id]"                   string => "100729";
      "cred[tel_data_api_accounts][plans/ILD/UV_CARD/name]"                     string => "UV Card";
      "cred[tel_data_api_accounts][plans/ILD/NO_PLAN/cos_id]"                   string => "94916";
      "cred[tel_data_api_accounts][plans/ILD/NO_PLAN/name]"                     string => "No Plan";

Some truly boring arrays for the `tel_data_api_accounts` credentials
follow.  They define plans and their parameters.  For the
`tel_data_api_accounts` credentials specifically they are not too
important, because environments that use those credentials are not
using our new code, but the plan (as of the writing of this) is that
they will.

      "cred[tel_data_transferto][db/name]"  string => "TEL_DATA";
      "cred[tel_data_transferto][db/user]"  string => "transferto";
      "cred[tel_data_transferto][db/pfile]" string => "$(g.pwfileroot)/transferto";
      "cred[tel_data_transferto][db/host]" string => "db.hometowntelecom.com";
      "cred[tel_data_transferto][db/port]" string => "";

A minimal set of credentials called `tel_data_transferto` follow.
These are for the legacy TransferTo messaging daemon, and as you can
see only define database connectivity.

      "cred[ultra_develop_tel_api_accounts][shipwire/env]" string => "Test";
      "cred[ultra_develop_tel_api_accounts][ultra/login/site]" string => "seanapi-dev.ultra.me";
      "cred[ultra_develop_tel_api_accounts][ultra/rlogin/site]" string => "devlive.ultra.me";
      "cred[ultra_develop_tel_api_accounts][ultra/disable/provisioning]" string => "0";
      "cred[ultra_develop_tel_api_accounts][ultra/disable/activation]" string => "0";
      "cred[ultra_develop_tel_api_accounts][ultra/disable/porting]" string => "0";
      "cred[ultra_develop_tel_api_accounts][ultra/disable/gateway/porting]" string => "0";

The `ultra_develop_tel_api_accounts` credentials are for development
environments.  You've seen most of the keys already.  All the
`ultra/disable/*` keys need to be set to `1` to have an effect.  Keep
in mind that the reason why they are not called `ultra/enable/*` is
that we want their *absence* to be the same as enabling the feature;
in other words, we want things to work by default and get disabled
only explicitly.  Please try to keep that philosophy, because the
alternative (to enable things explicitly) will inevitably cause
problems when you forget to enable feature 27 out of 122.

Put another way, it's better to have everything working by default.

      "cred[ultra_develop_tel_api_accounts][db/name]"  string => "ULTRA_DEVELOP_TEL";
      "cred[ultra_develop_tel_api_accounts][db/user]"  string => "api_accounts";
      "cred[ultra_develop_tel_api_accounts][db/pfile]" string => "$(g.pwfileroot)/api_accounts";
      "cred[ultra_develop_tel_api_accounts][db/host]" string => "db.hometowntelecom.com";
      "cred[ultra_develop_tel_api_accounts][db/port]" string => "";

The database connectivity for the `ultra_develop_tel_api_accounts`
credentials.

      "cred[ultra_develop_tel_api_accounts][constants/account_group_id]" string => "70";
      "cred[ultra_develop_tel_api_accounts][constants/signup_cos_id]" string => "94916";
      "cred[ultra_develop_tel_api_accounts][constants/creation_date]" string => "GETUTCDATE()";

More for the `ultra_develop_tel_api_accounts` credentials.  Note the
creation date is in UTC here.  PROGRESS!!!  STANDARDS!!!

(If you look through the legacy `api.php` and `db.php` code, you'll
see much pain regarding the UTC vs. local time conversions.)

      "cred[ultra_develop_tel_api_accounts][webcc/always_fail/cc_number]" string => "!BAE80CA04EC1124F56B6C5754EE2612A2503";
      "cred[ultra_develop_tel_api_accounts][webcc/always_fail/cc_exp]" string => "0213";
      "cred[ultra_develop_tel_api_accounts][webcc/always_fail/cc_ccv]" string => "786";
      "cred[ultra_develop_tel_api_accounts][webcc/always_succeed/cc_number]" string => "!AE4F8C5FC46B2C220AB29BC6760DF2968947";
      "cred[ultra_develop_tel_api_accounts][webcc/always_succeed/cc_exp]" string => "0213";
      "cred[ultra_develop_tel_api_accounts][webcc/always_succeed/cc_ccv]" string => "798";

What fun!  We add some `webcc/*` special CC numbers to the
`ultra_develop_tel_api_accounts` credentials.

      # Plan services
      "cred[ultra_develop_tel_api_accounts][gateway/ensure/1]" string => "$(services[gateway/ensure/1])";
      "cred[ultra_develop_tel_api_accounts][gateway/ensure/0]" string => "$(services[gateway/ensure/0])";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/gateway/ensure/1]" string => "$(services[TWENTY_NINE/gateway/ensure/1])";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/gateway/ensure/0]" string => "$(services[TWENTY_NINE/gateway/ensure/0])";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/gateway/ensure/1]" string => "$(services[THIRTY_NINE/gateway/ensure/1])";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/gateway/ensure/0]" string => "$(services[THIRTY_NINE/gateway/ensure/0])";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/gateway/ensure/1]" string => "$(services[FORTY_NINE/gateway/ensure/1])";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/gateway/ensure/0]" string => "$(services[FORTY_NINE/gateway/ensure/0])";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/gateway/ensure/1]" string => "$(services[FIFTY_NINE/gateway/ensure/1])";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/gateway/ensure/0]" string => "$(services[FIFTY_NINE/gateway/ensure/0])";

Specifically for the `ultra_develop_tel_api_accounts` credentials, we
use the global `services` array so every set of credentials can have
the same services.

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/cos_id]" string => "98274";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/name]" string => "Ultra $29";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/aka]" string => "L29";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/cost]" string => "2900";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/talk_minutes]" string => "inf";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/intl_minutes]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/monthly]" string => "1";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/has_calling_card_balance]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/rechargeable]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/taxable]" string => "1";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/TWENTY_NINE/speedy/user_cap]"       string => "500";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/cos_id]" string => "98275";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/name]" string => "Ultra $39";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/aka]" string => "L39";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/cost]" string => "3900";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/talk_minutes]" string => "inf";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/intl_minutes]" string => "500";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/monthly]" string => "1";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/has_calling_card_balance]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/rechargeable]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/taxable]" string => "1";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/THIRTY_NINE/speedy/user_cap]"       string => "500";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/cos_id]" string => "98276";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/name]" string => "Ultra $49";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/aka]" string => "L49";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/cost]" string => "4900";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/talk_minutes]" string => "inf";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/intl_minutes]" string => "2000";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/monthly]" string => "1";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/has_calling_card_balance]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/rechargeable]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/taxable]" string => "1";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FORTY_NINE/speedy/user_cap]"        string => "1000";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/cos_id]" string => "98279";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/name]" string => "Ultra $59";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/aka]" string => "L59";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/cost]" string => "5900";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/talk_minutes]" string => "inf";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/intl_minutes]" string => "2000";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/monthly]" string => "1";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/has_calling_card_balance]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/rechargeable]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/taxable]" string => "1";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/FIFTY_NINE/speedy/user_cap]"        string => "1000";

      "cred[ultra_develop_tel_api_accounts][plans/Ultra/STANDBY/cos_id]" string => "98277";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/STANDBY/name]" string => "Ultra SIM Standby";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/STANDBY/cost]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/STANDBY/talk_minutes]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/STANDBY/intl_minutes]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/STANDBY/monthly]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/STANDBY/has_calling_card_balance]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/STANDBY/rechargeable]" string => "0";
      "cred[ultra_develop_tel_api_accounts][plans/Ultra/STANDBY/taxable]" string => "1";

Plan definitions for the `ultra_develop_tel_api_accounts` credentials.
Wouldn't it be nice if CFEngine supported JSON?  Yes?  I opened that
feature request a while ago and even sent them the C code to implement
it...  So yeah, I know, I know.

      "cred[ultra_develop_tel_transferto][db/name]"  string => "ULTRA_DEVELOP_TEL";
      "cred[ultra_develop_tel_transferto][db/user]"  string => "transferto";
      "cred[ultra_develop_tel_transferto][db/pfile]" string => "$(g.pwfileroot)/transferto";
      "cred[ultra_develop_tel_transferto][db/host]" string => "db.hometowntelecom.com";
      "cred[ultra_develop_tel_transferto][db/port]" string => "";

Another minimal set of credentials, just for TransferTo in development.

      # db/ccv can be BA, BA_TEST, "", or something else (CCV Server, CCV Server in test mode, none, new Credit Card server)

      "cred[ultra_data_ultra_api][shipwire/env]" string => "Production";
      "cred[ultra_data_ultra_api][ultra/login/site]" string => "api.ultra.me";
      "cred[ultra_data_ultra_api][ultra/rlogin/site]" string => "services.ultra.me";
      "cred[ultra_data_ultra_api][ultra/disable/provisioning]" string => "0";
      "cred[ultra_data_ultra_api][ultra/disable/activation]" string => "0";
      "cred[ultra_data_ultra_api][ultra/disable/porting]" string => "0";
      "cred[ultra_data_ultra_api][ultra/disable/gateway/porting]" string => "0";

      "cred[ultra_data_ultra_api][db/name]"  string => "ULTRA_DATA";
      "cred[ultra_data_ultra_api][db/user]"  string => "ultra_api";
      "cred[ultra_data_ultra_api][db/pfile]" string => "$(g.pwfileroot)/ultra_api";
      "cred[ultra_data_ultra_api][db/host]" string => "db.hometowntelecom.com";
      "cred[ultra_data_ultra_api][db/port]" string => "";

      "cred[ultra_data_ultra_api][messaging/numberportabilitylookup/user]" string => "rizwank";
      "cred[ultra_data_ultra_api][messaging/numberportabilitylookup/password]" string => "cheeky monkey robot";

      "cred[ultra_data_ultra_api][constants/account_group_id]" string => "3";
      "cred[ultra_data_ultra_api][constants/signup_cos_id]" string => "1";
      "cred[ultra_data_ultra_api][constants/creation_date]" string => "GETUTCDATE()";

      "cred[ultra_data_ultra_api][webcc/always_fail/cc_number]" string => "!79C2A044D9619B5621DFB9E21357441F8470";
      "cred[ultra_data_ultra_api][webcc/always_fail/cc_exp]" string => "0613";
      "cred[ultra_data_ultra_api][webcc/always_fail/cc_ccv]" string => "786";
      "cred[ultra_data_ultra_api][webcc/always_succeed/cc_number]" string => "!6F14DDBE2D8A0170AEFE9B724218BDD33550";
      "cred[ultra_data_ultra_api][webcc/always_succeed/cc_exp]" string => "0613";
      "cred[ultra_data_ultra_api][webcc/always_succeed/cc_ccv]" string => "798";

      # Plan services
      "cred[ultra_data_ultra_api][gateway/ensure/1]" string => "$(services[gateway/ensure/1])";
      "cred[ultra_data_ultra_api][gateway/ensure/0]" string => "$(services[gateway/ensure/0])";

      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/gateway/ensure/1]" string => "$(services[TWENTY_NINE/gateway/ensure/1])";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/gateway/ensure/0]" string => "$(services[TWENTY_NINE/gateway/ensure/0])";

      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/gateway/ensure/1]" string => "$(services[THIRTY_NINE/gateway/ensure/1])";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/gateway/ensure/0]" string => "$(services[THIRTY_NINE/gateway/ensure/0])";

      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/gateway/ensure/1]" string => "$(services[FORTY_NINE/gateway/ensure/1])";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/gateway/ensure/0]" string => "$(services[FORTY_NINE/gateway/ensure/0])";

      # Ultra Plans

      "cred[ultra_data_ultra_api][plans/Ultra/STANDBY/cos_id]" string => "1";
      "cred[ultra_data_ultra_api][plans/Ultra/STANDBY/name]" string => "Ultra SIM Standby";
      "cred[ultra_data_ultra_api][plans/Ultra/STANDBY/cost]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/STANDBY/talk_minutes]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/STANDBY/intl_minutes]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/STANDBY/monthly]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/STANDBY/has_calling_card_balance]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/STANDBY/rechargeable]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/STANDBY/taxable]" string => "1";

      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/cos_id]" string => "2";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/name]" string => "Ultra $29";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/aka]" string => "L29";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/cost]" string => "2900";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/talk_minutes]" string => "inf";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/intl_minutes]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/monthly]" string => "1";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/has_calling_card_balance]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/rechargeable]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/taxable]" string => "1";

      "cred[ultra_data_ultra_api][plans/Ultra/TWENTY_NINE/speedy/user_cap]"       string => "500";

      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/cos_id]" string => "3";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/name]" string => "Ultra $39";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/aka]" string => "L39";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/cost]" string => "3900";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/talk_minutes]" string => "inf";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/intl_minutes]" string => "500";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/monthly]" string => "1";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/has_calling_card_balance]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/rechargeable]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/taxable]" string => "1";

      "cred[ultra_data_ultra_api][plans/Ultra/THIRTY_NINE/speedy/user_cap]"       string => "500";

      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/cos_id]" string => "4";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/name]" string => "Ultra $49";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/aka]" string => "L49";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/cost]" string => "4900";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/talk_minutes]" string => "inf";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/intl_minutes]" string => "2000";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/monthly]" string => "1";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/has_calling_card_balance]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/rechargeable]" string => "0";
      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/taxable]" string => "1";

      "cred[ultra_data_ultra_api][plans/Ultra/FORTY_NINE/speedy/user_cap]"       string => "1000";

The `ultra_data_ultra_api` above are just like the
`ultra_develop_tel_api_accounts` credentials but with different data.
Sometimes.  It's a lot of configuration, basically, which used to live
in code all over the place... here it's all in one place.

      # production gateway credentials

      "cred[prod_gateway][ultra/disable/provisioning]" string => "0";
      "cred[prod_gateway][ultra/disable/activation]" string => "0";
      "cred[prod_gateway][ultra/disable/porting]" string => "0";
      "cred[prod_gateway][ultra/disable/preverify]" string => "0";
      "cred[prod_gateway][ultra/disable/postverify]" string => "0";
      "cred[prod_gateway][ultra/disable/gateway/porting]" string => "0";

      "cred[prod_gateway][db/name]"  string => "ULTRA_ASPIDER";
      "cred[prod_gateway][db/user]"  string => "api_accounts";
      "cred[prod_gateway][db/pfile]" string => "$(g.pwfileroot)/api_accounts";
      "cred[prod_gateway][db/host]" string => "db.hometowntelecom.com";
      "cred[prod_gateway][db/port]" string => "";

      # Plan services
      "cred[prod_gateway][gateway/ensure/1]" string => "$(services[gateway/ensure/1])";
      "cred[prod_gateway][gateway/ensure/0]" string => "$(services[gateway/ensure/0])";

      "cred[prod_gateway][plans/Ultra/TWENTY_NINE/gateway/ensure/1]" string => "$(services[TWENTY_NINE/gateway/ensure/1])";
      "cred[prod_gateway][plans/Ultra/TWENTY_NINE/gateway/ensure/0]" string => "$(services[TWENTY_NINE/gateway/ensure/0])";

      "cred[prod_gateway][plans/Ultra/THIRTY_NINE/gateway/ensure/1]" string => "$(services[THIRTY_NINE/gateway/ensure/1])";
      "cred[prod_gateway][plans/Ultra/THIRTY_NINE/gateway/ensure/0]" string => "$(services[THIRTY_NINE/gateway/ensure/0])";

      "cred[prod_gateway][plans/Ultra/FORTY_NINE/gateway/ensure/1]" string => "$(services[FORTY_NINE/gateway/ensure/1])";
      "cred[prod_gateway][plans/Ultra/FORTY_NINE/gateway/ensure/0]" string => "$(services[FORTY_NINE/gateway/ensure/0])";

      # sean_dev environment

      "e[sean_dev][git/repo]"     string => "$(web_repo)";
      "e[sean_dev][git/branch]"   string => "master";
      "e[sean_dev][git/deploy]"   string => "$(g.wwwroot)/sean_dev/weblive";
      "e[sean_dev][git/schedule]" string => "!any"; # disable deployments
      "e[sean_dev][git/chown/o]"  string => "sean";
      "e[sean_dev][git/chown/g]"  string => "apache";
      "e[sean_dev][git/runas]"    string => "sean";

This is a typical environment, which will be deployed in development.
It's Sean's development environment, so we cleverly named it
`sean_dev`.

The environment definitions live in the `e` array.  That array name
has just one letter because it's very heavily used.

Here we define `git/*` parameters, which indicate from where, and how
often the environment will be deployed.  This one in particular will
never be automatically deployed (it runs on the negation of *any*),
unless the directory does not exist in which case we do the initial
clone to maintain idempotency.  We also specify the `runas` of the
`git pull` or `git clone` and the owner and group of the deployment
directory.

      "e[sean_dev][credentials]"  string => "tel_data_api_accounts messaging globals";

Aha!  All the credential definitions above come into play here.  This
tells the `sean_dev` environment, or rather programs running in it,
what credentials are available to them, in a space-separated list.
      
      "e[sean_dev][db/ccv]"       string => "BA";
      "e[sean_dev][db/TSIP]"      string => "3.9";
      "e[sean_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[sean_dev][mc/server]"    string => "localhost";
      "e[sean_dev][mc/port]"      string => "11211";
      "e[sean_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[sean_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[sean_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      
We define for this environment the memcached server, port, and prefix,
We also set the AWS key and password file.  The `db/ccv` and `db/TSIP`
options are not actively used as far as I know.  The AWS options are
currently used only for the UV card program, which is now
discontinued.
      
      "e[sean_dev][security/env]" string => "internal_dev";

      "e[sean_dev][partner/mfunds/env]" string => "mfunds_prod";
      "e[sean_dev][partner/api/allow]" string => "$(dev_partners)";

Security environments are defined later, much like credentials, but
only one can be used.  Here we say `sean_dev` is in the `internal_dev`
security environment.

Ignore anything with `mfunds`, it's relevant only for the UV card.

The `partner/api/allow` space-separated list of partners is crucial.
It states what partners can access the `partner.php` API.

      "e[sean_dev][www/sites]"    string => "devlive.uvnv.com";

Finally, we list what web sites are supported by the `sean_dev`
environment in a space-separated list.

      "www[devlive.uvnv.com][enabled]"                string => "true";
      "www[devlive.uvnv.com][type]"                   string => "ssl";
      "www[devlive.uvnv.com][vhost]"                  string => "70.39.130.41:443";
      "www[devlive.uvnv.com][name]"                   string => "devlive.uvnv.com:443";
      "www[devlive.uvnv.com][aliases]"                string => "devlive.uvcard.com:443";
      
Websites are defined in the `www` array.  The top key is the site
name, then we have several properties like `name` and `aliases` which
are used by the `webserver` bundle.
      
      "www[devlive.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[devlive.uvnv.com][ErrorLog]"               string => "logs/devlive_ssl_error_log";
      "www[devlive.uvnv.com][TransferLog]"            string => "logs/devlive_ssl_access_log";
      "www[devlive.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[devlive.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[devlive.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[devlive.uvnv.com][DocumentRoot]"           string => "$(e[sean_dev][git/deploy])";
      "www[devlive.uvnv.com][overrides]"              string => "All";
      "www[devlive.uvnv.com][extras]"                 string => "";
      "www[devlive.uvnv.com][url_aliases]"            string => "";

This is an unremarkable site with little of note.

Note that the `DocumentRoot` refers to the parent environment's
`git/deploy` directory.  That was based on our experience and allows
web sites to be more than mere Git deployments.

      # ultra_api_dev environment

      "e[ultra_api_dev][git/repo]"     string => "$(web_repo)";
      "e[ultra_api_dev][git/branch]"   string => "master";
      "e[ultra_api_dev][git/deploy]"   string => "$(g.wwwroot)/ultra_api_dev/web";
      "e[ultra_api_dev][git/schedule]" string => "!any"; # disable deployments
      "e[ultra_api_dev][git/chown/o]"  string => "sean";
      "e[ultra_api_dev][git/chown/g]"  string => "apache";
      "e[ultra_api_dev][git/runas]"    string => "sean";

      "e[ultra_api_dev][credentials]"  string => "ultra_develop_tel_api_accounts messaging globals";
      "e[ultra_api_dev][db/ccv]"       string => "BA";
      "e[ultra_api_dev][db/TSIP]"      string => "3.11";
      "e[ultra_api_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[ultra_api_dev][mc/server]"    string => "localhost";
      "e[ultra_api_dev][mc/port]"      string => "11211";
      "e[ultra_api_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[ultra_api_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[ultra_api_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[ultra_api_dev][security/env]" string => "public";

      "e[ultra_api_dev][partner/mfunds/env]" string => "mfunds_prod";
      "e[ultra_api_dev][partner/api/allow]" string => "api_public portal";

      "e[ultra_api_dev][www/sites]"    string => "api-dev.ultra.me";

      "www[api-dev.ultra.me][enabled]"                string => "true";
      "www[api-dev.ultra.me][type]"                   string => "ssl";
      "www[api-dev.ultra.me][vhost]"                  string => "70.39.130.41:443";
      "www[api-dev.ultra.me][name]"                   string => "api-dev.ultra.me:443";
      "www[api-dev.ultra.me][aliases]"                string => "";
      "www[api-dev.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[api-dev.ultra.me][ErrorLog]"               string => "logs/ultra_api_dev_ssl_error_log";
      "www[api-dev.ultra.me][TransferLog]"            string => "logs/ultra_api_dev_ssl_access_log";
      "www[api-dev.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[api-dev.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[api-dev.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[api-dev.ultra.me][DocumentRoot]"           string => "$(e[ultra_api_dev][git/deploy])";
      "www[api-dev.ultra.me][overrides]"              string => "FileInfo Options";
      "www[api-dev.ultra.me][extras]"                 string => "";
      "www[api-dev.ultra.me][url_aliases]"            string => "";

Nothing of note here, except that `api-dev.ultra.me` is not allowed to
override `All` settings, but only `FileInfo Options`.

      # sean_api_dev environment

      "e[seanapi_dev][git/repo]"     string => "$(web_repo)";
      "e[seanapi_dev][git/branch]"   string => "master";
      "e[seanapi_dev][git/deploy]"   string => "$(g.wwwroot)/sean_api_dev/web";
      "e[seanapi_dev][git/schedule]" string => "!any"; # disable deployments
      "e[seanapi_dev][git/chown/o]"  string => "sean";
      "e[seanapi_dev][git/chown/g]"  string => "apache";
      "e[seanapi_dev][git/runas]"    string => "sean";

      "e[seanapi_dev][credentials]"  string => "ultra_develop_tel_api_accounts messaging globals";
      "e[seanapi_dev][db/ccv]"       string => "BA";
      "e[seanapi_dev][db/TSIP]"      string => "3.11";
      "e[seanapi_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[seanapi_dev][mc/server]"    string => "localhost";
      "e[seanapi_dev][mc/port]"      string => "11211";
      "e[seanapi_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[seanapi_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[seanapi_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      # "e[seanapi_dev][security/env]" string => "public_logging";
      "e[seanapi_dev][security/env]" string => "internal_dev";

      "e[seanapi_dev][partner/mfunds/env]" string => "mfunds_prod";
      "e[seanapi_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[seanapi_dev][www/sites]"    string => "seanapi-dev.ultra.me";

      "www[seanapi-dev.ultra.me][enabled]"                string => "true";
      "www[seanapi-dev.ultra.me][type]"                   string => "ssl";
      "www[seanapi-dev.ultra.me][vhost]"                  string => "70.39.130.41:443";
      "www[seanapi-dev.ultra.me][name]"                   string => "seanapi-dev.ultra.me:443";
      "www[seanapi-dev.ultra.me][aliases]"                string => "";
      "www[seanapi-dev.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[seanapi-dev.ultra.me][ErrorLog]"               string => "logs/seanapi_dev_ssl_error_log";
      "www[seanapi-dev.ultra.me][TransferLog]"            string => "logs/seanapi_dev_ssl_access_log";
      "www[seanapi-dev.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[seanapi-dev.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[seanapi-dev.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[seanapi-dev.ultra.me][DocumentRoot]"           string => "$(e[seanapi_dev][git/deploy])";
      "www[seanapi-dev.ultra.me][overrides]"              string => "FileInfo Options";
      "www[seanapi-dev.ultra.me][extras]"                 string => "";
      "www[seanapi-dev.ultra.me][url_aliases]"            string => "";

More of the same.

      # ultra_ui_dev environment

      "e[ultra_ui_dev][git/repo]"            string => "$(ultra_repo)";
      "e[ultra_ui_dev][git/branch]"          string => "master";
      "e[ultra_ui_dev][git/deploy]"          string => "$(g.wwwroot)/ultra_ui_dev/ultra";
      "e[ultra_ui_dev][git/schedule]"        string => "!any"; # disable deployments
      "e[ultra_ui_dev][git/chown/o]"         string => "sean";
      "e[ultra_ui_dev][git/chown/g]"         string => "apache";
      "e[ultra_ui_dev][git/runas]"           string => "sean";
      "e[ultra_ui_dev][service_env]"         string => "ultra_api_dev";

      "e[ultra_ui_dev][credentials]"         string => "ultra_develop_tel_api_accounts messaging globals";
      "e[ultra_ui_dev][db/ccv]"              string => "BA";
      "e[ultra_ui_dev][db/TSIP]"             string => "3.11";
      "e[ultra_ui_dev][mc/prefix]"           string => "$(mc_dev_prefix)";
      "e[ultra_ui_dev][mc/server]"           string => "localhost";
      "e[ultra_ui_dev][mc/port]"             string => "11211";
      "e[ultra_ui_dev][aws/key]"             string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[ultra_ui_dev][aws/pfile]"           string => "$(g.pwfileroot)/aws_uv_secret";
      "e[ultra_ui_dev][aws/uvbucket]"        string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[ultra_ui_dev][security/env]"        string => "internal_dev";
      "e[ultra_ui_dev][ultra/smarty/key]"     string => "3108359955169830372";
      "e[ultra_ui_dev][ultra/api/sites]"     string => "$(e[seanapi_dev][www/sites])";

      "e[ultra_ui_dev][partner/mfunds/env]"  string => "mfunds_prod";
      "e[ultra_ui_dev][partner/api/allow]"   string => "";

      "e[ultra_ui_dev][www/sites]"           string => "devlive.ultra.me";

This is a repository for UI development so it uses the `ultra.git`
repository.

      "www[devlive.ultra.me][enabled]"                string => "true";
      "www[devlive.ultra.me][type]"                   string => "ssl";
      "www[devlive.ultra.me][vhost]"                  string => "70.39.130.41:443";
      "www[devlive.ultra.me][name]"                   string => "devlive.ultra.me:443";
      "www[devlive.ultra.me][aliases]"                string => "";
      "www[devlive.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[devlive.ultra.me][ErrorLog]"               string => "logs/ultra_ui_dev_ssl_error_log";
      "www[devlive.ultra.me][TransferLog]"            string => "logs/ultra_ui_dev_ssl_access_log";
      "www[devlive.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[devlive.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[devlive.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[devlive.ultra.me][DocumentRoot]"           string => "$(e[ultra_ui_dev][git/deploy])";
      "www[devlive.ultra.me][overrides]"              string => "FileInfo Options";
      "www[devlive.ultra.me][extras]"                 string => "";
      "www[devlive.ultra.me][url_aliases]"            string => "
Alias /logs/tsa.log /var/tmp/logs-ultra_ui_dev/tsa.log
";

      # ultra_api_prod_public environment

      "e[ultra_api_prod_public][git/repo]"     string => "$(web_repo)";
      "e[ultra_api_prod_public][git/branch]"   string => "ultra-prod";
      "e[ultra_api_prod_public][git/deploy]"   string => "$(g.wwwroot)/ultra_api_prod_public/web";
      "e[ultra_api_prod_public][git/schedule]" string => "any"; # always deploy
      
This is a production API environment coming from the `ultra-prod` branch of `web.git` and always (every 5 minutes, currently) deployed.
      
      "e[ultra_api_prod_public][git/chown/o]"  string => "apache";
      "e[ultra_api_prod_public][git/chown/g]"  string => "apache";
      "e[ultra_api_prod_public][git/runas]"    string => "apache";

      "e[ultra_api_prod_public][credentials]"  string => "ultra_data_ultra_api messaging globals";
      "e[ultra_api_prod_public][db/ccv]"       string => "BA";
      "e[ultra_api_prod_public][db/TSIP]"      string => "3.11";
      "e[ultra_api_prod_public][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[ultra_api_prod_public][mc/server]"    string => "localhost";
      "e[ultra_api_prod_public][mc/port]"      string => "11211";
      "e[ultra_api_prod_public][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[ultra_api_prod_public][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[ultra_api_prod_public][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[ultra_api_prod_public][security/env]" string => "public";

This environment used the `public` security environment.  It will be
open to the public.

      "e[ultra_api_prod_public][partner/mfunds/env]" string => "mfunds_prod";
      "e[ultra_api_prod_public][partner/api/allow]" string => "api_public portal";

      "e[ultra_api_prod_public][www/sites]"    string => "api.ultra.me";

      "www[api.ultra.me][enabled]"                string => "true";
      "www[api.ultra.me][type]"                   string => "ssl";
      "www[api.ultra.me][vhost]"                  string => "70.39.130.45:443";
      "www[api.ultra.me][name]"                   string => "api.ultra.me:443";
      "www[api.ultra.me][aliases]"                string => "";
      "www[api.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[api.ultra.me][ErrorLog]"               string => "logs/ultra_api_prod_public_ssl_error_log";
      "www[api.ultra.me][TransferLog]"            string => "logs/ultra_api_prod_public_ssl_access_log";
      "www[api.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[api.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[api.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[api.ultra.me][DocumentRoot]"           string => "$(e[ultra_api_prod_public][git/deploy])";
      "www[api.ultra.me][overrides]"              string => "FileInfo Options";
      "www[api.ultra.me][extras]"                 string => "";
      "www[api.ultra.me][url_aliases]"            string => "";

      # ultra_api_prod_internal environment

      "e[ultra_api_prod_internal][git/repo]"     string => "$(web_repo)";
      "e[ultra_api_prod_internal][git/branch]"   string => "ultra-prod";
      "e[ultra_api_prod_internal][git/deploy]"   string => "$(g.wwwroot)/ultra_api_prod_internal/web";
      "e[ultra_api_prod_internal][git/schedule]" string => "any"; # always deploy
      "e[ultra_api_prod_internal][git/chown/o]"  string => "apache";
      "e[ultra_api_prod_internal][git/chown/g]"  string => "apache";
      "e[ultra_api_prod_internal][git/runas]"    string => "apache";

      "e[ultra_api_prod_internal][credentials]"  string => "ultra_data_ultra_api messaging globals";
      "e[ultra_api_prod_internal][db/ccv]"       string => "BA";
      "e[ultra_api_prod_internal][db/TSIP]"      string => "3.11";
      "e[ultra_api_prod_internal][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[ultra_api_prod_internal][mc/server]"    string => "localhost";
      "e[ultra_api_prod_internal][mc/port]"      string => "11211";
      "e[ultra_api_prod_internal][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[ultra_api_prod_internal][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[ultra_api_prod_internal][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[ultra_api_prod_internal][security/env]" string => "internal_dev";

      "e[ultra_api_prod_internal][partner/mfunds/env]" string => "mfunds_dev";
      "e[ultra_api_prod_internal][partner/api/allow]" string => "$(dev_partners)";

      "e[ultra_api_prod_internal][www/sites]"    string => "live-api.ultra.me";

      "www[live-api.ultra.me][enabled]"                string => "true";
      "www[live-api.ultra.me][type]"                   string => "ssl";
      "www[live-api.ultra.me][vhost]"                  string => "70.39.130.45:443";
      "www[live-api.ultra.me][name]"                   string => "live-api.ultra.me:443";
      "www[live-api.ultra.me][aliases]"                string => "";
      "www[live-api.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[live-api.ultra.me][ErrorLog]"               string => "logs/rgalli2-dev_ssl_error_log";
      "www[live-api.ultra.me][TransferLog]"            string => "logs/rgalli2-dev_ssl_access_log";
      "www[live-api.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[live-api.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[live-api.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[live-api.ultra.me][DocumentRoot]"           string => "$(e[ultra_api_prod_internal][git/deploy])";
      "www[live-api.ultra.me][overrides]"              string => "FileInfo Options";
      "www[live-api.ultra.me][extras]"                 string => "";
      "www[live-api.ultra.me][url_aliases]"            string => "";


      # ultra_ui_prod environment

      "e[ultra_ui_prod][git/repo]"            string => "$(ultra_repo)";
      "e[ultra_ui_prod][git/branch]"          string => "ultra-prod";
      "e[ultra_ui_prod][git/deploy]"          string => "$(g.wwwroot)/ultra_ui_prod/ultra";
      "e[ultra_ui_prod][git/schedule]"        string => "any"; # always deploy
      "e[ultra_ui_prod][git/chown/o]"         string => "apache";
      "e[ultra_ui_prod][git/chown/g]"         string => "apache";
      "e[ultra_ui_prod][git/runas]"           string => "apache";
      "e[ultra_ui_prod][service_env]"         string => "ultra_api_prod_public";

      "e[ultra_ui_prod][credentials]"         string => "ultra_data_ultra_api messaging globals";
      "e[ultra_ui_prod][db/ccv]"              string => "BA";
      "e[ultra_ui_prod][db/TSIP]"             string => "3.11";
      "e[ultra_ui_prod][mc/prefix]"           string => "$(mc_prod_prefix)";
      "e[ultra_ui_prod][mc/server]"           string => "localhost";
      "e[ultra_ui_prod][mc/port]"             string => "11211";
      "e[ultra_ui_prod][aws/key]"             string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[ultra_ui_prod][aws/pfile]"           string => "$(g.pwfileroot)/aws_uv_secret";
      "e[ultra_ui_prod][aws/uvbucket]"        string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[ultra_ui_prod][security/env]"        string => "public";

      "e[ultra_ui_prod][ultra/smarty/key]"     string => "3108359954964801208";
      "e[ultra_ui_prod][ultra/api/sites]"     string => "$(e[ultra_api_prod_public][www/sites])";

      "e[ultra_ui_prod][partner/mfunds/env]"  string => "mfunds_prod";
      "e[ultra_ui_prod][partner/api/allow]"   string => "";

      "e[ultra_ui_prod][www/sites]"           string => "services.ultra.me";

      "www[services.ultra.me][enabled]"                string => "true";
      "www[services.ultra.me][type]"                   string => "ssl";
      "www[services.ultra.me][vhost]"                  string => "70.39.130.45:443";
      "www[services.ultra.me][name]"                   string => "services.ultra.me:443";
      "www[services.ultra.me][aliases]"                string => "";
      "www[services.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[services.ultra.me][ErrorLog]"               string => "logs/ultra_ui_prod_ssl_error_log";
      "www[services.ultra.me][TransferLog]"            string => "logs/ultra_ui_prod_ssl_access_log";
      "www[services.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[services.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[services.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[services.ultra.me][DocumentRoot]"           string => "$(e[ultra_ui_prod][git/deploy])";
      "www[services.ultra.me][overrides]"              string => "FileInfo Options";
      "www[services.ultra.me][extras]"                 string => "";
      "www[services.ultra.me][url_aliases]"            string => "
Alias /logs/tsa.log /var/tmp/logs-ultra_ui_prod/tsa.log
";

This web site defines a special URL alias.

      # rgalli_dev environment

      "e[rgalli_dev][git/repo]"     string => "$(web_repo)";
      "e[rgalli_dev][git/branch]"   string => "master";
      "e[rgalli_dev][git/deploy]"   string => "$(g.wwwroot)/rgalli_dev/web";
      "e[rgalli_dev][git/schedule]" string => "!any"; # disable deployments
      "e[rgalli_dev][git/chown/o]"  string => "rgalli";
      "e[rgalli_dev][git/chown/g]"  string => "apache";
      "e[rgalli_dev][git/runas]"    string => "rgalli";

      "e[rgalli_dev][credentials]"  string => "tel_data_api_accounts messaging globals";
      "e[rgalli_dev][db/ccv]"       string => "BA";
      "e[rgalli_dev][db/TSIP]"      string => "3.9";
      "e[rgalli_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[rgalli_dev][mc/server]"    string => "localhost";
      "e[rgalli_dev][mc/port]"      string => "11211";
      "e[rgalli_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[rgalli_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[rgalli_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[rgalli_dev][security/env]" string => "internal_dev";

      "e[rgalli_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[rgalli_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[rgalli_dev][www/sites]"    string => "rgalli-dev.uvnv.com";

      "www[rgalli-dev.uvnv.com][enabled]"                string => "true";
      "www[rgalli-dev.uvnv.com][type]"                   string => "ssl";
      "www[rgalli-dev.uvnv.com][vhost]"                  string => "70.39.130.41:443";
      "www[rgalli-dev.uvnv.com][name]"                   string => "rgalli-dev.uvnv.com:443";
      "www[rgalli-dev.uvnv.com][aliases]"                string => "";
      "www[rgalli-dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[rgalli-dev.uvnv.com][ErrorLog]"               string => "logs/rgalli-dev_ssl_error_log";
      "www[rgalli-dev.uvnv.com][TransferLog]"            string => "logs/rgalli-dev_ssl_access_log";
      "www[rgalli-dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[rgalli-dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[rgalli-dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[rgalli-dev.uvnv.com][DocumentRoot]"           string => "$(e[rgalli_dev][git/deploy])";
      "www[rgalli-dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[rgalli-dev.uvnv.com][extras]"                 string => "";
      "www[rgalli-dev.uvnv.com][url_aliases]"            string => "";

      # rgalli2_dev environment

      "e[rgalli2_dev][git/repo]"     string => "$(web_repo)";
      "e[rgalli2_dev][git/branch]"   string => "ultra-apidev";
      "e[rgalli2_dev][git/deploy]"   string => "$(g.wwwroot)/rgalli2_dev/web";
      "e[rgalli2_dev][git/schedule]" string => "any"; # always deploy
      "e[rgalli2_dev][git/chown/o]"  string => "rgalli";
      "e[rgalli2_dev][git/chown/g]"  string => "apache";
      "e[rgalli2_dev][git/runas]"    string => "rgalli";

      "e[rgalli2_dev][credentials]"  string => "ultra_develop_tel_api_accounts messaging globals";
      "e[rgalli2_dev][db/ccv]"       string => "BA";
      "e[rgalli2_dev][db/TSIP]"      string => "3.11";
      "e[rgalli2_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[rgalli2_dev][mc/server]"    string => "localhost";
      "e[rgalli2_dev][mc/port]"      string => "11211";
      "e[rgalli2_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[rgalli2_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[rgalli2_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[rgalli2_dev][security/env]" string => "internal_dev";

      "e[rgalli2_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[rgalli2_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[rgalli2_dev][www/sites]"    string => "rgalli2-dev.uvnv.com";

The `rgalli2_dev` environment has historically been crucial for
development.  You should probably not kill it without lots of testing.

      "www[rgalli2-dev.uvnv.com][enabled]"                string => "true";
      "www[rgalli2-dev.uvnv.com][type]"                   string => "ssl";
      "www[rgalli2-dev.uvnv.com][vhost]"                  string => "70.39.130.41:443";
      "www[rgalli2-dev.uvnv.com][name]"                   string => "rgalli2-dev.uvnv.com:443";
      "www[rgalli2-dev.uvnv.com][aliases]"                string => "";
      "www[rgalli2-dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[rgalli2-dev.uvnv.com][ErrorLog]"               string => "logs/rgalli2-dev_ssl_error_log";
      "www[rgalli2-dev.uvnv.com][TransferLog]"            string => "logs/rgalli2-dev_ssl_access_log";
      "www[rgalli2-dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[rgalli2-dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[rgalli2-dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[rgalli2-dev.uvnv.com][DocumentRoot]"           string => "$(e[rgalli2_dev][git/deploy])";
      "www[rgalli2-dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[rgalli2-dev.uvnv.com][extras]"                 string => "";
      "www[rgalli2-dev.uvnv.com][url_aliases]"            string => "";

      # rgalli3_dev environment

      "e[rgalli3_dev][git/repo]"     string => "$(web_repo)";
      "e[rgalli3_dev][git/branch]"   string => "master";
      "e[rgalli3_dev][git/deploy]"   string => "$(g.wwwroot)/rgalli3_dev/web";
      "e[rgalli3_dev][git/schedule]" string => "!any"; # disable deployments
      "e[rgalli3_dev][git/chown/o]"  string => "rgalli";
      "e[rgalli3_dev][git/chown/g]"  string => "apache";
      "e[rgalli3_dev][git/runas]"    string => "rgalli";

      "e[rgalli3_dev][credentials]"  string => "ultra_develop_tel_api_accounts messaging globals";
      "e[rgalli3_dev][db/ccv]"       string => "BA";
      "e[rgalli3_dev][db/TSIP]"      string => "3.11";
      "e[rgalli3_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[rgalli3_dev][mc/server]"    string => "localhost";
      "e[rgalli3_dev][mc/port]"      string => "11211";
      "e[rgalli3_dev][f/ok_cc_dup]"  string => "1";
      "e[rgalli3_dev][f/ok_email_dup]" string => "1";
      
Some special features are enabled for this environment.  The PHP code
will respect them.

      "e[rgalli3_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[rgalli3_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[rgalli3_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[rgalli3_dev][security/env]" string => "internal_dev";

      "e[rgalli3_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[rgalli3_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[rgalli3_dev][www/sites]"    string => "rgalli3-dev.uvnv.com";

      "www[rgalli3-dev.uvnv.com][enabled]"                string => "true";
      "www[rgalli3-dev.uvnv.com][type]"                   string => "ssl";
      "www[rgalli3-dev.uvnv.com][vhost]"                  string => "70.39.130.41:443";
      "www[rgalli3-dev.uvnv.com][name]"                   string => "rgalli3-dev.uvnv.com:443";
      "www[rgalli3-dev.uvnv.com][aliases]"                string => "";
      "www[rgalli3-dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[rgalli3-dev.uvnv.com][ErrorLog]"               string => "logs/rgalli3-dev_ssl_error_log";
      "www[rgalli3-dev.uvnv.com][TransferLog]"            string => "logs/rgalli3-dev_ssl_access_log";
      "www[rgalli3-dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[rgalli3-dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[rgalli3-dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[rgalli3-dev.uvnv.com][DocumentRoot]"           string => "$(e[rgalli3_dev][git/deploy])";
      "www[rgalli3-dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[rgalli3-dev.uvnv.com][extras]"                 string => "";
      "www[rgalli3-dev.uvnv.com][url_aliases]"            string => "";

      # rgalli4_dev environment

      "e[rgalli4_dev][git/repo]"     string => "$(web_repo)";
      "e[rgalli4_dev][git/branch]"   string => "master";
      "e[rgalli4_dev][git/deploy]"   string => "$(g.wwwroot)/rgalli4_dev/web";
      "e[rgalli4_dev][git/schedule]" string => "!any"; # disable deployments
      "e[rgalli4_dev][git/chown/o]"  string => "rgalli";
      "e[rgalli4_dev][git/chown/g]"  string => "apache";
      "e[rgalli4_dev][git/runas]"    string => "rgalli";

      "e[rgalli4_dev][credentials]"  string => "ultra_data_ultra_api messaging globals";

`rgalli4_dev` is one of very few environments to run in production yet
use development (from the `master` branch) code.  Beware.

(Of course, it's critical to production.  That's a given.)

      "e[rgalli4_dev][db/ccv]"       string => "BA";
      "e[rgalli4_dev][db/TSIP]"      string => "3.11";
      "e[rgalli4_dev][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[rgalli4_dev][mc/server]"    string => "localhost";
      "e[rgalli4_dev][mc/port]"      string => "11211";
      "e[rgalli4_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[rgalli4_dev][f/ok_cc_dup]"  string => "1";
      "e[rgalli4_dev][f/ok_email_dup]" string => "1";
      "e[rgalli4_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[rgalli4_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[rgalli4_dev][security/env]" string => "internal_dev";

      "e[rgalli4_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[rgalli4_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[rgalli4_dev][www/sites]"    string => "rgalli4-dev.uvnv.com";

      "www[rgalli4-dev.uvnv.com][enabled]"                string => "true";
      "www[rgalli4-dev.uvnv.com][type]"                   string => "ssl";
      "www[rgalli4-dev.uvnv.com][vhost]"                  string => "70.39.130.39:443";
      "www[rgalli4-dev.uvnv.com][name]"                   string => "rgalli4-dev.uvnv.com:443";
      "www[rgalli4-dev.uvnv.com][aliases]"                string => "";
      "www[rgalli4-dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[rgalli4-dev.uvnv.com][ErrorLog]"               string => "logs/rgalli4-dev_ssl_error_log";
      "www[rgalli4-dev.uvnv.com][TransferLog]"            string => "logs/rgalli4-dev_ssl_access_log";
      "www[rgalli4-dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[rgalli4-dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[rgalli4-dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[rgalli4-dev.uvnv.com][DocumentRoot]"           string => "$(e[rgalli4_dev][git/deploy])";
      "www[rgalli4-dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[rgalli4-dev.uvnv.com][extras]"                 string => "";
      "www[rgalli4-dev.uvnv.com][url_aliases]"            string => "";

      # tzz3_dev environment

      "e[tzz3_dev][git/repo]"     string => "$(web_repo)";
      "e[tzz3_dev][git/branch]"   string => "master";
      "e[tzz3_dev][git/deploy]"   string => "$(g.wwwroot)/tzz3_dev/web";
      "e[tzz3_dev][git/schedule]" string => "!any"; # disable deployments
      "e[tzz3_dev][git/chown/o]"  string => "tzz";
      "e[tzz3_dev][git/chown/g]"  string => "apache";
      "e[tzz3_dev][git/runas]"    string => "tzz";

      "e[tzz3_dev][credentials]"  string => "ultra_data_ultra_api messaging globals";
      
This is like `rgalli4_dev` except it's not critical to production.  I
promise.

      "e[tzz3_dev][db/ccv]"       string => "BA";
      "e[tzz3_dev][db/TSIP]"      string => "3.11";
      "e[tzz3_dev][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[tzz3_dev][mc/server]"    string => "localhost";
      "e[tzz3_dev][mc/port]"      string => "11211";
      "e[tzz3_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[tzz3_dev][f/ok_cc_dup]"  string => "1";
      "e[tzz3_dev][f/ok_email_dup]" string => "1";
      "e[tzz3_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[tzz3_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[tzz3_dev][security/env]" string => "internal_dev";

      "e[tzz3_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[tzz3_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[tzz3_dev][www/sites]"    string => "tzz3-dev.uvnv.com";

      "www[tzz3-dev.uvnv.com][enabled]"                string => "true";
      "www[tzz3-dev.uvnv.com][type]"                   string => "ssl";
      "www[tzz3-dev.uvnv.com][vhost]"                  string => "70.39.130.39:443";
      "www[tzz3-dev.uvnv.com][name]"                   string => "tzz3-dev.uvnv.com:443";
      "www[tzz3-dev.uvnv.com][aliases]"                string => "";
      "www[tzz3-dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[tzz3-dev.uvnv.com][ErrorLog]"               string => "logs/tzz3-dev_ssl_error_log";
      "www[tzz3-dev.uvnv.com][TransferLog]"            string => "logs/tzz3-dev_ssl_access_log";
      "www[tzz3-dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[tzz3-dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[tzz3-dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[tzz3-dev.uvnv.com][DocumentRoot]"           string => "$(e[tzz3_dev][git/deploy])";
      "www[tzz3-dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[tzz3-dev.uvnv.com][extras]"                 string => "";
      "www[tzz3-dev.uvnv.com][url_aliases]"            string => "";

      # riz_dev environment

      "e[riz_dev][git/repo]"     string => "$(web_repo)";
      "e[riz_dev][git/branch]"   string => "master";
      "e[riz_dev][git/deploy]"   string => "$(g.wwwroot)/riz_dev/web";
      "e[riz_dev][git/schedule]" string => "!any"; # never auto-deploy
      "e[riz_dev][git/chown/o]"  string => "rizwank";
      "e[riz_dev][git/chown/g]"  string => "apache";
      "e[riz_dev][git/runas]"    string => "rizwank";

      "e[riz_dev][credentials]"  string => "ultra_develop_tel_api_accounts messaging globals";
      "e[riz_dev][db/ccv]"       string => "BA";
      "e[riz_dev][db/TSIP]"      string => "3.11";
      "e[riz_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[riz_dev][mc/server]"    string => "localhost";
      "e[riz_dev][mc/port]"      string => "11211";
      "e[riz_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[riz_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[riz_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[riz_dev][security/env]" string => "internal_dev";

      "e[riz_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[riz_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[riz_dev][www/sites]"    string => "riz-dev.uvnv.com";

      "www[riz-dev.uvnv.com][enabled]"                string => "true";
      "www[riz-dev.uvnv.com][type]"                   string => "ssl";
      "www[riz-dev.uvnv.com][vhost]"                  string => "70.39.130.41:443";
      "www[riz-dev.uvnv.com][name]"                   string => "riz-dev.uvnv.com:443";
      "www[riz-dev.uvnv.com][aliases]"                string => "";
      "www[riz-dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[riz-dev.uvnv.com][ErrorLog]"               string => "logs/riz-dev_ssl_error_log";
      "www[riz-dev.uvnv.com][TransferLog]"            string => "logs/riz-dev_ssl_access_log";
      "www[riz-dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[riz-dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[riz-dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[riz-dev.uvnv.com][DocumentRoot]"           string => "$(e[riz_dev][git/deploy])";
      "www[riz-dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[riz-dev.uvnv.com][extras]"                 string => "";
      "www[riz-dev.uvnv.com][url_aliases]"            string => "";

      # riz_api environment

      "e[riz_api][git/repo]"     string => "$(web_repo)";
      "e[riz_api][git/branch]"   string => "master";
      "e[riz_api][git/deploy]"   string => "$(g.wwwroot)/riz_api/web";
      "e[riz_api][git/schedule]" string => "any"; # always deploy
      "e[riz_api][git/chown/o]"  string => "rizwank";
      "e[riz_api][git/chown/g]"  string => "apache";
      "e[riz_api][git/runas]"    string => "rizwank";

      "e[riz_api][credentials]"  string => "ultra_data_ultra_api messaging globals";
      "e[riz_api][db/ccv]"       string => "BA";
      "e[riz_api][db/TSIP]"      string => "3.11";
      "e[riz_api][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[riz_api][mc/server]"    string => "localhost";
      "e[riz_api][mc/port]"      string => "11211";
      "e[riz_api][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[riz_api][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[riz_api][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[riz_api][security/env]" string => "internal_dev";

      "e[riz_api][partner/mfunds/env]" string => "mfunds_prod";
      "e[riz_api][partner/api/allow]" string => "$(dev_partners)";

      "e[riz_api][www/sites]"    string => "riz-api.ultra.me";

      "www[riz-api.ultra.me][enabled]"                string => "true";
      "www[riz-api.ultra.me][type]"                   string => "ssl";
      "www[riz-api.ultra.me][vhost]"                  string => "70.39.130.41:443";
      "www[riz-api.ultra.me][name]"                   string => "riz-api.ultra.me:443";
      "www[riz-api.ultra.me][aliases]"                string => "";
      "www[riz-api.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[riz-api.ultra.me][ErrorLog]"               string => "logs/riz-api_ssl_error_log";
      "www[riz-api.ultra.me][TransferLog]"            string => "logs/riz-api_ssl_access_log";
      "www[riz-api.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[riz-api.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[riz-api.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[riz-api.ultra.me][DocumentRoot]"           string => "$(e[riz_api][git/deploy])";
      "www[riz-api.ultra.me][overrides]"              string => "FileInfo Options";
      "www[riz-api.ultra.me][extras]"                 string => "";
      "www[riz-api.ultra.me][url_aliases]"            string => "";

      # eapi_dev environment

      "e[eapi_dev][git/repo]"     string => "$(web_repo)";
      "e[eapi_dev][git/branch]"   string => "ultra-apidev";
      "e[eapi_dev][git/deploy]"   string => "$(g.wwwroot)/eapi_dev/web";
      "e[eapi_dev][git/schedule]" string => "any"; # always deploy
      "e[eapi_dev][git/chown/o]"  string => "rizwank";
      "e[eapi_dev][git/chown/g]"  string => "apache";
      "e[eapi_dev][git/runas]"    string => "rizwank";

      "e[eapi_dev][credentials]"  string => "ultra_develop_tel_api_accounts messaging globals";
      "e[eapi_dev][db/ccv]"       string => "BA";
      "e[eapi_dev][db/TSIP]"      string => "3.11";
      "e[eapi_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[eapi_dev][mc/server]"    string => "localhost";
      "e[eapi_dev][mc/port]"      string => "11211";
      "e[eapi_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[eapi_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[eapi_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[eapi_dev][security/env]" string => "external_dev";

This environment is open externally, but only for the development
ranges (whatever the `external_dev` security environment defines).

      "e[eapi_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[eapi_dev][partner/api/allow]" string => "ePay";

It only allows access to the `ePay` partner!

      "e[eapi_dev][www/sites]"    string => "eapi-dev.ultra.me";

      "www[eapi-dev.ultra.me][enabled]"                string => "true";
      "www[eapi-dev.ultra.me][type]"                   string => "ssl";
      "www[eapi-dev.ultra.me][vhost]"                  string => "70.39.130.41:443";
      "www[eapi-dev.ultra.me][name]"                   string => "eapi-dev.ultra.me:443";
      "www[eapi-dev.ultra.me][aliases]"                string => "";
      "www[eapi-dev.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[eapi-dev.ultra.me][ErrorLog]"               string => "logs/eapi-dev_ssl_error_log";
      "www[eapi-dev.ultra.me][TransferLog]"            string => "logs/eapi-dev_ssl_access_log";
      "www[eapi-dev.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[eapi-dev.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[eapi-dev.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[eapi-dev.ultra.me][DocumentRoot]"           string => "$(e[eapi_dev][git/deploy])";
      "www[eapi-dev.ultra.me][overrides]"              string => "FileInfo Options";
      "www[eapi-dev.ultra.me][extras]"                 string => "";
      "www[eapi-dev.ultra.me][url_aliases]"            string => "";

      # threeci_prod environment

      "e[threeci_prod][git/repo]"     string => "$(web_repo)";
      "e[threeci_prod][git/branch]"   string => "master";
      "e[threeci_prod][git/deploy]"   string => "$(g.wwwroot)/threeci_prod/web";
      "e[threeci_prod][git/schedule]" string => "any"; # always deploy
      "e[threeci_prod][git/chown/o]"  string => "rizwank";
      "e[threeci_prod][git/chown/g]"  string => "apache";
      "e[threeci_prod][git/runas]"    string => "rizwank";

      "e[threeci_prod][credentials]"  string => "ultra_data_ultra_api messaging globals";
      "e[threeci_prod][db/ccv]"       string => "BA";
      "e[threeci_prod][db/TSIP]"      string => "3.11";
      "e[threeci_prod][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[threeci_prod][mc/server]"    string => "localhost";
      "e[threeci_prod][mc/port]"      string => "11211";
      "e[threeci_prod][f/ok_cc_dup]"  string => "1";
      "e[threeci_prod][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[threeci_prod][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[threeci_prod][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[threeci_prod][security/env]" string => "external_dev";

      "e[threeci_prod][partner/mfunds/env]" string => "mfunds_dev";
      "e[threeci_prod][partner/api/allow]" string => "interactivecare threeci";

      "e[threeci_prod][www/sites]"    string => "threeci-api.ultra.me";

      "www[threeci-api.ultra.me][enabled]"                string => "true";
      "www[threeci-api.ultra.me][type]"                   string => "ssl";
      "www[threeci-api.ultra.me][vhost]"                  string => "70.39.130.40:443";
      "www[threeci-api.ultra.me][name]"                   string => "threeci-api.ultra.me:443";
      "www[threeci-api.ultra.me][aliases]"                string => "";
      "www[threeci-api.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[threeci-api.ultra.me][ErrorLog]"               string => "logs/threeci-api_ssl_error_log";
      "www[threeci-api.ultra.me][TransferLog]"            string => "logs/threeci-api_ssl_access_log";
      "www[threeci-api.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[threeci-api.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[threeci-api.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[threeci-api.ultra.me][DocumentRoot]"           string => "$(e[threeci_prod][git/deploy])";
      "www[threeci-api.ultra.me][overrides]"              string => "FileInfo Options";
      "www[threeci-api.ultra.me][extras]"                 string => "";
      "www[threeci-api.ultra.me][url_aliases]"            string => "";

      # celluphone_dev environment

      "e[celluphone_dev][git/repo]"     string => "$(web_repo)";
      "e[celluphone_dev][git/branch]"   string => "ultra-apidev";
      "e[celluphone_dev][git/deploy]"   string => "$(g.wwwroot)/celluphone_dev/web";
      "e[celluphone_dev][git/schedule]" string => "any"; # always deploy
      "e[celluphone_dev][git/chown/o]"  string => "rizwank";
      "e[celluphone_dev][git/chown/g]"  string => "apache";
      "e[celluphone_dev][git/runas]"    string => "rizwank";

      "e[celluphone_dev][credentials]"  string => "ultra_develop_tel_api_accounts messaging globals";
      "e[celluphone_dev][db/ccv]"       string => "BA";
      "e[celluphone_dev][db/TSIP]"      string => "3.11";
      "e[celluphone_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[celluphone_dev][mc/server]"    string => "localhost";
      "e[celluphone_dev][mc/port]"      string => "11211";
      "e[celluphone_dev][f/ok_cc_dup]"  string => "1";
      "e[celluphone_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[celluphone_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[celluphone_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[celluphone_dev][security/env]" string => "external_dev";

      "e[celluphone_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[celluphone_dev][partner/api/allow]" string => "celluphone";

      "e[celluphone_dev][www/sites]"    string => "celluphone-dev.uvnv.com";

      "www[celluphone-dev.uvnv.com][enabled]"                string => "true";
      "www[celluphone-dev.uvnv.com][type]"                   string => "ssl";
      "www[celluphone-dev.uvnv.com][vhost]"                  string => "70.39.130.41:443";
      "www[celluphone-dev.uvnv.com][name]"                   string => "celluphone-dev.uvnv.com:443";
      "www[celluphone-dev.uvnv.com][aliases]"                string => "";
      "www[celluphone-dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[celluphone-dev.uvnv.com][ErrorLog]"               string => "logs/celluphone-dev_ssl_error_log";
      "www[celluphone-dev.uvnv.com][TransferLog]"            string => "logs/celluphone-dev_ssl_access_log";
      "www[celluphone-dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[celluphone-dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[celluphone-dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[celluphone-dev.uvnv.com][DocumentRoot]"           string => "$(e[celluphone_dev][git/deploy])";
      "www[celluphone-dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[celluphone-dev.uvnv.com][extras]"                 string => "";
      "www[celluphone-dev.uvnv.com][url_aliases]"            string => "";


      # epay_prod environment

      "e[epay_prod][git/repo]"     string => "$(web_repo)";
      "e[epay_prod][git/branch]"   string => "ultra-prod";
      "e[epay_prod][git/deploy]"   string => "$(g.wwwroot)/epay_prod/web";
      "e[epay_prod][git/schedule]" string => "any"; # always deploy
      "e[epay_prod][git/chown/o]"  string => "rizwank";
      "e[epay_prod][git/chown/g]"  string => "apache";
      "e[epay_prod][git/runas]"    string => "rizwank";

      "e[epay_prod][credentials]"  string => "ultra_data_ultra_api messaging globals";
      "e[epay_prod][db/ccv]"       string => "BA";
      "e[epay_prod][db/TSIP]"      string => "3.11";
      "e[epay_prod][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[epay_prod][mc/server]"    string => "localhost";
      "e[epay_prod][mc/port]"      string => "11211";
      "e[epay_prod][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[epay_prod][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[epay_prod][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[epay_prod][security/env]" string => "external_dev";

      "e[epay_prod][partner/mfunds/env]" string => "mfunds_dev";
      "e[epay_prod][partner/api/allow]" string => "ePay";

      "e[epay_prod][www/sites]"    string => "epay-api.ultra.me";

      "www[epay-api.ultra.me][enabled]"                string => "true";
      "www[epay-api.ultra.me][type]"                   string => "ssl";
      "www[epay-api.ultra.me][vhost]"                  string => "70.39.130.40:443";
      "www[epay-api.ultra.me][name]"                   string => "epay-api.ultra.me:443";
      "www[epay-api.ultra.me][aliases]"                string => "";
      "www[epay-api.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[epay-api.ultra.me][ErrorLog]"               string => "logs/epay-api_ssl_error_log";
      "www[epay-api.ultra.me][TransferLog]"            string => "logs/epay-api_ssl_access_log";
      "www[epay-api.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[epay-api.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[epay-api.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[epay-api.ultra.me][DocumentRoot]"           string => "$(e[epay_prod][git/deploy])";
      "www[epay-api.ultra.me][overrides]"              string => "FileInfo Options";
      "www[epay-api.ultra.me][extras]"                 string => "";
      "www[epay-api.ultra.me][url_aliases]"            string => "";

      # celluphone_prod environment

      "e[celluphone_prod][git/repo]"     string => "$(web_repo)";
      "e[celluphone_prod][git/branch]"   string => "ultra-prod";
      "e[celluphone_prod][git/deploy]"   string => "$(g.wwwroot)/celluphone_prod/web";
      "e[celluphone_prod][git/schedule]" string => "any"; # always deploy
      "e[celluphone_prod][git/chown/o]"  string => "rizwank";
      "e[celluphone_prod][git/chown/g]"  string => "apache";
      "e[celluphone_prod][git/runas]"    string => "rizwank";

      "e[celluphone_prod][credentials]"  string => "ultra_data_ultra_api messaging globals";
      "e[celluphone_prod][db/ccv]"       string => "BA";
      "e[celluphone_prod][db/TSIP]"      string => "3.11";
      "e[celluphone_prod][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[celluphone_prod][mc/server]"    string => "localhost";
      "e[celluphone_prod][mc/port]"      string => "11211";
      "e[celluphone_prod][f/ok_cc_dup]"  string => "1";
      "e[celluphone_prod][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[celluphone_prod][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[celluphone_prod][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[celluphone_prod][security/env]" string => "external_dev";

      "e[celluphone_prod][partner/mfunds/env]" string => "mfunds_dev";
      "e[celluphone_prod][partner/api/allow]" string => "celluphone";

      "e[celluphone_prod][www/sites]"    string => "celluphone-api.ultra.me";

      "www[celluphone-api.ultra.me][enabled]"                string => "true";
      "www[celluphone-api.ultra.me][type]"                   string => "ssl";
      "www[celluphone-api.ultra.me][vhost]"                  string => "70.39.130.40:443";
      "www[celluphone-api.ultra.me][name]"                   string => "celluphone-api.ultra.me:443";
      "www[celluphone-api.ultra.me][aliases]"                string => "";
      "www[celluphone-api.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[celluphone-api.ultra.me][ErrorLog]"               string => "logs/celluphone-api_ssl_error_log";
      "www[celluphone-api.ultra.me][TransferLog]"            string => "logs/celluphone-api_ssl_access_log";
      "www[celluphone-api.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[celluphone-api.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[celluphone-api.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[celluphone-api.ultra.me][DocumentRoot]"           string => "$(e[celluphone_prod][git/deploy])";
      "www[celluphone-api.ultra.me][overrides]"              string => "FileInfo Options";
      "www[celluphone-api.ultra.me][extras]"                 string => "";
      "www[celluphone-api.ultra.me][url_aliases]"            string => "";


      # rainmaker_prod environment

      "e[rainmaker_prod][git/repo]"     string => "$(web_repo)";
      "e[rainmaker_prod][git/branch]"   string => "ultra-prod";
      "e[rainmaker_prod][git/deploy]"   string => "$(g.wwwroot)/rainmaker_prod/web";
      "e[rainmaker_prod][git/schedule]" string => "any"; # always deploy
      "e[rainmaker_prod][git/chown/o]"  string => "rizwank";
      "e[rainmaker_prod][git/chown/g]"  string => "apache";
      "e[rainmaker_prod][git/runas]"    string => "rizwank";

      "e[rainmaker_prod][credentials]"  string => "ultra_data_ultra_api messaging globals";
      "e[rainmaker_prod][db/ccv]"       string => "BA";
      "e[rainmaker_prod][db/TSIP]"      string => "3.11";
      "e[rainmaker_prod][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[rainmaker_prod][mc/server]"    string => "localhost";
      "e[rainmaker_prod][mc/port]"      string => "11211";
      "e[rainmaker_prod][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[rainmaker_prod][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[rainmaker_prod][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[rainmaker_prod][security/env]" string => "external_dev";

      "e[rainmaker_prod][partner/mfunds/env]" string => "mfunds_dev";
      "e[rainmaker_prod][partner/api/allow]" string => "rainmaker";

      "e[rainmaker_prod][www/sites]"    string => "rainmaker-api.ultra.me";

      "www[rainmaker-api.ultra.me][enabled]"                string => "true";
      "www[rainmaker-api.ultra.me][type]"                   string => "ssl";
      "www[rainmaker-api.ultra.me][vhost]"                  string => "70.39.130.40:443";
      "www[rainmaker-api.ultra.me][name]"                   string => "rainmaker-api.ultra.me:443";
      "www[rainmaker-api.ultra.me][aliases]"                string => "";
      "www[rainmaker-api.ultra.me][favicon]"                string => "$(uv_favicon)";
      "www[rainmaker-api.ultra.me][ErrorLog]"               string => "logs/rainmaker-api_ssl_error_log";
      "www[rainmaker-api.ultra.me][TransferLog]"            string => "logs/rainmaker-api_ssl_access_log";
      "www[rainmaker-api.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[rainmaker-api.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[rainmaker-api.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[rainmaker-api.ultra.me][DocumentRoot]"           string => "$(e[rainmaker_prod][git/deploy])";
      "www[rainmaker-api.ultra.me][overrides]"              string => "FileInfo Options";
      "www[rainmaker-api.ultra.me][extras]"                 string => "";
      "www[rainmaker-api.ultra.me][url_aliases]"            string => "";

      # rainmaker_dev environment

      "e[rainmaker_dev][git/repo]"     string => "$(web_repo)";
      "e[rainmaker_dev][git/branch]"   string => "master";
      "e[rainmaker_dev][git/deploy]"   string => "$(g.wwwroot)/rainmaker_dev/web";
      "e[rainmaker_dev][git/schedule]" string => "any"; # always deploy
      "e[rainmaker_dev][git/chown/o]"  string => "rizwank";
      "e[rainmaker_dev][git/chown/g]"  string => "apache";
      "e[rainmaker_dev][git/runas]"    string => "rizwank";

      "e[rainmaker_dev][credentials]"  string => "ultra_develop_tel_api_accounts messaging globals";
      "e[rainmaker_dev][db/ccv]"       string => "BA";
      "e[rainmaker_dev][db/TSIP]"      string => "3.11";
      "e[rainmaker_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[rainmaker_dev][mc/server]"    string => "localhost";
      "e[rainmaker_dev][mc/port]"      string => "11211";
      "e[rainmaker_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[rainmaker_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[rainmaker_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[rainmaker_dev][security/env]" string => "external_dev";

      "e[rainmaker_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[rainmaker_dev][partner/api/allow]" string => "rainmaker";

      "e[rainmaker_dev][www/sites]"    string => "rainmaker-dev.ultra.me rainmaker-dev.uvnv.com";

      "www[rainmaker-dev.uvnv.com][enabled]"                string => "true";
      "www[rainmaker-dev.uvnv.com][type]"                   string => "ssl";
      "www[rainmaker-dev.uvnv.com][vhost]"                  string => "70.39.130.41:443";
      "www[rainmaker-dev.uvnv.com][name]"                   string => "rainmaker-dev.uvnv.com:443";
      "www[rainmaker-dev.uvnv.com][aliases]"                string => "";
      "www[rainmaker-dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[rainmaker-dev.uvnv.com][ErrorLog]"               string => "logs/rainmaker-dev_ssl_error_log";
      "www[rainmaker-dev.uvnv.com][TransferLog]"            string => "logs/rainmaker-dev_ssl_access_log";
      "www[rainmaker-dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[rainmaker-dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[rainmaker-dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[rainmaker-dev.uvnv.com][DocumentRoot]"           string => "$(e[rainmaker_dev][git/deploy])";
      "www[rainmaker-dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[rainmaker-dev.uvnv.com][extras]"                 string => "";
      "www[rainmaker-dev.uvnv.com][url_aliases]"            string => "";

      "www[rainmaker-dev.ultra.me][enabled]"                string => "true";
      "www[rainmaker-dev.ultra.me][type]"                   string => "ssl";
      "www[rainmaker-dev.ultra.me][vhost]"                  string => "70.39.130.41:443";
      "www[rainmaker-dev.ultra.me][name]"                   string => "rainmaker-dev.ultra.me:443";
      "www[rainmaker-dev.ultra.me][aliases]"                string => "";
      "www[rainmaker-dev.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[rainmaker-dev.ultra.me][ErrorLog]"               string => "logs/rainmaker-dev_ssl_error_log";
      "www[rainmaker-dev.ultra.me][TransferLog]"            string => "logs/rainmaker-dev_ssl_access_log";
      "www[rainmaker-dev.ultra.me][SSLCertificateKeyFile]"  string => "$(ultra_cert_key)";
      "www[rainmaker-dev.ultra.me][SSLCertificateFile]"     string => "$(ultra_cert_crt)";
      "www[rainmaker-dev.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[rainmaker-dev.ultra.me][DocumentRoot]"           string => "$(e[rainmaker_dev][git/deploy])";
      "www[rainmaker-dev.ultra.me][overrides]"              string => "FileInfo Options";
      "www[rainmaker-dev.ultra.me][extras]"                 string => "";
      "www[rainmaker-dev.ultra.me][url_aliases]"            string => "";

      # indiald_app_dev environment

      "e[indiald_app_dev][git/repo]"     string => "$(web_repo)";
      "e[indiald_app_dev][git/branch]"   string => "indiald-iphone";
      "e[indiald_app_dev][git/deploy]"   string => "$(g.wwwroot)/indiald_app_dev/web";
      "e[indiald_app_dev][git/schedule]" string => "any"; # always deploy
      "e[indiald_app_dev][git/chown/o]"  string => "rizwank";
      "e[indiald_app_dev][git/chown/g]"  string => "apache";
      "e[indiald_app_dev][git/runas]"    string => "rizwank";

      "e[indiald_app_dev][credentials]"  string => "tel_data_api_accounts messaging globals";
      "e[indiald_app_dev][db/ccv]"       string => "BA";
      "e[indiald_app_dev][db/TSIP]"      string => "3.9";
      "e[indiald_app_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[indiald_app_dev][mc/server]"    string => "localhost";
      "e[indiald_app_dev][mc/port]"      string => "11211";
      "e[indiald_app_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[indiald_app_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[indiald_app_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[indiald_app_dev][security/env]" string => "internal_dev";

      "e[indiald_app_dev][partner/mfunds/env]" string => "mfunds_prod";
      "e[indiald_app_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[indiald_app_dev][www/sites]"    string => "indiald-iphone.hometowntelecom.com";

      "www[indiald-iphone.hometowntelecom.com][enabled]"                string => "true";
      "www[indiald-iphone.hometowntelecom.com][type]"                   string => "ssl";
      "www[indiald-iphone.hometowntelecom.com][vhost]"                  string => "70.39.130.37:2443";
      "www[indiald-iphone.hometowntelecom.com][name]"                   string => "indiald-iphone.hometowntelecom.com:2443";
      "www[indiald-iphone.hometowntelecom.com][aliases]"                string => "";
      "www[indiald-iphone.hometowntelecom.com][favicon]"                string => "$(uv_favicon)";
      "www[indiald-iphone.hometowntelecom.com][SSLCertificateKeyFile]"  string => "$(htt_cert_key)";
      "www[indiald-iphone.hometowntelecom.com][SSLCertificateFile]"     string => "$(htt_cert_crt)";
      "www[indiald-iphone.hometowntelecom.com][DirectoryIndex]"         string => "index.html";
      "www[indiald-iphone.hometowntelecom.com][ErrorLog]"               string => "logs/indiald_app_dev_ssl_error_log";
      "www[indiald-iphone.hometowntelecom.com][TransferLog]"            string => "logs/indiald_app_dev_ssl_access_log";
      "www[indiald-iphone.hometowntelecom.com][DocumentRoot]"           string => "$(e[indiald_app_dev][git/deploy])";
      "www[indiald-iphone.hometowntelecom.com][overrides]"              string => "FileInfo Options";
      "www[indiald-iphone.hometowntelecom.com][extras]"                 string => "";
      "www[indiald-iphone.hometowntelecom.com][url_aliases]"            string => "";

      # tzz_dev environment

      "e[tzz_dev][git/repo]"     string => "$(web_repo)";
      "e[tzz_dev][git/branch]"   string => "master";
      "e[tzz_dev][git/deploy]"   string => "$(g.wwwroot)/tzz_dev/web";
      "e[tzz_dev][git/schedule]" string => "any"; # always deploy
      "e[tzz_dev][git/chown/o]"  string => "tzz";
      "e[tzz_dev][git/chown/g]"  string => "apache";
      "e[tzz_dev][git/runas]"    string => "tzz";

      "e[tzz_dev][credentials]"  string => "tel_data_api_accounts messaging globals";
      "e[tzz_dev][db/ccv]"       string => "BA";
      "e[tzz_dev][db/TSIP]"      string => "3.9";
      "e[tzz_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[tzz_dev][mc/server]"    string => "localhost";
      "e[tzz_dev][mc/port]"      string => "11211";
      "e[tzz_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[tzz_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[tzz_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[tzz_dev][security/env]" string => "internal_dev";

      "e[tzz_dev][partner/mfunds/env]" string => "mfunds_prod";
      "e[tzz_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[tzz_dev][www/sites]"    string => "dev.uvnv.com 2443-api-dev.hometowntelecom.com api-dev.hometowntelecom.com";

      "www[dev.uvnv.com][enabled]"                string => "true";
      "www[dev.uvnv.com][type]"                   string => "ssl";
      "www[dev.uvnv.com][vhost]"                  string => "70.39.130.41:443";
      "www[dev.uvnv.com][name]"                   string => "dev.uvnv.com:443";
      "www[dev.uvnv.com][aliases]"                string => "dev.uvcard.com:443";
      "www[dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[dev.uvnv.com][ErrorLog]"               string => "logs/dev_ssl_error_log";
      "www[dev.uvnv.com][TransferLog]"            string => "logs/dev_ssl_access_log";
      "www[dev.uvnv.com][DocumentRoot]"           string => "$(e[tzz_dev][git/deploy])";
      "www[dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[dev.uvnv.com][extras]"                 string => "";
      "www[dev.uvnv.com][url_aliases]"            string => "";

      "www[2443-api-dev.hometowntelecom.com][enabled]"                string => "true";
      "www[2443-api-dev.hometowntelecom.com][type]"                   string => "ssl";
      "www[2443-api-dev.hometowntelecom.com][vhost]"                  string => "70.39.130.37:2443";
      "www[2443-api-dev.hometowntelecom.com][name]"                   string => "api-dev.hometowntelecom.com:2443";
      "www[2443-api-dev.hometowntelecom.com][aliases]"                string => "";
      "www[2443-api-dev.hometowntelecom.com][favicon]"                string => "$(uv_favicon)";
      "www[2443-api-dev.hometowntelecom.com][SSLCertificateKeyFile]"  string => "$(htt_cert_key)";
      "www[2443-api-dev.hometowntelecom.com][SSLCertificateFile]"     string => "$(htt_cert_crt)";
      "www[2443-api-dev.hometowntelecom.com][DirectoryIndex]"         string => "index.html";
      "www[2443-api-dev.hometowntelecom.com][ErrorLog]"               string => "logs/api_ssl_error_log";
      "www[2443-api-dev.hometowntelecom.com][TransferLog]"            string => "logs/api_ssl_access_log";
      "www[2443-api-dev.hometowntelecom.com][DocumentRoot]"           string => "$(e[tzz_dev][git/deploy])";
      "www[2443-api-dev.hometowntelecom.com][overrides]"              string => "FileInfo Options";
      "www[2443-api-dev.hometowntelecom.com][extras]"                 string => "";
      "www[2443-api-dev.hometowntelecom.com][url_aliases]"            string => "";

      "www[api-dev.hometowntelecom.com][enabled]"                string => "true";
      "www[api-dev.hometowntelecom.com][type]"                   string => "ssl";
      "www[api-dev.hometowntelecom.com][vhost]"                  string => "70.39.130.37:443";
      "www[api-dev.hometowntelecom.com][name]"                   string => "api-dev.hometowntelecom.com:443";
      "www[api-dev.hometowntelecom.com][aliases]"                string => "";
      "www[api-dev.hometowntelecom.com][favicon]"                string => "$(uv_favicon)";
      "www[api-dev.hometowntelecom.com][SSLCertificateKeyFile]"  string => "$(htt_cert_key)";
      "www[api-dev.hometowntelecom.com][SSLCertificateFile]"     string => "$(htt_cert_crt)";
      "www[api-dev.hometowntelecom.com][DirectoryIndex]"         string => "index.php";
      "www[api-dev.hometowntelecom.com][DocumentRoot]"           string => "$(e[tzz_dev][git/deploy])";
      # just for this server, for the mobile app, disable .htaccess and thus the UV password
      "www[api-dev.hometowntelecom.com][overrides]"              string => "FileInfo Options";
      "www[api-dev.hometowntelecom.com][extras]"                 string => "<Directory \"$(e[tzz_dev][git/deploy])\">
    AllowOverride None
  </Directory>
";
      "www[api-dev.hometowntelecom.com][url_aliases]"            string => "";

      # tzz2_dev environment

      "e[tzz2_dev][git/repo]"     string => "$(web_repo)";
      "e[tzz2_dev][git/branch]"   string => "master";
      "e[tzz2_dev][git/deploy]"   string => "$(g.wwwroot)/tzz2_dev/web";
      "e[tzz2_dev][git/schedule]" string => "!any"; # disable deployments
      "e[tzz2_dev][git/chown/o]"  string => "tzz";
      "e[tzz2_dev][git/chown/g]"  string => "apache";
      "e[tzz2_dev][git/runas]"    string => "tzz";

      "e[tzz2_dev][credentials]"  string => "ultra_develop_tel_api_accounts messaging globals";
      "e[tzz2_dev][db/ccv]"       string => "BA";
      "e[tzz2_dev][db/TSIP]"      string => "3.11";
      "e[tzz2_dev][mc/prefix]"    string => "$(mc_dev_prefix)";
      "e[tzz2_dev][mc/server]"    string => "localhost";
      "e[tzz2_dev][mc/port]"      string => "11211";
      "e[tzz2_dev][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[tzz2_dev][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[tzz2_dev][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[tzz2_dev][security/env]" string => "internal_ad_dev";

      "e[tzz2_dev][partner/mfunds/env]" string => "mfunds_dev";
      "e[tzz2_dev][partner/api/allow]" string => "$(dev_partners)";

      "e[tzz2_dev][www/sites]"    string => "tzz2-dev.uvnv.com";

      "www[tzz2-dev.uvnv.com][enabled]"                string => "true";
      "www[tzz2-dev.uvnv.com][type]"                   string => "ssl";
      "www[tzz2-dev.uvnv.com][vhost]"                  string => "70.39.130.41:443";
      "www[tzz2-dev.uvnv.com][name]"                   string => "tzz2-dev.uvnv.com:443";
      "www[tzz2-dev.uvnv.com][aliases]"                string => "";
      "www[tzz2-dev.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[tzz2-dev.uvnv.com][ErrorLog]"               string => "logs/tzz2-dev_ssl_error_log";
      "www[tzz2-dev.uvnv.com][TransferLog]"            string => "logs/tzz2-dev_ssl_access_log";
      "www[tzz2-dev.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[tzz2-dev.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[tzz2-dev.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[tzz2-dev.uvnv.com][DocumentRoot]"           string => "$(e[tzz2_dev][git/deploy])";
      "www[tzz2-dev.uvnv.com][overrides]"              string => "FileInfo Options";
      "www[tzz2-dev.uvnv.com][extras]"                 string => "";
      "www[tzz2-dev.uvnv.com][url_aliases]"            string => "";

      # glickmancapital_prod environment

      "e[glickmancapital_prod][git/repo]" string => "https://cmviewer:bmb00@source.hometowntelecom.com:8843/scm/WEB/glickmancapital.com.git";
      "e[glickmancapital_prod][git/branch]"   string => "master";
      "e[glickmancapital_prod][git/deploy]"   string => "$(g.wwwroot)/glickmancapital_prod/glickmancapital.com";
      "e[glickmancapital_prod][git/schedule]" string => "!quick"; # deploy when !quick (hourly)
      "e[glickmancapital_prod][git/chown/o]"  string => "apache";
      "e[glickmancapital_prod][git/chown/g]"  string => "apache";
      "e[glickmancapital_prod][git/runas]"    string => "apache";

      "e[glickmancapital_prod][credentials]"  string => "";
      "e[glickmancapital_prod][db/ccv]"       string => "";
      "e[glickmancapital_prod][db/TSIP]"      string => "";
      "e[glickmancapital_prod][mc/prefix]"    string => "";
      "e[glickmancapital_prod][mc/server]"    string => "";
      "e[glickmancapital_prod][mc/port]"      string => "";
      "e[glickmancapital_prod][security/env]" string => "public";

      "e[glickmancapital_prod][www/sites]"    string => "glickmancapital.com";

`glickmancapital.com` is a Wordpress website, served by the
`glickmancapital_prod` environment.  Note the lack of DB credentials
and most other normal facilities, the `!quick` deployment schedule,
and the different Git repository.

      "www[glickmancapital.com][enabled]"                string => "true";
      "www[glickmancapital.com][type]"                   string => ""; # plain HTTP
      "www[glickmancapital.com][vhost]"                  string => "70.39.130.43:80";
      "www[glickmancapital.com][name]"                   string => "glickmancapital.com:80";
      "www[glickmancapital.com][aliases]"                string => "www.glickmancapital.com:80 glickman.uvnv.com:80";
      "www[glickmancapital.com][favicon]"                string => "$(uv_favicon)";
      "www[glickmancapital.com][SSLCertificateKeyFile]"  string => "";
      "www[glickmancapital.com][SSLCertificateFile]"     string => "";
      "www[glickmancapital.com][DirectoryIndex]"         string => "index.html";
      "www[glickmancapital.com][DocumentRoot]"           string => "$(e[glickmancapital_prod][git/deploy])";
      "www[glickmancapital.com][overrides]"              string => "FileInfo Options";
      "www[glickmancapital.com][extras]"                 string => "";
      "www[glickmancapital.com][url_aliases]"            string => "";

      # ultrame_ee_dev environment
      "e[ultrame_ee_dev][git/repo]" string => "https://cmviewer:bmb00@source.hometowntelecom.com:8843/scm/CRSH/ultra-ee.git";
      "e[ultrame_ee_dev][git/branch]"   string => "master";
      "e[ultrame_ee_dev][git/deploy]"   string => "$(g.wwwroot)/ultrame_ee_dev/ultra-ee";
      "e[ultrame_ee_dev][git/schedule]" string => "!any"; # disable deployments
      "e[ultrame_ee_dev][git/chown/o]"  string => "sean";
      "e[ultrame_ee_dev][git/chown/g]"  string => "apache";
      "e[ultrame_ee_dev][git/runas]"    string => "sean";

      "e[ultrame_ee_dev][credentials]"  string => "";
      "e[ultrame_ee_dev][db/ccv]"       string => "";
      "e[ultrame_ee_dev][db/TSIP]"      string => "";
      "e[ultrame_ee_dev][mc/prefix]"    string => "";
      "e[ultrame_ee_dev][mc/server]"    string => "";
      "e[ultrame_ee_dev][mc/port]"      string => "";
      "e[ultrame_ee_dev][security/env]" string => "demo";

      "e[ultrame_ee_dev][www/sites]"    string => "crush-dev.ultra.me";

Similarly to `glickmancapital.com`, `crush-dev.ultra.me` is served by
a special `ultrame_ee_dev` environment.  This uses ExpressionEngine,
which is like Wordpress in some ways but better in many others.

      "www[crush-dev.ultra.me][enabled]"                string => "true";
      "www[crush-dev.ultra.me][type]"                   string => ""; # plain HTTP
      "www[crush-dev.ultra.me][vhost]"                  string => "70.39.130.41:80";
      "www[crush-dev.ultra.me][name]"                   string => "crush-dev.ultra.me:80";
      "www[crush-dev.ultra.me][aliases]"                string => "";
      "www[crush-dev.ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[crush-dev.ultra.me][SSLCertificateKeyFile]"  string => "";
      "www[crush-dev.ultra.me][SSLCertificateFile]"     string => "";
      "www[crush-dev.ultra.me][DirectoryIndex]"         string => "index.php";
      "www[crush-dev.ultra.me][DocumentRoot]"           string => "$(e[ultrame_ee_dev][git/deploy])";
      "www[crush-dev.ultra.me][overrides]"              string => "FileInfo Options";
      "www[crush-dev.ultra.me][extras]"                 string => "";
      "www[crush-dev.ultra.me][url_aliases]"            string => "
RewriteRule /terms /mobile-plans-terms-of-use [PT]
RewriteRule /activate https://services.ultra.me/activate.php [R]
";

Some special rewrite rules for this website/

      # ultrame_ee_prod environment
      "e[ultrame_ee_prod][git/repo]" string => "https://cmviewer:bmb00@source.hometowntelecom.com:8843/scm/CRSH/ultra-ee.git";
      "e[ultrame_ee_prod][git/branch]"   string => "master";
      "e[ultrame_ee_prod][git/deploy]"   string => "$(g.wwwroot)/ultrame_ee_prod/ultra-ee";
      "e[ultrame_ee_prod][git/schedule]" string => "any"; # always deploy
      "e[ultrame_ee_prod][git/chown/o]"  string => "apache";
      "e[ultrame_ee_prod][git/chown/g]"  string => "apache";
      "e[ultrame_ee_prod][git/runas]"    string => "apache";

      "e[ultrame_ee_prod][credentials]"  string => "";
      "e[ultrame_ee_prod][db/ccv]"       string => "";
      "e[ultrame_ee_prod][db/TSIP]"      string => "";
      "e[ultrame_ee_prod][mc/prefix]"    string => "";
      "e[ultrame_ee_prod][mc/server]"    string => "";
      "e[ultrame_ee_prod][mc/port]"      string => "";
      "e[ultrame_ee_prod][security/env]" string => "public";

      "e[ultrame_ee_prod][www/sites]"    string => "ultra.me";

This is the production `ultra.me` website.  Keep it working.

      "www[ultra.me][enabled]"                string => "true";
      "www[ultra.me][type]"                   string => ""; # plain HTTP
      "www[ultra.me][vhost]"                  string => "70.39.130.40:80";
      "www[ultra.me][name]"                   string => "ultra.me:80";
      "www[ultra.me][aliases]"                string => "www.ultra.me:80";
      "www[ultra.me][favicon]"                string => "$(ultra_favicon)";
      "www[ultra.me][SSLCertificateKeyFile]"  string => "";
      "www[ultra.me][SSLCertificateFile]"     string => "";
      "www[ultra.me][DirectoryIndex]"         string => "index.php";
      "www[ultra.me][DocumentRoot]"           string => "$(e[ultrame_ee_prod][git/deploy])";
      "www[ultra.me][overrides]"              string => "FileInfo Options";
      "www[ultra.me][extras]"                 string => "";
      "www[ultra.me][url_aliases]"            string => "
RewriteRule /terms /mobile-plans-terms-of-use [PT]
RewriteRule /activate https://services.ultra.me/activate.php [R]
";

      # uvnv-wordpress_prod environment

      "e[uvnv-wordpress_prod][git/repo]"     string => "https://cmviewer:bmb00@source.hometowntelecom.com:8843/scm/WEB/uvnv-wordpress.git";
      "e[uvnv-wordpress_prod][git/branch]"   string => "master";
      "e[uvnv-wordpress_prod][git/deploy]"   string => "$(g.wwwroot)/uvnv-wordpress_prod/uvnv-wordpress";
      "e[uvnv-wordpress_prod][git/schedule]" string => "!any"; # disable deployments
      "e[uvnv-wordpress_prod][git/chown/o]"  string => "chris";
      "e[uvnv-wordpress_prod][git/chown/g]"  string => "apache";
      "e[uvnv-wordpress_prod][git/runas]"    string => "chris";

      "e[uvnv-wordpress_prod][credentials]"  string => "";
      "e[uvnv-wordpress_prod][db/ccv]"       string => "";
      "e[uvnv-wordpress_prod][db/TSIP]"      string => "";
      "e[uvnv-wordpress_prod][mc/prefix]"    string => "";
      "e[uvnv-wordpress_prod][mc/server]"    string => "";
      "e[uvnv-wordpress_prod][mc/port]"      string => "";
      "e[uvnv-wordpress_prod][security/env]" string => "public";

      "e[uvnv-wordpress_prod][www/sites]"    string => "uvnv.com";

      "www[uvnv.com][enabled]"                string => "true";
      "www[uvnv.com][type]"                   string => ""; # plain HTTP
      "www[uvnv.com][vhost]"                  string => "70.39.130.39:80";
      "www[uvnv.com][name]"                   string => "uvnv.com:80";
      "www[uvnv.com][aliases]"                string => "www.uvnv.com:80";
      "www[uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[uvnv.com][SSLCertificateKeyFile]"  string => "";
      "www[uvnv.com][SSLCertificateFile]"     string => "";
      "www[uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[uvnv.com][DocumentRoot]"           string => "$(e[uvnv-wordpress_prod][git/deploy])";
      "www[uvnv.com][overrides]"              string => "All";
      "www[uvnv.com][extras]"                 string => "";
      "www[uvnv.com][url_aliases]"            string => "";

`uvnv.com` is a Wordpress site.

      # indiald_prod environment

      "e[indiald_prod][git/repo]"     string => "$(web_repo)";
      "e[indiald_prod][git/branch]"   string => "prod";
      "e[indiald_prod][git/deploy]"   string => "$(g.wwwroot)/indiald_prod/web";
      "e[indiald_prod][git/schedule]" string => "!quick"; # deploy when !quick (hourly)
      "e[indiald_prod][git/chown/o]"  string => "apache";
      "e[indiald_prod][git/chown/g]"  string => "apache";
      "e[indiald_prod][git/runas]"    string => "apache";

      "e[indiald_prod][credentials]"  string => "tel_data_api_accounts messaging globals";
      "e[indiald_prod][db/ccv]"       string => "BA";
      "e[indiald_prod][db/TSIP]"      string => "3.9";
      "e[indiald_prod][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[indiald_prod][mc/server]"    string => "localhost";
      "e[indiald_prod][mc/port]"      string => "11211";
      "e[indiald_prod][security/env]" string => "public";

      "e[indiald_prod][www/sites]"    string => "2443-api.hometowntelecom.com api.hometowntelecom.com";

`indiald.com` is served by this production environment through a nasty
ASP-to-REST bridge.  Don't break it.

      "www[2443-api.hometowntelecom.com][enabled]"                string => "true";
      "www[2443-api.hometowntelecom.com][type]"                   string => "ssl";
      "www[2443-api.hometowntelecom.com][vhost]"                  string => "70.39.130.36:2443";
      "www[2443-api.hometowntelecom.com][name]"                   string => "api.hometowntelecom.com:2443";
      "www[2443-api.hometowntelecom.com][aliases]"                string => "";
      "www[2443-api.hometowntelecom.com][favicon]"                string => "$(uv_favicon)";
      "www[2443-api.hometowntelecom.com][SSLCertificateKeyFile]"  string => "$(htt_cert_key)";
      "www[2443-api.hometowntelecom.com][SSLCertificateFile]"     string => "$(htt_cert_crt)";
      "www[2443-api.hometowntelecom.com][DirectoryIndex]"         string => "index.html";
      "www[2443-api.hometowntelecom.com][DocumentRoot]"           string => "$(e[indiald_prod][git/deploy])";
      "www[2443-api.hometowntelecom.com][overrides]"              string => "FileInfo Options";
      "www[2443-api.hometowntelecom.com][extras]"                 string => "";
      "www[2443-api.hometowntelecom.com][url_aliases]"            string => "";

      "www[api.hometowntelecom.com][enabled]"                string => "true";
      "www[api.hometowntelecom.com][type]"                   string => "ssl";
      "www[api.hometowntelecom.com][vhost]"                  string => "70.39.130.36:443";
      "www[api.hometowntelecom.com][name]"                   string => "api.hometowntelecom.com:443";
      "www[api.hometowntelecom.com][aliases]"                string => "";
      "www[api.hometowntelecom.com][favicon]"                string => "$(uv_favicon)";
      "www[api.hometowntelecom.com][SSLCertificateKeyFile]"  string => "$(htt_cert_key)";
      "www[api.hometowntelecom.com][SSLCertificateFile]"     string => "$(htt_cert_crt)";
      "www[api.hometowntelecom.com][DirectoryIndex]"         string => "index.php";
      "www[api.hometowntelecom.com][DocumentRoot]"           string => "$(e[indiald_prod][git/deploy])";
      "www[api.hometowntelecom.com][overrides]"              string => "FileInfo Options";
      "www[api.hometowntelecom.com][extras]"                 string => "";
      "www[api.hometowntelecom.com][url_aliases]"            string => "";

      # uvnv_prod environment

      "e[uvnv_prod][git/repo]"     string => "$(web_repo)";
      "e[uvnv_prod][git/branch]"   string => "uvnv-prod";
      "e[uvnv_prod][git/deploy]"   string => "$(g.wwwroot)/uvnv_prod/web";
      "e[uvnv_prod][git/schedule]" string => "any"; # always deploy
      "e[uvnv_prod][git/chown/o]"  string => "apache";
      "e[uvnv_prod][git/chown/g]"  string => "apache";
      "e[uvnv_prod][git/runas]"    string => "apache";

      "e[uvnv_prod][credentials]"  string => "tel_data_api_accounts messaging globals";
      "e[uvnv_prod][db/ccv]"       string => "BA";
      "e[uvnv_prod][db/TSIP]"      string => "3.9";
      "e[uvnv_prod][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[uvnv_prod][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[uvnv_prod][aws/uvbucket]" string => "HTT_UVCARD_APPLICATIONDATA1";
      "e[uvnv_prod][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[uvnv_prod][mc/server]"    string => "localhost";
      "e[uvnv_prod][mc/port]"      string => "11211";
      "e[uvnv_prod][security/env]" string => "public";

      "e[uvnv_prod][partner/mfunds/env]" string => "mfunds_prod";
      "e[uvnv_prod][partner/api/allow]" string => "";

      "e[uvnv_prod][www/sites]"    string => "services.uvnv.com";

      "www[services.uvnv.com][enabled]"                string => "true";
      "www[services.uvnv.com][type]"                   string => "ssl";
      "www[services.uvnv.com][vhost]"                  string => "70.39.130.39:443";
      "www[services.uvnv.com][name]"                   string => "services.uvnv.com:443";
      "www[services.uvnv.com][aliases]"                string => "";
      "www[services.uvnv.com][favicon]"                string => "$(uv_favicon)";
      "www[services.uvnv.com][SSLCertificateKeyFile]"  string => "$(uvnv_cert_key)";
      "www[services.uvnv.com][SSLCertificateFile]"     string => "$(uvnv_cert_crt)";
      "www[services.uvnv.com][DirectoryIndex]"         string => "index.php";
      "www[services.uvnv.com][DocumentRoot]"           string => "$(e[uvnv_prod][git/deploy])";
      "www[services.uvnv.com][overrides]"              string => "All";
      "www[services.uvnv.com][extras]"                 string => "
<Files approve.php>
AuthName \"Restricted Area\"
AuthType Basic
AuthUserFile /var/tmp/uvnv-approve-passwd/.htpasswd
AuthGroupFile /dev/null
require valid-user
</Files>
";
      "www[services.uvnv.com][url_aliases]"            string => "";

`uvnv.com` calls `services.uvnv.com` for API services; this
environment provides that services site.

      # transferto_runner_prod environment

      "e[transferto_runner_prod][git/repo]"     string => "$(web_repo)";
      "e[transferto_runner_prod][git/branch]"   string => "prod_runner";
      "e[transferto_runner_prod][git/deploy]"   string => "$(g.wwwroot)/transferto_runner_prod/web";
      "e[transferto_runner_prod][git/schedule]" string => "!quick"; # deploy when !quick (hourly)
      "e[transferto_runner_prod][git/chown/o]"  string => "root";
      "e[transferto_runner_prod][git/chown/g]"  string => "root";
      "e[transferto_runner_prod][git/runas]"    string => "root";

      "e[transferto_runner_prod][credentials]"  string => "tel_data_transferto messaging globals";
      "e[transferto_runner_prod][db/ccv]"       string => "BA";
      "e[transferto_runner_prod][db/TSIP]"      string => "3.9";
      "e[transferto_runner_prod][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[transferto_runner_prod][mc/server]"    string => "localhost";
      "e[transferto_runner_prod][mc/port]"      string => "11211";

      "e[transferto_runner_prod][www/sites]"    string => "";

      # ultra_gateway_prod environment

      "e[ultra_gateway_prod][git/repo]"     string => "$(web_repo)";
      "e[ultra_gateway_prod][git/branch]"   string => "ultra-prod";
      "e[ultra_gateway_prod][git/deploy]"   string => "$(g.wwwroot)/ultra_gateway_prod/web";
      "e[ultra_gateway_prod][git/schedule]" string => "any"; # always deploy
      "e[ultra_gateway_prod][git/chown/o]"  string => "apache";
      "e[ultra_gateway_prod][git/chown/g]"  string => "apache";
      "e[ultra_gateway_prod][git/runas]"    string => "apache";

      "e[ultra_gateway_prod][credentials]"  string => "prod_gateway globals";
      "e[ultra_gateway_prod][mc/prefix]"    string => "$(mc_prod_prefix)";
      "e[ultra_gateway_prod][mc/server]"    string => "localhost";
      "e[ultra_gateway_prod][mc/port]"      string => "11211";
      "e[ultra_gateway_prod][aws/key]"      string => "AKIAJTSLGJPAZ2UWGUAQ";
      "e[ultra_gateway_prod][aws/pfile]"    string => "$(g.pwfileroot)/aws_uv_secret";
      "e[ultra_gateway_prod][security/env]" string => "public";

      "e[ultra_gateway_prod][www/sites]"    string => "";

Finally, the gateway production environment, mostly unremarkable.

      # mfunds_dev mFunds environment
      "mfunds[mfunds_dev][url/service]"  string => "http://indiald.mfundsmobile.com/PartnerWebService/";
      "mfunds[mfunds_dev][url/iframe]"   string => "http://indiald.mfundsmobile.com/portal/ui0/";

      "mfunds[mfunds_dev][id/client]"    string => "175700";
      "mfunds[mfunds_dev][id/sub/p]"     string => "529572";
      "mfunds[mfunds_dev][id/sub/c]"     string => "-1";
      "mfunds[mfunds_dev][id/pkg/pv]"    string => "184997";
      "mfunds[mfunds_dev][id/pkg/pp]"    string => "184765";
      "mfunds[mfunds_dev][id/pkg/cv]"    string => "-1";
      "mfunds[mfunds_dev][id/pkg/cp]"    string => "-1";

      # mfunds_prod mFunds environment
      "mfunds[mfunds_prod][url/service]" string => "https://card.uvnv.com/PartnerWebService/";
      "mfunds[mfunds_prod][url/iframe]"  string => "https://card.uvnv.com/portaldev/ui0/";

      "mfunds[mfunds_prod][id/client]"   string => "356247";
      "mfunds[mfunds_prod][id/sub/p]"    string => "633289";
      "mfunds[mfunds_prod][id/sub/c]"    string => "633293";
      "mfunds[mfunds_prod][id/pkg/pv]"   string => "152112";
      "mfunds[mfunds_prod][id/pkg/pp]"   string => "152111";
      "mfunds[mfunds_prod][id/pkg/cv]"   string => "152117";
      "mfunds[mfunds_prod][id/pkg/cp]"   string => "152116";

The `mfunds` array can be safely ignored.

      # IP ranges for security environments

      "ranges[internal]" string => "73.205.112.0/26 70.39.130.0/25 172.16.200.0/24 69.86.226.45 208.105.5.30";
      "ranges[partners]" string => "66.251.29.125";
      "ranges[threeci]" string => "64.95.69.115 64.94.225.115";
      "ranges[ALL]" string => ".*";

The `ranges` array is for convenience, so we don't have to reference
them explicitly in each security environment, and so people can edit
them safely.

      # security environments
      "security[internal_dev][ip/allow]"   string => "$(ranges[internal])";
      "security[internal_dev][ip/deny]"    string => "$(ranges[ALL])";
      "security[internal_dev][f/meta]"     string => "1";
      "security[internal_dev][f/sql_log]"  string => "1";
      "security[internal_dev][f/test]"     string => "1";
      "security[internal_dev][f/raise]"    string => "1";
      "security[internal_dev][f/fake]"     string => "1";
      "security[internal_dev][auth/en]"    string => "1";
      "security[internal_dev][auth/mode]"  string => "HTTPAUTH=dougmeli:Flora=/home/ht/uv-htpasswd";

This is an internal security environment.  The various `f/*` settings
are for the PHP code, in `partner.php` mostly.  The `ip/*` settings
are used by `partner.php` to see if it should block access (thus they
are not true IP blocks).  Finally, the `auth/*` settings declare
whether authentication is enabled and if so, what the username and
password, and htpasswd file are.

      "security[external_dev][ip/allow]"   string => "$(ranges[ALL])";
      # "security[external_dev][ip/allow]"   string => "$(ranges[internal]) $(ranges[partners])";

You may want to review this.

      "security[external_dev][ip/deny]"    string => "$(ranges[ALL])";
      "security[external_dev][f/meta]"     string => "1";
      "security[external_dev][f/sql_log]"  string => "1";
      "security[external_dev][f/test]"     string => "1";
      "security[external_dev][f/raise]"    string => "1";
      "security[external_dev][f/fake]"     string => "1";
      "security[external_dev][auth/en]"    string => "1";
      "security[external_dev][auth/mode]"  string => "HTTPAUTH=dougmeli:Flora=/home/ht/uv-htpasswd";

      "security[internal_ad_dev][ip/allow]"   string => "$(ranges[internal])";
      "security[internal_ad_dev][ip/deny]"    string => "$(ranges[ALL])";
      "security[internal_ad_dev][f/meta]"     string => "1";
      "security[internal_ad_dev][f/sql_log]"  string => "1";
      "security[internal_ad_dev][f/test]"     string => "1";
      "security[internal_ad_dev][f/raise]"    string => "1";
      "security[internal_ad_dev][f/fake]"     string => "1";
      "security[internal_ad_dev][auth/en]"    string => "1";
      # "security[internal_ad_dev][auth/mode]"  string => "AD";
      "security[internal_ad_dev][auth/mode]"  string => "HTTPAUTH=dougmeli:Flora=/home/ht/uv-htpasswd";

This environment is intended for LDAP or AD authentication.

      "security[demo][ip/allow]"   string => "$(ranges[ALL])"; # TODO
      "security[demo][ip/deny]"    string => "NONE"; # TODO
      "security[demo][f/meta]"     string => "0";
      "security[demo][f/sql_log]"  string => "0";
      "security[demo][f/test]"     string => "0";
      "security[demo][f/raise]"    string => "0";
      "security[demo][f/fake]"     string => "0";
      "security[demo][auth/en]"    string => "1";
      "security[demo][auth/mode]"  string => "HTTPAUTH=dougmeli:Flora=/home/ht/uv-htpasswd";

      "security[public][ip/allow]"   string => "$(ranges[ALL])"; # TODO
      "security[public][ip/deny]"    string => "NONE"; # TODO
      "security[public][f/meta]"     string => "0";
      "security[public][f/sql_log]"  string => "0";
      "security[public][f/test]"     string => "0";
      "security[public][f/raise]"    string => "0";
      "security[public][f/fake]"     string => "0";
      "security[public][auth/en]"    string => "0";
      "security[public][auth/mode]"  string => "OFF";

      "security[public_logging][ip/allow]"   string => "$(ranges[ALL])"; # TODO
      "security[public_logging][ip/deny]"    string => "NONE"; # TODO
      "security[public_logging][f/meta]"     string => "0";
      "security[public_logging][f/sql_log]"  string => "1";
      "security[public_logging][f/test]"     string => "0";
      "security[public_logging][f/raise]"    string => "0";
      "security[public_logging][f/fake]"     string => "0";
      "security[public_logging][auth/en]"    string => "0";
      "security[public_logging][auth/mode]"  string => "OFF";

Phew!  All done configuring environments!!!

      # generic stuff

      "sites_spaces[$(elist)]"  string => "$(e[$(elist)][www/sites])";
      "canon_env[$(elist)]"     string => canonify("$(elist)");

These are temporary variables for passing data around.  `$(elist)` is
the list of environments, coming from the `environments` variable
defined globally, if you'll recall.

```
  methods:
      "write"  usebundle => write_environment("$(elist)",
                                              "$(environments.e[$(elist)][www/sites])",
                                              "$(environments.e[$(elist)][credentials])");

      "do"     usebundle => do_environment("$(elist)",
                                           # canonify("$(elist)"), # TODO: report bug with this
                                           "$(canon_env[$(elist)])",
                                           "$(environments.e[$(elist)][www/sites])",
                                           "$(environments.e[$(elist)][credentials])",
                                           "$(environments.e[$(elist)][git/deploy])");

}
```

We finsh the `environments` bundle with followup calls to the
`write_environment` and `do_environment` bundles.

```
bundle agent write_environment(env, sites_string, credentials_string)
{
  classes:
      "$(canon_env)_has_mfunds" expression => regcmp("[a-zA-Z_]+", "$(mfunds_env)");
      "$(canon_env)_has_security" expression => regcmp("[a-zA-Z_]+", "$(security_env)");
```

The classes above are only defined if the environment `env` has a
mFunds or security environment.

      # TODO: remove unused environments
    vars:
      "canon_env"          string => canonify("$(env)");

      "env_configroot"     string => "$(g.configroot)/$(env)",
      policy => "free";

      "site_configroot"    string => "$(g.configroot)/sites",
      policy => "free";

      "mfunds_env"         string => "$(environments.e[$(env)][partner/mfunds/env])";
      "mfunds_configroot"  string => "$(g.configroot)/mfunds/$(mfunds_env)",
      policy => "free";

      "security_env"         string => "$(environments.e[$(env)][security/env])";
      "security_configroot"  string => "$(g.configroot)/security/$(security_env)",
      policy => "free";

      "cred_configroot"  string => "$(g.configroot)/credentials",
      policy => "free";

      "evars"              slist  => getindices("environments.e[$(env)]");
      "sites"              slist  => splitstring("$(sites_string)", " ", "100");
      "credentials"        slist  => splitstring("$(credentials_string)", " ", "100");

      # all the mFunds variables
      "mvars"              slist  => getindices("environments.mfunds[$(mfunds_env)]"),
      policy => "free",
      ifvarclass => "$(canon_env)_has_mfunds";

      "mvars"              slist  => { "cf_null" },
      policy => "free",
      ifvarclass => "!$(canon_env)_has_mfunds";

      # all the security variables
      "svars"              slist  => getindices("environments.security[$(security_env)]"),
      policy => "free",
      ifvarclass => "$(canon_env)_has_security";

      "svars"              slist  => { "cf_null" },
      policy => "free",
      ifvarclass => "!$(canon_env)_has_security";

All these variables are setting up a way to record all the things
about environment `env` that code or users may find interesting.

    files:
      "$(env_configroot)/."
      perms => mog("755", "root", "root"),
      create => "true";

      "$(env_configroot)/$(evars)"
      perms => mog("644", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("$(environments.e[$(env)][$(evars)])");

For each environment variable, e.g. `a/b/c`, we create a file under a
subdirectory of the `configroot` defined in `g`.  The file's contents
are the value in the `e` array for that key.

      "$(mfunds_configroot)/$(mvars)"
      perms => mog("644", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("$(environments.mfunds[$(mfunds_env)][$(mvars)])"),
      ifvarclass => "$(canon_env)_has_mfunds";

      "$(security_configroot)/$(svars)"
      perms => mog("644", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("$(environments.security[$(security_env)][$(svars)])"),
      ifvarclass => "$(canon_env)_has_security";

We do the same for every piece of data in the mFunds and security
environments.

    methods:
      "write credentials" usebundle => write_environment_credentials("$(cred_configroot)", $(credentials), "environments.cred");

      "write site" usebundle => write_site("$(site_configroot)", "$(sites)");

      "econfig" usebundle => write_env_config("$(env_configroot)",
                                              "e",
                                              "$(env)");

      "mconfig" usebundle => write_env_config("$(env_configroot)",
                                              "mfunds",
                                              "$(mfunds_env)"),
      ifvarclass => "$(canon_env)_has_mfunds";

      "sconfig" usebundle => write_env_config("$(env_configroot)",
                                              "security",
                                              "$(security_env)"),
      ifvarclass => "$(canon_env)_has_security";

      "cconfig" usebundle => write_env_config("$(env_configroot)/$(credentials)",
                                              "cred",
                                              "$(credentials)");

Yay, more method calls.  Now for this environment, we call
`write_site` for each of its sites; and then we call
`write_env_config` for the environment itself, and then for its mFunds
and security environments, and then for its credentials.

    reports:
     verbose::
      "Wrote environment config $(env) into $(env_configroot)";

      "Wrote mFunds environment config $(mfunds_env) into $(mfunds_configroot)",
      ifvarclass => "$(canon_env)_has_mfunds";

      "Wrote security environment config $(security_env) into $(security_configroot)",
      ifvarclass => "$(canon_env)_has_security";

We finish the bundle with some reports.

```
bundle agent write_environment_credentials(root, credential, array)
{
  vars:
      "cvars" slist => getindices("$(array)[$(credential)]"),
      policy => "free";

  files:
      "$(root)/$(credential)/$(cvars)",
      perms => mog("644", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("$($(array)[$(credential)][$(cvars)])");
}
```

The environment credentials are recorded by this
`write_environment_credentials` bundle.  It finds all the keys of the
credential array and for each one, records it in a file.

```
bundle agent write_env_config(root, type, env)
{
  classes:
      "mfunds_mode" expression => strcmp("mfunds", "$(type)");
      "cred_mode" expression => strcmp("cred", "$(type)");
      "security_mode" expression => strcmp("security", "$(type)");

      "messaging_cred_mode" expression => strcmp("$(env)", "messaging");
```

This bundle records the configuration in different ways depending on
what kind of information the configuration represents.

    vars:
      "vars" slist  => getindices("environments.$(type)[$(env)]"),
      policy => "free";

First, get the list of variables.

    mfunds_mode::
      "safe" slist  => { "url/service", "url/iframe" },
      policy => "free";

    !messaging_cred_mode.cred_mode::
      "safe" slist  => {
                         "ultra/disable/provisioning",
                         "ultra/disable/activation"
                       },
      policy => "free";

    messaging_cred_mode.cred_mode::
      "safe" slist  => { "cf_null" },
      policy => "free";

    security_mode::
      "safe" slist  => { "cf_null" },
      policy => "free";

    !security_mode.!cred_mode.!mfunds_mode::
      "safe" slist  => { "ultra/api/sites", "partner/mfunds/env", "ultra/smarty/key", "credentials" },
      policy => "free";

Determine the `safe` slist of keys that will be written in the
aggregated Javascript file.  This should be a subset of the `vars`
slist.

    any::
      "php_var_lines" slist => maplist("'$(this)' => '$(environments.$(type)[$(env)][$(this)])'", "vars"),
      policy => "free";

      "php_var_all" string => join(",$(const.n)", "php_var_lines"),
      policy => "free";

      "php" string => "<?php
$$(type)_config = array(
$(php_var_all)
);
?>",
      policy => "free";

Record the configuration as a PHP file.

      "perl_var_lines" slist => maplist("'$(this)' => '$(environments.$(type)[$(env)][$(this)])'", "vars"),
      policy => "free";

      "perl_var_all" string => join(",$(const.n)", "perl_var_lines"),
      policy => "free";

      "perl" string => "
our %$(type)_config = (
$(perl_var_all)
);
",
      policy => "free";

Record the configuration as a Perl file.

      "js_var_lines" slist => maplist("'$(this)': '$(environments.$(type)[$(env)][$(this)])'", "safe"),
      policy => "free";

      "js_var_all" string => join(",$(const.n)", "js_var_lines"),
      policy => "free";

      "js" string => "
$(type)_config = {
$(js_var_all)
};
",
      policy => "free";

Record the configuration as a Javascript file (note the use of `safe`
instead of `vars`).  This will be visible externally so be careful.

    files:
      "$(root)/$(type).php"
      perms => mog("644", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("$(php)");

      "$(root)/$(type).pm"
      perms => mog("644", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("$(perl)");

      "$(root)/$(type).js"
      perms => mog("644", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("$(js)");

Here we actually write the data we defined above.

    reports:
      veryverbose::
       "Wrote $(root)/$(type) with data $(php)";

And, when `-Dveryverbose` is used, we print all the PHP data.  Trust
me, it's a lot.

```
bundle agent write_site(root, site)
{
  vars:
      "wvars" slist  => getindices("environments.www[$(site)]");

  files:
      "$(root)/."
      perms => mog("755", "root", "root"),
      create => "true";

      "$(root)/$(site)/$(wvars)"
      perms => mog("644", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("$(environments.www[$(site)][$(wvars)])");

  reports:
    veryverbose::
      "Wrote site config $(site) into $(root)";
}
```

This bundle simply writes all the site configuration to one files per
key under a given root.

```
bundle agent do_environment(env, cenv, sites_string, credentials_string, dpath)
{
  classes:
      "do_mirror" expression => "any",
      ifvarclass => "$(expression)";
```

The `do_environment` bundle actually does the work necessary to set up
the environment.

Here's were we first set the *do_mirror* class based on whether
`$(expression)` is true.  This expression, defined below, is known as
the environment's `git/schedule` in the scope of the `environments`
bundle.

      "enabled_site_$(canon[$(sites)])"
      expression => strcmp("$(environments.www[$(sites)][enabled])", "true");

      "have_checkout" expression => fileexists("$(stage)/.git");
      "have_path" expression => fileexists("$(dpath)/.git");

      "empty_sites" expression => strcmp("$(sites_string)", "");

Is the site enabled?  We set a class like `enabled_site_uvnv_com` for `uvnv.com` if the site's `enabled` key is `true`.

    vars:
      "expression"  string => "$(environments.e[$(env)][git/schedule])";

      "origin"      string => "$(environments.e[$(env)][git/repo])";
      "branch"      string => "$(environments.e[$(env)][git/branch])";
      "stage"       string => "$(dpath).stage";
      "parent_path" string => execresult("/usr/bin/dirname $(stage)", "noshell");

`stage` is an intermediate location where we clone or update the Git
checkout before synchronizing it with the final place.  This is
necessary because the Git checkout would leave files unreadable
occasionally despite our efforts.

      "vcs"         string => "/usr/bin/git";
      "runas"       string => "$(environments.e[$(env)][git/runas])";
      "perms_owner" string => "$(environments.e[$(env)][git/chown/o])";
      "perms_group" string => "$(environments.e[$(env)][git/chown/g])";
      "umask"       string => "022";

    empty_sites::
      "sites"              slist  => { "cf_null" };
      "canon[$(sites)]"    string => canonify("$(sites)");

    !empty_sites::
      "sites"              slist  => splitstring("$(sites_string)", " ", "100");
      "canon[$(sites)]"    string => canonify("$(sites)");

These variables will be used to figure out what sites we're doing.

    files:
     !have_checkout::
      "$(parent_path)/."
      perms => mog("755", "$(runas)", "apache"),
      create => "true";

When without a checkout, create the parent path.

    have_checkout::
      "$(stage)/."
      depth_search => recurse("inf"),
      file_select => exclude("registrations.txt"),
      perms => og("$(perms_owner)", "$(perms_group)");

      "$(stage)/cm_env_name"
      perms => mog("644", "$(perms_owner)", "$(perms_group)"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("$(env)");

With a checkout, change the staging directory's permissions and create
a `cm_env_name` file holding the environment name.

    have_dpath::
      "$(dpath)/data/registrations.txt"
      perms => mog("644", "apache", "apache");

This is a hack for UI work.

    methods:
     do_mirror||!have_checkout::
      "deploy environment" usebundle => cfdc_vcs:mirror("mirror_$(cenv)_",
                                                        "mirror_$(cenv)_",
                                                        $(vcs),
                                                        $(stage),
                                                        $(origin),
                                                        $(branch),
                                                        $(runas),
                                                        $(umask));

When mirroring _or_ we don't have a checkout, use the `mirror` bundle
to clone or pull.

    any::
      "set up website $(sites)"
      usebundle => web_environment("$(g.wwwroot)", "$(sites)", "$(env)"),
      ifvarclass => "enabled_site_$(canon[$(sites)])";

Oh look, another bundle call.  This one actually sets up the web
environment if the site is enabled.

    commands:
     do_mirror.have_checkout::
      "/bin/chmod -R a+rX $(stage)";
      "/usr/bin/rsync -a $(stage)/ $(dpath)/";

When mirroring _and_ we have a checkout, we use `/bin/chmod` and
`/usr/bin/rsync` to install from the staging directory to the final
deployment path with the right permissions.

    reports:
     verbose::
      "Web environment $(env) => site $(sites) (enabled = $(environments.www[$(sites)][enabled]))";

    cfengine::
      "Disabled or undefined site $(sites), skipping it!"
      ifvarclass => "!enabled_site_$(canon[$(sites)])";

    !have_checkout::
      "Setting up environment $(env) checkout from scratch: $(origin) checkout into $(dpath) via $(stage), if expression '$(expression)'";
    verbose.do_mirror::
      "Updating the checkout of environment $(env)";
    verbose.have_checkout::
      "We already have the checkout of environment $(env)";

We heard you like reports so we put some in this bundle, too.

More with web stuff later, but for now we'll step back to sysadmin
stuff.

```
bundle agent base
{
  commands:
      # always run this, regardless, because we hatessssss SELinux
      "/usr/sbin/setenforce 0";
}
```

This is base configuration for everyone.

```
bundle agent users
{
      # generate password with
      # perl -e '$pass = <>; chomp $pass; @letters = ("A".."Z", "a".."z", "0".."9", "/", "."); $salt = join("", map { $letters[rand@letters]; } (0..85)); print crypt($pass, q[$6$] . $salt) . "\n";'
```

Here we'll create users.  This bundle is supposed to be a holdover until we get proper LDAP/AD authentication.

    classes:
      "steve_exists"   expression => returnszero("/usr/bin/id steve   | /bin/grep -q uid", "useshell");
      "tzz_exists"     expression => returnszero("/usr/bin/id tzz     | /bin/grep -q uid", "useshell");
      "rizwank_exists" expression => returnszero("/usr/bin/id rizwank | /bin/grep -q uid", "useshell");
      "jesse_exists"   expression => returnszero("/usr/bin/id jesse   | /bin/grep -q uid", "useshell");

    web_dev_01::
      "rgalli_exists"  expression => returnszero("/usr/bin/id rgalli  | /bin/grep -q uid", "useshell");
	web_01::
      "rgalli_exists"  expression => returnszero("/usr/bin/id rgalli  | /bin/grep -q uid", "useshell");

Hack to find if a user exists already

    commands:
     !jesse_exists::
      "/usr/sbin/groupadd -g 1500 jesse";
      "/usr/sbin/useradd -d /home/jesse -c 'Jesse Anderson' -g 1500 -m -p '$6$/Zx5Qr9k$wLTAuRpBUQNEEPnJhstQQe/rU/Veq69s.Ysqzm7EmSbQ9QZSxdJRR5YRUYP1jyRP5D4ddtlkzSXIzAGh6iNmV/' -s /bin/bash -u 1500 jesse";

    !rizwank_exists::
      "/usr/sbin/groupadd -g 501 rizwank";
      "/usr/sbin/useradd -d /home/rizwank -c 'Rizwan Kassim' -g 501 -m -p '$6$t1MmGAHM$8iGIS8m1IG8rQS/Fy4sSAOPMuzwCoFKcrWlrPRi5LVq/mVVijLfEpIVJWo5hPViGbEgdPOVWQMGLPO4HpWXUt.' -s /bin/bash -u 501 -G wheel rizwank";

    !tzz_exists::
      "/usr/sbin/groupadd -g 500 tzz";
      "/usr/sbin/useradd -d /home/tzz -c 'Ted Z' -g 500 -m -p '$6$ua0LaK76UW3e44MI$djh0ZU6W9pMEFsd4Lk42mSB1NhFBRxD.PprTg3JDz2OghcfMFAoYfrZaztNDCfCIm5dej04eNhcaTIkvIAkqA1' -s /bin/zsh -u 500 -G wheel tzz";

    !steve_exists::
      "/usr/sbin/groupadd -g 1501 steve";
      "/usr/sbin/useradd -d /home/steve -c 'Steve Taylor' -g 1501 -m -p '$6$FgbyJsy.LxVT7CdE$3nP.fZcUvALWw4uvPkB.HcWFWa9jEme4R2ep8Y9cCz2z7..o9qc6SLSC2OHai.qNE3G4pJ3fXUOk6kB1VQT5P1' -s /bin/bash -u 1501 -G wheel steve";

    !rgalli_exists.web_dev_01::
      "/usr/sbin/groupadd -g 1502 rgalli";
      "/usr/sbin/useradd -d /home/rgalli -c 'Raffaello Galli' -g 1502 -m -p '$6$DPoOQ2qPZzJ7OEEK$zNqG8COuUb82MRH9bT1Nrwh9Hry1fg8AYVbQTEyYQ8BA/gpEqO2rSbErZoOBvZWnQy1vVGpXZWsX.LOyfvd3z.' -s /bin/bash -u 1502 rgalli";
    !rgalli_exists.web_01::
      "/usr/sbin/groupadd -g 1503 rgalli";
      "/usr/sbin/useradd -d /home/rgalli -c 'Raffaello Galli' -g 1503 -m -p '$6$DPoOQ2qPZzJ7OEEK$zNqG8COuUb82MRH9bT1Nrwh9Hry1fg8AYVbQTEyYQ8BA/gpEqO2rSbErZoOBvZWnQy1vVGpXZWsX.LOyfvd3z.' -s /bin/bash -u 1503 rgalli";

Hack to create the user.  Note how crappily the UIDs are handled.

```
bundle agent rcfiles
{
  classes:
      "splunk_boot_enabled" expression => fileexists("/etc/init.d/splunk");
      "splunk_installed" expression => fileexists("/opt/splunkforwarder");

    splunk_installed::
      "splunk_configured" expression => returnszero("/bin/grep -q tardis $(splunk_root)/etc/system/local/outputs.conf", "useshell");
```

The `rcfiles` bundle installs configuration files and restarts daemons
if necessary.  The Splunk piece is the most complicated, and here we
start on it.

    processes:
     web_dev_01::
      "soapy-server" restart_class => "soapy_server_start";
      "soapy-piper" restart_class => "soapy_piper_start";

    any::
      "splunkd" restart_class => "splunk_start";

    transferto_prod_runner_home::
      "transferto-runner.pl" restart_class => "transferto_prod_runner_start";

    memcached_home::
      "memcached" restart_class => "memcached_start";

These `processes` promises define classes if the process named is not
running.

    vars:
      "splunk_root" string => "/opt/splunkforwarder";
      "splunk_control" string => "$(splunk_root)/bin/splunk";
      "allowed_hosts" slist => {
                           "heechee.asuscomm.com",
                         };

      "tzz_ssh_key" slist => {
                               "ssh-dss AAAAB3NzaC1kc3MAAACBANo2D6oQeTBAMyvLZEofJeTqOXjvQakWZ+GE3Kw+/g3XEYcExKWkpqURjsCB1pOqnx4Hl6W7aERlXB2dZ+WSlW6mdNEGUszcRaCuJzKqzvwaI1dsk41tE+DUhNywA+Uw4Ns2L8vFhrngNq/mCc2KcscvA0CX63z6q6xdLXhssdjPAAAAFQDGQBGGhTrwUTlU1NpfXlCe+bsbuwAAAIEAyU0nzTgPeeF/XpzkVAG48DQWApUVIBqJ/xMQTUMtBN9q9ewcskfLXfHum6i2dwY3/DMsVV2JcCOCTVV4Og92EEkLTW6RBmPH1QSLWh8RZ63A/7ABSwjrgwS4I7fvS8BCK+TgmBQNo1IO/J4uFDNZNFdmoo3q2gSjwuisYQOcaIwAAACADJey8DZhCqMY6MJfAuFhSNXuJQnLxrGdQQle6dY6KQT5SxQCmROczY6jDa3jxwlsIAFP++HQRs20T8sbp6zLfHm3wbn0BBRifxCzdi2Al8MgQ6U+cEspdyKuMA/n9sreHG3keACHwfgC7Da7O9Tjqhjd1nPmx/Z752kif96pVZc= tzz@heechee.beld.net from cfengine",
                         };

      "riz_ssh_key" slist => {
                               "ssh-dss AAAAB3NzaC1kc3MAAAEBAKfYybMUt2Ooo9Qeiudll074TSOVf3dQQEFenoIuKFNB2rJrythRoeR5Z6/iIZsmuuav7k/51OdJa7y/gfzOkTjV6XL06OFDaEMGSHq0T/J3vdeelseYicF145ctDshfWzYuT3mQioFragn20HPdrgYWFELLf1qMFfwP1zTATu0MT5T43pAUqrnitCKlfCvIzxvZKI/4cSmcCZJqDGOsCEr11iiXOggsZg9XXRyyapQEhB0AUZNTwMlK0JayqG3HZPJMryv9fK3WQyMOXVXyLXEEOq2TrI0uw+gqVc1L5q7DyeWS606sBk6g9pxz9trjflT2mbew04YEwQQo7F8A3GUAAAAVAP3kRYTmTJURg0J0wYqS3TjslCF5AAABAFiVciClnKRq62ILY+vH779/ouYxrG8vqiVEsCvvYnLltnx+5MzOoWNtFZb5TawKR0X2heVfSax/GZQ75irajDLg29NZ/K6v3hOuoeKpdy7UqTUNH2YMV8ot6gUuN1CRfjkSUCUqABsyw4Mh6WH2mmD56Nxf9e6lwfkYnLTVknGpqsFd/J/9LCKlEYYr71TChXGcaar6GRDo1InV8oD9cA9Y58fL9FuVEH23FvXSRmvXZ5jgxqv0xrDMr62wl3FqoPedGFUxmD9/Tg5ohOW3/52Ka5M8RoAi8acY/l265385xqU/nehc3qkYoZeIC9Vk5Mn20Qdrls/9sMW0cSzFilYAAAEATY2okYVWfiysi2iz5xtHOZZhZUug6Ga1c6fXAdO+6ZSs/3yg3y9qQXT4XXZytEVqT2NJU5YX3J/V8tJ1D+bfLuUmHPKFddNbcDCYXNJe+BXVXbM6XdT+kKiMKwejVucQoIrCT9DKz+OJYyPFLtSMOuTWGGyiA/MgQynlChnqQqlZQcZrZjE6y6nizYwKbPjHfyVpEKXZVMxQ+5znMt+byw9dD1pJutRe9wJAds6EKr5+5I2CUDn/crutBFJMQs1VA/2xH1NNXwJ815LlPzih0JbMpL8jYF8nGD0m1jQSClRHMzZiieLb4mE0ossrvRqBxpSPSKMeo0IdmgqNPuF2pg== rizwank@GEEKYMEDIA",
                               "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAz7CHBgo3LA84M42+EhgPrd7NGyD//okMFu1w6MH6tjCu4UiRjFYjFv8gPTkruZukpei9Re+hnVU+OV0JuDi5MewsYiW7BW+ndWfo/h9NweZbc4w38ZSJ4MOzlMARIMHHjyq7C9T1R8FmE320DMwk+RXrkZWZ3t42Jsr7mucF93FAaMrk4X6PQpLOtk+FrlxIO7THxLRdAanCOarldjV8PGhCUxrqSdj+B/X8gN9N1nxa/Sj+cDIJ9dR/h2rcaqdn19qjuoO4hFVpIA4BV9gEszW6MrW+jmJ7rI/W6uax4BtEZ5jS+xMtPxfvWCEhW17jGAG/eIObRbeeYx4KUj9niw== rizwank@GEEKYMEDIA"
                         };

      "root_ssh_key" slist => { @(tzz_ssh_key), @(riz_ssh_key) };

Miscellaneous variables.  Nothing complicated here.

    files:
      "/etc/rsyslog.conf"
      create => "true",
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("rsyslog"),
      edit_defaults => empty,
      edit_line => expand_block_template("$(g.fileroot)/rsyslog.conf");

This promise installs `/etc/rsyslog.conf` from a template under the
fileroot we defined in the `g` bundle.  The permissions are set
appropriately.  If the promise is repaired (the file's old contents
had to be changed), the class *rsyslog_repaired* will be defined.

      "/var/log/web"
      link_from => ln_s("/var/log/httpd");

      "/var/log/apache"
      link_from => ln_s("/var/log/httpd");

      "/var/log/ultra"
      link_from => ln_s("/var/log/htt");

This is how you link.  Riz can explain why this is needed.

      "/var/log/htt/."
      create => "true",
      perms => mog("771", "apache", "apache");

      "/var/log/htt"
      depth_search => recurse("inf"),
      perms => og("apache", "apache");

Make sure `/var/log/htt` exists and has the right permissions.

      "/var/lib/denyhosts/allowed-hosts"
      perms => mog("644", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("@(rcfiles.allowed_hosts)");

Here, we create this file based on only the contents of a variable.

      # SSH keys
      "/home/rizwank/.ssh/." perms => mog("700", "rizwank", "rizwank");
      "/home/tzz/.ssh/."     perms => mog("700", "tzz", "tzz");
      "/root/.ssh/."         perms => mog("700", "root", "root");

      "/home/rizwank/.ssh/authorized_keys"
      perms => mog("600", "rizwank", "rizwank"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("@(rcfiles.riz_ssh_key)");

      "/home/tzz/.ssh/authorized_keys"
      perms => mog("600", "tzz", "tzz"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("@(rcfiles.tzz_ssh_key)");

      "/root/.ssh/authorized_keys"
      perms => mog("600", "root", "root"),
      create => "true",
      edit_defaults => empty,
      edit_line => append_if_no_lines("@(rcfiles.root_ssh_key)");

SSH authorized keys for a few.

      "/etc/sudoers"
      perms => mog("440", "root", "root"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/sudoers");

This is a simple file copy, no template expansion is done.

      "/etc/php.ini"
      perms => mog("664", "root", "root"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/php.ini");

      "/etc/zabbix/zabbix_agentd.conf"
      perms => mog("600", "root", "root"),
      classes => context_yesnorepaired("zabbix_agentd"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/zabbix_agentd.conf");

      "/etc/ntp.conf"
      perms => mog("600", "root", "root"),
      classes => context_yesnorepaired("ntp_conf"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/ntp.conf");

      "/etc/sysconfig/memcached"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("memcached_conf"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/memcached.conf");

      "/usr/share/perl5/CPAN/Config.pm"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("cpan_config"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/CPAN-Config.pm");

      "/etc/selinux/config"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("selinux_config"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/selinux.config");

Pretty simple file copies...

    postfix_satellite::
      "/etc/postfix/main.cf"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("postfix_main"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/postfix-satellite-main.cf");

    postfix_hub::
      "/etc/postfix/main.cf"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("postfix_main"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/postfix-hub-main.cf");

Postfix-specific configurations.

    splunk_installed::
      "$(splunk_root)/etc/system/local/outputs.conf"
      perms => mog("600", "root", "root"),
      classes => context_yesnorepaired("splunk"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/splunk-outputs.conf");

      "$(splunk_root)/etc/system/local/inputs.conf"
      perms => mog("600", "root", "root"),
      classes => context_yesnorepaired("splunk"),
      edit_defaults => empty,
      edit_line => expand_block_template("$(g.fileroot)/splunk-inputs.conf");

Set up Spunk on a client.

    commands:
     selinux_config_repaired::
      "/usr/sbin/setenforce 0";

    ntp_conf_repaired::
      "/etc/init.d/ntpd start";
      "/etc/init.d/ntpd restart";
      "/sbin/chkconfig ntpd --levels 345 on";

    memcached_conf_repaired||memcached_start::
      "/etc/init.d/memcached start";
      "/sbin/chkconfig memcached --levels 345 on";

    memcached_conf_repaired::
      "/etc/init.d/memcached restart";

    postfix_main_repaired::
      "/etc/init.d/postfix start";
      "/etc/init.d/postfix restart";
      "/sbin/chkconfig postfix --levels 345 on";

    zabbix_agentd_repaired::
      "/etc/init.d/zabbix-agent start";
      "/etc/init.d/zabbix-agent restart";
      "/sbin/chkconfig zabbix-agent --levels 345 on";

    splunk_installed.!splunk_configured::
      "$(splunk_control) edit user admin -password wellcalled -role admin -auth admin:changeme";
      # broken, we'll deploy outputs.conf manually 
      # "/usr/bin/curl -k -u admin:wellcalled https://localhost:8089/services/data/outputs/tcp/group -d name=lan_receivers -d method=autobalance -d compressed=1 -d servers=tardis-01.coresite.hometowntelecom.com:8997";
#$(splunk_control) add forward-server tardis-01.coresite.hometowntelecom.com -auth admin:wellcalled
      # broken, we'll deploy inputs.conf manually 
      # "$(splunk_control) add monitor /var/log";

    splunk_installed.!splunk_boot_enabled::
      "$(splunk_control) enable boot-start";

    splunk_installed.splunk_start::
      "$(splunk_control) start --no-prompt --answer-yes --accept-license";

    splunk_repaired::
      "$(splunk_control) restart";

    soapy_server_start::
      "/etc/init.d/soapy-server restart";

    soapy_piper_start::
      "/etc/init.d/soapy-piper restart";

    rsyslog_repaired::
      "/etc/init.d/rsyslog restart";

    transferto_prod_runner_start::
      "/etc/init.d/runner-init-script.pl start";

All these commands let CFEngine start or restart daemons as needed.
The Splunk setup is a little more involved, but the general idea is to
get things working.

    reports:
     splunk_client.!splunk_installed::
      "You should install Splunk.";

    transferto_prod_runner_start::
      "Need to restart the transferto-runner script!";

    soapy_server_start::
      "Need to restart /etc/init.d/soapy-server";

    soapy_piper_start::
      "Need to restart /etc/init.d/soapy-piper";

Knock, knock!

Who's there?

Reports!  

Reports who?

Reports!

Reports who?

Reports!

Reports who?

Reports!

etc.

```
body contain setuid_in_dir_sh(u,d)
{
      exec_owner => "$(u)";
      chdir      => "$(d)";
      useshell   => "true";
}
```

This is a convenience body (in CFEngine, bodies are like key-value
parameterizations of functions) to run a command setuid something and
in a specific directory.

```
bundle agent packages
{
  vars:
      "base_package" slist =>
      {
        # Dell OMSA
        "srvadmin-all", "dell_ft_install",
        # Our basic packages
        "zsh", "screen", "postfix", "etckeeper", "telnet", "w3m",
        "zabbix-agent", "ntp", "lynx", "gcc", "freetds-devel", "vim-enhanced",
        "bind-utils", "man", "unzip", "tree", "wget", "nc", "emacs-nox",
        # "java-1.7.0-openjdk-devel",
        # "denyhosts", # disabled because it's a real pain -tzz

        "perl", "perl-CPAN", "perl-DBD-Sybase", "perl-JSON", "perl-JSON-XS",
        # for cf-sketch.pl, because Net::HTTPS doesn't pull the right things on CentOS
        "perl-IO-Socket-SSL",
        # for the gateway and other XML parsing
        "libxml2-devel",
      };

```

There are the base packages installed on *managed* machines.

      "memcached_package" slist =>
      {
        "memcached", "perl-Cache-Memcached", "perl-XML-Simple",
      };

      "web_package" slist =>
      {
        "php-pecl-memcached", "php-pear-Validate-Finance-CreditCard",
        "php-xml", "php-pear-XML-Parser", "php-pear-Date",
        "php-pecl-memcache", "php-pdo", "php",
        "php-pear-XML-Serializer", "php-channel-phpunit", "php-cli",
        "php-gd", "php-common", "php-mssql", "php-pear",
        "php-pear-Validate",
        "php-soap", "php-mcrypt",

        # PHP-MySQL glue and the mysql client binaries
        "mysql", "php-mysql",

        "httpd", "httpd-tools", "mod_ssl", "mod_auth_kerb", "ImageMagick", "graphviz",
      };

      "pears" slist =>
      {
        "Text_Password",
      };

These are the extra packages for various purposes.

    packages:

      "$(base_package)" package_policy => "addupdate", package_method => yum;

     memcached_home::
      "memcached" package_policy => "addupdate", package_method => yum;

     webserver::
      "$(web_package)" package_policy => "addupdate", package_method => yum;
      "$(memcached_package)" package_policy => "addupdate", package_method => yum;

These promises actually install the packages with `addupdate`
(meaning, only add or update, never remove) policy.

    commands:
     any::
      "/usr/bin/cpan App::cpanminus";
      "/usr/local/bin/cpanm Cache::Memcached::Fast DBI JSON::XS Daemon::Control XML::Compile::WSDL11 Any::Daemon";
      # this doesn't work, the make must be run interactively...
      # "/bin/env SYBASE=/usr /usr/local/bin/cpanm DBD::Sybase";
      "/bin/chmod -R a+rX /usr/local/lib64/perl5 /usr/share/perl5 /usr/lib64/perl5 /usr/local/share/perl5";
      "/usr/bin/yum -y install mosh --enablerepo=epel-testing";

      "$(g.cfsketch_home)/cf-sketch.pl --expert --install VCS::vcs_mirror --install Security::SSH --installtarget $(g.cmroot)/sketches -q"
      contain => in_dir("$(g.cfsketch_home)");

    webserver::
      "/usr/bin/pear install -s $(pears)";
      "/bin/chmod -R a+rX /usr/share/pear";

    webserver.Saturday.Hr12::
      "/usr/bin/pear channel-update php.pear.net";

On *any* system, we run some basic commands.  On *webserver* machines, we install a few PEAR modules.

```
bundle agent resolver
{
  vars:
      "resolver" slist => { "70.39.130.29", "8.8.4.4", "8.8.8.8" };

  files:
      # we don't use create => "true" because resolv.conf should not be missing!
      # cfengine automatically sets sys.resolv to /etc/resolv.conf
      "$(sys.resolv)" comment => "resolv.conf policy",
      perms => mog("644", "root", "root"),
      edit_defaults => std_defs_timestamp,
      edit_line => write_resolvconf("hometowntelecom.com",
                                    "ultra.local hometowntelecom.com sirius.hometowntelecom.com coresite.hometowntelecom.com",
                                    @(resolver.resolver));
}
```

The `resolver` bundle configures `/etc/resolv.conf`.

Could it be more fun?  Probably.  With a joke or two, it could be
great at parties.  "Have you met my `resolver` bundle?"

```
bundle edit_line write_resolvconf(domain,search,list)

 # domain is ...
 # search is the search domains with space
 # list is an slist of nameserver addresses

{
  delete_lines:

      ""             comment => "All blank lines";
      "#.*"          comment => "All comments";
      "domain.*"     comment => "Reset domain lines from resolver";
      "search.*"     comment => "Reset search lines from resolver";
      "nameserver.*" comment => "Reset nameservers in resolver";

  insert_lines:

      "# added by cfengine" comment => "Tag the file";
      "domain $(domain)"    comment => "Add domain to resolver";
      "search $(search)"    comment => "Add search domains to resolver";
      "nameserver $(list)"  comment => "Add name servers to resolver";
}
```

This bundle edits the `resolv.conf` file with specific data.

```
bundle agent etckeeper(dir)
{
  vars:
      "ignored" slist => { "/etc/opt/" };

      "git" string => "/usr/bin/git";

    debian::
      "etckeeper" string => "/usr/sbin/etckeeper";

    !debian::
      "etckeeper" string => "/usr/bin/etckeeper";

  classes:
      "git_exists" expression => fileexists("$(dir)/.git");

  commands:
    !git_exists::
      "$(etckeeper) init -d $(dir)";
      "$(etckeeper) commit -d $(dir) initialize";

    git_exists::
      "$(etckeeper) commit -d $(dir) checkpoint";

  files:
    git_exists::
      "$(dir)/.gitignore"
      edit_line => insert_lines($(ignored)),
      edit_defaults => std_defs_timestamp;

    redhat::
      "/etc/etckeeper/etckeeper.conf"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("etckeeper_conf"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/etckeeper-centos.conf");
}
```

The `etckeeper` bundle is very important.  It snapshots a directory
with `etckeeper` which makes it possible to look at past versions of
the directory.

```
bundle agent runner_cron_d
{
  vars:
    any::
      "crond" string => "/etc/cron.d";

  files:
      "$(crond)/."
      comment => "$(crond) cron directory",
      create => "true",
      perms => mo("755","root");

      "$(crond)/runner"
      comment => "runner cron job",
      edit_defaults => wipe_clear_nobackup,
      edit_line => write_cron_d_runner,
      create => "true",
      classes => context_yesnorepaired("runner_crond"),
      perms => mo("644","root");

  reports:
    runner_crond_repaired::
      "The runners cron.d entry was updated.";
}
```

The `runner_cron_d` bundle maintains the `/etc/cron.d/runner` cron
file.

```
bundle edit_line write_cron_d_runner
{
  vars:
      # we need to make a local copy of the slist
      "runners" slist => { getindices("g.resolved_environments") },
      policy => "free";

      "crones" slist => { getindices("g.crones") },
      policy => "free";
```

Here we edit `/etc/cron.d/runner` (but note that in the `edit_line`
bundle, we don't need to know the file name).  We get the list of
resolver runners and crones from the `g` bundle.

```
  insert_lines:
      "# edited by cfengine
SHELL=/bin/sh

MAILTO=$(g.mailto)";
```

Always insert this preamble.

      # insert one of these per environment with a runner
      "* * * * * $(environments.e[$(runners)][git/chown/o]) HTT_ENV=$(runners) HTT_CONFIGROOT=$(g.configroot)/$(runners) $(environments.e[$(runners)][git/deploy])/runners/state_machine/transition-runner.pl 5 'https://dougmeli:Flora@SITE/pr/internal/1/ultra/api/internal__ResolvePendingTransitions' $(runners) $(g.resolved_environments[$(runners)]) 2>&1 | /usr/bin/logger -i -t $(runners)/transition-runner -p user.debug";

Each environment named in `resolved_environments` gets a cron job
here.  The cron job will resolve the environment itself, plus all the
others named in the array value.

      "$(g.crones[$(crones)][cron_schedule]) apache /usr/bin/curl -is 'https://dougmeli:Flora@$(g.crones[$(crones)][site])/pr/internal/1/ultra/api/internal__Crone' -d 'mode=$(crones)' 2>&1 | /usr/bin/logger -i -t $(g.crones[$(crones)][site])/crone-runner -p user.debug";

For each crone, we add a HTTP call to the crone's site, with the crone's schedule, and with the crone's name as the `mode` parameter.

    web_01::
      "0 8,9,10,11,12,13,14,15,16 * * * $(environments.e[ultra_api_prod_internal][git/chown/o]) cd $(environments.e[ultra_api_prod_internal][git/deploy]); HTT_ENV=ultra_api_prod_internal HTT_CONFIGROOT=$(g.configroot)/ultra_api_prod_internal /usr/bin/php $(environments.e[ultra_api_prod_internal][git/deploy])/runners/monthly-charge.php 1000 today 2>&1 | /usr/bin/logger -i -t ultra_api_prod_internal/monthly-charge-runner -p user.debug";
      "* * * * * root /scripts/messaging-daemon.sh 2>&1 >> /var/log/htt/messaging-daemon.log";

These were added to handle specific business needs.

```
bundle agent cfrun
{
  vars:
    any::
      "crond" string => "/etc/cron.d";

      "cfrun_literal" string => "
# edited by cfengine
SHELL=/bin/sh
MAILTO=$(g.mailto)
*/5 * * * *	root	(/var/cfengine/bin/cf-agent -K -f git-failsafe.cf; /var/cfengine/bin/cf-agent -K -Dquick -f $(g.cmroot)/ht.cf) | logger -t cfrun -p user.debug
2 * * * *	root	(/var/cfengine/bin/cf-agent -K -f git-failsafe.cf; /var/cfengine/bin/cf-agent -K -f $(g.cmroot)/ht.cf) | logger -t cfrun -p user.debug";

  files:
    managed::
      "$(g.home)/."
      comment => "$(g.home) HT home directory",
      create => "true",
      perms => mo("755","root");

      "$(crond)/."
      comment => "$(crond) cron directory",
      create => "true",
      perms => mo("755","root");

      "$(crond)/cfrun"
      comment => "cfrun cron job",
      edit_defaults => wipe_clear_nobackup,
      edit_line => insert_lines("$(cfrun_literal)"),
      create => "true",
      classes => context_yesnorepaired("cfrun"),
      perms => mo("644","root");

  reports:
    cfrun_repaired::
      "The cfrun cron entry was updated.";
}
```

The `cfrun` bundle sets up a cron job `/etc/cron.d/cfrun` that runs
`cf-agent` with and without `-Dquick` at certain intervals you can see
above.

```
# clone of expand_template from cfengine_stdlib.cf with string + preserve_block
bundle edit_line expand_block_template(datafile)
{
  vars:
      "data" string => readfile($(datafile), 100000);

  insert_lines:

      "$(data)"
      insert_type => "preserve_block",
      comment => "Expand variables in the template string",
      expand_scalars => "true";
}
```

The `expand_block_template` bundle is special magic.  Feel free to
figure out what it does but don't remove it.

Ah, OK.  Done with `ht.cf`.  I bet you're so excited about CFEngine, you can't wait... for another .cf file!  Here's `webserver.cf`!

```
bundle agent webserver
{
  vars:
      "etc_httpd_root" string => "$(g.debug_prefix)/etc/httpd";
      "etc_httpd_htt_siteroot" string => "$(etc_httpd_root)/conf.d/htt";

      "ports" slist => { "8000", "80", "443", "2443", "8087", "8088" };
      "ports_lines" slist => maplist("Listen $(this)", "ports");
      "listen_ports" string => join("$(const.n)", "ports_lines");

      "virtual_lines" slist => maplist("NameVirtualHost $(this)", "virtual_addresses");
      "virtuals" string => join("$(const.n)", "virtual_lines");
```

The `webserver` bundle sets up the whole webserver.  Here we define
some global variables.

```
    web_dev_01::
      # "server_name" string => "web-dev-01.hometowntelecom.com:80";
      "server_name" string => "$(environments.www[dev.uvnv.com][name])";
      "server_root" string => "$(environments.e[tzz_dev][git/deploy])";
      "virtual_addresses" slist => { "70.39.130.37:80", "70.39.130.37:443", "70.39.130.37:2443", "70.39.130.37:8000", "70.39.130.41:80", "70.39.130.41:443" };
      "extra" string => "
LoadModule auth_kerb_module modules/mod_auth_kerb.so

<Directory \"$(environments.e[tzz_dev][git/deploy])\">
    Options Indexes FollowSymLinks
    AllowOverride None
    Order allow,deny
    Allow from all
</Directory>
";


    web_01::
      # "server_name" string => "web-01.hometowntelecom.com:80";
      "server_name" string => "$(environments.www[uvnv.com][name])";
      "server_root" string => "$(environments.e[uvnv-wordpress_prod][git/deploy])";
      "virtual_addresses" slist => { "70.39.130.36:80", "70.39.130.36:443", "70.39.130.36:2443", "70.39.130.36:8000", "70.39.130.39:80", "70.39.130.39:443", "70.39.130.40:80", "70.39.130.40:443", "70.39.130.45:443", };
      "extra" string => "
LoadModule auth_kerb_module modules/mod_auth_kerb.so

<Directory \"$(environments.e[uvnv-wordpress_prod][git/deploy])\">
    Options Indexes FollowSymLinks
    AllowOverride All
</Directory>
";
```

Next, for each webserver we define a specific configuration.

    files:
      "$(etc_httpd_root)/conf/httpd.conf"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("httpd"),
      create => "true",
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/common.httpd.conf");

      "$(etc_httpd_root)/conf/httpd-vars.conf"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("httpd"),
      create => "true",
      edit_defaults => empty,
      edit_line => expand_block_template("$(g.fileroot)/common.httpd-vars.conf");

      "$(etc_httpd_root)/conf.d/ssl.conf"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("httpd"),
      copy_from => local_digest_cp_nobackup("$(g.fileroot)/$(sys.uqhost).httpd.ssl.conf");

      "/var/log/httpd/."
      perms => mog("750", "apache", "apache"),
      classes => context_yesnorepaired("httpd"),
      create => "true";

      "$(g.wwwroot)/."
      perms => mog("755", "root", "apache");

We create and configure the top-level webserver root, with templates and plain file copies.

    processes:
      "httpd" restart_class => "start_httpd";

    commands:
     start_httpd::
      "/etc/init.d/httpd start";

     httpd_repaired::
      "/etc/init.d/httpd reload";

     any::
      "/sbin/chkconfig httpd --levels 345 on";

We check if `httpd` is running and if not, start it.  If required, we
reload it.

    reports:
     httpd_repaired::
      "HTTPD configured and reloaded.";

And... reports!!!

```
bundle agent web_environment (root, site, env)
{
  classes:
      "has_certificate"    expression => fileexists("$(SSLCertificateFile)");
      "has_certificatekey" expression => fileexists("$(SSLCertificateKeyFile)");
      "no_aliases"         expression => strcmp("$(aliases)", "");
      "ssl_site"           expression => strcmp("$(type)", "ssl");
      "auth_AD"            expression => strcmp("$(security_mode)", "AD");
      "auth_off"           expression => strcmp("$(security_mode)", "OFF");
      "have_auth_file"     expression => regextract(
                                                     "HTTPAUTH=[^=]+=(.+)",
                                                     $(security_mode),
                                                     "auth_file_array"
                                                   );
```

This bundle sets up an individual site `SITE.conf` file.

Here we declare classes for whether the site has the SSL certificate
and key; whether it has aliases; whether it's an *ssl_site*, and how
authentication is configured.

    vars:
      "security_env"  string => "$(environments.e[$(env)][security/env])",
      policy => "free";

      "security_mode" string => "$(environments.security[$(security_env)][auth/mode])",
      policy => "free";

      "wvars"    slist => getindices("environments.www[$(site)]"),
      policy => "free";

      "$(wvars)" string => "$(environments.www[$(site)][$(wvars)])",
      policy => "free";

      "deployed_favicon" string => "$(root)/icons/favicon.$(site).ico",
      policy => "free";

      "partners_allow" slist  => splitstring("$(environments.e[$(site)][partner/api/allow])", " ", "100");

Grab some variables here out of the `environments` bundle.

Of note is the `wvars` iteration.  We define a variable for every key
in the `www[SITE]` array.  It's a bit like Pascal or Javascript,
narrowing the scope `with` a particular record.

    have_auth_file::
      "auth_file" string => "$(auth_file_array[1])";

    !have_auth_file::
      "auth_file" string => "/home/ht/uv-htpasswd";

Do different things based on whether we were told to use a specific
htpasswd file or not.

```
    ssl_site.auth_AD::
      "root_directives" string => "
  Options Indexes
  SSLRequireSSL
  AuthType Kerberos
  AuthName \"Kerberos Login\"
  KrbMethodNegotiate On
  KrbMethodK5Passwd On
  KrbAuthRealms ULTRA.LOCAL
  KrbServiceName apache
  Krb5KeyTab /etc/httpd/conf/apache-web-dev-01.keytab
  KrbVerifyKDC off
  KrbLocalUserMapping on
  require valid-user
  AuthLDAPURL \"ldaps://keymaster.ultra.local:636/dc=ultra,dc=local?cn?sub?(objectClass=user)\" NONE
  AuthLDAPBindDN \"cn=http-web-dev-01,ou=Service Accounts,dc=ultra,dc=local\"
  AuthLDAPBindPassword \"crU+Az2=ResefuSwU!up\"
  require ldap-group \"CN=$(env),CN=Users,DC=ultra,DC=local\"

",
      policy => "free";

    !(ssl_site.auth_AD)::
      "root_directives" string => "",
      policy => "free";

    !(auth_AD||auth_off)::
      "docroot_directives" string => "
AuthUserFile $(auth_file)
AuthType Basic
AuthName \"U pw\"
Require valid-user
# AuthGroupFile $(auth_file).group
# Require group $(env)
AllowOverride $(overrides)
",
      policy => "free";

    auth_AD||auth_off::
      "docroot_directives" string => "AllowOverride $(overrides)
",
      policy => "free";

    !no_aliases::
      "alias_list"   slist  => splitstring("$(aliases)", " ", "100"),
      policy => "free";

      "alias_lines"  slist => maplist("  ServerAlias $(this)", "alias_list"),
      policy => "free";

      "aliases_str"  string => join("$(const.n)", "alias_lines"),
      policy => "free";

    no_aliases::
      "aliases_str"  string => "",
      policy => "free";
```

Set up configuration data for each type of site, using the data from
the `environments` bundle liberally.

    files:
      "$(root)/icons/."
      perms => mog("755", "root", "apache");

      "$(deployed_favicon)"
      perms => mog("644", "apache", "root"),
      copy_from => local_digest_cp_nobackup("$(favicon)");

      "$(webserver.etc_httpd_htt_siteroot)/$(site).conf"
      perms => mog("644", "root", "root"),
      classes => context_yesnorepaired("httpd"),
      create => "true",
      edit_defaults => empty,
      edit_line => expand_block_template("$(g.fileroot)/common.httpd-$(type)site.conf");

This works on the icons and then creates `SITE.conf` in the right
place with all the data we've accumulated so far.

```
  reports:
    ssl_site.!has_certificate::
      "You need to install the $(site) certificate file in $(environments.www[$(site)][SSLCertificateFile])";
    ssl_site.!has_certificatekey::
      "You need to install the $(site) certificate key file in $(environments.www[$(site)][SSLCertificateKeyFile])";
    verbose::
      "Setting up web environment for site $(site) in environment $(env), security environment $(security_env), security mode $(security_mode)";
    veryverbose::
      "$(site) wvars $(wvars) = '$($(wvars))'";
```

You figure out what the above does.  I'm not telling.




Wed Aug 19 11:56:19 PDT 2015

Re: Primo

[11:30:06] Scott Ostrander: [8/18/2015 4:32:00 PM] Raffaello Galli: https://api.primo.me/

username should be intl_prod

passwd up to you



<<< 
[11:30:17] Raffaello Galli: correct
[11:30:29] Raffaello Galli: api.primo.me   should prompt for apache credentials
[11:30:41] Raffaello Galli: [ultra/api/internal/password] <= this has nothing to do with that
[11:33:37] Ted Zlatanov: this one is done with the "security" array.  Here's an example:

      "security[intl_dev][ip/allow]"   string => "$(ranges[internal]) $(ranges[intl_dev])";
      "security[intl_dev][ip/deny]"    string => "$(ranges[ALL])";
      "security[intl_dev][f/meta]"     string => "1";
      "security[intl_dev][f/sql_log]"  string => "1";
      "security[intl_dev][f/test]"     string => "1";
      "security[intl_dev][f/raise]"    string => "1";
      "security[intl_dev][f/fake]"     string => "1";
      "security[intl_dev][auth/en]"    string => "1";
      "security[intl_dev][auth/mode]"  string => "HTTPAUTH=intl_ott_api:--passwordremoved--=/home/ht/intl-htpasswd";
[11:33:47] Raffaello Galli: yep
[11:33:58] Ted Zlatanov: so it references some IP ranges, but otherwise it's pretty clear, yeah?
[11:34:32] Raffaello Galli: Ted, I think the topology of the Primo Prod environments may be a bit different
[11:34:46] Raffaello Galli: we have a load balancer in front of 6 servers
[11:34:56] Raffaello Galli: those 6 servers have the same env
[11:35:03] Raffaello Galli: intl_prod
[11:35:15] Ted Zlatanov: ...then you connect the security settings to the environment like this:
      "e[sean_dev][security/env]" string => "internal_dev";
[11:35:47] Raffaello Galli: the environments themselves should not be behind HTTPS
[11:35:59] Raffaello Galli: the load balancer yes (api.primo.me)
[11:37:38] Raffaello Galli: I am available to discuss in detail
[11:38:31] Ted Zlatanov: Raf: all right, I can talk in the next 15 minutes.
[11:38:50] Raffaello Galli: (y)
[11:38:59] Raffaello Galli: MSISDN 9179231687
[11:49:19] Ted Zlatanov: all right, we figured it out.  What I explained above is for securing an environment directly, when it's available directly to the outside.  But primo is different, it's 6 web servers going to F5 over HTTP, then the F5 load balancer talks HTTP/S to the outside.  So the username and password should only be set on the load balancer, not in Apache or through CFEngine.
[11:50:17] Raffaello Galli: exactly
[11:50:58] Ted Zlatanov: OK, so to set a web site to be HTTP (no SSL): you just give it a type that's anything BUT "ssl":

       "www[dev-web.indiald.com][type]"                   string => "";
[11:51:26] Ted Zlatanov: you probably want to change the vhost as well.  Here's a SSL-enabled site:
      "www[tweakker-prod.ultra.me][type]"                   string => "ssl";
      "www[tweakker-prod.ultra.me][vhost]"                  string => "*:443";
[11:51:55] Ted Zlatanov: here's indiald.com, a non-SSL site:
      "www[indiald.com][type]"                   string => "";
      "www[indiald.com][vhost]"                  string => "*:80";
[11:52:57] Ted Zlatanov: Scott: so I think that's all you have to do on the CFEngine side, set the type and the vhost for api.primo.me, which is conveniently already done:
      "www[api.primo.me][type]"						string => "";
      "www[api.primo.me][vhost]"					string => "*:80";
[11:54:30] Ted Zlatanov: I hope that explains the steps.  The instructions above about API credentials and the site security settings are still valuable and should be saved, but not relevant in this specific case.  Please let me know if you want me to save the instructions.  Thanks!

